---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios de io                                       --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         31/01/2006  Cria��o                                          --
-- Leandro              lmf         03/02/2006  Cabe�alho das fun��es                            --
-- Jamerson Lima        jrfl        05/04/2006  Rework (HFLEX-TSTR-LUA-CODE-INSP001)             --
-- Jamerson Lima        jrfl        27/06/2006  Novos testes com nome de arquivos inexistentes   --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

tc1 = TestCase("create")

-------------------------------------------------------------------------------
--> Teste para criar at� 2000 arquivos
-------------------------------------------------------------------------------
function tc1.test_create1()
    local dir = "/files/"
    local last_created = 0
    local files = 1000
    assert_true(aux_create_files(last_created + 1, files), "N�o foi poss�vel criar " .. files .. " arquivos.")

    last_created = files
    files = 1500
    assert_true(aux_create_files(last_created + 1, files), "N�o foi poss�vel criar " .. files .. " arquivos.")

    last_created = files
    files = 2000
    assert_true(aux_create_files(last_created + 1, files), "N�o foi poss�vel criar " .. files .. " arquivos.")

    for i = 1, files do
        os.remove(dir .. "teste" .. i)
    end
    os.remove(dir)
end

-------------------------------------------------------------------------------
--> Teste para criar todos os arquivos usados pelos outros testes
-------------------------------------------------------------------------------
function tc1.test_create2()
    assert_true(aux_create_testfiles(), "N�o foi poss�vel criar todos os arquivos de teste.")
end

-- ****************** TESTE DE IO.OPEN ****************** --

-- Testes de IO.OPEN (21 testes)--
tc = TestCase( "io.open" )

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            inexistente                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (1/21)--
-- Tenta abrir um arquivo inexistente --
function tc.test_open1()

    local result = true
    local error_message = ""
    local error_code = 0
    local filename = "inexistente"

    result, error_message, error_code = io.open( filename, "r" )

    assert_nil( result, "test_open1. Retorno incorreto tentando abrir arquivo inexistente", true )

    assert_equal( TST_ERR_NOTFOUND, error_code, "test_open1. Codigo de erro incorreto tentando abrir arquivo inexistente", true)

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_NOTFOUND ], error_message, "test_open1. Menssagem de erro incorreta tentando abrir arquivo inexistente")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            previamente aberto utilizando o modo "r"                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (2/21)--
-- Tenta criar um arquivo v�lido e abri-lo novamente em modo 'r' --
function tc.test_open2()

    local handle1 = nil
    local handle2 = nil
    local filename = "/open2.txt"

    handle1 = io.open( filename, "rw")

    assert_not_nil( handle1, string.format( "Dependencia: Erro tentando criar %s", filename ), true )

    handle2 = io.open( filename, "r" )

    assert_not_nil( handle2, "test_open2. Erro tentando abrir em modo 'r' um arquivo previamente aberto" )

    handle1:close( )

    if( handle2 ~= nil ) then
        handle2:close( )
    end

    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            previamente aberto utilizando o modo "rw"                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (3/21)--
-- Tenta criar um arquivo v�lido e abri-lo novamente em modo 'rw' --
function tc.test_open3()

    local handle1 = nil
    local handle2 = nil
    local error_code = 0
    local error_message = ""
    local filename = "/open3.txt"

    handle1 = io.open( filename, "rw" )

    assert_not_nil( handle1, string.format( "Dependencia: Erro tentando criar %s", filename ), true )

    handle2, error_message, error_code = io.open( filename, "rw" )

    assert_nil( handle2, "test_open3. Retorno incorreto tentando abrir em modo 'r' um arquivo previamente aberto", true )

    assert_equal( TST_ERR_ISOPEN, error_code, "test_open3. Codigo de erro incorreto tentando abrir em modo 'r' um arquivo previamente aberto", true)

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_ISOPEN ], error_message, "test_open3. Menssagem de erro incorreta tentando abrir em modo 'r' um arquivo previamente aberto")

    handle1:close( )

    if( handle2 ~= nil ) then
        handle2:close( )
    end

    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            existente em um modo inv�lido                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (4/21)--
-- Tenta abrir um arquivo existente em um modo inv�lido--
function tc.test_open4()

    local result = true
    local error_code = 0
    local error_message = ""
    local filename = "/open4.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: '%s' nao encontrado", filename ), true )

    result, error_message, error_code = io.open( filename, "xyz" )

    assert_nil( result, "test_open4. Retorno incorreto tentando abrir arquivo em modo invalido", true)

    assert_equal( TST_ERR_INVALMODE, error_code, "test_open4. Codigo de erro incorreto tentando abrir arquivo em modo invalido", true)

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_INVALMODE ], error_message, "test_open4. Menssagem de erro incorreta tentando abrir arquivo em modo invalido")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open5                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            inexistente em um modo inv�lido                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (5/21)--
-- Tenta abrir um arquivo inexistente em um modo inv�lido--
-- Pra que esse teste?
-- function tc.test_open5()
--
--     local ret = io.open("lalalala", "49")
--
--     assert_nil(ret, "Erro tentando abrir arquivo inexistente em modo invalido")
-- end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open6                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (6/21)--
-- Tenta criar arquivos com nomes inv�lidos --
function tc.test_open6( )

    local result = true
    local error_code = 0
    local error_message = ""
    local filename = "/1234.567"

    result, error_message, error_code = io.open( filename, "rw" )

    assert_nil(result, string.format( "test_open6. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open6. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open6. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open7                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (7/21)--
-- Tenta criar arquivos com nomes inv�lidos --
function tc.test_open7( )

    local result = true
    local error_code = 0
    local error_message = ""
    local filename = "/open7.***"

    result, error_message, error_code = io.open( filename, "rw" )

    assert_nil(result, string.format( "test_open7. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open7. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open7. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open8                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (8/21)--
-- Tenta criar arquivos com nomes inv�lidos --
function tc.test_open8( )

    local result = true
    local error_code = 0
    local error_message = ""
    local filename = "/_open8.txt"

    result, error_message, error_code = io.open( filename, "rw" )

    assert_nil(result, string.format( "test_open8. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open8. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open8. Retorno incorreto tentando criar arquivo com nome invalido(%s)", filename ) )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open9                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            global                                                                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (9/21)--
-- Tenta criar arquivo global --
function tc.test_open9( )

    local filename = "open9.txt"

    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "test_open9. Retorno incorreto tentando criar arquivo global %s", filename ), true )

    handle:close( )

    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open10                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            sem extensao                                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (10/21)--
-- Tenta criar um arquivo v�lido sem extensao --
function tc.test_open10()

    local filename = "/open10"

    local handle  = io.open( filename, "rw" )

    assert_not_nil( filename, "test_open10. Retorno incorreto tentando criar um arquivo sem extensao" ,true )

    handle:close( )

    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open11                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            dentro de um subdiretorio                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (11/21)--
-- Tenta criar um arquivo v�lido em um subdiretorio --
function tc.test_open11( )

    local dirname  = "/open11/"
    local filename = "open11.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: %s nao encontrado", dirname ), true )

    local handle = io.open( ( dirname .. filename ), "rw" )

    assert_not_nil( handle, "test_open11. Retorno incorreto tentando criar um arquivo em um subdiretorio", true )

    handle:close( )

    os.remove( dirname .. filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open12                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            global inexistente                                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (12/21)--
-- Tenta abrir um arquivo global inexistente --
function tc.test_open12( )

    local handle, error_message, error_code = io.open( "/ne.txt", "rg" )

    assert_nil( handle, "test_open12. Retorno incorreto tentando abrir arquivo global inexistente", true )

    assert_equal( TST_ERR_NOTFOUND, error_code, "test_open12. Codigo de erro incorreto tentando criar arquivo com nome invalido", true )

    assert_equal( "/ne.txt: " .. lua_error_message[ TST_ERR_NOTFOUND ], error_message, "test_open12. Mensagem de erro incorreta tentando criar arquivo com nome invalido" )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open13                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            global previamente aberto utilizando o modo "r"                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (13/21)--
-- Tenta criar um arquivo global v�lido e abri-lo novamente em modo 'rg' --
function tc.test_open13( )

    local filename = "open13.txt"

    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser criado", filename ), true )

    local handle2 = io.open( filename, "rg" )

    if( not assert_not_nil( handle2, "Retorno incorreto tentando abrir em modo 'rg' um arquivo global previamente aberto" ) ) then
        handle:close( )
        os.remove( filename, true )
        return
    end

    handle:close( )
    handle2:close( )
    os.remove( filename, true )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open14                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            global previamente aberto utilizando o modo "rwg"                                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (14/21)--
-- Tenta criar um arquivo global v�lido e abri-lo novamente em modo 'rwg' --
function tc.test_open14()

    local filename = "open14.txt"
    local error_code = 0
    local error_message = ""

    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser criado", filename ), true )

    local handle2, error_message, error_code = io.open( filename, "rwg" )

    if( not assert_nil( handle2, "test_open14. Retorno incorreto tentando abrir em modo 'rg' um arquivo global previamente aberto" ) ) then
        handle:close( )
        os.remove( filename, true )
        return
    end

    handle:close( )
    os.remove( filename, true )

    assert_equal( TST_ERR_ISOPEN, error_code, "test_open14. Codigo de erro incorreto tentando abrir em modo 'rg' um arquivo global previamente aberto", true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_ISOPEN ], error_message, "test_open14. Mensagem de erro incorreta tentando abrir em modo 'rg' um arquivo global previamente aberto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open15                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            existente em um modo inv�lido                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (15/21)--
-- Tenta abrir um arquivo global existente em um modo inv�lido--
function tc.test_open15()

    local filename = "open15.txt"
    local error_message = ""
    local error_code = 0

    local handle = io.open( filename, "rwg")

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser criado", filename ), true )

    handle:close( )

    local handle2, error_message, error_code = io.open( filename, "xyg")

    os.remove( filename, true )

    assert_nil( handle2, "test_open15. Retorno incorreto tentando abrir arquivo em modo invalido", true )

    assert_equal( TST_ERR_INVALMODE, error_code, "test_open15. Codigo de erro incorreto tentando abrir arquivo em modo invalido", true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_INVALMODE ], error_message, "test_open15. Mensagem de erro incorreta tentando abrir arquivo em modo invalido" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open17                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            global com um nome inv�lido                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (17/21)--
-- Tenta criar arquivos globais com nomes inv�lidos --
function tc.test_open17( )

    local error_code = 0
    local error_message = ""
    local filename = "/1234.567"

    local handle, error_message, error_code = io.open( filename, "rwg")

    assert_nil(ret, string.format( "test_open17. Retorno incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true)

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open17. Codigo de erro incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open17. Mensagem de erro incorreta tentando criar arquivo global com nome invalido (%s)", filename ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open18                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            global com um nome inv�lido                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (18/21)--
-- Tenta criar arquivos globais com nomes inv�lidos --
function tc.test_open18( )

    local error_code = 0
    local error_message = ""
    local filename = "/open18.***"

    local handle, error_message, error_code = io.open( filename, "rwg")

    assert_nil(ret, string.format( "test_open18. Retorno incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true)

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open18. Codigo de erro incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open18. Mensagem de erro incorreta tentando criar arquivo global com nome invalido (%s)", filename ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open19                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta criar um arquivo            --
--            global com um nome inv�lido                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (19/21)--
-- Tenta criar arquivos globais com nomes inv�lidos --
function tc.test_open19( )

    local error_code = 0
    local error_message = ""
    local filename = "/_open19.txt"

    local handle, error_message, error_code = io.open( filename, "rwg")

    assert_nil(ret, string.format( "test_open19. Retorno incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true)

    assert_equal( TST_ERR_PATHERR, error_code, string.format( "test_open19. Codigo de erro incorreto tentando criar arquivo global com nome invalido (%s)", filename ), true )

    assert_equal( filename .. ": " .. lua_error_message[ TST_ERR_PATHERR ], error_message, string.format( "test_open19. Mensagem de erro incorreta tentando criar arquivo global com nome invalido (%s)", filename ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_open20                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.open, tenta abrir um arquivo            --
--            global sem extensao                                                                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.open (20/21)--
-- Tenta criar um arquivo global v�lido sem extensao --
function tc.test_open20( )

    local filename = "open20"
    local handle = io.open( filename, "rwg")

    assert_not_nil( handle, "Erro tentando criar um arquivo global sem extensao", true)

    handle:close( filename )

    os.remove( filename, true )
end

-- ****************** TESTE DE IO.CLOSE ****************** --

-- Testes de IO.CLOSE (6 testes)--
tc2 = TestCase("io.close")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_close1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.close, tenta fechar um arquivo local    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.close (1/6)--
-- Tenta fechar um arquivo --
function tc2.test_close1( )

    local filename = "/close1.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = io.close( handle )

    assert_true( result, "test_close1. Retorno incorreto no fechamento de arquivo local" )

    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_close2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.close, tenta fechar um arquivo global   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.close (2/6)--
-- Tenta fechar um arquivo global --
function tc2.test_close2()

     local filename = "close2.txt"

    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = io.close( handle )

    assert_true( result, "test_close2. Retorno incorreto no fechamento de arquivo global" )

    assert_true( os.remove( filename, true ), "test_close2. Arquivo global nao foi fechado" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_close4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.close, tenta fechar um arquivo local    --
--            dentro de um subdiretorio                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.close (4/6)--
-- Tenta fechar um arquivo em um subdiretorio--
function tc2.test_close4()

    local dirname = "/close4/"
    local filename = "close4.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: dir. %s nao encontrado", dirname ), true )

    local handle = io.open( ( dirname .. filename ), "rw" )

    local result = io.close( handle )

    os.remove( ( dirname .. filename ) )

    assert_true( result, "test_close4. Retorno incorreto no fechamento de arquivo em um subdiretorio" )

end

-- ****************** TESTE DE IO.INPUT ****************** --

-- Testes de IO.INPUT (3 testes)--
tc3 = TestCase("io.input")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_input1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.input, associa a entrada a um arquivo   --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.input (1/4)--
-- Teste de io.input em arquivos locais --
function tc3.test_input1()

    local filename = "/input1.txt"

    io.input( filename )

    local result = io.read( "*l" )
    if( not assert_equal( "teste linha 1", result, "test_input1. Erro no io.input em arquivos locais" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_input1. Erro no io.input em arquivos locais" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    if( assert_equal( "teste linha 3", result, "test_input1. Erro no io.input em arquivos locais" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    assert_equal( "teste linha 4", result, "test_input1. Erro no io.input em arquivos locais" )

    io.input( ):close( )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_input3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.input, associa a entrada a um arquivo   --
--            local em um subdiretorio e verifica se a opera��o foi realizada com sucesso        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.input (3/4)--
-- Teste de io.input em arquivos locais em um subdiretorio --
function tc3.test_input3( )

    local dirname = "/input3/"
    local filename = "input3.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s nao encontrado", ( dirname .. filename ) ), true )

    io.input( dirname .. filename )

    local result = io.read( "*l" )
    if( not assert_equal( "teste linha 1", result, "test_input3. Erro no io.input em arquivo em subdiretorio" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_input3. Erro no io.input em arquivo em subdiretorio" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    if( assert_equal( "teste linha 3", result, "test_input3. Erro no io.input em arquivo em subdiretorio" ) ) then
        io.input( ):close( )
        return
    end

    result = io.read( "*l" )
    assert_equal( "teste linha 4", result, "test_input3. Erro no io.input em arquivo em subdiretorio" )

    io.input( ):close( )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_input1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.input, associa a entrada a um arquivo   --
--            inexistente                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.input (4/4)--
function tc3.test_input4()

    local filename = "/ne.txt"

    local result, error_message = pcall( function() io.input( filename ) end )

    assert_false( result, "test_input4. Excessao nao foi gerada para nome de arquivo inexistente" )
end

-- ****************** TESTE DE IO.LINES ****************** --

-- Testes de IO.LINES (5 testes)--
tc4 = TestCase("io.lines")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo       --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (1/8)--
-- Teste de io.lines em arquivos locais sem uso do io.input --
function tc4.test_lines1()

    local i = 1
    local filename = "/lines1.txt"
    local line = ""
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    for line in io.lines( filename ) do
        assert_equal( string.format( "teste linha %i", i ), line, "test_lines1. Erro no io.lines", true )
        i = i + 1
    end

    assert_equal( 5, i, "test_lines1. Erro no io.lines" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo local --
--            de testes associado a entrada padr�o atrav�s do io.input e verifica se a opera��o  --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (2/8)--
-- Teste de io.lines em arquivos locais utilizando o io.input --
function tc4.test_lines2()

    local i = 1
    local filename = "/lines2.txt"
    local line = ""

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    io.input( filename )

    local i = 1

    for line in io.input():lines() do
        assert_equal( string.format( "teste linha %i", i ), line, "test_lines2. Erro no io.lines" )
        i = i + 1
    end

    assert_equal( 5, i, "test_lines2. Erro no io.lines" )

    io.input():close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo       --
--            global de testes associado a entrada padr�o atrav�s do io.input e verifica se a    --
--            opera��o foi realizada com sucesso                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (4/8)--
-- Teste de io.lines em arquivos globais utilizando o io.input --
function tc4.test_lines4()

    local filename = "lines4.txt"

    local handle = io.open( filename,"rwg" )

    assert_not_nil( handle, string.format( "Dependencia: Nao foi possivel criar %s", filename ), true )

    handle:write( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n" )
    handle: close()

    handle = io.open( filename,"rg" )
    io.input( handle )

    local i = 1

    for line in io.input():lines() do
        if( not assert_equal( string.format( "teste linha %i", i), line, ".test_lines4. Erro no io.lines" ) ) then
            io.input():close()
            os.remove( filename, true )
            return
        end
        i = i + 1
    end

    assert_equal( 5, i, ".test_lines4. Erro no io.lines" )

    io.input():close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo       --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (5/8)--
-- Teste de io.lines em arquivo em um subdiretorio --
function tc4.test_lines5()

    local dirname = "/lines5/"
    local filename = "lines5.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: dir. %s nao encontrado", ( dirname .. filename ) ), true )

    local i = 1

    for line in io.lines( dirname .. filename ) do
        assert_equal( string.format( "teste linha %i", i ), line, "test_lines5. Erro no io.lines", true )
        i = i + 1
    end

    assert_equal( 5, i, "test_lines5. Erro no io.lines" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines6                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo       --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (6/8)--
-- Teste de io.lines em arquivos locais com quebra de linha CR NL (\r\n) --
function tc4.test_lines6()

    local i = 1
    local filename = "/lines6.txt"
    local line = ""

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    for line in io.lines( filename ) do
        assert_equal( string.format( "teste linha %i", i ), line, "test_lines6. Erro no io.lines lendo arquivo com quebra de linha CR NL", true )
        i = i + 1
    end

    assert_equal( 5, i, "test_lines6. Erro no io.lines lendo arquivo com quebra de linha CR NL" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines7                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, varre as linhas do arquivo       --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (7/8)--
-- Teste de io.lines em arquivos locais sem quebra de linha no fim do arquivo --
function tc4.test_lines7()

    local i = 1
    local filename = "/lines7.txt"
    local line = ""

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    for line in io.lines( filename ) do
        assert_equal( string.format( "teste linha %i", i ), line, "test_lines7. Erro no io.lines lendo arquivo sem quebra de linha no fim do arquivo", true )
        i = i + 1
    end

    assert_equal( 5, i, "test_lines7. Erro no io.lines lendo arquivo sem quebra de linha no fim do arquivo" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_lines8                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.lines, passa um arquivo invalido        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.lines (8/8)--
function tc4.test_lines8()

    local i = 1
    local filename = "/ne.txt"

    local result = pcall( function() io.lines( filename ) end )

    assert_false( result, "test_lines8. Erro no io.lines passando um nome de arquivo invalido" )
end

-- ****************** TESTE DE IO.OUTPUT ****************** --

-- Testes de IO.OUTPUT (4 testes)--
tc5 = TestCase("io.output")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_output1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.output, associa a sa�da a um arquivo    --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.output (1/4)--
-- Teste de io.output em arquivos locais --
function tc5.test_output1()

    local filename = "/output1.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: Nao pode criar %s", filename ), true )

    io.output( handle )

    io.write( "teste de io.output\n" )
    io.write( "serah q funfa?\n" )
    io.write( "vamos saber agora\n" )

    io.close()

    local handle2 = io.open( filename )

    assert_not_nil( handle2, string.format( "Dependencia: Nao pode abrir %s", filename ), true )

    local aux = handle2:read( "*l" )
    if( not assert_equal( "teste de io.output", aux, "test_output1. Erro no io.output com arq. local" ) ) then
        io.close( handle2 )
        os.remove( filename )
        return
    end

    aux = handle2:read( "*l" )
    if( not assert_equal("serah q funfa?", aux, "test_output1. Erro no io.output com arq. local") ) then
        io.close( handle2 )
        os.remove( filename )
        return
    end

    aux = handle2:read( "*l" )
    assert_equal("vamos saber agora", aux, "test_output1. Erro no io.output com arq. local")

    io.close( handle2 )
    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_output3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.output, associa a sa�da a um arquivo    --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.output (3/4)--
-- Teste de io.output em arquivo em um subdiretorio --
function tc5.test_output3()

    local dirname = "/output3/"
    local filename = "output3.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: Dir. %s nao encontrado", dirname ), true )

    local handle = io.open( ( dirname .. filename ), "rw" )

    assert_not_nil( handle, string.format( "Dependencia: Nao pode criar %s", filename ), true )

    io.output( handle )

    io.write( "teste de io.output\n" )
    io.write( "serah q funfa?\n" )
    io.write( "vamos saber agora\n" )

    io.close()

    local handle2 = io.open( ( dirname .. filename ) )

    assert_not_nil( handle2, string.format( "Dependencia: Nao pode abrir %s", filename ), true )

    local aux = handle2:read( "*l" )
    if( not assert_equal( "teste de io.output", aux, "test_output3. Erro no io.output com arq. global" ) ) then
        io.close( handle2 )
        os.remove( filename, true )
        return
    end

    aux = handle2:read( "*l" )
    if( not assert_equal("serah q funfa?", aux, "test_output3. Erro no io.output com arq. global") ) then
        io.close( handle2 )
        os.remove( filename, true )
        return
    end

    aux = handle2:read( "*l" )
    assert_equal("vamos saber agora", aux, "test_output3. Erro no io.output com arq. global")

    io.close( handle2 )
    os.remove( ( dirname .. filename ) )
end

-- ****************** TESTE DE IO.READ ****************** --

-- Testes de IO.READ (7 testes)--
tc6 = TestCase("io.read")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_read1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.read, ler dados de um arquivo           --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.read (1/8)--
-- Tenta ler em um arquivo local --
-- O io.read l� da entrada padr�o q no caso � o teclado, por�m como os testes do sistema de arquivos s�o autom�ticos  --
-- O io.read ser� testado em conjunto com o io.input, e o teste com o teclado ser� feito na suite do teclado (OH!!!)  --
function tc6.test_read1()

    local filename = "/read1.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    io.input( filename )

    local aux = io.read( "*l" )
    if( not assert_equal( "teste linha 1", aux, "test_read1. Erro na leitura do arquivo local" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    if( not assert_equal( "teste linha 2", aux, "test_read1. Erro na leitura do arquivo local" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    if( not assert_equal( "teste linha 3", aux, "test_read1. Erro na leitura do arquivo local" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    assert_equal( "teste linha 4", aux, "test_read1. Erro na leitura do arquivo local" )

    io.input():close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_read3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.read, ler dados de um arquivo           --
--            local de testes utilizando os modos "*n" e "number" e verifica se a opera��o       --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.read (3/8)--
-- Tenta ler em um arquivo local usando os modos '*n' e 'number' --
function tc6.test_read3()

    local filename = "/read3.txt"

    assert_true( os.exists( filename ) , string.format( "Dependencia: %s nao encontrado", filename ), true )

    io.input( filename )

    local aux = io.read( 12 )

    if( not assert_equal( "teste linha ", aux, "test_read3. Erro lendo linha 1 passando number " ) ) then
        io.input():close()
        return
    end

    aux = io.read("*n")
    io.read(1) -- \n
    if( not assert_equal( 1, aux, "test_read3. Erro lendo linha 1 passando *n" ) ) then
        io.input():close()
        return
    end

    local aux = io.read(12)
    if( not assert_equal( "teste linha ", aux, "test_read3. Erro lendo linha 2 passando number " ) ) then
        io.input():close()
        return
    end

    aux = io.read("*n")
    io.read(1) -- \n
    if( not assert_equal( 2, aux, "test_read3. Erro lendo linha 2 passando *n" ) ) then
        io.input():close()
        return
    end

    local aux = io.read(12)
    if( not assert_equal( "teste linha ", aux, "test_read3. Erro lendo linha 3 passando number " ) ) then
        io.input():close()
        return
    end

    aux = io.read("*n")
    io.read(1) -- \n
    if( not assert_equal( 3, aux, "test_read3. Erro lendo linha 3 passando *n" ) ) then
        io.input():close()
        return
    end

    local aux = io.read(12)
    if( not assert_equal( "teste linha ", aux, "test_read3. Erro lendo linha 4 passando number " ) ) then
        io.input():close()
        return
    end

    aux = io.read("*n")
    io.read(1) -- \n
    if( not assert_equal( 4, aux, "test_read3. Erro lendo linha 4 passando *n" ) ) then
        io.input():close()
        return
    end

    io.input():close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_read5                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.read, ler dados de um arquivo           --
--            local de testes utilizando o modo "*a" e verifica se a opera��o                    --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.read (5/8)--
-- Tenta ler em um arquivo local usando o modo '*a' --
function tc6.test_read5()

    local filename = "/read5.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    io.input( filename )

    local aux = io.read( "*a" )

    assert_equal( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n", aux, "test_read5. Retorno incorreto passando '*a'" )

    io.input():close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_read7                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.read, ler dados de um arquivo           --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.read (7/8)--
-- Tenta ler em um arquivo local dentro de um subdiretorio--
function tc6.test_read7()

    local dirname = "/read7/"
    local filename = "read7.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s nao encontrado", ( dirname .. filename ) ), true )

    local handle = io.open(( dirname .. filename ),"r")

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", ( dirname .. filename ) ), true )

    io.input( handle )

    local aux = io.read( "*l" )
    if( not assert_equal("teste linha 1", aux, "test_read7. Erro na leitura do arquivo em subdiretorio" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    if( not assert_equal("teste linha 2", aux, "test_read7. Erro na leitura do arquivo em subdiretorio" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    if( not assert_equal("teste linha 3", aux, "test_read7. Erro na leitura do arquivo em subdiretorio" ) ) then
        io.input():close()
        return
    end

    aux = io.read( "*l" )
    if( not assert_equal("teste linha 4", aux, "test_read7. Erro na leitura do arquivo em subdiretorio" ) ) then
        io.input():close()
        return
    end

    io.input():close()
end

-- ****************** TESTE DE IO.WRITE ****************** --

-- Testes de IO.WRITE (4 testes)--
tc7 = TestCase("io.write")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_write1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, escreve dados em um arquivo      --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.write (1/4)--
-- Tenta ecrever em um arquivo local --
-- O io.write escreve na sa�da padr�o q no caso � o display, por�m como os testes do sistema de arquivos s�o autom�ticos  --
-- O io.write ser� testado em conjunto com o io.output, e o teste com o display ser� feito na suite do display (De Novo!!!)  --
function tc7.test_write1()

    local filename = "/write1.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    io.output( handle )

    if( not assert_true( io.write( "teste de io.write\n" ), "test_write1. Retorno incorreto escrevendo em um arquivo local" ) ) then
        io.close( handle )
        os.remove( filename )
        return
    end

    if( not assert_true( io.write( "serah q funfa?\n" ), "test_write1. Retorno incorreto escrevendo em um arquivo local" ) ) then
        io.close( handle )
        os.remove( filename )
        return
    end

    if( not assert_true( io.write( "vamos saber agora\n" ), "test_write1. Retorno incorreto escrevendo em um arquivo local" ) ) then
        io.close( handle )
        os.remove( filename )
        return
    end

    io.close()

    local handle2 = io.open( filename )

    local aux = handle2:read( "*l" )
    if( not assert_equal( "teste de io.write", aux, "test_write1. Texto nao foi escrito em um arquivo local" ) ) then
        io.close( handle2 )
        os.remove( filename )
        return
    end

    aux = handle2:read( "*l" )
    if( not assert_equal( "serah q funfa?", aux, "test_write1. Texto nao foi escrito em um arquivo local" ) ) then
        io.close( handle2 )
        os.remove( filename )
        return
    end

    aux = handle2:read( "*l" )
    if( not assert_equal( "vamos saber agora", aux, "test_write1. Texto nao foi escrito em um arquivo local" ) ) then
        io.close( handle2 )
        os.remove( filename )
        return
    end

    handle2:close()
    os.remove( filename )


end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_write3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, escreve dados em um arquivo      --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.write (3/4)--
function tc7.test_write3()

    local dirname  = "/write3/"
    local filename = "write3.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: dir. %s nao encontrado", dirname ), true )

    local handle = io.open( ( dirname .. filename ), "rw" )
    
    assert_not_nil( handle, string.format( "%s nao pode ser aberto", filename ), true )

    io.output( handle )

    if( not assert_true( io.write( "teste de io.write\n" ), "test_write3. Retorno incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( handle )
        os.remove( dirname .. filename )
        return
    end

    if( not assert_true( io.write( "serah q funfa?\n" ), "test_write3. Retorno incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( handle )
        os.remove( dirname .. filename )
        return
    end

    if( not assert_true( io.write( "vamos saber agora\n" ), "test_write3. Retorno incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( handle )
        os.remove( dirname .. filename )
        return
    end

    io.close()

    local temp = io.open( dirname .. filename )
    
    assert_not_nil( temp, string.format( "%s nao pode ser aberto", filename ), true )

    local aux = temp:read( "*l" )
    if( not assert_equal( "teste de io.write", aux, "test_write3. Texto incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( temp )
        os.remove( dirname .. filename )
        return
    end

    aux = temp:read( "*l" )
    if( not assert_equal( "serah q funfa?", aux, "test_write3. Texto incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( temp )
        os.remove( dirname .. filename )
        return
    end

    aux = temp:read("*l")
    if( not assert_equal( "vamos saber agora", aux, "test_write3. Texto incorreto escrevendo em arquivo em subdir." ) ) then
        io.close( temp )
        os.remove( dirname .. filename )
        return
    end

    io.close( temp )
    os.remove( dirname .. filename )
end

-- ****************** TESTE DE IO.TYPE ****************** --

-- Testes de IO.TYPE (7 testes)--
tc8 = TestCase("io.type")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            passando como par�metro um handle inv�lido                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (1/7)--
-- io.type passando um handle inv�lido --
function tc8.test_type1()
    local ret = io.type( 123456 )

    assert_nil(ret, "test_type1. Retorno incorreto no io.type passando um handle invalido")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            passando como par�metro um handle de um arquivo aberto                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (2/7)--
-- io.type passando um handle de arquivo aberto --
function tc8.test_type2()

    local filename = "/type2.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "r" )

    assert_not_nil( handle, string.format( "%s nao pode ser aberto", filename ), true )

    local ret = io.type( handle )

    assert_equal("file", ret, "test_type2. Retorno incorreto no io.type passando um handle de arquivo aberto")

    io.close( handle )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            passando como par�metro um handle de um arquivo fechado                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (3/7)--
-- io.type passando um handle de arquivo fechado --
function tc8.test_type3()

    local filename = "/type3.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "r" )

    assert_not_nil( handle, string.format( "%s nao pode ser aberto", filename ), true )

    handle:close()

    local ret = io.type( handle )

    assert_equal("closed file", ret, "test_type3. Retorno incorreto no io.type passando um handle de arquivo aberto")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            passando como par�metro um handle de um arquivo aberto dentro de um subdiretorio   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (4/7)--
-- io.type passando um handle de arquivo aberto em um subdiretorio --
function tc8.test_type4()

    local dirname = "/type4/"
    local filename = "type4.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s nao encontrado", ( dirname .. filename ) ), true )

    local aux = io.open( (dirname .. filename ), "rw" )
    
    assert_not_nil( aux, string.format( "%s nao pode ser aberto", ( dirname .. filename ) ), true )

    local ret = io.type( aux )

    assert_equal("file", ret, "test_type4. Retorno incorreto do io.type passando um handle de arquivo aberto em um subdiretorio")

    io.close( aux )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type5                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            global passando como par�metro um handle de um arquivo aberto                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (5/7)--
-- io.type passando um handle de arquivo global aberto --
function tc8.test_type5()

    local filename = "type5.txt"

    local aux = io.open( filename, "rwg" )

    assert_not_nil( aux, string.format( "%s nao pode ser aberto", filename ), true )

    local ret = io.type( aux )

    assert_equal( "file", ret, "test_type5. Retorno incorreto do io.type passando um handle de arquivo global aberto" )

    io.close( aux )
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_type6                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.type, verifica o tipo do arquivo        --
--            global passando como par�metro um handle de um arquivo global fechado              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.type (6/7)--
-- io.type passando um handle de arquivo global fechado --
function tc8.test_type6()
    local filename = "type6.txt"

    local aux = io.open( filename, "rwg" )

    assert_not_nil( aux, string.format( "%s nao pode ser aberto", filename ), true )

    io.close( aux )

    local ret = io.type( aux )

    assert_equal( "closed file", ret, "test_type6. Retorno incorreto do io.type passando um handle de arquivo global fechado" )

    os.remove( filename, true )
end

-- ****************** TESTE DE FILE:CLOSE() ****************** --

-- Testes de FILE:CLOSE() (4 testes)--
tc9 = TestCase("file:close")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileclose1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:close, tenta fechar um arquivo local  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:close() (1/4)--
-- Tenta fechar um arquivo local --
function tc9.test_fileclose1()

    local filename = "/fclose1.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "%s nao pode ser aberto", filename ), true )

    local result = handle:close()

    assert_true( os.remove( filename ), "test_fileclose1. O arquivo local nao foi fechado", true )

    assert_true( result, "test_fileclose1. Retorno incorreto no fechamento de arquivo local" )

    --recupera o arquivo
    handle = io.open( filename, "rw" )
    handle:close()

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileclose2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:close, tenta fechar um arquivo global --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:close() (2/4)--
-- Tenta fechar um arquivo global --
function tc9.test_fileclose2()

    local filename = "fclose2.txt"

    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "%s nao pode ser aberto", filename ), true )

    local result = handle:close()

    assert_true( os.remove( filename, true ), "test_fileclose2. O arquivo global nao foi fechado", true )

    assert_true( result, "test_fileclose2. Retorno incorreto no fechamento de arquivo global" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileclose3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:close, tenta fechar um arquivo local  --
--            em um subdiretorio                                                                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:close() (3/4)--
-- Tenta fechar um arquivo em um subdiretorio --
function tc9.test_fileclose3()

    local dirname = "/fclose3/"
    local filename = "fclose3.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: dir. %s nao encontrado", dirname ), true )

    local ret = io.open( ( dirname .. filename ), "rw" )
    
    assert_not_nil( ret, string.format( "%s nao pode ser aberto", ( dirname .. filename ) ), true )

    local aux = ret:close()

    assert_true( os.remove( dirname .. filename ), "test_fileclose3. O arquivo em subdir. nao foi fechado", true )

    assert_true( aux, "test_fileclose3. Retorno incorreto no fechamento de arquivo em subdir." )
end


-- ****************** TESTE DE FILE:LINES() ****************** --

-- Testes de FILE:LINES (4 testes)--
tc10 = TestCase("file:lines")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filelines1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:lines, varre as linhas do arquivo     --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:lines (1/4)--
-- Teste de file:lines em arquivo local --
function tc10.test_filelines1()

    local filename = "/flines1.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )
    local ret = io.open( filename )

    local i = 1

    for line in ret:lines() do
        if( not assert_equal( string.format( "teste linha %i", i ), line, "test_filelines1. Erro no io.lines" ) ) then
            ret:close()
            return
        end
        i = i + 1
    end

    assert_equal( 5, i, "test_filelines1. Erro no file:lines para arquivos locais" )

    ret:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filelines2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:lines, varre as linhas do arquivo     --
--            global de testes e verifica se a opera��o foi realizada com sucesso                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:lines (2/4)--
-- Teste de file:lines em arquivo global --
function tc10.test_filelines2()
    local filename = "flines2.txt"

    local ret = io.open( filename, "rwg" )

    assert_not_nil( ret, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    ret:write( "teste linha 1\n" )
    ret:write( "teste linha 2\n" )
    ret:write( "teste linha 3\n" )
    ret:write( "teste linha 4\n" )

    ret:close()

    ret = io.open( filename, "rg" )

    local i = 1

    for line in ret:lines() do
        if( not assert_equal( string.format( "teste linha %i", i ) , line, string.format( "test_filelines2. Erro no io.lines lendo linha %i", i ) ) ) then
            ret:close()
            os.remove( filename, true )
            return
        end
        i = i + 1
    end

    assert_equal( 5, i, "test_filelines2. Erro no file:lines para arquivos locais" )

    ret:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filelines3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:lines, varre as linhas do arquivo     --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:lines (3/4)--
-- Teste de file:lines em arquivo em um subdiretorio --
function tc10.test_filelines3()

    local dirname = "/flines3/"
    local filename = "flines3.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s nao encontrado", ( dirname .. filename) ), true )
    local ret = io.open( dirname .. filename )

    local i = 1

    for line in ret:lines() do
        if( not assert_equal( string.format( "teste linha %i", i ), line, "test_filelines3. Erro no io.lines" ) ) then
            ret:close()
            return
        end
        i = i + 1
    end

    assert_equal( 5, i, "test_filelines3. Erro no file:lines para arquivos locais" )

    ret:close()
end


-- ****************** TESTE DE FILE:READ() ****************** --

-- Testes de FILE:READ (12 testes)--
tc11 = TestCase("file:read")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (1/12)--
-- Tenta ler em um arquivo local --
function tc11.test_fileread1()

    local filename = "/fread1.txt"
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )
    
    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( "*l" )
    if( not assert_equal( "teste linha 1", result, "test_fileread1. Erro lendo linha 1 de arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_fileread1. Erro lendo linha 2 de arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 3", result, "test_fileread1. Erro lendo linha 3 de arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 4", result, "test_fileread1. Erro lendo linha 4 de arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_nil( result, "test_fileread1. Erro lendo final de arquivo local" ) ) then
        handle:close()
        return
    end

    handle:close()
end


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            global de testes e verifica se a opera��o foi realizada com sucesso                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (2/12)--
-- Tenta ler em um arquivo global --
function tc11.test_fileread2()

    local filename = "fread2.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    --executa teste
    handle = io.open( filename, "rg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( "*l" )
    if( not assert_equal( "teste linha 1", result, "test_fileread2. Erro lendo linha 1 de arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_fileread2. Erro lendo linha 2 de arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 3", result, "test_fileread2. Erro lendo linha 3 de arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 4", result, "test_fileread2. Erro lendo linha 4 de arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*l" )
    if( not assert_nil( result, "test_fileread2. Erro lendo final de arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            local de testes utilizando os modos "*n" e "number" e verifica se a opera��o       --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (3/12)--
-- Tenta ler em um arquivo local usando os modos '*n' e 'number' --
function tc11.test_fileread3()

    local filename = "/fread3.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 1, result, "test_fileread3. Erro na leitura de numero na linha 1 do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 2, result, "test_fileread3. Erro na leitura de numero na linha 2 do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 3, result, "test_fileread3. Erro na leitura de numero na linha 3 do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 4, result, "test_fileread3. Erro na leitura de numero na linha 4 do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:read( 1 ) -- \n

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread4                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            global de testes utilizando os modos "*n" e "number" e verifica se a opera��o      --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (4/12)--
-- Tenta ler em um arquivo global usando os modos '*n' e 'number' --
function tc11.test_fileread4()

    local filename = "fread4.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg" )

    if( not assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ) ) ) then
        os.remove( filename, true )
        return
    end

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 1, result, "test_fileread3. Erro na leitura de numero na linha 1 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 2, result, "test_fileread3. Erro na leitura de numero na linha 2 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 3, result, "test_fileread3. Erro na leitura de numero na linha 3 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:read( 1 ) -- \n

    local result = handle:read( 12 )
    if( not assert_equal( "teste linha ", result, "test_fileread3. Erro leitura da linha 1 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    result = handle:read( "*n" )
    if( not assert_equal( 4, result, "test_fileread3. Erro na leitura de numero na linha 4 do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:read( 1 ) -- \n

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread5                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            local de testes utilizando o modo "*a" e verifica se a opera��o                    --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (5/12)--
-- Tenta ler em um arquivo local usando o modo '*a' --
function tc11.test_fileread5()


    local filename = "/fread5.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( "*a" )
    assert_equal( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "test_fileread5. Erro na leitura do arquivo local" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread6                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            global de testes utilizando o modo "*a" e verifica se a opera��o                   --
--            foi realizada com sucesso                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (6/12)--
-- Tenta ler em um arquivo global usando o modo '*a' --
function tc11.test_fileread6()

    local filename = "fread6.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    local handle2 = io.open( filename, "rg" )

    if( not assert_not_nil( handle2, string.format( "Dependencia: %s nao pode ser aberto", filename ) ) ) then
        os.remove( filename, true )
        return
    end

    local result = handle2:read( "*a" )
    assert_equal( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "Erro na leitura do arquivo global" )

    handle2:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread8                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            de testes passando um valor numerico maior que o tamanho do arquivo                --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (8/12)--
-- Tenta ler mais do que tem em um arquivo local --
function tc11.test_fileread8()

    local filename = "/fread8.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "r" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( 100 )
    assert_equal( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "test_fileread8. Passando valor numerico maior que o tamanho do arquivo. Retorno incorreto" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread9                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (9/12)--
-- Tenta ler em um arquivo em um subdiretorio --
function tc11.test_fileread9()

    local dirname = "/fread9/"
    local filename = "fread9.txt"

    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s%s nao encontrado", dirname, filename ), true )

    local handle = io.open( ( dirname .. filename ) )
    
    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( "*l" )
    if( assert_equal( "teste linha 1", result, "test_fileread9. Erro na leitura da linha 1 do arquivo em um subdiretorio" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( assert_equal( "teste linha 2", result, "test_fileread9. Erro na leitura da linha 2 do arquivo em um subdiretorio" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( assert_equal( "teste linha 3", result, "test_fileread9. Erro na leitura da linha 3 do arquivo em um subdiretorio" ) ) then
        handle:close()
        return
    end
    
    result = handle:read( "*l" )
    if( assert_equal( "teste linha 4", result, "test_fileread9. Erro na leitura da linha 4 do arquivo em um subdiretorio" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( assert_nil( result, "test_fileread9. Erro na leitura do fim do arquivo em um subdiretorio" ) ) then
        handle:close()
        return
    end

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread11                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            global de testes passando um valor numerico maior que o tamanho do arquivo         --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (11/12)--
-- Tenta ler mais do que tem em um arquivo global --
function tc11.test_fileread11()

    local filename = "fread11.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg")

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( 100 )
    assert_equal( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "test_fileread11. Erro passando um valor numerico maior que o tamanho do arquivo. Retorno incorreto" )

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileread12                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:read, ler dados de um arquivo         --
--            local de testes com quebra de linha CR+NL. e verifica se a opera��o foi            --
--            realizada com sucesso                                                              --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:read (12/12)--
-- Tenta ler em um arquivo local --
function tc11.test_fileread12()

    local filename = "/fread12.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read( "*l" )
    if( not assert_equal( "teste linha 1", result, "test_fileread1. Erro lendo linha 1 de arquivo com CR+NL" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_fileread1. Erro lendo linha 2 de arquivo com CR+NL" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 3", result, "test_fileread1. Erro lendo linha 3 de arquivo com CR+NL" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 4", result, "test_fileread1. Erro lendo linha 4 de arquivo com CR+NL" ) ) then
        handle:close()
        return
    end

    result = handle:read( "*l" )
    if( not assert_nil( result, "test_fileread1. Erro lendo final de arquivo com CR+NL" ) ) then
        handle:close()
        return
    end

    handle:close()
end

-- ****************** TESTE DE FILE:SEEK() ****************** --

-- Testes de FILE:SEEK (12 testes)--
tc12 = TestCase("file:seek")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no fim do arquivo     --
--            local e verifica se a opera��o foi realizada com sucesso                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (1/13)--
-- Posicionando no fim do arquivo local--
function tc12.test_fileseek1()

    local filename = "/fseek1.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )
    
    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:seek( "end" )

    local result = handle:read( "*l" )
    assert_nil( result, "test_fileseek1. Erro tentando posicionar no fim do arquivo local" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no fim do arquivo     --
--            global e verifica se a opera��o foi realizada com sucesso                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (2/13)--
-- Posicionando no fim do arquivo global--
function tc12.test_fileseek2()

    local filename = "fseek2.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg" )

    handle:seek( "end" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle:read("*l")
    assert_nil( result, "test_fileseek2. Erro tentando posicionar no fim do arquivo global" )

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no in�cio do arquivo  --
--            local e verifica se a opera��o foi realizada com sucesso                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (3/13)--
-- Posicionando no inicio do arquivo local--
function tc12.test_fileseek3()

    local filename = "/fseek3.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:read( "*a" )

    if( not assert_nil( handle:read( "*l" ), "Dependencia: read nao funcionou", filename ) ) then
        handle:close()
        return
    end

    handle:seek( "set" )

    local result = handle:read( "*l" )

    assert_equal( "teste linha 1", result, "test_fileseek3. Erro tentando posicionar no inicio do arquivo local" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek4                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no in�cio do arquivo  --
--            global e verifica se a opera��o foi realizada com sucesso                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (4/13)--
-- Posicionando no inicio do arquivo global--
function tc12.test_fileseek4()

    local filename = "fseek4.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg" )
    
    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:read( "*a" )

    handle:seek( "set" )

    local result = handle:read( "*l" )
    assert_equal( "teste linha 1", result, "test_fileseek4. Erro tentando posicionar no inicio do arquivo global" )

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek5                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no meio do arquivo    --
--            local a partir do uso do "cur" e verifica se a opera��o foi realizada com sucesso  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (5/13)--
-- Posicionando no meio do arquivo local a partir do uso do 'cur' --
function tc12.test_fileseek5()

    local filename = "/fseek5.txt"
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:seek( "cur", 14 )
    local result = handle:read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_fileseek5. Erro tentando posicionar no meio do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:seek( "cur", 14 )
    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 4", result, "test_fileseek5. Erro tentando posicionar no meio do arquivo local" ) ) then
        handle:close()
        return
    end

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek6                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no meio do arquivo    --
--            global a partir do uso do "cur" e verifica se a opera��o foi realizada com sucesso --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (6/13)--
-- Posicionando no meio do arquivo global a partir do uso do 'cur' --
function tc12.test_fileseek6()
    local filename = "fseek6.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:seek( "cur", 14 )
    local result = handle:read( "*l" )
    if( not assert_equal( "teste linha 2", result, "test_fileseek5. Erro tentando posicionar no meio do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:seek( "cur", 14 )
    result = handle:read( "*l" )
    if( not assert_equal( "teste linha 4", result, "test_fileseek5. Erro tentando posicionar no meio do arquivo local" ) ) then
        handle:close()
        os.remove( filename, true )
        return
    end

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek8                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no fim do arquivo     --
--            a partir do uso do "end" passando um offset negativo e verifica se a opera��o      --
--            foi realizada com sucesso                                                          --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (8/13)--
-- Posicionando no fim do arquivo com um offset negativo --
function tc12.test_fileseek8()

    local filename = "/fseek8.txt"
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "r" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:seek( "end", -14 )
    local result = handle:read( "*l" )
    assert_equal( "teste linha 4", result, "test_fileseek8. Erro tentando passar um offset negativo" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek9                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no fim do arquivo     --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (9/13)--
-- Posicionando no fim de arquivo em um subdiretorio--
function tc12.test_fileseek9()

    local dirname = "/fseek9/"
    local filename = "fseek9.txt"
    
    assert_true( os.exists( dirname .. filename ), string.format( "Dependencia: %s%s nao encontrado", dirname, filename ), true )

    local handle = io.open( ( dirname .. filename ) )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:seek( "end" )

    local result = handle:read( "*l" )

    assert_nil( result, "test_fileseek9. Erro tentando posicionar no fim do arquivo em um subdiretorio" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek11                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:seek, posiciona no fim do arquivo     --
--            global a partir do uso do "end" passando um offset negativo e verifica se a        --
--            opera��o foi realizada com sucesso                                                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (11/13)--
-- Posicionando no fim do arquivo global com um offset negativo --
function tc12.test_fileseek11()

    local filename = "fseek11.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    handle = io.open( filename, "rg" )

    handle:seek( "end", -14 )
    local result = handle:read( "*l" )
    assert_equal( "teste linha 4", result, "test_fileseek11. Erro tentando passar um offset negativo em um arquivo global" )

    handle:close()
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek12                                                                    --
--                                                                                               --
--  Descricao: Utilizacao de todos os flags do seek                                              --
--                                                                                               --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (12/13)--
-- Verificando o retorno de fseek--
function tc12.test_fileseek12()

    local filename = "/fseek12.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "r" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    --posiciona no inicio da terceira linha
    local result = handle:seek( "cur", 28 )
    if( not assert_equal( 28, result, "test_fileseek12. Retorno incorreto passando 'cur' e offset positivo" ) ) then
        handle:close()
        return
    end

    --posiciona no inicio da segunda linha
    result = handle:seek( "cur", -14 )
    if( not assert_equal( 14, result, "test_fileseek12. Retorno incorreto passando 'cur' e offset negativo" ) ) then
        handle:close()
        return
    end

    --posiciona no fim do arquivo
    result = handle:seek( "end" )
    if( not assert_equal( 56, result, "test_fileseek12. Retorno incorreto passando 'end'" ) ) then
        handle:close()
        return
    end

    --da erro
    result = handle:seek( "end", 14 )
    if( not assert_nil( result, "test_fileseek12. Retorno incorreto passando 'end' e offset positivo" ) ) then
        handle:close()
        return
    end

    --posiciona no inicio da quarta e ultima linha
    result = handle:seek( "end", -14 )
    if( not assert_equal( 42, result, "test_fileseek12. Retorno incorreto passando 'end' e offset negativo" ) ) then
        handle:close()
        return
    end

    --posiciona no inicio do arquivo
    result = handle:seek( "set" )
    if( not assert_equal( 0, result, "test_fileseek12. Retorno incorreto passando 'set'" ) ) then
        handle:close()
        return
    end

    --da erro
    result = handle:seek( "set", -14 )
    if( not assert_nil( result, "test_fileseek12. Retorno incorreto passando 'set' e numero negativo" ) ) then
        handle:close()
        return
    end

    --posiciona no inicio da segunda linha
    result = handle:seek( "set", 14 )
    if( not assert_equal( 14, result, "test_fileseek12. Retorno incorreto passando 'set' e numero positivo" ) ) then
        handle:close()
        return
    end

    --deixa o cursor onde esta
    result = handle:seek( "cur" )
    assert_equal( 14, result, "test_fileseek12. Retorno incorreto passando 'cur'" )

    handle:close()
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_fileseek13                                                                    --
--                                                                                               --
--  Descricao: Utilizacao do seek em conjunto com o write                                        --
--                                                                                               --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:seek (13/13)--
-- Verificando o retorno de fseek--
function tc12.test_fileseek13()

    local filename = "/fseek13.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "1234567890" )

    local result = handle:seek( "set" )

    if( not assert_equal( 0, result, "test_fileseek13. Usando o seek com 'set'. Retorno incorreto" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    handle:write( "0123456789" )

    result = handle:seek( "set" )

    if( not assert_equal( 0, result, "test_fileseek13. Usando o seek com 'set'. Retorno incorreto" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    if( not assert_equal( "0123456789", handle:read( "*a" ), "test_fileseek13. Usando o seek com 'set'. O ponteiro nao esta na posicao correta" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    result, m, c = handle:seek( "cur", -5 )

    if( not assert_equal( 5, result, "test_fileseek13. Usando o seek com 'cur'. Retorno incorreto" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    handle:write( "55555" )

    result = handle:seek( "set" )

    if( not assert_equal( 0, result, "test_fileseek13. Usando o seek com 'set'. Retorno incorreto" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    if( not assert_equal( "0123455555", handle:read( "*a" ), "test_fileseek13. Usando o seek com 'cur'. O ponteiro nao esta na posicao correta" ) ) then
        handle:close()
        os.remove( filename )
        return
    end

    handle:close()
    os.remove( filename )
end

-- ****************** TESTE DE FILE:WRITE() ****************** --

-- Testes de FILE:WRITE (8 testes)--
tc13 = TestCase("file:write")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            local de testes e verifica se a opera��o foi realizada com sucesso                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (1/8)--
-- Tenta ecrever em um arquivo local --
function tc13.test_filewrite1()

    local filename = "/fwrite1.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste de file:write\n" )
    handle:write( "serah q funfa?\n" )
    handle:write( "vamos saber agora\n" )

    handle:close()

    local temp = io.open( filename )
    
    assert_not_nil( temp, string.format( "%s nao pode ser aberto", filename ), true )

    local result = temp:read( "*l" )
    assert_equal("teste de file:write", result, "test_filewrite1. Erro na escrita em arquivo local utilizando o file:write")

    result = temp:read( "*l" )
    assert_equal("serah q funfa?", result, "test_filewrite1. Erro na escrita em arquivo local utilizando o file:write")

    result = temp:read( "*l" )
    assert_equal("vamos saber agora", result, "test_filewrite1. Erro na escrita em arquivo local utilizando o file:write")

    io.close( temp )
    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            global de testes e verifica se a opera��o foi realizada com sucesso                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (2/8)--
-- Tenta ecrever em um arquivo global --
function tc13.test_filewrite2()

    local filename = "fwrite2.txt"
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste de file:write\n" )
    handle:write( "serah q funfa?\n" )
    handle:write( "vamos saber agora\n" )

    handle:close()

    local temp = io.open( filename,"rg" )

    assert_not_nil( temp, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = temp:read( "*l" )
    assert_equal( "teste de file:write", result, "test_filewrite2. Erro na escrita em arquivo global utilizando o file:write")

    result = temp:read( "*l" )
    assert_equal( "serah q funfa?", result, "test_filewrite2. Erro na escrita em arquivo global utilizando o file:write")

    result = temp:read( "*l" )
    assert_equal( "vamos saber agora", result, "test_filewrite2. Erro na escrita em arquivo global utilizando o file:write")

    io.close( temp )
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            de testes nao vazio e verifica se a opera��o foi realizada com sucesso             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (3/8)--
-- Tenta ecrever em um arquivo nao vazio --
function tc13.test_filewrite3()

    local filename = "/fwrite3.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 0\n" )

    handle:close()

    local temp = io.open( filename,"r")

    assert_not_nil( temp, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = temp:read( "*a" )
    assert_equal("teste linha 0\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "Erro escrevendo em um arquivo nao vazio")

    io.close( temp )
    
    --recupera o arquivo a forma original
    os.remove( filename )
    local handle = io.open( filename, "rw" )
    handle:write( "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n" )
    handle:close()

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite4                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            de testes passando mais de um argumento e verifica se a opera��o foi sucesso       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (4/8)--
-- Tenta ecrever em um arquivo local--
function tc13.test_filewrite4()

    local filename = "/fwrite4.txt"

    local handle = io.open( filename, "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste de file:write\n","serah q funfa?\n","vamos saber agora\n" )

    handle:close()

    local temp = io.open( filename,"r" )

    assert_not_nil( temp, string.format( "%s nao pode ser aberto", filename ), true )

    local result = temp:read( "*l" )
    assert_equal("teste de file:write", result, "test_filewrite4. Erro na escrita em arquivo utilizando o file:write com mais de um argumento")

    result = temp:read("*l")
    assert_equal("serah q funfa?", result, "test_filewrite4. Erro na escrita em arquivo utilizando o file:write com mais de um argumento")

    result = temp:read("*l")
    assert_equal("vamos saber agora", result, "test_filewrite4. Erro na escrita em arquivo utilizando o file:write com mais de um argumento")

    io.close(temp)
    os.remove( filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite5                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            em um subdiretorio e verifica se a opera��o foi realizada com sucesso              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (5/8)--
-- Tenta ecrever em um arquivo em um subdiretorio --
function tc13.test_filewrite5()

    local dirname = "/fwrite5/"
    local filename = "fwrite5.txt"

    assert_true( os.exists( dirname ), string.format( "Dependencia: %s nao encontrado", dirname ), true )

    local handle = io.open( ( dirname .. filename ), "rw" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste de file:write\n" )
    handle:write( "serah q funfa?\n" )
    handle:write( "vamos saber agora\n" )

    handle:close()

    local temp = io.open( dirname .. filename )
    
    assert_not_nil( temp, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = temp:read( "*l" )
    assert_equal("teste de file:write", result, "test_filewrite5. Erro na escrita em arquivo em um subdiretorio utilizando o file:write")

    result = temp:read("*l")
    assert_equal("serah q funfa?", result, "test_filewrite5. Erro na escrita em arquivo em um subdiretorio utilizando o file:write")

    result = temp:read("*l")
    assert_equal("vamos saber agora", result, "test_filewrite5. Erro na escrita em arquivo em um subdiretorio utilizando o file:write")

    io.close( temp )
    os.remove( dirname .. filename )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite6                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            global de testes nao vazio e verifica se a opera��o foi realizada com sucesso      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (6/8)--
-- Tenta ecrever em um arquivo global nao vazio --
function tc13.test_filewrite6()

    local filename = "fwrite6.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste linha 1\n" )
    handle:write( "teste linha 2\n" )
    handle:write( "teste linha 3\n" )
    handle:write( "teste linha 4\n" )

    handle:close()

    local handle2 = io.open( filename, "rwg" )

    assert_not_nil( handle2, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    assert_true( handle2:write("teste linha 0\n"), string.format( "Dependencia: %s nao pode ser escrito", filename ), true )

    handle2:close()

    local handle3 = io.open( filename, "rg" )

    assert_not_nil( handle3, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    local result = handle3:read( "*a" )
    assert_equal( "teste linha 0\nteste linha 2\nteste linha 3\nteste linha 4\n", result, "test_filewrite6. Erro escrevendo em um arquivo nao vazio" )

    io.close( handle3 )
    os.remove( filename, true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filewrite7                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o file:write, escreve dados em um arquivo    --
--            global de testes passando mais de um argumento e verifica se a opera��o foi sucesso--
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de file:write (7/8)--
-- Tenta escrever em um arquivo global --
function tc13.test_filewrite7()
    local filename = "fwrite7.txt"

    --cria arquivo global de teste
    local handle = io.open( filename, "rwg" )

    assert_not_nil( handle, string.format( "Dependencia: %s nao pode ser aberto", filename ), true )

    handle:write( "teste de file:write\n" )
    handle:write( "serah q funfa?\n" )
    handle:write( "vamos saber agora\n" )

    handle:close()
    
    handle = io.open( filename, "rg" )

    handle:write("teste de file:write\n","serah q funfa?\n","vamos saber agora\n")

    handle:close()

    local temp = io.open( filename,"rg" )

    local result = temp:read( "*l" )
    if( not assert_equal( "teste de file:write", result, "Erro na escrita em arquivo utilizando o file:write com mais de um argumento" ) ) then
        temp:close()
        os.remove( filename, true )
        return
    end

    result = temp:read( "*l" )
    if( not assert_equal( "serah q funfa?", result, "Erro na escrita em arquivo global utilizando o file:write" ) ) then
        temp:close()
        os.remove( filename, true )
        return
    end

    result = temp:read( "*l" )
    if( assert_equal( "vamos saber agora", result, "Erro na escrita em arquivo global utilizando o file:write" ) ) then
        temp:close()
        os.remove( filename, true )
        return
    end

    io.close( temp )
    os.remove( filename, true )
end

tc14 = TestCase("delete")
function tc14.test_delete1()
    assert_true(aux_delete_testfiles(), "N�o foi poss�vel remover os arquivos de teste")
end

-------------------------------------------------------------------------------
--> Fun��o para criar n arquivos
-------------------------------------------------------------------------------
function aux_create_files(start, stop)
    local dir = "/files/"
    local handle
    local count = 0
    if not os.exists(dir) then
        os.mkdir(dir)
    end
    for i = start, stop do
        handle = io.open(dir .. "teste" .. i, "rw")
        if not handle then
            break --n�o conseguiu criar o arquivo
        end
        handle:close()
        count = i
    end
    if count == stop then
        return true --conseguiu criar os n arquivos
    else
        return false --n�o conseguiu criar os n arquivos
    end
end

-------------------------------------------------------------------------------
--> Fun��o para criar todos os arquivos de teste
-------------------------------------------------------------------------------
function aux_create_testfiles()
    local handle
    local count = 0
    local dirnames = {"/flines3/", "/fread9/", "/input3/", "/lines5/", "/read7/",
                    "/fseek9/", "/type4/",
                    "/open11/", "/close4/", "/output3/", "/write3/", "/fclose3/", "/fwrite5/"}
    local filenames = {"/flines3/flines3.txt", "/fread9/fread9.txt", "/input3/input3.txt", 
                    "/lines5/lines5.txt", "/read7/read7.txt", "flines1.txt", "fread1.txt",
                    "fread3.txt", "fread5.txt", "fread8.txt", "fread12.txt", "fseek1.txt",
                    "fseek3.txt", "fseek5.txt", "fseek8.txt", "fseek12.txt", "fwrite3.txt",
                    "input1.txt", "lines1.txt", "lines2.txt", "lines6.txt", "lines7.txt",
                    "read1.txt", "read3.txt", "read5.txt"}
    local emptyfiles = {"/fseek9/fseek9.txt", "/type4/type4.txt", "close3.txt",
                    "fclose1.txt", "open4.txt", "type2.txt", "type3.txt"}--,
                    --"open3.txt"}
    local total = table.getn(filenames) + table.getn(emptyfiles)
    local content = "teste linha 1\nteste linha 2\nteste linha 3\nteste linha 4\n"

    for i = 1, table.getn(dirnames) do
        if not os.exists(dirnames[i]) then
            os.mkdir(dirnames[i])
        end
    end

    for i = 1, table.getn(filenames) do
        handle = io.open(filenames[i], "rw")
        if not handle then
            break --n�o conseguiu criar o arquivo
        end
        handle:write(content)
        handle:close()
        count = count + 1
    end

    for i = 1, table.getn(emptyfiles) do
        handle = io.open(emptyfiles[i], "rw")
        if not handle then
            break --n�o conseguiu criar o arquivo
        end
        handle:close()
        count = count + 1
    end

    if count == total then
        return true --conseguiu criar os arquivos de teste
    else
        return false --n�o conseguiu criar os arquivos de teste
    end
end

-------------------------------------------------------------------------------
--> Fun��o para apagar todos os arquivos de teste
-------------------------------------------------------------------------------
function aux_delete_testfiles()
    local count = 0
    local dirnames = {"/flines3/", "/fread9/", "/input3/", "/lines5/", "/read7/",
                    "/fseek9/", "/type4/",
                    "/open11/", "/close4/", "/output3/", "/write3/", "/fclose3/", "/fwrite5/"}
    local filenames = {"/flines3/flines3.txt", "/fread9/fread9.txt", "/input3/input3.txt", 
                    "/lines5/lines5.txt", "/read7/read7.txt", "flines1.txt", "fread1.txt",
                    "fread3.txt", "fread5.txt", "fread8.txt", "fread12.txt", "fseek1.txt",
                    "fseek3.txt", "fseek5.txt", "fseek8.txt", "fseek12.txt", "fwrite3.txt",
                    "input1.txt", "lines1.txt", "lines2.txt", "lines6.txt", "lines7.txt",
                    "read1.txt", "read3.txt", "read5.txt", "fseek12.txt", "write1.txt",
                    "/write3/write3.txt"}
    local emptyfiles = {"/fseek9/fseek9.txt", "/type4/type4.txt", "close3.txt",
                    "fclose1.txt", "open4.txt", "type2.txt", "type3.txt",
                    "open3.txt"}

    for i = 1, table.getn(filenames) do
        os.remove(filenames[i])
    end

    for i = 1, table.getn(emptyfiles) do
        os.remove(emptyfiles[i])
    end

    for i = 1, table.getn(dirnames) do
        os.remove(dirnames[i])
    end
    
    return true
end

lunit.run()
