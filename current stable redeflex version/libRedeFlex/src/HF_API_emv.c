#include "HF_API.h"

int HEMV_iInit (void)
{
    return HF_SUCCESS;
}

void HEMV_Version (char *pszVersion)
{
}

int HEMV_iCheckCard (void)
{
    return HF_SUCCESS;
}

int HEMV_iStartTransaction (void)
{
    return HF_SUCCESS;
}

int HEMV_iDefData (unsigned int uiTag, int iLen, const unsigned char *pbValue)
{
    return HF_SUCCESS;
}

int HEMV_iGetData (unsigned int uiTag, int *piLen, unsigned char *pbValue)
{
    return HF_SUCCESS;
}

int HEMV_iDefApp (int iRef, int iAID_Len, const unsigned char *pbAID)
{
    return HF_SUCCESS;
}

int HEMV_iGetCandidateList (int *piItems, unsigned char *pbLabels)
{
    return HF_SUCCESS;
}

int HEMV_iSelectApp (int iAppIdx)
{
    return HF_SUCCESS;
}

int HEMV_iProcessTransaction (int *piResult, int *piCVMResult, void *pvHandle)
{
    return HF_SUCCESS;
}

int HEMV_iCompleteTransaction (int iOnlineResult, int *piResult)
{
    return HF_SUCCESS;
}
