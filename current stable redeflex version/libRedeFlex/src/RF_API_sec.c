#include "HF_API.h"

/**
 * Inserts a new encryption key in the safe area of the device.
 *
 * @param type
 *			Type of the key.
 * @param keyIdx
 * 			Index of the safe area where the key is stored.
 * @param p_key
 * 			Pointer to the byte array that contains the key to be stored.
 * @param key_sz
 * 			Number of bytes p_key.
 * @param p_ksn
 * 			Pointer to the byte array that contains the KSN (only for DUKPT keys).
 * 			Can be NULL ('\ 0') when the key is not the DUKPT type.
 * @param ksn_sz
 * 			Number of bytes p_ksn. If the key is not DUKPT type parameter is ignored.
 * @return
 * 			RF_SUCCESS � Key successfully stored;
 * 			RF_ERR_INVALIDARG � Invalid key type;
 * 			RF_ERROR_DEVICEFAULT � Error on key storing.
 *
 */
RF_INT32 RF_SEC_store_key(RF_SEC_KEY_TYPE type, RF_UINT16 keyIdx, const RF_UINT8 *p_key, RF_UINT32 key_sz, const RF_UINT8 *p_ksn, RF_UINT32 ksn_sz)
{
    return RF_SUCCESS;
}

