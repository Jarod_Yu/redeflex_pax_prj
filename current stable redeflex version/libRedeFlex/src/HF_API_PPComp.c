#include "HF_API.h"

struct RF_API_BC_callback_reg local_callbacks;

RF_INT16 RF_API_BC_register_callbacks(struct RF_API_BC_callback_reg callbacks)
{
    local_callbacks = callbacks;
    return PP_OK;
}

HF_INT32 HF_PP_SetDisplayCallback(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_PP_SetDisplayExCallback(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_PP_SetMenuCallback (HF_VOID)
{
    return HF_SUCCESS;
}

//29.06.2015
HF_INT32 HF_PP_Open(HF_CHAR * psCom)
{
	return PP_Open(psCom);
}

//29.06.2015
HF_INT32 HF_PP_Close(HF_CHAR * psIdleMsg)
{
    return PP_Close(psIdleMsg);
}

HF_INT32 HF_PP_Abort(void)
{
	return PP_Abort();
}

HF_INT32 HF_PP_GetInfo(HF_CHAR * psInput, HF_CHAR * psOutput)
{
    return PP_GetInfo(psInput, psOutput);
}

HF_INT32 HF_PP_DefineWKPAN(HF_CHAR * psInput, HF_CHAR * psOutput)
{
    return PP_DefineWKPAN(psInput, psOutput);
}

//29.06.2015
HF_INT32 HF_PP_Display(HF_CHAR * psMsg)
{
    return PP_Display(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_DisplayEx(HF_CHAR * psMsg)
{
    return PP_DisplayEx(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_StartGetKey(void)
{
    return PP_StartGetKey();
}

//29.06.2015
HF_INT32 HF_PP_GetKey(void)
{
    return PP_GetKey();
}

HF_INT32 HF_PP_StartGetPIN(HF_CHAR * psInput)
{
    return PP_StartGetPIN(psInput);
}

HF_INT32 HF_PP_GetPIN(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
    return PP_GetPIN(psOutput, psMsgNotify);
}

//29.06.2015
HF_INT32 HF_PP_StartCheckEvent(HF_CHAR * psInput)
{
    return PP_StartCheckEvent(psInput);
}

//29.06.2015
HF_INT32 HF_PP_CheckEvent(HF_CHAR * psOutput)
{
    return PP_CheckEvent(psOutput);
}

HF_INT32 HF_PP_EncryptBuffer(HF_CHAR * psInput, HF_CHAR * psOutput)
{
    return PP_EncryptBuffer(psInput, psOutput);
}

HF_INT32 HF_PP_GetDUKPT(HF_CHAR * psInput, HF_CHAR * psOutput)
{
    return PP_GetDUKPT(psInput, psOutput);
}

HF_INT32 HF_PP_StartChipDirect(HF_CHAR * psInput)
{
    return PP_StartChipDirect(psInput);
}

HF_INT32 HF_PP_ChipDirect(HF_CHAR * psOutput)
{
    return PP_ChipDirect(psOutput);
}

//29.06.2015
HF_INT32 HF_PP_StartRemoveCard(HF_CHAR * psMsg)
{
    return PP_StartRemoveCard(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_RemoveCard(HF_CHAR * psMsgNotify)
{
    return PP_RemoveCard(psMsgNotify);
}

HF_INT32 HF_PP_StartGenericCmd(HF_CHAR * psInput)
{
    return PP_StartGenericCmd(psInput);
}

HF_INT32 HF_PP_GenericCmd(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
    return PP_GenericCmd(psOutput, psMsgNotify);
}

//29.06.2015
HF_INT32 HF_PP_StartGetCard(HF_CHAR * psSrvConPar, HF_CHAR * psInput)
{
	//questionar psSrvConPar
    return PP_StartGetCard(psInput);
}

//29.06.2015
HF_INT32 HF_PP_GetCard(HF_CHAR * psOutput, HF_CHAR * psSrvErrMsg, HF_CHAR * psMsgNotify)
{
	//questionar psSrvErrMsg
    return PP_GetCard(psOutput, psMsgNotify);
}

HF_INT32 HF_PP_ResumeGetCard(void)
{
    return PP_ResumeGetCard();
}

HF_INT32 HF_PP_ChangeParameter(HF_CHAR * psInput)
{
    return PP_ChangeParameter(psInput);
}

HF_INT32 HF_PP_StartGoOnChip(HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psTagsOpt)
{
    return PP_StartGoOnChip(psInput, psTags, psTagsOpt);
}

HF_INT32 HF_PP_GoOnChip(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
    return PP_GoOnChip(psOutput, psMsgNotify);
}

HF_INT32 HF_PP_FinishChip(HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psOutput)
{
    return PP_FinishChip(psInput, psTags, psOutput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadInit(HF_CHAR * psInput)
{
    return PP_TableLoadInit(psInput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadRec(HF_CHAR * psInput)
{
    return PP_TableLoadRec(psInput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadEnd(void)
{
    return PP_TableLoadEnd();
}

//29.06.2015
HF_INT32 HF_PP_GetTimeStamp(HF_CHAR * psInput, HF_CHAR * psOutput)
{
    return PP_GetTimeStamp(psInput, psOutput);
}

/*HF_INT32 Ulong2Asc(HF_INT32 x, HF_CHAR y, HF_CHAR _x)
{
	return 0;
}

HF_INT32 ulAsc2Ulong(HF_CHAR x,HF_INT32 size_x)
{
	return 0;
}*/

int APP_iTestCancel()
{
	return 0;
}

/*

int _PIN_iGetKeyInfo()
{
	return 0;
}

int _PIN_iInsKey()
{
	return 0;
}

int _PIN_iOpen()
{
	return 0;
}

int _PIN_iEncryptEx()
{
	return 0;
}

int _PIN_iEncrypt()
{
	return 0;
}
*/
