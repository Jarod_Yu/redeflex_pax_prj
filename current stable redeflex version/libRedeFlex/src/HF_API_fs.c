#define DO_NOT_REDEFINE_FILESYSTEM_FUNCTIONS
#include "HF_API.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <osal.h>
#include <xui.h>

// hzg 20150428---------------------
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statfs.h>
// ---------------------------------

#define MAX_OPEN_FILES  30
#define STACK_MAX_DEPTH  10
#define ROOTPATH "/data/app/MAINAPP/"

typedef struct {
    char filename[HF_FS_MAX_PATH_LENGTH];
    FILE *f;
} HFI_FILE_T;

static HF_INT32 HF_errno;

typedef struct _tagFileInfo
{
	char *fileName;
	FILE *pFile;
	HF_INT32 rwFlag;   //1 write  0 read

}FILE_INFO;

typedef struct _tagFileNode
{
	FILE_INFO  fileInfo;
	struct _tagFileNode *next;
}FILE_NODE;

FILE_NODE *nodeHeader = NULL;

HF_INT32 curOpenFileNum = 0;

HF_INT32 stackPosition = -1;
HF_CHAR path_stack[STACK_MAX_DEPTH][HF_FS_MAX_FILE_NAME];


static HF_VOID nodeLogView(FILE_NODE *viewNode);
static HF_VOID changeFileListHeader(FILE_NODE *newNode);
static HF_BOOL nodeInsert(FILE_NODE *header, FILE_NODE *insertNode);
static HF_BOOL nodeDelete(FILE_NODE *header, FILE *fd);
static HF_BOOL nodeSearch(FILE_NODE *header, const HF_CHAR *filename);
static FILE_NODE *CreateNewNode(const HF_CHAR *filename, const HF_CHAR *accessmode, FILE *fd);
static HF_BOOL listInsert(const HF_CHAR *filename,  const HF_CHAR *accessmode, HFI_FILE_T *fp);
static HF_VOID upPath(HF_CHAR *path);
static HF_BOOL getAbsolutePath(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen);
static HF_BOOL checkPath(const HF_CHAR *filename, /*[PAXBR]*/HF_CHAR *szAbPath);
static HF_BOOL isDirEmpty(DIR *dirPtr);


HF_INT32 HF_fs_fopenH(const HF_CHAR *filename, const HF_CHAR *accessmode, HF_FILE_T **file)
{
	HF_INT32 iRet = 0;
	
	// Jarod@2015/07/28 ADD START
	DIR *dirPtr = NULL;
	// Jarod@2015/07/28 ADD END
	
	HFI_FILE_T *h_file_t = NULL;
	HF_CHAR absolutePath[HF_FS_MAX_PATH_LENGTH] = {0};

	OsLogSetTag("I");
	////PaxSetLogTag("  %s",__FUNCTION__);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fopenH %d", curOpenFileNum);

	if((NULL == file) || (NULL == filename))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fopenH (NULL == file) return [%d]", RF_ERR_DEVICEFAULT);
		return RF_ERR_DEVICEFAULT;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fopenH[%s - %s]", filename, accessmode);

	//if the open file number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	if(curOpenFileNum >= MAX_OPEN_FILES)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fopenH[HF_ERR_OPENFILES] : %d, %d", curOpenFileNum, MAX_OPEN_FILES);
    	return HF_ERR_OPENFILES;
	}

	if(*filename == '/')
	{
		filename++;
	}

	//whether the path is correct
	if(!checkPath(filename, NULL))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_fopenH[%d]", HF_errno);
		return HF_errno;
	}

	//whether accessmode is correct
    if( (NULL == accessmode) || ((0 != strcmp(accessmode, "r")) && (0 != strcmp(accessmode, "rw"))))
    {
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_fopenH[%d]", HF_ERR_INVALMODE);
    	return HF_ERR_INVALMODE;
    }

    //if filename is a directory
    dirPtr = opendir(filename);
    if(NULL != dirPtr)
    {
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5. HF_fs_fopenH[%d]", HF_ERR_ISADIR);
    	closedir(dirPtr);
    	return HF_ERR_ISADIR;
    }

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5.1 getAbsolutePath(%s, %s, %d)", filename, absolutePath, HF_FS_MAX_PATH_LENGTH);
    iRet = getAbsolutePath(filename, absolutePath, HF_FS_MAX_PATH_LENGTH);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5.2 getAbsolutePath(%s, %s, %d) = [%d]", filename, absolutePath, HF_FS_MAX_PATH_LENGTH, iRet);
    if(0 == iRet)
    {
    	strcpy(absolutePath, filename);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"6. HF_fs_fopenH[%s, %s]", absolutePath, filename);
    }

    //if the file is already open in "rw" mode, return HF_ERR_ISOPEN
	if(nodeSearch(nodeHeader, absolutePath) && (0 == strcmp(accessmode, "rw")))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"file is write absolutePath:%s ",absolutePath);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"6. File is already open in rw mode - absolutePath[%s]", absolutePath);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"6. HF_fs_fopenH[%d]", HF_ERR_ISOPEN);
	  	return HF_ERR_ISOPEN;
	}

	h_file_t = (HFI_FILE_T *)malloc(sizeof(HFI_FILE_T));
    if(!h_file_t)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"7. HF_fs_fopenH[%d]", HF_ERR_NOMEMORY);
    	return HF_ERR_NOMEMORY;
    }
    // [PAXBR] - Replace filename with absolutePath
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"h_file_t->filename[%s]", absolutePath);
	strcpy(h_file_t->filename, absolutePath);

    //trans open mode while accessmode is "rw"
    if(0 == strcmp(accessmode, "rw"))
    {
    	// [PAXBR] - Replace filename with absolutePath
    	if(0 == access(absolutePath, F_OK))
		{
			h_file_t->f = fopen(absolutePath, "r+");
		}
		else
		{
			h_file_t->f = fopen(absolutePath, "w+");
		}
    }
    else
    {
    	h_file_t->f = fopen(absolutePath, "r");
    }

    if (!h_file_t->f)
    {
    	free(h_file_t);
    	//fopen failed return the errno
    	// [PAXBR]
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d - %s]", errno, strerror(errno));
		switch(errno)
		{
		case ENOENT:
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOTFOUND);
			return HF_ERR_NOTFOUND;
		case ENOMEM:
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOMEMORY);
			return HF_ERR_NOMEMORY;
		case ENOSPC:
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOSPACE);
			return HF_ERR_NOSPACE;
		default:
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", RF_ERR_DEVICEFAULT);
			return RF_ERR_DEVICEFAULT;
		}
    }

    //insert node to open file list
    if(HF_FALSE == listInsert(absolutePath, accessmode, h_file_t))
    {
		free(h_file_t);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"9. HF_fs_fopenH[%d]", HF_errno);
    	return HF_errno;
    }

    *file = h_file_t;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fopenH");
    return HF_SUCCESS;
}

HF_FILE_T *HF_fs_fopenglobalH(const HF_CHAR *filename, const HF_CHAR *accessMode)
{
    // DEPRECATED
	//OsLogSetTag("HF_fs_fopenglobalH");
	////PaxSetLogTag("  %s",__FUNCTION__);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fopenglobalH(%s, %s)", filename, accessMode);
    return HF_NULL;
}

HF_FILE_T *HF_fs_fopenexternalH(const HF_CHAR *filename, const HF_CHAR *accessMode, const HF_CHAR *fatherApp)
{
    // DEPRECATED
	//OsLogSetTag("HF_fs_fopenexternalH");
	//PaxSetLogTag("  %s",__FUNCTION__);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fopenexternalH(%s, %s, %s)", filename, accessMode, fatherApp);
    return HF_NULL;
}

HF_INT32 HF_fs_fclose(HF_FILE_T *fp)
{
    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;
	//OsLogSetTag("HF_fs_fclose");
    //PaxSetLogTag("  %s",__FUNCTION__);
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fclose");

    if (!fpi)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fclose[%d]", HF_SUCCESS);
        return HF_SUCCESS;
    }

    if (!fpi->f)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fclose[%d]", HF_ERR_DEVICEFAULT);
        return HF_ERR_DEVICEFAULT;
    }

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fclose[%s]", fpi->filename);

    //delete node from file list
 	if(nodeDelete(nodeHeader, fpi->f))
 	{
 		curOpenFileNum--;
 	}

     fclose(fpi->f);
     fpi->f = NULL;

     memset(fpi->filename, 0, HF_FS_MAX_PATH_LENGTH);
     free(fpi);
     fpi = NULL;

 	 PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fclose[%d]", HF_SUCCESS);
     return HF_SUCCESS;
}

HF_INT32 HF_fs_fread(const HF_FILE_T *fp, HF_INT8 *buffer, HF_UINT32 length)
{
    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

    //OsLogSetTag("HF_fs_fread");
    //PaxSetLogTag("  %s",__FUNCTION__);
    //PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fread :%d", length);
    if((!fpi )|| (!fpi->f ) || (NULL == buffer) || (length > HF_FS_MAXREADBUFFSZ))
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fread[%d]", HF_ERR_DEVICEFAULT);
        return HF_ERR_DEVICEFAULT;
    }

    HF_INT32 ret = fread(buffer, 1, length, fpi->f);
    if (!ret)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fread[%d]", HF_ERR_EOF);
    	ret = HF_ERR_EOF;
    }

    //PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fread");
    return ret;
}

HF_INT32 HF_fs_fwrite(const HF_FILE_T *fp, const HF_INT8 *buffer, HF_UINT32 length)
{
    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

//    OsLogSetTag("HF_fs_fwrite");
    //PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fwrite:%d", length);
    //PaxSetLogTag("  %s",__FUNCTION__);

	HF_INT32 writeCnt = 0;

    if ((!fpi) || (!fpi->f) || (!buffer))
    {
//    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fwrite[%d]", HF_ERR_DEVICEFAULT);
        return HF_ERR_DEVICEFAULT;
    }

    if(length > HF_FS_MAXWRITEBUFFSZ)
    {
//    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fwrite[HF_ERR_BUFFOVERFLOW] : [%d > %d]", length, HF_FS_MAXWRITEBUFFSZ);
    	return HF_ERR_BUFFOVERFLOW;
    }

    writeCnt =  fwrite(buffer, 1, length, fpi->f);
    if(writeCnt < 0)
    {
    	switch(errno)
    	{
    	case EROFS:
//        	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_fwrite[%d]", HF_ERR_READONLY);
    		return HF_ERR_READONLY;
    	case ENOSPC:
//        	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_fwrite[%d]", HF_ERR_NOSPACE);
    		return HF_ERR_NOSPACE;
    	default:
//        	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_fwrite[%d]", HF_ERR_DEVICEFAULT);
    		return HF_ERR_DEVICEFAULT;
    	}
    }

//    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fwrite");
    return writeCnt;
}

HF_INT32 HF_fs_fseek(const HF_FILE_T *fp, HF_INT32 offset, HF_UINT16 position)
{
    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

//    OsLogSetTag("HF_fs_fseek");
    //PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fseek");
    //PaxSetLogTag("  %s",__FUNCTION__);

    if ((!fpi) || (!fpi->f))
    {
//    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fseek[%d]", HF_ERR_DEVICEFAULT);
        return HF_ERR_DEVICEFAULT;
    }

    if(fseek(fpi->f, offset, position))
    {
//    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fseek[%d]", HF_ERR_OUTOFBOUNDS);
    	return HF_ERR_OUTOFBOUNDS;
    }

//    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fseek");
    return ftell(fpi->f);
}

HF_INT32 HF_fs_unlink(const HF_CHAR *filename)
{
	DIR *dirPtr = NULL;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
//    OsLogSetTag("HF_fs_unlink");
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_unlink :%s", filename);
	//PaxSetLogTag("  %s",__FUNCTION__);

	if((NULL != filename) && (*filename == '/'))
	{
		filename++;
	}

	if(!checkPath(filename, abPath))
	{
//		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_unlink[%d]", HF_errno);
		return HF_errno;
	}

	//if the path is a dir
	if(HF_fs_isdir(abPath))
	{
		dirPtr = opendir(abPath);
		if(NULL == dirPtr)
		{
//			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_unlink[%d]", RF_ERR_DEVICEFAULT);
			return RF_ERR_DEVICEFAULT;
		}

		//if the dir is not empty
		if(!isDirEmpty(dirPtr))
		{
			closedir(dirPtr);
//			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_unlink[%d]", HF_ERR_NOTEMPTY);
			return HF_ERR_NOTEMPTY;
		}

		closedir(dirPtr);
	}

	//if the path is a file or empty dir
	//remove() delete file and empty dir
	//unlink() delete only file
	if(remove(abPath))
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			case EACCES:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOACCESSRIGHT);
				return HF_ERR_NOACCESSRIGHT;
			case ENOMEM :
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOMEMORY);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", RF_ERR_DEVICEFAULT);
				return RF_ERR_DEVICEFAULT;
		}
	}

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_unlink");
	return HF_SUCCESS;
}

HF_INT32 HF_fs_chdir(const HF_CHAR *dirname)
{
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];
//    OsLogSetTag("HF_fs_chdir");
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_chdir :%s", dirname);
	//PaxSetLogTag("  %s",__FUNCTION__);

    memset(abPath, 0, sizeof(abPath));

	if((NULL != dirname) && (*dirname == '/'))
	{
		dirname++;
	}

	if(!checkPath(dirname, abPath))
	{
//		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_chdir[%d]", HF_errno);
		return HF_errno;
	}

	//whether the path is a file
    FILE *file = fopen(abPath, "r");
    if (NULL != file)
    {
		fclose(file);
    	if(HF_FALSE == HF_fs_isdir(abPath))
    	{
    		return HF_ERR_ISAFILE;
    	}
    }

    if(chdir(abPath))
    {
		switch(errno)
		{
			case ENOENT:
//				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_chdir[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			default:
//				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_chdir[%d]", RF_ERR_DEVICEFAULT);
				return RF_ERR_DEVICEFAULT;
		}
    }

//    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_chdir");
    return HF_SUCCESS;
}

HF_INT32 HF_fs_getcwd(HF_CHAR *dirname)
{
//    OsLogSetTag("HF_fs_getcwd");
//    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_getcwd");
	//PaxSetLogTag("  %s",__FUNCTION__);

	if(NULL == dirname)
	{
		return RF_ERR_DEVICEFAULT;
	}

    if(!getcwd(dirname, HF_FS_MAX_PATH_LENGTH))
    {
//    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getcwd[%d]", RF_ERR_DEVICEFAULT);
    	return RF_ERR_DEVICEFAULT;
    }


//    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getcwd");
    return HF_SUCCESS;
}


HF_INT32 HF_fs_mkdir(const HF_CHAR *dirname)
{
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];
	//OsLogSetTag("HF_fs_mkdir");
	//PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));

	if((NULL != dirname) && (*dirname == '/'))
	{
		dirname++;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_mkdir");
	if(!checkPath(dirname, abPath))
	{
	  PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_mkdir[%d]", HF_errno);
	  return HF_errno;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1.1 HF_fs_mkdir[%s]", dirname);

	// check if the dir is exists
	// [PAXBR] - Replace dirname with abPath
	if ( abPath != NULL )
	{
		if(opendir(abPath))
		{
		   PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_mkdir[%d]", HF_ERR_ALREADYEXISTS);
		   return HF_ERR_ALREADYEXISTS;
		}

		   //TODO what is the mode?
		  //if(mkdir(abPath, S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP|S_IXGRP|S_IROTH|S_IWOTH|S_IXOTH))
		if(mkdir(abPath, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH))
		{
		   PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. mkdir[%d - %s]", errno, strerror(errno));
		   switch(errno)
		   {
			   case ENOSPC:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_mkdir[%d]", HF_ERR_NOSPACE);
				return HF_ERR_NOSPACE;
			   default:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_mkdir[%d]", HF_ERR_DEVICEFAULT);
				return HF_ERR_DEVICEFAULT;
		   }
		}
	 }

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_mkdir");
    return HF_SUCCESS;
}

HF_INT32 HF_fs_rename(const HF_CHAR *oldname, const HF_CHAR *newname)
{
	HF_CHAR  abPathOld[HF_FS_MAX_PATH_LENGTH];
	HF_CHAR  abPathNew[HF_FS_MAX_PATH_LENGTH];

//    OsLogSetTag("HF_fs_rename");
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_rename:%s %s", oldname, newname);
	//PaxSetLogTag("  %s",__FUNCTION__);

    memset(abPathOld, 0, sizeof(abPathOld));
    memset(abPathNew, 0, sizeof(abPathNew));

	if((NULL != oldname) && (*oldname == '/'))
	{
		oldname++;
	}

	if((NULL != newname) && (*newname == '/'))
	{
		newname++;
	}

	if(!checkPath(oldname, abPathOld) || !checkPath(newname, abPathNew))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_rename[%d]", HF_errno);
		return HF_errno;
	}

	//if file or dir is exists
	if(HF_fs_exists(abPathNew))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_rename[%d]", HF_ERR_ALREADYEXISTS);
		return HF_ERR_ALREADYEXISTS;
	}

    if(rename(abPathOld, abPathNew))
    {
    	switch(errno)
    	{
    		case ENOENT:
    			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOTFOUND);
    			return HF_ERR_NOTFOUND;
    		case EACCES:
    			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOACCESSRIGHT);
    			return HF_ERR_NOACCESSRIGHT;
    		case ENOSPC:
    			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOSPACE);
    			return HF_ERR_NOSPACE;
    		default:
    			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_DEVICEFAULT);
    			return HF_ERR_DEVICEFAULT;
    	}
    }

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_rename");
    return HF_SUCCESS;
}

HF_INT32 HF_fs_freediskspace(HF_VOID)
{
	struct statfs diskInfo;

    //OsLogSetTag("HF_fs_freediskspace");
    //PaxSetLogTag("  %s",__FUNCTION__);

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_freediskspace");

	memset(&diskInfo, 0, sizeof(diskInfo));

	if(statfs("/data", &diskInfo))  // /data/app/MAINAPP
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_rename[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_bavail %d", diskInfo.f_bavail);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_bfree %d", diskInfo.f_bfree);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_blocks %d", diskInfo.f_blocks);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_bsize %d", diskInfo.f_bsize);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__," diskInfo.f_ffree %d", diskInfo.f_ffree);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__," diskInfo.f_files %d", diskInfo.f_files);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_flags %d", diskInfo.f_flags);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_frsize %d", diskInfo.f_frsize);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_fsid %d", diskInfo.f_fsid);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_namelen %d", diskInfo.f_namelen);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_spare %d", diskInfo.f_spare);
//	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"diskInfo.f_type %d", diskInfo.f_type);

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_freediskspace");
    return (diskInfo.f_bfree*diskInfo.f_bsize);
}

HF_INT32 HF_fs_opendir(const HF_CHAR *dirname, HF_HANDLE_T * hDir)
{
	DIR *dirPtr = NULL;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

    //OsLogSetTag("HF_fs_opendir");
    //PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_opendir:%s %d",dirname ,curOpenFileNum);

	if((!dirname) || (!hDir))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_opendir[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	if(*dirname == '/')
	{
		dirname++;
	}

	//if the open dir number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	if(curOpenFileNum >= MAX_OPEN_FILES)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_opendir[HF_ERR_OPENFILES]: [%d >= %d]", curOpenFileNum, MAX_OPEN_FILES);
    	return HF_ERR_OPENFILES;
	}

	if(!checkPath(dirname, abPath))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_opendir[%d]", HF_errno);
		return HF_errno;
	}

    dirPtr = opendir(abPath);
	if(NULL == dirPtr)
	{
		switch(errno)
		{
			//no such directory
			case ENOENT:
			case ENOTDIR:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_NOMEMORY);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_DEVICEFAULT);
				return HF_ERR_DEVICEFAULT;
		}
	}

	//if dir is empty
	if(isDirEmpty(dirPtr))
	{
		//close dir and return
		closedir(dirPtr);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5. HF_fs_opendir[%d]", HF_ERR_EMPTYDIR);
		return HF_ERR_EMPTYDIR;
	}

	//get the dir handle
	*hDir = dirPtr;
	curOpenFileNum++;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_opendir");
    return HF_SUCCESS;
}

HF_INT32 HF_fs_readdir(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz)
{
	off_t offDir = 0;
	struct dirent *entry = NULL;

    //OsLogSetTag("HF_fs_readdir");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_readdir");

	if((!hDir) || (!filefound))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_readdir[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	offDir = telldir((DIR *)hDir);
	if(isDirEmpty((DIR *)hDir))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_readdir[%d]", HF_ERR_EMPTYDIR);
		return HF_ERR_EMPTYDIR;
	}
	seekdir((DIR *)hDir, offDir);

	entry = readdir((DIR *)hDir);
	if(!entry)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_readdir[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	if(strlen(entry->d_name) > buffSz+1)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_readdir[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}
	else
	{
		strcpy(filefound, entry->d_name);
	}

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_readdir");
    return HF_SUCCESS;
}

HF_INT32 HF_fs_closedir(HF_HANDLE_T dirsearch)
{
    //OsLogSetTag("HF_fs_closedir");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_closedir");

    if(!dirsearch)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_closedir[%d]", HF_ERR_DEVICEFAULT);
    	return HF_ERR_DEVICEFAULT;
    }

    if(0 != closedir((DIR *)dirsearch))
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_closedir[%d]", HF_ERR_DEVICEFAULT);
    	return HF_ERR_DEVICEFAULT;
    }

    //free(dirsearch);
    curOpenFileNum--;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_closedir");
    return HF_SUCCESS;
}

HF_BOOL HF_fs_isdir(const HF_CHAR *path)
{
	struct stat dirStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

    //OsLogSetTag("HF_fs_opendir");
    //PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));
	memset(&dirStat, 0, sizeof(dirStat));

    //OsLogSetTag("HF_fs_isdir");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_isdir:%s", path);

    if(!path)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_isdir[%d]", HF_FALSE);
    	return HF_FALSE;
    }

	if(*path == '/')
	{
		path++;
	}

	if(HF_FALSE == getAbsolutePath(path, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_isdir getAbsolutePath - error");
		return HF_FALSE;
	}

	if(stat(abPath, &dirStat) != 0)
	{
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_isdir[%d]", HF_FALSE);
		return HF_FALSE;
	}

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_isdir");
	return S_ISDIR(dirStat.st_mode);
}

HF_BOOL HF_fs_isglobal(const HF_CHAR *filename)
{
    // ---------------------------------
    // TODO
    // ---------------------------------
    return HF_FALSE;
}

HF_BOOL HF_fs_exists(const HF_CHAR *path)
{
	struct stat dirStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

    //OsLogSetTag("HF_fs_exists");
	//PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));
	memset(&dirStat, 0, sizeof(dirStat));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_exists:%s", path);

    if(!path)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_exists[%d]", HF_ERR_DEVICEFAULT);
    	return HF_ERR_DEVICEFAULT;
    }

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_exists[%s]", path);

	if(*path == '/')
	{
		path++;
	}

	if(HF_FALSE == getAbsolutePath(path, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_exists getAbsolutePath - error");
		return HF_FALSE;
	}

	if(stat(path, &dirStat) != 0)   //stat excute success means the path is a dir or file
	{
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_exists[%d]", HF_FALSE);
		return HF_FALSE;
	}

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_exists");
	return HF_TRUE;
}

HF_INT32 HF_fs_getfsize(const HF_CHAR *filename, HF_UINT32 *fileSz)
{
	struct stat fileStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

    //OsLogSetTag("HF_fs_getfsize");
	//PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));
	memset(&fileStat, 0, sizeof(fileStat));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_getfsize:%s", filename);

	if((!filename) || (!fileSz))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getfsize[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	if(*filename == '/')
	{
		filename++;
	}

	*fileSz = 0;

	if(!checkPath(filename, abPath))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_getfsize[%d]", HF_errno);
		return HF_errno;
	}

    if(HF_fs_isdir(abPath))
    {
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getfsize[%d]", HF_ERR_ISADIR);
    	return HF_ERR_ISADIR;
    }

	if(stat(abPath, &fileStat) != 0)
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_NOMEMORY);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_DEVICEFAULT);
				return HF_ERR_DEVICEFAULT;
		}
	}

    *fileSz = fileStat.st_size;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getfsize");
    return HF_SUCCESS ;
}

HF_INT32 HF_fs_getmtime(const HF_CHAR *filename, HF_INT32 *mTime)
{
	struct stat fileStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

    //OsLogSetTag("HF_fs_getmtime");
	//PaxSetLogTag("  %s",__FUNCTION__);

	memset(abPath, 0, sizeof(abPath));
	memset(&fileStat, 0, sizeof(fileStat));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_getmtime");

	if((!filename) || (!mTime))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getmtime[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	if(*filename == '/')
	{
		filename++;
	}

	if(!checkPath(filename, abPath))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_getmtime[%d]", HF_errno);
		return HF_errno;
	}

	if(stat(abPath, &fileStat) != 0)
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_NOMEMORY);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_DEVICEFAULT);
				return HF_ERR_DEVICEFAULT;
		}
	}

    *mTime = fileStat.st_mtim.tv_sec;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getmtime");
    return HF_SUCCESS;
}

HF_BOOL RF_fs_existsexternal(const HF_CHAR *path, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_existsexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_existsexternal(%s, %s)", path, volume);
    return HF_FALSE;
}

HF_INT32 RF_fs_chdirexternal(const HF_CHAR *dirname, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_chdirexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_chdirexternal(%s, %s)", dirname, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_opendirexternal(const HF_CHAR *dirname, HF_HANDLE_T *hDir, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_opendirexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_opendirexternal(%s, %s)", dirname, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_readdirexternal(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_readdirexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_readdirexternal(%s, %d, %s)", filefound, buffSz, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_BOOL RF_fs_isdirexternal(const HF_CHAR *path, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_isdirexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_isdirexternal(%s, %s)", path, volume);
    return HF_FALSE;
}

HF_INT32 RF_fs_unlinkexternal(const HF_CHAR *filename, const HF_CHAR *volume)
{
    //OsLogSetTag("RF_fs_unlinkexternal");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_unlinkexternal(%s, %s)", filename, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_fopenexternalH(const HF_CHAR *filename, const HF_CHAR *accessMode, const HF_CHAR *volume, HF_FILE_T **output)
{
    //OsLogSetTag("RF_fs_fopenexternalH");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_fopenexternalH(%s, %s, %s)", filename, accessMode, volume);
    return HF_NULL;
}

//TODO Below is the function define by richard
/************add by richard 2015-05-15 file limit***********/
static HF_VOID nodeLogView(FILE_NODE *viewNode)
{
	FILE_NODE *tempNode = NULL;
	HF_INT32 listLen = 0;

	//OsLogSetTag("nodeLogView");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeLogView");

	if((NULL == viewNode))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"viewNode NULL");
		return ;
	}

	tempNode = viewNode;
	while(NULL != tempNode)
	{
		listLen++;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"viewNode fileName:%s pFile:%p rwFlag:%d CurfNum:%d listLen:%d",
			tempNode->fileInfo.fileName, tempNode->fileInfo.pFile, tempNode->fileInfo.rwFlag, curOpenFileNum, listLen);

		tempNode = tempNode->next;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeLogView");
}

//change global variable - nodeHeader
static HF_VOID changeFileListHeader(FILE_NODE *newNode)
{
	//OsLogSetTag("changeFileListHeader");
	//PaxSetLogTag("  %s",__FUNCTION__);

	nodeHeader = newNode;
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"change ->nodeHeader:%p ",nodeHeader);
}

static HF_BOOL nodeInsert(FILE_NODE *header, FILE_NODE *insertNode)
{
	FILE_NODE *tempNode = NULL;

	//OsLogSetTag("nodeInsert");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeInsert");

	if(NULL == insertNode)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeInsert[%d]", HF_FALSE);
		return HF_FALSE;
	}

	//insert the first file node
	if(NULL == header)
	{
		changeFileListHeader(insertNode);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeInsert[%d]", HF_TRUE);
		return HF_TRUE;
	}

	tempNode = header;
	while(NULL != tempNode->next)
	{
		tempNode = tempNode->next;
	}

	tempNode->next = insertNode;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeInsert");
	return HF_TRUE;
}

static HF_BOOL nodeDelete(FILE_NODE *header, FILE *fd)
{
	FILE_NODE *tempNode = NULL;
	FILE_NODE *tempNode2 = NULL;

	//OsLogSetTag("nodeDelete");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeDelete");

	if((NULL == header) || (NULL == fd))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeDelete[%d]", HF_FALSE);
		return HF_FALSE;
	}

	tempNode = header;

	//if the delete node is at the first of the list
	if(tempNode->fileInfo.pFile == fd)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ANTES DELETE %p %p",tempNode, tempNode->next);
		tempNode2 = tempNode->next;
		free(tempNode->fileInfo.fileName);
		free(tempNode);

		//change nodeHeader
		changeFileListHeader(tempNode2);

		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"DELETE PRIMEIRO N�� %p",tempNode);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeDelete[%d]", HF_TRUE);

		return HF_TRUE;
	}

	//delete a node in the mid or end of the list
	while(NULL != tempNode->next)
	{
		if(tempNode->next->fileInfo.pFile == fd)
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ANTES DELETE %p %p %p",tempNode, tempNode->next, tempNode->next->next);
			tempNode2 = tempNode->next->next;

			//free node memory
			free(tempNode->next->fileInfo.fileName);
			free(tempNode->next);

			tempNode->next = tempNode2;
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"AP��S DELETE %p %p",tempNode, tempNode->next);

			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. SAIU nodeDelete[%d]", HF_TRUE);

			return HF_TRUE;
		}

		tempNode = tempNode->next;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeDelete(%d)", HF_FALSE);

	return HF_FALSE;
}

static HF_BOOL nodeSearch(FILE_NODE *header, const HF_CHAR *filename)
{
	FILE_NODE *tempNode = NULL;

	//OsLogSetTag("nodeSearch");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeSearch");

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeSearch[%s]", filename);

	if((NULL == filename) || (NULL == header))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeSearch[%d]", HF_FALSE);
		return HF_FALSE;
	}

	tempNode = header;
	while(NULL != tempNode)
	{
		if(0 == strcmp(tempNode->fileInfo.fileName, filename) && (1 == tempNode->fileInfo.rwFlag))
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeSearch[%d]", HF_TRUE);
			return HF_TRUE;
		}

		tempNode = tempNode->next;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeSearch[%d]", HF_FALSE);
	return HF_FALSE;
}

static FILE_NODE *CreateNewNode(const HF_CHAR *filename, const HF_CHAR *accessmode, FILE *fd)
{
	FILE_NODE *fileNode = NULL;

	//get file infomation

	//OsLogSetTag("CreateNewNode");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU CreateNewNode");

	if((NULL == filename) || (NULL == accessmode) || (NULL == fd))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. CreateNewNode[%d]", HF_NULL);
		return HF_NULL;
	}

	//malloc filenode
	fileNode = (FILE_NODE *)malloc(sizeof(FILE_NODE));
	if(NULL == fileNode)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc FALHOU");
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. CreateNewNode[%d]", HF_NULL);
		return NULL;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc fileNode:%p LEN:%d", fileNode, sizeof(FILE_NODE));

	//malloc filename
	fileNode->fileInfo.fileName = (HF_CHAR *)malloc((strlen(filename)+1));
	if(NULL == fileNode)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. CreateNewNode[%d]", HF_NULL);
		return NULL;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc fileName:%p LEN:%d",fileNode->fileInfo.fileName, strlen(filename)+1);

	strcpy(fileNode->fileInfo.fileName, filename);
	fileNode->fileInfo.pFile = fd;
	fileNode->next = NULL;
	if(0 == strcmp(accessmode, "r"))
	{
		fileNode->fileInfo.rwFlag = 0;
	}
	else
	{
		fileNode->fileInfo.rwFlag = 1;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"create node fileName:%s pFile:%p rwFlag:%d", fileNode->fileInfo.fileName, fileNode->fileInfo.pFile, fileNode->fileInfo.rwFlag);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU CreateNewNode");

	return fileNode;
}

static HF_BOOL listInsert(const HF_CHAR *filename,  const HF_CHAR *accessmode, HFI_FILE_T *fp)
{
	FILE_NODE *fileNode = NULL;

	//OsLogSetTag("CreateNewNode");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU listInsert");

	if((NULL == filename) || (NULL == accessmode) || (NULL == fp))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. listInsert[%d]", HF_NULL);
		return HF_NULL;
	}

    //create filenode for the open file
	fileNode = CreateNewNode(filename, accessmode, fp->f);
	if(NULL == fileNode)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. listInsert[%d]", HF_ERR_NOMEMORY);
		HF_errno = HF_ERR_NOMEMORY;
		return HF_FALSE;
	}

	if(HF_TRUE == nodeInsert(nodeHeader, fileNode))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"INSER??O COM SUCESSO  pFile:%p ",fileNode->fileInfo.pFile);
		curOpenFileNum++;
	}

	//show current file node
	nodeLogView(nodeHeader);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU listInsert");
	return HF_TRUE;
}

/***********************************************************/
static HF_VOID stack_init()
{
	stackPosition = 2;
	memset(path_stack, 0, sizeof(path_stack));
	strcpy(path_stack[0], "data");
	strcpy(path_stack[1], "app");
	strcpy(path_stack[2], "MAINAPP");
}

static HF_VOID stack_clean()
{
	stackPosition = -1;
	memset(path_stack, 0, sizeof(path_stack));
}

static HF_BOOL stack_push(HF_CHAR *dir)
{
	stackPosition++;
	if((stackPosition >= 0) && (stackPosition < STACK_MAX_DEPTH))
	{
		strcpy(path_stack[stackPosition], dir);
		return HF_TRUE;
	}
	else
	{
		return HF_FALSE;
	}
}

static HF_CHAR* stack_pop()
{
	if((stackPosition < 0) || (stackPosition >= STACK_MAX_DEPTH))
	{
		return NULL;
	}
	else
	{
		return path_stack[stackPosition--];
	}
}


static HF_BOOL getAbsolutePath(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen)
{
	int i = 0;
	HF_CHAR * tok = NULL;
	HF_CHAR * abPath = NULL;
	HF_CHAR temp[HF_FS_MAX_PATH_LENGTH];

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU getAbsolutePath");

	stack_init();
	memset(temp, 0 , maxLen);

	if((NULL == filename) || (NULL == output))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. getAbsolutePath[%d]", HF_FALSE);
		return HF_FALSE;
	}

	strcpy(temp, filename);
	tok = strtok(temp, "/");
	while (tok != HF_NULL)
	{
		if (strcmp(tok, "..") == 0)
		{
			if (NULL == stack_pop())
			{
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. getAbsolutePath stack_pop NULL");
				stack_clean();
				return HF_FALSE;
			}
		}
		else if (HF_strcmp(tok, ".") != 0)
		{
			if(strlen(tok) > HF_FS_MAX_FILE_NAME)
			{
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. getAbsolutePath strlen(tok) > HF_FS_MAX_FILE_NAME");
				stack_clean();
				return HF_FALSE;
			}

			stack_push(tok);
		}
		tok = strtok(NULL, "/");
	}

	memset(temp, 0 , maxLen);
	abPath = temp;
	for(i=0; i<=stackPosition; i++)
	{
		*abPath++ = '/';
		strcpy(abPath, path_stack[i]);
		abPath += strlen(path_stack[i]);
	}

	strcpy(output, temp);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. getAbsolutePath output:%s", output);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU getAbsolutePath");
	stack_clean();
	return HF_TRUE;
}

static HF_BOOL checkPath(const HF_CHAR *filename, HF_CHAR *szAbPath)
{
	HF_INT32 dirDepth = 0;
	HF_INT32 pathLen = 0;
	HF_INT32 dirOrFileLen = 0;
	HF_INT32 extendLen = 0;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	//OsLogSetTag("checkPath");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU checkPath");

	memset(abPath, 0, sizeof(abPath));

	// [PAXBR] - Initialize variable
	if ( szAbPath != NULL )
		memset(szAbPath, 0, sizeof(szAbPath));

	if(!filename)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. checkPath[%d]", RF_FALSE);
		HF_errno = RF_ERR_DEVICEFAULT;
		return RF_FALSE;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. checkPath[%s]", filename);

	//exclude the './' and "../"
	if(HF_FALSE == getAbsolutePath(filename, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		HF_errno = HF_ERR_MAXLEN;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. checkPath[%d]", RF_FALSE);
		return RF_FALSE;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. checkPath[%s, %s]", filename, abPath);

	//check the path word one by one
    while(*(abPath+pathLen) != '\0')
    {
    	if(*(abPath+pathLen) == '/')
    	{
    		dirOrFileLen = 0;
    		pathLen++;
    		dirDepth++;
    		continue;
    	}

    	//check first word of dir or file name
    	if(0 == dirOrFileLen)
    	{
    		if((*(abPath+pathLen)>='a' && *(abPath+pathLen) <='z') ||
    		   (*(abPath+pathLen)>='A' && *(abPath+pathLen) <='Z'))
    		{
    			dirOrFileLen++;
    			pathLen++;
    		}
    		else
    		{
    			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5. checkPath[%s, %s]", filename, abPath);
    			HF_errno = HF_ERR_PATHERR;
    			return RF_FALSE;
    		}
    	}
    	else
    	{
    		if('.' == *(abPath+pathLen) && !extendLen)
    		{
    			extendLen++;
    			pathLen++;
    			dirOrFileLen++;
    		}
    		else
    		{
    			if((*(abPath+pathLen)>='a' && *(abPath+pathLen) <='z') ||
    			   (*(abPath+pathLen)>='A' && *(abPath+pathLen) <='Z') ||
    			   (*(abPath+pathLen)>='0' && *(abPath+pathLen) <='9') || *(abPath+pathLen) == '_')
    			{
    				pathLen++;
    				dirOrFileLen++;
    				if(extendLen) extendLen++;
    			}
    			else
    			{
        			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"6. checkPath[%s, %s]", filename, abPath);

    				HF_errno = HF_ERR_PATHERR;
    				return RF_FALSE;
    			}
    		}
    	}

    	if((dirOrFileLen >= HF_FS_MAX_FILE_NAME) || (pathLen >= HF_FS_MAX_PATH_LENGTH))
    	{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"7. checkPath[%s, %s]", filename, abPath);

        	HF_errno = HF_ERR_MAXLEN;
        	return RF_FALSE;
    	}

    	if(dirDepth > HF_FS_MAX_DIR_DEPTH)
    	{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. checkPath[%s, %s]", filename, abPath);

    		HF_errno = HF_ERR_PATHTOODEEP;
    		return RF_FALSE;
    	}

    	if(extendLen > 4)
    	{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"9. checkPath[%s, %s]", filename, abPath);

    		HF_errno = HF_ERR_PATHERR;
    		return RF_FALSE;
    	}
    }

    // [PAXBR] - Replace the filename with the absolute path
    if ( szAbPath != NULL )
    	strcpy(szAbPath, abPath);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU checkPath");
    return RF_TRUE;
}

static HF_BOOL isDirEmpty(DIR *dirPtr)
{
	HF_INT32 elemNum = 0;

	//OsLogSetTag("isDirEmpty");
	//PaxSetLogTag("  %s",__FUNCTION__);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU isDirEmpty");

	if(!dirPtr)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. isDirEmpty[%d]", HF_FALSE);
		return HF_FALSE;
	}

	//let the pointer point to the begin position
	rewinddir(dirPtr);

	//check whether the dir is empty
	while(readdir(dirPtr))
	{
		elemNum++;
	}

	if(elemNum == 2)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. isDirEmpty[%d]", HF_TRUE);
		return HF_TRUE;
	}

	//let the pointer point to the begin position
	rewinddir(dirPtr);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU isDirEmpty[%d]", HF_FALSE);
	return HF_FALSE;
}
