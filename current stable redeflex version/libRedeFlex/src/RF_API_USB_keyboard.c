#ifndef _RF_API_USB_KEYBOARD_C_
#define _RF_API_USB_KEYBOARD_C_
/*
Copyright 2015 EBICS
*************************************************************
Nome     : RF_API_USB_keyboard.c
Descri��o: Fun��es de teclado USB
Autor    : 
Login    : 
Data     : 
Empresa  : 
*************************************************************/
/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/
#include "HF_API.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Locais                                           */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------
Autor,Empresa:	
Funcao		:	RF_USB_keyboard_getkeystroke()
Descri��o	:	Espera e retorna o c�digo da tecla pressionada no teclado pelo usu�rio ou um
				c�digo de erro (podendo indicar timeout).
				O valor retornado (em caso de sucesso) � o byte capturado no teclado (o
				mapeamento desta byte em c�digo de tecla ser� realizado pelo RedeFlex).
				A fun��o deve utilizar de "bufferiza��o" para captura teclas digitas pelo usu�rio
				antes da chamada da fun��o
Argumentos	:	Timeout	-	tempo m�ximo de espera pela tecla em segundos.
				Um valor negativo indica espera indefinida.
				O valor zero indica retornar imediatamente, indicando TIMEOUT caso o usu�rio n�o
				tenha digitado nenhuma tecla anteriormente.
Retorno		:	O byte capturado do teclado em caso de sucesso (um valor n�o negativo). Em caso
				de falha, um dos seguintes valores:
					RF_ERR_RESOURCEALLOC	-	se o recurso compartilhado n�o p�de atender �
												requisi��o (ex.: n�o p�de alocar um timer)
					RF_ERR_TIMEOUT			-	se tiver passado o tempo definido pelo par�metro
												'timeout' e nenhuma tecla tiver sido pressionada
					RF_ERR_DEVICEFAULT		-	se houve alguma falha no dispositivo
-----------------------------------------------------------------------------------------------*/
RF_INT32 RF_USB_keyboard_getkeystroke( RF_INT32 timeout )
{

	return RF_SUCCESS;
}

/*-----------------------------------------------------------------------------------------------
Autor,Empresa:	
Funcao		:	RF_USB_keyboard_getkeystroke_nobuffer()
Descri��o	:	Esta fun��o tem o mesmo comportamento da fun��o RF_USB_keyboard_getkeystroke, com
				a �nica diferen�a que ela desconsidera teclas que tenham sido pressionadas pelo
				usu�rio antes da chamada da fun��o, limpando o �buffer� de teclas j� pressionadas
Argumentos	:	Timeout	-	tempo m�ximo de espera pela tecla em segundos.
				Um valor negativo indica espera indefinida.
Retorno		:	O c�digo da tecla pressionada em caso de sucesso (um valor n�o negativo). Em caso
				de falha, um dos seguintes valores:
					RF_ERR_RESOURCEALLOC	-	se o recurso compartilhado n�o p�de atender �
												requisi��o (ex.: n�o p�de alocar um timer)
					RF_ERR_TIMEOUT			-	se tiver passado o tempo definido pelo par�metro
												'timeout' e nenhuma tecla tiver sido pressionada
					RF_ERR_DEVICEFAULT		-	se houve alguma falha no dispositivo
-----------------------------------------------------------------------------------------------*/
RF_INT32 RF_USB_keyboard_getkeystroke_nobuffer( RF_INT32 timeout )
{
	
	return RF_SUCCESS;
}
#ifdef __cplusplus
}
#endif // __cplusplus
#endif // _RF_API_USB_KEYBOARD_C_
