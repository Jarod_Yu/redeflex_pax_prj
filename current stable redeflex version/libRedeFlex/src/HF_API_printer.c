#include "HF_API.h"
#include <stdarg.h>

HF_INT32 HF_printer_linefeed(HF_INT32 lines)
{
    // ---------------------------------
    printf("HF_printer_linefeed(%d)\n", (int)lines);
    while (lines--)
    {
        printf("\n");
    }
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_printer_print(const HF_CHAR *text)
{
    // ---------------------------------
    OsLogSetTag("HF_printer_print");
    printf("%s\n", text);
    OsLog(LOG_DEBUG, "HF_printer_print[%s]", text);
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_printer_printf(const HF_CHAR *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
    // ---------------------------------
    printf(fmt, args);
    // ---------------------------------
	va_end(args);

    return HF_SUCCESS;
}

HF_INT32 HF_printer_printimage(const HF_IMAGE *image, HF_UINT32 flag)
{
    return HF_SUCCESS;
}

HF_INT32 HF_printer_check(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_printer_setFont(HF_FONT_T font)
{
    return HF_SUCCESS;
}

HF_INT32 HF_printer_getFont(HF_FONT_T *pFont)
{
    return HF_SUCCESS;
}

HF_INT32 HF_printer_getLineCharCount(HF_FONT_T pFont, HF_UINT32 *charCount)
{
    return HF_SUCCESS;
}

HF_INT32 HF_printer_printimagebuff(const HF_VOID *buff, HF_UINT32 size)
{
    return HF_SUCCESS;
}
