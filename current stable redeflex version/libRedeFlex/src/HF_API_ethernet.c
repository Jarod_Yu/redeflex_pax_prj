#include "HF_API.h"

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_connect(const HF_CHAR *pIp, const HF_CHAR *pPort, HF_BOOL  useSsl)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_disconnect(HF_VOID)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_status(HF_VOID)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_sendmsg(const HF_UINT8 *msg, HF_UINT16 length)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_recvmsg(HF_UINT8 *data, HF_UINT16 maxlen, HF_UINT32 timeout)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_init(void)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HFI_network_shutdown (HF_VOID)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_configLoad(const HF_CHAR *filename)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_setProperty(const HF_CHAR *key, const HF_CHAR *value)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_getProperty(const HF_CHAR *key, HF_CHAR *buffer, HF_INT32 buffSz)
{
    return HF_SUCCESS;
}

HF_API_DEPRECATED_DO_NOT_IMPLEMENT HF_INT32 HF_network_isAvailable(HF_VOID)
{
    return HF_SUCCESS;
}

//-----------------------------------------------------------------------------------

RF_INT32 RF_ETHERNET_config(RF_ETHERNET_CONFIG_INFO* config)
{
    return HF_SUCCESS;
}

RF_INT32 RF_ETHERNET_up(RF_VOID)
{
    return HF_SUCCESS;
}

RF_INT32 RF_ETHERNET_down(RF_VOID)
{
    return HF_SUCCESS;
}

RF_INT32 RF_ETHERNET_status(RF_ETHERNET_STATUS_INFO* p_item)
{
    return HF_SUCCESS;
}
