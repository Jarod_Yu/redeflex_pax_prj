#include "HF_API.h"
#include "osal.h"
#include <pthread.h>
#include <linux/stddef.h>
#include <signal.h>
#include <semaphore.h>
#include <errno.h>

#if 1
/*
 * REDE informed us that is only necessary to return RF_SUCCESS
 * because these functions are deprecated and no longer used
 */
HF_INT32 HF_thread_semaphoreCreate(HF_INT32 *sem)
{
    return HF_SUCCESS;
}

HF_INT32 HF_thread_semaphoreWait(HF_INT32 *hSemaHnd)
{
    return HF_SUCCESS;
}

HF_INT32 HF_thread_semaphorePost(HF_INT32 *hSemaHnd)
{
    return HF_SUCCESS;
}

#else
HF_INT32 HF_thread_semaphoreCreate(HF_INT32 *sem)
{
	int iRet = -1;

	if(NULL == sem)
	{
		return RF_ERR_INVALIDPARAM;
	}

	iRet = sem_init((sem_t*)sem , 0, 0);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread_semaphoreCreate");
	OsLog(LOG_INFO, "thread_semaphoreCreate fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}

//TODO wait rede's answer, whether we should add this function or not.
HF_INT32 HF_thread_semaphoreDestroy(HF_INT32 *sem)
{
	int iRet = -1;

	if(NULL == sem)
	{
		return RF_ERR_INVALIDPARAM;
	}

	iRet = sem_destroy((sem_t*)sem);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread_semaphoreDestroy");
	OsLog(LOG_INFO, "thread_semaphoreDestroy fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}

HF_INT32 HF_thread_semaphoreWait(HF_INT32 *hSemaHnd)
{
	int iRet = -1;

	if(NULL == hSemaHnd)
	{
		return RF_ERR_INVALIDPARAM;
	}

	iRet = sem_wait((sem_t *)hSemaHnd);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread_semaphoreWait");
	OsLog(LOG_INFO, "thread_semaphoreWait fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}

HF_INT32 HF_thread_semaphorePost(HF_INT32 *hSemaHnd)
{
	int iRet = -1;

	if(NULL == hSemaHnd)
	{
		return RF_ERR_INVALIDPARAM;
	}

	iRet = sem_post((sem_t *)hSemaHnd);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread_semaphorePost");
	OsLog(LOG_INFO, "thread_semaphorePost fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}
#endif

void POS_Sleep(Uint32 uiDelayMs)
{
	OsSleep(uiDelayMs);
}

HF_INT32 HF_thread_iThreadStart(HF_INT32 *threadHnd, void(*threadMainFunc)(void))
{
	int iRet = -1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_thread_iThreadStart");

	if((NULL == threadHnd) || (NULL == threadMainFunc))
	{
		return RF_ERR_INVALIDPARAM;
	}

	iRet = pthread_create((pthread_t*)threadHnd, NULL, (void *)threadMainFunc, NULL);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread Start");
	OsLog(LOG_INFO, "thread start fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}

HF_INT32 HF_thread_iThreadEnd(HF_INT32 threadHnd)
{
	int iRet = -1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_thread_iThreadStart");

	iRet = pthread_join((pthread_t)threadHnd,NULL);

	if(0 == iRet)
	{
		return RF_SUCCESS;
	}

	OsLogSetTag("thread end");
	OsLog(LOG_INFO, "thread end fail [errno=%d %s] \n", errno,strerror(errno));

	return iRet;
}
