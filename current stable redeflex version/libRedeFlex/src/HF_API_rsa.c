#include "HF_API.h"

HF_INT32 HF_rsa_loadkeyH(const HF_CHAR *filename, HF_RSA_KEY **key)
{
    HF_UINT32 size;
    HF_INT32 ret = HF_SUCCESS;
    HF_FILE_T *file;

    if ( (NULL == filename) || (NULL == key) )
    {
        return HF_ERR_INVALIDARG;
    }

    ret = HF_fs_getfsize(filename, &size);
    if (ret != HF_SUCCESS)
    {
        return ret;
    }
    ret = HF_fs_fopenH(filename, "r", &file);
    if (!file || HF_SUCCESS!=ret)
    {
        return ret;
    }
    *key = HF_malloc(size + 1);
    HF_CHAR *key_base64 = (HF_CHAR*) *key;
    ret = HF_fs_fread(file, key_base64, size);
    if (ret != size)
    {
        return HF_ERR_DEVICEFAULT;
    }
    HF_fs_fclose(file);
    key_base64[size] = 0;
    if (key_base64[size - 1] == '\n')
    {
        key_base64[size - 1] = 0;
    }
    if (HF_rsa_isPublic(key_base64) != HF_TRUE
            && HF_rsa_isPrivate(key_base64) != HF_TRUE)
    {
        return HF_ERR_INVALIDRSAKEY;
    }
    return HF_SUCCESS;
}

HF_BOOL HF_rsa_isPublic(const HF_RSA_KEY *key)
{
    HF_INT8 *key_data;
    HF_INT32 key_size;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    HF_BOOL result = HF_FALSE;
    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return result;
}

HF_BOOL HF_rsa_isPrivate(const HF_RSA_KEY *key)
{
    HF_INT8 *key_data=NULL;
    HF_INT32 key_size=0;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    HF_BOOL result=HF_FALSE;

    if (NULL == key)
    {
        return HF_FALSE;
    }

    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return result;
}

HF_INT32 HF_rsa_disposekey(HF_RSA_KEY *key)
{
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    memset(key_base64, 0, strlen(key_base64));
    HF_free(key);
    return HF_SUCCESS;
}

HF_INT32 HF_rsa_encryptH(const HF_RSA_KEY *key, const HF_INT8 *data,
        HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz)
{
    if (dataSz < 1)
    {
        return HF_ERR_INVALIDARG;
    }
    HF_INT8 *key_data;
    HF_INT32 key_size;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    if (HF_rsa_isPublic(key_base64) != HF_TRUE)
    {
        return HF_ERR_NOTAPUBLICKEY;
    }
    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return ret;
}

HF_INT32 HF_rsa_decryptH(const HF_RSA_KEY *key, const HF_INT8 *data,
        HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz)
{
    if (dataSz < 1)
    {
        return HF_ERR_INVALIDARG;
    }
    HF_INT8 *key_data;
    HF_INT32 key_size;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    if (HF_rsa_isPrivate(key_base64) != HF_TRUE)
    {
        return HF_ERR_NOTAPRIVATEKEY;
    }
    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return ret;
}

HF_INT32 HF_rsa_signH(const HF_INT8 *hash, HF_INT32 hashlen,
        HF_INT8 **signature, HF_INT32 *siglen, const HF_RSA_KEY *key)
{
    if (hashlen < 1)
    {
        return HF_ERR_INVALIDARG;
    }
    HF_INT8 *key_data;
    HF_INT32 key_size;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    if (HF_rsa_isPrivate(key_base64) != HF_TRUE)
    {
        return HF_ERR_NOTAPRIVATEKEY;
    }
    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return ret;
}

HF_INT32 HF_rsa_verify(const HF_INT8 *hash, HF_INT32 hashlen,
        const HF_INT8 *signature, HF_INT32 siglen, const HF_RSA_KEY *key)
{
    if (hashlen < 1)
    {
        return HF_ERR_INVALIDARG;
    }
    if (siglen < 1)
    {
        return HF_ERR_INVALIDARG;
    }
    HF_INT8 *key_data;
    HF_INT32 key_size;
    HF_CHAR *key_base64 = (HF_CHAR*) key;
    if (HF_rsa_isPublic(key_base64) != HF_TRUE)
    {
        return HF_ERR_NOTAPUBLICKEY;
    }
    HF_INT32 ret = HF_base64_decodeH(key_base64, strlen(key_base64), &key_data,
            &key_size);
    if (ret != HF_SUCCESS)
    {
        return HF_FALSE;
    }
    // ---------------------------------
    // TODO
    // ---------------------------------
    HF_free(key_data);
    return ret;
}
