/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_keyboard.h
Descrição: Cabeçalho que exporta as funções do teclado.
Autor    : Jorge
Data     : 17/10/2007
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_KEYBOARD_H_
#define _HF_KEYBOARD_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/* Define mantido para manter compatibilidade */ 
#define HF_KEYS_MAX 20

#define HF_MIN_API_KEY_VALUE 1
#define HF_MAX_API_KEY_VALUE 22

#define HF_KEY_ZERO    1
#define HF_KEY_ONE     2
#define HF_KEY_TWO     3
#define HF_KEY_THREE   4
#define HF_KEY_FOUR    5
#define HF_KEY_FIVE    6
#define HF_KEY_SIX     7
#define HF_KEY_SEVEN   8
#define HF_KEY_EIGHT   9
#define HF_KEY_NINE   10
#define HF_KEY_ENTER  11
#define HF_KEY_CANCEL 12
#define HF_KEY_HASH   13
#define HF_KEY_CLEAR  14
#define HF_KEY_STAR   15
#define HF_KEY_DOWN   16
#define HF_KEY_UP     17
#define HF_KEY_MENU   18
#define HF_KEY_LEFT   19
#define HF_KEY_RIGHT  20
#define HF_KEY_F1     21
#define HF_KEY_F2     22
#define HF_KEY_F3     19 //ICT/IWL
#define HF_KEY_F4     20 //ICT/IWL

/* Buzzer Frequency */
#if defined (__TELIUM2__)
#define HF_BUZZER_CLICK     2
#define HF_BUZZER_SHORT     7
#define HF_BUZZER_LONG      20
#define HF_BUZZER_LOW       (100 * 57)
#define HF_BUZZER_MIDTONE   (150 * 57)
#define HF_BUZZER_HIGH      (200 * 57)
#define HF_BUZZER_DEFAULT   HF_BUZZER_MIDTONE
#elif defined (WIN32)
#define HF_BUZZER_CLICK     80      // ms
#define HF_BUZZER_SHORT     700
#define HF_BUZZER_LONG      2000
#define HF_BUZZER_LOW       500
#define HF_BUZZER_MIDTONE   2000
#define HF_BUZZER_HIGH      7000
#define HF_BUZZER_DEFAULT   2600 
#else
#define HF_BUZZER_CLICK     200     // ms
#define HF_BUZZER_SHORT     700
#define HF_BUZZER_LONG      2000
#define HF_BUZZER_LOW       54      //1245
#define HF_BUZZER_MIDTONE   62      //1245
#define HF_BUZZER_HIGH      70      //1245
#define HF_BUZZER_DEFAULT   67      //2600
#endif


/*
 * provide by Brazil colleagues.
 */
typedef struct {
    HF_UINT32 key_api_code;
    HF_CHAR *label;
} RF_KEYBOARD_CODES_T;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/***************************************************************************************************************
 * Description:
 * Returns the label of a button from its value (key).
 * The result points to a static memory location and do not attempt to deallocate it.
 * Input Parameter:
 * the key value of Redeflex CA
 * Output Parameter:nothing
 * Return:
 * The label button - If the function successfully executed
 * or RF_NULL pointer on failure. In the latter case, it is assigned to the global variable RF_errno of the following values:
 * RF_ERR_KEYNOTSUPPORTED – if the key code is not supported
 ***************************************************************************************************************/
const HF_CHAR *HF_keyboard_getkeylabel(HF_UINT32 key);

/***************************************************************************************************************
 * Description:
 * Wait and returns the key code pressed on user keyboard or an error code (which may indicate timeout).
 * The returned key code is a mapped value for Redeflex CA (one of those set at the beginning of this section).
 * Input Parameter:
 * maximum waiting time for the key in seconds. A negative value indicates an indefinite waiting.
 * Output Parameter:nothing
 * Return:
 * The key code pressed on success (a non-negative value). - If the function successfully executed
 * RF_ERR_RESOURCEALLOC - if the shared resource could not meet the request (ex .: was unable to allocate a timer)
 * RF_ERR_TIMEOUT - if you have spent the time set by the 'timeout' parameter and no key has been pressed
 * RF_ERR_DEVICEFAULT - if there was a fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_keyboard_getkeystroke(HF_INT32 timeout);

/***************************************************************************************************************
 * Description:
 * This function has the same behavior of RF_keyboard_getkeystroke function, with the only difference that
 * it ignores keys that have been pressed by the user before the function call.
 * That is, it uses a previously keystrokes buffer.
 * Input Parameter:
 * maximum waiting time for the key in seconds. A negative value indicates an indefinite waiting.
 * Output Parameter:nothing
 * Return:
 * The key code pressed on success (a non-negative value). - If the function successfully executed
 * RF_ERR_RESOURCEALLOC - if the shared resource could not meet the request (ex .: was unable to allocate a timer)
 * RF_ERR_TIMEOUT - if you have spent the time set by the 'timeout' parameter and no key has been pressed
 * RF_ERR_DEVICEFAULT - if there was a fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_keyboard_getkeystroke_nobuffer(HF_INT32 timeout);

/***************************************************************************************************************
 * Description:
 * Activates the sounder (buzzer). The sound is not associated with any particular key,
 * no need to press any key to the sounder is activated. This function is asynchronous.
 * Input Parameter:
 * length - the time, in milliseconds, of the sound duration. One can use one of RF_BUZZER_CLICK macros
 * (200 milliseconds),RF_BUZZER_SHORT (700 milliseconds) or RF_BUZZER_LONG (2000 milliseconds).
 * frequency - frequency of sound (in Hertz). You can tell the value directly or use macros: RF_BUZZER_LOW,
 * RF_BUZZER_MIDTONE, RF_BUZZER_HIGH or RF_BUZZER_DEFAULT.To directly inform the value, remember that the
 * human ear only hears sounds between 20Hz and 20,000Hz. If the platform does not support the last argument
 * frequently as it will play the nearest frequency supported by the platform.
 * Output Parameter:nothing
 * Return:
 * RF_SUCCESS - if the function executed successfully
 * RF_ERR_RESOURCEALLOC - if the shared resource could not meet the request
 * RF_ERR_DEVICEFAULT - if there was a fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_keyboard_buzzer(HF_UINT32 length, HF_UINT32 frequency);

/***************************************************************************************************************
 * Description:
 * Enable / disable the beep after the keystroke.
 * Input Parameter:
 * value - if RF_TRUE activates the 'beep' after pressing any key. Otherwise disables the beep.
 * Output Parameter:nothing
 * Return:
 * RF_SUCCESS - if the function executed successfully
 * RF_ERR_DEVICEFAULT - if there was a fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_keyboard_setBeepOn(HF_BOOL value);

/***************************************************************************************************************
 * Description:
 * Tells whether the beep after the keystroke is active
 * Input Parameter:nothing
 * Output Parameter:nothing
 * Return:
 * RF_TRUE - beep activated
 * RF_FALSE - beep disabled
 ***************************************************************************************************************/
HF_BOOL  HF_keyboard_getBeepOn(HF_VOID);

/***************************************************************************************************************
 * Description:
 * Returns the key map
 * Input Parameter:nothing
 * Output Parameter:nothing
 * Return:
 * Structure with the key map to the device
 ***************************************************************************************************************/
RF_KEYBOARD_CODES_T *RF_keyboard_getkeyset(void);

/***************************************************************************************************************
 * The below USB functions was created to support a keyboard connection to the USB port.
 * As the D200 doesn't have this option, we don't implement these functions.
 ***************************************************************************************************************/
HF_INT32 HF_keyboard_usb_open(HF_CHAR *keymapstr);
HF_INT32 HF_keyboard_usb_close(HF_VOID);
HF_INT32 HF_keyboard_usb_getkeystroke(HF_INT32 timeout);
HF_INT32 HF_keyboard_univ_getkeystroke(HF_INT32 timeout);

#endif
