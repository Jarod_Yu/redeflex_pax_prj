/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_util.h
Descrição: Cabeçalho
Autor    : Guilhreme
Data     : 25/07/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex

Autor    : Anderson Neves
Data     : 19/05/2015
Empresa  : C.E.S.A.R
Descrição: Declaração das funções HFI_isStringNumber, HF_util_ipStringToBigEndian
           e HF_util_ipBigEndianToString
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_UTIL_H_
#define _HF_UTIL_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_INT32 HF_util_asciinToBcd(const HF_CHAR* pInput, HF_INT32 dataSz, HF_CHAR* pOutput, HF_INT32 bcdBuffSize);

HF_INT32 HF_util_asciiToBcd(const HF_CHAR* pInput, HF_CHAR* pOutput, HF_INT32 bcdBuffSize);

HF_INT32 HF_util_bcdnToAscii(const HF_CHAR* pInput, HF_INT32 pInputBcdSz, HF_CHAR* output, HF_INT32 pOutputSz);

HF_BOOL HFI_isNumber(HF_CHAR ch);

HF_BOOL HFI_isStringNumber(HF_CHAR* str);

HF_BOOL HFI_isLetter(HF_CHAR ch);

HF_BOOL HFI_isSpecial(HF_CHAR ch);

HF_BOOL HFI_isPadding(HF_CHAR ch);

HF_BOOL HFI_isANSIISOBCDFormat(HF_CHAR ch);

HF_UINT32 HF_util_strftime(HF_CHAR *s, HF_UINT32 maxsize, const HF_CHAR *fmt, const HF_TM_T *tt);

HF_INT32 HF_util_asciinToHex(const HF_CHAR* pInput, HF_INT32 dataSz, HF_CHAR* pOutput, HF_INT32* bcdBuffSize);

HF_INT32 HF_util_asciiToHex(const HF_CHAR* pInput, HF_CHAR* pOutput, HF_INT32 bcdBuffSize);

HF_INT32 HF_util_ipStringToBigEndian(const HF_CHAR* pInput, HF_UINT32* pOutput);

HF_INT32 HF_util_ipBigEndianToString(const HF_UINT32* pInput, HF_CHAR* pOutput);

#endif

