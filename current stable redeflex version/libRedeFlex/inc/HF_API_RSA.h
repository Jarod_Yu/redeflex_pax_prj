/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_RSA.h
Descrição: Cabeçalho que exporta as funções de RSA.
Autor    : Itapaje
Data     : 04/04/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_RSA_H_
#define _HF_RSA_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef HF_VOID HF_RSA_KEY;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/***************************************************************************************************************
 * Description: Load a RSA key from the file. When it is not needed, the resources reserved for that key must be deallocated by HF_rsa_disposekey.
 * Input Parameter:     filename - The name of the file that contains the RSA key
 *                                    key - Pointer to the pointer which is referencing the RSA key to be loaded
 *  Output Parameter: NO output parameter
 * Return:                      RF_SUCCESS - If the function successfully executed
 *                                    RF_ERR_INVALIDRSAKEY - If the file does not contain a RSA key in valid format
 *                                    RF_ERR_NOTFOUND - If the file does not exist
 *                                    RF_ERR_MAXLEN - If the path of the file larger than the allowed
 *                                    RF_ERR_ISOPEN - If the file is already opened
 *                                    RF_ERR_OPENFILES - If the maximum number of opened files achieved
 *                                    RF_ERR_HDRCORRUPT - If the file header is corrupted
 *                                    RF_ERR_PATHERR - If the file name contains an invalid character sequence
 *                                    RF_ERR_NOMEMORY - If there is no more free memory in the system
 *                                    RF_ERR_DEVICEFAULT - If there is any fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_rsa_loadkeyH(const HF_CHAR *filename, HF_RSA_KEY **key);

/***************************************************************************************************************
 * Description: Checks if a RSA key is a public key.
 * Input Parameter:     key - Pointer to the RSA key
 *  Output Parameter: NO output parameter
 * Return:                      RF_TRUE - If the RSA key is a public key
 *                                    RF_FALSE - If the RSA key is a private key (or invalid)
 ***************************************************************************************************************/
HF_BOOL HF_rsa_isPublic(const HF_RSA_KEY *key);

/***************************************************************************************************************
 * Description: Checks if a RSA key is a private key.
 * Input Parameter:     key - Pointer to the RSA key
 *  Output Parameter: NO output parameter
 * Return:                      RF_TRUE - If the RSA key is a private key
 *                                    RF_FALSE - If the RSA key is a public key (or invalid)
 ***************************************************************************************************************/
HF_BOOL HF_rsa_isPrivate(const HF_RSA_KEY *key);

/***************************************************************************************************************
 * Description: Deallocate the reserved memory for the RSA key.
 * Input Parameter:     key - Pointer to the key to be deallocated
 *  Output Parameter: NO output parameter
 * Return:                      RF_SUCCESS - If the function successfully executed
 ***************************************************************************************************************/
HF_INT32 HF_rsa_disposekey(HF_RSA_KEY *key);


/***************************************************************************************************************
 * Description: It encrypts the data using the public key loaded by HF_rsa_loadkeyH.
 *                       The function allocates memory needed and returns in result, which must be deallocated using RF_free.
 * Input Parameter:     key - The memory pointer to the public key
 *                                   data - Pointer to the memory for the data to be encrypted
 *                                   dataSz - The length of the data
 *  Output Parameter: result - Pointer to the pointer where the encrypted data will be stored
 *                                    resultSz - The length of the result
 * Return:                      RF_SUCCESS - If the function successfully executed
 *                                    RF_ERR_NOTAPUBLICKEY - If the key passed in is a private key
 *                                    RF_ERR_INVALIDRSAKEY - If the key passed in is not valid (e.g. maximum size exceeded)
 *                                    RF_ERR_DEVICEFAULT - If there is any fault in the device
 *                                    RF_ERR_NOMEMORY - If there is no more free memory in the system
 ***************************************************************************************************************/
HF_INT32 HF_rsa_encryptH(const HF_RSA_KEY *key, const HF_INT8 *data, HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz);

/***************************************************************************************************************
 * Description: It decrypts the data using the private key loaded by HF_rsa_loadkeyH.
 *                       The function allocates memory needed and returns in result, which must be deallocated using RF_free.
 * Input Parameter:     key - The memory pointer to the private key
 *                                   data - Pointer to the memory for the data to be decrypted
 *                                   dataSz - The length of the data
 *  Output Parameter: result - Pointer to the pointer where the decrypted data will be stored
 *                                    resultSz - The length of the result
 * Return:                      RF_SUCCESS - If the function successfully executed
 *                                    RF_ERR_NOTAPRIVATEKEY - If the key passed in is a public key
 *                                    RF_ERR_INVALIDRSAKEY - If the key passed in is not valid (e.g. maximum size exceeded)
 *                                    RF_ERR_INVALIDPAD - If the padding of the encrypted data was not encoded properly
 *                                    RF_ERR_DEVICEFAULT - If there is any fault in the device
 *                                    RF_ERR_NOMEMORY - If there is no more free memory in the system
 ***************************************************************************************************************/
HF_INT32 HF_rsa_decryptH(const HF_RSA_KEY *key, const HF_INT8 *data, HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz);

/***************************************************************************************************************
 * Description: Sign the hash using a private key. In general , the parameter is a hash code (SHA1 or MD5), but can also be another type of data (e.g. a 3DES key).
 *                       To generate the SHA1 hash, see HF_SHA1_hash.
 *                       The function allocate memory needed and returns in the signature, which must be deallocated using RF_free.
 * Input Parameter:     hash - The data (typically a hash value) to be signed
 *                                   hashlen - Then length of data to be signed
 *                                   dataSz - The length of the data
 *                                   key - The key used to generate the signature
 *  Output Parameter: signature - Pointer to the pointer. This parameter will receive the address of the allocated memory where the signature will be stored
 *                                    siglen - The length of the resulting signature
 * Return:                      RF_SUCCESS - If the function successfully executed
 *                                    RF_ERR_NOTAPRIVATEKEY - If the key passed in is a public key
 *                                    RF_ERR_INVALIDRSAKEY - If the key passed in is not valid (e.g. maximum size exceeded)
 *                                    RF_ERR_DEVICEFAULT - If there is any fault in the device
 ***************************************************************************************************************/
HF_INT32 HF_rsa_signH(const HF_INT8 *hash, HF_INT32 hashlen, HF_INT8 **signature, HF_INT32 *siglen, const HF_RSA_KEY *key);

/***************************************************************************************************************
 * Description: It decrypts the data using the private key loaded by HF_rsa_loadkeyH.
 *                       The function allocates memory needed and returns in result, which must be deallocated using RF_free.
 * Input Parameter:     key - The memory pointer to the private key
 *                                   data - Pointer to the memory for the data to be decrypted
 *                                   dataSz - The length of the data
 *  Output Parameter: result - Pointer to the pointer where the decrypted data will be stored
 *                                    resultSz - The length of the result
 * Return:                      RF_SUCCESS - If the function successfully executed
 *                                    RF_ERR_NOTAPUBLICKEY - If the key passed in is a private key
 *                                    RF_ERR_INVALSIGNATURE - If the signature is invalid
 *                                    RF_ERR_INVALIDPAD - If the padding of the signature was not encoded properly
 *                                    RF_ERR_INVALIDRSAKEY - If the key passed in is not valid (e.g. maximum size exceeded)
 *                                    RF_ERR_INVALIDPAD - If the padding of the encrypted data was not encoded properly
 *                                    RF_ERR_DEVICEFAULT - If there is any fault in the device
 *                                    RF_ERR_NOMEMORY - If there is no more free memory in the system
 ***************************************************************************************************************/
HF_INT32 HF_rsa_verify(const HF_INT8 *hash, HF_INT32 hashlen, const HF_INT8 *signature, HF_INT32 siglen, const HF_RSA_KEY *key);

#endif

