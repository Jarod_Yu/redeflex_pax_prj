/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_management.h
Descrição: Cabeçalho que exporta as funções de Gerenciamento de Processos
Autor    : Guilherme
Data     : 31/07/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_MANAGEMENT_H_
#define _HF_MANAGEMENT_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/
#define HF_TIMER_INFINITE -1
/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef struct {
	HF_INT64 startTime;
	HF_INT64 count;
} HF_TIMER_T;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/
//API function
/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/11
Name:					RF_sleep
Description:			Suspends the current thread for the time specified in the parameter. The parameter must be specified in terms of milliseconds.
Parameters:				delay - the amount of milliseconds in which the current thread should be suspended

Returns:				RF_SUCCESS - if the function executed successfully
						RF_ERR_RESOURCEALLOC - if the shared resource can not meet the request (ex .: was not possible to allocate a timer)

Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
/***************************************************************************************************************
 * Description:         Suspends the current thread for the time specified in the parameter. The parameter must be specified in terms of milliseconds.
 * Input Parameter:     delay - the amount of milliseconds in which the current thread should be suspended
 * Output Parameter:
 * Return:				RF_SUCCESS - if the function executed successfully
						RF_ERR_RESOURCEALLOC - if the shared resource can not meet the request (ex .: was not possible to allocate a timer)
 * // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_sleep(HF_UINT32 delay);

/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/11
Name:					RF_timer_start
Description:			Starts a timer lasting count milliseconds.
Parameters:				timer - RF_TIMER_T pointer to a structure that is the handle of this timer;
						count - the number of milliseconds to the timer duration. RF_TIMER_INFINITE can be passed as an argument that will cause the RF_timer_remaining function always returns 1000 (1 second).

Returns:				RF_SUCCESS - if the function executed successfully

Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
/***************************************************************************************************************
 * Description:         Starts a timer lasting count milliseconds.
 * Input Parameter:     timer - RF_TIMER_T pointer to a structure that is the handle of this timer;						count - the number of milliseconds to the timer duration. RF_TIMER_INFINITE can be passed as an argument that will cause the RF_timer_remaining function always returns 1000 (1 second).
 * Output Parameter:
 * Return:				RF_SUCCESS - if the function executed successfully
 * // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_timer_start(HF_TIMER_T *timer, HF_INT64 count);
//RF_INT32 RF_timer_start (RF_TIMER_T * timer, RF_INT64 count);

/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/11
Name:					RF_timer_remaining
Description:			It returns the remaining time of the timer.
Parameters:				timer - RF_TIMER_T pointer to a structure that is the handle of the timer.
Returns:				the time remaining on the timer, in milliseconds. If the timer has been set up with the constant RF_TIMER_INFINITE, then the function will always return in 1000.
						RF_ERR_TIMEREXPIRED if the timer has expired.
Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
/***************************************************************************************************************
 * Description:         It returns the remaining time of the timer.
 * Input Parameter:     timer - RF_TIMER_T pointer to a structure that is the handle of the timer.
 * Output Parameter:
 * Return:				the time remaining on the timer, in milliseconds. If the timer has been set up with the constant RF_TIMER_INFINITE, then the function will always return in 1000.
						RF_ERR_TIMEREXPIRED if the timer has expired.
 * // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT64 HF_timer_remaining(HF_TIMER_T *timer);
//RF_INT64 RF_timer_remaining (const RF_TIMER_T * timer);

/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/11
Name:					RF_timer_elapsed
Description:			It returns the time that has passed since the creation of the timer.
Parameters:				timer - RF_TIMER_T pointer to a structure that is the handle of the timer.

Returns:				the time that has passed since the creation of the timer (in milliseconds).

Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
/***************************************************************************************************************
 * Description:         It returns the time that has passed since the creation of the timer.
 * Input Parameter:     timer - RF_TIMER_T pointer to a structure that is the handle of the timer.
 * Output Parameter:
 * Return:				the time that has passed since the creation of the timer (in milliseconds).
 * // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT64 HF_timer_elapsed(HF_TIMER_T *timer);
//RF_INT64 RF_timer_elapsed (const RF_TIMER_T * timer);

#endif

