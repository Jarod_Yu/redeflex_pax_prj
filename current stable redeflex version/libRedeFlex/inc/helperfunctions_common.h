/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : helperfunctions_common.h
Descrição: HelperFunctions comuns Ã s plataformas.
Autor    : Guilherme
Data     : 15/12/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef __HELPERFUNCTIONS_COMMON_H__
#define __HELPERFUNCTIONS_COMMON_H__

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/
#ifndef __cplusplus

#define STATIC_CAST( T, E )  ( (T) (E) )
#define CONST_CAST( T, E )  ( (T) (E) )
#define REINTERPRET_CAST( T, E )  ( (T) (E) )
#define DYNAMIC_CAST( T, E ) ( (T) (E) )

#else

#define STATIC_CAST( T, E ) (static_cast<T>( (E) ) )
#define CONST_CAST( T, E )  (const_cast<T>( (E) ) )
#define REINTERPRET_CAST( T, E ) (reinterpret_cast<T>( (E) ) )
#define DYNAMIC_CAST( T, E ) (dynamic_cast<T>( (E) ) )

#endif

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                                                                               */
/*  Nome: HF_rand                                                                                */
/*                                                                                               */
/*  Descricao:                                                                                   */
/*         Retorna um número aleatório.                                                          */
/*                                                                                               */
/*  Argumentos:                                                                                  */
/*         (sem argumentos)                                                                      */
/*                                                                                               */
/*  Retornos:                                                                                    */
/*         Retorna um número aleatório.                                                          */
/*                                                                                               */
/*-----------------------------------------------------------------------------------------------*/
HF_INT32 HF_rand(HF_VOID);


RF_VOID* HF_malloc(RF_SIZE_T size);
RF_VOID  HF_free(RF_VOID* ptr);

RF_VOID* HF_calloc(RF_SIZE_T num,RF_SIZE_T size);
RF_VOID* HF_realloc(RF_VOID* ptr,RF_SIZE_T size);

#endif
