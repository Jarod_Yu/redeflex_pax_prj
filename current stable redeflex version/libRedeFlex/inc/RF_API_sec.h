/*-----------------------------------------------------------------------------------------------*/
/* Nome do arquivo:                 RF_API_sec .h                                                */
/* Tres Letras Representativas:     API                                                          */
/* Descricao:                       Cabeçalho que exporta as funções de chave.                   */
/*-----------------------------------------------------------------------------------------------*/
/*                            Historico                                                          */
/*-----------------------------------------------------------------------------------------------*/
/*  Nome                 Login         CR        Data         Descricao                          */
/*-----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                   Includes                                                    */
/*-----------------------------------------------------------------------------------------------*/

typedef enum
{
 RF_SEC_KEY_TYPE_3DES_PIN = 1,
 RF_SEC_KEY_TYPE_3DES_DATA,
 RF_SEC_KEY_TYPE_DUKPT_PIN,
 RF_SEC_KEY_TYPE_DUKPT_DATA,

} RF_SEC_KEY_TYPE;

RF_INT32 RF_SEC_store_key(RF_SEC_KEY_TYPE type, RF_UINT16 keyIdx, const RF_UINT8 *p_key, RF_UINT32 key_sz, const RF_UINT8 *p_ksn, RF_UINT32 ksn_sz);
