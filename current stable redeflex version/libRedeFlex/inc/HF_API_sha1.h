/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_sha1.h
Descrição: Cabeçalho que exporta as funções de SHA-1.
Autor    : GUilherme
Data     : 28/07/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_SHA1_H_
#define _HF_SHA1_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_SHA1_DIGEST_SIZE 20

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/***************************************************************************************************************
 * Description: Generates a cryptographic SHA-1 hash of the data. The result is always 20 bytes.
 * Input Parameter:		data - Pointer to the data for which the hash will be created
 *                                     length - The size of the data for which the hash will be created
 *	Output Parameter:   result - Pointer to where the hash is stored. The result is always 20 bytes
 * Return:                       RF_SUCCESS - If the function successfully executed
 *                                     RF_ERR_NOMEMORY - If there is no more free memory in the system
 *                                     RF_ERR_DEVICEFAULT - If an unknown problem has occurred
***************************************************************************************************************/
HF_INT32 HF_sha1_hash(const HF_VOID *data, HF_UINT32 length, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Initializes the incremental calculation of the SHA-1 hash.
 * Input Parameter:       No input parameter
 *  Output Parameter:   No output parameter
 * Return:                       RF_SUCCESS - If the function successfully executed
 *                                     RF_ERR_RESOURCEALLOC - If any incremental calculation of SHA-1 hash is already running
***************************************************************************************************************/
HF_INT32 HF_sha1_chainInit(HF_VOID);

/***************************************************************************************************************
 * Description: Realize the update of the SHA-1 hash calculation in progress.
 * Input Parameter:      data - The block of data that will be submitted to the SHA-1 calculation
 *                                     length - The size of the data block
 *  Output Parameter:   No output parameter
 * Return:                       RF_SUCCESS - If the function successfully executed
 *                                     RF_ERR_INVALIDSTATE - If RF_sha1_chainInit has not been previously called
 *                                     RF_ERR_DEVICEFAULT - Failure in routine internal SHA-1 calculation
***************************************************************************************************************/
HF_INT32 HF_sha1_chainUpdate(const HF_VOID *data, HF_UINT32 length);

/***************************************************************************************************************
 * Description: Returns the result of the incremental calculation of SHA-1 hash.
 * Input Parameter:       No input parameter
 *  Output Parameter:   result - Memory block where the result will be copied (20 bytes in size)
 * Return:                       RF_SUCCESS - If the function successfully executed
 *                                     RF_ERR_INVALIDSTATE - If RF_sha1_chainInit has not been previously called
 *                                     RF_ERR_DEVICEFAULT - Failure in routine internal SHA-1 calculation
***************************************************************************************************************/
HF_INT32 HF_sha1_chainResult(HF_INT8 *result);

#endif

