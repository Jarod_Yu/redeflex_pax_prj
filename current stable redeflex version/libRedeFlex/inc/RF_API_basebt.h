/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : RF_API_basebt.h
Descri��o: Cabe�alho que exporta as fun��es de BT
Autor    : Everton Dornelas
Data     : 13/04/2015
Empresa  : C.E.S.A.R
*********************** MODIFICA��ES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descri��o: Inspe��o de c�digo
ID       : 01 Projeto Redeflex

Autor    : Anderson Neves
Data     : 27/05/2015
Empresa  : C.E.S.A.R
Descri��o: Altera��o da numera��o dos defines
           RF_ERR_BASEBT_ASSOCIATION_FAILURE,
           RF_ERR_BASEBT_DISASSOCIATION_FAILURE e
           RF_ERR_BASEBT_DISASSOCIATION_NOT_FOUND
           para que seja apresentado o erro correto no Lua
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _RF_API_basebt_H_
#define _RF_API_basebt_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                   Includes                                                    */
/*-----------------------------------------------------------------------------------------------*/

#define RF_ERR_BASEBT_ASSOCIATION_FAILURE         HF_ERR_BASEBT_ASSOCIATION_FAILURE
#define RF_ERR_BASEBT_DISASSOCIATION_FAILURE      HF_ERR_BASEBT_DISASSOCIATION_FAILURE
#define RF_ERR_BASEBT_DISASSOCIATION_NOT_FOUND    HF_ERR_BASEBT_DISASSOCIATION_NOT_FOUND

typedef enum {
	RF_BASEBT_ASSOCIATION_STATUS_NOT_ASSOCIATED = 1,
	RF_BASEBT_ASSOCIATION_STATUS_ASSOCIATING = 2,
	RF_BASEBT_ASSOCIATION_STATUS_ASSOCIATED = 3,
	RF_BASEBT_ASSOCIATION_STATUS_ERROR = 4
} RF_BASEBT_ASSOCIATION_STATUS;

typedef enum
{
	RF_BASEBT_CONNECTION_STATUS_DISCONNECTED = 1,
	RF_BASEBT_CONNECTION_STATUS_CONNECTED = 2,
	RF_BASEBT_CONNECTION_STATUS_OUT_OF_RANGE = 3,
	RF_BASEBT_CONNECTION_STATUS_LOST_SYNC = 4
} RF_BASEBT_CONNECTION_STATUS;

typedef enum
{
	//Desconhecido
	RF_BASEBT_ERROR_CONNECTION_UNKNOWN = 1,

	//Cabo de rede desconectado
	RF_BASEBT_ERROR_CONNECTION_NETWORK_DISCONNECTED,

	//Base fora do alcance do dispositivo
	RF_BASEBT_ERROR_CONNECTION_OUT_OF_RANGE,

	//Dispositivo nao associado a uma base
	RF_BASEBT_ERROR_CONNECTION_NOT_ASSOCIATED,

	//Erro de sincronismo(ap�s associacao)
	RF_BASEBT_ERROR_CONNECTION_SYNC_FAILURE

} RF_BASEBT_CONNECTION_ERROR;

typedef enum {

	//Desconhecido
	RF_BASEBT_ERROR_ASSOCIATION_UNKNOWN = 1,

	//Problema na conexao fisica entre dispositivo e a base
	RF_BASEBT_ERROR_ASSOCIATION_PHY,

	//Problema de sincronismo no momento de estabelcer a conexao
	RF_BASEBT_ERROR_ASSOCIATION_SYNC,

	//Incompatibilidade de Driver/Firmware
	RF_BASEBT_ERROR_ASSOCIATION_DRIVER

} RF_BASEBT_ASSOCIATION_ERROR;

typedef struct{

	// String terminada em NULL('\0')
	RF_CHAR *serialNumber;

	// String terminada em NULL('\0')
	RF_CHAR *bluetooth_address;

	// String terminada em NULL('\0')
	RF_CHAR *baseFirmwareVersion;

	// String terminada em NULL('\0')
	RF_CHAR *deviceFirmwareVersion;

} RF_BASEBT_ASSOCIATION_INFO;

typedef struct{

	RF_BASEBT_ASSOCIATION_INFO info;

	//Em caso de erro de associacao, contem o tipo do erro
	RF_BASEBT_ASSOCIATION_ERROR errorType;

	//Erro nativo do dispositivo
	RF_INT32 nativeError;

} RF_BASEBT_ASSOCIATION_RESULT;

typedef struct
{
	 RF_BASEBT_ASSOCIATION_STATUS associationStatus;
	 RF_BASEBT_CONNECTION_STATUS connectionStatus;
	 RF_BASEBT_ASSOCIATION_ERROR assocErrorType;
	 RF_BASEBT_CONNECTION_ERROR connErrorType;
	 RF_INT32 assocNativeError;
	 RF_INT32 assocNativeStatus;
	 RF_INT32 connNativeError;
	 RF_INT32 connNativeStatus;

	 //Dados da conexao IP da base
	 RF_IP_NETWORK_INFO ipInfo;

	 //Endereco MAC da Base no formato IEEE 802) ex. 01-23-45-67-89-AB
	 RF_CHAR macAddress[18];

} RF_BASEBT_STATUS_INFO;

typedef struct
{
	RF_UINT16 connect_max_retry;
	RF_UINT32 reconnect_wait_mseg;
} RF_BASEBT_CONFIG_INFO;

RF_INT32 RF_BASEBT_associate (RF_BASEBT_ASSOCIATION_RESULT* p_res);
RF_INT32 RF_BASEBT_disassociate (RF_BASEBT_ASSOCIATION_INFO info);
RF_INT32 RF_BASEBT_config(RF_BASEBT_CONFIG_INFO *config);
RF_INT32 RF_BASEBT_up (RF_VOID);
RF_INT32 RF_BASEBT_down (RF_VOID);
RF_INT32 RF_BASEBT_status (RF_BASEBT_STATUS_INFO* p_info);

#endif
