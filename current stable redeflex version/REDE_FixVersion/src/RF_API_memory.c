#include "HF_API.h"
#include "stdlib.h"

#include <osal.h>
#include <sys/sysinfo.h>

RF_VOID* RF_memory_realloc(RF_VOID* ptr, RF_SIZE_T size)
{
	ptr = realloc(ptr,size);

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_memory_realloc ptr:%p size:%d", ptr, size);

    return ptr;
}

RF_SIZE_T RF_memory_max_available_KB(RF_VOID)
{
	int iRet = 0;
	struct sysinfo sys;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	memset(&sys, 0, sizeof(sysinfo));

	iRet = sysinfo(&sys);
	if(0 != iRet)
	{
		return HF_NULL;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"Max Available:[%d]",(sys.freeram / 1024));

	return (sys.freeram / 1024);
}
