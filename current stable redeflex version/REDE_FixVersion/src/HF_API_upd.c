#include "HF_API.h"
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/reboot.h>
#include <sys/types.h>   
#include <sys/stat.h>

#define	FILE_MAX_NAME_LEN 		50
#define	UPDATE_FILE_NUM 			20
#define	UPDATE_FILE_FOLDER	 	"/data/app/MAINAPP/redeUpdate"
#define	REDE_UNIQUE_PATH	 		"/data/app/MAINAPP/root"
#define	FILE_BIN_NAME 				"update.bin"
#define	FILE_LIST_NAME 				"update.def"
#define	LIB_DAYNA_DIR				"/data/app/MAINAPP/lib"
#define	LIB_STATIC_DIR				"/data/app/MAINAPP/lib_a" 

#define STACK_MAX_DEPTH_EXT  			10
#define HF_FS_MAX_DIR_DEPTH_EXT     	(6 + 1) 
#define HF_FS_MAX_PATH_LENGTH_EXT   	((FILE_MAX_NAME_LEN + 1) * HF_FS_MAX_DIR_DEPTH_EXT)

#define	FILE_TYPE_NOTSUPPORT 		0
#define	FILE_TYPE_APP  				1	
#define	FILE_TYPE_APP_PARAM  		2	
#define	FILE_TYPE_SYS_LIB  			3	
#define	FILE_TYPE_PUB_KEY  		4	
#define	FILE_TYPE_AUP  				5	
#define	FILE_TYPE_APP_LIB			8
#define	FILE_TYPE_FIRMWARE 		9	

extern int OsFirmwareUpgrade(const char *FwFileName);

typedef struct fileList
{
	unsigned char   ucFileType;
	char fileName[FILE_MAX_NAME_LEN];
	int	fileSize;
}fileList;
static fileList gstFileList[UPDATE_FILE_NUM];
static int giSubFileNum = 0;
char  gcAbPath[HF_FS_MAX_PATH_LENGTH_EXT] = {0};

HF_INT32 stackPositionExt = -1;
HF_CHAR path_stackExt[STACK_MAX_DEPTH_EXT][FILE_MAX_NAME_LEN];

static int makeSubFile(const char *filePath);
static int analyzeSubFileType(void);
static int updateSubFile(void);
static int deleteFiles(void);
static int replaceAppLib(int index);

HF_INT32 HF_upd_autocargaHiperflex(const HF_CHAR *caminho)
{
	int  iRet = -1;
	
	OsLogSetTag("autocargaHiperflex");

	if(NULL == caminho)
	{
 		OsLog(LOG_INFO, "\n param err \n");
 		return HF_ERR_INVALIDARG;
 	}
 	
	// 1.get update.def and update.bin path from caminho
	iRet = makeSubFile(caminho);
	if(iRet != HF_SUCCESS)
	{
		OsLog(LOG_INFO, "\n makeSubFile fail \n");
		return iRet;
	}

	// 2. analyze file type
	iRet = analyzeSubFileType();
	if(iRet != HF_SUCCESS)
	{
		OsLog(LOG_INFO, "\n analyzeSubFileType fail \n");
		return iRet;
	}
	
	// 3. according above sub files' type to select PAX relative API to update.
	iRet = updateSubFile();
	if(iRet != HF_SUCCESS)
	{
		OsLog(LOG_INFO, "\n updateSubFile fail \n");
		return iRet;
	}

	// 4. Delete all relative update files. 
	iRet = deleteFiles();
	if(iRet != HF_SUCCESS)
	{
		OsLog(LOG_INFO, "\n deleteFiles fail \n");
		return iRet;
	}
	
	// 5.reboot system
	OsLog(LOG_INFO, "\n updated file success, system reboot \n");
	reboot(RB_AUTOBOOT);
	
 	return HF_SUCCESS;
}

HF_INT32 HF_upd_downloadHiperflex(const HF_UPD_PARAMS* params)
{
	 return HF_SUCCESS;
}

static HF_VOID stack_init()
{
	// improve () by REDE_UNIQUE_PATH 
	stackPositionExt = 3;
	memset(path_stackExt, 0, sizeof(path_stackExt));
	strcpy(path_stackExt[0], "data");
	strcpy(path_stackExt[1], "app");
	strcpy(path_stackExt[2], "MAINAPP");
	strcpy(path_stackExt[3], "root");
}

static HF_VOID stack_clean()
{
	stackPositionExt = -1;
	memset(path_stackExt, 0, sizeof(path_stackExt));
}

static HF_BOOL stack_push(HF_CHAR *dir)
{
	stackPositionExt++;
	if((stackPositionExt >= 0) && (stackPositionExt < STACK_MAX_DEPTH_EXT))
	{
		strcpy(path_stackExt[stackPositionExt], dir);
		return HF_TRUE;
	}
	else
	{
		return HF_FALSE;
	}
}

static HF_CHAR* stack_pop()
{
	if((stackPositionExt < 0) || (stackPositionExt >= STACK_MAX_DEPTH_EXT))
	{
		return NULL;
	}
	else
	{
		return path_stackExt[stackPositionExt--];
	}
}

static HF_BOOL getAbsolutePathExt(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen)
{
	int i = 0;
	HF_CHAR * tok = NULL;
	HF_CHAR * abPath = NULL;
	HF_CHAR temp[HF_FS_MAX_PATH_LENGTH_EXT];

	OsLogSetTag("getAbsolutePathExt");

	stack_init();
	memset(temp, 0 , maxLen);

	if((NULL == filename) || (NULL == output))
	{
		OsLog(LOG_DEBUG," \n param err \n");
		return HF_FALSE;
	}

	if(*filename == '/')
	{
		OsLog(LOG_DEBUG,"\n filename [%s]\n", filename);
		filename++;
	}

	if(strncmp(filename, "data/app/MAINAPP/root", 21) == 0)
	{
		*output++ = '/';
		strcpy(output, filename);
		OsLog(LOG_DEBUG,"\n output [%s] \n", output);
		return HF_TRUE;
	}


	strcpy(temp, filename);
	tok = strtok(temp, "/");
	while (tok != HF_NULL)
	{
		if (strcmp(tok, "..") == 0)
		{
			if (NULL == stack_pop())
			{
				OsLog(LOG_DEBUG,"\nstack_pop NULL\n");
				stack_clean();
				return HF_FALSE;
			}
		}
		else if (strcmp(tok, ".") != 0)
		{
			if(strlen(tok) > FILE_MAX_NAME_LEN)
			{
				OsLog(LOG_DEBUG,"\n strlen(tok) > FILE_MAX_NAME_LEN \n");
				stack_clean();
				return HF_FALSE;
			}

			stack_push(tok);
		}
		tok = strtok(NULL, "/");
	}

	memset(temp, 0 , maxLen);
	abPath = temp;
	for(i=0; i<=stackPositionExt; i++)
	{
		*abPath++ = '/';
		strcpy(abPath, path_stackExt[i]);
		abPath += strlen(path_stackExt[i]);
	}

	strcpy(output, temp);

	OsLog(LOG_DEBUG,"\noutput:%s\n", output);
	
	stack_clean();
	return HF_TRUE;
}

static int checkFileExist(const HF_CHAR *path)
{
	DIR *pDir = NULL;
	struct dirent *pDirEntry = NULL;
	int iFileListFlag = 0, iFileBinFlag = 0;
	
	OsLogSetTag("checkFileExist");
	
	//opendir
	pDir = opendir(path);
	if(pDir == NULL)
	{
		OsLog(LOG_INFO, "open dir %s fail \n", path);
		return HF_ERR_DEVICEFAULT;
	}
	
	//readdir, check whether file update.def & update.bin exisit
	while((pDirEntry = readdir(pDir)) != NULL)
	{

		if(strcmp(pDirEntry->d_name, FILE_LIST_NAME) == 0)
		{
			iFileListFlag = 1;
		}
		else if(strcmp(pDirEntry->d_name, FILE_BIN_NAME) == 0)
		{
			iFileBinFlag = 1;
		}

		if(iFileListFlag && iFileBinFlag)
		{
			closedir(pDir);
			return 1;
		}
	}

	OsLog(LOG_INFO, "\n err iFileListFlag:%d, iFileBinFlag:%d \n", iFileListFlag,iFileBinFlag);
	
	closedir(pDir);
	return 0;
}

static int makeSubFile(const char *filePath)
{
	int i = 0, iRet = -1, iFileIndex = 0;
	FILE *fp = NULL;
	FILE *subFileFp = NULL;
	char ucFileListPath[255] = {0}, ucBinPath[255] = {0};
	int iBufsize = 150;
	char cFileListBuf[128] = {0};
	
	char *pTmp = NULL;
	DIR *pDir = NULL;
	char tmpSubFile[128] = {0};
	unsigned char *subFileBuf =NULL;
	long lTotalSize = 0;
	struct stat fileStat;
	char  abPath[HF_FS_MAX_PATH_LENGTH_EXT] = {0};
	
	OsLogSetTag("makeSubFile");

	if(NULL == filePath)
	{
		return HF_ERR_INVALIDARG;
	}

	//extract filePath.
	if(HF_FALSE == getAbsolutePathExt(filePath, abPath, HF_FS_MAX_PATH_LENGTH_EXT))
	{
		OsLog(LOG_DEBUG,"getAbsolutePathExt err \n");
		return HF_ERR_INVALIDARG;
	}

	OsLog(LOG_INFO, "\n abPath:%s \n", abPath);
	memset(gcAbPath, 0, sizeof(gcAbPath));
	strcpy(gcAbPath, abPath);

	//check update.bin & update.def
	if(1 != checkFileExist(abPath))
	{
		OsLog(LOG_DEBUG,"\n file not exist \n");
		return HF_ERR_INVALIDARG;
	}
	
	strcpy(ucFileListPath,abPath);
	strcpy(ucBinPath,abPath);
	strcat(ucFileListPath, "/");
	strcat(ucBinPath, "/");
	strcat(ucFileListPath, FILE_LIST_NAME);
	strcat(ucBinPath, FILE_BIN_NAME);
	
	// 2. read file name & size from update.def
	fp = fopen(ucFileListPath, "r");
	if(fp == NULL)
	{
		OsLog(LOG_INFO, "open %s fail [errno=%d %s] \n", ucBinPath,errno,strerror(errno));
		return HF_ERR_DEVICEFAULT;
	}

	memset(gstFileList, 0, sizeof(gstFileList));
	giSubFileNum = 0;
	
	while(!feof(fp))
	{
		if(fgets(cFileListBuf, iBufsize, fp) != NULL)
		{
			pTmp = strtok(cFileListBuf, ",");
			if(NULL != pTmp)
			{
				memcpy(gstFileList[iFileIndex].fileName, pTmp, MIN(FILE_MAX_NAME_LEN, strlen(pTmp)));
			}
			else
			{
				OsLog(LOG_INFO, "the %d line fileName blank \n", (iFileIndex+1));
				continue;
			}

			memset(pTmp, 0, sizeof(pTmp));
			pTmp = strtok(NULL, ",");
			if(NULL != pTmp)
			{
				gstFileList[iFileIndex].fileSize = atoi(pTmp);
			}
			else
			{
				OsLog(LOG_INFO, "the %d line fileSize blank \n", (iFileIndex+1));
				continue;
			}
			
			iFileIndex++;
		}
	}

	giSubFileNum = iFileIndex;

#if 1//debug	
	OsLog(LOG_INFO, "total file num:%d \n", iFileIndex);
	for(i=0; i<giSubFileNum; i++)
	{
		OsLog(LOG_INFO, "file[%d] name:%s, size:%d \n", i, gstFileList[i].fileName, gstFileList[i].fileSize);
	}
#endif
	
	fclose(fp);
	fp = NULL;
	
	//sub files' total size must be smaller or equal to default.bin Size
	for(i=0; i<giSubFileNum;i++)
	{
		lTotalSize += gstFileList[i].fileSize;
	}

	if(lTotalSize == 0)
	{
		OsLog(LOG_INFO, "\n sub file size zero \n");
		return HF_ERR_INVALIDARG;
	}

	memset(&fileStat, 0 ,sizeof(fileStat));
	if(stat(ucBinPath, &fileStat) == 0)
	{
		OsLog(LOG_INFO, "lTotalSize:%d,  fileStat.st_size:%d \n", lTotalSize, fileStat.st_size);
		if(lTotalSize >  fileStat.st_size)
		{
			OsLog(LOG_INFO, "\n %s size err \n", ucBinPath);
			return HF_ERR_INVALIDARG;
		}
	}
	else
	{
		OsLog(LOG_INFO, "stat %s fail \n", ucBinPath);
		return HF_ERR_DEVICEFAULT;
	}

	// 3. create folder /data/app/MAINAPP/redeUpdate
	pDir = opendir(UPDATE_FILE_FOLDER);				
	if(pDir == NULL)
	{
		OsLog(LOG_INFO, "\nstart mkdir %s  \n", UPDATE_FILE_FOLDER);
		
		iRet = mkdir(UPDATE_FILE_FOLDER, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH);
		if(iRet == -1)
		{
			OsLog(LOG_INFO, "mkdir %s error \n", UPDATE_FILE_FOLDER);
			return HF_ERR_DEVICEFAULT;
		}
	}
	else
	{
		OsLog(LOG_INFO, "\nclose dir %s  \n", UPDATE_FILE_FOLDER);
		closedir(pDir);
	}

	// 4. create sub files in UPDATE_FILE_FOLDER, and divide update.bin contents into sub files
	fp = fopen(ucBinPath, "rb");
	if(fp == NULL)
	{
		OsLog(LOG_INFO, "open %s fail [errno=%d %s] \n", ucBinPath,errno,strerror(errno));
		return HF_ERR_DEVICEFAULT;
	}

	for(i=0; i<giSubFileNum;i++)
	{
		memset(tmpSubFile, 0, sizeof(tmpSubFile));
		strcpy(tmpSubFile, UPDATE_FILE_FOLDER);
		strcat(tmpSubFile, "/");
		strcat(tmpSubFile, gstFileList[i].fileName);

		subFileFp =  fopen(tmpSubFile, "wb");
		
		if(subFileFp == NULL)
		{
			OsLog(LOG_INFO, "create file %s error \n", tmpSubFile);
			fclose(fp);
			fp = NULL;
			return HF_ERR_DEVICEFAULT;
		}

		if(subFileBuf != NULL)
		{
			free(subFileBuf);
			subFileBuf = NULL;
		}
		subFileBuf = (unsigned char *)malloc(gstFileList[i].fileSize);
		if(subFileBuf == NULL)
		{
			OsLog(LOG_INFO, "malloc error \n");
			fclose(subFileFp);
			subFileFp = NULL;
			fclose(fp);
			fp = NULL;
			return HF_ERR_NOMEMORY;
		}

		memset(subFileBuf, 0, sizeof(subFileBuf));
		iRet = fread(subFileBuf, 1, gstFileList[i].fileSize, fp);
		OsLog(LOG_INFO, "fread iRet:%d subFileBuf:%s \n",iRet,subFileBuf);
		if(iRet > 0)
		{
			if(fwrite(subFileBuf, 1, gstFileList[i].fileSize, subFileFp)<0)
			{
				OsLog(LOG_INFO, "write sub file error \n");
				return HF_ERR_DEVICEFAULT;
			}
		}

		fclose(subFileFp);
		subFileFp = NULL;

		if(subFileBuf != NULL)
		{
			free(subFileBuf);
			subFileBuf = NULL;
		}

		if(feof(fp))
		{
			OsLog(LOG_INFO, "\n file end!\n");
			break;
		}

		if (iRet == 0)
		{
			OsLog(LOG_INFO, "\n file size not enough!\n");
			break;
		}
		else if(iRet < 0)
		{
			OsLog(LOG_INFO, "\n fread err!\n");
			break;
		}	
	}
	
	fclose(fp);
	fp = NULL;
	
 	return HF_SUCCESS;
}

static int analyzeSubFileType(void)
{
	int i = 0;

	OsLogSetTag("analyzeSubFileType");

	for(i = 0; i< giSubFileNum; i++)	
	{
		if (strstr(gstFileList[i].fileName, ".aip")!=NULL)
		{
			gstFileList[i].ucFileType = FILE_TYPE_APP;
		}
		else if (strstr(gstFileList[i].fileName, ".aup")!=NULL)
		{
			gstFileList[i].ucFileType = FILE_TYPE_AUP;
		}
		else if ((strstr(gstFileList[i].fileName, ".a")!= NULL) || (strstr(gstFileList[i].fileName, ".so")!= NULL))
		{
			gstFileList[i].ucFileType = FILE_TYPE_APP_LIB;
		}
		else if (strstr(gstFileList[i].fileName, ".puk")!=NULL)
		{
			gstFileList[i].ucFileType = FILE_TYPE_PUB_KEY;
		}
		else if (strstr(gstFileList[i].fileName, ".zip")!=NULL)
		{
			gstFileList[i].ucFileType = FILE_TYPE_FIRMWARE;
		}
		else//danielle TODO
		{
			OsLog(LOG_INFO, "\n SP: unsupport %s type \n",gstFileList[i].fileName);
			gstFileList[i].ucFileType = FILE_TYPE_NOTSUPPORT;
			//gstFileList[i].ucFileType = FILE_TYPE_APP_PARAM;
			//FILE_TYPE_SYS_LIB;
		}
	}

	return HF_SUCCESS;
}

static int updateSubFile(void)
{
	int i = 0, iRet = -1;
	char name[128] =  {0};
	char fileName[255] = {0};
	
	OsLogSetTag("updateSubFile");

	for(i = 0; i< giSubFileNum; i++)
	{
		memset(fileName, 0, sizeof(fileName));
		
		strcpy(fileName, UPDATE_FILE_FOLDER);
		strcat(fileName, "/");
		strcat(fileName, gstFileList[i].fileName);

		OsLog(LOG_INFO, "\n NewVersion updateFileName:%s ucFileType:0x%x \n",fileName,gstFileList[i].ucFileType);

		if(FILE_TYPE_NOTSUPPORT == gstFileList[i].ucFileType)
		{
			OsLog(LOG_INFO, "\n unsupport %s type \n",gstFileList[i].fileName);
			continue;
		}
		
		if(FILE_TYPE_APP_LIB == gstFileList[i].ucFileType)
		{
			iRet = replaceAppLib(i);
			
			OsLog(LOG_INFO, "\n replaceAppLib iRet:%d \n",iRet);
			
			if(iRet != 0) 
			{
				return iRet;
			}
		}
		else if(FILE_TYPE_FIRMWARE == gstFileList[i].ucFileType)
		{	
			iRet = OsFirmwareUpgrade(fileName);
			if(iRet != 0) 
			{
				return iRet;
			}
		}
		else
		{
			memset(name, 0, sizeof(name));
			
			if(FILE_TYPE_APP_PARAM == gstFileList[i].ucFileType)
			{
				/*
				APPINFO	stAppInfo;
				memset(&stAppInfo,0,sizeof(stAppInfo));
				MIDReadAppInfo(0, &stAppInfo);
				strcpy(name,stAppInfo.AppName); 
				*/
			}
			else if(FILE_TYPE_PUB_KEY == gstFileList[i].ucFileType)
			{
				strcpy(name,"uspuk0");// [uspuk0,uspuk8]
			}

			iRet = OsInstallFile(name, fileName, gstFileList[i].ucFileType);

			OsLog(LOG_INFO, "\n OsInstallFile iRet:%d \n",iRet);
			
			if(iRet != 0) 
			{
				return iRet;
			}
		}
	}

	return HF_SUCCESS;
}

static int deleteFiles(void)
{
	int i = 0, iRet = -1;
	char  cTmpPath[128] = {0};
	
	for(i=0; i<giSubFileNum;i++)
	{
		memset(cTmpPath, 0, sizeof(cTmpPath));
		strcpy(cTmpPath, UPDATE_FILE_FOLDER);
		strcat(cTmpPath, "/");
		strcat(cTmpPath, gstFileList[i].fileName);
		if(remove(cTmpPath) == -1)
		{
			OsLog(LOG_INFO, "\n remove cTmpPath:%s fail, errno:%d , %s \n",cTmpPath, errno,strerror(errno));
			return iRet;
		}
	}

	memset(cTmpPath, 0, sizeof(cTmpPath));
	strcpy(cTmpPath,gcAbPath);
	strcat(cTmpPath, "/");
	strcat(cTmpPath, FILE_BIN_NAME);
	if(remove(cTmpPath) == -1)
	{
		OsLog(LOG_INFO, "\n remove cTmpPath:%s fail, errno:%d , %s \n",cTmpPath, errno,strerror(errno));
		return iRet;
	}	

	memset(cTmpPath, 0, sizeof(cTmpPath));
	strcpy(cTmpPath,gcAbPath);
	strcat(cTmpPath, "/");
	strcat(cTmpPath, FILE_LIST_NAME);
	if(remove(cTmpPath) == -1)
	{
		OsLog(LOG_INFO, "\n remove cTmpPath:%s fail, errno:%d , %s \n",cTmpPath, errno,strerror(errno));
		return iRet;
	}	

	return HF_SUCCESS;
}

static int replaceAppLib(int index)
{
	int i = 0, iRet = -1, iStaticFlag = 0;
	char libHeadList[][50] = {"libredeflex_REDE","libRede_lib",  "libD_EMV_LIB", "lib_PROLIN_D2x0", "libClEntryS2FMProlin", "libClMCS2FMProlin",  "libClWaveS2FMProlin", "libD_WAVE_LIB", "libDevice_Prolin", "libMC_Prolin", "libosal"};//correspoing with array replaceLibName, mustn't be changed . if change, array replaceLibName should also be changed.
	char replaceLibName[][50] = {"libredeflex_REDE.a","libRede_lib.so",  "libD_EMV_LIB_v602_01.so", "lib_PROLIN_D2x0_v103.so", "libClEntryS2FMProlin_v503.so", "libClMCS2FMProlin_debug2.so",  "libClWaveS2FMProlin_v304.so", "libD_WAVE_LIB_v305.so", "libDevice_Prolin_v503.so", "libMC_Prolin_v502.so", "libosal2.so"};
	char tmpDstFile[128] = {0};
	char tmpSrcFile[128] = {0};
	char  tmp[256] = {0}; 
	struct stat fileStat;
	char tmpBkupDstFile[128] = {0};
	
	//danielle TODO Maybe the better way is to read dir /data/app/MAINAPP/lib, or lib_a
	
	OsLogSetTag("replaceAppLib");

	if((index < 0) || (index > giSubFileNum))
	{
 		OsLog(LOG_INFO, "\n param err \n");
 		return HF_ERR_INVALIDARG;
 	}

	while(libHeadList[i] != NULL)
	{
		if(memcmp(gstFileList[index].fileName,libHeadList[i],strlen(libHeadList[i])) == 0)//replace app lib file
		{
			OsLog(LOG_INFO, "\n name:%s \n",gstFileList[index].fileName);
			
			if(strstr(gstFileList[index].fileName, ".a")!= NULL)
			{
				iStaticFlag = 1;
			}
			else
			{
				iStaticFlag = 0;
			}

			OsLog(LOG_INFO, "\n replaceAppLibHead:%s, i:%d, iStaticFlag:%d, index:%d \n",libHeadList[i],i,iStaticFlag,index);
			
			memset(tmpSrcFile,0,sizeof(tmpDstFile));
			strcpy(tmpSrcFile, UPDATE_FILE_FOLDER);
			strcat(tmpSrcFile, "/");
			strcat(tmpSrcFile, gstFileList[index].fileName);

			memset(tmpDstFile,0,sizeof(tmpDstFile));
			strcpy(tmpDstFile,(iStaticFlag ? LIB_STATIC_DIR : LIB_DAYNA_DIR));
			strcat(tmpDstFile,"/");
			strcat(tmpDstFile,replaceLibName[i]);

			OsLog(LOG_INFO, "\n TmpDstFile: %s \n", tmpDstFile);
			OsLog(LOG_INFO, "\n TmpSrcFile: %s \n", tmpSrcFile);

			memset(&fileStat, 0 ,sizeof(fileStat));
			if(stat(tmpDstFile, &fileStat) != 0)
			{
				OsLog(LOG_INFO, "stat %s fail \n", tmpDstFile);
				return HF_ERR_DEVICEFAULT;
			}
			
			OsLog(LOG_INFO, "\n tmpSrcFile:%s, st_mode:0x%x \n",tmpSrcFile,fileStat.st_mode);

			//add start
			OsLog(LOG_INFO, "\n backup Dst file start \n");
			memset(tmpBkupDstFile,0,sizeof(tmpDstFile));
			strcpy(tmpBkupDstFile, (iStaticFlag ? LIB_STATIC_DIR : LIB_DAYNA_DIR));
			strcat(tmpBkupDstFile, "/");
			if(iStaticFlag == 1)
			{
				strcat(tmpBkupDstFile, "test.a");
			}
			else
			{
				strcat(tmpBkupDstFile, "test.so");
			}

			memset(tmp,0,sizeof(tmp));
			sprintf(tmp,"cp %s  %s",tmpDstFile,tmpBkupDstFile); 			
			iRet = system(tmp);	
			OsLog(LOG_INFO, "\n tmp:%s, i:%d iRet:%d \n",tmp,i,iRet);
			if(iRet == -1)
			{
				OsLog(LOG_INFO, "\n bkup file, call system  fail, errno:%d , %s \n", errno,strerror(errno));
				return iRet;
			}
			OsLog(LOG_INFO, "\n backup Dst file end \n");
			//add end
			
			memset(tmp,0,sizeof(tmp));
			sprintf(tmp,"cp %s  %s",tmpSrcFile,tmpDstFile); 			
			iRet = system(tmp);	
			OsLog(LOG_INFO, "\n tmp:%s, i:%d iRet:%d \n",tmp,i,iRet);
			if(iRet == -1)
			{
				OsLog(LOG_INFO, "\n call system  fail, errno:%d , %s \n", errno,strerror(errno));

				//add start
				memset(tmp,0,sizeof(tmp));
				sprintf(tmp,"cp %s  %s",tmpBkupDstFile,tmpDstFile); 			
				iRet = system(tmp);	
				OsLog(LOG_INFO, "\n tmp:%s, i:%d iRet:%d \n",tmp,i,iRet);
				if(iRet == -1)
				{
					OsLog(LOG_INFO, "\n call system  fail, errno:%d , %s \n", errno,strerror(errno));
					return iRet;
				}
				//add end
				
				return iRet;
			}
			
			iRet = chmod(tmpDstFile, fileStat.st_mode);	
			if(iRet == -1)
			{
				OsLog(LOG_INFO, "\n chmod tmpDstFile:%s fail, errno:%d , %s \n",tmpDstFile, errno,strerror(errno));
				return iRet;
			}
			OsLog(LOG_INFO, "\n chmod iRet:%d \n",iRet);
			
			return iRet;
			
		}

		OsLog(LOG_INFO, "\n libHeadList[%d]:%s \n",i,libHeadList[i]);
		i++;
	}
	
	return iRet;
}
