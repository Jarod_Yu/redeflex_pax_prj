// Jarod@2015/09/10 UPD START
#include <openssl/rsa.h>
#include <openssl/err.h>
#include <openssl/objects.h>
#include <openssl/err.h>
// Jarod@2015/09/10 UPD END
#include "HF_API_base64.H"
#include "HF_API.h"


/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HFI_HASHSZ 20
#define HFI_PADDING_MINSIZE 11

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

BIGNUM* HFI_readBigNumber(const HF_INT8* dataIn, HF_UINT32 dataSz)
{
    HF_UINT32 i;
    BIGNUM* result;
    HF_INT8* dataOut = (HF_INT8*) HF_malloc(dataSz);

    for (i = 0; i < dataSz; i++)
    {
        dataOut[i] = dataIn[dataSz - i - 1];
    }

    result = BN_bin2bn((const unsigned char *)dataOut, dataSz, HF_NULL);

    HF_free(dataOut);

    return result;
}

HF_INT32 HF_rsa_loadkeyH(const HF_CHAR *filename, HF_RSA_KEY **key)
{
    RSA *rsa;
    HF_FILE_T *file;
    HF_UINT32 fileSz;
    HF_CHAR *keyData;
    HF_INT32 keylen;
    HF_CHAR temp[1024];
    HF_CHAR *data;
    HF_INT32 res;
    HF_BOOL isPublic;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_loadkeyH=====\n\n\n\n");

    if (key == HF_NULL || filename == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    res = HF_fs_getfsize(filename, &fileSz);
    if (res != HF_SUCCESS)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.unsuccess ret:[%d]=====\n\n",res);
        return res;
    }

    res = HF_fs_fopenH(filename, "r", &file);
    if(file == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.unsuccess ret:[%d]=====\n\n",res);
        return res;
    }

    res = HFI_fs_readline(file, temp, sizeof(temp)-1);
    if(res < 0)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_UNKNOWNFORMAT=====\n\n");
        HF_fs_fclose(file);
        return HF_ERR_UNKNOWNFORMAT;
    }

    if(HF_strncmp(temp, "-----", 5) != 0)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_UNKNOWNFORMAT=====\n\n");
        HF_fs_fclose(file);
        return HF_ERR_UNKNOWNFORMAT;
    }

    if(HF_strstr(temp, "PUBLIC"))
    {
        isPublic = HF_TRUE;
    }
    else if(HF_strstr(temp, "PRIVATE"))
    {
        isPublic = HF_FALSE;
    }
    else
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====6.HF_ERR_INVALIDRSAKEY=====\n\n");
        HF_fs_fclose(file);
        return HF_ERR_INVALIDRSAKEY;
    }

    data = (HF_CHAR *) HF_malloc(fileSz);
    HF_memset(data, 0, fileSz);

    while(1)
    {
    	HF_memset(temp,0,sizeof(temp));

        res = HFI_fs_readline(file, temp, sizeof(temp)-1);
        if(res < 0)
        {
            if(strncmp(temp, "-----", 5) == 0)
            {

                break;
            }
            HF_free(data);
            HF_fs_fclose(file);
            PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====7.HF_ERR_INVALIDRSAKEY=====\n\n");
            return HF_ERR_UNKNOWNFORMAT;
        }

        if(HF_strncmp(temp, "-----", 5) == 0)
        {
            break;
        }

        HF_strcat(data, temp);
    }

    HF_fs_fclose(file);

    res = HF_base64_decodeH(data, HF_strlen(data), &keyData, &keylen);
    HF_free(data);

    if(res < 0)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====7.HF_ERR_INVALIDRSAKEY=====\n\n");
        return HF_ERR_INVALIDRSAKEY;
    }

    if(keyData == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====8.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }

    rsa = RSA_new();
    if (rsa == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====9.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }

    /* get the first 32bits of the data read from the file, */
    /* it is used to calculade the size of the modulus */
    res = (HF_INT32) *keyData;

    if (isPublic == HF_TRUE)
    {
        rsa->e = HFI_readBigNumber(keyData + 4, 4);
        rsa->n = HFI_readBigNumber(keyData + 8, res * 4);
    }
    else
    {
        BIGNUM *r0=NULL,*r1=NULL,*r2=NULL;
        BN_CTX *ctx = BN_CTX_new();

        BN_CTX_start(ctx);
        r0 = BN_CTX_get(ctx);
        r1 = BN_CTX_get(ctx);
        r2 = BN_CTX_get(ctx);

        rsa->p = HFI_readBigNumber((keyData + 4), res * 2);
        rsa->q = HFI_readBigNumber((keyData + 4) + (res * 2), res * 2);
        rsa->d = HFI_readBigNumber((keyData + 4) + (res * 4), res * 4);

        /* calculate n */
        rsa->n = BN_new();
        BN_mul(rsa->n, rsa->p, rsa->q, ctx);

        /* calculate e */
        rsa->e = BN_new();
        BN_sub(r1,rsa->p,BN_value_one());
        BN_sub(r2,rsa->q,BN_value_one());
        BN_mul(r0,r1,r2,ctx);
        BN_mod_inverse(rsa->e,rsa->d,r0,ctx);

        /* calculate d mod (p-1) */
        rsa->dmp1 = BN_new();
        BN_mod(rsa->dmp1,rsa->d,r1,ctx);

        /* calculate d mod (q-1) */
        rsa->dmq1 = BN_new();
        BN_mod(rsa->dmq1,rsa->d,r2,ctx);

        /* calculate inverse of q mod p */
        rsa->iqmp = BN_new();
        BN_mod_inverse(rsa->iqmp,rsa->q,rsa->p,ctx);

        BN_CTX_end(ctx);
        BN_CTX_free(ctx);

        if(RSA_check_key(rsa) != 1)
        {
			HF_free(keyData);
			PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====10.HF_ERR_INVALIDRSAKEY=====\n\n");
            return HF_ERR_INVALIDRSAKEY;
        }
    }

	HF_free(keyData);

    *key = (HF_RSA_KEY *)rsa;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_BOOL HF_rsa_isPublic(const HF_RSA_KEY *key)
{
    RSA *rsa;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_isPublic=====\n\n\n\n");
/*    XuiClearKey();
    XuiGetKey()*/;


    if (key == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"  the rsa public key is null \n\n");

        return HF_FALSE;
    }

    rsa = (RSA *) key;

    return rsa->d == HF_NULL && rsa->e != HF_NULL && rsa->n != HF_NULL;
}

HF_BOOL HF_rsa_isPrivate(const HF_RSA_KEY *key)
{
    RSA *rsa;


    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_isPrivate=====\n\n\n\n");
/*
	XuiClearKey();
    XuiGetKey();
*/


    if (key == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"  the rsa private key is null \n\n");
        return HF_FALSE;
    }

    rsa = (RSA *) key;

    return rsa->n != HF_NULL && rsa->e != HF_NULL &&
           rsa->d != HF_NULL && rsa->p != HF_NULL &&
           rsa->q != HF_NULL;
}


HF_INT32 HF_rsa_disposekey(HF_RSA_KEY *key)
{
    RSA *rsa = (RSA *) key;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_disposekey=====\n\n\n\n");
/*
	XuiClearKey();
    XuiGetKey();
*/


    if (rsa == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"  HF_ERR_INVALIDARG \n\n");
        return HF_ERR_INVALIDARG;
    }

    RSA_free(key);

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"  HF_SUCCESS \n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_rsa_encryptH(const HF_RSA_KEY *key, const HF_INT8 *data, HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz)
{
    RSA *rsa;
    HF_INT8 *dataout;
    HF_UINT32 blockSz;
    HF_UINT32 nBlocks;
    HF_UINT32 pos;
    HF_UINT32 i;
    HF_INT8* temp;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_encryptH [%d]=====\n\n\n\n",dataSz);

    if (key == HF_NULL || data == HF_NULL || dataSz == 0 || result == HF_NULL || resultSz == HF_NULL )
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    if (!HF_rsa_isPublic(key))
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_NOTAPUBLICKEY=====\n\n");
        return HF_ERR_NOTAPUBLICKEY;
    }

    rsa = (RSA *) key;
    blockSz = RSA_size(rsa);
    nBlocks = (HF_UINT32)HF_ceil(((HF_DOUBLE)dataSz / ((HF_DOUBLE)blockSz - HFI_PADDING_MINSIZE)));
    dataout = (HF_INT8 *)HF_malloc(blockSz * nBlocks);
    if (dataout == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }



    // Jarod@2015/09/15 ADD START
    unsigned char rsa_n[256]={0};
    BN_bn2bin(rsa->n, rsa_n);
    for (i=0;i<128;i++)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"rsa_n[%d]:%02x",i,rsa_n[i]);
    }
    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\n\n\n");


    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"dataSz:[%d]",dataSz);
    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\n\n\n");

    for (i=0;i<dataSz;i++)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"data[%d]:%02x",i,data[i]);
    }
    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\n\n\n");
    // Jarod@2015/09/15 ADD END



    temp = (HF_INT8*) HF_malloc(blockSz);
    if (temp == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_NOMEMORY=====\n\n");
        HF_free(dataout);
        return HF_ERR_NOMEMORY;
    }

    *resultSz = 0;
    pos = 0;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====nBlocks:[%d]=====\n\n",nBlocks);

    for (i = 0; i < nBlocks; i++)
    {
        HF_INT32 encDataSz;
        HF_UINT32 j, k;
        HF_UINT32 dataToEnc = HF_MIN(dataSz - pos, blockSz - HFI_PADDING_MINSIZE);

        PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====dataToEnc:[%d]=====\n\n",dataToEnc);

        /* invert the bytes in the block */
        for (j = 0; j < dataToEnc; j++)
        {
            temp[j] = *(data + pos + dataToEnc - j - 1);
            PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"temp[%d]:\t%02x",j,temp[j]);
        }

        encDataSz = RSA_public_encrypt(dataToEnc, (const unsigned char*)temp, (unsigned char *)dataout + (*resultSz), rsa, RSA_PKCS1_PADDING);
        if (encDataSz == -1)
        {
        	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_DEVICEFAULT=====\n\n");
			HF_free(temp);
            HF_free(dataout);
            return HF_ERR_DEVICEFAULT;
        }

        /* invert the bytes again */
        j = *resultSz;
        k = *resultSz + encDataSz - 1;
        while (j < k)
        {
            HF_INT8 t = dataout[j];
            dataout[j] = dataout[k];
            dataout[k] = t;
            k--;
            j++;
        }

        pos += dataToEnc;
        *resultSz += encDataSz;
    }

	HF_free(temp);

    *result = dataout;

    for (i=0;i<*resultSz;i++)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"*result[%d]:%02x",i,dataout[i]);
    }

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_rsa_decryptH(const HF_RSA_KEY *key, const HF_INT8 *data, HF_INT32 dataSz, HF_INT8 **result, HF_INT32 *resultSz)
{
    RSA *rsa;
    HF_UINT32 blockSz;
    HF_UINT32 nBlocks;
    HF_INT8 *dataout;
    HF_UINT32 i;
    HF_INT8* temp;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_decryptH=====\n\n\n\n");
/*
    XuiClearKey();
    XuiGetKey();
*/

    if (key == HF_NULL || data == HF_NULL || result == HF_NULL || resultSz == HF_NULL  )
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    rsa = (RSA *) key;
    if (!HF_rsa_isPrivate(key))
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_NOTAPRIVATEKEY=====\n\n");
        return HF_ERR_NOTAPRIVATEKEY;
    }

    blockSz = RSA_size(rsa);

    if (dataSz < (HF_INT32)blockSz || dataSz % blockSz != 0)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    nBlocks = (HF_UINT32)(dataSz / blockSz);
    dataout = (HF_INT8 *)HF_malloc(blockSz * nBlocks);
    if (dataout == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }

    temp = (HF_INT8*) HF_malloc(blockSz);
    if (temp == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_NOMEMORY=====\n\n");
        HF_free(dataout);
        return HF_ERR_NOMEMORY;
    }

    *resultSz = 0;

    for (i = 0; i < nBlocks; i++)
    {
        HF_INT32 decDataSz;
        HF_UINT32 j, k;

        /* invert the bytes in the block */
        for (j = 0; j < blockSz; j++)
        {
            temp[j] = *(data + (blockSz * (i + 1)) - j - 1);
        }

        decDataSz = RSA_private_decrypt(blockSz, (const unsigned char*)temp, (unsigned char*)dataout + (*resultSz), rsa, RSA_PKCS1_PADDING);

        if (decDataSz == -1)
        {
        	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====6.HF_ERR_INVALIDPAD=====\n\n");
            HF_free(dataout);
            HF_free(temp);
			return HF_ERR_INVALIDPAD;
        }

        /* invert the bytes again */
        j = *resultSz;
        k = *resultSz + decDataSz - 1;
        while (j < k)
        {
            HF_INT8 t = dataout[j];
            dataout[j] = dataout[k];
            dataout[k] = t;
            k--;
            j++;
        }

        *resultSz += decDataSz;
    }

    HF_free(temp);

    *result = dataout;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

static HF_VOID HFI_rsa_addPadding(HF_INT8 *buff, HF_UINT32 buffSz, HF_UINT32 length)
{
    HF_UINT32 i = length;
    buff[i++] = 0;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HFI_rsa_addPadding=====\n\n\n\n");
/*
    XuiClearKey();
    XuiGetKey();
*/

    for (; i < buffSz - 2; i++)
    {
        HF_INT8 rdnInt;
        do
        {
            rdnInt = (HF_INT8)HF_rand();
        } while (rdnInt == 0);
        buff[i] = rdnInt;
    }
    buff[i++] = 2;
    buff[i++] = 0;
}

HF_INT32 HF_rsa_signH(const HF_INT8 *hash, HF_INT32 hashlen, HF_INT8 **signature, HF_INT32 *siglen, const HF_RSA_KEY *key)
{
    RSA *rsa;
    HF_INT32 j, k;
    HF_INT8* temp;


    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_signH=====\n\n\n\n");
/*
    XuiClearKey();
    XuiGetKey();
*/

    if (key == HF_NULL || signature == HF_NULL || siglen == HF_NULL || hash == HF_NULL || hashlen <= 0 || hashlen > HFI_HASHSZ || HF_strcmp( hash, "" ) == 0 )
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    rsa = (RSA*) key;
    if (!HF_rsa_isPrivate(rsa))
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_NOTAPRIVATEKEY=====\n\n");
        return HF_ERR_NOTAPRIVATEKEY;
    }

    *siglen = RSA_size(rsa);

    *signature = HF_malloc(*siglen);
    if (*signature == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }

    temp = (HF_INT8*) HF_malloc(*siglen);
    if(temp == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_NOMEMORY=====\n\n");
        HF_free(*signature);
        return HF_ERR_NOMEMORY;
    }

    HF_memcpy(temp, hash, hashlen);
    HFI_rsa_addPadding(temp, *siglen, hashlen);

    /* invert the bytes in the block */
    j = 0;
    k = *siglen - 1;
    while (j < k)
    {
        HF_INT8 t = temp[j];
        temp[j] = temp[k];
        temp[k] = t;
        k--;
        j++;
    }

    if (RSA_private_encrypt(*siglen, (const unsigned char*)temp, (unsigned char *)*signature, rsa, RSA_PKCS1_PADDING) == -1)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_DEVICEFAULT=====\n\n");
        HF_free(signature);
        HF_free(temp);
        return HF_ERR_DEVICEFAULT;
    }

    j = 0;
    k = *siglen - 1;
    while (j < k)
    {
        HF_INT8 t = (*signature)[j];
        (*signature)[j] = (*signature)[k];
        (*signature)[k] = t;
        k--;
        j++;
    }

	HF_free(temp);

	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_rsa_verify(const HF_INT8 *hash, HF_INT32 hashlen, const HF_INT8 *signature, HF_INT32 siglen, const HF_RSA_KEY *key)
{
    RSA *rsa;
    HF_INT32 j, k;
    HF_INT8* temp;
    HF_INT8* dataout;

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_rsa_verify=====\n\n\n\n");
/*
    XuiClearKey();
    XuiGetKey();
*/

    if (key == HF_NULL || signature == HF_NULL || hash == HF_NULL || hashlen <= 0 || siglen <= 0 )
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_INVALIDARG=====\n\n");
        return HF_ERR_INVALIDARG;
    }

    rsa = (RSA*) key;

    if (siglen != RSA_size(rsa))
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_INVALIDARG=====\n\n");
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====siglen:[%d],RSA_size:[%d]=====\n\n",siglen,RSA_size(rsa));
        return HF_ERR_INVALIDARG;
    }

    if (!HF_rsa_isPublic(rsa))
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_NOTAPUBLICKEY=====\n\n");
        return HF_ERR_NOTAPUBLICKEY;
    }

    temp = (HF_INT8*) HF_malloc(sizeof(HF_UINT8) * siglen);
    if (temp == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_NOMEMORY=====\n\n");
        return HF_ERR_NOMEMORY;
    }

    dataout = (HF_INT8*) HF_malloc(sizeof(HF_UINT8) * siglen);
    if (dataout == HF_NULL)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_NOMEMORY=====\n\n");
        HF_free(temp);
        return HF_ERR_NOMEMORY;
    }

    // inverts the signature
    for (j = 0; j < siglen; j++)
    {
        temp[j] = *(signature + siglen - j - 1);
    }

    //api de rsa informa q quando ocorre erro retorna -1
    if (RSA_public_decrypt(siglen, (unsigned char *) temp, (unsigned char *) dataout, rsa, RSA_NO_PADDING) == -1)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====6.HF_ERR_DEVICEFAULT=====\n\n");
        HF_free(dataout);
        HF_free(temp);
        return HF_ERR_DEVICEFAULT;
    }
    j = 0;
    k = siglen - 1;
    while (j < k)
    {
        HF_INT8 t = dataout[j];
        dataout[j] = dataout[k];
        dataout[k] = t;
        k--;
        j++;
    }

    for (j = 0; j < hashlen; j++)
    {
        if (dataout[j] != hash[j])
        {
        	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====7.HF_ERR_INVALSIGNATURE=====\n\n");
            HF_free(temp);
            HF_free(dataout);
            return HF_ERR_INVALSIGNATURE;
        }
    }

    /* verifies the existence of the padding */
    if (dataout[hashlen] != 0 && dataout[siglen - 1] == 0 && dataout[siglen - 2] == 2)
    {
    	PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====8.HF_ERR_INVALSIGNATURE=====\n\n");
        HF_free(temp);
        HF_free(dataout);
        return HF_ERR_INVALSIGNATURE;
    }

    HF_free(temp);
    HF_free(dataout);

    PaxLog_RSA(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}
