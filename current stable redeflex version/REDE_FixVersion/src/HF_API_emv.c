#include "HF_API.h"

int HEMV_iInit (void)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

void HEMV_Version (char *pszVersion)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
}

int HEMV_iCheckCard (void)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iStartTransaction (void)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iDefData (unsigned int uiTag, int iLen, const unsigned char *pbValue)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iGetData (unsigned int uiTag, int *piLen, unsigned char *pbValue)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iDefApp (int iRef, int iAID_Len, const unsigned char *pbAID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iGetCandidateList (int *piItems, unsigned char *pbLabels)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iSelectApp (int iAppIdx)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iProcessTransaction (int *piResult, int *piCVMResult, void *pvHandle)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

int HEMV_iCompleteTransaction (int iOnlineResult, int *piResult)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
