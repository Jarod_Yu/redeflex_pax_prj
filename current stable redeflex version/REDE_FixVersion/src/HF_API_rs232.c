#include "HF_API.h"

#include <osal.h>
#include <xui.h>

#define PORT_ATTR_MAX_LEN  12

static HF_BOOL GetRs232Config(const HF_RS232_CONFIG *config, HF_CHAR *attr);

HF_INT32 HF_rs232_openH(HF_CHAR *port, const HF_RS232_CONFIG *config, HF_RS232_HANDLE **handle)
{
	HF_CHAR attr[PORT_ATTR_MAX_LEN + 1];
	HF_INT32 iRet=0, *portHandle=NULL;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	memset(attr, 0, sizeof(attr));
	if((NULL == port) || (NULL == config) || (NULL == handle))
	{
		return RF_ERR_DEVICEFAULT;
	}

	if(HF_FALSE == GetRs232Config(config, attr))
	{
		return RF_ERR_DEVICEFAULT;
	}

	portHandle = (HF_INT32 *)malloc(sizeof(HF_INT32));
	if(NULL == portHandle)
	{
		return RF_ERR_DEVICEFAULT;
	}

	//trans the port name to prolin recognition,RF_RS232_COM1 or RF_RS232_COM2
	if(0 == strcmp(port, "RF_RS232_COM1"))
	{
		*portHandle = PORT_COM1;
	}
	else if(0 == strcmp(port, "RF_RS232_COM2"))
	{
		*portHandle = PORT_COM2;
	}
	else
	{
		free(portHandle);
		return RF_ERR_DEVICEFAULT;
	}

	//open the port
	iRet = OsPortOpen(*portHandle, attr);
	if(ERR_DEV_BUSY == iRet )
	{
		iRet = RF_ERR_RESOURCEALLOC;
	}
	else if(ERR_DEV_NOT_EXIST == iRet || ERR_INVALID_PARAM == iRet)
	{
		iRet = RF_ERR_DEVICEFAULT;
	}

	if (HF_SUCCESS == iRet)
	{
		*handle = portHandle;
	}
	else
	{
		free(portHandle);
	}
    return iRet;
}

HF_INT32 HF_rs232_recvmsg(const HF_RS232_HANDLE *handle, HF_UINT8 *buffer, HF_UINT16 bufflen, HF_UINT32 timeout)
{
	HF_INT32 recvLen = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if((NULL == handle) || (NULL == buffer))
	{
		return HF_ERR_DEVICEFAULT;
	}

	recvLen = OsPortRecv(*(HF_INT32 *)handle, buffer, bufflen, timeout);
	if((0 == recvLen) && (0 != bufflen))
	{
		return HF_ERR_TIMEOUT;
	}
	else if(recvLen < 0)
	{
		return HF_ERR_DEVICEFAULT;
	}

    return recvLen;
}

HF_INT32 HF_rs232_sendmsg(const HF_RS232_HANDLE *handle, const HF_UINT8 *data, HF_UINT16 datalen, HF_UINT32 timeout)
{
	ST_TIMER timer;
	HF_INT32 iRet = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	memset(&timer, 0, sizeof(timer));

	if((NULL == handle) || (NULL == data))
	{
		return HF_ERR_DEVICEFAULT;
	}

	iRet = OsTimerSet(&timer, timeout);
	if(0 != iRet)
	{
		return HF_ERR_DEVICEFAULT;
	}

	//send data until timeout
	while(OsTimerCheck(&timer) > 0)
	{
		//if sendLen is 0, send success
		iRet = OsPortSend(*(HF_INT32 *)handle, data, datalen);
		if(0 == iRet)
		{
			return datalen;
		}
		else
		{
			return HF_ERR_DEVICEFAULT;
		}
	}

    return HF_ERR_TIMEOUT;
}

HF_INT32 HF_rs232_close(HF_RS232_HANDLE *handle)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if(NULL == handle)
	{
	    return HF_SUCCESS;
	}

	OsPortClose(*(HF_INT32 *)handle);
	free(handle);
//	handle = NULL;

    return HF_SUCCESS;
}

HF_INT32 HF_rs232_usb_open(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_rs232_usb_close(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_rs232_usb_recvmsg(HF_UINT8 *buffer, HF_UINT16 bufflen, HF_UINT32 timeout)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_rs232_usb_sendmsg(const HF_UINT8 *data, HF_UINT16 datalen, HF_UINT32 timeout)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_rs232_usb_status(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

//TODO Below is the function define by richard
static HF_BOOL GetRs232Config(const HF_RS232_CONFIG *config, HF_CHAR *attr)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if((NULL == config) || (NULL == attr))
	{
		return HF_FALSE;
	}

	//get baud
	switch(config->baud)
	{
	case HF_RS232_BAUD_1200:
		strcpy(attr, "1200");
		break;
	case HF_RS232_BAUD_2400:
		strcpy(attr, "2400");
		break;
	case HF_RS232_BAUD_4800:
		strcpy(attr, "4800");
		break;
	case HF_RS232_BAUD_9600:
		strcpy(attr, "9600");
		break;
	case HF_RS232_BAUD_19200:
		strcpy(attr, "19200");
		break;
	case HF_RS232_BAUD_38400:
		strcpy(attr, "38400");
		break;
	case HF_RS232_BAUD_57600:
		strcpy(attr, "57600");
		break;
	case HF_RS232_BAUD_115200:
		strcpy(attr, "115200");
		break;
	default:
		//baud not right
		return HF_FALSE;
	}

	//get dataSzie
	switch(config->dataSize)
	{
	case HF_RS232_DATASIZE_7:
		strcat(attr, ",7");
		break;
	case HF_RS232_DATASIZE_8:
		strcat(attr, ",8");
		break;
	default:
		//dataSzie not right
		return HF_FALSE;
	}

	//get paritys
	switch(config->parity)
	{
	case HF_RS232_PARITY_NONE:
		strcat(attr, ",n");
		break;
	case HF_RS232_PARITY_ODD:
		strcat(attr, ",o");
		break;
	case HF_RS232_PARITY_EVEN:
		strcat(attr, ",e");
		break;
	default:
		//paritys not right
		return HF_FALSE;
	}

	//get stopbits
	switch(config->stopBits)
	{
	case HF_RS232_STOP_1:
		strcat(attr, ",1");
		break;
	case HF_RS232_STOP_2:
		strcat(attr, ",2");
		break;
	default:
		//stopbits not right
		return HF_FALSE;
	}

	return HF_TRUE;
}

