#include "HF_API.h"
#include <osal.h>

HF_TM_T global_time = {0};

//const int everyMonthDay = {31,28,31,30,31,30,31,31,30,31,30,31};	//Gabriel it isn't a array
const int everyMonthDay[] = {31,28,31,30,31,30,31,31,30,31,30,31};	//Kevin
/* IMPLEMENTADO POR G.CABRAL - 28.04.2015 */
HF_TM_T* HF_get_datetime(HF_VOID)
{
    ST_TIME dateTime = {0};
    RF_INT32 yearDay = 0;
    RF_INT32 i = 0;
//    RF_BOOL isBissexto = 0;	//Gabriel
    RF_BOOL isLeapYear = 0;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

    OsGetTime(&dateTime);

    global_time.day = dateTime.Day;
    global_time.month = dateTime.Month;
    global_time.year = dateTime.Year;

//    if((dateTime.Year - 1972) % 4 == 0)   //Gabriel
    if(((dateTime.Year % 4 == 0) && (dateTime.Year % 100 != 0)) || (dateTime.Year % 400 == 0))	//kevin
    {
//    	isBissexto = 1;	//Gabriel
    	isLeapYear = 1;
    }

    global_time.hour = dateTime.Hour;
    global_time.minute = dateTime.Minute;
    global_time.second = dateTime.Second;
    global_time.weekday= dateTime.DayOfWeek;


    for(i = 0; i < dateTime.Month - 1; i++)
    {
    	yearDay = yearDay + everyMonthDay[i];
    }
    yearDay = yearDay + dateTime.Day;

//    if(isBissexto && dateTime.Month > 2)	//Gabriel
    if(isLeapYear && dateTime.Month > 2)
    {
    	yearDay++;
    }

    global_time.yearday = yearDay;

    return &global_time;
}

/* IMPLEMENTADO POR G.CABRAL - 06.05.2015 */
HF_INT32 HF_get_time(HF_VOID)
{
	ST_TIME dateTime = {0};
	RF_INT32 prevDayExtras = 0;
	RF_INT32 prevLeapYear = 0;
	RF_INT32 prevYear = 0;
	RF_INT32 prevDay = 0;
	RF_INT32 prevDayThisYear = 0;
	RF_INT32 i = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	OsGetTime(&dateTime);

	for(prevYear = 1970; prevYear < dateTime.Year; prevYear++)
	{
		prevDay += 365;
	}
//Gabriel---------------------------------------------------------------------------
//	for(anoBissexto = 1972; anoBissexto <= dateTime.Year; anoBissexto += 4)
//	{
//		qtdDiasExtras++;
//	}
//----------------------------------------------------------------------------------
	//not every fourth year is leap year
	for(prevLeapYear = 1972; prevLeapYear < dateTime.Year; prevLeapYear += 4)	//kevin
	{
		if(((prevLeapYear % 4 == 0) && (prevLeapYear % 100 != 0)) || (prevLeapYear % 400 == 0))
		{
			prevDayExtras++;
		}
	}


	if(dateTime.Month == 1)
	{
		prevDay = prevDay + prevDayExtras + dateTime.Day - 1 ;
	}
	else
	{
		for(i = 0; i < dateTime.Month - 1; i++)
		{
			prevDayThisYear = prevDayThisYear + everyMonthDay[i];
		}
		if((dateTime.Month > 2) && (((dateTime.Year % 4 == 0) && (dateTime.Year % 100 != 0)) || (dateTime.Year % 400 == 0)))
		{
			prevDayThisYear++;
		}

		prevDay = prevDay + prevDayExtras + prevDayThisYear + dateTime.Day - 1;
	}

//    return qtdDias * 86400;	//Gabriel   haven't include today seconds
	return (prevDay * 86400 + dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second);	//kevin
}

/* IMPLEMENTADO POR G.CABRAL - 28.04.2015 */
HF_INT32 HF_set_datetime(const HF_TM_T *systemtime)
{
	ST_TIME dateTime = {0};
	HF_INT32 iRet = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if(NULL == systemtime)	//kevin
	{
		return HF_ERR_INVALIDARG;
	}

	dateTime.Day = systemtime->day;
	dateTime.Month = systemtime->month;

	if((systemtime->year < 1970) || (systemtime->year > 2037))	//OsSetTime can only set 2037
		return HF_ERR_INVALIDDATE;

	dateTime.Year = systemtime->year;

	if((systemtime->hour < 0) || (systemtime->hour > 23) || (systemtime->minute < 0) || (systemtime->minute > 59) || (systemtime->second < 0) || (systemtime->second > 59))
		return HF_ERR_INVALIDTIME;

	dateTime.Hour = systemtime->hour;
	dateTime.Minute = systemtime->minute;
	dateTime.Second = systemtime->second;
	dateTime.DayOfWeek = systemtime->weekday;

//	OsSetTime(&dateTime);	//Gabriel   haven't judge return code

	iRet = OsSetTime(&dateTime);	//kevin
	if(RET_OK == iRet)
	{
		return HF_SUCCESS;
	}
	else if(ERR_INVALID_PARAM == iRet)
	{
		return HF_ERR_INVALIDDATE;
	}
	else
	{
		return HF_ERR_RESOURCEALLOC;
	}
    return HF_SUCCESS;
}
