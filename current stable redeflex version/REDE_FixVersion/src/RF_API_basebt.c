#include "HF_API.h"

RF_INT32 RF_BASEBT_associate (RF_BASEBT_ASSOCIATION_RESULT* p_res)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

RF_INT32 RF_BASEBT_disassociate (RF_BASEBT_ASSOCIATION_INFO info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

RF_INT32 RF_BASEBT_config(RF_BASEBT_CONFIG_INFO *config)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

RF_INT32 RF_BASEBT_up (RF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

RF_INT32 RF_BASEBT_down (RF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

RF_INT32 RF_BASEBT_status (RF_BASEBT_STATUS_INFO* p_info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
