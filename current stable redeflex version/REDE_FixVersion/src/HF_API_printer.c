#include "HF_API.h"
#include <stdarg.h>

HF_INT32 HF_printer_linefeed(HF_INT32 lines)
{
    // ---------------------------------
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    while (lines--)
    {
        printf("\n");
    }
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_printer_print(const HF_CHAR *text)
{
    // ---------------------------------
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_printer_printf(const HF_CHAR *fmt, ...)
{
	va_list args;
    // ---------------------------------
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------
	va_start(args, fmt);
	va_end(args);

    return HF_SUCCESS;
}

HF_INT32 HF_printer_printimage(const HF_IMAGE *image, HF_UINT32 flag)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_printer_check(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_printer_setFont(HF_FONT_T font)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_printer_getFont(HF_FONT_T *pFont)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_printer_getLineCharCount(HF_FONT_T pFont, HF_UINT32 *charCount)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_printer_printimagebuff(const HF_VOID *buff, HF_UINT32 size)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
