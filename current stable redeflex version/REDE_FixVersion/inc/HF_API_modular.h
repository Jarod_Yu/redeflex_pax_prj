/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_modular.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Data:
Data     : 27/03/2015
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_API_MODULAR_H_
#define _HF_API_MODULAR_H_

/* Basic X-Plug types definition */
typedef signed char    Int8;     
typedef unsigned char  Uint8;    
typedef signed short   Int16;    
typedef unsigned short Uint16;   
typedef signed long    Int32;    
typedef unsigned long  Uint32;   

#ifndef __TELIUM2__
typedef unsigned char  Byte;
typedef unsigned short Word;
#endif

typedef int            Bool;     

#define POS_IMGTYPE				SFDFLG_BMP 
#define POSDSP_PIXWIDTH			240
#define POSDSP_PIXHEIGHT		320

#define POS_iSemaphoreCreate	HF_thread_semaphoreCreate
#define POS_iSepaphorePost		HF_thread_semaphorePost
#define POS_iSemaphoreWait		HF_thread_semaphoreWait
#define POS_iThreadStart		HF_thread_iThreadStart
#define POS_iThreadEnd			HF_thread_iThreadEnd

#define dllmalloc				HF_malloc
#define dllfree					HF_free

#define HF_CARDREADER_SWIPE_EVENT (1 << 0)
#define HF_CARDREADER_CHIP_EVENT  (1 << 1)
#define HF_SCREEN_TOUCH_EVENT     (1 << 2)
#define HF_KEYBOARD_EVENT         (1 << 3)
#define HF_CARDREADER_CLESS_EVENT (1 << 4)

#define HF_TRACK_MAX 110

/* Constants */

#ifndef Const
#define Const const
#endif

#ifndef PackedStruct
#define PackedStruct struct
#endif

#ifndef PackedUnion
#define PackedUnion union
#endif

#ifndef PackedEnd
#define PackedEnd
#endif


/* Util macros */

#ifndef UNREF_PAR
#define UNREF_PAR(x) x=x
#endif

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b) (((a) < (b)) ? (b) : (a))
#endif


#ifndef HI_BYTE
#define HI_BYTE(a) ((Byte) (((Uint16) (a)) / 0x100))
#define LO_BYTE(a) ((Byte) (((Uint16) (a)) & 0xFF))
#endif


/* Success return code */

#define PAL_OK                0

/* Basic error codes  (-1 to -20) */

#define PAL_MAXERR            -1
#define PAL_MINERR           -20

#define PAL_ALREADYOPEN      PAL_MAXERR
#define PAL_INVHANDLE        (PAL_MAXERR-1)
#define PAL_NOFUNC           (PAL_MAXERR-2)
#define PAL_INVNAME          (PAL_MAXERR-3)
#define PAL_CONFIGERR        (PAL_MAXERR-4)
#define PAL_LIBNOTFOUND      (PAL_MAXERR-5)
#define PAL_INTERNALERROR    (PAL_MAXERR-6)
#define PAL_INVPARAM         (PAL_MAXERR-7)
#define PAL_INVMODEL         (PAL_MAXERR-8)
#define PAL_INVCMD           (PAL_MAXERR-9)
#define PAL_TOOMANYOPEN      (PAL_MAXERR-10)
#define PAL_BUFFER_OVFLW     (PAL_MAXERR-11)
#define PAL_WRONGVERSION     (PAL_MAXERR-12)
#define PAL_INVLICENSE       (PAL_MAXERR-13)

struct POS_iWaitEvents_TOUCH_EVENT_DATA
{
  Int16 x;
  Int16 y;
};

struct POS_iWaitEvents_KEYBOARD_EVENT_DATA
{
  Int32 key;
};

struct POS_iWaitEvents_SWIPE_EVENT_DATA
{
  Int8* track1;
  Int8* track2;
  Int8* track3;
};

//TODO, project here have not RF_IWAIT_EVTS_OPTION_CAPABILITIES
//enum RF_IWAIT_EVTS_OPTIONS {RF_IWAIT_EVTS_OPTION_BEEP = 1};
enum RF_IWAIT_EVTS_OPTIONS {RF_IWAIT_EVTS_OPTION_BEEP = 1, RF_IWAIT_EVTS_OPTION_CAPABILITIES = 2};

//API function

/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/8
Name:					POS_iDisplayImage
Description:			Display image positioning on the display according to the coordinates X / Y pixel (px).
Parameters:				iX – Indicates position of the image on the X axis display.
						iY –Indicates position of the image on the Y axis display. When positioned in the 0 to 0 for X and Y the image is displayed in full screen
						ulSize – size "buffer" pbPtr in bytes
						pbPtr – points to the "buffer" with the bytes of the image to be displayed in BMP format with 24 bpp.

Returns:				When executed successfully returns 0 if results other than 0 is wrong.
Modified: [Abstract modification (date, author / id
factory description
*************************************************************
*/
Int16    POS_iDisplayImage(Int16 iX, Int16 iY, Uint32 ulSize, const Byte *pbPtr);
Int16    POS_iGetTouchXY (Int16 *piX, Int16 *piY);
/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/8
Name:					POS_iWaitEvents
Description:			Awaits event according to the parameterized events in the function call.
Parameters:				timeout - maximum waiting time of the event (in seconds).
						events - This parameter is a mask that indicates (for each bit) which events should be monitored.
						outputs - Pointer will point to the data structure that will store the data for the event.

Returns:				Zero - TIMEOUT
						Number greater than zero - captured Event
						Number less than zero - error.

Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
Int32    POS_iWaitEvents(Int32 timeout, Uint16 events, void** outputs);
/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/8
Name:					POS_iWaitEvents_options
Description:			Sets parameters for running the RF_POS_iWaitEvents.
Parameters:				option - Indicates which parameter / option being configured
						events - Bit mask indicating for which events the option is configured. It's the same mask used in RF_POS_iWaitEvents. •
						option_val - Indicates the value of the option. Each option can have a specific data structure.

Returns:				When executed successfully returns 0 if results other than 0 is wrong.
Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
RF_INT32 POS_iWaitEvents_options(RF_UINT32 options, RF_UINT16 events, RF_VOID* option_val);
/*
*************************************************************
Author and Publisher:	PAX Application Development Department Kevin Liu
Date:					2015/5/8
Name:					POS_iWaitEvents_get_options
Description:			Get the parameters for implementing the RF_POS_iWaitEvents.
Parameters:				option - Indicates which parameter / option is being consulted
						events - Bit mask indicating for which events the option is configured. It's the same mask used in RF_POS_iWaitEvents
						option_val - Pointer that indicates where the pointer to the option value should be returned.
						Each option can have a specific data structure.
						The caller must release the option_val memory.


Returns:				When executed successfully returns 0 if results other than 0 is wrong.
Modified: [Abstract modification (date, author / id
factory description]
*************************************************************
*/
RF_INT32 POS_iWaitEvents_get_options(RF_UINT32 option, RF_UINT16 events,  RF_VOID** option_val);

#endif
