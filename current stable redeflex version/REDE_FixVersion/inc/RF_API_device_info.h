/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : RF_API_device_info.h
Descri莽茫o: Arquivo fonte da plataforma Redeflex
Autor    : Renato
Data     : 27/03/2015
Empresa  : Rede S.A.
*********************** MODIFICA脟脮ES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descri莽茫o: Inspe莽茫o de c贸digo
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _RF_API_DEVICE_INFO_H_
#define _RF_API_DEVICE_INFO_H_

/*-----------------------------------------------------------------------------------------------------------------*/
/* Copyright 2015 Rede S.A.                                                                                        */
/* Nome do arquivo:                 RF_API_device_info.h                                                           */
/* Tres Letras Representativas:     -                                                                              */
/* Descricao:                       -                                                                              */
/* Autor:                           -                                                                              */
/* Data:                            27/03/2015                                                                     */
/* Empresa:                         Rede                                                                           */
/*-----------------------------------------------------------------------------------------------------------------*/
/*                            Historico                                                                            */
/* Nome                 Login       Data        Descricao                                             Empresa      */
/*-----------------------------------------------------------------------------------------------------------------*/
/*                                                                                                                 */
/*-----------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/
#ifdef INPUT_KEYBOARD
#undef INPUT_KEYBOARD
#endif


#define RF_SCREEN_BITS_PER_PIXEL		8*4

//D200　 2.3 LCD's PPI is 174
//S3900　LCD's PPI is 115 0.220（W）*0.220 (H) mm
//S800　 LCD's PPI is 141 0.18* 0.18  mm
#define RF_SCREEN_BITS_PER_INCH_D200		174
#define RF_SCREEN_BITS_PER_INCH_S3900		115
#define RF_SCREEN_BITS_PER_INCH_S800		141
#define RF_SCREEN_BITS_PER_INCH_PX5			133
#define RF_SCREEN_BITS_PER_INCH_PX7			186

#define RF_BMP_FMT_LIST						{1,2,16,32,64}
#define RF_BMP_FMT_LIST_SIZE				5

#define MONO_BITS_PER_PIXEL					1
#define COLOR_BITS_PER_PIXEL				8*4

#define SCREEN_CURRENT_SETTING_FILE			"./res/ScrSetting"

#define PRINTER_CURRENT_SETTING_FILE		"./res/PrnSetting"

#define SCREEN_BMP_FMT_LIST_FILE			"./res/ScrBmpFmtList"

#define PRINTER_BMP_FMT_LIST_FILE			"./res/PrnBmpFmtList"

#define PRINTER_DOTS_PER_INTCH				203  // 1in = 25.4 mm, 8 dots per mm, so result is 25.4*8


/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/
typedef enum
{
    GRAPH_MODE_CARACTER = 1,
    GRAPH_MODE_MONO     = 2,
    GRAPH_MODE_COLOR    = 3
} RF_GRAPHIC_MODE;

typedef enum
{
    GRAPH_ORIENTATION_PORTRAIT  = 1,
    GRAPH_ORIENTATION_LANDSCAPE = 2
} RF_GRAPHIC_ORIENTATION;

typedef enum {
    COMM_DIAL_SYNC          =  1,
    COMM_DIAL_ASYNC         =  2,
    COMM_WIRELESS_CSD       =  3,
    COMM_WIRELESS_GPRS      =  4,
    COMM_WIRELESS_EDGE      =  5,
    COMM_WIRELESS_3G        =  6,
    COMM_WIRELESS_4G        =  7,
    COMM_WIRELESS_WIFI      =  8,
    COMM_ETHERNET           =  9,
    COMM_WIRELESS_BLUETOOTH = 10,
    COMM_WIRELESS_NFC       = 11,
    COMM_BASE_BLUETOOTH     = 12,
    COMM_SERIAL_PORT        = 13,
    COMM_SERIAL_USB         = 14
} RF_COMM_TECH;



typedef enum {
    INPUT_KEYBOARD            =  1,
    INPUT_TOUCHSCREEN         =  2,
    INPUT_KEYBOARD_ONSCREEN   =  3,
    INPUT_KEYBOARD_EXTERNAL   =  4,
    INPUT_BIO_FINGERPRINT     =  5,
    INPUT_VOICE               =  6,
    INPUT_MAGCARD_READER      =  7,
    INPUT_CHIPCARD_READER     =  8,
    INPUT_SERIAL              =  9,
    INPUT_SERIAL_USB          = 10,
    INPUT_CHIPCARD_CONTACLESS = 11
} RF_INPUT_METHOD;

typedef enum
{
    GRAPH_BITMAP_FMT_BMP_MONO  =  1, // WINDOWS BMP MONOCROMATICO
    GRAPH_BITMAP_FMT_BMP_COLOR =  2, // WINDOWS BMP COLORIDO
    GRAPH_BITMAP_FMT_VMP_MONO  =  4, // VERIFONE VMP MONOCROMATICO
    GRAPH_BITMAP_FMT_VMP_COLOR =  8, // VERIFONE VMP COLORIDO
    GRAPH_BITMAP_FMT_JPEG      = 16, // JPEG
    GRAPH_BITMAP_FMT_GIF       = 32, // GIF
    GRAPH_BITMAP_FMT_PNG       = 64, // PNG
} RF_GRAPHIC_BMP_FORMAT;

typedef enum
{
    PRINTER_BITMAP_FMT_BMP_MONO  =  1, // WINDOWS BMP MONOCROMATICO
    PRINTER_BITMAP_FMT_BMP_COLOR =  2, // WINDOWS BMP COLORIDO
    PRINTER_BITMAP_FMT_VMP_MONO  =  4, // VERIFONE VMP MONOCROMATICO
    PRINTER_BITMAP_FMT_VMP_COLOR =  8, // VERIFONE VMP COLORIDO
    PRINTER_BITMAP_FMT_JPEG      = 16, // JPEG
    PRINTER_BITMAP_FMT_GIF       = 32, // GIF
    PRINTER_BITMAP_FMT_PNG       = 64, // PNG
} RF_PRINTER_BMP_FORMAT;

typedef enum
{
    RF_DEVICE_INFO_VERSAO_SO                =   1,
    RF_DEVICE_INFO_VERSAO_KERNEL_EMV        =  50,
    RF_DEVICE_INFO_VERSAO_EMV_CTLESS        = 100,
    RF_DEVICE_INFO_VERSAO_CTLESS_JCB        = 101,
    RF_DEVICE_INFO_VERSAO_CTLESS_MASTERCARD = 102,
    RF_DEVICE_INFO_VERSAO_CTLESS_VISA       = 103,
    RF_DEVICE_INFO_VERSAO_CTLESS_AMEX       = 104
} RF_DEVICE_INFO;


/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef struct RF_ScreenInfo
{
    RF_UINT32 structSize; // Tamanho, em bytes, desta estrutura

    RF_GRAPHIC_MODE mode;
    RF_UINT16 pixels_x, pixels_y;
    RF_UINT16 bits_per_pixel;
    RF_UINT16 pixels_per_inch;
    RF_GRAPHIC_ORIENTATION orientation;

    RF_UINT32 bmp_format_mask; //lista formatos bmp
} RF_ScreenInfo;

typedef struct RF_PrinterInfo
{
    RF_UINT32 structSize;

    RF_GRAPHIC_MODE mode;
    RF_UINT16 bits_per_pixel;
    RF_UINT16 dots_per_inch;
    RF_GRAPHIC_ORIENTATION orientation;

    RF_UINT32 bmp_format_mask;
} RF_PrinterInfo;

typedef struct RF_InputInfo
{
    RF_INPUT_METHOD method;
} RF_InputInfo;

typedef struct RF_CommInfo
{
    RF_COMM_TECH tech;
} RF_CommInfo;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*************************************************************************************************
 *Description：Lists the graphic capabilities of the device.
 *Arguments: 
 *		pp_info – Pointer should receive an array of RF _ screeninfo containing the graphic capabilities of the device
 *		p_infosz – Pointer that must receive the size of the array pointed to by PP _ info
 *Return:
 *		RF_SUCCESS - Success to enumerate capabilities
 *		RF_ERR_NOMEMORY - Failed to allocate memories
 *************************************************************************************************/
RF_INT32 RF_screen_get_capabilities( struct RF_ScreenInfo** pp_info, RF_UINT16* p_infosz);

/*************************************************************************************************
 *Description：Lists the capabilities of the printer device
 *Arguments:
 *		pp_info - Pointer should receive an array of RF _ printerinfo containing the capabilities of the printer device
 *		p_infosz - Pointer that must receive the size of the array pointed to by PP _ info
 *Return:
 *		RF_SUCCESS - Success to enumerate capabilities
 *		RF_ERR_NOMEMORY - Failed to allocate memories
 *************************************************************************************************/
RF_INT32 RF_printer_get_capabilities( struct RF_PrinterInfo** pp_info, RF_UINT16* p_infosz);

/*************************************************************************************************
 *Description：Lists the input methods available to the device
 *Arguments:
 *		pp_info - Pointer should receive an array of RF _ inputinfo containing the input methods available
 *		pp_infosz - Pointer that must receive the size of the array pointed to by PP _ info
 *Return:
 *		RF_SUCCESS - Success to enumerate capabilities
 *		RF_ERR_NOMEMORY - Failed to allocate memories
 *************************************************************************************************/
RF_INT32 RF_input_get_capabilities (struct RF_InputInfo** pp_info, RF_UINT16* p_infosz);

/*************************************************************************************************
 *Description：Lists the available communication technologies
 *Arguments:
 *		pp_info –Pointer should receive an array of RF _ comminfo containing communication technologies available
 *		p_infosz - Pointer that must receive the size of the array pointed to by PP _ info
 *Return:
 *		RF_SUCCESS - Success to enumerate capabilities
 *		RF_ERR_NOMEMORY - Failed to allocate memories
 *************************************************************************************************/
RF_INT32 RF_comm_get_capabilities (struct RF_CommInfo** pp_info, RF_UINT16* p_infosz);

/*************************************************************************************************
 *Description：Returns which of the modes available chart is being used by the device.
 *Arguments:
 *		P_info - Pointer to the RF structure _ screeninfo that store information about the graphics mode in use
 *Return:
 *		RF_SUCCESS - Success to get screen settings
 *************************************************************************************************/
RF_INT32 RF_screen_get_current_settings( struct RF_ScreenInfo* p_info );

/*************************************************************************************************
 *Description：Selects the graphic mode (and parameters) to be used by the device.
 *Arguments:
 *		P_info - Pointer to the RF structure _ screeninfo that store information about the graphics mode to be used
 *Return:
 *		RF_SUCCESS - Success to set screen settings
 *************************************************************************************************/
RF_INT32 RF_screen_set_current_settings( struct RF_ScreenInfo* p_info );

/*************************************************************************************************
 *Description：Returns the printing modes available is being used by the device.
 *Arguments:
 *		P_info –Pointer to the RF structure _ printerinfo that store information about the printing mode in use
 *Return:
 *		RF_SUCCESS - Success to get printer settings
 *************************************************************************************************/
RF_INT32 RF_printer_get_current_settings( struct RF_PrinterInfo* p_info );

/*************************************************************************************************
 *Description：Selects the printing mode (and parameters) to be used by the device.
 *Arguments:
 *		p_info –Pointer to the RF structure _ printerinfo that store information about the printing mode to be used
 *Return:
 *		RF_SUCCESS - Success to set screen settings
 *************************************************************************************************/
RF_INT32 RF_printer_set_current_settings( struct RF_PrinterInfo* p_info );

#endif
