/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_printer.h
Descrição: Cabeçalho que exporta as funções do imprimador.
Autor    : Andre
Data     : 22/10/2007
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_PRINTER_H_
#define _HF_PRINTER_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/
/*
 * Specifics for HF_API_printer.c
 */
#define HF_PRINTER_ESCCENTER 0x17
#define HF_PRINTER_ESCRIGHT 0x18
#define HF_PRINTER_ESCNORMAL 0x19
#define HF_PRINTER_ESCBOLD 0x1a
#define HF_PRINTER_ESCSEQ 0x1b

#define HF_PRINTER_PRN_AT_LEFT		0
#define HF_PRINTER_PRN_AT_CENTER	1
#define HF_PRINTER_PRN_AT_RIGHT 	2

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_INT32 HF_printer_linefeed(HF_INT32 lines);
HF_INT32 HF_printer_print(const HF_CHAR *text);
HF_INT32 HF_printer_printf(const HF_CHAR *fmt, ...);
HF_INT32 HF_printer_printimage(const HF_IMAGE *image, HF_UINT32 flag);
HF_INT32 HF_printer_check(HF_VOID);
HF_INT32 HF_printer_setFont(HF_FONT_T font);
HF_INT32 HF_printer_getFont(HF_FONT_T *);
HF_INT32 HF_printer_getLineCharCount(HF_FONT_T pFont, HF_UINT32 *charCount);
HF_INT32 HF_printer_paperfeed(HF_VOID);
HF_INT32 HF_printer_printimagebuff(const HF_VOID *buff,HF_UINT32 size);

#endif
