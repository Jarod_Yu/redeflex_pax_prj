/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_upd.h
Descrição: Cabeçalho que exporta as funções de update.
Autor    : Tiago
Data     : 25/03/2009
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_UPD_H_
#define _HF_UPD_H_

typedef struct _HF_UPD_PARAMS_ {
    /* Prefixo da discagem (configurado no terminal).                                            */
    const HF_CHAR* pabx;
    /* Telefone do servidor.                                                                     */
    const HF_CHAR* phone;
    /* Modo de discagem (tone ou pulse).                                                         */
    const HF_CHAR* dial_mode;
    /* N�mero da campanha (usado pelo servidor ingenico para determinar o pacote que ele enviar� */
    /* para o terminal.                                                                          */
    const HF_CHAR* caller_id;
    /* N�mero l�gico (com ele o servidor hypercom determina a aplica��o que enviar�              */
    /* para o terminal).                                                                         */
    const HF_CHAR* num_logico;
} HF_UPD_PARAMS;


/***************************************************************************************************************
 * Description:
 * Requests an update of the redeflex to operating system, informing the way which is the new updated package. 
 * The function should also enable the upgrade of OS, kernel, EMV, firmware, and other components.
 * Input Parameter:
 * caminho -The place where new files are redeflex.
 * The redeflex will provide two files in this way:
 * update.def 
 * 	Contains the list of files necessary to upgrade and the size of each.
 * 	01456555.txt,1456
 * 	01456555.txt,1456
 * 	so_kernel_104.bin,10400
 * 	Each line of the file (line separator standard UNIX (\ n) contains the name of the file (in the standard supported natively by the equipment, that is, should not be applied to the verification of the format of the redeflex) and the size (in bytes) of the same.
 * update.bin 
 * 	Contains a sequence of bytes of the file (concatenated following informed in the file update.def.
 * 	The routine is to read the name and size of each file and "extract" the update.bin files to an area which can be used by the device and which is outside the scope of the file system of the redeflex (because the names of the files need not comply with the restrictions imposed by the redeflex).
 * 	The list and file name is specific to each device, the redeflex imposes no restriction or recommendation.
 * Output Parameter:nothing
 * Return:
 * HF_SUCCESS - update success
 * HF_ERR_DEVICEFAULT - update fail
 * HF_ERR_INVALIDARG - one of the arguments is invalid
 ***************************************************************************************************************/
HF_INT32 HF_upd_autocargaHiperflex (const HF_CHAR *caminho );

HF_INT32 HF_upd_downloadHiperflex( const HF_UPD_PARAMS* params );

#endif
