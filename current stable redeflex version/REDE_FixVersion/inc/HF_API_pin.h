/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_pin.h
Descrição: Cabeçalho que exporta as funções de senha.
Autor    : Richardson
Data     : 15/07/2009
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_PIN_H_
#define _HF_PIN_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

// Descri��o:
// Captura uma senha de forma segura, utilizando uma MK pr�pria de PIN, cifrando com 3DES.
//
// Argumentos:
// �	minPinSize � Tamanho m�nimo da senha. Se o terminal n�o suportar, pode ser ignorado.
// �	maxPinSize � Tamanho m�ximo da senha. Se o terminal n�o suportar, pode ser ignorado. M�ximo de 16 d�gitos.
// �	firstLine � Texto que deve ser exibido na primeira linha do display. M�ximo de 16 caracteres.
// �	secondLine � Texto que deve ser exibido na segunda linha do display. M�ximo de 16 caracteres.
// �	timeout � Tempo, em milissegundos, que a tela de captura de senha deve esperar por uma tecla.
// �	pan � N�mero do cart�o. No formato ASCII. Ser� utilizado para formatar o PINBLOCK no formato ISO-0.
// �	keyID � Posi��o de mem�ria onde a MasterKey est� armazenada.
// �	workingkey � Chave de sess�o cifrada com a MasterKEY. Ela ser� utilizada para cifrar a senha.
// �	pinBlock � Aponta para o local onde a senha cifrada ser� armazenada. Tamanho de 8 bytes.
// Retorno:
// �	HF_SUCCESS � Se a fun��o executou com sucesso.
// �	HF_TIMEOUT � Timeout.
// �	HF_NO_PIN � A tecla ENTRA foi pressionada com a senha vazia.
// �	HF_CANCELED � A tecla CANCELA foi pressionada.
// �	HF_ERR_DEVICEFAULT � Falha interna do dispositivo

HF_INT32 HF_pin_getPin_MK3DES(HF_INT8 minPinSize, HF_INT8 maxPinSize,
							HF_CHAR *firstLine, HF_CHAR *secondLine, HF_INT32 timeout, HF_CHAR *pan, 
							HF_UINT16 keyID, const HF_INT8 *workingkey, HF_INT8 *pinBlock )	;


// Descri��o:
// Captura uma senha de forma segura, utilizando DUKPT.
//
// Argumentos:
// �	minPinSize � Tamanho m�nimo da senha. Se o terminal n�o suportar, pode ser ignorado.
// �	maxPinSize � Tamanho m�ximo da senha. Se o terminal n�o suportar, pode ser ignorado. M�ximo de 16 d�gitos.
// �	firstLine � Texto que deve ser exibido na primeira linha do display. M�ximo de 16 caracteres.
// �	secondLine � Texto que deve ser exibido na segunda linha do display. M�ximo de 16 caracteres.
// �	timeout � Tempo, em milisegundos, que a tela de captura de senha deve esperar por uma tecla.
// �	pan � N�mero do cart�o. No formato ASCII. Ser� utilizado para formatar o PINBLOCK no formato ISO-0.
// �	keyID � Posi��o de mem�ria onde a MasterKey est� armazenada.
// �	pinBlock � Aponta para o local onde a senha cifrada ser� armazenada. Tamanho de 8 bytes.
// �	ksn � Aponta para o local onde o KSN retornado na utiliza��o do DUKPT ser� armazenado. Tamanho igual a 10 bytes.
// Retorno:
// �	HF_SUCCESS � Se a fun��o executou com sucesso.
// �	HF_TIMEOUT � Timeout.
// �	HF_NO_PIN � A tecla ENTRA foi pressionada com a senha vazia.
// �	HF_CANCELED � A tecla CANCELA foi pressionada.
// �	HF_ERR_DEVICEFAULT � Falha interna do dispositivo

HF_INT32 HF_pin_getPin_DUKPT(HF_INT8 minPinSize, HF_INT8 maxPinSize,
							HF_CHAR *firstLine, HF_CHAR *secondLine, HF_INT32 timeout, HF_CHAR *pan, 
							HF_UINT16 keyID, HF_INT8 *pinBlock, HF_INT8 *ksn);

#endif
