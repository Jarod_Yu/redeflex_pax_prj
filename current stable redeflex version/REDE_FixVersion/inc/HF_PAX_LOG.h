/*------------------------------------------------------------
* FileName: HF_PAX_LOG.H.h
* Author: Admin
* Date: 2015-09-11
------------------------------------------------------------*/
#ifndef HF_PAX_LOG_H_H
#define HF_PAX_LOG_H_H

//#define GSM_Log
//#define Module_Log
//#define Sha1_Log
//#define RSA_Log
//#define fs_Log
//#define Socket_Log
//#define des_Log
//#define BC_Log
//#define thread_Log

//#define PAX_LOG



#define MAX_CHARS_RS232 (1 * 1024)



void PaxLog_GSM(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_Module(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_Sha1(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_RSA(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_fs(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_Socket(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_3des(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_BC(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

void PaxLog_thread(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);



void PaxLog(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...);

#endif
