/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_PPComp.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Renato
Data     : 27/03/2015
Empresa  : 
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef HF_EXTERNAL_PPCOMP_H
#define HF_EXTERNAL_PPCOMP_H

#ifndef _BC_H_

/* C�digos de erro do PIN-Pad Compartilhado */

#define PP_OK             0
#define PP_PROCESSING     1
#define PP_NOTIFY         2

#define PP_F1             4
#define PP_F2             5
#define PP_F3             6
#define PP_F4             7
#define PP_BACKSP         8

#define PP_INVCALL       10
#define PP_INVPARM       11
#define PP_TIMEOUT       12
#define PP_CANCEL        13
#define PP_ALREADYOPEN   14
#define PP_NOTOPEN       15
#define PP_EXECERR       16
#define PP_NOFUNC        18
#define PP_ERRMANDAT     19

#define PP_TABEXP        20
#define PP_TABERR        21
#define PP_NOAPPLIC      22

#define PP_PORTERR       30
#define PP_COMMERR       31
#define PP_UNKNOWNSTAT   32
#define PP_RSPERR        33
#define PP_COMMTOUT      34

#define PP_INTERR        40
#define PP_MCDATAERR     41
#define PP_ERRPIN        42
#define PP_NOCARD        43

#define PP_SAMERR        50

#define PP_DUMBCARD      60
#define PP_ERRCARD       61
#define PP_CARDINV       62
#define PP_CARDBLOCKED   63
#define PP_CARDNAUTH     64
#define PP_CARDEXPIRED   65
#define PP_CARDERRSTRUCT 66
#define PP_CARDINVALIDAT 67
#define PP_CARDPROBLEMS  68
#define PP_CARDINVDATA   69
#define PP_CARDAPPNAV    70
#define PP_CARDAPPNAUT   71
#define PP_NOBALANCE     72
#define PP_LIMITEXC      73

#else

#endif

#define PP_MENU_MAX_BUTTONS_SCREEN_SINGLE_COLUMN 4
#define PP_MENU_MAX_BUTTONS_SCREEN_TWO_COLUMNS 8

/* Fun��es de controle */
HF_INT32 HF_PP_Open (HF_CHAR * psCom);
HF_INT32 HF_PP_Close (HF_CHAR * psIdleMsg);
HF_INT32 HF_PP_Abort (HF_VOID);

/* Fun��es b�sicas de pinpad */

HF_INT32 HF_PP_GetInfo (HF_CHAR * psInput, HF_CHAR * psOutput);
HF_INT32 HF_PP_DefineWKPAN (HF_CHAR * psInput, HF_CHAR * psOutput);
HF_INT32 HF_PP_Display (HF_CHAR * psMsg);
HF_INT32 HF_PP_DisplayEx (HF_CHAR * psMsg);
HF_INT32 HF_PP_StartGetKey (HF_VOID);
HF_INT32 HF_PP_GetKey (HF_VOID);
HF_INT32 HF_PP_StartGetPIN (HF_CHAR * psInput);
HF_INT32 HF_PP_GetPIN (HF_CHAR * psOutput, HF_CHAR * psMsgNotify);
HF_INT32 HF_PP_StartCheckEvent (HF_CHAR * psInput);
HF_INT32 HF_PP_CheckEvent (HF_CHAR * psOutput);
HF_INT32 HF_PP_EncryptBuffer (HF_CHAR * psInput, HF_CHAR * psOutput);
HF_INT32 HF_PP_GetDUKPT (HF_CHAR * psInput, HF_CHAR * psOutput);
HF_INT32 HF_PP_StartChipDirect (HF_CHAR * psInput);
HF_INT32 HF_PP_ChipDirect (HF_CHAR * psOutput);
HF_INT32 HF_PP_StartRemoveCard (HF_CHAR * psMsg);
HF_INT32 HF_PP_RemoveCard (HF_CHAR * psMsgNotify);
HF_INT32 HF_PP_StartGenericCmd (HF_CHAR * psInput);
HF_INT32 HF_PP_GenericCmd (HF_CHAR * psOutput, HF_CHAR * psMsgNotify);

/* Fun��es de processamento de cart�o */

HF_INT32 HF_PP_StartGetCard (HF_CHAR * psSrvConPar, HF_CHAR * psInput);
HF_INT32 HF_PP_GetCard (HF_CHAR * psOutput, HF_CHAR * psSrvErrMsg, HF_CHAR * psMsgNotify);
HF_INT32 HF_PP_ResumeGetCard (HF_VOID);
HF_INT32 HF_PP_ChangeParameter (HF_CHAR * psInput);
HF_INT32 HF_PP_StartGoOnChip (HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psTagsOpt);
HF_INT32 HF_PP_GoOnChip (HF_CHAR * psOutput, HF_CHAR * psMsgNotify);
HF_INT32 HF_PP_FinishChip (HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psOutput);

/* Fun��es para manuten��o das tabelas */

HF_INT32 HF_PP_TableLoadInit (HF_CHAR * psInput);
HF_INT32 HF_PP_TableLoadRec (HF_CHAR * psInput);
HF_INT32 HF_PP_TableLoadEnd (HF_VOID);
HF_INT32 HF_PP_GetTimeStamp (HF_CHAR * psInput, HF_CHAR * psOutput);

/*******************************************
********** Callbacks de Tela  **************
********************************************/
/* Limpa a tela */
typedef RF_INT16 (*RF_API_BC_callback_clear_screen)(RF_VOID*);
/* Exibir mensagem na tela */
typedef RF_INT16 (*RF_API_BC_callback_show_message)(RF_CHAR* message);
/*Exibir menu e retornar op��o selecionada pelo usu�rio */                     
typedef RF_INT16 (*RF_API_BC_callback_show_menu)(RF_CHAR* title, HF_CHAR** itens, RF_UINT8 num_itens,RF_UINT8 timeout);
/*Exibir tela de captura de senha*/
typedef RF_INT16 (*RF_API_BC_callback_show_password_info)(RF_CHAR* message, RF_UINT8 num_caracteres); 

struct RF_API_BC_callback_reg
{
  RF_API_BC_callback_clear_screen cb_screen;
  RF_API_BC_callback_show_message cb_show_msg;
  RF_API_BC_callback_show_menu cb_show_menu;
  RF_API_BC_callback_show_password_info cb_show_pass_info;
};

/* Registra os callbacks*/
/* Deve ser executada logo apos o PP_OPEN */
RF_INT16 RF_API_BC_register_callbacks( struct RF_API_BC_callback_reg );

#endif
