local I_ADIANTAR_PAPEL = 3--5

-- quantidade estimada de linhas que o logo ocupa
local I_LINHAS_LOGO = 3

I_ALINHADO_ESQUERDA = 0
I_ALINHADO_CENTRO 	= 1
I_ALINHADO_DIREITA 	= 2

tImpressao = {
	---
	-- Monta uma imagem com o texto a ser impresso pelo terminal.
	-- @author Arthur Henrique, Reuel Jonathan
	-- @param sStr - Texto a ser impresso
	-- @param fFonteGrande - Flag para indicar fonte grande (true) ou pequena (false). Default: false
	-- @param iAlinhamento - inteiro para indicar alinhamento 1 - Centro, 2 - Direita. Default: 0
	fImprimeLinha = function (sStr , fFonteGrande, iAlinhamento)
		iAlinhamento = iAlinhamento or I_ALINHADO_ESQUERDA
		fFonteGrande = fFonteGrande or false		
		
		if(fFonteGrande) then
			mprinter.setfont("/lua/shared/grande1.spf")
		else
			mprinter.setfont("/lua/shared/pequena1.spf")
		end
		
		if (iAlinhamento == I_ALINHADO_CENTRO) then
			sStr = hutil.completarTexto(sStr, " ", sStr:len() + ( (42 - sStr:len()) /2) , true)
		elseif (iAlinhamento == I_ALINHADO_DIREITA) then
			sStr = hutil.completarTexto(sStr, " ", 42 , true)
		end

		if (sStr ~= nil) then
			mprinter.print(sStr)
		else
			LogDebug('fImprimeLinha - sStr = nil', I_LOG_FATAL, I_LOGDEBUG_TIPO_PRINT)
		end

	end,

	---
	-- Monta uma imagem com o texto a ser impresso pelo terminal.
	-- @author Arthur Henrique, Reuel Jonathan
	-- @param tLinhas - tabela com as linhas a serem impressas, com os campos:
	-- 		sTexto - Texto a ser impresso
	-- 		fFonteGrande (opcional) - flag indicando tamanho da fonte
	-- 		iAlinhamento (opcional) - alinhamento do texto: 1 - centro, 2 - direita
	-- @return Quantidade de linhas impressas
	fImprimir = function (tLinhas)
		local iLinhasImpressas = 0

		if (tLinhas ~= nil) then

			for k, v in pairs(tLinhas) do
				tImpressao.fImprimeLinha(v.sTexto, v.fFonteGrande, v.iAlinhamento)
				iLinhasImpressas = iLinhasImpressas + 1
			end

			printer.linefeed(I_ADIANTAR_PAPEL) -- Imprime cinco linhas em branco para adiantar o papel
			iLinhasImpressas = iLinhasImpressas + I_ADIANTAR_PAPEL
		else
			LogDebug('fImprimir - tLinhas = nil', I_LOG_FATAL, I_LOGDEBUG_TIPO_PRINT)
		end
		
		return iLinhasImpressas
	end,

	---
	--Fun��o para imprimir o logo da REDE em um comprovante
	--@author Reuel Jonathan
	--@return Quantidade de linhas impressas
	fImprimirImagemRede = function ()
		local img = image.load("lrede.bmp")
		printer.printimage(img, "center")
		image.unload(img)

		return I_LINHAS_LOGO
	end,
	
	---
	-- Imprimi linhas em branco
	-- @author Danilo de Souza Pinto
	-- @param iLinhas - quantidade de linhas a serem impressas.
	fAvancarPapel = function (iLinhas)
		if (iLinhas ~= nil) then
			printer.linefeed(iLinhas) -- Imprime cinco linhas em branco para adiantar o papel
		else
			LogDebug('fAvancarPapel - iLinhas = nil', I_LOG_FATAL, I_LOGDEBUG_TIPO_PRINT)
		end
	end,
}