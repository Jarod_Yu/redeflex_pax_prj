require('graph')
graph.init('igcfg.dat')

main = function()
	--return mainBoleto()
	--return  mainConcessionaria()
	--return mainAPP()
	
	local tResp = graph.Especific({fCorban = true, iTempo = 20})
	printer.print('sResp: ' .. tostring(tResp[2]))
end

mainConcessionaria = function (sDadoLido)	
	local sTela = ""
	local sDadoLido, sDadoLido2, sDadoLido3, sDadoLido4, iTamMax = sDadoLido, '', '', '', 12
	local iErro = 0	
	
	local sTitulo = "concession�rias"
	local sTexto = "digite o c�digo de barras"
	repeat
		-- criando um textfield com a imagem I043, e dentro das dimens�es da imagem, inserindo os textos como se estivessem em textfields {}
		-- O mesmo acontece para os bot�es, por�m, bot�es aceitam eventos de touch
		-- '~!' este comando signica que ele deve manter a tela antiga e montar a nova mesclando as duas
		-- '$1' diz a cor do texto e 'F08' fonte a ser utilizada
--[[		sTela = 			
			"~D063~I041~U027~F03" .. 'titulo' .. 								-- Background + titulo			
			"~C~U060~I043{~D009~F08~$3" .. sDadoLido .. "}" .. 					-- Primeira caixa de texto
			"~!~D160~I043{~D009~F08~$3" .. sDadoLido2 .. "}" ..					-- Primeira caixa de texto
			"~D009" .. "~L015" .. "~I004{~F04~$0" .. 'cancela' .. "~K012}" .. 	-- Bot�o esquerdo rodap�
			"~D009" .. "~R015" .. "~I013{~F04~$1" .. 'confirma' .. "~K011}" ..	-- Bot�o direto rodap�
			"~D070" .. "~C" .. "~I008{~F04~$1" .. 'limpar campos' .. "~K013}"	-- Bot�o do centro

]]
			
						--tela de concessionarias
			sTela = 			
			"~D063~I041~U027~F03" .. sTitulo .. 								-- Background + titulo			
			"~U063~L015~F02" .. sTexto ..  					-- campo de texto
			"~U087~L015~I300{~V~C~F01~$3" .. sDadoLido .. "}" .. 					-- Primeira caixa de texto
			"~!~U087~L123~I300{~V~C~F01~$3" .. sDadoLido2 .. "}" ..					-- segunda caixa de texto
			
			"~U115~L015~I300{~V~C~F01~$3" .. sDadoLido3 .. "}" .. 					-- terceira caixa de texto
			"~!~U115~L123~I300{~V~C~F01~$3" .. sDadoLido4 .. "}" ..					-- quarta caixa de texto
			
			"~D009" .. "~L015" .. "~I004{~F03~$0" .. 'cancela' .. "~K012}" .. 	-- Bot�o esquerdo rodap�
			"~D009" .. "~R015" .. "~I013{~F03~$1" .. 'confirma' .. "~K011}" ..	-- Bot�o direto rodap�
			"~D075" .. "~C" .. "~I008{~F04~$1" .. 'limpar campos' .. "~K013}"	-- Bot�o do centro
			
			
		iErro = ui.graphical(sTela)
		
		-- Erro na cria��o da TELA
		if (iErro ~= 0) then
			printer.print("Erro na cria��o da tela..."..iErro)
			return {0, iErro}
		end
		
		-- Fun��o respons�vel por aguardar evento de tecla ou touch xEvt == 4 evento TOUCH xEvt == 8 evento KEYBOARD
		xEvt,xKey = ui.graphical_wait_events(10, 0x000C)
		
		-- TRATANDO OS RETORNOS DO EVENTO
		
		--timeout
		if (xKey == nil) then
			return {0, nil}
		end
		
		-- bot�o cancelar n�o retorna dado lido
		if (xKey == 12 or xKey == 18) then
			return {0, nil}
		
		-- bot�o enter retornando o dado lido
		-- Verifica��es podem ser feitas neste momento, por exemplo se o tamanho do dado atingiu o tamanho minimo.
		elseif (xKey == 11 or xKey == 17) then		
			return {0, sDadoLido .. sDadoLido2..sDadoLido3..sDadoLido4}
		
		-- Evento retornou um valor de 0 a 9		
		elseif ((xKey >= 0) and (xKey <= 10)) then
			-- Corrigindo o valor de KEY que vem da plataforma com n�mero a mais
			xKey = xKey - 1
			
			-- Checando se n�o passou do tamanho m�ximo do campo
			if (#sDadoLido < iTamMax) then
				sDadoLido = sDadoLido .. tostring(xKey) -- Atualiza o dado lido
			
			-- Passou pelo tamanho m�ximo do campo 1, indo para o segundo campo
			elseif (#sDadoLido2 < iTamMax) then
				sDadoLido2 = sDadoLido2 .. tostring(xKey) -- Atualiza o dado lido
			elseif (#sDadoLido3 < iTamMax) then
				sDadoLido3 = sDadoLido3 .. tostring(xKey) -- Atualiza o dado lido
			elseif (#sDadoLido4 < iTamMax) then
				sDadoLido4 = sDadoLido4 .. tostring(xKey) -- Atualiza o dado lido
			else
				-- se passou, n�o deixa inserir mais e emite beep
				keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
			end
		
		-- Tecla para apagar dado
		elseif (xKey == 14) then
			-- Removendo �ltimo digito
			if (#sDadoLido > 0 and #sDadoLido2 == 0) then				
				sDadoLido = string.sub(sDadoLido, 1, #sDadoLido -1)	
				if (#sDadoLido == 0) then
					-- n�o possui dado para apagar
					return mainAPP()
				end
			
			elseif (#sDadoLido2 > 0 and #sDadoLido3 == 0) then
				sDadoLido2 = string.sub(sDadoLido2, 1, #sDadoLido2 -1)	
			elseif (#sDadoLido3 > 0 and #sDadoLido4 == 0) then
				sDadoLido3 = string.sub(sDadoLido3, 1, #sDadoLido3 -1)	
			elseif (#sDadoLido4 > 0) then
				sDadoLido4 = string.sub(sDadoLido4, 1, #sDadoLido4 -1)	
			else   
				-- n�o possui dado para apagar
				return mainAPP()
			end
		
		-- Bot�o limpar campos
		elseif (xKey == 19) then
			sDadoLido, sDadoLido2, sDadoLido3, sDadoLido4  = '', '', '', ''
			return mainAPP()
		else
			 -- Tecla n�o permitida
			keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
		end
				
	until nil
	
	
	printer.print('fim: ' .. tostring(ret))
	
end

mainBoleto = function (sDadoLido)	
	local sTela = ""
	local sDadoLido, sDadoLido2, sDadoLido3, sDadoLido4, sDadoLido5 = sDadoLido, '', '', '', ''
	local iTamMax1, iTamMax2, iTamMax3, iTamMax4, iTamMax5 = 10, 11, 11, 1, 14
	local iErro = 0		
	local sTitulo = "boleto"
	local sTexto = "digite o c�digo de barras"
	repeat

			--tela de boleto
			sTela = 
			"~D063~I041~U027~F03" .. sTitulo .. 								-- Background + titulo
			"~U063~L015~F02" .. sTexto ..  											-- campo de texto
			"~U087~L015~I300{~V~C~F01~$3" .. sDadoLido .. "}" .. 					-- Primeira caixa de texto
			"~U087~L123~I300{~V~C~F01~$3" .. sDadoLido2 .. "}" ..					-- segunda caixa de texto
			
			"~!~U115~L015~I300{~V~C~F01~$3" .. sDadoLido3 .. "}" .. 					-- terceira caixa de texto
			"~U115~L123~I301{~V~C~F01~$3" .. sDadoLido4 .. "}" ..					-- quarta caixa de texto
			
			"~!~U143~L015~I302{~V~C~F01~$3" .. sDadoLido5 .. "}" .. 					-- quinta caixa de texto

			"~D009" .. "~L015" .. "~I004{~F03~$0" .. 'cancela' .. "~K012}" .. 	-- Bot�o esquerdo rodap�
			"~D009" .. "~R015" .. "~I013{~F03~$1" .. 'confirma' .. "~K011}" ..	-- Bot�o direto rodap�
			"~D075" .. "~C" .. "~I008{~F04~$1" .. 'limpar campos' .. "~K013}"	-- Bot�o do centro

		iErro = ui.graphical(sTela)
		
		-- Erro na cria��o da TELA
		if (iErro ~= 0) then
			printer.print("Erro na cria��o da tela..."..iErro)
			return {0, iErro}
		end
		
		-- Fun��o respons�vel por aguardar evento de tecla ou touch xEvt == 4 evento TOUCH xEvt == 8 evento KEYBOARD
		xEvt,xKey = ui.graphical_wait_events(10, 0x000C)
		
		-- TRATANDO OS RETORNOS DO EVENTO
		
		--timeout
		if (xKey == nil) then
			return {0, nil}
		end
		
		-- bot�o cancelar n�o retorna dado lido
		if (xKey == 12 or xKey == 18) then
			return {0, nil}
		
		-- bot�o enter retornando o dado lido
		-- Verifica��es podem ser feitas neste momento, por exemplo se o tamanho do dado atingiu o tamanho minimo.
		elseif (xKey == 11 or xKey == 17) then		
			return {0, sDadoLido .. sDadoLido2..sDadoLido3..sDadoLido4..sDadoLido5}
		
		-- Evento retornou um valor de 0 a 9		
		elseif ((xKey >= 0) and (xKey <= 10)) then
			-- Corrigindo o valor de KEY que vem da plataforma com n�mero a mais
			xKey = xKey - 1
			
			-- Checando se n�o passou do tamanho m�ximo do campo
			if (#sDadoLido < iTamMax1) then
				sDadoLido = sDadoLido .. tostring(xKey) -- Atualiza o dado lido
			
			-- Passou pelo tamanho m�ximo do campo 1, indo para o segundo campo
			elseif (#sDadoLido2 < iTamMax2) then
				sDadoLido2 = sDadoLido2 .. tostring(xKey) -- Atualiza o dado lido
			elseif (#sDadoLido3 < iTamMax3) then
				sDadoLido3 = sDadoLido3 .. tostring(xKey) -- Atualiza o dado lido
			elseif (#sDadoLido4 < iTamMax4) then
				sDadoLido4 = sDadoLido4 .. tostring(xKey) -- Atualiza o dado lido
			elseif (#sDadoLido5 < iTamMax5) then
				sDadoLido5 = sDadoLido5 .. tostring(xKey) -- Atualiza o dado lido
			else
				-- se passou, n�o deixa inserir mais e emite beep
				keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
			end
		
		-- Tecla para apagar dado
		elseif (xKey == 14) then
			-- Removendo �ltimo digito
			if (#sDadoLido > 0 and #sDadoLido2 == 0) then				
				sDadoLido = string.sub(sDadoLido, 1, #sDadoLido -1)	
				if (#sDadoLido == 0) then
					-- n�o possui dado para apagar
					return mainAPP()
				end
			
			elseif (#sDadoLido2 > 0 and #sDadoLido3 == 0) then
				sDadoLido2 = string.sub(sDadoLido2, 1, #sDadoLido2 -1)	
			elseif (#sDadoLido3 > 0 and #sDadoLido4 == 0) then
				sDadoLido3 = string.sub(sDadoLido3, 1, #sDadoLido3 -1)	
			elseif (#sDadoLido4 > 0 and #sDadoLido5 == 0) then
				sDadoLido4 = string.sub(sDadoLido4, 1, #sDadoLido4 -1)	
			elseif (#sDadoLido5 > 0) then
				sDadoLido5 = string.sub(sDadoLido5, 1, #sDadoLido5 -1)	
			else   
				-- n�o possui dado para apagar
				return mainAPP()
			end
		
		-- Bot�o limpar campos
		elseif (xKey == 19) then
			sDadoLido, sDadoLido2, sDadoLido3, sDadoLido4, sDadoLido5  = '', '', '', '', ''
			-- Limpo todos campos e volto para tela inicial para inser��o do primeiro digito
			return mainAPP()
		else
			 -- Tecla n�o permitida
			keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
		end
				
	until nil
	
	
	printer.print('fim: ' .. tostring(ret))
	
end


mainAPP = function ()	
	local sTela = ""
	local sDadoLido, sDadoLido2, sDadoLido3, sDadoLido4, sDadoLido5 = '', '', '', '', ''
	local iTamMax1, iTamMax2, iTamMax3, iTamMax4, iTamMax5 = 10, 11, 11, 1, 14
	local iErro = 0		
	local sTitulo = "pague aqui ita�"
	local sTexto = "digite o c�digo de barras:"
	repeat

		--tela de boleto
		sTela = fGeraTela(sTitulo, sTexto, sDadoLido)

		iErro = ui.graphical(sTela)
		
		-- Erro na cria��o da TELA
		if (iErro ~= 0) then
			printer.print("Erro na cria��o da tela..."..iErro)
			return {0, iErro}
		end
		
		-- Fun��o respons�vel por aguardar evento de tecla ou touch xEvt == 4 evento TOUCH xEvt == 8 evento KEYBOARD
		xEvt,xKey = ui.graphical_wait_events(10, 0x000C)
		
		-- TRATANDO OS RETORNOS DO EVENTO
		
		--timeout
		if (xKey == nil) then
			return {0, nil}
		end
		
		-- bot�o cancelar n�o retorna dado lido
		if (xKey == 12 or xKey == 18) then
			return {0, nil}
		
		-- N�O RETORNA AT� DADO SER TOTALMENTE LIDO, minimo de X caracteres inseridos
		-- bot�o enter retornando o dado lido
		-- Verifica��es podem ser feitas neste momento, por exemplo se o tamanho do dado atingiu o tamanho minimo.
		elseif (xKey == 11 or xKey == 17) then
			if (#sDadoLido == 0) then
				-- input n�o atingiu tamanho minimo 
				keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
			end
		
		-- Evento retornou um valor de 0 a 9		
		elseif ((xKey >= 0) and (xKey <= 10)) then
			-- Corrigindo o valor de KEY que vem da plataforma com n�mero a mais
			xKey = xKey - 1
			
			--tela de boleto
			sTela = fGeraTela(sTitulo, sTexto, tostring(xKey))

			iErro = ui.graphical(sTela)	
			management.sleep(150)
						
			-- Conforme regra da FEBRABAN se o c�digo iniciar pelo n�mero 8 a tela exibida deve ser de 
			-- pagamento por concession�ria, qualquer outro n�mero que iniciar a digita��o a tela a ser 
			-- apresentada � a de inser��o do c�digo de boleto.
			if (xKey == 8) then
				return  mainConcessionaria(tostring(xKey))
			else
				return mainBoleto(tostring(xKey))
			end
		
		-- Tecla para apagar dado
		elseif (xKey == 14) then
			-- Removendo �ltimo digito
			if (#sDadoLido > 0) then				
				sDadoLido = string.sub(sDadoLido, 1, #sDadoLido -1)				
			else   
				-- n�o possui dado para apagar
				keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
			end
		
		-- Bot�o limpar campos
		elseif (xKey == 19) then
			sDadoLido = ''
		else
			 -- Tecla n�o permitida
			keyboard.buzzer (BEEP_SHORT,BEEP_MIDTONE)
		end
				
	until nil
	
	
	printer.print('fim: ' .. tostring(ret))
	
end

function fGeraTela (sTitulo, sTexto, sDadoLido) 
	--tela de boleto
	sTela = 
	"~D063~I041~U027~F03" .. sTitulo .. 								-- Background + titulo
	"~U063~L015~F02" .. sTexto ..  											-- campo de texto
	"~U087~L015~I301{~V~C~F01~$3" .. sDadoLido .. "}" .. 					-- Primeira caixa de texto

	"~D009" .. "~L015" .. "~I004{~F03~$0" .. 'cancela' .. "~K012}" .. 	-- Bot�o esquerdo rodap�
	"~D009" .. "~R015" .. "~I013{~F03~$1" .. 'confirma' .. "~K011}" ..	-- Bot�o direto rodap�
	"~D075" .. "~C" .. "~I008{~F04~$1" .. 'limpar campos' .. "~K013}"	-- Bot�o do centro
			
	return sTela
end



