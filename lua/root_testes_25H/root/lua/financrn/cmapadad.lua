---
--Mapeamento das tabelas de configura��o
--@author Danilo Oliveira
--@release Vers�o inicial 1.0
--@copyright CESAR

--[[
Tipos de dados:
i - inteiro
s - string (texto)
f - booleano (S/N)
]]

MAPA_DADO = {
	Emissor = {
		arquivo = "emissor.tbl",
		podeApagar = true,
		mapa =
		{
			{ "Id"     						,   4   , "i"},   --   1. Id do Registro
			{ "Nome"   						,  20   , "s"},   --   2. Nome a ser impresso nos comprovantes offline
			
			--Options 1 - MSB is Most Significant Bit
			{ "VerificarModCartao"    		,   1   , "f"},   --   4. Verificar o MOD 10 do cart�o?         (S/N)
			{ "TransacaoTrilhaCVC2" 		,   1   , "f"},   --   5. Transa��o c/ trilha coleta CVC2?      (S/N)
			{ "ChipRequerConfirmacao"    	,   1   , "f"},   --   6. Chip requer Confirma��o Positiva?     (S/N)
			{ "ColetarTrilhaUm"    			,   1   , "f"},   --   7. Coleta Trilha 1?                      (S/N)
			{ "ColetarTrilhaDois"    		,   1   , "f"},   --   8. Coleta Trilha 2?                      (S/N)
			{ "ColetarQuatroUltDigitTrilha" ,   1   , "f"},   --   9. Trans. c/ trilha coleta 4 �lt.d�git.? (S/N)
			{ "PinEntryRequired"    		,   1   , "f"},   --  10. PIN Entry Required?                   (S/N)
			{ "ColetarServiceCodePin"    	,   1   , "f"},   --  11. Service Code coleta de PIN?           (S/N)
			----------------------------------------------------------------------------------------------------
			--Options 2 - MSB Most Significant Bit
			{ "fValidarServiceCode"					,   1   , "f"},   --  12. Valida 1a pos. do Service Code?       (S/N)
			{ "SolicitarChip"	     				,   1   , "f"},	  --  13. Obriga solicitar CHIP?                (S/N)
			{ "UsoFuturo"     						,   1   , "f"},	  --  14. Uso futuro
			{ "ChecarDataExpiracao"  				,   1   , "f"},	  --  15. Check Expiry Date (default = no)    	(S/N)
			{ "UsoFuturo"     						,   1   , "f"},	  --  16. Uso futuro
			{ "UsoFuturo"     						,   1   , "f"},	  --  17. Uso futuro
			{ "UsoFuturo"     						,   1   , "f"},	  --  18. Uso futuro
			{ "UsoFuturo"     						,   1   , "f"},	  --  19. Uso futuro
			----------------------------------------------------------------------------------------------------
			--Options 3 - MSB is Most Significant Bit
			{ "UsoFuturo"     				,   1   , "f"},   --  20. Uso futuro
			{ "PrivateLabel"   		  		,   1   , "f"},	  --  21. Private Label 						(S/N)
			{ "UsoFuturo"     				,   1   , "f"},	  --  22. Uso futuro
			{ "UsoFuturo"  					,   1   , "f"},	  --  23. Uso futuro
			{ "VoucherFrota"   				,   1   , "f"},	  --  24. Uso futuro
			{ "Credito"     				,   1   , "f"},	  --  25. Cr�dito								(S/N)
			{ "Debito"     					,   1   , "f"},	  --  26. D�bito								(S/N)
			{ "Voucher"     				,   1   , "f"},	  --  27. Voucher                               (S/N)
			----------------------------------------------------------------------------------------------------
			--Options 4 - MSB is Most Significant Bit
			{ "SolicitarCVC2TransDigitada"	,   1   , "f"},   --  28. Trans. digitada solicita CVC2?        (S/N)
			{ "AceitarCVC2Inexistente"	  	,   1   , "f"},	  --  29. Aceita CVC2 inexistente ou ileg�vel?  (S/N)
			{ "ColetarQuatroUltDigitDig"	,   1   , "f"},	  --  30. Trans. digitada solicita 4 �lt. d�g.? (S/N)
			{ "ImprimirCNE"  				,   1   , "f"},	  --  31. Imprime CNE (Tag 9F0B)?               (S/N)
			{ "HabilitaCvc5posicoes"		,   1   , "f"},	  --  32. Habilita 5 posi��es CVC2(False = 3 posi.)
			{ "ImprimirApplicationLabel"	,   1   , "f"},	  --  33. Imprime Application Label(9F06)
			{ "HabilitaMascaraPreAut"		,   1   , "f"},	  --  34. Habilita Mascara p/ Cart�o na Pr�-Aut (S/N)
			{ "NaoUtilizado"     			,   1   , "f"},	  --  35. N�o utilizado
			-----------------------------------------------------------------------------------------------------
			{ "MascaraCartaoPI" 			,   2   , "i"},   --  36. Posi��o inicial para preencher o PAN com "*"
			{ "MascaraCartaoQD" 			,   2   , "i"},   --  37. Quantidade de d�gitos exibidos a partir do d�gito menos significativo
			{ "ValorMinimoConfirmPositiva"  ,   4   , "i"},   --  38. 0000-9999 � os par�metros de confirma��o positiva dever�o ser coletados durante o processamento de uma transa��o, cujo valor seja superior ao dado contido neste par�metro. Obs.: valor inteiro (sem centavo)
			{ "QuantidadePromptsGrupo1"     ,   2   , "i"},   --  39. Quantidade m�xima de prompts a solicitar para o Primeiro Grupo 01h .. 0Eh (00 indica que issuer n�o configura Confirma��o Positiva)
			{ "QuantidadePromptsGrupo2"     ,   2   , "i"},   --  40. Quantidade m�xima de prompts a solicitar para o Segundo Grupo 01h .. 0Eh
			{ "QuantidadePromptsGrupo3"     ,   2   , "i"},   --  41. Quantidade m�xima de prompts a solicitar para o Terceiro Grupo
			{ "QuantidadePromptsGrupo4"     ,   2   , "i"},   --  42. Quantidade m�xima de prompts a solicitar para o Quarto Grupo Tamb�m utilizado como PROMPT DISTRIBUTION na transa��o de captura distribui��o
			{ "NumMaxDigConfirmPositiva"    ,   2   , "i"}    --  43. A quantidade total de d�gitos a coletar para a Confirma��o Positiva n�o deve ultrapassar este limite: Range: 00h .. 0Eh Ex. se m�ximo de d�gitos acatados = 12, este par�metro deve estar preenchido com 0C.
		}
	},

	Prompt = {
		arquivo = "prompt.tbl",
		podeApagar = true,
		mapa = {
			{ "NumTabela"     				,   4   , "i"}, -- Table Number -01h � FFh Id. do Grupo de Dados 2 N Cada grupo pode conter n (01h
			{ "IdGrupoDados" 				,   4   , "i"}, -- Cada grupo pode conter n (01h a FFh) prompts. Range: 01h.. FFh Ex. Grupo de Dados de Nascimento (contendo os prompts 01h, 02h, 16h e FEh) ou Grupo de Documentos Pessoais (contendo os prompts (03h e 04h)
			{ "IdPrompt"     				,   4   , "i"}, -- Range: 01h .. FFh Obs.: este n�mero deve ser �nico, e ser� utilizado pelo emissor para a identifica��o de quais dados foram coletados pelo terminal.
			{ "Prompt"						,   16  , "s"}, -- Corresponde ao texto que deve ser exibido em display do Pinpad, solicitando o dado ao portador do cart�o Ex: �M�S NASC:�
			{ "NumDigitosColeta"			,   2   , "i"}, -- N�mero de d�gitos a coletar no PinPad, correspondente ao prompt em tratamento Range: 01h � 0Eh Ex: para M�S NASC ter�amos 02h de d�gitos a coletar
			--Options 1
			{ "PromptObrigatorio"    		,   1   , "f"}, -- (S/N) (�S� - prompt obrigat�rio para esse grupo )
			{ "Mascarado" 					,   1   , "f"}, -- (S/N) (�S� - ecoa asteriscos (*) durante a digita��o do PINPAD)
			{ "Unused"          			,   1   , "f"},
			{ "Unused"    					,   1   , "f"},
			{ "Unused"    					,   1   , "f"},
			{ "Unused" 						,   1   , "f"},
			{ "Unused"    					,   1   , "f"},
			{ "Unused"    					,   1   , "f"}
		}
	},

	Grupos = {
		arquivo = "grupos.tbl",
		podeApagar = true,
		mapa = {
			{ "NumTabela"    				,   4   , "i"}, -- De 01h a FFh
			{ "IdGrupo"    					,   4   , "i"}, -- Identificador do Grupo
			{ "Emissor"    					,   4   , "i"}  -- ID do registro da tabela emissor
		}
	},

	Erro = {
		arquivo = "erro.dbt",
		podeApagar = true,
		mapa = {
			{ "Mensagem"    				,   255  , "s" }
		}
	},

	BinRange = {
		arquivo = "binrange.tbl",
		podeApagar = true,
		mapa = {
			{"iEmissorId"		, 4, "i"}, --Id do Emissor relacionado.
			{"NumeroDeDigitos"  , 2, "i"}, --N�mero x de d�gitos do PAN a serem considerados
			{"PANtamBaixo"		, 19, "s"}, -- Low value of PAN starting digits. Exemplo: se  x = 6, poss�veis valores: 000000-999999.
			{"PANtamAlto"		, 19, "s"}  --High value of PAN starting digits.

		}
	},

	Lojista = {
		arquivo = "lojista.tbl",
		podeApagar = true,
		mapa = {
			{"NumEstabelecimento" , 15, "s"}, --N�mero do estabelecimento no cadastro (PV). Enviado no bit 42. EMV: tag "9F16"".
			{"NumTerm" 			  ,  8, "s"}, --N�mero l�gico do terminal. Enviado no bit 41. EMV: tags "9F1C"" e "9F1E"".
			{"NomeLoja" 		  , 23, "s"}, --Nome do estabelecimento, que ser� impresso no comprovante.
			{"CNPJ" 			  , 14, "s"}, --N�mero do CNPJ do estabelecimento. Sem formata��o (sem m�scara).
			{"Endereco"			  , 23, "s"}, --Endereço do estabelecimento, que ser� impresso no comprovante
			{"Cidade" 			  , 17, "s"}, --Cidade de localiza��o do estabelecimento, que ser� impresso no comprovante.
			{"CEP" 				  ,  8, "s"}, --CEP da localiza��o do estabelecimento.
			{"Telefone"			  , 17, "s"}, --Telefone do estabelecimento, que ser� impresso no comprovante. Sem formata��o. Ex: 21212200
			{"MCCC"				  ,  4, "i"}, --Merchant Category Country Code: Parâmetro de configura��o EMV tag “9F15”
		}
	},

	Criptografia = {
		arquivo = "cript.tbl",
		podeApagar = true,
		mapa = {
			{"sIndiceChave"      	  , 2, "s"}, -- Posi��o no banco de chaves do terminal
			
			--Tipo da Chave:
			{"f3DESDados"              , 1, "f"},
			{"f3DESSenha"              , 1, "f"},
			{"fDUKPTDados"             , 1, "f"}, -- UsoFuturo
			{"fDUKPTSenha"             , 1, "f"},
			{"fDESDados"               , 1, "f"}, -- Somente para PDV Discado/IP
			{"fDESSenha"               , 1, "f"}, -- Somente para PDV Discado/IP
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			----------------------------------------------------------------------------------------------------
			{"sTamanhoChave"     	  , 2, "s"}, --Tamanho da chave (para DUKPT este campo vem preenchido com zeros)
			{"sWorkingKey"       	  ,32, "s"}, --Para chave DES (completado com �F� � direita) Para chave DUKPT (campo preenchido com zeros)
			{"iWorkingKeyCheckValue"  , 8, "i"}, --Cont�m os 4 primeiros bytes de check value da working-key Cont�m FFFFFFFF quando dado n�o dispon�vel

			--Uso da chave(Options 1):
			{"fPrincipal"              , 1, "f"},
			{"fContingencia"           , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"},
			{"fUsoFuturo"              , 1, "f"}
			----------------------------------------------------------------------------------------------------
		}
	}, 

	ChipData = {
		arquivo = "chipdata.tbl",
		podeApagar = true,
		mapa = {
			-- {"IdRegistro"         , 4,  "i"}, -- Id do Regsitro
			{"ICCAID"             , 32, "s"}, --Identifica��o do AID (Tag EMV 9F06)
			--Options 1 - MSB is Most Significant Bit
			{"ICCOptions1"     	  , 2,  "s"},
			----------------------------------------------------------------------------------------------------
			--Options 2 - MSB is Most Significant Bit
			{"UsoFuturo"      	  , 1,  "f"},
			{"UsoFuturo"      	  , 1,  "f"},
			{"UsoFuturo"          , 1,  "f"},
			{"UsoFuturo"          , 1,  "f"},
			{"UsoFuturo"          , 1,  "f"},
			{"Credito"       	  , 1,  "f"},
			{"Debito"      	  	  , 1,  "f"},
			{"Voucher"      	  , 1,  "f"},
			----------------------------------------------------------------------------------------------------
			--Options 3 - MSB is Most Significant Bit
			{"PINparaICC"      	  , 1,  "f"}, -- Plain Text PIN for ICC Verification
			{"CriptografarPINon"  , 1,  "f"}, -- Enciphered PIN for Online Verification
			{"Assinatura"      	  , 1,  "f"}, -- Signature (Paper)
			{"CriptografarPINoff" , 1,  "f"}, -- Enciphered PIN for Offline Verification
			{"SemCVM" 		  	  , 1,  "f"}, -- Plain Text PIN for ICC Verification
			{"UsoFuturo"          , 1,  "f"},
			{"UsoFuturo"  		  , 1,  "f"},
			{"UsoFuturo"          , 1,  "f"}, 
			----------------------------------------------------------------------------------------------------
			--Options 4 - MSB is Most Significant Bit
			{"UsoFuturo"      	  , 1,  "f"},
			{"UsoFuturo"      	  , 1,  "f"},
			{"UsoFuturo"      	  , 1,  "f"},
			{"UsoFuturo"     	  , 1,  "f"},
			{"UsoFuturo"     	  , 1,  "f"},
			{"UsoFuturo"     	  , 1,  "f"},
			{"UsoFuturo"     	  , 1,  "f"},
			{"HabilitaOff"        , 1,  "f"},
			----------------------------------------------------------------------------------------------------
			{"TAIDVER1"         , 4,  "s"}, -- Terminal AID�s Version Number 1 (Tag 9F09) -- Usamos o valor em HEXA mesmo.
			{"TAIDVER2"         , 4,  "s"}, -- Terminal AID�s Version Number 2 (Tag 9F09)
			{"TAIDVER3"         , 4,  "s"}, -- Terminal AID�s Version Number 3 (Tag 9F09)
			{"TAIDTDOL"         , 40, "s"}, -- Terminal Default TDOL
			{"TAIDDDOL"         , 40, "s"}, -- Terminal Default DDOL
			{"RC_Offl_APP"      , 2,  "s"}, -- Offline Approved
			{"RC_Offl_DEC"      , 2,  "s"}, -- Offline Declined Response Code
			{"RC_Unab_Ap"       , 2,  "s"}, -- Unable to go Online (Offline Approved) Response Code
			{"RC_Unab_Dc"       , 2,  "s"}, -- Unable to go Online (Offline Declined) Response Code
			{"Icctargpct"       , 2,  "i"}, -- Target Percentage for Biased Random Selection
			{"Iccmaxpct"        , 2,  "i"}, -- Max. Target Percentage for Biased Random Selection
			{"Iccthresh"        , 4,  "i"}, -- Threshold Value for Biased Random Selection
			{"Icctacden"        , 10,  "s"}, -- TAC Terminal Action Code Denial
			{"Icctaconl"        , 10,  "s"}, -- TAC Terminal Action code online
			{"Icctacdef"        , 10,  "s"}, -- TAC Terminal action code Default
			{"TransCategoria"   , 1,  "s"}, -- Icc_tcc default �R� Tag EMV �9F53�
			--Terminal Contactless Transaction Limit � 00000000 a 99999999 (com centavos) at� R$ 999.999,99. Apenas para transa��es contactless.
			{"CtlsTransLimite"  , 8,  "s"}, -- Acima deste valor, cart�o contactless n�o deve ser aceito.
			{"FloorLimit"   	, 8,  "s"}, -- Se a transa��o for realizada com um valor menor que este, o terminal poder� aprovar localmente, off-line.
			{"CtlsCVMlimite"    , 8,  "s"}, -- Acima deste valor, CVM ser� pedido. Ex: senha.
			{"BIT55"            , 40, "s"},
			{"Status"			, 2,  "i"}
		}
	},

	Transacao = {
		arquivo = "trans.dbt",
		podeApagar = true,
		mapa = {
			{ "sTipoTran"		, 4 ,  "s" }, -- C�digo da transa��o (ex.: Cr�dito - 0350)ChipData
			{ "sNSU"			, 12,  "s" }, -- Gerado para cada transa��o como um ID.
			{ "sPANMascarado"	, 30,  "s" }, -- PAN mascarado
			{ "sValor"			, 8 ,  "s" }, -- Valor da transa��o
			{ "sData"			, 8 ,  "s" }, -- Data da transa��o
			{ "sHora"			, 4 ,  "s" }, -- Hora da transa��o
			{ "iEmissorId"		, 4 ,  "i" }, -- ID do emissor
			{ "sCV"				, 12,  "s" },
			{ "sNomeEmissor"	, 20,  "s" }, -- Nome do Emissor
			{ "fServico"		, 1 ,  "f" }  -- Flag de servico
		}
	},

	ControleLote = {
		arquivo = "ctrllote.dbt",
		podeApagar = true,
		mapa = {
			{ "iNumeroLote"         , 6   ,   "i" }
		}
	},
	
	Lote = {
		arquivo = "lote.dbt",
		podeApagar = true,
		mapa = {
			{ "sTipoTran"           , 4   ,   "s" },
			{ "sCV"                 , 12  ,   "s" },
			{ "sTipoTranOrig"       , 4   ,   "s" },
   			{ "iEmissorId"          , 4   ,   "i" }, -- ID do emissor
   			{ "sNomeEmissor"        , 20  ,   "s" }, -- Nome do Emissor, desce do Host
   		    { "iPrnOffset"          , 32  ,   "s" }, -- Posi��o do Bit63 (�ndice)
   			{ "iPrnTamanho"         , 16  ,   "s" }, -- Tamanho do Bit63
			{ "sNomeCliente"        , 255 ,   "s" }, -- Nome do cliente - 5F20
            { "sCardholdExt"        , 45  ,   "s" }, -- Cardholder Name Extended - 9F0B
   			{ "sPANMascarado"       , 255 ,   "s" }, -- PAN do cart�o
   			{ "sPANCript"           , 255 ,   "h" }, -- PAN do cart�o
   			{ "iTamPANCript"        , 64  ,   "s" }, -- Tamanho do nro do cartao
		   	{ "sTrilha1"            , 255 ,   "h" }, -- Trilha 1 do cart�o criptografia
		    { "sTrilha2"            , 255 ,   "h" }, -- Trilha 2 do cart�o criptografia 
		    { "sCNPJEstab"          , 255 ,   "s" }, -- CNPJ do estabelecimento
		  
		    { "fEnviada"            , 1   ,   "f" }, -- 7 Off line enviada sem resposta
		    { "fEnviadaOk"          , 1   ,   "f" }, -- 6 Off line enviada com resposta
		    { "fTrilha1"            , 1   ,   "f" }, -- 5 Envia trilha 1 == tEmissor.ColetarTrilhaUm
		    { "fEMVSenhaOffOK"      , 1   ,   "f" }, -- 4 Senha foi verificada off line
		    { "fEMVSenhaOnLine"     , 1   ,   "f" }, -- 3 Senha deve ser verificada on line
		    { "fEMVNegado"          , 1   ,   "f" }, -- 2 Transa��o com chip negada
		    { "fSenha"          	, 1   ,   "f" }, -- 1 Transa��es magn�ticas onde o cart�o opera com senha
		    { "fAssinatura"     	, 1   ,   "f" }, -- 0 Exige a assinatura no cupom
		 
		    { "fModoOnLine"         , 1   ,   "f" }, -- 7 Modo da conex�o, online ou offline
		    { "fModoOnLineOrig"     , 1   ,   "f" }, -- 6
		    { "fQuedaEnergia"       , 1   ,   "f" }, -- 5 Queda de energia
		    { "fFallbackMagnetico"  , 1   ,   "f" }, -- 
		    { "fFallbackDigitado"   , 1   ,   "f" }, -- 
		    { "fConfPositiva"       , 1   ,   "f" }, -- 3 Houve coleta de conf positiva ou referida ?
		    { "fPANSeqNbExiste"     , 1   ,   "f" }, -- 2 Se false, iPAN_SeqNb n�o existe
		    { "fTrilha2"            , 1   ,   "f" }, -- 1 Envia trilha 2 == tEmissor.ColetarTrilhaDois
		    
		    { "fTrsIntegrada"       , 1   ,   "f" }, -- 0
		    { "fCartaoInternac"     , 1   ,   "f" }, -- 7 Indica que o cart�o foi emitido fora do pa�s
		    { "fVoucherDsp"         , 1   ,   "f" }, -- 6 flag p/ controlar se transa��o voucher foi exibida em display  

		    { "fVlrMenorCVMLim"     , 1   ,   "f" }, -- 5 True, caso uma transa��o CONTACTLESS tenha valor menor do que o CVM Limit
		  
		    { "sData"               , 6   ,   "s" }, -- Byte vbBCD_DateTime[6],   , -- Date DDMMAA (3N)
		    { "sHora"               , 4   ,   "s" }, --                           , -- Time HHMM (2N)
		    { "sValidade"           , 4   ,   "s" }, -- Data de expira��o do cart�o AAMM
		    { "sParcelas"           , 2   ,   "s" }, -- N�mero de parcelas
		    { "sValor"              , 12  ,   "s" }, -- Valor da transa��o
		    { "sDataPre"            , 6   ,   "s" }, -- Data da transacao Pre-datada DDMMYY
		    { "sNSU"                , 12  ,    "s" }, -- NSU
		    { "sNSUOrig"            , 12  ,    "s" }, -- NSU
		    { "sAutorizacao"        , 6   ,   "s" }, -- Autoriza��o
		    { "sResposta"           , 4   ,   "s" }, -- C�digo de resposta
		    { "sRRN"                , 12  ,   "s" }, -- RRN
		    { "iTipoEntrada"        , 1   ,   "i" }, -- Tipo de entrada 0=manual, 1=magnetico, 2=chip
		    --{ "sFinPlanoPagto"      , 4   ,   "s" }, -- Plano de pagamento p/ Financiamento
		    --{ "sFinCiclo"           , 4   ,   "s" }, -- Ciclo p/ Financiamento
		    { "sAppCryptogram"      , 8   ,   "s" }, -- TAG 9f26 -- Permite processamento de autentica��o do cart�o
		    { "sIssuerAppData"      , 32  ,   "s" }, -- TAG 9f10 -- Prov� elementos que o emissor definiu para a mensagem de autoriza��o.
														     -- Presente se provido pelo cart�o no comando de resposta do comando GENERATE AC
		    { "iIssAppDataLen"      , 2   ,   "i" },
		    { "sUnpredictable"      , 4   ,   "s" }, -- TAG 9f37 -- Prov� variabilidade e unicidade do criptograma, reduzindo risco de fraude. 
																   -- Presente se utilizado no c�lculo do criptograma
		    { "sATC"                , 2   ,   "s" }, -- TAG 9f36 -- Indica a ordem sequencial de transa��o que ser� executada pela aplica��o. Permite variabilidade e unicidade do criptograma, reduzindo o risco de fraude
		    { "sTVR"                , 5   ,   "s" }, -- TAG 95 -- Informa ao emissor os detalhes do processamento da transa��o
		    { "sAIP"                , 2   ,   "s" },
		    { "iPAN_SeqNb"          , 2   ,   "i" },
		    { "sCryptInfoData"      , 1   ,   "s" }, -- TAG 9f27 -- Indica o tipo de criptograma retornado pelo cart�o (nega/aprova offline ou solicita aprova��o online)
		    { "sAppLabel"           , 255 ,   "s" },
		    { "iEntries"            , 2   ,   "i" }, -- Qtde de dados coletados
		    { "sConfPositiva"       , 255 ,   "s" }, -- (id do prompt e tamanho) x 16
		    { "sConfPosCript"       , 255 ,   "s" }, -- Criptograma gerado para conf pos / refer
		    { "sValorParcela"       , 12  ,   "s" }, -- Valor da parcela da transacao
		    { "iIdxVencimento"      , 2   ,   "i" }, -- Vencimento 0x01=30 dias, 0x02=60 dias, 0x03=90 dias e 0x04=120 dias
		    { "iCodIdxVou"          , 2   ,   "i" }, -- 01-Abastecimento, 02-Troca de oleo, 03-Lavagem, 04-Pedagio, 05-Outros
		    --{ "iCodCombustivel"     , 2   ,   "i" }, -- Voucher frota
		    --{ "sCodVeiculo"         , 255 ,   "s" }, -- Voucher frota
		    --{ "sCodCondutor"        , 255 ,   "s" }, -- Voucher frota 
		    --{ "sLitragem"           , 255 ,   "s" }, -- Voucher frota 
		    --{ "sKM"                 , 255 ,   "s" }, -- Voucher frota
		  											   -- 123456789012345 / M�ximo de 15 caracteres
		    { "iPrnOffsetVou"       , 64  ,   "s" }, -- Posi��o do Bit47 (�ndice)
		    { "iPrnTamanhoVou"      , 32  ,   "s" }, -- Tamanho do Bit47 (somente a tag 0x01)
		    { "sCelCript"           , 255 ,   "h" }, -- Id do portador criptografado
		    { "iTamCelCript"        , 32  ,   "s" }, -- Tamanho do Id do portador criptografado  
		    { "sTermCapability"     , 8   ,   "s" }, -- Terminal Capability Tag 0x9F33
		    { "sAplVersionNb"       , 4   ,   "s" }, -- Application Version Number Tag 0x9F08
		    { "sCVMResult"          , 8   ,   "s" }, -- CVM Result Tag 0x9F34
   		    { "sAppRef"             , 1   ,   "s" }, -- Referencia � aplicacao de Servicos, caso a transacao seja integrada  
   		    { "sGenACIndex"         , 2   ,   "s" }, -- Indica ate qual Generate AC foi executado. 1st = 01 e 2nd = 02
   		    { "iTipoCaptura"        , 2   ,   "i" },
   		    { "sISR"                , 15  ,   "s" }, -- Issuer Script Result
   			{ "sCV"                 , 12  ,   "s" }, -- comprovante de venda
   			{ "sAID"                , 20  ,   "s" }, -- 
   			{ "sTC"                 , 16  ,   "s" }, -- Transaction certificate
   			{ "sARQC"               , 16  ,   "s" },  --
   			{ "sSaldoVoucher"       , 200 ,   "s" },  -- Saldo restante do Voucher
   			{ "fAprovada"     		, 1   ,   "f" },  -- Indica se a transa��o foi aprovada
   			{ "sMsgRetornoServidor"	, 280  ,  "s" },  -- bit 63 retornada pelo servidor
   			{ "sPinCriptografado"   , 16  ,   "s" },  -- Pin criptografado vem desmontar processo cart�o
   			{ "sAuto"				, 6    ,  "s" },  -- bit 38	
   			{ "sCNE"                , 60  ,   "s" }, -- 
   			{ "sEmissor"            , 20  ,   "s" }, -- 

			{ "iTransPrivateLabel"  , 1   ,   "i" }, -- guarda o tipo de transa��o Private Label para imprimir corretamente no comprovante (venda, consulta e estorno)

			{ "sDataPreAuto"  		, 6   ,   "s" }, -- Cont�m a data da transa��o de pr�-autoriza��o, cuja confirma��o/estorno pode ocorrer em dias subseq�entes
			{ "sCodigoPreAuto" 		, 6   ,   "s" }, -- Cont�m o C�digo de Autoriza��o da transa��o de pr�-autoriza��o bem sucedida sendo confirmada/estornada
   			{ "fServico"		  	, 1   ,   "f" }, -- flag indicando se a transa��o � de servi�o	
		}
	},

	GlobalConfig = { --atualiza
		arquivo = "../../cfg/globcfg.cfg",
		podeApagar = false,
		mapa = {
			{ "fAtualizaInicializacao"      , 1  ,   "f" },
			{ "fRetornoTelecarga"           , 1  ,   "f" }, -- Informa que o aplicativo est� sendo iniciado ap�s uma telecarga
			{ "fRetornoInicializacao"       , 1  ,   "f" },
			{ "fTerminalBloqueado"    		, 1  ,   "f" },	  --  Bloqueio/Desbloqueio do Terminal
			{ "sSenhaLoj"       			, 4  ,   "s" },
			{ "fAgendaLojista"     			, 1  ,   "f" }, --RF 03/03/15  Indica que o lojista deve fazer o agendamento da telecarga
			{ "fAgendadoPeloLojista"		, 1  ,   "f" }, --RF 04/03/15  Indica que o agendamento foi feito pelo lojista
			{ "fExecutaAtualizacao"			, 1  ,   "f" },	--RF 24/03/15  Lojista optou por fazer a finaliza��o/atualizacao
			{ "fExecutaBaixaTec"			, 1  ,   "f" },	--RF 24/03/15  Executa bx tec ap�s instala��o
			{ "fAutocarga"					, 1  ,   "f" },	--RF 31/03/15  Fluxo de autocarga
			{ "fAutoInicializacao"			, 1  ,   "f" },	--RF 31/03/15  Fluxo de auto inicializa��o
			{ "fMsgFimAutocargaPend"		, 1  ,   "f" },	--RF 31/03/15  Mensagem de fim de autocarga pendente
			{ "fMsgFimAutoInitPend"			, 1  ,   "f" },	--RF 31/03/15  Mensagem de fim de auto inicializacao pendente
			{ "fMsgBxTecPend"				, 1  ,   "f" },	--RF 01/04/15  Mensagem de baixa tecnica pendente ap�s a instala��o
			{ "fFunc92"						, 1  ,   "f" } -- RF 18-07-15 Telecarga via fun��o 92.
		}
	},

	Terminal = {
		arquivo = "term.tbl",
		podeApagar = true,
		mapa = {
			{ "sData"               		, 6  ,   "s" }, -- YYMMDD - terminal deve atualizar sua data com este par�metro
			{ "iHora"               		, 6  ,   "i" }, -- HHMMSS - terminal deve atualizar seu hor�rio com este par�metro.  
			{ "sSenhaLoj"           		, 4  ,   "s" }, -- Corresponde ao C�digo de Acesso do lojista, solicitado em algumas transa��es.   Default 0000
			{ "sSenhaTec"           		, 4  ,   "s" }, -- Solicitada em opera��es realiz�veis no terminal pelos t�cnicos.  
			{ "sSenhaInicia"        		, 4  ,   "s" }, -- Solicitada para realiza��o de nova inicializa��o  
			{ "sSimboloMoeda"       		, 1  ,   "s" }, -- Utilizado quando o s�mbolo da moeda em uso deve ser impresso ou exibido em display  
			{ "iPosicaoDec"         		, 1  ,   "i" }, -- Posi��o do ponto decimal, � partir da direita. EMV: transaction currency exponent - tag �5F36�  
			{ "iCodigoLocal"         		, 3  ,   "i" }, -- Currency code - (Brasil - moeda Real - 986)  Par�metro de configura��o EMV (tag �5F2A�).  
			{ "sTerminalCodPais"        	, 3  ,   "s" }, -- Utilizado no preenchimento do bit 61 (Posi��es 14 - 16: POS Country Code): Brasil: 076 Par�metro de configura��o EMV (tag �9F1A�). 
			{ "iIntervaloFechamento"    	, 2  ,   "i" }, -- Cont�m o n�mero de horas, a partir do qual o terminal deve processar automaticamente a Finaliza��o.
			{ "iHorarioFinalizacao"     	, 4  ,   "i" }, -- hhmm em que o terminal deve efetuar a Finaliza��o, de forma autom�tica e diariamente. Deve ser preenchido com �ZEROS� para os terminais que n�o se utilizam desta funcionalidade. 
			{ "iAdvicesCounter"         	, 2  ,   "i" }, -- N�mero m�ximo de mensagens de Advice na mem�ria. Se este limite for atingido, j� deve enviar. Valor m�ximo 99 transa��es. 
			{ "iTempoOcioso"         		, 2  ,   "i" }, -- Tempo (em minutos) em que o terminal em estado de repouso (Idle) e sem intera��o do operador, entra em estado ocioso. 
			{ "iTempoInatividade"       	, 4  ,   "i" }, -- Tempo de inatividade (sem tr�fego de transa��es) que o terminal aguardar� para enviar mensagem �TOAKI� (minutos) � 4 d�gitos (nota��o BCD). 
			{ "iTempoOciosoDesfazimento"	, 2  ,   "i" }, -- Tempo de ociosidade para envio do desfazimento (em minutos) pelo terminal
			{ "iNivelBaixoBateria"      	, 2  ,   "i" }, -- Indica o valor percentual m�nimo do n�vel de carga de bateria, em que o terminal exibe uma mensagem de notifica��o ao operador.
			{ "iNivelCriticoBateria"    	, 2  ,   "i" }, -- Indica o valor percentual cr�tico do n�vel de carga de bateria, em que o terminal exibe uma mensagem de notifica��o ao operador e inicia o desligamento.
			{ "iNivelMinimoBateria"     	, 2  ,   "i" }, -- Indica o valor percentual m�nimo do n�vel de carga de bateria para o terminal iniciar o processo de Telecarga.
			{ "iIntervaloEnvioTele"			, 2  ,   "i" }, -- Tempo (em minutos) de inatividade que, depois de atingido, o terminal deve enviar a mensagem de Telemetria. Se igual �00� o terminal N�O deve enviar a mensagem de Telemetria.
			----------------------------------------------------------------------------------------------------
			--Options 1 - MSB is Most Significant Bit
			{ "fHabEstatisticaWireless" 	,   1   , "f"},   --  7. (S/N)
			{ "fHabContingenciaGSM"     	,   1   , "f"},	  --  6. (S/N)
			{ "fTermApenasGSM"  			,   1   , "f"},	  --  5. (S/N) 
			{ "fHabEstatisticaServico"     	,   1   , "f"},	  --  4. Habilita estat�stica de servi�o.
			{ "fUsoFuturo"     				,   1   , "f"},	  --  3. Uso futuro
			{ "fUsoFuturo"     				,   1   , "f"},	  --  2. Uso futuro								
			{ "fUsoFuturo"     				,   1   , "f"},	  --  1. Uso futuro								
			{ "fUsoFuturo"     				,   1   , "f"},	  --  0. Uso futuro
			----------------------------------------------------------------------------------------------------
			--Options 2 - MSB is Most Significant Bit
			{ "fSemVia" 					,   1   , "f"},   --  7. n�o imprimir comprovante 
			{ "fEstabAuto"     				,   1   , "f"},	  --  6. Estabelecimento autom�tico 
			{ "fEstabPergunta"  			,   1   , "f"},	  --  5. Estabelecimento autom�tico 
			{ "fClienteAuto"     			,   1   , "f"},	  --  4. Cliente autom�tico 
			{ "fClientePergunta"     		,   1   , "f"},	  --  3. Cliente com pergunta
			{ "fAmbosPergunta"     			,   1   , "f"},	  --  2. Ambas com pergunta								
			{ "fImprimeLogo"     			,   1   , "f"},	  --  1.Imprime Logotipo nos comprovantes								
			{ "fImprimeMaiorCVM"     		,   1   , "f"},	  --  0. N�o imprime comprovante cliente se valor menor que CVM Limit 
			----------------------------------------------------------------------------------------------------
			--Options 3 - MSB is Most Significant Bit
			{ "fColetaTrilha1" 				,   1   , "f"},   --  7. UsoFuturo
			{ "fUsoFuturo"     				,   1   , "f"},	  --  6. UsoFuturo
			{ "fUsoFuturo"  				,   1   , "f"},	  --  5. UsoFuturo 
			{ "fUsoFuturo"     				,   1   , "f"},	  --  4. Uso futuro
			{ "fUsoFuturo"     				,   1   , "f"},	  --  3. Uso futuro
			{ "fUsoFuturo"     				,   1   , "f"},	  --  2. Uso futuro								
			{ "fUsoFuturo"     				,   1   , "f"},	  --  1. Uso futuro								
			{ "fDescartImpressao"     		,   1   , "f"},	  --  0. Prioriza descartar a impress�o do comprovante cliente?
			----------------------------------------------------------------------------------------------------
			--Options 4 - MSB is Most Significant Bit - Habilitacao Chip
			{ "fTerminal" 					,   1   , "f"},   --  7. Terminal
			{ "fPrivateLabel"     			,   1   , "f"},	  --  6. Private Label
			{ "fUsoFuturo" 					,   1   , "f"},	  --  5. UsoFuturo 
			{ "fUsoFuturo"     				,   1   , "f"},	  --  4. Uso futuro
			{ "fVouFrota"     				,   1   , "f"},	  --  3. Voucher Frota
			{ "fCredito"     				,   1   , "f"},	  --  2. Credito								
			{ "fDebito"     				,   1   , "f"},	  --  1. Debito							
			{ "fVoucher"    				,   1   , "f"},	  --  0. Voucher

			----------------------------------------------------------------------------------------------------
			{ "iVelocidadeMax"		        ,   2   ,   "i" },  -- Velocidade m�xima de comunica��o dial			
			
		}
	},

	Comunicacao = {
		arquivo = "comunica.tbl",
		podeApagar = true,
		mapa = {
			{"iId"     								, 4, "i"},   -- Id do Registro
			{"iNumTentativas"  		               	, 2, "i"},   -- N�mero de tentativas			
			{"iTempoEsperaConx"               		, 4, "i"},   -- Tempo de espera da conex�o			
			----------------------------------------------------------------------------------------------------
			--Options 1 - MSB is Most Significant Bit
			{"fTransFinanceira"               		, 1, "f"},
			{"fAdmin"              					, 1, "f"}, --Administrativa: Resumo de Vendas, Estorno, Teste de Comunica��o, Fim de Autocarga, Fim de Autoinicializa��o, Finaliza��o.
			{"fInicializacao"              			, 1, "f"},
			{"fTOAKI"              					, 1, "f"},
			{"fBaixaTecnica"               			, 1, "f"},
			{"fTelemetria"               			, 1, "f"},
			{"fServicos"              				, 1, "f"},
			{"fUsoFuturo"              				, 1, "f"},
			----------------------------------------------------------------------------------------------------
			{"iNII"			               			, 3, "i"},
			{"iTempoResposta"              			, 2, "i"},
			{"iIdDetalhes"               			, 4, "i"}
		}
	},

	ComunicDetalhe = {
		arquivo = "comdet.tbl",
		podeApagar = true,
		mapa = {
			{"iId"     								,  4, "i"},   -- Id do Registro
			{"iTipoConexao"  		               	,  2, "i"},   -- 00 � Dial 01- GSM 02-GPRS 03-Ethernet			
			{"sTelefone"			           		, 24, "s"},   
			{"iModemMode"		               		,  2, "i"},   
			{"sIP"		               				, 12, "s"},
			{"sPorta"		           				,  6, "s"},
			{"sAPN"		         	  				, 25, "s"},
			{"iOperadora"	       	  				,  2, "i"}    -- 01 � Claro 02 � Oi 03 � Tim 04 - Vivo
		}
	},

	KeyData = {
		arquivo = "kdata.tbl",
		podeApagar = true,
		mapa = {
			-- { "IDChipData"                          , 4  ,   "i" }, -- Id do Registro de Chipdata Correspondente.
			{ "Keynmbr"         				    , 4  ,   "i" }, -- Position in table Range: 01h-99h 
			{ "CapKidx"         				    , 2  ,   "s" }, -- CA Public Key Index  
			{ "CapkLen"         				    , 4  ,   "s" }, -- CA Public key length (Ex. Length 0x80 = 128 bytes = 1024 bits) 
			{ "Capk1"         				    	,496 ,   "s" }, -- CA Public Key Value   			
			{ "CapkRID"         				  	, 10 ,   "s" }, -- CA Public key associated RID (Ex. A0 00 00 00 04)  			
			{ "CapkExpLen"         				   	, 1  ,   "i" }, -- Tamanho CA Public Key Exponent
			{ "CapkExp"         				   	, 6  ,   "i" }, -- CA Public Key Exponent
			{ "CapkHash"         				   	, 40 ,   "s" }, -- CA Public Key Hash Value     
		}
	},	

	Operadoras = {
		arquivo = "operad.dbt",
		podeApagar = true,
		mapa = {
			{"sIIN"	    				, 2, "s"},
			{"sOperadora"				, 7, "s"},
			{"iOperadora"				, 2, "i"},
		}
	},

	Inicializacao = {
		arquivo = "inicia.dbt",
		podeApagar = true,
		mapa = {
			{"iOperadora"	, 2, "i"},
			{"sAPN"			,25, "s"}
		}
	},

	BinExcecao = {
		arquivo = "excecao.tbl",
		podeApagar = true,
		mapa = {
			{"sBin"           ,   19,    "s"}
		}
	},

	UltimaFinalizacao = {
		arquivo = "ultfinal.dbt",
		podeApagar = true,
		mapa = {
			{"sHora"          ,  4,       "s"},
			{"sData"          ,  8,       "s"}
		}
	},

	T_ULTIMA_FINALIZACAO_RELATORIO = {
		arquivo = "finalrel.dbt",
		podeApagar = true,
		mapa = {
			{"sTexto"   ,  42,       "s"},
			{"fFonteGrande"     ,   1,       "f"},
			{"iAlinhamento"     ,   1,       "i"},
		}
	},

	T_TELEMETRIA = {
		arquivo = "telemtr.dbt",
		podeApagar = true,
		mapa = {
			{"sHora"          ,  6,       "s"},
			{"sData"          ,  6,       "s"},
			{"sMensagem"	  ,128, 	  "s"},
			{"iUltimoEnvio"	  , 11,		  "i"}		-- momento (em segundos) em que a telemetria foi enviada
		}
	},

	T_ESTATISTICA = {
		arquivo = "estat.dbt",
		podeApagar = true,
		mapa = {
			{"fPendente"	  ,  1, 	  "f"},  	 -- se verdadeiro, indica que a estat�stica deve ser enviada assim que poss�vel
			{"iNMMIN"         ,  4,       "i"},		 -- Messages IN ( Message from line ) :  Total de frames recebidos pelo POS. EX: 0110, 0210, 0410, 0810, etc.
			{"iNMMOUT"        ,  4,       "i"},		 -- Message OUT: Total de frames formatados e transmitidos pelo POS, independentemente se foram recebidos pelo HOST
			{"iNMLU"		  ,  4,		  "i"},		 -- contabiliza as ocorr�ncias de tentativas de conex�es fracassadas devido a linha em uso ou linha sem tom de discagem.
			{"iNMREDIALS"     ,  4,       "i"},		 -- REDIAL COUNT ( Total de redials ) : Total de Redial Prim�rio + Redial Secund�rio (Para GPRS funciona como reconex�o).

			{"iNMCERR"        ,  4,       "i"},		 -- COMMS ERRORS : Total de transa��es n�o completadas devido a problemas no meio de comunica��o pelos motivos CE e LC.
			{"iNMTOUT"        ,  4,       "i"},		 -- TRANSACTION TIMEOUTS : Total de transa��es n�o completadas por esgotamento do tempo de resposta do POS. 
			{"iNMRTOUT"       ,  4,       "i"},		 -- TIMEOUTS DUE TO REVERSAL PENDING ( Reversal Timeouts ): Total de transa��es de desfazimento n�o completadas por esgotamento do tempo de resposta do POS. 
			{"iNMID"       	  ,  4,       "i"},		 -- Devem ser contabilizadas as mensagens inesperados ou inconsistentes (G4.3)
			{"fNMPINPAD"      ,  1,       "f"},		 -- PINPAD PERMISION: os valores 00 para PINPAD inativo e 01 para PINPAD ativo, indicando se o PINPAD est� operando corretamente (status ativo).
			{"iNMLINPRNT"  	  ,  6,       "i"},		 -- NUMBER OF LINE PRINTED-inclusive linhas em branco

			{"iNMPTRAN"    	  ,  4,       "i"},		 -- PRIMARY TRANSACTION COUNT: Total de transa��es (frames) completadas com sucesso, quando o POS est� utilizando o n�mero prim�rio do POS. Entende-se por �completado com sucesso� todo fluxo completo de mensagens
			{"iNMSTRAN"    	  ,  4,       "i"},		 -- SECONDARY TRANSACTION COUNT: Total de transa��es (frames) completadas com sucesso, quando o POS est� utilizando o n�mero secund�rio do POS. Entende-se por �completado com sucesso� todo fluxo completo de mensagens
			{"iNMPREDIAL"  	  ,  4,       "i"},		 -- PRIMARY REDIAL COUNT: Total de rediscagens do POS utilizando o n�mero prim�rio do POS. Para GPRS contabiliza o n�mero de reconex�o prim�ria.
			{"iNMSREDIAL"  	  ,  4,       "i"},		 -- SECONDARY REDIAL COUNT: Total de rediscagens do POS utilizando o n�mero secund�rio do POS. Para GPRS contabiliza o n�mero de reconex�o secund�ria.
			{"iNMCRDERR"	  ,  4,		  "i"},		 -- CARD READER ERRORS
			{"iNMCRDREADS"	  ,  4,		  "i"},		 -- TOTAL MSR READS (leituras de tarja)

			{"iNFALLBACK"	  ,  4,		  "i"},		 -- NUMBER OF FALLBACKS � PARA CAPTURA DE CHIP CARD
			{"sTPDISCAGEM"	  ,  1,		  "s"},		 -- P-PULSO (dec�dico); T-TOM (dtmf); espa�o�n�o se aplica
			{"sTPCONEXAO"	  ,  1,		  "s"},		 -- D-DIAL; L-LEASED; N-LAN/ETHERNET; C-CDPD; G-GPRS; S-GSM; X-CDMA-1X;  W-WNB
			{"sFONEINIC"	  , 24,		  "s"},		 -- TELEFONE de inicializa��o do terminal (preenchido com FF � direita)
			{"sFONEPRIM"	  , 24,		  "s"},		 -- TELEFONE PRIM�RIO-Transa��o (preenchido com FF � direita)
			{"sFONESECUND"	  , 24,		  "s"},		 -- TELEFONE SECUND�RIO-Transa��o (preenchido com FF � direita)
			{"sCODIGOPABX"	  ,  4,		  "s"},		 -- C�digo PABX

			{"iTMPCONEXAO"    ,  4,       "i"},		 -- Soma dos tempos de conex�o - tempo gasto entre o in�cio da discagem at� recebimento do sinal da portadora (envolve somente a tentativa bem sucedida de discagem)
			{"iTMPOPERADOR"   ,  4,       "i"},		 -- Tempo m�dio entre o recebimento da portadora e o envio da mensagem para a REDE
			{"iTMPTRANSACAO"  ,  4,       "i"},		 -- Tempo m�dio entre o envio da mensagem e o recebimento da resposta. Neste tempo s�o consideradas todas as mensagens trocadas na conex�o em tratamento.
			
			{"iNPINERRO"	  ,  4,		  "i"},		 -- Cart�o com chip - N�mero de entradas erradas do PIN
			{"iNTRNCANC"	  ,  4,		  "i"},		 -- Cart�o com chip - N�mero de Transa��es canceladas (quando pressionada a tecla <Cancela>) ap�s o momento da solicita��o da senha pelo terminal
			{"iNCRTBLOQ"	  ,  4,		  "i"},		 -- Cart�o com chip - N�mero de cart�es bloqueados devido � digita��o incorreta da senha
			{"iNOFFLINE"	  ,  4,		  "i"},		 -- Contabiliza as transa��es enviadas para a REDE em modo Offline, atrav�s das mensagens 0220/0230. Devem ser contabilizados somente os fluxos completos, isto �, aqueles que receberam a resposta 0230.

			{"iNMPTRANGPRS"	  ,  4,		  "i"},		 -- Quantidade de transa��es completadas com sucesso, utilizando o IP prim�rio do POS.
			{"iNMSTRANGPRS"	  ,  4,		  "i"},		 -- Quantidade de transa��es completadas com sucesso, utilizando o IP secund�rio do POS.

			{"iNMCVCliente"	  ,  6,		  "i"},		 -- Total impresso de CVs via cliente
			{"iNMCVEstab"	  ,  6,		  "i"},		 -- Total impresso de CVs via estabelecimento
			{"iNMCVAmbos"	  ,  6,		  "i"},		 -- Total impresso de CVs via estabelecimento e cliente
			{"iNMCVGeral"	  ,  6,		  "i"},		 -- Total de transa��es (chip e magn�tico) , exceto estorno, reimpress�o e transa��es que s� imprimem uma via de comprovante (Consulta Private Label, SERASA)

			{"sCONFFBACKGSM"  ,  1,		  "s"},		 -- 
			{"iNMSQMUP"  	  ,  4,		  "i"},	 	 --
			{"iNMSQMIN"  	  ,  4,		  "i"},		 --
			{"iNMSQMDOWN"  	  ,  4,		  "i"},		 --

			{"iNMNAS"	  	  ,  4,		  "i"},		 -- Contabiliza o n�mero de tentativas de discagem para o telefone secund�rio de transa��es que n�o foram atendidas.
			{"iNMNAP"	  	  ,  4,		  "i"},		 -- Contabiliza o n�mero de tentativas de discagem para o telefone prim�rio de transa��es que n�o foram atendidas.
			{"iNMLOP"	  	  ,  4,		  "i"},		 -- Contabiliza o n�mero de tentativas de discagem para o telefone prim�rio de transa��es que retornaram sinal de ocupado.
			{"iNMLOS"	  	  ,  4,		  "i"},		 -- Contabiliza o n�mero de tentativas de discagem para o telefone secund�rio de transa��es que retornaram sinal de ocupado
			{"iNMTelecarga"	  ,  4,		  "i"},		 -- Contabiliza a quantidade de processos de telecarga efetuados com sucesso pelo terminal.
			
			{"iQERRATTACH"	  ,  4,		  "i"},		 -- Quantidade de ERRO no Attachment da Rede computadas desde a �ltima finaliza��o
			{"iQERRMODEM"	  ,  4,		  "i"},		 -- Quantidade de erros internos do modem
			{"iQERRSIMCARD"	  ,  4,		  "i"},		 -- Quantidade de erros com o sim card
			{"iQERRTCP1"	  ,  4,		  "i"},		 -- Quantidade de falha TCP Prim�rio
			{"iQERRTCP2"	  ,  4,		  "i"},		 -- Quantidade de falha TCP Secund�rio
			{"iQERRPPP"	  	  ,  4,		  "i"},		 -- Quantidade de erro no procedimento de PPP
			{"iNMQATTACH"	  ,  4,		  "i"},		 -- Quantidade de conex�es PPP com a Rede
			{"iQFBACKGSM"	  ,  4,		  "i"},		 -- Quantidade de fallback GSM
			{"iNMPTRANGSM"	  ,  4,		  "i"},		 -- Quantidade de transa��es completadas com sucesso, utilizando o n�mero prim�rio do fallback GSM do POS
			{"iNMSTRANGSM"	  ,  4,		  "i"},		 -- Quantidade de transa��es completadas com sucesso, utilizando o n�mero secund�rio do fallback GSM do POS
			
			-- contadores auxiliares
			{"iSomaSinal"     ,  6,       "i"},		 -- Soma dos sinais das transa��es. Durante o envio da finaliza��o, ser� utilizado para calcular a m�dia dos sinais.
            {"iSomaConexao"   ,  7,       "i"},		 -- Soma dos tempos de conex�o das transa��es em milissegundos. Auxilia no c�lculo de iTMPCONEXAO
            {"iSomaOperador"  ,  7,       "i"},		 -- Soma dos tempos de operador das transa��es em milissegundos. Auxilia no c�lculo de iTMPOPERADOR
            {"iSomaTransacao" ,  7,       "i"},		 -- Soma dos tempos da transa��o em milissegundos. Auxilia no c�lculo de iTMPTRANSACAO
			{"iQntTransacoes" ,  4,       "i"},		 -- Quantidade de transa��es realizadas com sucesso. Auxilia no c�lculo de iTMPCONEXAO, iTMPOPERADOR e iTMPTRANSACAO
			{"fIP_P"  		  ,  1,       "f"}		 -- Auxiliar para detectar conex�es prim�rias ou secund�rias
		}
	},

	T_VERSAO_TABELAS = {
		arquivo = "../../cfg/verstab.cfg",
		mapa = {
			{"iTipoTabelas"	  ,  1, 	"i"},  	-- n�mero (1 caracter) usado para saber o tipo de tabelas. � preciso definir algo como: 0 = tabelas comuns, 1 = tabelas EMV, etc.
			{"iVersaoTabelas" ,  1,     "i"}	-- n�mero (1 caracter) usado para identificar a vers�o das tabelas dento de um dia. Por exemplo, se ser�o lan�adas novas tabelas hoje, esse campo come�ariam com 0. Se forem atualizadas as tabelas no mesmo dia, ele passa a ser 1.
		}
	},

	T_CFG_CONSTANTES_GLOBAIS = {
		arquivo = "../../cfg/cteglob.cfg",
		mapa = {
			{"iQtdTentativasSYN",  2,	"i"}    -- tcp_syn_retries: constante utilizada pela funcao POSIX "connect" que configura a qtd. de tentativas de retransmissao do pacote SYN (quantidade de tentativas de transmiss�o � transparente para o Lua)
		}
	},

	T_I_LOG_DEBUG = {
		arquivo = "../../cfg/logdebug.cfg",
		mapa = {
			{"fErroRN"	  		,  1, 	"f"}, -- Erros da RN
			{"fEstados"	  		,  1, 	"f"}, -- Estados visitados
			{"fConexaoGeral"	,  1, 	"f"}, -- Mensagens gerais de conex�o
			{"fConexaoDetalhes"	,  1, 	"f"}, -- Mensagens detalhadas de conex�o
			{"fUI"	  			,  1, 	"f"}, -- Interface com o usu�rio
			{"fBC"	  			,  1, 	"f"}, -- BC
			{"fMsgISO" 			,  1,   "f"}, -- Mensagem ISO
			{"fEstatistica" 	,  1,   "f"}  -- Dados de estat�stica
		}
	},
	
	-->> RF 19-02-15 Arquivo com as informa��es dos servi�os instalados no terminal
	T_SERVICO_CFG = {
		arquivo = "../../cfg/servico.cfg",
		mapa = {
			{ "appname"					,	20	, "s"},
			{ "appdirectory"			,	20	, "s"},
			{ "apptype"					,	10	, "s"},
			{ "appid"					,	2	, "i"},
			{ "idfuncao"				,	20	, "s"},--2
			{ "showonmenu"				,	1	, "f"},
			{ "menuname"				,	40	, "s"},
			{ "menuidposicao"			,	2	, "i"},
			{ "appisactive"				,	1	, "f"},
			{ "servicofinanceiro"		,	1	, "f"},
			{ "allowNFCtransactions"	,	1	, "f"},
			{ "allowinteraction"		,	1	, "f"},
			{ "allowmenurelatorio"		,	1	, "f"},
			{ "allowconfpositiva"		,	1	, "f"},
			{ "allowconfreferida"		,	1	, "f"},
			{ "allowfallbackdigitado"	,	1	, "f"},
			{ "allowdigitada"			,	1	, "f"},
			{ "allowoffline"			,	1	, "f"},
		}
	},
	
	Log = {
		arquivo = "../../log/lua.txt",
		mapa = {
			{"sErro"	  		,  1000, 	"s"}, -- Erros LUA
		}
	},
	
	T_HASH_CARGATABELAS = {
		arquivo = "hashbc.dbt",
		podeApagar = true,
		mapa = {
			{"hash"				  ,  40, 	"s"},  	-- hash do arquivo tbl
		}
	},
	
	--para apagar na funcao 93
	Desfazimento = {
		arquivo = "desfaz.dbt",
		podeApagar = true,
		mapa = {
			
		}
	},
	
	--para apagar na funcao 93
	Advice = {
		arquivo = "advice.dbt",
		podeApagar = true,
		mapa = {
			
		}
	},
	
	--para apagar na funcao 93
	SegGAC = {
		arquivo = "seggac.dbt",
		podeApagar = true,
		mapa = {
			
		}
	},
	
	--para apagar na funcao 93
	ISR = {
		arquivo = "isr.dbt",
		podeApagar = true,
		mapa = {
			
		}
	},
	
	--para apagar na funcao 93
	Telemetria = {
		arquivo = "telemtr.dbt",
		podeApagar = true,
		mapa = {
			
		}
	},
	
	

}