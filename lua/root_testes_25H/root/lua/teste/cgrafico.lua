HF_BUTTON_ACCEPT_BUTTON	= 100 -- Bot�o de Ok selecionado.
HF_BUTTON_REJECT_BUTTON	= 101 -- Bot�o de Cancelar ou Voltar selecionado.
HF_BUTTON_CLEAR_BUTTON = 203 -- Bot�o de Limpar selecionado.
HF_BUTTON_FIRST_BUTTON = 204 -- Primeiro bot�o selecionado (Bot�o da esquerda do footer).
HF_BUTTON_SECOND_BUTTON = 205 -- Segundo bot�o selecionado. 
HF_BUTTON_THIRD_BUTTON = 206 -- Terceiro bot�o selecionado.
HF_BUTTON_LAST_BUTTON = 207 -- �ltimo bot�o selecionado (Bot�o da direita do footer).
HF_BUTTON_SETTINGS_DOWN_BUTTON = 208 -- Retorno da �rea superior esquerda na tela de settings
HF_BUTTON_SETTINGS_UP_BUTTON = 209 -- Retorno da �rea superior direita na tela de settings
HF_BUTTON_SETTINGS_BUTTON = 210 -- Retorno do bot�o inferior exibido na tela de settings.
HF_CHIP_EVENT = 301 -- Evento de uso do cart�o por meio de CHIP detectado.
HF_MAGNETIC_EVENT = 302 -- Evento de uso do cart�o por meio de Fita Magn�tica detectado.
HF_TIMEOUT_EVENT = 303 -- Evento Timeout de passado como par�metro da fun��o detectado.
HF_TIMEOUT_MENU_EVENT = 304 -- Evento Timeout utilizado para Menu na aplica��o, ele n�o tem um equivalente na Lib.

HF_BUTTON_BACK_BUTTON = 524 -- Bot�o de voltar selecionado.

HF_BUTTON_KEYBOARD_ACCEPT_BUTTON = 1000 -- Retorno do bot�o �Ok� da tela de keyboard.
HF_BUTTON_KEYBOARD_REJECT_BUTTON = 1001 -- Retorno do bot�o �Cancelar� da tela de keyboard.
HF_BUTTON_KEYBOARD_PREV_BUTTON = 1002 -- Retorno do bot�o �Voltar� da tela de keyboard.
HF_BUTTON_KEYBOARD_ARROW_LEFT_BUTTON 	= 1014 -- Retorno do bot�o �Seta Esquerda� da tela de Keyboard.
HF_BUTTON_KEYBOARD_ARROW_RIGHT_BUTTON = 1015 -- Retorno do bot�o �Seta direita� da tela de Keyboard.

--Tipos de m�scaras de input
I_INPUT_TIPO_ALFANUMERICO = 1 --Input do tipo alfanumerico
I_INPUT_TIPO_NUMERICO = 2 -- Input do tipo num�rico
I_INPUT_TIPO_IP = 3 -- Input do tipo IP
I_INPUT_TIPO_SENHA = 4 -- Input do tipo senha

HF_BUTTON_KEYBOARD_REJECT_BUTTON_BUG = 63569
HF_BUTTON_KEYBOARD_ACCEPT_BUTTON_BUG = 63568
HF_BUTTON_KEYBOARD_F_BUTTON = 63516

HF_BUTTON_KEYBOARD_ENTER = 11
HF_BUTTON_KEYBOARD_CANCEL = 12