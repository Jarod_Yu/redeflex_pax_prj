-- Arquivo de constantes de textos

-- >> Op��es de alinhamento
T_ALINHAMENTO 		= {LEFT = 'left', CENTER = 'center', RIGHT = 'right'}
-- <<

-- >> Op��es de fontes
T_FONTES			= {F00 = 'arial18px', F01 = 'arial14px', F02 = 'arial16px', F03 = 'arialb16px', F04 = 'arial16px', F05 = 'arial23px', 
	F06 = 'primas12px', F07 = 'arial20px', F08 = 'arial25px', F09 = 'arialb25px', }
-- <<

-- >> Textos da aplica��o
T_TEXTO_HEADER_01	= {'header 320x240', 'header 240x320', 'header mono'}
T_TEXTO_HEADER_02 	= {'header 2 320x240', 'header 2 240x320', 'header 2 mono'}

T_TEXTO_BODY_01 	= {'body 320x240', 'body 240x320', 'body mono'}
T_TEXTO_BODY_02 	= {'body 2 320x240', 'body 2 240x320', 'body 2 mono'}
-- � possivel iniciar a frase em negrito e terminar sem negrito, por�m a fonte da palavra em negrito est� fixa.
T_TEXTO_BODY_03 	= {'<b>valor:</b>15454', '<b>valor:</b>15454', '<b>valor:</b>15454'}

T_TEXTO_RODAPE_01 	= {'rodape 320x240 \nteste', 'rodape 240x320 \nteste', 'rodape mono \nteste'}
-- <<

-- >> Pular linha
T_TEXTO_PULAR_LINHA_01	= {'', '', ''}
T_TEXTO_PULAR_LINHA_02 	= {'\n', '\n', '\n'}
-- <<