-- Arquivos de constantes
require("cestados")
require("cmodulos")
require("cacoes")
require("cgrafico")
require("cui")
require("cstrings")

tEstado = {
	modulo = I_MOD_INICIAL,
	passo = I_ESTADO_INICIAL
}

-- m�quina de estados
tME = {	
	fPassoAtual = function ( )
		return tEstado.passo
	end,

	fModuloAtual = function( )
		return tEstado.modulo
	end,
}

--Arquvios Controladores
require("iniciact")

tControladores = {
	[I_MOD_INICIAL				] = tIniciaCT
}

mainRN = {	
	main = function(tTela)
		tTela = tControladores[tME.fModuloAtual()].ControlarNavegacao(tTela)
		
		return tTela	
	end
}

function main (params, identity)
	-- Blindagem pra se caso chamar a app com valores nulos
	if(params.tTela == nil) then
		-- Estrutura para ser feita a passagem de par�metros entra a app e Controlador.
		local tTela = { telaID = "", params = {}, bateria = {}, estatistica = {}}
		
		return mainRN.main(tTela)	
	else
		return mainRN.main(params.tTela)
	end
end
