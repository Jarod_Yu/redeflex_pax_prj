
tIniciaCT = {	
	tPassos = {
		
		[I_ESTADO_INICIAL 		] = function(tTela) 
			tEstado.passo  = I_ESTADO_INICIAL_SPECIFIC
			tTela.telaID = I_TELA_ID_DIALOG_NEW
			tTela.params = {
				tHeader = {
					tLinha = {
						{tTexto 		= T_TEXTO_HEADER_01, 	sAlinhamento = T_ALINHAMENTO.CENTER, sFonte = T_FONTES.F00},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.CENTER, sFonte = T_FONTES.F00},
					}
				},		
				tBody = {
					tLinha = {
						{tTexto 		= T_TEXTO_BODY_01, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F00},				
						-- pulando 1 linha, pode ser passado como uma string vazia ou com '\n'
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F00},				
						{tTexto 		= T_TEXTO_BODY_01, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F00},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F00},						
					}
				},
				fLogo = true,
				--fAnimacao = true,
				iTempo = 10,
				--tBotoesRodape = {'cancela'}
			}			
						
			return tTela 
		end,
		
		[I_ESTADO_INICIAL_SPECIFIC 		] = function(tTela) 
			tEstado.passo  = I_ESTADO_INICIAL_FINALIZA_APP
			tTela.telaID = I_TELA_ID_SPECIFIC
			tTela.params = {fCorban = true, iTempo = 30}		
						
			return tTela 
		end,
		
		[I_ESTADO_INICIAL_FINALIZA_APP 		] = function(tTela) 
			printer.print('tTela[2]: ' .. tostring(tTela[2]))
			tEstado.passo  = I_ESTADO_INICIAL
			tTela.telaID = I_TELA_ID_RELATORIO
			tTela.params = {
				tHeader = {
					tLinha = {
						{tTexto 		= T_TEXTO_HEADER_02, 	sAlinhamento = T_ALINHAMENTO.CENTER, sFonte = T_FONTES.F03},
					}
				},		
				tBody = {
					tLinha = {
						{tTexto 		= T_TEXTO_BODY_01, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},							
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
						{tTexto 		= T_TEXTO_PULAR_LINHA_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},	
						{tTexto 		= T_TEXTO_BODY_02, 	sAlinhamento = T_ALINHAMENTO.LEFT,	sFonte = T_FONTES.F06},
					}
				},
				iTempo = 10,
			}
			
			-- Passo est� a��o para o controlador encerrar a APP
			tTela.acoes = {{acao = I_ACAO_EXECUTAR_FINANCEIRA}}
						
			return tTela 
		end,
			
	},

	---
	--Escolhe o passo do m�dulo atual de acordo com o index do passo na m�quina de estados
	ControlarNavegacao = function(tTela)
		return tIniciaCT.tPassos[tME.fPassoAtual()](tTela)
	end

}

-- se quiser atribuit ao tControladores logo no require
return tIniciaCT