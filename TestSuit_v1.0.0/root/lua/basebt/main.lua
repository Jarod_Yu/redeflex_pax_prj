require "lunit"

lunit.import "all"

tc1 = TestCase("basebt")

function tc1.test_associate()
    --[[
    resultAssociate = basebluetooth.associate()
    assert_not_nil(resultAssociate, "Não foi possível associar com a base.")
    if resultAssociate ~= nil then
        printer.print("basebt error idx: " .. BASEBT_ERROR_IDX);
        printer.print("basebt association idx: " .. BASEBT_ASSOCIATION_IDX);
        printer.print("error type: " .. resultAssociate[BASEBT_ERROR_IDX].errorType);
        printer.print("native error: " .. resultAssociate[BASEBT_ERROR_IDX].nativeError);
        printer.print("serial number: " .. resultAssociate[BASEBT_ASSOCIATION_IDX].serialNumber);
        printer.print("bluetooth address: " .. resultAssociate[BASEBT_ASSOCIATION_IDX].bluetoothAddress);
        printer.print("base firmware version: " .. resultAssociate[BASEBT_ASSOCIATION_IDX].baseFirmwareVersion);
        printer.print("device firmware version: " .. resultAssociate[BASEBT_ASSOCIATION_IDX].deviceFirmwareVersion);
    end
    ]]--
end

function tc1.test_disassociate()
    --[[
    resultAssociate = basebluetooth.associate()
    if(resultAssociate ~= nil) then
        assert_not_nil(basebluetooth.disassociate(resultAssociate[BASEBT_ASSOCIATION_IDX]),
            "Não foi possível desassociar com a base.")
    end
    ]]--

    assert_true(basebluetooth.disassociate({
        serialNumber="serialNumber",
        bluetoothAddress="bluetoothAddress",
        baseFirmwareVersion="baseFirmwareVersion",
        deviceFirmwareVersion="deviceFirmwareVersion"
    }))
end

function tc1.test_config()
    --[[
    resultAssociate = basebluetooth.associate()
    if(resultAssociate ~= nil) then
    ]]--

    assert_true(basebluetooth.config(10, 10000), "Não foi possível definir os parâmetros para conexão com a base bluetooth.")

    --end
end

function tc1.test_up()
    --[[
    resultAssociate = basebluetooth.associate()
    if(resultAssociate ~= nil) then
    ]]--

    resultConfig = basebluetooth.config(10, 10000)
    if(resultConfig == true) then
        assert_not_nil(basebluetooth.up(), "Não foi possível iniciar o processo de conexão com a base.")
    end

    --end
end

function tc1.test_down()
    --[[
    resultAssociate = basebluetooth.associate()
    if(resultAssociate ~= nil) then
    ]]--

    resultConfig = basebluetooth.config(10, 10000)
    if(resultConfig == true) then
        resultUp = basebluetooth.up()
        if(resultUp ~= nil) then
            assert_not_nil(basebluetooth.down(), "Não foi possível iniciar o processo de desativação da conexão com a base.")
        end
    end

    --end
end

function tc1.test_status()
    --[[
    resultAssociate = basebluetooth.associate()
    if(resultAssociate ~= nil) then
    ]]--

    resultStatus = basebluetooth.status()
    assert_not_nil(resultStatus, "Não foi possível obter o status da conexão com a base.")

    if(resultStatus ~= nil) then
        printer.print("\n--> Before config\n")
        print_status(resultStatus)
    end

    resultConfig = basebluetooth.config(10, 10000)
    if(resultConfig == true) then
        resultUp = basebluetooth.up()
        if(resultUp ~= nil) then
            resultStatus = basebluetooth.status()
            assert_not_nil(resultStatus, "Não foi possível obter o status da conexão com a base.")

            if(resultStatus ~= nil) then
                printer.print("\n--> Base up\n")
                print_status(resultStatus)
            end
        end
    end

    --end
end

function print_status(resultStatus)
    printer.print("basebt status idx: " .. BASEBT_STATUS_IDX);
    printer.print("basebt ip idx: " .. BASEBT_IP_IDX);
    printer.print("association status: " .. resultStatus[BASEBT_STATUS_IDX].associationStatus);
    printer.print("assoc native status: " .. resultStatus[BASEBT_STATUS_IDX].assocNativeStatus);
    printer.print("connection status: " .. resultStatus[BASEBT_STATUS_IDX].connectionStatus);
    printer.print("conn native status: " .. resultStatus[BASEBT_STATUS_IDX].connNativeStatus);
    printer.print("assoc error type: " .. resultStatus[BASEBT_STATUS_IDX].assocErrorType);
    printer.print("assoc native error: " .. resultStatus[BASEBT_STATUS_IDX].assocNativeError);
    printer.print("conn error type: " .. resultStatus[BASEBT_STATUS_IDX].connErrorType);
    printer.print("conn native error: " .. resultStatus[BASEBT_STATUS_IDX].connNativeError);
    printer.print("ip address: " .. resultStatus[BASEBT_IP_IDX].ipAddress);
    printer.print("mask: " .. resultStatus[BASEBT_IP_IDX].mask);
    printer.print("gateway: " .. resultStatus[BASEBT_IP_IDX].gateway);
    printer.print("dns 1: " .. resultStatus[BASEBT_IP_IDX].dns1);
    printer.print("dns 2: " .. resultStatus[BASEBT_IP_IDX].dns2);
    printer.print("is dhcp: ")
    printer.print(resultStatus[BASEBT_IP_IDX].isDhcp);
    printer.print("lease duration: " .. resultStatus[BASEBT_IP_IDX].leaseDuration);
    printer.print("dhcp server address: " .. resultStatus[BASEBT_IP_IDX].dhcpServerAddress);
end

lunit.run()