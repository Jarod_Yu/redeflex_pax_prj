-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              management/main.lua                                                 --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                    Testes de compression                                               --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl       9844      28/06/2006        Cria��o                              --
--  Jamerson Lima        jrfl       9844      11/05/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP0010) --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

-- ****************** TESTE DE SLEEP ****************** --

tc = TestCase("management.sleep")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sleep1                                                                           --
--  Descricao: Caso de sucesso. Chama sleep passando o valor 10000 (10 segs)                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sleep1()

    local time1 = os.time()
    management.sleep( 10000 )
    local time2 = os.time()

    assert_equal( time1 + 10, time2, string.format( "test_sleep1. Caso de sucesso, chama sleep por 10 segs. Tempo de sleep esperado: 10 segs, obtido: %d segs", ( time2 - time1 ) ) )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sleep2                                                                           --
--  Descricao: Passando tempo 0                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sleep2()

    local result = management.sleep( 0 )

    assert_true( result, "test_sleep2. Passando tempo 0. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sleep3                                                                           --
--  Descricao: Passando tempo negativo                                                               --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sleep3()

    local result, error_message, error_code = management.sleep( -1 )

    assert_nil( result, "test_sleep3. Passando tempo negativo. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDARG, error_code, "test_sleep3. Passando tempo negativo. Codigo de erro incorreto", true )
end

lunit.run()