-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 sha1/main.lua                                                    --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de sha1                                   --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      23/5/2006        Criacao                              --
--  Jamerson Lima        jrfl          9844    15/6/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP007)  --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

--nome dos arquivos que seram utilizados nos testes
indice_filename = "/indices.txt"
string_filename = "/string.txt"

--nome dos arquivos que contem o hash dos arquivos acima
indice_shafilename = "/indices.sha"
string_shafilename = "/string.sha"

-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   SHA1 TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "sha1" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_calculate1                                                                  --
--  Descricao: Caso de sucesso. Calcula Hash de uma string                                           --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sha1_calculate1()

    local message, error_message = LoadFileInBuffer( string_filename )

    assert_not_nil( message, error_message, true )

    --abre e le o conteudo do arquivo com hash
    local expected_sha_hash = LoadFileInBuffer( string_shafilename )

    assert_not_nil( expected_sha_hash, error_message, true )

    --chama a funcao testada
    local result = sha1.calculate( message )

    assert_equal( expected_sha_hash, result, "test_sha1_calculate1. Calcula hash de uma string. Retorno incorreto" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_calculate2                                                                  --
--  Descricao: Caso de sucesso. Calcula Hash de um arquivo de indice                                 --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sha1_calculate2()

    local message, error_message = LoadFileInBuffer( string_filename )

    assert_not_nil( message, error_message )

    --abre e le o conteudo do arquivo com hash
    local expected_sha_hash = LoadFileInBuffer( string_shafilename )

    assert_not_nil( expected_sha_hash, error_message )

    --chama a funcao testada
    local result = sha1.calculate( message )

    assert_equal( expected_sha_hash, result, "test_sha1_calculate2. Calcula hash de um arquivo de indice. Retorno incorreto" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_calculate3                                                                  --
--  Descricao: Passando string vazia                                                                 --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_sha1_calculate3()

    local error_code = 0
    local error_message = ""

    --chama a funcao testada
    local result, error_message, error_code = sha1.calculate( "" )

    assert_nil( result, "test_sha1_calculate3. Passando string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_sha1_calculate3. Passando string vazia. Retorno incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_sha1_calculate3. Passando string vazia. Retorno incorreto" )

end

tc_incremental = TestCase( "sha1.incremental" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_inc_calculate1                                                              --
--  Descricao: Caso de sucesso. Calcula Hash de um arquivo                                           --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc_incremental.test_sha1_inc_calculate1()

    --abre e le o conteudo do arquivo com hash
    local expected_sha_hash = LoadFileInBuffer( string_shafilename )
    assert_not_nil( expected_sha_hash, error_message, true )

    local fis, error_message = streams.openfileis( string_filename )
    assert_not_nil( fis, error_message, true )

    local result, error_message = sha1.chaininit()
    assert_true( result, "test_sha1_inc_calculate1. Caso de sucesso. Calcula Hash de um arquivo. Erro em sha1.chaininit." )

    local bytes = fis:read(64, TIMER_INFINITE)
    while (bytes ~= nil) do
          result, error_message = sha1.chainupdate(bytes)
          assert_true( result, "test_sha1_inc_calculate1. Caso de sucesso. Calcula Hash de um arquivo. Erro em sha1.chainupdate." )
          bytes = fis:read(64, TIMER_INFINITE)
    end

    result, error_message = sha1.chainresult()
    assert_not_nil( result, "test_sha1_inc_calculate1. Caso de sucesso. Calcula Hash de um arquivo. Erro em sha1.chainresult." )

    assert_equal( expected_sha_hash, result, "test_sha1_inc_calculate1. Calcula Hash de um arquivo. Retorno incorreto" )

    fis:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_inc_calculate2                                                              --
--  Descricao: Caso de insucesso. Chama sha1.chaininit mais de uma vez                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc_incremental.test_sha1_inc_calculate2()

    local result, error_message = sha1.chaininit()

    assert_true( result, "test_sha1_inc_calculate2. Caso de insucesso. Chama sha1.chaininit mais de uma vez. Erro em sha1.chaininit." )

    result, error_message, error_code = sha1.chaininit()

    assert_nil( result, "test_sha1_inc_calculate2. Caso de insucesso. Chama sha1.chaininit mais de uma vez. Erro em sha1.chaininit." )

    assert_equal( TST_ERR_RESOURCEALLOC, error_code, "test_sha1_inc_calculate2. Chama sha1.chaininit mais de uma vez. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_RESOURCEALLOC ], error_message, "test_sha1_inc_calculate2. Chama sha1.chaininit mais de uma vez. Mensegam de erro incorreta" )

    sha1.chainresult()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_inc_calculate3                                                              --
--  Descricao: Caso de insucesso. Chama sha1.chainupdate sem chamar sha1.chaininit                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc_incremental.test_sha1_inc_calculate3()

    local result, error_message, error_code = sha1.chainupdate("dados")

    assert_nil( result, "test_sha1_inc_calculate3. Caso de insucesso. Chama sha1.chainupdate sem chamar sha1.chaininit. Erro em sha1.chainupdate." )

    assert_equal( TST_ERR_INVALIDSTATE, error_code, "test_sha1_inc_calculate3. Caso de insucesso. Chama sha1.chainupdate sem chamar sha1.chaininit. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDSTATE ], error_message, "test_sha1_inc_calculate3. Caso de insucesso. Chama sha1.chainupdate sem chamar sha1.chaininit. Mensegam de erro incorreta" )

    sha1.chainresult()
end


-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_inc_calculate4                                                              --
--  Descricao: Caso de insucesso. Chama sha1.chainresult sem chamar sha1.chaininit                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc_incremental.test_sha1_inc_calculate4()

    local result, error_message, error_code = sha1.chainresult()

    assert_nil( result, "test_sha1_inc_calculate4. Caso de insucesso. Chama sha1.chainresult sem chamar sha1.chaininit. Erro em sha1.chainresult." )

    assert_equal( TST_ERR_INVALIDSTATE, error_code, "test_sha1_inc_calculate4. Caso de insucesso. Chama sha1.chainresult sem chamar sha1.chaininit. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDSTATE ], error_message, "test_sha1_inc_calculate4. Caso de insucesso. Chama sha1.chainresult sem chamar sha1.chaininit. Mensegam de erro incorreta" )

    sha1.chainresult()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_sha1_inc_calculate5                                                              --
--  Descricao: Caso de insucesso. Chama sha1.chainupdate passando string vazia.                      --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc_incremental.test_sha1_inc_calculate5()

    local result, error_message = sha1.chaininit()
    assert_true( result, "test_sha1_inc_calculate5. Caso de insucesso. Chama sha1.chainupdate passando string vazia.. Erro em sha1.chaininit." )

    result, error_message, error_code = sha1.chainupdate("")

    assert_nil( result, "test_sha1_inc_calculate5. Caso de insucesso. Chama sha1.chainupdate passando string vazia.. Erro em sha1.chainupdate." )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_sha1_inc_calculate5. Caso de insucesso. Chama sha1.chainupdate passando string vazia.. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_sha1_inc_calculate5. Chama sha1.chainupdate passando string vazia.. Mensegam de erro incorreta" )

    sha1.chainresult()

end

lunit.run()