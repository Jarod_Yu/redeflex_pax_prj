---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios de ui                                       --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         16/02/2006  Cria��o                                          --
-- Jamerson Lima        jrfl        10/04/2006  Implementacao de novos testes                    --
-- Jamerson Lima        jrfl        14/06/2006  Rework(HFLEX-LUA-TSTR-CODE-INSP006)              --
-- Leandro              lmf         08/01/2007  Inclusao de novos testes (19096)                 --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------


-- ****************** TESTE DE TEXTFIELD (Autom�ticos) ****************** --

-- Testes para o textfield (14 teste)--
tc = TestCase("ui.textfield")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Teste com valor negativo em  --
--            'maxlenght'                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (2/15)--
function tc.test_textfield2()
    local ret, error_message, error_code = ui.textfield("Title",  "Teste", -1 )

    assert_nil( ret, "test_textfield2. Maxlenght negativo. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_textfield2. Maxlenght negativo. Mensagem de erro incorreta", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_textfield2. Maxlenght negativo. Codigo de erro incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Teste com valor negativo em  --
--            'minlenght'                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (3/15)--
function tc.test_textfield3()
    local ret, error_message, error_code = ui.textfield("Title", "Teste", 3, -1)

    assert_nil(ret, "test_textfield3. Minlenght negativo " ,true)

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_textfield3. Minlenght negativo. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_textfield3. Minlenght negativo. Mensagem de erro incorreta", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield4                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Teste com 'maxlenght' menor  --
--            que 'minlenght'                                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (4/15)--
function tc.test_textfield4()
    local ret, error_message, error_code = ui.textfield("Title", "Teste", 3, 7)

    assert_nil(ret, "test_textfield4. Maxlenght menor que minlenght. Retorno incorreto", true)

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_textfield4. Maxlenght menor que minlenght. Mensagem de erro incorreta", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_textfield4. Maxlenght menor que minlenght. Codigo de erro incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield5                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Teste com 'maxlenght' igual  --
--            a '0'                                                                              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (5/15)--
function tc.test_textfield5()
    local ret, error_message, error_code = ui.textfield("Title", "Teste", 0)

    assert_nil(ret, "test_textfield5. Maxlenght igual a 0. Retorno incorreto", true)

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_textfield5. Maxlenght igual a 0. Mensagem de erro incorreta", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_textfield5. Maxlenght igual a 0. Codigo de erro incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield6                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Teste com 'minlenght' igual  --
--            a '0'                                                                              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (6/15)--
function tc.test_textfield6()
    local ret = ui.textfield("Title", "Teste", 3, 0)

    assert_not_nil( string.find( tostring( ret ), "TextField" ), "test_textfield6. Minlength igual a 0. Retorno incorreto", true)

    ui.destroy(ret)
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield7                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield somente --
--            com 'label' e verificando o retorno                                                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (7/15)--
function tc.test_textfield7()
    local ret = ui.textfield("Title", "Teste")

    assert_not_nil( string.find( tostring( ret ), "TextField" ), "test_textfield7. Caso de sucesso passando 'label' . Retorno incorreto", true )

    ui.destroy(ret)
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield8                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield somente --
--            com 'label' e 'maxlenght' e verificando o retorno                                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (8/15)--
function tc.test_textfield8()
    local ret = ui.textfield("Title", "Teste", 8)

    assert_not_nil( string.find( tostring( ret ), "TextField" ), "test_textfield8. Caso de sucesso passando 'label' e 'maxlength' . Retorno incorreto", true )

    ui.destroy(ret)
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield9                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield somente --
--            com 'label', 'maxlenght' e 'minlenght' e verificando o retorno                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (9/15)--
function tc.test_textfield9()
    local ret = ui.textfield("Title", "Teste", 8, 2)

    assert_not_nil( string.find( tostring( ret ), "TextField" ), "test_textfield9. Caso de sucesso passando 'label', 'maxlength' e 'minlength' . Retorno incorreto", true )

    ui.destroy(ret)
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield10                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            todos os par�metros v�lidos                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (10/15)--
function tc.test_textfield10()
    local ret = ui.textfield("Title", "Teste", 8, 2, false)

    assert_not_nil( string.find( tostring( ret ), "TextField" ), "test_textfield10. Caso de sucesso com todos os parametros . Retorno incorreto", true )

    ui.destroy(ret)
end

-- ****************** TESTE DE TEXTFIELD (Interativos) ****************** --

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield11                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            label 'Teste'                                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (11/15)--
function tc.test_textfield11()
    display.clear()
    display.print("Criando Textfield com label 'Teste'",0,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.textfield("Title", "Teste")

    if (ret ~= nil) then
        display.clear()

        ret:show()
        ui.destroy(ret)

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro criando textfield com label 'Teste'")
    else
        assert_fail("Erro criando textfield com label 'Teste'")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield12                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            label 'Teste' e 'maxlenght' igual a '5'                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (12/15)--
function tc.test_textfield12()
    display.clear()
    display.print("Criando Textfield com maxlenght '5'",0,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.textfield("Title", "Teste", 5)

    if (ret ~= nil) then
        display.clear()

        ret:show()
        ui.destroy(ret)

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro criando textfield com maxlenght '5'")
    else
        assert_fail("Erro criando textfield com maxlenght '5'")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield13                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            label 'Teste', maxlenght igual a '5' e minlenght igual a '2'                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (13/15)--
function tc.test_textfield13()
    display.clear()
    display.print("Criando Textfield com minlenght '2'",0,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.textfield("Title", "Teste", 5, 2)

    if (ret ~= nil) then
        display.clear()

        ret:show()
        ui.destroy(ret)

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro criando textfield com minlenght '2'")
    else
        assert_fail("Erro criando textfield com minlenght '2'")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield14                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            label 'Teste', maxlenght igual a '5', minlenght igual a '2' e ispassword igual a   --
--            true                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (14/15)--
function tc.test_textfield14()
    display.clear()
    display.print("Criando Textfield com ispassword 'true'",0,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.textfield("Title", "Teste", 5, 2, true)

    if (ret ~= nil) then
        display.clear()

        ret:show()
        ui.destroy(ret)

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro criando textfield com ispassword 'true'")
    else
        assert_fail("Erro criando textfield com ispassword 'true'")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield15                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.textfield. Criando um textfield com     --
--            label 'Teste', maxlenght igual a '5', minlenght igual a '3' e verificando o que    --
--            foi digitado                                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do textfield (15/15)--
function tc.test_textfield15()
    display.clear()
    display.print("Criando Textfield e verificando o q foi digitado",0,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    display.clear()
    display.print("label: 'Teste'",0,0)
    display.print("min e max: 3 e 5",1,0)
    display.print("<'123'> p/Sair",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    repeat
        local ret = ui.textfield("Title", "Teste", 5, 3)

        if (ret ~= nil) then

            ret:show()

            texto = ret:text()

            ui.destroy(ret)

            display.print(texto, 3, 0)
        else
            assert_fail("Erro criando textfield" , true)
            texto = "123"
        end
        display.clear()
        display.print("Voce digitou",0,0)
        display.print(texto,1,0)
        display.print("Press any key...", 3, 0)
        keyboard.getkeystroke(-1)
    until texto == "123"

    display.clear()
    display.print("Teste OK?", 0, 0)
    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro criando textfield e verificando texto digitado")
end


-- ****************** TESTE DE TRANSIENT (Autom�ticos) ****************** --

-- Testes para o transient (10 teste)--
tc2 = TestCase("ui.transient")

---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient2                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 0 secs    ------
----            title = 'Teste', message = 'Msg Teste' e nextscreen um texfield com label igual a  ------
----            'Teste OK?'                                                                        ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (2/7)------
function tc2.test_transient2()

        local ret, error_message, error_code = ui.transient("Teste", "Msg Teste", 0)

        assert_nil( ret, "test_transient2. duration igual a 0. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code,"test_transient2. duration igual a 0. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message,"test_transient2. duration igual a 0. Mensagem de erro incorreta" )
end

-- ****************** TESTE DE TRANSIENT (Interativos) ****************** --

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_transient3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    --
--            title = 'Teste', message = 'Msg Teste'                                             --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

---- Teste do transient (3/7)------
function tc2.test_transient3()

       display.clear()
       display.print("Transient",0,0)
       display.print("Duration: 5 secs",1,0)
       display.print("Caso de sucesso",2,0)
       display.print("Press any key...", 3, 0)

       keyboard.getkeystroke(-1)

       display.clear()
       local ret = ui.transient("Teste", "Msg Teste", 5000)

       assert_true( ret, "Erro transient com duration 5 secs e title=Teste e texto=msg teste 'Teste OK?'", true )

       key = lerTecla()
       assert_equal(KEY_ONE, key, "Erro transient com duration 5 secs e title=Teste e texto=msg teste 'Teste OK?'")

end----

---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient4                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            title = 'Teste Com String Extremamente Grande Em Relacao ao Display do POS',       ------
----            message = 'Msg Teste' e nextscreen um texfield com label igual a 'Teste OK?'       ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (4/7)------
function tc2.test_transient4()

        display.clear()
        display.print("Title Grande",0,0)
        display.print("Duration: 2 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)
        
        local cols, rows = display.geometry()
        local text = string.rep( "Title Grande", math.ceil( ( cols * rows ) / 12 ) )

        display.clear()

        local result = ui.transient( text, "Msg Teste", 2000)

        assert_true( result, "Erro no teste do transient com title grande", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com title grande")

end

---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient5                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            title = tring vazia (""), message = 'Msg Teste' e nextscreen um texfield com label ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (5/7)------
function tc2.test_transient5()

        display.clear()
        display.print("Title Vazio",0,0)
        display.print("Duration: 2 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("", "Msg Teste", 2000)

        assert_true( ret, "Erro no teste do transient com title vazio", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com title vazio")

end

---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient6                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = 'Teste Com String Extremamente Grande Em Relacao ao Display do POS',     ------
----            title = 'Teste' e nextscreen um texfield com label igual a 'Teste OK?'             ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (6/7)------
function tc2.test_transient6()
    display.clear()----
    display.print("Message Grande",0,0)
    display.print("Duration: 2 secs",1,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local cols, rows = display.geometry()
    local text = string.rep( "Message Grande", math.ceil( ( cols * rows ) / 14 ) )

    display.clear()
    local ret = ui.transient("Teste", text, 2000)

    assert_true( ret, "Erro no teste do transient com message grande", true )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste do transient com message grande")
end

--------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient7                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = tring vazia (""), title = 'Teste' e nextscreen um texfield com label     ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (7/7)------
function tc2.test_transient7()
        display.clear()
        display.print("Message Vazia",0,0)
        display.print("Duration: 2 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("Teste", "", 2000)

        assert_true( ret, "Erro no teste do transient com message vazia", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com message vazia")

end

---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient8                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = 'Texto', title = 'Teste' e nextscreen um texfield com label     ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (8/7)------
function tc2.test_transient8()
        display.clear()
        display.print("Transient teste",0,0)
        display.print("Duration: -1 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("Teste", "Texto", -1000, 81, "left", "left", "bottom")

        assert_true( ret, "Erro no teste do transient com message vazia", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com message vazia")

end


--------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient8                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = 'Texto', title = 'Teste' e nextscreen um texfield com label     ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (9/7)------
function tc2.test_transient9()
        display.clear()
        display.print("Transient teste",0,0)
        display.print("Duration: 2 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("Teste", "Texto", 2000, 3, "center", "right")

        assert_true( ret, "Erro no teste do transient com message vazia", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com message vazia")

end


---------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient8                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = 'Texto', title = 'Teste' e nextscreen um texfield com label     ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (8/7)------
function tc2.test_transient8()
        display.clear()
        display.print("Transient teste",0,0)
        display.print("Duration: -1 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("Teste", "Texto", -1000, 81, "left", "left", "bottom")

        assert_true( ret, "Erro no teste do transient com message vazia", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com message vazia")

end

--------------------------------------------------------------------------------------------------------
----                                                                                               ------
----  Nome:     test_transient9                                                                    ------
----                                                                                               ------
----  Descricao:                                                                                   ------
----            Funcao respons�vel por validar a fun��o ui.transient. Teste com duration 2 secs    ------
----            message = 'Texto', title = 'Teste' e nextscreen um texfield com label     ------
----            igual a 'Teste OK?'                                                                ------
----                                                                                               ------
----  Argumentos:                                                                                  ------
----                                                                                               ------
----                                                                                               ------
----  Dependencias:                                                                                ------
----                                                                                               ------
----                                                                                               ------
----  Retornos:                                                                                    ------
----    Casos de erro:                                                                             ------
----        Nenhum                                                                                 ------
----    Casos de sucesso:                                                                          ------
----        Nenhum                                                                                 ------
---------------------------------------------------------------------------------------------------------
--
---- Teste do transient (9/7)------
function tc2.test_transient9()
        display.clear()
        display.print("Transient teste",0,0)
        display.print("Duration: 2 secs",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        display.clear()

        local ret = ui.transient("Teste", "Texto", 2000, 3, "center", "right")

        assert_true( ret, "Erro no teste do transient com message vazia", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste do transient com message vazia")

end

-- ****************** TESTE DE MESSAGE (Autom�ticos) ****************** --

-- Testes para o message (6 teste)--
tc3 = TestCase("ui.message")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_message2                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.message. Teste com par�metros v�lidos   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do message (2/6)--
function tc3.test_message2()
    display.clear()
    display.print("Message",0,0)
    display.print("Title: 'Teste'",1,0)
    display.print("Msg: 'Msg Teste'",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.message("Teste", "Msg Teste")

    assert_true( ret, "Erro no teste de message com parametros validos", true )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste de message com parametros validos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_message3                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.message. Teste com title vazio e        --
--            message 'Msg Teste'                                                                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do message (3/6)--
function tc3.test_message3()
    display.clear()
    display.print("Message",0,0)
    display.print("Title: Vazio",1,0)
    display.print("Msg: 'Msg Teste'",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.message("", "Msg Teste")
    
    assert_true( ret, "Erro no teste de message com parametros validos", true )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste de message com title vazio")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_message4                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.message. Teste com message vazia e      --
--            title 'Teste'                                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do message (4/6)--
function tc3.test_message4()
    display.clear()
    display.print("Message",0,0)
    display.print("Title: 'Teste'",1,0)
    display.print("Msg: Vazia",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.message("Teste", "")
    
    assert_true( ret, "Erro no teste de message com message vazia", true )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste de message com message vazia")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_message5                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.message. Teste com title igual a        --
--            'Teste Com String Extremamente Grande Em Relacao ao Display do POS' e message      --
--            'Msg Teste'                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do message (5/6)--
function tc3.test_message5()
    display.clear()
    display.print("Message",0,0)
    display.print("Title: Grande",1,0)
    display.print("Msg: 'Msg Teste'",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)
    local cols, rows = display.geometry()
    local text = string.rep( "Title: Grande", math.ceil( ( cols * rows ) / 13 ) )

    display.clear()

    local ret = ui.message( text, "Msg Teste" )
    
    assert_true( ret, "Erro no teste de message com title muito grande", true )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste de message com title muito grande")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_message6                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.message. Teste com message igual a      --
--            'Teste Com String Extremamente Grande Em Relacao ao Display do POS' e title        --
--            'Teste'                                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do message (6/6)--
function tc3.test_message6()
    display.clear()
    display.print("Message",0,0)
    display.print("Title: 'Teste'",1,0)
    display.print("Msg: Grande",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)
    
    local cols, rows = display.geometry()
    local text = string.rep( "Msg: Grande", math.ceil( ( cols * rows ) / 11 ) )

    display.clear()

    local ret = ui.message( "Teste", text )

    assert_true( ret, "Erro no teste de message com message muito grande", true )

    management.sleep( 100 )

    key = lerTecla()
    assert_equal(KEY_ONE, key, "Erro no teste de message com message muito grande")
end

-- ****************** TESTE DE MENU (Interativos) ****************** --

tc4 = TestCase("ui.menu")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com title = 'Menu Teste' e  --
--            5 op��es = {"op 1", "op 2", ...}                                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (2/7)--
function tc4.test_menu2()
    display.clear()
    display.print("Menu",0,0)
    display.print("Title: 'Menu Teste'",1,0)
    display.print("#Op: 5",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.menu("Menu Teste", {"op 1", "op 2", "op 3", "op 4", "op 5"})

    assert_not_nil( ret, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com title vazio e           --
--            5 op��es = {"op 1", "op 2", ...}                                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (3/7)--
function tc4.test_menu3()
    display.clear()
    display.print("Menu",0,0)
    display.print("Title: Vazio",1,0)
    display.print("#Op: 5",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.menu("", {"op 1", "op 2", "op 3", "op 4", "op 5"})

    assert_not_nil( ret, "Erro no teste de menu com title vazio e 5 opcoes", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com title vazio e 5 opcoes")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com title = 'Menu teste' e  --
--            5 op��es vazias                                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (4/7)--
function tc4.test_menu4()
    display.clear()
    display.print("Menu",0,0)
    display.print("Title: 'Menu Teste'",1,0)
    display.print("#Op: 5 vazias",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.menu("Menu Teste", {"", "", "", "", ""})

    assert_not_nil( ret, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes vazias", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes vazias")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu5                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com title igual a           --
--            'Teste Com String Extremamente Grande Em Relacao ao Display do POS' e 5 op��es     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (5/7)--
function tc4.test_menu5()
    display.clear()
    display.print("Menu",0,0)
    display.print("Title: Grande",1,0)
    display.print("#Op: 5",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)
    
    local cols, rows = display.geometry()
    local text = string.rep( "Title: Grande", math.ceil( cols / 13 ) )

    display.clear()

    local ret = ui.menu( text, {"op 1", "op 2", "op 3", "op 4", "op 5"})

    assert_not_nil( ret, "Erro no teste de menu com title grande e 5 opcoes", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com title grande e 5 opcoes")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu6                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com title 'Menu Teste' e    --
--            5 op��es com 'Teste Com String Extremamente Grande Em Relacao ao Display do POS'   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (6/7)--
function tc4.test_menu6()
    display.clear()
    display.print("Menu",0,0)
    display.print("Title: 'Menu Teste'",1,0)
    display.print("#Op: 5 grandes",2,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local cols, rows = display.geometry()
    local opGrande = string.rep( "Opcao grande", math.ceil( cols / 12 ) )

    display.clear()

    local ret = ui.menu("Menu Teste", {opGrande ,opGrande , opGrande , opGrande , opGrande })

    assert_not_nil( ret, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes com string's grandes", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com title 'Menu Teste' e 5 opcoes com string's grandes")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu7                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.menu. Teste com shownumbers = false     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do menu (7/7)--
function tc4.test_menu7()
    display.clear()
    display.print("Menu",0,0)
    display.print("shownumbers = false",1,0)
    display.print("Press any key...", 3, 0)

    keyboard.getkeystroke(-1)

    local ret = ui.menu("Menu Teste", {"op 1", "op 2", "op 3", "op 4", "op 5"}, false)

    assert_not_nil( ret, "Erro no teste de menu com shownumbers = false", true )

    ret:show()

    key = lerTecla()

    ui.destroy( ret )

    assert_equal(KEY_ONE, key, "Erro no teste de menu com shownumbers = false")
end


-- ****************** TESTE DE DESTROY (Autom�ticos) ****************** --

-- Testes para o destroy (4 teste)--
tc5 = TestCase("ui.destroy")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_destroy3                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.destroy. Teste com handle para          --
--            textfield                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do destroy (3/4)--
function tc5.test_destroy3()

    local ret, error_message, error_code = ui.textfield("Title", "lalala")

    assert_not_nil( ret, string.format( "Dependencia: textfield nao pode ser criado[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    ui.destroy(ret)

    ret = ui.textfield("Title", "lalala")

    assert_not_nil( ret, "test_destroy3. destruir textfield. Handle nao foi liberado" )

    ui.destroy(ret)

    --trata com pcall para evitar que o lua gere um erro e pare a execucao do teste
    local result = pcall(function() local text = ret:text() end )

    assert_false( result, "test_destroy3. destruir textfield. Handle nao foi liberado" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_destroy4                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.destroy. Teste com handle para          --
--            menu                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do destroy (4/4)--
function tc5.test_destroy4()
    local ret, error_message, error_code = ui.menu("lalala", {"lelele", "lilili"})

    assert_not_nil( ret, string.format( "Dependencia: menu nao pode ser criado[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    ui.destroy(ret)

    ret = ui.menu("lalala", {"lelele", "lilili"})

    assert_not_nil( ret, "test_destroy4. destruir menu. Handle nao foi liberado" )

    ui.destroy(ret)

    --trata com pcall para evitar que o lua gere um erro e pare a execucao do teste
    local result = pcall(function() local acc = ret:accepted() end )

    assert_false( result, "test_destroy4. destruir menu. Handle nao foi liberado" )
end

-- ****************** TESTE DE TEXTFIELD:TEXT (Autom�ticos) ****************** --

-- Testes para o text (4 teste)--
tc6 = TestCase("ui.text")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_text1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.text. Teste com par�metros 'nil'        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do text (1/4)--
function tc6.test_text1()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        local ret, error_message, error_code = tf:text(nil)

        ui.destroy(tf)

        assert_equal("", ret, "Erro no teste de text passando 'nil'. Retorno incorreto", true)

    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

-- ****************** TESTE DE TEXTFIELD:TEXT (Interativos) ****************** --

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_text2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.text. Teste setando o texto para        --
--            'Texto Teste'                                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do text (2/4)--
function tc6.test_text2()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Textfield:text",0,0)
        display.print("setando texto p/",1,0)
        display.print("Texto Teste",2,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:text("Texto Teste")

        if( not assert_equal( "Texto Teste", ret, "Retorno incorreto no teste de text setando texto p/'Texto Teste'" ) ) then
            ui.destroy(tf)
            return
        end

        tf:show()

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste de text setando texto p/'Texto Teste'")

        ui.destroy(tf)
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_text3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.text. Teste setando o texto para        --
--            'Teste Com String Extremamente Grande Em Relacao ao Display do POS'                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do text (3/4)--
function tc6.test_text3()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Textfield:text",0,0)
        display.print("setando texto c/",1,0)
        display.print("String gigante",2,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local cols, rows = display.geometry()
        local text = string.rep( "String gigante", math.ceil( ( cols * rows ) / 14 ) )

        display.clear()

        local ret, error_message, error_code = tf:text( text )

        if( not assert_nil( ret, "Retorno incorreto no teste de text setando texto c/ string gigante" ) ) then
            ui.destroy(tf)
            return
        end

        if( not assert_equal( TST_STAT_TEXTTRUNCATE, error_code, "Codigo de erro incorreto no teste de text setando texto c/ string gigante" ) ) then
            ui.destroy(tf)
            return
        end

        tf:show()

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste de text setando texto c/ string gigante")

        ui.destroy(tf)
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_text4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.text. Teste setando o texto com         --
--            string vazia                                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do text (4/4)--
function tc6.test_text4()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Textfield:text",0,0)
        display.print("setando texto c/",1,0)
        display.print("String vazia",2,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:text("")

        if( not assert_equal( "", ret, "Retorno incorreto no teste de text setando texto c/string vazia" ) ) then
            ui.destroy(tf)
            return
        end

        tf:show()

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste de text setando texto c/string vazia")

        ui.destroy(tf)
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end


-- ****************** TESTE DE TEXTFIELD:MAXLENGHT (Autom�ticos) ****************** --

-- Testes para o maxlenght (1 teste)--
tc7 = TestCase("ui.maxlenght")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_maxlenght1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.text. Teste do maxlenght                --
--            Caso de sucesso                                                                    --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
-- Teste do maxlenght (1/1)--
function tc7.test_maxlenght1()
    local textf = ui.textfield("Title", "Teste", 13)

    if (textf ~= nil) then
        local ret = textf:maxlength()

        ui.destroy(textf)

        assert_equal(13, ret, "Erro no teste do maxlenght")
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end


-- ****************** TESTE DE TEXTFIELD:PATTERN (Interativos) ****************** --

-- Testes para o pattern (3 teste)--
tc8 = TestCase("ui.pattern")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pattern2                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.pattern. Teste com pattern igual a      --
--            '##/##/####'                                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pattern (2/3)--
function tc8.test_pattern2()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Textf:pattern",0,0)
        display.print("setando pattern",1,0)
        display.print("##/##/####",2,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:pattern("##/##/####")

        if( not assert_true( ret,"Retorno incorreto no teste de pattern passando '##/##/####'", true ) ) then
            ui.destroy(tf)
            return
        end

        tf:show()

        key = lerTecla()

        ui.destroy(tf)

        assert_equal(KEY_ONE, key, "Erro no teste de pattern passando '##/##/####'")
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pattern3                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.pattern. Teste com jokechar igual a '@' --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pattern (3/3)--
function tc8.test_pattern3()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Textf:pattern",0,0)
        display.print("setando jokechar",1,0)
        display.print("'@'",2,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:pattern("@@/@@/@@@@", "@")

        if( not assert_true( ret,"Retorno incorreto no teste de pattern com jokechar '@'", true ) ) then
            ui.destroy(tf)
            return
        end

        tf:show()

        key = lerTecla()

        ui.destroy(tf)

        assert_equal(KEY_ONE, key, "Erro no teste de pattern com jokechar '@'")

    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

-- ****************** TESTE DE TEXTFIELD:SHOW (Interativos) ****************** --

-- Testes para o show (2 teste)--
tc11 = TestCase("ui.textfield:show")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_show1                                                               --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.show. Teste para testar o retorno true  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do show (1/2)--
function tc11.test_textfield_show1()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Qdo o textfield aparecer pressione enter",0,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:show()

        ui.destroy(tf)

        assert_true(ret, "Retorno incorreto no teste de show com o usuario pressionando enter", true )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste de show com o usuario pressionando enter")
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_show2                                                               --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.show. Teste para testar o retorno false --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do show (2/2)--
function tc11.test_textfield_show2()
    local tf = ui.textfield("Title", "Teste")

    if (tf ~= nil) then
        display.clear()
        display.print("Qdo o textfield aparecer pressione cancel",0,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = tf:show()

        ui.destroy(tf)

        assert_false( ret, "Retorno incorreto no teste de show com o usuario pessionando cancel" ,false )

        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste de show com o usuario pressionando cancel")
    else
        assert_fail("Falha na pre-condicao do teste(criar textfield)")
    end
end


-- ****************** TESTE DE MENU:ACCEPTED (Interativos) ****************** --

-- Testes para o accepted (2 teste)--
tc16 = TestCase("ui.accepted")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_accepted2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.accepted. Teste interativo              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do accepted (2/2)--
function tc16.test_accepted2()
    local me = ui.menu("Teste", {"Op 1",  "Op 2", "Op 3", "Sair"})

    if (me ~= nil) then
        display.clear()
        display.print("Menu:accepted",0,0)
        display.print("Para finalizar escolha a opcao 4",1,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        repeat
              me:show()
              display.clear();
              display.print("Opcao Escolhida", 0, 0)
              display.print(me:accepted(),1,0)

              display.print("Press any key...", 3, 0)

              keyboard.getkeystroke(-1)
        until me:accepted() == 4

        display.clear(3)
        key = lerTecla()
        assert_equal(KEY_ONE, key, "Erro no teste interatio de accepted")

        ui.destroy(me)
    else
        assert_fail("Falha na pre-condicao do teste(criar menu)")
    end
end


-- ****************** TESTE DE MENU:SHOW (Interativos) ****************** --

-- Testes para o show (2 teste)--
tc17 = TestCase("ui.menu:show")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_show1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.show. Teste para testar o retorno true  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do show (1/2)--
function tc17.test_menu_show1()
    local me = ui.menu("Teste", {"Op 1",  "Op 2", "Op 3"})

    if (me ~= nil) then
        display.clear()
        display.print("Qdo o menu aparecer pressione enter",0,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = me:show()

        ui.destroy(me)

        assert_true(ret, "Retorno incorreto no teste de show com o usuario pessionando enter" ,true)
        
        key = lerTecla()
        assert_equal( KEY_ONE, key, "Erro no teste de show com o usuario pessionando enter" )
    else
        assert_fail("Falha na pre-condicao do teste(criar menu)")
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_show2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o ui.show. Teste para testar o retorno false --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do show (2/2)--
function tc17.test_menu_show2()
    local me = ui.menu("Teste", {"Op 1",  "Op 2", "Op 3"})

    if (me ~= nil) then
        display.clear()
        display.print("Qdo o menu aparecer pressione cancel",0,0)
        display.print("Press any key...", 3, 0)

        keyboard.getkeystroke(-1)

        local ret = me:show()

        ui.destroy(me)

        assert_false(ret, "Retorno invalido no teste de show com o usuario pessionando cancel")
        
        key = lerTecla()
        assert_equal( KEY_ONE, key, "Erro no teste de show com o usuario pessionando cancel" )
    else
        assert_fail("Falha na pre-condicao do teste(criar menu)")
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGN (Autom�ticos) ****************** --

-- Testes para o textfield:align --
tc18 = TestCase("textfield:align")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align1                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield passando halign invalido                                           --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align1( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:align( "invalido" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align2                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield passando valign invalido                                           --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align2( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:align( "center", "invalido" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )
        ----------------------------

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align3                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield passando halign vazio                                              --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align3( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:align( "" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align4                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield passando valign vazio                                              --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align4( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:align( "center", "" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGN (Interativos) ****************** --

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align5                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield apenas passando halign(todos os valores possiveis)                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align5( )

    display.clear()
    display.print( "Textfield. Teste de halign" )

    management.sleep( 100 )

    --laco que cria um textfield alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_halign in ipairs( { "left", "center", "right" } ) do

        display.clear()
        display.print( string.format( "Alterando o alinhamento horizontal do textfield para '%s'. Press any key", actual_halign ) )
        keyboard.getkeystroke( -1 )

        --cria um textfield
        local tf, error_message = ui.textfield("Title",  string.format( "%s", actual_halign ) )

        --verifica se textfield foi criado
        assert_not_nil( tf, string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = tf:align( actual_halign )

        tf:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_textfield_align5. Retorno incorreto para o parametro '%s'" , actual_halign ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_textfield_align5. Resultado incorreto para o parametro '%s'" , actual_halign ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( tf )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align6                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:align.                           --
--            Chama textfield apenas passando valign(todos os valores possiveis)                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc18.test_textfield_align6( )

    display.clear()
    display.print( "Textfield. Teste de valign" )

    management.sleep( 100 )

    --laco que cria um textfield alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_valign in ipairs( { "top", "middle", "botton" } ) do

        display.clear()
        display.print( string.format( "Alterando o alinhamento vertical do textfield para '%s'. Press any key", actual_valign ) )
        keyboard.getkeystroke( -1 )

        --cria um textfield
        local tf, error_message = ui.textfield("Title",  string.format( "%s", actual_valign ) )

        --verifica se textfield foi criado
        assert_not_nil( tf, string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = tf:align( "center", actual_valign )

        tf:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_textfield_align6. Retorno incorreto para o parametro '%s'" , actual_valign ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_textfield_align6. Resultado incorreto para o parametro '%s'" , actual_valign ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( tf )
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGNTEXT (Autom�ticos) ****************** --

-- Testes para o textfield:aligntext --
tc19 = TestCase("textfield:aligntext")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_aligntext1                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:aligntext                        --
--            Chama textfield passando align invalido                                            --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc19.test_textfield_aligntext1( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:aligntext( "invalido" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_aligntext2                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:aligntext                        --
--            Chama textfield passando align vazio                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc19.test_textfield_aligntext2( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:aligntext( "" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGNTEXT (Interativos) ****************** --
---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_align3                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:aligntext                        --
--            Chama textfield apenas passando align(todos os valores possiveis)                  --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc19.test_textfield_aligntext3( )

    display.clear()
    display.print( "Textfield. Teste de aligntext" )

    management.sleep( 100 )

    --laco que cria um textfield alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_align in ipairs( { "left", "right" } ) do

        display.clear()
        display.print( string.format( "Alterando o alinhamento do texto do textfield para '%s'. Press any key", actual_align ) )
        keyboard.getkeystroke( -1 )

        --cria um textfield
        local tf, error_message = ui.textfield("Title",  string.format( "%s", actual_align ) )

        --verifica se textfield foi criado
        assert_not_nil( tf, string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = tf:aligntext( actual_align )

        tf:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_textfield_aligntext3. Retorno incorreto para o parametro '%s'" , actual_align ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_textfield_aligntext3. Resultado incorreto para o parametro '%s'" , actual_align ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( tf )
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGNLABEL (Autom�ticos) ****************** --

-- Testes para o textfield:alignlabel --
tc20 = TestCase("textfield:alignlabel")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_alignlabel1                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:alignlabel                       --
--            Chama textfield passando align invalido                                            --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc20.test_textfield_alignlabel1( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:alignlabel( "invalido" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_alignlabel2                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:alignlabel                       --
--            Chama textfield passando align vazio                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc20.test_textfield_alignlabel2( )

    --cria um textfield
    local tf, error_message = ui.textfield("Title",  "Teste" )

    --verifica se textfield foi criado
    if( tf ~= nil ) then

        local return_value, error_message, error_code = tf:alignlabel( "" )

        ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o textfield
        ui.destroy( tf )
    else
        assert_fail( string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

-- ****************** TESTE DE TEXTFIELD:ALIGNLABEL (Interativos) ****************** --
---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_textfield_alignlabel3                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o textfield:alignlabel                       --
--            Chama textfield apenas passando align(todos os valores possiveis)                  --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc20.test_textfield_alignlabel3( )

    display.clear()
    display.print( "Textfield. Teste de alignlabel" )

    management.sleep( 100 )

    --laco que cria um textfield alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_align in ipairs( { "left", "center", "right" } ) do
    
        display.clear()
        display.print( string.format( "Alterando o alinhamento do titulo do textfield para '%s'. Press any key", actual_align ) )
        keyboard.getkeystroke( -1 )

        --cria um textfield
        local tf, error_message = ui.textfield("Title",  string.format( "%s", actual_align ) )

        --verifica se textfield foi criado
        assert_not_nil( tf, string.format( "Dependencia: textfield nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = tf:alignlabel( actual_align )

        tf:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_textfield_alignlabel3. Retorno incorreto para o parametro '%s'" , actual_align ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_textfield_alignlabel3. Resultado incorreto para o parametro '%s'" , actual_align ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( tf )
    end
end

-- ****************** TESTE DE MENU:ALIGNTITLE (Autom�ticos) ****************** --

-- Testes para o menu:aligntitle --
tc21 = TestCase("menu:aligntitle")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_aligntitle1                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:aligntitle.                           --
--            Chama menu passando align invalido                                                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc21.test_menu_aligntitle1( )

    --cria um menu
    local men, error_message = ui.menu( "Teste", { "op1", "op2" } )

    --verifica se menu foi criado
    if( men ~= nil ) then

        local return_value, error_message, error_code = men:aligntitle( "invalido" )

         ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )


        --destroi o menu
        ui.destroy( men )
    else
        assert_fail( string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_aligntitle2                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:aligntitle                            --
--            Chama menu passando aligntitle vazio                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc21.test_menu_aligntitle2( )

    --cria um menu
    local men, error_message = ui.menu( "Teste", { "op1", "op2" } )

    --verifica se menu foi criado
    if( men ~= nil ) then

        local return_value, error_message, error_code = men:aligntitle( "" )

         ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o menu
        ui.destroy( men )
    else
        assert_fail( string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

-- ****************** TESTE DE MENU:ALIGNTITLE (Interativos) ****************** --
---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_aligntitle3                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:aligntitle                            --
--            Chama menu apenas passando aligntitle(todos os valores possiveis)                  --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc21.test_menu_aligntitle3( )

    display.clear()
    display.print( "Menu. Teste de aligntitle" )

    management.sleep( 100 )

    --laco que cria um menu alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_align in ipairs( { "left", "center", "right" } ) do

        display.clear()
        display.print( string.format( "Alterando o alinhamento do titulo do menu para '%s'. Press any key", actual_align ) )
        keyboard.getkeystroke( -1 )

        --cria um menu
        local men, error_message = ui.menu( string.format( "%s", actual_align ), { "Op1", "Op2", "Op3" } )

        --verifica se menu foi criado
        assert_not_nil( men, string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = men:aligntitle( actual_align )

        men:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_menu_aligntitle3. Retorno incorreto para o parametro '%s'" , actual_align ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_menu_aligntitle3. Resultado incorreto para o parametro '%s'" , actual_align ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( men )
    end
end

-- ****************** TESTE DE MENU:ALIGNOPTIONS (Autom�ticos) ****************** --

-- Testes para o menu:alignoptions --
tc22 = TestCase("menu:alignoptions")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_alignoptions1                                                            --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:alignoptions.                         --
--            Chama menu passando align invalido                                                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc22.test_menu_alignoptions1( )

    --cria um menu
    local men, error_message = ui.menu( "Teste", { "Op1", "Op2" } )

    --verifica se menu foi criado
    if( men ~= nil ) then

        local return_value, error_message, error_code = men:alignoptions( "invalido" )

         ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o menu
        ui.destroy( men )
    else
        assert_fail( string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_alignoptions2                                                            --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:alignoptions.                         --
--            Chama menu passando align vazio                                                    --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc22.test_menu_alignoptions2( )

    --cria um menu
    local men, error_message = ui.menu( "Teste", { "Op1", "Op2" } )

    --verifica se menu foi criado
    if( men ~= nil ) then

        local return_value, error_message, error_code = men:alignoptions( "" )

         ----verifica os retornos----
        assert_nil( return_value, "Valign invalido. Retorno incorreto", true )

        assert_equal( TST_ERR_INVALIDARG, error_code, "Valign invalido. Codigo de erro incorreto", true )

        assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Valign invalido. Mensagem de erro incorreta", true )

        --destroi o menu
        ui.destroy( men )
    else
        assert_fail( string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
    end
end

-- ****************** TESTE DE MANU:ALIGNOPTIONS (Interativos) ****************** --
---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_menu_alignoptions3                                                            --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o menu:alignoptions.                         --
--            Chama menu apenas passando align(todos os valores possiveis)                       --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc22.test_menu_alignoptions3( )

    display.clear()
    display.print( "Menu. Teste de alignoptions" )

    management.sleep( 100 )

    --laco que cria um menu alterando o alinhamento para todos os alinhamentos possiveis
    for actual_index,actual_align in ipairs( { "left", "center", "right" } ) do

        display.clear()
        display.print( string.format( "Alterando o alinhamento das opcoes para '%s'. Press any key", actual_align ) )
        keyboard.getkeystroke( -1 )

        --cria um menu
        local men, error_message = ui.menu( string.format( "%s", actual_align ), { "Op1", "Op2", "Op3" } )

        --verifica se menu foi criado
        assert_not_nil( men, string.format( "Dependencia: menu nao pode ser criado:[%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --define o alinhamento
        local return_value = men:alignoptions( actual_align )

        men:show( )

        -- verifica o retorno
        assert_true( return_value, string.format( "test_menu_alignoptions3. Retorno incorreto para o parametro '%s'" , actual_align ), true )

        ----pergunta se a funcao funcionou----
        local result = lerTecla( )

        assert_equal( KEY_ONE, result, string.format( "test_menu_alignoptions3. Resultado incorreto para o parametro '%s'" , actual_align ), true )
        --------------------------------------

        --destroi o textfield
        ui.destroy( men )
    end
end

-- ****************** TESTE DE SCREENNUMBER  ****************** --

-- Testes para screennumber --
tc_screennumber = TestCase("screennumber")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_screennumber1                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Setando numero da tela de um textfield                                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_screennumber.test_screennumber1( )
    display.clear()

    display.print("A seguir um TextField", 0, 0)
    display.print("Com ScreenNumber 18", 1, 0)
    display.print("Sera exibido", 2, 0)
    display.print("Pressione uma tecla", 5, 0)

    keyboard.getkeystroke( -1 )

    local tf = ui.textfield( "Teste de", "Screen Number" )

    tf:screennumber( 18 )

    tf:show()

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de screennumber")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_screennumber2                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Setando numero da tela de um menu                                                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_screennumber.test_screennumber2( )
    display.clear()

    display.print("A seguir um menu", 0, 0)
    display.print("Com ScreenNumber 18", 1, 0)
    display.print("Sera exibido", 2, 0)
    display.print("Pressione uma tecla", 5, 0)

    keyboard.getkeystroke( -1 )

    local menu = ui.menu( "ScreenNumber" , {"Primeira","Segunda"} )

    menu:screennumber( 18 )

    menu:show()

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de screennumber")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_screennumber3                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Setando o numero da tela e em seguida removendo o numero com UI_NOSCREENNUMBER     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_screennumber.test_screennumber3( )
    display.clear()

    display.print("A seguir um TextField", 0, 0)
    display.print("Com ScreenNumber 12", 1, 0)
    display.print("Sera exibido", 2, 0)
    display.print("Pressione uma tecla", 5, 0)

    keyboard.getkeystroke( -1 )

    local tf = ui.textfield( "Teste de", "Screen Number" )

    tf:screennumber( 12 )

    tf:show()

    display.clear()

    display.print("Removendo o ScreenNumber", 0, 0)
    display.print("Com UI_NOSCREENNUMBER", 1, 0)
    display.print("Pressione uma tecla", 5, 0)

    keyboard.getkeystroke( -1 )

    tf:screennumber( UI_NOSCREENNUMBER )
    tf:show()

    display.clear()

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de screennumber")
end

lunit.run()
