require "lunit"

lunit.import "all"

tc1 = TestCase("ethernet")

function tc1.test_config1()
    assert_false(ethernet.config(10, 10000, false, {
            ipAddress="123",
            mask="255.255.255.255",
            gateway="172.27.64.1",
            dns1="0.0.0.0",
            dns2="172.27.65.22"
        }),
        "Foi possível definir os parâmetros para conexão ethernet com DHCP false e IP inválido.")
end

function tc1.test_config2()
    assert_true(ethernet.config(10, 10000, false, {
            ipAddress="172.27.100.254",
            mask="255.255.255.255",
            gateway="172.27.64.1",
            dns1="0.0.0.0",
            dns2="172.27.65.22"
        }),
        "Não foi possível definir os parâmetros para conexão ethernet com DHCP false.")
end

function tc1.test_config3()
    assert_true(ethernet.config(10, 10000, true),
        "Não foi possível definir os parâmetros para conexão ethernet com DHCP true.")
end

lunit.run()