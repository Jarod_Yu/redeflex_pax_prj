---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios da extens�o da impressora                   --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         26/01/2006  Cria��o                                          --
-- Leandro              lmf         03/02/2006  Cabe�alho das fun��es                            --
-- Jamerson Lima        jrfl        26/07/2006  Testes de fonte (CR 9844)                        --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"

---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------


-- ****************** TESTE DE LINEFEED ****************** --

-- Testes para o linefeed da impressora (2 testes)--
tc = TestCase("printer.linefeed")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_linefeed1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.linefeed, solicita que o usu�rio   --
--            informe a quantidade de linhas do linefeed e presione zero para encerrar o teste.  --
--            No final o usu�rio � questionado sobre a corretude do teste                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Solicita digita��o do n�mero de linhas a avan�ar (1/2)--
function tc.test_linefeed1()

    local key = 0

    repeat
        display.clear()
        display.print("Digite <0-9> para avancar papel (<CANCEL> para sair)", 0,0)

        printer.print( "*" )

        key = keyboard.getkeystroke(-1)

        if (key > 1 and key <= 10) then
           local result = printer.linefeed(key - 1)

           assert_true( result, "Retorno incorreto no teste de linefeed", true )
        end
    until ( key == KEY_CANCEL )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de linefeed")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_linefeed2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.linefeed, passa um numero de li-   --
--            nhas negativas.                                                                    --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc.test_linefeed2()

    local result = printer.linefeed( -5 )

    assert_true( result, "Retorno incorreto no teste de linefeed com valor negativo" )
end


-- ****************** TESTE DE PRINT ****************** --

-- Testes para o print da impressora (4 testes) --
tc2 = TestCase("printer.print")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.print, imprime caracteres          --
--            alfanum�ricos e pergunta ao usu�rio se a impress�o foi ok                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Imprime caracteres alfanumericos (1/4)--
function tc2.test_print1()
    display.clear()

    display.print("Imprime caracteres alfanumericos",0,0)
    local result = printer.print("ABCDEFEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
    printer.linefeed( 10 )

    assert_true( result, "Retorno incorreto no teste de impressao de caracteres alfanumericos", true );

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de impressao de caracteres alfanumericos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.print, imprime caracteres          --
--            epeciais e pergunta ao usu�rio se a impress�o foi ok                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Imprime caracteres especiais (2/4)--
function tc2.test_print2()
    display.clear()

    display.print("Imprime caracteres especiais",0,0)
    local result = printer.print("'!@#$%&*()-_=+[]{}|,.;/:?")
    printer.linefeed( 10 )

    assert_true( result, "Retorno incorreto no teste de impressao de caracteres especiais", true );

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de impressao de caracteres especiais")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.print, imprime texto com           --
--            quebra de linha e pergunta ao usu�rio se a impress�o foi ok                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Imprime texto com quebra de linha (3/4)--
function tc2.test_print3()
    display.clear()

    display.print("Imprime texto com quebra de linha",0,0)
    local result = printer.print("Hiper\nFlex")
    printer.linefeed( 10 )

    assert_true( result, "Retorno incorreto no teste de impressao com quebra de linha", true );

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de impressao com quebra de linha")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.print, passa como par�metro        --
--            texto nil e verifica o comportamento do POS                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Chama a fun��o com texto NIL (4/4)--
function tc2.test_print4()
    display.clear()

    display.print("Imprime passando parametro nulo",0,0)
    local result = printer.print( nil )

    printer.linefeed( 10 )

    assert_true( result, "Retorno incorreto no teste de impressao com parametro nulo", true );

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de impressao com parametro nulo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.print, imprime texto               --
--            em negrito e normal.                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc2.test_print5()

    local result = printer.print( "1: Normal(padrao)", "left" )
    assert_true( result, "Retorno incorreto no teste de impressao sem negrito(padrao)", true );

    result = printer.print( "2: Bold true", "left", true )
    assert_true( result, "Retorno incorreto no teste de impressao com negrito", true );

    result = printer.print( "3: Bold false", "left", false )
    assert_true( result, "Retorno incorreto no teste de impressao sem negrito", true );

    printer.linefeed( 10 )

    display.clear()
    display.print( "Impressao correta?" )

    tecla = lerTecla()
    assert_equal( KEY_ONE, tecla, "Erro no teste de impressao alternando negrito" )
end

-- ****************** TESTE DE CHECK ****************** --

-- Testes para o check da impressora (2 testes) --
tc3 = TestCase("printer.check")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_check1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.check, verifica o status OK        --
--            e questiona o usu�rio sobre a corretude do teste                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Verifica se o status � OK (1/2)--
function tc3.test_check1()
    display.clear()

    display.print("Verifica status",0,0)
    
    management.sleep( 1000 )

    local ret = printer.check()

    display.clear()
    display.print("Status: ".. tostring( ret ),2,0)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de check da impressora")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_check2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.check, solicita ao usu�rio que     --
--            retire o papel da maquineta e verifica o status falta de papel                     --
--            e questiona o usu�rio sobre a corretude do teste                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Verifica o status de falta de papel (2/2)--
function tc3.test_check2()
    display.clear()

    display.print("Retire o papel",0,0)

    display.print("Press any key...",2,0)

    keyboard.getkeystroke()

    display.clear()

    display.print("Verifica status",0,0)
    
    management.sleep( 1000 )

    local ret = printer.check()

    display.print("Status: "..tostring( ret),1,0 )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de check da impressora")
end

-- ****************** TESTE DE PRINTIMAGE ****************** --

-- Testes para o printimage (1 teste)--
tc4 = TestCase("printer.printimage")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_printimage1                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printer.printimage, imprime a imagem       --
--            referente ao arquivo "teste.bmp", e questiona ao usu�rio a corretude do teste      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de impress�o de imagem (1/1)--
function tc4.test_printimage1()
    display.clear()
    display.print("Teste de impressao de imagem",0,0)

    local imagem = os.loadimage("teste.bmp")

    local ret = printer.printimage( imagem )

    printer.linefeed( 10 )

    assert_true( ret, "Caso de sucesso passando apenas o arquivo. Retorno incorreto", true )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Caso de sucesso. Erro imprimindo imagem")
end

-- ****************** TESTE DE FONT ****************** --

tc_font = TestCase("printer.font")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Alterna para todas as fontes         --
--            possiveis                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font1()

    local system_fonts = { "small", "normal", "large" }
    local result = TST_SUCCESS

    for _,curr_font in ipairs( system_fonts ) do

        result = printer.font( curr_font )

        assert_equal( curr_font, result, string.format( "test_font1. Alterando para fonte '%s'. Retorno incorreto", curr_font ), true )

        printer.print( curr_font )
        printer.linefeed( 10 )
        
        display.clear()
        display.print( "Impressao correta?" )

        local pressed_key = lerTecla()
        assert_equal( KEY_ONE, pressed_key, string.format( "test_font1. Erro  Alterando para fonte '%s'", curr_font ), true )
    end

    printer.font( "normal" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Seta uma fonte invalida              --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font2()

    local result, error_message, error_code = printer.font( "inv" )

    assert_nil( result, "test_font2. Alterando para fonte invalida. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDARG, error_code, "test_font2. Alterando para fonte invalida. Codigo de erro incorreto", true )
    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_font2. Alterando para fonte invalida. Mensagem de erro incorreta" )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Chama a funcao sem passar parametros --
--            e verifica se ocoreu alguma mudanca de fonte                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font3()

    printer.print( "Teste de fonte. A fonte nao deve mudar" )

    printer.font()
    printer.print( string.format( "fonte atual: %s\nfonte continua a mesma?", printer.font() ) )
    
    printer.linefeed( 10 )

    local pressed_key = lerTecla()
    assert_equal( KEY_ONE, pressed_key, "test_font3. Erro chamando font sem param." )
end

lunit.run()
