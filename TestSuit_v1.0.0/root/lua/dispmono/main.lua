---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes unit�rios da extens�o do display do LUA               --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         25/01/2006  Cria��o                                          --
-- Leandro              lmf         03/02/2006  Cabe�alho das fun��es                            --
-- Henrique Borges      hbas        17/03/2006  Coment�rio dos testes de nil                     --
-- Jamerson Lima        jrfl        11/05/2006  Correcao de erros                                --
-- Jamerson Lima        jrfl        06/06/2006  Rework(HFLEX-LUA-TSTR-CODE-INSP005)              --
-- Jamerson Lima        jrfl        26/07/2006  Testes de fonte (CR 9844)                        --
-- Guilherme            gkmo        28/09/2006  Corrigindo testes (16643)                        --
-- Leandro              lmf         05/01/2007  Inclusao de novos testes (19096)                 --
-- Leandro              lmf         15/01/2007  Rework (HFLEX-LUA-TSTR-CODE-REV012)              --
-- Leonardo N�brega     lfn         26/03/2007  Teste de posi��o vertical do texto (20408).      --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

---------------------------------------------------------------------------------------------------
--                                     Variaveis GLobais                                         --
---------------------------------------------------------------------------------------------------

--plataforma pode ser: 'ingenico', 'hypercom' ou 'pc'
local plataforma = "ingenico"

---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

-- ****************** TESTE DE DISPLAY.PRINT ****************** --

-- Testes para o print do display (9 testes)--
tc = TestCase("display.print")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print1                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, exibe caracteres            --
--            alfanum�ricos no display e pergunta ao usu�rio se a exibi��o foi ok                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de impress�o de caracteres alfanum�ricos (1/9)--
function tc.test_display_print1( )

    local texto = "ABCDEFGHIJKLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0

    largura, altura = display.geometry( )

    display.clear( )
    display.print( "Preenche com caracteres alfanumericos",0,0 )
    display.print( "Press any key...",3,0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    linha, coluna = display.print( texto, 0, 0 )
    assert_equal( ( string.len( texto ) % largura ), coluna, "test_display_print1. Coluna retornada incorreta no teste de caracteres alfanumericos", true )
    assert_equal( ( math.floor( string.len( texto ) / largura ) ), linha, "test_display_print1. Linha retornada no teste de caracteres alfanumericos", true )

    management.sleep( 100 )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_display_print1. Erro no teste de caracteres alfanumericos" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print2                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, exibe caracteres            --
--            epeciais no display e pergunta ao usu�rio se a exibi��o foi ok                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe caracteres especiais (2/9)--
function tc.test_display_print2()

    local texto = "'!@#$%&*()-_=+[]{}|,.;/:?"
    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0

    largura, altura = display.geometry( )

    display.clear( )
    display.print( "Exibe caracteres especiais", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    linha, coluna = display.print( texto, 0, 0 )

    assert_equal( ( string.len( texto ) % largura ), coluna, "test_display_print2. Coluna retornada incorreta no teste de caracteres alfanumericos", true )
    assert_equal( ( math.floor( string.len( texto ) / largura ) ), linha, "test_display_print2. Linha retornada no teste de caracteres alfanumericos", true )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_display_print2. Erro no teste de caracteres especiais" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print3                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, exibe texto com             --
--            quebra de linha no display e pergunta ao usu�rio se a exibi��o foi ok              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto com quebra de linha (3/9)--
function tc.test_display_print3()

    local coluna = 0
    local linha = 0

    largura, altura = display.geometry( )

    display.clear( )
    display.print("Imprimi HiperFlex quebrado (Hiper/Flex)",0,0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear()
    linha, coluna = display.print( "Hiper\nFlex", 0, 0 )

    assert_equal( 4, coluna, "test_display_print3. Coluna retornada incorreta no teste de quebra de linha", true )
    assert_equal( 1, linha, "test_display_print3. Linha retornada no teste de quebra de linha", true )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_display_print3. Erro no teste de quebra de linha" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print4                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, preenche pausadamente       --
--            o display e pergunta ao usu�rio se o preenchimento foi ok                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Preenche pausadamente todo o display (4/9)--
function tc.test_display_print4()

    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0

    display.clear()
    display.print("Preenche pausadamente todo o display", 0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()

    largura, altura = display.geometry( )

    for l = 0, altura - 1, 1 do
        for c = 0, largura - 1, 1 do

            linha, coluna = display.print( "*", l, c )

            --se for a ultima coluna da linha, o cursor deve voltar para o comeco da proxima linha
            if( c == largura - 1 ) then
                --comeco da proxima linha
                assert_equal( 0, coluna, "test_display_print4. Coluna retornada incorreta no teste de preenchimento do display com '*'", true )

                --se nao for a ultima linha, deve ir para a proxima linha
                if( l ~= altura - 1 ) then
                    assert_equal( l + 1, linha, "test_display_print4. Linha retornada no teste de preenchimento do display com '*'", true )
                end

            else
                assert_equal( c + 1, coluna, "test_display_print4. Coluna retornada incorreta no teste de preenchimento do display com '*'", true )
                assert_equal( l, linha, "test_display_print4. Linha retornada no teste de preenchimento do display com '*'", true )
            end

            management.sleep(10)
        end
    end

    management.sleep(100)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_display_print4. Erro no teste de preenchimento do display com '*'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print5                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, exibe uma string maior que  --
--            o display e pergunta ao usu�rio se a exibi��o foi ok                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Imprime uma string maior do que o display (5/9)--
function tc.test_display_print5()

    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0
    local texto = ""

    largura, altura = display.geometry( )

    local tamanho_texto = largura * altura

    texto = string.rep( "0", tamanho_texto + 10 )

    display.clear()
    display.print( "Devera preencher o display com zeros", 0, 0 )

    display.print("Press any key...",3,0)

    display.clear()

    linha, coluna = display.print( texto, 0, 0 );
    
    keyboard.getkeystroke(-1)

    assert_equal( 0, coluna, "test_display_print5. Coluna retornada incorreta no teste de preenchimento do display com zeros", true )
    assert_equal( 0, linha, "test_display_print5. Linha retornada no teste de preenchimento do display com zeros", true )

    management.sleep(100)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_display_print5. Erro no teste de preenchimento do display com zeros")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print6                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.print, tenta exibir texto          --
--            em coordenadas negativas do display e pergunta ao usu�rio se a exibi��o foi ok     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Tenta imprimir na tela com coordenadas negativas (6/9)--
function tc.test_display_print6()
    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0
    local texto = "Imprime linha -1 e col -1"

    largura, altura = display.geometry( )

    display.clear()
    linha, coluna = display.print( texto, -1,-1 )

    assert_equal( 0, coluna, "test_display_print6. Coluna retornada incorreta no teste de coordenadas negativas", true )
    assert_equal( 0, linha, "test_display_print6. Linha retornada no teste de coordenadas negativas", true )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_display_print6. Erro no teste de coordenadas negativas")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print7                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--           Funcao respons�vel por validar a fun��o display.print, tenta exibir caracteres fora --
--           do display e pergunta ao usu�rio se a exibi��o foi ok                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Tenta imprimir e coordenadas fora do display (7/9)--
function tc.test_display_print7()

    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0
    local texto = "Tentativa de impressao fora do display"

    largura, altura = display.geometry( )

    display.clear()
    linha, coluna = display.print( texto, altura + 1, largura + 1 )

    assert_equal( 0, coluna, "test_display_print7. Coluna retornada incorreta imprimindo fora do display", true )
    assert_equal( 0, linha, "test_display_print7. Linha retornada incorreta imprimindo fora do display", true )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_display_print7. Erro imprimindo fora do display")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_display_print9                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe texto passando como           --
--            par�metros string e booleans, e questiona o usu�rio sobre a corretude do teste     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto passando como par�metro strings e booleans(5/5)--
function tc.test_display_print9()
    display.clear()
    display.print("Teste passando strings e bools. Deve imprimir:",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()
    display.print("(1 = 1) true\n(1 > 2) false",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()

    print("(1 = 1)", true, "\n(1 > 2)", false)


    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_display_print9. Erro passando como parametros strings e booleans")
end

-- ****************** TESTE DE CLEAR ****************** --

-- Testes para o clear do display (3 testes)--
tc2 = TestCase("display.clear")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_clear1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.clear, limpa linha a linha         --
--            o display e pergunta ao usu�rio se a opera��o foi ok                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Limpa linha a linha do display (1/3)--
function tc2.test_clear1()

    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0
    local texto = ""

    largura, altura = display.geometry( )

    local tamanho_texto = largura * altura

    for i = 1, tamanho_texto, 1 do
        texto = texto .. "*"
    end

    display.clear( )
    display.print( "Apaga linha a linha do display", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )

    display.print( texto, 0, 0 )
    management.sleep( 100 )

    for l = 0, largura - 1, 1 do
        display.clear( l )
        management.sleep( 100 )
    end

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_clear1. Erro apagando display linha a linha" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_clear2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.clear, limpa uam linha inexistente --
--            do display, se der erro na opera��o o lunit exibe uma msg de erro                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Tenta apagar um linha de coordenada fora do diplay (2/3)--
function tc2.test_clear2()

    local linhas = 0
    local colunas = 0

    colunas,linhas = display.geometry( )

    assert_true( ( linhas > 0 ) and ( colunas > 0 ), "Dependencia: geometry retornou num. linhas incorreto", true )

    local texto = string.rep( "*", linhas * colunas )

    display.clear()
    display.print( "Apaga linha fora do display. Nao deve acontecer nada. Press any key" )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.print( texto )
    local result = display.clear( linhas + 1 )

    local tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_clear2. Erro passando uma linha fora do display" )


end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_clear3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.clear, limpa todo                  --
--            o display e pergunta ao usu�rio se a opera��o foi ok                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Limpa todo o display (3/3)--
function tc2.test_clear3( )

    local largura = 0
    local altura = 0
    local coluna = 0
    local linha = 0
    local texto = ""

    largura, altura = display.geometry( )

    local tamanho_texto = largura * altura

    for i = 1, tamanho_texto, 1 do
        texto = texto .. "*"
    end

    display.clear()
    display.print( "Apaga todo o display. Nao deve ficar nenhuma linha com asterisco. Press any key" )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.print( texto, 0, 0 )

    display.clear( )

    local tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_clear3. Erro limpando o display" )
end


-- ****************** TESTE DE GEOMETRY ****************** --

-- Testes para o geometry do display (1 teste)--
tc3 = TestCase( "display.geometry" )

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_geometry1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o display.geometry, compara as dimens�es     --
--            do display previamente estabelecidas com as retornadas pela display.geometry       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Mostra geometria do display (1/1)--
function tc3.test_geometry_pixels1( )

    local cols   = 0
    local rows   = 0
    local width  = 0
    local height = 0
    local i      = 0
    local msg    = ""

    --teste de geometry

    cols, rows = display.geometry()

    assert_true( is_number( cols ), string.format( "test_geometry_pixels1. geometry. Qnd de colunas incorreta (%s)", tostring( cols ) ), true )
    assert_true( is_number( rows ), string.format( "test_geometry_pixels1. geometry. Qnd de linhas incorreta (%s)", tostring( rows ) ), true )

    msg = string.rep( "0", ( cols * rows ) )

    display.clear()
    display.print( string.format( "Deve aparecer %i colunas e %i linhas. Press key", cols, rows ) )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.print( msg )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.print( string.format( "Colunas: %i", cols ), 0, 0 )
    display.print( string.format( "Linhas:  %i", rows ), 1, 0 )

    local tecla = lerTecla()

    assert_equal( KEY_ONE, tecla, "test_geometry_pixels1. geometry. Erro nos numeros de colunas e linhas retornadas", true )

    --teste de pixels

    width, height = display.pixels()

    assert_true( is_number( width ), string.format( "test_geometry_pixels1. pixels. Largura incorreta (%s)", tostring( width ) ), true )
    assert_true( is_number( height ), string.format( "test_geometry_pixels1. pixels. Altura incorreta (%s)", tostring( height ) ), true )

    display.clear()
    display.print( string.format( "De acordo com pixels & geometry. Um caracter contem %iX%i pixels\n0X_/|", ( width / cols ), ( height / rows ) ) )

    tecla = lerTecla()
    assert_equal( KEY_ONE, tecla, "test_geometry_pixels1. pixels. Erro na largura e altura retornadas", true )
end


-- ****************** TESTE DE IO.WRITE ****************** --

-- O io.write, quando um arquivo n�o � especificado atrav�s da fun��o io.output,   --
-- imprime sua sa�da na sa�da padr�o que no caso � o display. Como os testes de io --
-- s�o autom�ticos, ent�o optou-se por p�r estes testes neste arquivo              --

-- Testes para o io.write (4 teste)--
tc5 = TestCase( "io.write" )

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_iowrite1                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, exibe caracteres                 --
--            alfanum�ricos no display e pergunta ao usu�rio se a exibi��o foi ok                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de impress�o de caracteres alfanum�ricos (1/4)--
function tc5.test_iowrite1( )
    display.clear( )
    display.print( "Preenche com caracteres alfanumericos", 0, 0 )
    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    local result = io.write( "ABCDEFGHIJKLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" );

    assert_true( result, "test_iowrite1. Retorno incorreto no teste de caracteres alfanumericos", true )

    management.sleep( 100 )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_iowrite1. Erro no teste de caracteres alfanumericos" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_iowrite2                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, exibe caracteres                 --
--            epeciais no display e pergunta ao usu�rio se a exibi��o foi ok                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe caracteres especiais (2/4)--
function tc5.test_iowrite2( )
    display.clear( )
    display.print( "Exibe caracteres especiais", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear()
    local result = io.write( "'!@#$%&*()-_=+[]{}|,.;/:?" )

    assert_true( result, "test_iowrite2. Retorno incorreto no teste de caracteres especiais", true )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_iowrite2. Erro no teste de caracteres especiais" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_iowrite3                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, exibe texto com                  --
--            quebra de linha no display e pergunta ao usu�rio se a exibi��o foi ok              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto com quebra de linha (3/4)--
function tc5.test_iowrite3( )
    display.clear( )
    display.print( "Imprimi HiperFlex quebrado (Hiper/Flex)", 0, 0 )

    display.print( "Press any key...",3,0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    local result = io.write( "Hiper\nFlex" )

    assert_true( result, "test_iowrite3. Retorno incorreto no teste de quebra de linha", true )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_iowrite3. Erro no teste de quebra de linha" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_iowrite4                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.write, exibe texto passando como        --
--            par�metros string e n�meros, e questiona o usu�rio sobre a corretude do teste      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto passando como par�metro strings e n�meros(4/4)--
function tc5.test_iowrite4( )
    display.clear( )
    display.print( "Teste passando strings e numeros. Deve imprimir:", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    display.print( "Hiplerflex:\nGerentes:1\nMembros:15", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    local result = io.write( "Hiperflex:\nGerentes:", 1, "\nMembros:", 15 )

    assert_true( result, "test_iowrite4. Retorno incorreto passando como parametros strings e numeros", true )

    tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_iowrite4. Erro passando como parametros strings e numeros" )
end


-- ****************** TESTE DE PRINT ****************** --

-- O print � uma fun��o b�sica utilizada para debug que utiliza a sa�da padr�o para exibi��o            --
-- das msgs. Sendo assim, optou-se por colocar seus testes neste arquivo, pelo mesmo motivo do io.write --

-- Testes para o print (4 teste)--
tc6 = TestCase( "print" )

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe caracteres                    --
--            alfanum�ricos no display e pergunta ao usu�rio se a exibi��o foi ok                --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de impress�o de caracteres alfanum�ricos (1/4)--
function tc6.test_print1( )
    display.clear( )
    display.print( "Preenche com caracteres alfanumericos", 0, 0 )
    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    print( "ABCDEFGHIJKLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" );

    management.sleep( 100 )

    local tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_print1. Erro no teste de caracteres alfanumericos" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe caracteres                    --
--            epeciais no display e pergunta ao usu�rio se a exibi��o foi ok                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe caracteres especiais (2/4)--
function tc6.test_print2( )
    display.clear( )
    display.print( "Exibe caracteres especiais", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke( -1 )

    display.clear( )
    print( "'!@#$%&*()-_=+[]{}|,.;/:?" )

    local tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_print2. Erro no teste de caracteres especiais" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe texto com                     --
--            quebra de linha no display e pergunta ao usu�rio se a exibi��o foi ok              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto com quebra de linha (3/4)--
function tc6.test_print3( )
    display.clear( )
    display.print( "Imprimi HiperFlex quebrado (Hiper/Flex)", 0, 0 )

    display.print( "Press any key...", 3, 0 )

    keyboard.getkeystroke(-1)

    display.clear( )
    print( "Hiper\nFlex" );

    local tecla = lerTecla( )

    assert_equal( KEY_ONE, tecla, "test_print3. Erro no teste de quebra de linha" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe texto passando como           --
--            par�metros string e n�meros, e questiona o usu�rio sobre a corretude do teste      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto passando como par�metro strings e n�meros(4/4)--
function tc6.test_print4()
    display.clear()
    display.print("Teste passando strings e numeros. Deve imprimir:",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()
    display.print("Hiplerflex:\nGerentes:1\nMembros:15",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()
    print("Hiperflex:\nGerentes:", 1, "\nMembros:", 15)

    local tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_print3. Erro passando como parametros strings e numeros ")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_print5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o print, exibe texto passando como           --
--            par�metros string e booleans, e questiona o usu�rio sobre a corretude do teste     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Exibe texto passando como par�metro strings e booleans(5/5)--
function tc6.test_print5()
    display.clear()
    display.print("Teste passando strings e bools. Deve imprimir:",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()
    display.print("(1 = 1) true\n(1 > 2) false",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()

    print("(1 = 1)", true, "\n(1 > 2)", false)


    local tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_print5. Erro passando como parametros strings e booleans")
end

-- ****************** TESTE DE PRINTIMAGE ****************** --

-- Testes para o printimage (1 teste)--
tc7 = TestCase("display.printimage")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_printimage1                                                                   --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o printimage, exibe a imagem referente ao    --
--            arquivo "teste.bmp", e questiona ao usu�rio a corretude do teste                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de impress�o de imagem (1/1)--
function tc7.test_printimage1()
    local imagem, lar, alt = os.loadimage("teste.bmp")

    display.clear()
    display.print("Teste de imagem",0,0)

    display.print("Press any key...",3,0)

    keyboard.getkeystroke(-1)

    display.clear()
    display.printimage(imagem, 0, 0)

    local tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_printimage1. Erro exibindo imagem")
end

-- ****************** TESTE DE FONT ****************** --

tc_font = TestCase("display.font")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Alterna para todas as fontes         --
--            possiveis                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font1()

    local system_fonts = { "small", "normal", "large" }
    local result = TST_SUCCESS
    local def_font = display.font()

    for _,curr_font in ipairs( system_fonts ) do

        result = display.font( curr_font )

        assert_equal( curr_font, result, string.format( "test_font1. Alterando para fonte '%s'. Retorno incorreto", curr_font ), true )

        display.clear()
        display.print( curr_font )

        local pressed_key = lerTecla()
        assert_equal( KEY_ONE, pressed_key, string.format( "test_font1. Erro  Alterando para fonte '%s'", curr_font ), true )
    end

    display.font( def_font )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Seta uma fonte invalida              --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font2()

    local result, error_message, error_code = display.font( "inv" )

    assert_nil( result, "test_font2. Alterando para fonte invalida. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDARG, error_code, "test_font2. Alterando para fonte invalida. Codigo de erro incorreto", true )
    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_font2. Alterando para fonte invalida. Mensagem de erro incorreta" )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_font3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o font, Chama a funcao sem passar parametros --
--            e verifica se ocoreu alguma mudanca de fonte                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_font.test_font3()

    display.clear()
    display.print( "Teste de fonte. A fonte nao deve mudar. Press. tecla" )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.font()
    display.print( string.format( "fonte atual: %s\nfonte continua a mesma?", display.font() ) )

    local pressed_key = lerTecla()
    assert_equal( KEY_ONE, pressed_key, "test_font3. Erro chamando font sem param." )
end

-- ****************** TESTE DE AUTOFLUSH & FLUSH ****************** --

tc_flush_autoflush = TestCase("display.flush & display.autoflush")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_flush_autoflush1                                                              --
--                                                                                               --
--  Descricao:                                                                                   --
--            desliga autoflush, imprime na tela e chama o flush.                                --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_flush_autoflush.test_flush_autoflush1()

    display.clear()
    display.print( "Desligando autoflush. Ao apertar tecla na primeira vez o display eh limpo. Ao apertar a segunda vez o flush eh chamado" )
    local result = display.autoflush( false )

    if( not assert_equal( false, result, "test_flush_autoflush1. Desligando o altoflush. Retorno incorreto" ) ) then
        display.autoflush( true )
        return
    end

    keyboard.getkeystroke( -1 )

    --nao deve fazer nada ate o flush ser chamado
    display.clear()

    keyboard.getkeystroke( -1 )

    display.flush()

    display.autoflush( true )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_flush_autoflush1. Erro Dando o flush")

end

-- ****************** TESTE DE DRAWLINE ****************** --

tc_drawline = TestCase("display.drawline")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha na diagonal.                                                         --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawline.test_drawline1()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha na diagonal. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( 0, 0, largura - 1, altura - 1 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline1. Erro desenhando linha na diagonal")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha na horizontal.                                                       --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawline.test_drawline2()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha na horizontal. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( 0, altura / 2, largura - 1, altura / 2 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline2. Erro desenhando linha na horizontal")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha na vertical.                                                         --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawline.test_drawline3()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha na vertical. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( largura / 2, 0, largura / 2, altura )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline3. Erro desenhando linha na vertical")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline4                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha com largura maior que a tela.                                        --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawline.test_drawline4()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha com largura maior que a tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( 0, altura / 2, largura + 10, altura / 2 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline4. Erro desenhando linha com largura maior que a tela")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline5                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha com altura maior que a tela.                                         --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawline.test_drawline5()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha com altura maior que a tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( largura / 2, 0, largura / 2, altura + 10 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline5. Erro desenhando linha com altura maior que a tela")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline6                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha fora da tela.                                                        --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_drawline.test_drawline6()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha fora da tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    local ret = display.drawline( largura + 10, altura + 10, largura + 20, altura + 20 )

    assert_true( ret, "test_drawline6. Erro desenhando linha fora da tela")

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline6. Erro desenhando linha fora da tela")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawline7                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha linha com ponto 1 maior que o ponto 2.                                     --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_drawline.test_drawline7()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando uma linha com ponto 1 maior que o ponto 2. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawline( largura - 1, altura - 1, 0, 0 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawline7. Erro desenhando linha com ponto 1 maior que o ponto 2")
end

-- ****************** TESTE DE DRAWPOINT ****************** --

tc_drawpoint = TestCase("display.drawline")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawpoint1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha um ponto no centro da tela.                                                --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawpoint.test_drawpoint1()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando um ponto no centro da tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawpoint( largura / 2, altura / 2 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawpoint1. Erro desenhando um ponto no centro da tela")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawpoint2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha um ponto fora da tela.                                                     --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawpoint.test_drawpoint2()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando um ponto fora da tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    local ret = display.drawpoint( largura +10, altura + 10 )

    assert_true( ret, "test_drawpoint2. Erro desenhando um ponto fora da tela")

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawpoint2. Erro desenhando um ponto fora da tela")
end

-- ****************** TESTE DE DRAWRECT ****************** --

tc_drawrect = TestCase("display.drawrect")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawrect1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha um retangulo.                                                              --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawrect.test_drawrect1()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando um retangulo. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawrect( (largura / 2) - 5, (altura / 2) - 5, 10 , 10 )

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawrect1. Erro desenhando um retangulo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_drawrect2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            desenha um retangulo fora da tela.                                                 --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

function tc_drawrect.test_drawrect2()
    local largura, altura = display.pixels()

    display.clear()
    display.print( "Desenhando um retangulo fora da tela. Pressione uma tecla para continuar" )

    keyboard.getkeystroke( -1 )

    display.clear()
    local ret = display.drawrect( largura + 10, altura + 10, 10 , 10 )

    assert_true( ret, "test_drawrect2. Erro desenhando um retangulo fora da tela")

    local tecla = lerTecla()
    assert_equal(KEY_ONE, tecla, "test_drawrect2. Erro desenhando um retangulo fora da tela")
end

-- ****************** TESTE DE INVERTCOLORS ****************** --

tc_invertcolors = TestCase("display.invertcolors")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_invertcolors1                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Testa a fun��o de inverter as cores do display solicitando um feedback do usuario  --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_invertcolors.test_invertcolors1()
    local largura, altura = display.pixels()
    display.clear()

    display.print("Teste de Invertcolors", 0, 0)
    display.print("Invertendo Cores", 1, 0)
    display.print("Pressione uma tecla", 2, 0)

    keyboard.getkeystroke( -1 )

    display.invertcolors( true )
    assert_true( display.invertcolors(), "Falha no retorno do invertcolors", false )

    display.clear()
    display.print("Testando Texto com", 0, 0)
    display.print("Cores Invertidas", 1, 0)
    display.print("Pressione uma tecla", 2, 0)

    keyboard.getkeystroke( -1 )

    display.invertcolors( false )
    assert_false( display.invertcolors(), "Falha no retorno do invertcolors", false )

    display.clear()
    display.print("As cores voltaram ao normal. A seguir sera desenhado um retangulo preto com formas com as cores invertidas dentro", 0, 0)
    display.print("Pressione uma tecla", 5, 0)

    keyboard.getkeystroke( -1 )

    display.clear()
    display.drawrect( 0, 0, largura - 1, altura - 1 )

    display.invertcolors( true )
    assert_true( display.invertcolors(), "Falha no retorno do invertcolors", false )

    display.drawrect( 10, 10, 20, 10 );
    display.drawline( 10, 40, 100, 40 );
    display.drawline( 10, 50, 100, 50 );
    display.drawline( 10, 60, 100, 60 );

    keyboard.getkeystroke( -1 )

    display.invertcolors( false )
    assert_false( display.invertcolors(), "Falha no retorno do invertcolors", false )

    display.clear()
    display.print("Resultado do Teste:", 0, 0)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de display.invertcolors")
end

-- ****************** TESTE DE POSI��O VERTICAL DOS CARACTERES ****************** --

tc_vpos = TestCase("teste de posicao vertical")

-- Alturas das fontes.
local fontHeights = {}
fontHeights[      "small" ] = 8
fontHeights[     "normal" ] = 10
fontHeights[      "large" ] = 16
fontHeights[ "large_bold" ] = 16

-- Desenha algumas linhas na tela para mostrar se os caracteres s�o desenhados
-- onde deveriam.
local function desenhar_linhas( fonte )
    local widthPixels, heightPixels = display.pixels( )
    local heightChar = fontHeights[ fonte ]
    local numLinhas = heightPixels / heightChar
    for linha = 1, numLinhas do
        local y = linha * heightChar
        display.drawline( 0, y, widthPixels - 1, y )
    end
end

-- Imprime linhas horizontais por cima de uma string de teste.
local function imprimir_teste( fonte )
    local stringTeste = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ..
                        "abcdefghijklmnopqrstuvwxyz" ..
                        "!@#$%&*()[]{};:,.<>/?" ..
                        "0123456789"
    display.clear( )
    local old_font = display.font( )
    display.font( fonte )
    display.print( stringTeste, 0, 0 )
    desenhar_linhas( fonte )
    display.font( old_font )
    keyboard.getkeystroke( -1 )
end

-- Fun��o para testar o posicionamento vertical do texto impresso no display.
local function teste_vpos( fonte )
    local texto_msg = "verifique se as linhas cortam o texto impresso na fonte " .. fonte .. "."
    ui.message( "pos vertical", texto_msg )
    keyboard.getkeystroke( -1 )

    imprimir_teste( fonte )

    display.clear( )
    display.print( "O texto deve ter sido.", 0 )
    display.print( "impresso entre as",      1 )
    display.print( "linhas.",                2 )
    local tecla = lerTecla( )
    assert_equal( KEY_ONE, tecla, "Erro no teste de pos vertical com fonte " .. fonte )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_small                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Testa o posicionamento vertical do texto impresso no display com a fonte small.    --
--  Argumentos:                                                                                  --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_vpos.test_small( )
    teste_vpos( "small" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_normal                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Testa o posicionamento vertical do texto impresso no display com a fonte normal.   --
--  Argumentos:                                                                                  --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_vpos.test_normal( )
    teste_vpos( "normal" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_large                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Testa o posicionamento vertical do texto impresso no display com a fonte large.    --
--  Argumentos:                                                                                  --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_vpos.test_large( )
    teste_vpos( "large" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_large_bold                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Testa o posicionamento vertical do texto impresso no display com a fonte           --
--            large_bold.                                                                        --
--  Argumentos:                                                                                  --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_vpos.test_large_bold( )
    teste_vpos( "large_bold" )
end

lunit.run()
