---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios do os                                       --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         08/02/2006  Cria��o                                          --
-- Jamerson Lima        jrfl        13/04/2006  Inclusao dos teste de os.isdir, os.isglobal,     --
--                                              os.exists e os.filesize                          --
-- Jamerson Lima        jrfl        17/04/2006  Correcao de erros                                --
-- Jamerson Lima        jrfl        08/06/2006  Rework(HFLEX-LUA-TSTR-CODE-INSP006)              --
-- Guilherme            gkmo        15/01/2007  Adicionando novos testes para os.update          --
-- Guilherme            gkmo        25/01/2007  Corrigindo bug em os.update1                     --
-- Guilherme            gkmo        13/04/2007  Atualizando teste de profundidade m�xima         --
--                                              do caminho (23398)                               --
-- Henrique Ferreira    hsf         13/04/2007  CR23999: corrigindo testes de readdir            --
---------------------------------------------------------------------------------------------------

--Arquivos que devem estar na pasta de testes de os no POS:
-- teste.bmp
-- dir1/dir2/dir3/dir4
-- rename3.txt
-- rename4.txt
-- rename5.txt
-- rename6.txt
-- rename8
-- rename9/dir1
-- rename9/dir2
-- rename9/file.txt
-- remove3.txt
-- remove4.txt
-- remove5.txt
-- remove6.txt
-- remove7
-- remove8/remove8.txt
-- isdir2.txt
-- isglob1.txt
-- isglob2.txt
-- exists2.txt
-- filesiz1.txt
-- filesiz2.txt
-- filesiz3.txt
-- filesiz4.txt
-- filemt1.txt
-- filemt2.txt
-- filemt3
-- readdir2/dir1
-- readdir2/dir2
-- readdir2/file.txt
-- readdir3/dir1
-- readdir3/dir2
-- readdir3/file.txt
-- tmp0.tmp
-- tmp1.tmp

---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

-- ****************** TESTE DE DIFFTIME ****************** --

-- Testes para o difftime (3 teste)--
tc3 = TestCase("os.difftime")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_difftime1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.difftime, testando com 't2' maior que   --
--            't1'                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do difftime (1/3)--
function tc3.test_difftime1( )
    local ret = os.difftime(1,0)

    assert_equal(1, ret, "test_difftime1. Erro no difftime passando t2 maior que t1")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_difftime2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.difftime, testando com 't2' menor que   --
--            't1'                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do difftime (2/3)--
function tc3.test_difftime2( )
    local ret = os.difftime(0,1)

    assert_equal(-1, ret, "test_difftime2. Erro no difftime passando t2 menor que t1")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_difftime3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.difftime, testando com 't2' igual a     --
--            't1'                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do difftime (3/3)--
function tc3.test_difftime3()
    local ret = os.difftime(1,1)

    assert_equal(0, ret, "test_difftime3. Erro no difftime passando t2 igual a t1")
end


-- ****************** TESTE DE FREEDISKSPACE ****************** --

-- Testes para o freediskspace (2 teste)--
tc5 = TestCase("os.freediskspace")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_freediskspace1                                                                --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.freediskspace, verificando o espa�o     --
--            livre e comparando com > 0                                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do freediskspace (1/2)--
function tc5.test_freediskspace1()
    local ret = os.freediskspace()

    assert_true(ret > 0, "test_freediskspace1. Caso de sucesso")
end

-- ****************** TESTE DE LOADIMAGE ****************** --

-- Testes para o loadimage (3 teste)--
tc7 = TestCase("os.loadimage")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_loadimage2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.loadimage, passando como par�metro      --
--            o nome de um arquivo inexistente                                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do loadimage (2/3)--
function tc7.test_loadimage2()

    local result = true
    local error_message = ""
    local error_code = 0

    result, error_message, error_code = os.loadimage("lalalala")
    
    assert_nil( result, "test_loadimage2. Passando arquivo invalido. Retorno incorreto", true )
    
    assert_equal( TST_ERR_NOTFOUND, error_code, "test_loadimage2. Passando arquivo invalido. Codigo de erro incorreto", true )
    
    assert_equal( lua_error_message[ TST_ERR_NOTFOUND ], error_message, "test_loadimage2. Passando arquivo invalido. Mensagem de erro incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_loadimage3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.loadimage, passando como parametro      --
--            o nome de um arquivo existente                                                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do loadimage (3/3)--
function tc7.test_loadimage3()

    --teste de dependencia: verifica se a imagem para teste existe
    assert_true( os.exists( "teste.bmp" ), "Dependencia: imagem 'teste.bmp' nao encontrado", true )

    local ret  = os.loadimage( "teste.bmp" )

    local img_width  = ret:width( )
    local img_height = ret:height( )

    assert_not_nil(ret, "test_loadimage3. Caso de sucesso. Retorno incorreto", true )

    os.unloadimage( ret )
end

-- ****************** TESTE DE UNLOADIMAGE ****************** --

-- Testes para o unloadimage (2 teste)--
tc_unloadimage = TestCase("os.unloadimage")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_unloadimage1                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.unloadimage, passando como parametro    --
--            o nome de um arquivo existente                                                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do unloadimage (1/2)--
function tc_unloadimage.test_unloadimage1( )
    --teste de dependencia: verifica se a imagem para teste existe
    assert_true( os.exists( "teste.bmp" ), "Dependencia: imagem 'teste.bmp' nao encontrado", true )

    local img = os.loadimage( "teste.bmp" )

    assert_not_nil( img, "Dependencia: imagem 'teste.bmp' nao foi carregada" , true )

    local result = os.unloadimage( img )

    assert_true( result, "test_unloadimage1. Caso de sucesso. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_unloadimage2                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.unloadimage, passando como parametro    --
--            um handle ja fechado                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do unloadimage (2/2)--
function tc_unloadimage.test_unloadimage2( )

    local result = true
    local error_message = ""
    local error_code = 0

    --teste de dependencia: verifica se a imagem para teste existe
    assert_true( os.exists( "teste.bmp" ), "Dependencia: imagem 'teste.bmp' nao encontrado", true )

    local img = os.loadimage( "teste.bmp" )

    assert_not_nil( img, "Dependencia: imagem 'teste.bmp' nao foi carregada" , true )

    result = os.unloadimage( img )

    assert_true( result, "Dependencia: imagem 'teste.bmp' nao foi fechada", true )

    result, error_message, error_code = os.unloadimage( img )

    assert_nil( result, "test_unloadimage2. Fechando handle fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG , error_code, "test_unloadimage2. Fechando handle fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_unloadimage2. Fechando handle fechado. Mensagem de erro incorreto" )
end

-- ****************** TESTE DE TIME ****************** --

-- Testes para o time (4 teste)--
tc11 = TestCase("os.time")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_time2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.time, testando a fun��o sem par�metros  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do time (2/4)--
function tc11.test_time2()
    local ret = os.time()

    assert_true( ret > 0, "test_time2. Retorno incorreto no teste sem parametros" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_time3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.time, passando uma data v�lida como     --
--            par�metro                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do time (3/4)--
function tc11.test_time3()
    local data = {}
    data.year = 1970
    data.month = 01
    data.day = 01
    data.hour = 0
    data.min = 0
    data.sec = 0

    local ret = os.time(data)

    assert_equal(0, ret, "test_time3. Retorno incorreto no teste da data: 01/01/1970")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_time4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.time, passando uma data v�lida como     --
--            par�metro                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do time (4/4)--
function tc11.test_time4()
    local data = {}
    data.year = 2006
    data.month = 02
    data.day = 18
    data.hour = 20
    data.min = 06
    data.sec = 10

    local ret = os.time(data)

    assert_equal(1140293170, ret, "test_time4. Retorno incorreto no teste da data: 18/02/2006 20:06:10")
end

-- ****************** TESTE DE MKDIR ****************** --

-- Testes para o mkdir (6 teste)--
tc8 = TestCase("os.mkdir")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mkdir2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.mkdir, criando diret�rio e verificando  --
--            status da opera��o                                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mkdir (2/6)--
function tc8.test_mkdir2()

    assert_equal( "/", os.getcwd( ), "Dependencia: diretorio nao eh raiz", true )

    if( os.exists( "mkdir2" ) ) then
        local ret = os.remove( "mkdir2" )
        assert_true( ret, "Dependencia: diretorio 'mkdir2' nao pode ser apagado", true )
    end

    ret = os.mkdir( "mkdir2" )

    assert_true( os.exists( "mkdir2" ), "test_mkdir2. 'mkdir2' nao foi criado", true )

    assert_true(ret, "test_mkdir2. Retorno incorreto criando diretorio 'mkdir2'" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mkdir3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.mkdir, criando diret�rio com o mesmo    --
--            nome e verificando status da operacao                                              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mkdir (3/6)--
function tc8.test_mkdir3()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_equal( "/", os.getcwd( ), "Dependencia: diretorio nao eh raiz", true )

    assert_true( os.exists( "mkdir3" ), "Dependencia: diretorio 'mkdir3' nao existe", true )

    result, error_message, error_code = os.mkdir( "mkdir3" )

    assert_nil( result, "test_mkdir3. Retorno incorreto criando diretorio com o mesmo nome", true )

    assert_equal( TST_ERR_ALREADYEXISTS,  error_code, "test_mkdir3. Codigo de erro incorreto criando diretorio com o mesmo nome", true )

    assert_equal( lua_error_message[ TST_ERR_ALREADYEXISTS ], error_message, "test_mkdir3. Mensagem de erro incorreta criando diretorio com o mesmo nome" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mkdir4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.mkdir, criando diret�rio com nome       --
--            inv�lido                                                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mkdir (4/6)--
function tc8.test_mkdir4()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_equal( "/", os.getcwd( ), "Dependencia: diretorio nao eh raiz", true )

    result, error_message, error_code = os.mkdir(" ")

    assert_nil( result, "test_mkdir4. Retorno incorreto criando diretorio com nome vazio" )

    assert_equal( TST_ERR_PATHERR,  error_code, "test_mkdir4. Codigo de erro incorreto criando diretorio com nome vazio", true )

    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message, "test_mkdir4. Mensagem de erro incorreta criando diretorio com nome vazio", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mkdir5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.mkdir, criando diret�rio com nome       --
--            inv�lido                                                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mkdir (5/6)--
function tc8.test_mkdir5()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_equal( "/", os.getcwd( ), "Dependencia: diretorio nao eh raiz", true )

    result, error_message, error_code = os.mkdir("123_lala")

    assert_nil( result, "test_mkdir5. Retorno incorreto criando diretorio com nome invalido" )

    assert_equal( TST_ERR_PATHERR, error_code, "test_mkdir5. Codigo de erro incorreto criando diretorio com nome invalido", true )

    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message, "test_mkdir5. Mensagem de erro incorreta criando diretorio com nome invalido", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mkdir6                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.mkdir, testando o n�mero m�ximo de      --
--            subdiret�rios                                                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mkdir (6/6)--
function tc8.test_mkdir6()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_equal( "/", os.getcwd( ), "Dependencia: diretorio nao eh raiz", true )

    result, error_message, error_code = os.chdir( "/dir1/dir2/dir3" )

    if( not assert_true( result, string.format( "Dependencia:  Nao foi possivel descer para /dir1/dir2/dir3 [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        --em caso de erro, volta para a raiz
        os.chdir( "/" )
        return
    end

    result, error_message, error_code = os.mkdir( "mkdir6" )
    
    os.chdir( "/" )

    assert_nil( result, "test_mkdir6. Retorno incorreto criando diretorio excedendo o numero maximo de subdiretorios", true )

    assert_equal( TST_ERR_PATHTOODEEP, error_code, "test_mkdir6. Codigo de erro incorreto criando diretorio excedendo o numero maximo de subdiretorios", true )

    assert_equal( lua_error_message[ TST_ERR_PATHTOODEEP ], error_message, "test_mkdir6. Mensagem de erro incorreta criando diretorio excedendo o numero maximo de subdiretorios" )
end



-- ****************** TESTE DE RENAME ****************** --

-- Testes para o rename (8 teste)--
tc11 = TestCase("os.rename")

function tc11:setup()
    os.chdir( "/" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo inexistente  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (2/9)--
function tc11.test_rename2()

    local result = true
    local error_message = ""
    local error_code = 0

    result, error_message, error_code = os.rename("atchim", "espirro")

    assert_nil( result, "test_rename2. Retorno incorreto renomeando arquivo inexistente" ,true)

    assert_equal( TST_ERR_NOTFOUND, error_code, "test_rename2. Codigo de erro incorreto renomeando arquivo inexistente", true )

    assert_equal( lua_error_message[ TST_ERR_NOTFOUND ], error_message, "test_rename2. Mensagem de erro incorreta renomeando arquivo inexistente" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo local        --
--            'teste.txt' para 'teste.pdf'                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (3/9)--
function tc11.test_rename3()

    assert_true( os.exists( "rename3.txt" ), "Dependencia: 'rename3.txt' nao encontrado", true )
    assert_false( os.exists( "rename3.pdf" ), "Dependencia: 'rename3.pdf' nao deve existir", true )

    local result = os.rename( "rename3.txt", "rename3.pdf" )

    result = assert_true( result, "test_rename3. renomeando arquivo local 'rename3.txt'" )

    assert_false( os.exists( "rename3.txt" ), string.format( "test_rename3. arquivo local 'rename3.txt' nao foi apagado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    assert_true( os.exists( "rename3.pdf" ), string.format( "test_rename3. arquivo local 'rename3.pdf' nao foi criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    os.rename( "rename3.pdf", "rename3.txt" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename4                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo existente    --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (4/9)--
function tc11.test_rename4()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "rename4.txt" ), "Dependencia: Arquivo 'rename4.txt' nao encontrado", true )

    result, error_message, error_code = os.rename("rename4.txt", "_teste")

    assert_nil( result, "test_rename4. Retorno incorreto renomeando arquivo para '_teste'", true)

    assert_equal( TST_ERR_PATHERR, error_code, "test_rename4. Codigo de erro incorreto renomeando arquivo para '_teste'", true )

    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message, "test_rename4. Mensagem de erro incorreta renomeando arquivo para '_teste'", true )

    assert_true( os.exists( "rename4.txt" ), "test_rename4. arquivo apagado erroneamente renomeando arquivo para '_teste'" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename5                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo existente    --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (5/9)--
function tc11.test_rename5()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "rename5.txt" ), "Dependencia: Arquivo 'rename5.txt' nao encontrado", true )

    result, error_message, error_code = os.rename("rename5.txt", "123teste")

    assert_nil( result, "test_rename5. Retorno incorreto renomeando arquivo para '123teste'", true)

    assert_equal( TST_ERR_PATHERR, error_code, "test_rename5. Codigo de erro incorreto renomeando arquivo para '123teste'", true )

    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message, "test_rename5. Mensagem de erro incorreta renomeando arquivo para '123teste'", true )

    assert_true( os.exists( "rename5.txt" ), "test_rename5. arquivo apagado erroneamente renomeando arquivo para '123teste'" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename6                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo existente    --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (6/9)--
function tc11.test_rename6()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "rename6.txt" ), "Dependencia: Arquivo 'rename6.txt' nao encontrado", true )

    result, error_message, error_code = os.rename("rename6.txt", "teste.***")

    assert_nil( result, "test_rename6. Retorno incorreto renomeando arquivo para 'teste.***'", true)

    assert_equal( TST_ERR_PATHERR, error_code, "test_rename6. Codigo de erro incorreto renomeando arquivo para 'teste.***'", true )

    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message, "test_rename6. Mensagem de erro incorreta renomeando arquivo para 'teste.***'", true )

    assert_true( os.exists( "rename6.txt" ), "test_rename6. arquivo apagado erroneamente renomeando arquivo para 'teste.***'" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename7                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo global       --
--            'teste.txt' para 'teste.pdf'                                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (7/9)--
function tc11.test_rename7()

    if( not os.exists( "rename7.txt", true ) ) then
        local file = io.open( "rename7.txt", "rwg" )

        assert_not_nil( file, "Dependencia: rename7.txt nao pode ser criado", true )

        file:close( )
    end

    local result = os.rename("rename7.txt", "rename7.pdf", true )

    assert_true( result, "test_rename7. Retorno incorreto renomeando arquivo global", true )

    assert_false( os.exists( "rename7.txt", true ), "test_rename7. arquivo local 'rename7.txt' nao foi apagado", true ) 

    assert_true( os.exists( "rename7.pdf", true ), "test_rename7. arquivo local 'rename7.pdf' nao foi criado" )

    os.remove( "rename7.txt", true )
    os.remove( "rename7.pdf", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename8                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando um diret�rio         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (8/9)--
function tc11.test_rename8()

    assert_true( os.exists( "rename8" ), "Dependencia: Dir. 'rename8' nao encontrado", true )

    local result = os.rename("rename8", "renamed8")

    assert_true( result, "test_rename8. Retorno incorreto renomeando diretorio", true )

    assert_false( os.exists( "rename8" ), "test_rename8. dir. 'rename8' nao foi apagado", true )
    assert_true( os.exists( "renamed8" ), "test_rename8. dir. 'renamed8' nao foi criado" )

    os.rename("renamed8", "rename8")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename9                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando um diret�rio         --
--            com dados dentro                                                                   --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (9/9)--
function tc11.test_rename9()

    assert_true( os.exists( "rename9" ), "Dependencia: Dir. 'rename9' nao encontrado", true )

    local result = os.rename("rename9", "renamed9")

    assert_true( result, "test_rename9. Retorno incorreto renomeando diretorio", true )

    assert_false( os.exists( "rename9" ), "test_rename9. dir. 'rename9' nao foi apagado", true )
    assert_true( os.exists( "renamed9" ), "test_rename9. dir. 'renamed9' nao foi criado" )
    
    os.rename("renamed9", "rename9")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_rename10                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.rename, renomeando arquivo existente    --
--            com um nome inv�lido                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do rename (10/10)--
function tc11.test_rename10()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "rename10.txt" ), "Dependencia: Arquivo 'rename10.txt' nao encontrado", true )

    result, error_message, error_code = os.rename("rename10.txt", "aaaaaaaaaaaaaaaaaaaa")

    assert_nil( result, "test_rename10. Retorno incorreto no teste de maxlength", true)

    assert_equal( TST_ERR_MAXLEN, error_code, "test_rename10. Codigo de erro incorreto no teste de maxlength", true )

    assert_equal( lua_error_message[ TST_ERR_MAXLEN ], error_message, "test_rename10. Mensagem de erro incorreta no teste de maxlength", true )

    assert_true( os.exists( "rename10.txt" ), "test_rename10. arquivo apagado erroneamente no teste de maxlength" )
end


-- ****************** TESTE DE REMOVE ****************** --

-- Testes para o remove (8 teste)--
tc10 = TestCase("os.remove")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um aqrquivo           --
--            inexistente                                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (2/8)--
function tc10.test_remove2()

    local result = true
    local error_message = ""
    local error_code = 0

    result, error_message, error_code = os.remove("lalala.bla")

    assert_nil( result, "test_remove2. Retorno incorreto removendo arquivo inexistente", true )

    assert_equal( TST_ERR_NOTFOUND, error_code, "test_remove2. Erro removendo arquivo inexistente", true )

    assert_equal( lua_error_message[ TST_ERR_NOTFOUND ], error_message, "test_remove2. Erro removendo arquivo inexistente" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um arquivo local      --
--            v�lido                                                                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (3/8)--
function tc10.test_remove3()

    assert_true( os.exists( "remove3.txt" ), "Dependencia: 'remove3.txt' nao encontrado", true )

    local result = os.remove( "remove3.txt" )

    assert_true( result, "test_remove3. Retorno incorreto removendo arquivo local", true )

    assert_false( os.exists( "remove3.txt" ), "test_remove3. Removendo arquivo local. 'remove3.txt' nao apagado", true )

    --recria o arquivo
    local handle = io.open( "remove3.txt", "rw" )
    if( handle ~= nil ) then
        handle:close()
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove4                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um arquivo global     --
--            v�lido                                                                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (4/8)--
function tc10.test_remove4()

    if( not os.exists( "remove4.txt", true ) ) then
        local file = io.open( "remove4.txt", "rwg" )

        assert_not_nil( file, "Dependencia: remove4.txt nao pode ser criado", true )

        file:close( )
    end

    local result = os.remove( "remove4.txt", true )

    assert_true( result, "test_remove4. Retorno incorreto removendo arquivo global", true )

    assert_false( os.exists( "remove4.txt", true ), "test_remove4. removendo arquivo global. 'remove4.txt' nao apagado" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove5                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um arquivo local      --
--            aberto                                                                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (5/8)--
function tc10.test_remove5()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "remove5.txt" ), "Dependencia: 'remove5.txt' nao encontrado", true )

    local file = io.open( "remove5.txt", "rw" )

    assert_not_nil( file, "Dependencia: 'remove5.txt' nao pode ser aberto", true )

    result, error_message, error_code = os.remove( "remove5.txt" )

    file:close( )

    assert_nil( result, "test_remove5. Removendo arquivo local aberto. Retorno incorreto", true )

    assert_equal( TST_ERR_ISOPEN, error_code, "test_remove5. Removendo arquivo local aberto. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_ISOPEN ], error_message, "test_remove5. Removendo arquivo local aberto. Mensagem incorreta", true )

    assert_true( os.exists( "remove5.txt" ), "test_remove5. 'remove5.txt' foi removido" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove7                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um diret�rio vazio    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (7/8)--
function tc10.test_remove7()

    assert_true( os.exists( "remove7" ), "Dependencia: Dir. 'remove7' nao encontrado", true )

    local result = os.remove( "remove7" )

    assert_true( result, "test_remove7. Retorno incorreto removendo diretorio vazio" )

    assert_false( os.exists( "remove7" ), "test_remove7. Removendo diretorio vazio. Diretorio nao removido", true )
    
    --recria o dir.
    os.mkdir( "remove7" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_remove8                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.remove, removendo um diret�rio com      --
--            arquivos                                                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do remove (8/8)--
function tc10.test_remove8()

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "remove8" ), "Dependencia: Dir. 'remove8' nao encontrado", true )
    assert_true( os.exists( "remove8/remove8.txt" ), "Dependencia: 'remove8/remove8.txt' nao encontrado", true )

    result, error_message, error_code = os.remove( "remove8" )

    assert_nil( result, "test_remove8. Retorno incorreto removendo diretorio nao vazio", true )

    assert_equal( TST_ERR_NOTEMPTY, error_code, "test_remove8. Codigo de erro incorreto removendo diretorio nao vazio", true )

    assert_equal( lua_error_message[ TST_ERR_NOTEMPTY ], error_message, "test_remove8. Mensagem incorreta removendo diretorio nao vazio" )
end



-- ****************** TESTE DE READDIR ****************** --

-- Testes para o readdir (3 teste)--
tc9 = TestCase("os.readdir")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_readdir2                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.readdir, verificando se a fun��o lista  --
--            arquivos e diret�rios no diret�rio corrente                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do readdir (2/3)--
function tc9.test_readdir2()

    assert_true( os.chdir( "readdir2" ), "Dependencia: Nao foi possivel descer para o dir. 'readdir2'" )

    local result = os.readdir( )


    local i = 1
    local mark = nil
    while result[i] do
      if(result[i] == "dir1" ) then mark = true   end
      i = i + 1
    end

    if( assert_true( mark, "test_readdir2. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end
    
    i = 1
    mark = nil
    while result[i] do
      if(result[i] == "dir2" ) then  mark = true end
      i = i + 1
    end
    if( assert_true( mark, "test_readdir2. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end
    
    i = 1
    mark = nil
    while result[i] do
      if(result[i] == "file.txt" ) then mark = true    end
      i = i + 1
    end
    if( assert_true( mark, "test_readdir2. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end

    os.chdir( "/" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_readdir3                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.readdir, verificando se a fun��o lista  --
--            arquivos e diret�rios no diret�rio corrente                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do readdir (3/3)--
function tc9.test_readdir3()

    assert_true( os.exists( "readdir3" ), "Dependencia: Dir. 'readdir3' nao encontrado" )

    local result = os.readdir( "/readdir3" )

    local i = 1
    local mark = nil
    while result[i] do
      if(result[i] == "dir1" ) then mark = true  end
      i = i + 1
    end
    if( assert_true( mark, "test_readdir3. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end
    
    i = 1
    mark = nil
    while result[i] do
      if(result[i] == "dir2" ) then mark = true  end
      i = i + 1
    end
    if( assert_true( mark, "test_readdir3. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end
    
    i = 1
    mark = nil
    while result[i] do
      if(result[i] == "file.txt" ) then mark = true   end
      i = i + 1
    end
    if( assert_true( mark, "test_readdir3. Lendo diretorio sem passar parametro. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end

    os.chdir( "/" )
end


-- ****************** TESTE DE GETCWD ****************** --

-- Testes para o getcwd (2 teste)--
tc6 = TestCase("os.getcwd")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getcwd1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.getcwd, verificando o retorno da fun��o --
--            no caso de estar na raiz                                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do getcwd (1/2)--
function tc6.test_getcwd1( )

    local result = os.getcwd( )

    assert_equal( "/", result, "test_getcwd1. Retorno incorreto pegando path da raiz")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getcwd2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.getcwd, pegando path de um sub-         --
--            diretorio.                                                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do getcwd (2/2)--
function tc6.test_getcwd2()

    local result = os.chdir( "dir1" )

    assert_true( result, "Dependencia: nao foi possivel ir para /dir1", true )

    result = os.getcwd( )
    
    os.chdir( "/" )

    assert_equal( "/dir1/", result, "test_getcwd2. Retorno incorreto pegando path de um subdir.")
end


-- ****************** TESTE DE DATE ****************** --

-- Testes para o date (3 teste)--
tc2 = TestCase("os.date")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_date1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.date, pegando a data atual e verificando--
--            se o retorno � uma string                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do date (1/3)--
function tc2.test_date1( )

    local result = os.date( )

    assert_true( is_string(result), "test_date1. Retorno da data atual nao eh string", true )
	
	tecla = ui.show_message('test_date1','executando')
	result = ui.show_message('test_date1',('Data Atual:\n' .. tostring( result )), TIMEOUT_UI)

    assert_equal( KEY_ONE, result, "test_date1. Falha no retorno da data atual em string", true )
	
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_date2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.date, pegando a data atual formatada    --
--            por '*t' e verificando se o retorno � uma tabela com os valores corretos           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do date (2/3)--
function tc2.test_date2( )

    test_time = { }

    test_time.year = 2006
    test_time.month = 12
    test_time.day = 31
    test_time.hour = 11
    test_time.min = 59
    test_time.sec = 59

    local result = os.date( "*t", os.time( test_time ) )

    assert_equal(2006, result.year, "test_date2. Erro: ano invalido passando parametro '*t'", true )
    assert_equal(12, result.month, "test_date2. Erro: mes invalido passando parametro '*t'", true )
    assert_equal(31, result.day, "test_date2. Erro: dia invalido passando parametro '*t'", true )
    assert_equal(11, result.hour, "test_date2. Erro: hora invalida passando parametro '*t'", true )
    assert_equal(59, result.min, "test_date2. Erro: minutos invalidos passando parametro '*t'", true )
    assert_equal(59, result.sec, "test_date2. Erro: segundos invalidos passando parametro '*t'" )

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_date3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.date, formatando a data referente a '0' --
--            segundos.                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do date (3/3)--
function tc2.test_date3( )
    local result = os.date( "*t", 0 )

    assert_equal( 1970, result.year, "test_date3. Erro: ano invalido passando hora 0", true )
    assert_equal( 1, result.month, "test_date3. Erro: mes invalido passando hora 0", true )
    assert_equal( 1, result.day, "test_date3. Erro: dia invalido passando hora 0", true )
    assert_equal( 0, result.hour, "test_date3. Erro: hora invalida passando hora 0", true )
    assert_equal( 0, result.min, "test_date3. Erro: minutos invalidos passando hora 0", true )
    assert_equal( 0, result.sec, "test_date3. Erro: segundos invalidos passando hora 0" )
end

-- ****************** TESTE DE CHDIR ****************** --

-- Testes para o chdir (5 teste)--
tc = TestCase("os.chdir")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_chdir1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.chdir, mudando o diret�rio corrente     --
--            para o diret�rio criado                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do chdir (1/5)--
function tc.test_chdir1()

    assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz", true )

    local result = os.chdir( "dir1" )

    assert_true( result, "test_chdir1. Tentando mudar o diretorio corrente para 'dir1'. Retorno incorreto", true )

    assert_equal( "/dir1/", os.getcwd( ), "test_chdir1. Tentando mudar o diretorio corrente para 'dir1'. Diretorio atual incorreto", true )

    os.chdir( "/" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_chdir2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.chdir, mudando o diret�rio corrente     --
--            utilizando '..'                                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do chdir (2/5)--
function tc.test_chdir2()

    assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz", true )

    assert_true( os.chdir( "dir1" ), "Dependencia: Nao foi possivel mudar para /dir1/", true )

    assert_equal( "/dir1/", os.getcwd( ), "Dependencia: Diretorio atual nao eh /dir1/", true )

    if( not assert_true( os.chdir( "dir2" ), "Dependencia: Nao foi possivel mudar para /dir1/dir2", true ) ) then
        os.chdir( "/" )
        return
    end

    if( not assert_equal( "/dir1/dir2/", os.getcwd( ), "Dependencia: Diretorio atual nao eh /dir1/di2", true ) ) then
        os.chdir( "/" )
        return
    end

    if( not assert_true( os.chdir( ".." ), "test_chdir2. Subindo para dir1 com '..'. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end

    if( not assert_equal( "/dir1/", os.getcwd( ), "test_chdir2. Subindo para dir1 com '..'. Diretorio atual incorreto", true ) ) then
        os.dir( "/" )
        return
    end

    if( not assert_true( os.chdir( ".." ), "test_chdir2. Subindo para '/' com '..'. Retorno incorreto" ) ) then
        os.chdir( "/" )
        return
    end

    if( not assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz" ) ) then
        os.chdir( "/" )
        return
    end

    os.chdir( "/" )

    local result, error_message, error_code = os.chdir( ".." )

    assert_nil( result, "test_chdir2. Subindo com '..' estando na raiz. Retorno incorreto", true )
    assert_equal( TST_ERR_PATHERR, error_code, "test_chdir2. Subindo com '..' estando na raiz. Codigo de erro incorreto", true )
    assert_equal( lua_error_message[ TST_ERR_PATHERR ], error_message,  "test_chdir2. Subindo com '..' estando na raiz. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_chdir3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.chdir, mudando o diret�rio corrente     --
--            utilizando '/'                                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do chdir (3/5)--
function tc.test_chdir3()

    assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz", true )

    assert_true( os.chdir( "dir1" ), "Dependencia: Nao foi possivel mudar para dir. 'dir1'", true )

    assert_equal( "/dir1/", os.getcwd( ), "Dependencia: Nao foi possivel mudar para dir. 'dir1'", true )

    local result = os.chdir( "/" )

    if( not assert_true(result, "test_chdir3. Mudando o diretorio atraves do '/'. Retorno incorrreto" ) ) then
        os.chdir( "/" )
        return
    end

    assert_equal( "/", os.getcwd( ), "test_chdir3. Mudando o diretorio atraves do '/'. Diretorio atual incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_chdir4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.chdir, mudando o diret�rio corrente     --
--            utilizando um dirpath que come�a com '/'                                           --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do chdir (4/5)--
function tc.test_chdir4()

    assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz", true )

    local result = os.chdir( "/dir1/dir2" )

    assert_true( result, "test_chdir4. Retorno incorreto mudando o diretorio atraves do caminho completo", true )

    assert_equal( "/dir1/dir2/", os.getcwd( ), "test_chdir4. Diretorio atual incorreto mudando o diretorio atraves do caminho completo" )

    os.chdir( "/" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_chdir5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.chdir, Passando '.'                     --
--                                                                                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do chdir (5/5)--
function tc.test_chdir5()

    local current_path = os.getcwd( )

    local result = os.chdir( "." )

    assert_true( result, "test_chdir5. Passando '.'. Retorno incorreto", true )

    assert_equal( current_path, os.getcwd( ), "test_chdir5. Passando '.'. Diretorio atual incorreto", true )
end

-- ****************** TESTE DE ISDIR ****************** --

-- Testes para o isdir (5 teste)--
tc_isdir = TestCase("os.isdir")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isdir1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isdir.                                  --
--            Passando o caminho de um diretorio                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isdir (1/4)--
function tc_isdir.test_isdir1( )

    assert_equal( "/", os.getcwd( ), "Dependencia: Diretorio atual nao eh raiz", true )

    --chama a funcao testada
    local result = os.isdir( "dir1" )

    --testa o retorno da funcao
    assert_true( result, "test_isdir1. Passando diretorio. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isdir2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isdir.                                  --
--            Passando o caminho de um arquivo                                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isdir (2/4)--
function tc_isdir.test_isdir2( )

    --chama a funcao testada
    local result = os.isdir( "isdir2.txt" )


    --testa o retorno da funcao
    assert_false( result, "test_isdir2. Passando arquivo. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isdir3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isdir.                                  --
--            Passando um caminho de um diretorio inexistente                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isdir (3/4)--
function tc_isdir.test_isdir3( )

    --chama a funcao testada
    local result = os.isdir( "inexistente" )

    --testa o retorno da funcao
    assert_false( result, "test_isdir3. Passando diretorio inexistente. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isdir4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isdir.                                  --
--            Passando uma string vazia                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isdir (4/4)--
function tc_isdir.test_isdir4( )

    --chama a funcao testada
    local result = os.isdir( "" )

    --testa o retorno da funcao
    assert_false( result, "test_isdir4. Passando string vazia. Retorno incorreto" )
end

-- ****************** TESTE DE ISGLOBAL ****************** --

-- Testes para o isglobal (5 teste)--
tc_isglobal = TestCase("os.isglobal")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isglobal1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isglobal.                               --
--            Passando o caminho de um arquivo global                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isglobal (1/5)--
function tc_isglobal.test_isglobal1( )

    local result = true
    local handle = nil
    local error_message = ""
    local error_code = 0

    if( not os.exists( "isglob1.txt", true ) ) then
        --cria arquivo para teste
        handle, error_message, error_code = io.open( "isglob1.txt", "rwg" )

        --caso o arquivo nao exista e nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
        assert_not_nil( handle, string.format( "Dependencia: 'isglob1.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

        --fecha o handle do arquivo aberto
        handle:close()
    end

    --chama a funcao testada
    local result = os.isglobal( "isglob1.txt" )

    --testa o retorno da funcao
    assert_true( result, "test_isglobal1. Passando arquivo global. Retorno incorreto" )

    --remove o arquivo para teste
    os.remove( "isglob1.txt", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isglobal2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isglobal.                               --
--            Passando o caminho de um arquivo normal                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isglobal (2/5)--
function tc_isglobal.test_isglobal2( )

    --chama a funcao testada
    local result = os.isglobal( "isglob2.txt" )

    --testa o retorno da funcao
    assert_false( result, "test_isglobal2. Passando arquivo normal. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isglobal3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isglobal.                               --
--            Passando o caminho de diretorio                                                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isglobal (3/5)--
function tc_isglobal.test_isglobal3( )

    assert_true( os.exists( "dir1" ), "Dependencia: 'dir1' nao existe" )

    --chama a funcao testada
    local result = os.isglobal( "dir1" )

    --testa o retorno da funcao
    assert_false( result, "test_isglobal3. Passando diretorio. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isglobal4                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isglobal.                               --
--            Passando o caminho de um arquivo inexistente                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isglobal (4/5)--
function tc_isglobal.test_isglobal4( )

    --chama a funcao testada
    result = os.isglobal( "inexistente" )

    --testa o retorno da funcao
    assert_false( result, "test_isglobal4. Passando caminho inexistente. Retorno incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_isglobal5                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.isglobal.                               --
--            Passando uma string vazia                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do isglobal (5/5)--
function tc_isglobal.test_isglobal5( )

    local result = true
    local error_message = ""
    local error_code = 0

    --chama a funcao testada
    result = os.isglobal( "" )

    --testa o retorno da funcao
    assert_false( result, "test_isglobal5. Passando vazio. Retorno incorreto", true )
end

-- ****************** TESTE DE EXISTS ****************** --

-- Testes para o exists (5 teste)--
tc_exists = TestCase("os.exists")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_exists1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.exists.                                 --
--            Passando o caminho de um arquivo normal                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do exists (1/5)--
function tc_exists.test_exists1( )

    --cria arquivo para teste
    local handle, error_message, error_code = io.open( "exists1.txt", "rw" )
    --caso o arquivo nao exista e nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( handle, string.format( "Dependencia: 'exists1.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    handle:close()

    --chama a funcao testada
    local result = os.exists( "exists1.txt" )

    os.remove( "exists1.txt" )

    --testa o retorno da funcao
    assert_true( result, "test_exists1. Passando arquivo. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_exists2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.exists.                                 --
--            Passando o caminho de um arquivo global                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do exists (2/5)--
function tc_exists.test_exists2( )

    local result = true
    local error_message = ""
    local error_code = 0

    --cria arquivo para teste
    local handle, error_message, error_code = io.open( "exists2.txt", "rwg" )
    --caso o arquivo nao exista e nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( handle, string.format( "Dependencia: 'exists2.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    handle:close()

    --chama a funcao testada
    local result = os.exists( "exists2.txt", true )

    --remove o arquivo para teste
    os.remove( "exists2.txt", true )

    --testa o retorno da funcao
    assert_true( result, "test_exists2. Passando arquivo global. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_exists3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.exists.                                 --
--            Passando o caminho de um diretorio                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do exists (3/5)--
function tc_exists.test_exists3( )

    local result, error_message, error_code = os.mkdir( "exists3" )

    assert_true( result, string.format( "Dependencia: dir. exists3 nao pode ser criado [%s,'%s']", tostring( error_code ), tostring( error_message ) ), true )

    --chama a funcao testada
    result = os.exists( "exists3" )

    os.remove( "exists3" )

    --testa o retorno da funcao
    assert_true( result, "test_exists3. Passando diretorio. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_exists4                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.exists  .                               --
--            Passando o caminho de um arquivo inexistente                                       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do exists (4/5)--
function tc_exists.test_exists4( )

    --chama a funcao testada
    result = os.exists( "inexistente" )

    --testa o retorno da funcao
    assert_false( result, "test_exists4. Passando caminho inexistente. Retorno incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_exists5                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.exists  .                               --
--            Passando uma string vazia                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do exists (5/5)--
function tc_exists.test_exists5( )

    --chama a funcao testada
    result = os.exists( "" )

    --testa o retorno da funcao
    assert_false( result, "test_exists5. Passando vazio. Retorno incorreto", true )
end

-- ****************** TESTE DE FILESIZE ****************** --

-- Testes para o filesize (7 teste)--
tc_filesize = TestCase("os.filesize")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize1                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando o caminho de um arquivo normal com 1K                                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (1/7)--
function tc_filesize.test_filesize1( )

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "filesiz1.txt" ), "Dependencia: 'filesiz1.txt' nao encontrado", true )

    --chama a funcao testada
    local result, error_message, error_code = os.filesize( "filesiz1.txt" )

    --testa o retorno da funcao
    assert_equal( 1024, result, string.format( "test_filesize1. Passando arquivo. Retorno incorreto [%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize2                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando o caminho de um arquivo global com 1K                                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (2/7)--
function tc_filesize.test_filesize2( )

    local result = true
    local error_message = ""
    local error_code = 0

    if( os.exists( "filesiz2.txt", true ) ) then
        os.remove( "filesiz2.txt", true )
    end

    --se nao existir, cria arquivo para teste
    --cria arquivo para teste
    result, error_message, error_code = io.open( "filesiz2.txt", "rwg" )

    --caso o arquivo nao exista e nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( result, string.format( "Dependencia: 'filesiz2.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --preenche o arquivo com 1K de dados
    for i = 1, 1024 do
         result:write( "a" )
    end

    result:close( )


    --chama a funcao testada
    result = os.filesize( "filesiz2.txt", true )

    --remove o arquivo para teste
    os.remove( "filesiz2.txt", true )

    --testa o retorno da funcao
    assert_equal( 1024, result, "test_filesize2. Passando arquivo global. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize3                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando o caminho de um arquivo normal vazio                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (3/7)--
function tc_filesize.test_filesize3( )

    assert_true( os.exists( "filesiz3.txt" ), "Dependencia: 'filesiz3.txt' nao encontrado", true )

    --chama a funcao testada
    local result = os.filesize( "filesiz3.txt" )

    --testa o retorno da funcao
    assert_equal( 0, result, "test_filesiz3. Passando arquivo vazio. Retorno incorreto" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize4                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando o caminho de um arquivo global vazio                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (4/7)--
function tc_filesize.test_filesize4( )

    local result = true
    local error_message = ""
    local error_code = 0

    if( os.exists( "filesiz4.txt", true ) ) then
        os.remove( "filesiz4.txt" , true )
    end

    --cria arquivo para teste
    result, error_message, error_code = io.open( "filesiz4.txt", "rwg" )

    --caso o arquivo nao exista e nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( result, string.format( "Dependencia: 'filesiz4.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    result:close( )

    --chama a funcao testada
    result, error_message, error_code = os.filesize( "filesiz4.txt", true )

    --remove o arquivo para teste
    os.remove( "filesiz4.txt", true )

    --testa o retorno da funcao
    assert_equal( 0, result, string.format( "test_filesize4. Passando arquivo global vazio. Retorno incorreto [%s - '%s']", tostring( error_code ), tostring( error_message ) ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize5                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando o caminho de um diretorio                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (5/7)--
function tc_filesize.test_filesize5( )

    local result = true
    local error_message = ""
    local error_code = 0

    assert_true( os.exists( "dir1" ), "Dependencia: 'dir1' nao encontrado", true )

    --chama a funcao testada
    result, error_message, error_code = os.filesize( "dir1" )

    --testa o retorno da funcao
    assert_nil( result, string.format( "test_filesize5. Passando diretorio. Retorno incorreto [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    assert_equal( TST_ERR_ISADIR, error_code , "test_filesize5. Passando diretorio. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_ISADIR ], error_message , "test_filesize5. Passando diretorio. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize6                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando um caminho de um arquivo inexistente                                      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (6/7)--
function tc_filesize.test_filesize6( )

    local result = true
    local error_message = ""
    local error_code = 0

    --chama a funcao testada
    result, error_message, error_code = os.filesize( "inexistente" )

    --testa o retorno da funcao
    assert_nil( result, "test_filesize6. Passando arquivo inexistente. Retorno incorreto", true )

    assert_equal( TST_ERR_NOTFOUND, error_code , "test_filesize6. Passando arquivo inexistente. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_NOTFOUND ], error_message , "test_filesize6. Passando arquivo inexistente. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filesize7                                                                     --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filesize                                --
--            Passando string vazia                                                              --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filesize (7/7)--
function tc_filesize.test_filesize7( )

    local result = true
    local error_message = ""
    local error_code = 0

    --chama a funcao testada
    result, error_message, error_code = os.filesize( "" )

    --testa o retorno da funcao
    assert_nil( result, "test_filesize7. Passando vazio. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code , "test_filesize7. Passando vazio. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message , "test_filesize7. Passando vazio. Mensagem de erro incorreta", true )
end

-- ****************** TESTE DE FILEMTIME ****************** --

-- Testes para o filemtime (4 teste)--
tc_exists = TestCase("os.filemtime")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filemtime1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filemtime                               --
--            Passando o caminho de um arquivo normal                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filemtime (1/4)--
function tc_filesize.test_filemtime1( )

    local result = true
    local error_message = ""
    local error_code = 0

    if( os.exists( "filemt1.txt" ) ) then

        --se o arquivo para teste exista, ele eh removido para que seja criado novamente com o mtime atualizado
        result, error_message, error_code = os.remove( "filemt1.txt" )

        --caso o arquivo exista e a remocao dele falhe, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
        assert_not_nil( result, string.format( "Dependencia: 'filemt1.txt' nao pode ser apagado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    end

    --cria arquivo para teste e guarda um timestamp proximo do timestamp de criacao do arquivo
    local time1 = os.time( )
    result, error_message, error_code = io.open( "filemt1.txt", "rw" )
    local time2 = os.time( )

    --caso o arquivo nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( result, string.format( "Dependencia: 'filemt1.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    result:close( )

    --chama a funcao testada
    result = os.filemtime( "filemt1.txt" )

    --remove o arquivo para teste
    os.remove( "filemt1.txt" )

    --testa o retorno da funcao
    assert_true( ( ( result >= time1 ) and ( result <= time2 ) ), string.format( "test_filemtime1. Passando arquivo normal. timestamp '%i' nao esta entre '%i' e '%i'", result, time1, time2 ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filemtime2                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filemtime                               --
--            Passando o caminho de um arquivo global                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filemtime (2/4)--
function tc_filesize.test_filemtime2( )

    local result = true
    local error_message = ""
    local error_code = 0

    if( os.exists( "filemt2.txt", true ) ) then

        --se o arquivo para teste exista, ele eh removido para que seja criado novamente com o mtime atualizado
        result, error_message, error_code = os.remove( "filemt2.txt", true )

        --caso o arquivo exista e a remocao dele falhe, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
        assert_not_nil( result, string.format( "Dependencia: 'filemt2.txt' nao pode ser apagado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    end

    --cria arquivo para teste e guarda um timestamp proximo do timestamp de criacao do arquivo
    local time1 = os.time( )
    result, error_message, error_code = io.open( "filemt2.txt", "rwg" )
    local time2 = os.time( )

    --caso o arquivo nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( result, string.format( "Dependencia: 'global.txt' nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    result:close( )

    --chama a funcao testada
    result = os.filemtime( "filemt2.txt", true )

    assert_not_nil( result,"test_filemtime2. Passando arquivo global. Retorno nulo", true )

    --remove o arquivo para teste
    os.remove( "filemt2.txt", true )

    --testa o retorno da funcao
    assert_true( ( ( result >= time1 ) and ( result <= time2 ) ), string.format( "test_filemtime1. Passando arquivo global. timestamp '%i' nao esta entre '%i' e '%i'", result, time1, time2 ) )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_filemtime3                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.filemtime                               --
--            Passando o caminho de um diretorio                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do filemtime (3/4)--
function tc_filesize.test_filemtime3( )

    local result = true
    local error_message = ""
    local error_code = 0

    if( os.exists( "filemt3" ) ) then

        --se o diretorio para teste exista, ele eh removido para que seja criado novamente com o mtime atualizado
        result, error_message, error_code = os.remove( "filemt3" )

        --caso o arquivo exista e a remocao dele falhe, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
        assert_not_nil( result, string.format( "Dependencia: diretorio nao pode ser apagado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
    end

    --cria arquivo para teste e guarda um timestamp proximo do timestamp de criacao do arquivo
    local time1 = os.time( )
    result, error_message, error_code = os.mkdir( "filemt3" )
    local time2 = os.time( )

    --caso o arquivo nao tenha conseguido ser criado, a execucao o teste eh cancelada(devido a o ultimo parametro do assert ser 'true')
    assert_not_nil( result, string.format( "Dependencia: diretorio nao pode ser criado: [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --chama a funcao testada
    result = os.filemtime( "filemt3" )

    --remove o arquivo para teste
    os.remove( "filemt3" )

    --testa o retorno da funcao
    assert_true( ( ( result >= time1 ) and ( result <= time2 ) ), string.format( "test_filemtime3. Passando diretorio. timestamp '%i' nao esta entre '%i' e '%i'", result, time1, time2 ) )
end

-- ****************** TESTE DE TMPNAME ****************** --

-- Testes para o os.tmpname (2 testes)--
tc_tmpname = TestCase("os.tmpname")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_tmpname1                                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.tmpname                                 --
--            Chama a funcao e verifica se os nomes estao corretos                               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do tmpname (1/1)--
function tc_tmpname.test_tmpname1( )

    assert_true( os.exists( "tmp0.tmp" ), "Dependencia: tmp0.tmp nao encontrado", true )
    assert_true( os.exists( "tmp2.tmp" ), "Dependencia: tmp2.tmp nao encontrado", true )

    assert_equal( "tmp1.tmp", os.tmpname(), "test_tmpname2. Retorno incorreto de os.tmpname()" )
    assert_equal( "tmp3.tmp", os.tmpname(), "test_tmpname2. Retorno incorreto de os.tmpname()" )
    assert_equal( "tmp4.tmp", os.tmpname(), "test_tmpname2. Retorno incorreto de os.tmpname()" )

end

-- ****************** TESTE DE GETENV ****************** --

-- Testes para o os.getenv (2 testes)--
tc_getenv = TestCase("os.getenv")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getenv1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.getenv                                  --
--            Chama a funcao passando o numero de serie do terminal(Caso de sucesso)             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do getenv (1/2)--
function tc_getenv.test_getenv1()

    local id_name = { "SYS_SERIALNU", "SYS_HIPERFLEXVER", "SYS_PLATFORMNAME", "SYS_PLATFORMVER" }
    local i = 1

    assert_not_nil( SYS_SERIALNU, "test_getenv1. Caso de sucesso. Constante SYS_SERIALNU nula", true )
    assert_not_nil( SYS_HIPERFLEXVER, "test_getenv1. Caso de sucesso. Constante SYS_HIPERFLEXVER nula", true )
    assert_not_nil( SYS_PLATFORMNAME, "test_getenv1. Caso de sucesso. Constante SYS_PLATFORMNAME nula", true )
    assert_not_nil( SYS_PLATFORMVER, "test_getenv1. Caso de sucesso. Constante SYS_PLATFORMVER nula", true )

    for i,actual_id in ipairs( { SYS_SERIALNU, SYS_HIPERFLEXVER, SYS_PLATFORMNAME, SYS_PLATFORMVER } ) do		
        local result = os.getenv( actual_id )

		result = ui.show_message('test_getenv1','executando')
		result = ui.show_message('test_getenv1',(id_name[i] .. ": " .. tostring( result ) .. "\nCorreto?"), TIMEOUT_UI)
		
        assert_equal( KEY_ONE, result, string.format( "test_getenv1. Caso de sucesso. Erro Passando a constante %s", id_name[i] ), true )
    end
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getenv2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.getenv                                  --
--            Chama a funcao passando um numero invalido                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

--Teste do getenv (2/2)--
-- nao ta funcionando, sempre ta dando erro na funcao
function tc_getenv.test_getenv2()

    local result, error_message, error_code = os.getenv( -1 )

    assert_nil( result, "test_getenv2. Passando um id invalido. Retorno incorreto", true );

    assert_equal( TST_ERR_INVALSYSPRTY, error_code, "test_getenv2. Passando um id invalido. Codigo de erro incorreto", true );

    assert_equal( lua_error_message[ TST_ERR_INVALSYSPRTY ], error_message, "test_getenv2. Passando um id invalido. Mensagem de erro incorreta" );
end

-- ****************** TESTE DE IMAGE:WIDTH IMAGE:HEIGHT & IMAGE:SIZE ****************** --

tc_image = TestCase("image")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_image_width_height_size1                                                      --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o image:width, image:height & image:size     --
--            Carrega uma imagem e verifica a dimensao da mesma                                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_image.test_image_width_height_size1()

    assert_true( os.exists( "teste.bmp" ), "Dependencia: 'teste.bmp nao foi encontrado'", true )

    local image = os.loadimage( "teste.bmp" )

    assert_not_nil( image, "Dependencia: imagem nao pode ser carregada", true )

    assert_equal( 128, image:width(), "test_image_width_height_size1. Chamando o width() de uma imagem. Retorno incorreto", true )
    assert_equal( 40, image:height(), "test_image_width_height_size1. Chamando o height() de uma imagem. Retorno incorreto", true )

    local width, height = image:size()
    
    assert_equal( 128, width, "test_image_width_height_size1. Largura retornado por size() esta incorreto", true )
    assert_equal( 40,  height, "test_image_width_height_size1. Altura retornado por size() esta incorreto", true )
end

-- ****************** TESTE DE OS.SETDATETIME ****************** --

tc_setdatetime = TestCase("os.setdatettime")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_setdatetime1                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.setdatetime                             --
--            Caso de sucesso. Passa uma data valida para ser setada                             --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_setdatetime.test_setdatetime1()

    --salva a data/hora atual para ser restaurada no fim do teste
    local curr_date = os.date( "*t" )


    assert_not_nil( curr_date, "Dependencia: Nao foi possivel verificar a data/hora atual", true )

    --cria data que sera modificada
    local test_date = {}
    test_date.year = 2025
    test_date.month = 08
    test_date.day = 13
    test_date.hour = 12
    test_date.min = 15
    test_date.sec = 00

    local result = os.setdatetime( test_date )

    assert_true( result, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Retorno incorreto", true )

    local new_date = os.date( "*t" )

    assert_equal( test_date.year, new_date.year, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Ano incorreto" )
    assert_equal( test_date.month, new_date.month, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Mes incorreto" )
    assert_equal( test_date.day, new_date.day, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Dia incorreto" )
    assert_equal( test_date.hour, new_date.hour, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Hora incorreta" )
    assert_equal( test_date.min, new_date.min, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Minuto incorreto" )
    assert_equal( test_date.sec, new_date.sec, "test_setdatetime1. Caso de sucesso. Alterando para uma data valida. Segundo incorreto" )

    --volta a data/hora original
    os.setdatetime( curr_date )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_setdatetime2                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.setdatetime                             --
--            Passando uma data invalida                                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_setdatetime.test_setdatetime2()

    local test_date = {}
    test_date.year = -100
    test_date.month = -100
    test_date.day = -100
    test_date.hour = 12
    test_date.min = 15
    test_date.sec = 00

    local result, error_message, error_code = os.setdatetime( test_date )

    assert_nil( result, "test_setdatetime2. Passando uma data invalida. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDDATE, error_code, "test_setdatetime2. Passando uma data invalida. Codigo de erro incorreto", true )
    assert_equal( lua_error_message[ TST_ERR_INVALIDDATE], error_message, "test_setdatetime2. Passando uma data invalida. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_setdatetime3                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o os.setdatetime                             --
--            Passando uma hora invalida                                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc_setdatetime.test_setdatetime3()

    local test_date = {}
    test_date.year = 2025
    test_date.month = 08
    test_date.day = 13
    test_date.hour = -100
    test_date.min = -100
    test_date.sec = -100

    local result, error_message, error_code = os.setdatetime( test_date )

    assert_nil( result, "test_setdatetime2. Passando uma hora invalida. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDTIME, error_code, "test_setdatetime2. Passando uma hora invalida. Codigo de erro incorreto", true )
    assert_equal( lua_error_message[ TST_ERR_INVALIDTIME ], error_message, "test_setdatetime2. Passando uma hora invalida. Mensagem de erro incorreta" )
end

-- ****************** TESTE DE OS.UPDATE ****************** --

tc_osupdate = TestCase("os.update")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_update1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Caso de sucesso. Agenda uma atualiza��o                                            --
--                                                                                               --
---------------------------------------------------------------------------------------------------
function tc_osupdate.test_update1()

    --[[ui.message("UPDATE1", "Entre com os dados do agendamento. Pressione uma tecla.")
	
    local test_date = {}
    
    local tf = ui.textfield("UPDATE1", "DD/MM/YYYY", 8, 8)

    tf:pattern("##/##/####")

    assert_true( tf:show(), "test_update1. Caso de sucesso. Agenda uma atualiza��o. Retorno incorreto", true )

    local strdata = tf:text()
    test_date.year = strdata:sub(5, 8)
    test_date.month = strdata:sub(3, 4)
    test_date.day = strdata:sub(1, 2)

    tf = ui.textfield("UPDATE1", "HH:MM:SS", 6, 6)
    tf:pattern("##:##:##")

    assert_true( tf:show(), "test_update1. Caso de sucesso. Agenda uma atualiza��o. Retorno incorreto", true )

    strdata = tf:text()
    test_date.hour = strdata:sub(1, 2)
    test_date.min = strdata:sub(3, 4)
    test_date.sec = strdata:sub(5, 6)]]
	
	ui.show_input("test_update1", "UPDATE1", "DDMMYYYY", false, "##/##/####", "##/##/####", TIMEOUT_UI, tipo)
	
    local test_date = {}
    
    local tf = ui.textfield("UPDATE1", "DD/MM/YYYY", 8, 8)

    tf:pattern("##/##/####")

    assert_true( tf:show(), "test_update1. Caso de sucesso. Agenda uma atualiza��o. Retorno incorreto", true )

    local strdata = tf:text()
    test_date.year = strdata:sub(5, 8)
    test_date.month = strdata:sub(3, 4)
    test_date.day = strdata:sub(1, 2)

    tf = ui.textfield("UPDATE1", "HH:MM:SS", 6, 6)
    tf:pattern("##:##:##")

    assert_true( tf:show(), "test_update1. Caso de sucesso. Agenda uma atualiza��o. Retorno incorreto", true )

    strdata = tf:text()
    test_date.hour = strdata:sub(1, 2)
    test_date.min = strdata:sub(3, 4)
    test_date.sec = strdata:sub(5, 6)

    local result = os.update( test_date )

    assert_equal( os.time(test_date), result, "test_update1. Caso de sucesso. Agenda uma atualiza��o. Retorno incorreto", true )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_update2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Caso de sucesso. Excluindo atualiza��es                                            --
--                                                                                               --
---------------------------------------------------------------------------------------------------
function tc_osupdate.test_update2()

    local result = os.update( nil )

    assert_equal( 0, result, "test_update2. Caso de sucesso. Excluindo atualiza��es. Retorno incorreto", true )
end

lunit.run()
