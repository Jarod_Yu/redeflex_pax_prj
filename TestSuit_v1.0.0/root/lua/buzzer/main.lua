---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios do buzzer                                   --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         27/01/2006  Cria��o                                          --
-- Leandro              lmf         03/02/2006  Cabe�alho das fun��es                            --
-- Jamerson Lima        jrfl        01/06/2006  Novos testes                                     --
-- Jamerson Lima        jrfl        06/06/2006  Rework(HFLEX-LUA-TSTR-CODE-INSP004)              --
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"

---------------------------------------------------------------------------------------------------
--                                     Variaveis Globais                                         --
---------------------------------------------------------------------------------------------------
local tempo = 250
local tempo2 = 200
local sleep = 30

---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

-- ****************** TESTE DE BUZZER ****************** --

-- Testes para o buzzer (1 teste)--

tc = TestCase("keyboard.buzzer")

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_buzzer1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o buzzer, um som � tocado para               --
--            o usu�rio e no fim do teste o mesmo indica se ouviu ou n�o o som.                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
-- Teste do buzzer (1/1)--
function tc.test_buzzer1()

    --display.clear()
	tecla = ui.show_message('test_buzzer1','executando')

    keyboard.buzzer(tempo, 4678)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3136)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4678)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3136)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4678)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)

    HF_sleep(sleep)
    keyboard.buzzer(tempo, 4434)

    HF_sleep(sleep)
    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)

    HF_sleep(sleep)
    keyboard.buzzer(tempo, 4434)

    HF_sleep(sleep)
    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)

    HF_sleep(sleep)
    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3322)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3322)

    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3322)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4978)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3136)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4978)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3136)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4978)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)



    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4434)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3729)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)



    keyboard.buzzer(tempo, 3322)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 3322)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 2793)
    HF_sleep(sleep)

    keyboard.buzzer(tempo, 4186)
    HF_sleep(sleep)

    --display.print("Ouviu o som?", 0, 0)
	
	--display.
	--ui.
	
	--titulo, mensagem, duracao
	tecla = ui.show_message('test_buzzer1','Ouviu o som?', TIMEOUT_UI)
	
	--ui.show_input
	--titulo, label, text, ispass (true/false), min, max, tempo, tipo

	--ui.show_menu
	--titulo, qtd_opces, opcao_inicial, tempo, opcoes
	-- opcoes = { 'texto 1', 'texto 2'}
    
	--tecla = lerTecla( )

    assert_equal(KEY_ONE, tecla, "Test_buzzer1. Erro no teste do buzzer")
end


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_buzzer2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o buzzer.                                    --
--            Passando uma duracao negativa.                                                     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc.test_buzzer2( )

    local result, error_message, error_code = keyboard.buzzer( -1, 4678 )

    assert_nil( result, "Test_buzzer2. Tempo < 0. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_buzzer2. Tempo < 0. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_buzzer2. Tempo < 0. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_buzzer3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o buzzer.                                    --
--            Passando uma frequencia negativa.                                                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc.test_buzzer3()

    local result, error_message, error_code = keyboard.buzzer( 1, -1 )

    assert_nil( result, "Test_buzzer3. Freq. < 0. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_buzzer3. Freq. < 0. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_buzzer3. Freq. < 0. Mensagem de erro incorreta" )
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_buzzer4                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o buzzer.                                    --
--            Passando todas as constantes de duracao                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc.test_buzzer4( )

    for _,actual_length in ipairs( { BEEP_CLICK, BEEP_SHORT, BEEP_LONG } ) do

        local result = keyboard.buzzer( actual_length, 4678 )
		
        assert_true( result, string.format( "test_buzzer4. length = '%s'. Retorno incorreto", actual_length ), true )
		
		tecla = ui.show_message('test_buzzer4','executando')
		
		tecla = ui.show_message('test_buzzer4', string.format("Ouviu o som para length=%d?", actual_length), TIMEOUT_UI)

        assert_equal(KEY_ONE, tecla, string.format( "test_buzzer4. length = '%s'", actual_length ), true )
    end

end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_buzzer5                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o buzzer.                                    --
--            Passando todas as constantes de frequencia                                         --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
function tc.test_buzzer5( )

    for _,actual_frequency in ipairs( { BEEP_LOW, BEEP_MIDTONE, BEEP_HIGH } ) do

        local result = keyboard.buzzer( 2000, actual_frequency )

        assert_true( result, string.format( "Test_buzzer5. frequency = '%s'. Retorno incorreto", actual_frequency ), true )

		tecla = ui.show_message('test_buzzer5','executando')
		tecla = ui.show_message('test_buzzer5', string.format("Ouviu o som para freq=%d?", actual_frequency), TIMEOUT_UI)

        assert_equal(KEY_ONE, tecla, string.format( "Test_buzzer5. frequency = '%s'", actual_frequency ), true )
    end
end

lunit.run()