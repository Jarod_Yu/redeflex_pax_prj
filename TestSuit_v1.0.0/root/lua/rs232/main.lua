-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              rs232/main.lua                                                      --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                                                                                        --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Guilherme            gkmo          19098     26/12/2006     Criacao                              --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   Funcoes                                                         --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   COMM TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "rs232" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_open_close1                                                                --
--  Descricao: Caso de sucesso. Abre e fecha uma porta RS232 usando as configur��es padr�o.          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_open_close1()

    local testname = "test_rs232_open_close1"
    local port = "COM1"

    ui.message("open_close1", "")

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Abre e fecha uma porta RS232. Porta nulo", true )
    
    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre e fecha uma porta RS232. Erro ao fechar porta RS232", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_open_close2                                                                --
--  Descricao: Caso de sucesso. Abre e fecha uma porta RS232 usando as configur��es que n�o a padr�o --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_open_close2()
    
    local testname = "test_rs232_open_close2"
    local port = "COM2"
    local config = {}
    config.baud = 57600
    config.parity = "even"
    config.stopbits = 1
    config.flow = "hard"

    ui.message("open_close2", "")

    local handle = rs232.open(port, config)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Abre e fecha uma porta RS232. Porta nulo", true )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre e fecha uma porta RS232. Erro ao fechar porta RS232", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_open_close3                                                                --
--  Descricao: Tenta abrir uma conex�o com uma porta inv�lida                                        --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_open_close3()

    local testname = "test_rs232_open_close3"
    local port = "INVALIDA"

    ui.message("open_close3", "")

    local handle, error_message, error_code = rs232.open(port)

    assert_nil( handle, testname .. ": Tenta abrir uma conex�o com uma porta inv�lida. Porta nulo", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, testname .. ": Tenta abrir uma conex�o com uma porta inv�lida. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, testname .. ": Tenta abrir uma conex�o com uma porta inv�lida. Mensagem de erro incorreta" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_open_close4                                                                --
--  Descricao: Tenta abrir uma conex�o com configura��es inv�lidas                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_open_close4()

    local testname = "test_rs232_open_close4"
    local port = "COM2"
    local config = {}
    config.baud = 1000 -- par�metro inv�lido
    config.parity = "even"
    config.stopbits = 1
    config.flow = "hard"
    
    ui.message("open_close4", "")

    local handle, error_message, error_code = rs232.open(port, config)

    assert_nil( handle, testname .. ": Tenta abrir uma conex�o com configura��es inv�lidas. Porta nulo", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, testname .. ": Tenta abrir uma conex�o com configura��es inv�lidas. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, testname .. ": Tenta abrir uma conex�o com configura��es inv�lidas. Mensagem de erro incorreta" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_send1                                                                      --
--  Descricao: Caso de sucesso. Estabelece uma conexao e envia dados por ela.                        --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_send1()

    local testname = "test_rs232_send1"
    local port = "COM1"
    local msg = "0123456789ABCDEFGHIJKLMNOPQRSTUVXZ"
    
    ui.message("send1", "")

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Estabelece uma conexao e envia dados por ela. Porta nulo", true )

    ui.message(testname, "Estabeleca uma conexao com o Hyperterminal e pressione uma tecla")

    keyboard.getkeystroke()

    local result = handle:send(msg)

    assert_equal( msg:len(), result, testname .. ": Caso de sucesso. Estabelece uma conexao e envia dados por ela. Retorno incorreto", true )

    ui.message(testname, "Recebeu a mensagem?\n1 - SIM\n2 - NAO")

    local key = keyboard.getkeystroke()
    
    assert_equal( key, KEY_ONE, testname .. ": Caso de sucesso. Estabelece uma conexao e envia dados por ela. Mensagem n�o enviada", true )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Estabelece uma conexao e envia dados por ela. Erro ao fechar porta RS232", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_receive1                                                                   --
--  Descricao: Caso de sucesso. Estabelece uma conexao e recebe dados por ela.                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_receive1()
    
    local testname = "test_rs232_receive1"
    local port = "COM1"
    local bytesToRead = 5

    ui.message("receive1", "Envie 5 bytes pela conexao RS232")

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Estabelece uma conexao e recebe dados por ela. Porta nulo", true )

    local result = handle:receive(bytesToRead, 120000)

    assert_equal( bytesToRead, result:len(), testname .. ": Caso de sucesso. Estabelece uma conexao e recebe dados por ela. Retorno incorreto", true )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Estabelece uma conexao e recebe dados por ela. Erro ao fechar porta RS232", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rs232_receive2                                                                   --
--  Descricao: Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela.                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_rs232_receive2()

    local testname = "test_rs232_receive2"
    local port = "COM1"
    local bytesToRead = 50

    ui.message("receive2", "Aguarde 10 segundos para dar TIMEOUT")

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Porta nulo", true )

    local result, error_message, error_code = handle:receive(bytesToRead, 10000)

    assert_nil( result, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela", true )

    assert_equal( TST_ERR_TIMEOUT, error_code, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_TIMEOUT ], error_message, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Mensagem de erro incorreta" )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Erro ao fechar porta RS232", true )

end

lunit.run( )
