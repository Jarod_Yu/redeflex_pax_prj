
-- Testes unit�rios da atualiza��o do Hiperflex.
-- lfn - 04/04/2008 - testes do caminho feliz e de
--                    argumentos inv�lidos
-- lfn - 07/04/2008 - menu para os testes
-- lfn - 09/04/2008 - fone vazio e caller id vazio

-- Teste do caminho feliz.
--
-- Compilar o Hiperflex com a op��o _TEST_HF_UPDATE_.
-- Esperar 30 segundos at� que haja o timeout
-- do menu do ams, ent�o ele processar� o arquivo
-- com o agendamento. A fun��o HF_upd_downloadHiperflex
-- vai imprimir os par�metros da atualiza��o.
local function teste_caminho_feliz( )
    local tbl_data = os.date( "*t" )
    local ret, msg = os.updateHF(
        tbl_data,
        "56788765",
        "123"
    )
    if ret == nil then
        printer.print( "erro em os.updateHF!" )
        printer.print( msg )
        return
    end

    local msg =
        "saia da aplicacao e " ..
        "verifique a impressao dos params"
    ui.message( "caminho feliz", msg,
                UI_NOSCREENNUMBER, "center", "center" )
    keyboard.getkeystroke( )
end

-- Test assertion para duas strings.
local function test_string( comment, expected, actual )
    if expected == actual then
        printer.print( comment .. ": ok" )
    else
        printer.print( comment .. ': erro (esperava "' .. expected ..
                       '", veio "' .. actual .. '")' )
    end
end

-- Teste de argumentos inv�lidos.
local function teste_args_invalidos( )
    local tbl_data = os.date( "*t" )

    -- timestamp nulo
    local ret, msg = os.updateHF( nil, "56788765", "123" )
    test_string( "timestamp nulo", "invalid argument", msg )

    -- telefone nulo
    ret, msg = os.updateHF( tbl_data, nil, "123" )
    test_string( "telefone nulo", "invalid argument", msg )

    -- caller id nulo
    ret, msg = os.updateHF( tbl_data, "56788765", nil )
    test_string( "caller id nulo", "invalid argument", msg )

    -- data inv�lida
    local day = tbl_data.day;
    tbl_data.day = 0;
    ret, msg = os.updateHF( tbl_data, "56788765", nil )
    test_string( "data invalida", "invalid argument", msg )
    tbl_data.day = day;

    -- telefone inv�lido
    ret, msg = os.updateHF( tbl_data, "aaa", "123" )
    test_string( "telefone invalido", "invalid argument", msg )

    -- telefone vazio
    ret, msg = os.updateHF( tbl_data, "", "123" )
    test_string( "telefone vazio", "invalid argument", msg )

    -- caller id vazio
    ret, msg = os.updateHF( tbl_data, "56788765", "" )
    test_string( "caller id vazio", "invalid argument", msg )

end

-- Teste de leitura do agendamento.
local function teste_ler_agendamento( )
    local time, phone, caller = os.updateHF( )
    local msg;
    if time == nil then
        msg = "ERRO!"
        local errmsg = phone
        printer.print( errmsg )
    else
        msg =
            "time: "  .. tostring( time ) .. "\n" ..
            "phone: [" .. phone .. "]\n" ..
            "caller: [" .. caller .. "]"
    end
    ui.message( "ler agend", msg,
                UI_NOSCREENNUMBER, "center", "center" )
    keyboard.getkeystroke( )
end

-- Teste de remo��o do agendamento.
local function teste_remover_agendamento( )
    local ret, errmsg = os.updateHF( nil )
    local msg;
    if errmsg ~= nil then
        msg = "ERRO!"
        printer.print( errmsg )
    else
        msg =
            "1. rode teste caminho feliz\n" ..
            "2. saia da apl e veja se o pos imprime params " ..
            "(nao deve)"
    end
    ui.message( "remover agend", msg,
                UI_NOSCREENNUMBER, "center", "center" )
    keyboard.getkeystroke( )
end

-- Main.
local function main( )
    local opcoes = {
        "caminho feliz",
        "args invalidos",
        "ler agend",
        "remover agend",
        "sair"
    }
    local menu = ui.menu( "Testes Upd HF", opcoes )
    while true do
        local aceitou = menu:show( )
        if not aceitou then
            break
        end

        local opcao = menu:accepted( )
        if opcao == 1 then
            teste_caminho_feliz( )
        elseif opcao == 2 then
            teste_args_invalidos( )
        elseif opcao == 3 then
            teste_ler_agendamento( )
        elseif opcao == 4 then
            teste_remover_agendamento( )
        elseif opcao == 5 then
            break
        end
    end
    ui.destroy( menu )
end

main( )
