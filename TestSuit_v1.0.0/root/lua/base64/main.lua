-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 pin/main.lua                                                    --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de tdes                                   --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Richardson        rco         NA      09/07/2009        Criacao                                 --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   TDES TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "pin" )

-------------------------------------------------------------------------------------------------------
--                                                                                                                                    --
--  Nome:      test_pin_getPinDUKPT                                                                          --
--  Descricao: Caso de sucesso. Executa com sucesso gerando um pinblock e um ksn  --
--                                                                                                                                    --
--  Argumentos:                                                                                                             --
--  Dependencias:                                                                                                         --
--  Retornos:                                                                                                                  --
--    Casos de erro:                                                                                                         --
--    Casos de sucesso:                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc.test_pin_getPinDUKPT()
	local error_message = nil
	local pinBlock = nil
	local ksn = nil
	local result = true
	local key = nil
	
	pinBlock, ksn = pin.getPinDUKPT (1, 16, "R$ 200,00", "SENHA DUKPT", 1000, "1234567890123", 1)
	
  log.logbytes(pinBlock)
	log.logbytes(ksn)
	
  assert_not_nil( pinBlock, "test_pin_getPinDUKPT. Caso de sucesso. pinBlock retornado", true )
	assert_not_nil( ksn, "test_pin_getPinDUKPT. Caso de sucesso. ksn retornado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                                                    --
--  Nome:      test_pin_getPinMK3DES                                                                       --
--  Descricao: Caso de sucesso. Executa com sucesso gerando um pinblock          --
--                                                                                                                                    --
--  Argumentos:                                                                                                             --
--  Dependencias:                                                                                                         --
--  Retornos:                                                                                                                  --
--    Casos de erro:                                                                                                        --
--    Casos de sucesso:                                                                                                  --
-------------------------------------------------------------------------------------------------------
function tc.test_pin_getPinMK3DES()
	local error_message = nil
	local tdes_key = nil
	local pinBlock = nil
	local key = nil
	local keyValue = "/tdes.key"
	local result = true
		
   		
	tdes_key, error_message = LoadFileInBuffer( keyValue )	
	
	assert_not_nil( tdes_key, error_message, true )
	
	pinBlock = pin.getPinMK3DES (1, 16, "R$ 200,00", "SENHA 3DES", 1000, "1234567890123", 1, tdes_key)
	               
	log.logbytes(pinBlock)
	
  assert_not_nil( pinBlock, "test_pin_getPinDUKPT. Caso de sucesso. pinBlock retornado", true )
end

lunit.run()
