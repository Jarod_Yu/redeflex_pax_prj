-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              barcode/main.lua                                                    --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                                                                                        --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
-- Guilherme             gkmo          20766     12/02/2007     Corrigindo bugs                      --
-- Guilherme             gkmo          19098     27/12/2006     Criacao                              --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   Funcoes                                                         --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   COMM TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "barcode" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_loadconfig1                                                              --
--  Descricao: Caso de sucesso. Carrega a configuração padrão.                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_loadconfig1()

    local testname = "test_barcode_loadconfig1"

    local result = barcode.loadconfig()

    assert_true( result, testname .. "Caso de sucesso. Carrega a configuração padrão. Retorno incorreto", true )

    result = barcode.config("port")
    assert_equal("COM1", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Porta incorreto", true )

    result = barcode.config("baud_rate")
    assert_equal("2400", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Baud rate incorreto", true )

    result = barcode.config("parity")
    assert_equal("odd", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Parity incorreto", true )

    result = barcode.config("data_size")
    assert_equal("7", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Data Size incorreto", true )

    result = barcode.config("stop_bits")
    assert_equal("2", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Stop Bits incorreto", true )

    result = barcode.config("protocol")
    assert_equal("STX_ETX_BCC", result, testname .. "Caso de sucesso. Carrega a configuração padrão. Protocol incorreto", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_loadconfig2                                                              --
--  Descricao: Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades.       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_loadconfig2()

    local testname = "test_barcode_loadconfig2"

    assert_true( os.exists( "barcode.cfg" ), "Dependencia: barcode.cfg nao encontrado", true )

    local result = barcode.loadconfig("barcode.cfg")

    assert_true( result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Retorno incorreto", true )

    result = barcode.config("port")
    assert_equal("COM1", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Porta incorreto", true )

    result = barcode.config("baud_rate")
    assert_equal("1200", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Baud rate incorreto", true )

    result = barcode.config("parity")
    assert_equal("even", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Parity incorreto", true )

    result = barcode.config("data_size")
    assert_equal("8", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Data Size incorreto", true )

    result = barcode.config("stop_bits")
    assert_equal("1", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Stop Bits incorreto", true )

    result = barcode.config("protocol")
    assert_equal("CR", result, testname .. "Caso de sucesso. Carrega a configuração a partir de um arquivo de propriedades. Protocol incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_loadconfig3                                                              --
--  Descricao: Tenta carregar a configuração de um arquivo inexistente.                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_loadconfig3()

    local testname = "test_barcode_loadconfig3"

    local result, error_message, error_code = barcode.loadconfig("invalido.cfg")

    --verifica se os retornos foram corretos
    assert_nil( result, testname .. ". Tenta carregar a configuração de um arquivo inexistente. Retorno incorreto", true )

    assert_equal( TST_ERR_NOTFOUND, error_code, testname .. ". Tenta carregar a configuração de um arquivo inexistente. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_NOTFOUND ], error_message, testname .. ". Tenta carregar a configuração de um arquivo inexistente. Mensagem de erro incorreta" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_config1                                                                  --
--  Descricao: Caso de sucesso. Modifica algumas configurações da leitora de código de barras.       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_config1()

    local testname = "test_barcode_config1"

    barcode.config("port", "COM1")
    barcode.config("baud_rate", "4800")
    barcode.config("parity", "none")
    barcode.config("data_size", "7")
    barcode.config("stop_bits", "2")
    barcode.config("protocol", "CR_LF")

    -- verifica se as configurações foram realizadas
    result = barcode.config("port")
    assert_equal("COM1", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Porta incorreto", true )

    result = barcode.config("baud_rate")
    assert_equal("4800", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Baud rate incorreto", true )

    result = barcode.config("parity")
    assert_equal("none", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Parity incorreto", true )

    result = barcode.config("data_size")
    assert_equal("7", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Data Size incorreto", true )

    result = barcode.config("stop_bits")
    assert_equal("2", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Stop Bits incorreto", true )

    result = barcode.config("protocol")
    assert_equal("CR_LF", result, testname .. "Caso de sucesso. Modifica algumas configurações da leitora de código de barras. Protocol incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_config2                                                                  --
--  Descricao: Tenta modificar uma propriedade inexistente.                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_config2()

    local testname = "test_barcode_config2"
    
    local result, error_message, error_code = barcode.config("invalida", "erro")

    --verifica se os retornos foram corretos
    assert_nil( result, testname .. ". Tenta modificar uma propriedade inexistente. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDBARCODEPRTY, error_code, testname .. ". Tenta modificar uma propriedade inexistente. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDBARCODEPRTY ], error_message, testname .. ". Tenta modificar uma propriedade inexistente. Mensagem de erro incorreta" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_saveconfig1                                                              --
--  Descricao: Caso de sucesso. Salva as configurações em um arquivo de propriedades                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_saveconfig1()

    local testname = "test_barcode_saveconfig1"

    local port = barcode.config("port")
    local parity = barcode.config("parity")
    local baud_rate = barcode.config("baud_rate")
    local protocol = barcode.config("protocol")
    local stop_bits = barcode.config("stop_bits")
    local data_size = barcode.config("data_size")

    local result = barcode.saveconfig("saved.cfg")

    assert_true( result, testname .. "Caso de sucesso. Salva as configurações em um arquivo de propriedades. Retorno incorreto", true )

    local prop = property.open("saved.cfg")
    
    assert_equal( prop:getstring("port"), port, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    assert_equal( prop:getstring("parity"), parity, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    assert_equal( prop:getstring("baud_rate"), baud_rate, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    assert_equal( prop:getstring("protocol"), protocol, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    assert_equal( prop:getstring("stop_bits"), stop_bits, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    assert_equal( prop:getstring("data_size"), data_size, testname .. ". Caso de sucesso. Salva as configurações em um arquivo de propriedades. Propriedade incorreta" )
    
    property.close(prop)
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_read1                                                                    --
--  Descricao: Caso de sucesso. Lê um código de barras.                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_read1()

    local testname = "test_barcode_read1"
    
    -- carrega as configurações padrão
    barcode.loadconfig()

    ui.message("READ1", "Passe o codigo de barras")

    local data = barcode.read(60 * 1000)

    assert_not_nil( data, testname .. ": Caso de sucesso. Lê um código de barras. Retorno incorreto", true )
    
    printer.print("Tamanho: " .. tostring(data:len()))
    printer.print(data)
    printer.linefeed(5)
    
    ui.message("READ1", "Os dados impressos estão corretos?\n1 - SIM\n2 - NAO")
    
    local key = keyboard.getkeystroke()
    
    assert_equal( key, KEY_ONE, testname .. ": Caso de sucesso. Lê um código de barras. Dados incorretos", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_barcode_read2                                                                    --
--  Descricao: Espera dar timeout.                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_barcode_read2()

    local testname = "test_barcode_read2"

    -- carrega as configurações padrão
    barcode.loadconfig()

    ui.message("READ2", "Aguarde 10 segundos para dar timeout")

    local result, error_message, error_code = barcode.read(10 * 1000)

    assert_nil( result, testname .. ": Espera dar timeout.", true )

    assert_equal( TST_ERR_TIMEOUT, error_code, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_TIMEOUT ], error_message, testname .. ": Estabelece uma conexao e espera dar TIMEOUT ao receber dados por ela. Mensagem de erro incorreta" )

end

lunit.run( )
