-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              compression/main.lua                                                --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                    Testes de compression                                               --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      30/03/2006        Cria��o                             --
--  Jamerson Lima        jrfl          NA      11/05/2006        Reformula��o dos testes             --
--  Jamerson Lima        jrfl          9844    11/05/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP0010)--
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-- O sucesso da execucao dos testes das funcoes de compressao de arquivo dependem dos seguintes
-- fatores:
-- Os seguintes arquivos texto( todos contem 1KB ) textos devem estar no POS pois vao sofrer compressao:
-- tif_001.dat
-- tif_002.txt
-- tif_003.txt
-- tif_004.txt
-- tif_006.txt
-- tif_009.txt
-- Os seguintes arquivos comrimidos devem estar no POS:
-- tbf_003.huf
-- tbf_004.huf
-- tbf_008.huf
-- tof_008.huf
-- tof_010.huf
-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                compression.COMPRESS/DECOMPRESS TEST CASES                                 --
-------------------------------------------------------------------------------------------------------
tc = TestCase( "compression.compress/compression.decompress" )

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress1                                                                   --
--  Descricao:  Compactar e descompactar arquivo com sequencia de hexadecimais [0 - 255]             --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress1( )

    local return_value = true
    local compression_input    = "/tif_001.dat"
    local compression_output   = "/tof_001.huf"
    local decompression_input  = "/tof_001.huf"
    local decompression_output = "/tdf_001.dat"

    --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( compression_input ), string.format( "Dependencia: %s nao encontrado", compression_input ), true )

     --comprime o arquivo de teste
    return_value = compression.compress( compression_input, compression_output )

    --verifica o retorno da funcao de compressao
    assert_true( return_value, "Test_compression_compress_decompress1. Compactar arquivo com hexadecimais [0 - 255]. Retorno incorreto", true )

    --verifica se o arquivo comprimido foi criado
    assert_true( os.exists( compression_output ), "Test_compression_compress_decompress1. Compactar arquivo com hexadecimais [0 - 255]. Output comprimido nao foi criado", true )

    --descomprime o arquivo comprimido gerado
    return_value = compression.decompress( decompression_input, decompression_output )

    --apaga o arquivo comprimido, pois a partir daqui nao sera mais necessario
    os.remove( compression_output )

    --verifica o retorno da funcao de descompressao
    assert_true( return_value, "Test_compression_compress_decompress1. Descompactar arquivo com hexadecimais [0 - 255]. Retorno incorreto", true )

    --verifica se o arquivo descomprimido foi criado
    assert_true( os.exists( decompression_output ), "Test_compression_compress_decompress1. Descompactar arquivo com hexadecimais [0 - 255]. Output descomprimido nao foi criado", true )

    --compara o tamanho do arquivo original com o arquivo descomprimido gerado(devem ser iguais)
    if( not assert_equal( ( os.filesize( compression_input ) ), ( os.filesize( decompression_output ) ), "Test_compression_compress_decompress1. Descompactar arquivo com hexadecimais [0 - 255]. Tamanho do output descomprimido incorreto" ) ) then
        os.remove( decompression_output )
        return
    end

    --abre o arquivo original e o descomprimido gerado para comparar seus conteudos(devem ser iguais)
    compression_input_handle    = io.open( compression_input )
    decompression_output_handle = io.open( decompression_output )

    --caso nao seja possivel abrir o handle do arquivo original, fecha o handle do arquivo descomprimido se foi criado
    if( not assert_not_nil( compression_input_handle, string.format( "Dependencia: %s nao pode ser aberto", compression_input ) ) ) then
        if( decompression_output_handle ~= nil ) then
            decompression_output_handle:close( )
        end
        os.remove( decompression_output )
        return
    end

    --caso nao seja possivel abrir o handle do arquivo descomprimido, fecha o handle do arquivo original aberto acima
    if( not assert_not_nil( decompression_output_handle, string.format( "Dependencia: %s nao pode ser aberto", decompression_output ) ) ) then
        compression_input_handle:close( )
        os.remove( decompression_output )
        return
    end

    compression_input_buffer = compression_input_handle:read( "*a" )
    decompression_output_buffer = decompression_output_handle:read( "*a" )

    compression_input_handle:close( )
    decompression_output_handle:close( )
    
     --apaga os arquivos gerados durante o teste
    os.remove( decompression_output )

    assert_equal( compression_input_buffer, decompression_output_buffer, "Test_compression_compress_decompress1. Descompactar arquivo com hexadecimais [0 - 255]. Arquivo descomprimido diferente do original" )

end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress_decompress2                                                        --
--  Descricao:  Compactar e descompactar arquivo com texto                                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress2( )

    local return_value = true
    local compression_input    = "/tif_002.txt"
    local compression_output   = "/tof_002.huf"
    local decompression_input  = "/tof_002.huf"
    local decompression_output = "/tdf_002.txt"

    --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( compression_input ), string.format( "Dependencia: %s nao encontrado", compression_input ), true )

     --comprime o arquivo de teste
    return_value = compression.compress( compression_input, compression_output )

    --verifica o retorno da funcao de compressao
    assert_true( return_value, "Test_compression_compress_decompress2. Compactar arquivo com texto. Retorno incorreto", true )

    --verifica se o arquivo comprimido foi criado
    assert_true( os.exists( compression_output ), "Test_compression_compress_decompress2. Compactar arquivo com texto. Output comprimido nao foi criado", true )

    --descomprime o arquivo comprimido gerado
    return_value = compression.decompress( decompression_input, decompression_output )

    --apaga o arquivo comprimido, pois a partir daqui nao sera mais necessario
    os.remove( compression_output )

    --verifica o retorno da funcao de descompressao
    assert_true( return_value, "Test_compression_compress_decompress2. Descompactar arquivo com texto. Retorno incorreto", true )

    --verifica se o arquivo descomprimido foi criado
    assert_true( os.exists( decompression_output ), "Test_compression_compress_decompress2. Descompactar arquivo com texto. Output descomprimido nao foi criado", true )

    --compara o tamanho do arquivo original com o arquivo descomprimido gerado(devem ser iguais)
    if( not assert_equal( ( os.filesize( compression_input ) ), ( os.filesize( decompression_output ) ), "Test_compression_compress_decompress2. Descompactar arquivo com texto. Tamanho do output descomprimido incorreto" ) ) then
        os.remove( decompression_output )
        return
    end

    --abre o arquivo original e o descomprimido gerado para comparar seus conteudos(devem ser iguais)
    compression_input_handle    = io.open( compression_input )
    decompression_output_handle = io.open( decompression_output )

    --caso nao seja possivel abrir o handle do arquivo original, fecha o handle do arquivo descomprimido se foi criado
    if( not assert_not_nil( compression_input_handle, string.format( "Dependencia: %s nao pode ser aberto", compression_input ) ) ) then
        if( decompression_output_handle ~= nil ) then
            decompression_output_handle:close( )
        end
        os.remove( decompression_output )
        return
    end

    --caso nao seja possivel abrir o handle do arquivo descomprimido, fecha o handle do arquivo original aberto acima
    if( not assert_not_nil( decompression_output_handle, string.format( "Dependencia: %s nao pode ser aberto", decompression_output ) ) ) then
        compression_input_handle:close( )
        os.remove( decompression_output )
        return
    end

    compression_input_buffer = compression_input_handle:read( "*a" )
    decompression_output_buffer = decompression_output_handle:read( "*a" )

    compression_input_handle:close( )
    decompression_output_handle:close( )

     --apaga os arquivos gerados durante o teste
    os.remove( decompression_output )

    assert_equal( compression_input_buffer, decompression_output_buffer, "Test_compression_compress_decompress2. Descompactar arquivo com texto. Arquivo descomprimido diferente do original" )

end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress_decompress3                                                        --
--  Descricao:  Compactar e comparar com um arquivo comprimido de referencia                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress3( )

    local return_value = true
    local compression_input    = "/tif_003.txt"
    local compression_output   = "/tof_003.huf"
    local reference_file  = "/tbf_003.huf"

    --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( compression_input ), string.format( "Dependencia: %s nao encontrado", compression_input ), true )

    --verifica se o arquivo de referencia existe
    assert_true( os.exists( reference_file ), string.format( "Dependencia: %s nao encontrado", reference_file ), true )

     --comprime o arquivo de teste
    return_value = compression.compress( compression_input, compression_output )

    --verifica o retorno da funcao de compressao
    assert_true( return_value, "Test_compression_compress_decompress3. Compactar arquivo e comparar com a referencia. Retorno incorreto", true )

    --verifica se o arquivo comprimido foi criado
    assert_true( os.exists( compression_output ), "Test_compression_compress_decompress3. Compactar arquivo e comparar com a referencia. Output comprimido nao foi criado", true )

    --compara o tamanho do arquivo original com o arquivo de referencia(devem ser iguais)
    assert_equal( ( os.filesize( compression_output ) ), ( os.filesize( reference_file ) ), "Test_compression_compress_decompress3. Compactar arquivo e comparar com a referencia. Tamanho do arquivo comprimido incorreto", true )

    --abre o arquivo original e o referencia para comparar seus conteudos(devem ser iguais)
    local compression_output_handle    = io.open( compression_output )
    local reference_handle = io.open( reference_file )

    --caso nao seja possivel abrir o handle do arquivo comprimido, fecha o handle do arquivo descomprimido se foi criado
    if( not assert_not_nil( compression_output_handle, string.format( "Dependencia: %s nao pode ser aberto", compression_output ) ) ) then
        if( reference_handle ~= nil ) then
            reference_handle:close( )
        end
        os.remove( compression_output )
        return
    end

    --caso nao seja possivel abrir o handle do arquivo referencia, fecha o handle do arquivo comprimido
    if( not assert_not_nil( reference_handle, string.format( "Dependencia: %s nao pode ser aberto", reference_file ) ) ) then
        compression_output_handle:close( )
        os.remove( compression_output )
        return
    end

    local reference_buffer = reference_handle:read( "*a" )
    local compression_output_buffer = compression_output_handle:read( "*a" )

    reference_handle:close( )
    compression_output_handle:close( )

    --apaga os arquivos gerados durante o teste
    os.remove( compression_output )

    assert_equal( reference_buffer, compression_output_buffer, "Test_compression_compress_decompress3. Compactar arquivo e comparar com a referencia. Arquivo comprimido incorreto" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress_decompress4                                                        --
--  Descricao:  Compactar e comparar com um arquivo comprimido de referencia                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress4( )

    local return_value = true
    local decompression_input    = "/tbf_004.huf"
    local decompression_output   = "/tdf_004.txt"
    local reference_file  = "/tif_004.txt"

    --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( decompression_input ), string.format( "Dependencia: %s nao encontrado", decompression_input ), true )

    --verifica se o arquivo de referencia existe
    assert_true( os.exists( reference_file ), string.format( "Dependencia: %s nao encontrado", reference_file ), true )

     --comprime o arquivo de teste
    return_value = compression.decompress( decompression_input, decompression_output )

    --verifica o retorno da funcao de descompressao
    assert_true( return_value, "Test_compression_compress_decompress4. Descompactar arquivo compactado por outro programa. Retorno incorreto", true )

    --verifica se o arquivo descomprimido foi criado
    assert_true( os.exists( decompression_output ), "Test_compression_compress_decompress4. Descompactar arquivo compactado por outro programa. Output descomprimido nao foi criado", true )

    --compara o tamanho do arquivo descomprimido com o arquivo de referencia(devem ser iguais)
    assert_equal( ( os.filesize( decompression_output ) ), ( os.filesize( reference_file ) ), "Test_compression_compress_decompress4. Descompactar arquivo compactado por outro programa. Tamanho do arquivo descomprimido incorreto", true )

    --abre o arquivo descomprimido e o referencia para comparar seus conteudos(devem ser iguais)
    local decompression_output_handle    = io.open( decompression_output )
    local reference_handle = io.open( reference_file )

    --caso nao seja possivel abrir o handle do arquivo descomprimido, fecha o handle do arquivo referencia se foi criado
    if( not assert_not_nil( decompression_output_handle, string.format( "Dependencia: %s nao pode ser aberto", decompression_output ) ) ) then
        if( reference_handle ~= nil ) then
            reference_handle:close( )
        end
        os.remove( decompression_output )
        return
    end

    --caso nao seja possivel abrir o handle do arquivo referencia, fecha o handle do arquivo descomprimido
    if( not assert_not_nil( reference_handle, string.format( "Dependencia: %s nao pode ser aberto", reference_file ) ) ) then
        decompression_output_handle:close( )
        os.remove( decompression_output )
        return
    end

    local reference_buffer = reference_handle:read( "*a" )
    local decompression_output_buffer = decompression_output_handle:read( "*a" )

    reference_handle:close( )
    decompression_output_handle:close( )

    --apaga os arquivos gerados durante o teste
    os.remove( decompression_output )

    assert_equal( reference_buffer, decompression_output_buffer, "Test_compression_compress_decompress4. Descompactar arquivo compactado por outro programa. Arquivo descomprimido incorreto" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress5                                                                   --
--  Descricao:  Passa um input vazio para compressao                                                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress5( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local compression_output   = "/tof_005.huf"

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.compress( "", compression_output )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress5. Passa um input vazio para compressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress5. Passa um input vazio para compressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress5. Passa um input vazio para compressao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress6                                                                   --
--  Descricao:  Passa um output vazio para compressao                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress6( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local compression_input   = "/tif_006.txt"

     --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( compression_input ), string.format( "Dependencia: %s nao encontrado", compression_input ), true )

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.compress( compression_input, "" )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress6. Passa um output vazio para compressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress6. Passa um output vazio para compressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress6. Passa um output vazio para compressao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress7                                                                   --
--  Descricao:  Passa um input vazio para descompressao                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress7( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local decompression_output   = "/tof_007.huf"

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.decompress( "", decompression_output )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress5. Passa um input vazio para descompressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress5. Passa um input vazio para descompressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress5. Passa um input vazio para descompressao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress8                                                                   --
--  Descricao:  Passa um output vazio para descompressao                                             --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress8( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local decompression_input   = "/tof_008.huf"

     --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( decompression_input ), string.format( "Dependencia: %s nao encontrado", decompression_input ), true )

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.decompress( decompression_input, "" )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress8. Passa um output vazio para descompressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress8. Passa um output vazio para descompressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress8. Passa um output vazio para descompressao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress9                                                                   --
--  Descricao:  Passando o mesmo caminho para o input e o output na compressao                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress9( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local compression_input   = "/tif_009.txt"

     --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( compression_input ), string.format( "Dependencia: %s nao encontrado", compression_input ), true )

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.compress( compression_input, compression_input )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress9. Passando o mesmo caminho para o input e o output na compressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress9. Passando o mesmo caminho para o input e o output na compressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress9. Passando o mesmo caminho para o input e o output na compressao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_compression_compress10                                                                  --
--  Descricao:  Passando o mesmo caminho para o input e o output na descompressao                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc.test_compression_compress_decompress10( )

    local return_value = true
    local error_message = ""
    local error_code = 0
    local decompression_input   = "/tof_010.huf"

     --verifica se o arquivo a ser comprimido existe
    assert_true( os.exists( decompression_input ), string.format( "Dependencia: %s nao encontrado", decompression_input ), true )

     --comprime o arquivo de teste
    return_value, error_message, error_code = compression.decompress( decompression_input, decompression_input )

    --verifica o retorno da funcao de compressao
    assert_nil( return_value, "Test_compression_compress_decompress10. Passando o mesmo caminho para o input e o output na descompressao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "Test_compression_compress_decompress10. Passando o mesmo caminho para o input e o output na descompressao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "Test_compression_compress_decompress10. Passando o mesmo caminho para o input e o output na descompressao. Mensagem de erro incorreta" )
end

lunit.run( )