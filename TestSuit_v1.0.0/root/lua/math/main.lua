---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios do math                                     --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         06/02/2006  Cria��o                                          --
-- Leandro              lmf         16/02/2006  Inclus�o de novos testes                         --
-- Jamerson Lima        jrfl        04/08/2006  Operacoes binarias                               --
-- Leandro              lmf         08/01/2007  Inclusao de novos testes (19096)                 --
-- Henrique Ferreira    hsf         13/04/2007  CR23373 testes das opera��es and, or e not       --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

local numRandomTestes = 20

-- ****************** TESTE DE ABS ****************** --

-- Testes para o abs (5 teste)--
tc = TestCase("math.abs")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_abs1                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.abs. Teste com um float positivo      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do abs (1/5)--
function tc.test_abs1()
    local ret = math.abs(3.14)

    assert_equal(3.14, ret, "Erro no teste do abs com float positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_abs2                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.abs. Teste com um float negativo      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do abs (2/5)--
function tc.test_abs2()
    local ret = math.abs(-18.31)

    assert_equal(18.31, ret, "Erro no teste do abs com float negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_abs3                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.abs. Teste com um inteiro positivo    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do abs (3/5)--
function tc.test_abs3()
    local ret = math.abs(14)

    assert_equal(14, ret, "Erro no teste do abs com inteiro positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_abs4                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.abs. Teste com um inteiro negativo    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do abs (4/5)--
function tc.test_abs4()
    local ret = math.abs(-10)

    assert_equal(10, ret, "Erro no teste do abs com inteiro negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_abs5                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.abs. Teste com '0'                    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do abs (5/5)--
function tc.test_abs5()
    local ret = math.abs(0)

    assert_equal(0, ret, "Erro no teste do abs com '0'")
end


-- ****************** TESTE DE CEIL ****************** --

-- Testes para o ceil (5 teste)--
tc2 = TestCase("math.ceil")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ceil1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.ceil. Teste com um float positivo     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do ceil (1/5)--
function tc2.test_ceil1()
    local ret = math.ceil(3.14)

    assert_equal(4, ret, "Erro no teste do ceil com float positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ceil2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.ceil. Teste com um float negativo     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do ceil (2/5)--
function tc2.test_ceil2()
    local ret = math.ceil(-18.31)

    assert_equal(-18, ret, "Erro no teste do ceil com float negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ceil3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.ceil. Teste com um inteiro positivo   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do ceil (3/5)--
function tc2.test_ceil3()
    local ret = math.ceil(14)

    assert_equal(14, ret, "Erro no teste do ceil com inteiro positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ceil4                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.ceil. Teste com um inteiro negativo   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do ceil (4/5)--
function tc2.test_ceil4()
    local ret = math.ceil(-10)

    assert_equal(-10, ret, "Erro no teste do ceil com inteiro negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ceil5                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.ceil. Teste com '0'                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do ceil (5/5)--
function tc2.test_ceil5()
    local ret = math.ceil(0)

    assert_equal(0, ret, "Erro no teste do ceil com '0'")
end

-- ****************** TESTE DE FLOOR ****************** --

-- Testes para o floor (5 teste)--
tc3 = TestCase("math.floor")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_floor1                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.floor. Teste com um float positivo    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do floor (1/5)--
function tc3.test_floor1()
    local ret = math.floor(3.14)

    assert_equal(3, ret, "Erro no teste do floor com float positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_floor2                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.floor. Teste com um float negativo    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do floor (2/5)--
function tc3.test_floor2()
    local ret = math.floor(-18.31)

    assert_equal(-19, ret, "Erro no teste do floor com float negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_floor3                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.floor. Teste com um inteiro positivo  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do floor (3/5)--
function tc3.test_floor3()
    local ret = math.floor(14)

    assert_equal(14, ret, "Erro no teste do floor com inteiro positivo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_floor4                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.floor. Teste com um inteiro negativo  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do floor (4/5)--
function tc3.test_floor4()
    local ret = math.floor(-10)

    assert_equal(-10, ret, "Erro no teste do floor com inteiro negativo")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_floor5                                                                        --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.floor. Teste com '0'                  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do floor (5/5)--
function tc3.test_floor5()
    local ret = math.floor(0)

    assert_equal(0, ret, "Erro no teste do floor com '0'")
end


-- ****************** TESTE DE MAX ****************** --

-- Testes para o max (11 teste)--
tc4 = TestCase("math.max")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max1                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros positivos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (1/11)--
function tc4.test_max1()
    local ret = math.max(3,4,7,1,18,12,13,17)

    assert_equal(18, ret, "Erro no teste do max com inteiros positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max2                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros negativos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (2/11)--
function tc4.test_max2()
    local ret = math.max(-32,-48,-13,-10,-39,-12,-33)

    assert_equal(-10, ret, "Erro no teste do max com inteiros negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max3                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (3/11)--
function tc4.test_max3()
    local ret = math.max(-32,8,-13,11,-39,12,-33)

    assert_equal(12, ret, "Erro no teste do max com inteiros")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max4                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com floats positivos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (4/11)--
function tc4.test_max4()
    local ret = math.max(1.32, 3.33, 0.83, 1.1, 2.127)

    assert_equal(3.33, ret, "Erro no teste do max com floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max5                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com floats negativos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (5/11)--
function tc4.test_max5()
    local ret = math.max(-1.47, -5.65, -0.347, -8.99, -6.78)

    assert_equal(-0.347, ret, "Erro no teste do max com floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max6                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com floats                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (6/11)--
function tc4.test_max6()
    local ret = math.max(-1.34, 3.33, -9.99, 0.81, -1.48, 2.56)

    assert_equal(3.33, ret, "Erro no teste do max com floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max7                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros e floats      --
--            positivos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (7/11)--
function tc4.test_max7()
    local ret = math.max(1, 3.33, 10, 0.81, 7, 2.56)

    assert_equal(10, ret, "Erro no teste do max com inteiros e floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max8                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros e floats      --
--            negativos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (8/11)--
function tc4.test_max8()
    local ret = math.max(-1.34, -5, -9.99, -1, -0.48, -2)

    assert_equal(-0.48, ret, "Erro no teste do max com inteiros e floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max9                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste com inteiros e floats      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (9/11)--
function tc4.test_max9()
    local ret = math.max(3.38, -5, -9.99, 1, -0.48, 21, 9.543)

    assert_equal(21, ret, "Erro no teste do max com inteiros e floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max10                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste de sequ�ncias  de inteiros --
--            com '0'                                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (10/11)--
function tc4.test_max10()
    local ret = math.max(-32,8,0,-13,11,-39,0,12,-33)

    assert_equal(12, ret, "Erro no teste do max com sequencias de inteiros com '0'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_max11                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.max. Teste de sequ�ncias de floats    --
--            com '0'                                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do max (11/11)--
function tc4.test_max11()
    local ret = math.max(-1.34, 3.33, 0, -9.99, 0.81, 0, -1.48, 2.56)

    assert_equal(3.33, ret, "Erro no teste do max com sequencias de floats com '0'")
end


-- ****************** TESTE DE MIN ****************** --

-- Testes para o min (11 teste)--
tc5 = TestCase("math.min")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min1                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros positivos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (1/11)--
function tc5.test_min1()
    local ret = math.min(3,4,7,1,18,12,13,17)

    assert_equal(1, ret, "Erro no teste do min com inteiros positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min2                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros negativos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (2/11)--
function tc5.test_min2()
    local ret = math.min(-32,-48,-13,-10,-39,-12,-33)

    assert_equal(-48, ret, "Erro no teste do min com inteiros negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min3                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (3/11)--
function tc5.test_min3()
    local ret = math.min(-32,8,-13,11,-39,12,-33)

    assert_equal(-39, ret, "Erro no teste do min com inteiros")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min4                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com floats positivos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (4/11)--
function tc5.test_min4()
    local ret = math.min(1.32, 3.33, 0.83, 1.1, 2.127)

    assert_equal(0.83, ret, "Erro no teste do min com floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min5                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com floats negativos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (5/11)--
function tc5.test_min5()
    local ret = math.min(-1.47, -5.65, -0.347, -8.99, -6.78)

    assert_equal(-8.99, ret, "Erro no teste do min com floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min6                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com floats                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (6/11)--
function tc5.test_min6()
    local ret = math.min(-1.34, 3.33, -9.99, 0.81, -1.48, 2.56)

    assert_equal(-9.99, ret, "Erro no teste do min com floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min7                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros e floats      --
--            positivos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (7/11)--
function tc5.test_min7()
    local ret = math.min(1, 3.33, 10, 0.81, 7, 2.56)

    assert_equal(0.81, ret, "Erro no teste do min com inteiros e floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min8                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros e floats      --
--            negativos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (8/11)--
function tc5.test_min8()
    local ret = math.min(-1.34, -5, -9.99, -1, -0.48, -2)

    assert_equal(-9.99, ret, "Erro no teste do min com inteiros e floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min9                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com inteiros e floats      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (9/11)--
function tc5.test_min9()
    local ret = math.min(3.38, -5, -9.99, 1, -0.48, 21, 9.543)

    assert_equal(-9.99, ret, "Erro no teste do min com inteiros e floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min10                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com sequ�ncias de inteiros --
--            com '0'                                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (10/11)--
function tc5.test_min10()
    local ret = math.min(-32,8,0,-13,11,0,-39,12,-33)

    assert_equal(-39, ret, "Erro no teste do min com sequencia de inteiros com '0'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_min11                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.min. Teste com sequ�ncia de floats    --
--            com '0'                                                                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do min (11/11)--
function tc5.test_min11()
    local ret = math.min(-1.34, 3.33, 0, -9.99, 0.81, 0, -1.48, 2.56)

    assert_equal(-9.99, ret, "Erro no teste do min com sequencia de floats com '0'")
end


-- ****************** TESTE DE MOD ****************** --

-- Testes para o mod (11 teste)--
tc5 = TestCase("math.mod")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod1                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros positivos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (1/11)--
function tc5.test_mod1()
    local ret = math.mod(18,12)

    assert_equal(6, ret, "Erro no teste do mod com inteiros positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod2                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros negativos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (2/11)--
function tc5.test_mod2()
    local ret = math.mod(-48, -32)

    assert_equal(-16, ret, "Erro no teste do mod com inteiros negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod3                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (3/11)--
function tc5.test_mod3()
    local ret = math.mod(-32,11)

    assert_equal(-10, ret, "Erro no teste do mod com inteiros")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod4                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com floats positivos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (4/11)--
function tc5.test_mod4()
    local ret = math.mod(4.68, 0.83)

    ret = ret - 0.53

    assert_true(ret < 0.0001, "Erro no teste do mod com floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod5                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com floats negativos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (5/11)--
function tc5.test_mod5()
    local ret = math.mod(-8.64, -2.38)

    ret = ret + 1.5

    assert_true(ret > -0.0001, "Erro no teste do mod com floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod6                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com floats                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (6/11)--
function tc5.test_mod6()
    local ret = math.mod(-9.99, 2.56)

    ret = ret + 2.31

    assert_true(ret > -0.0001, "Erro no teste do mod com floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod7                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros e floats      --
--            positivos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (7/11)--
function tc5.test_mod7()
    local ret = math.mod(7, 2.56)

    ret = ret - 1.88

    assert_true(ret < 0.0001, "Erro no teste do mod com inteiros e floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod8                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros e floats      --
--            negativos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (8/11)--
function tc5.test_mod8()
    local ret = math.mod(-8.34, -5)

    ret = ret + 3.34

    assert_true(ret > -0.0001, "Erro no teste do mod com inteiros e floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod9                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste com inteiros e floats      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (9/11)--
function tc5.test_mod9()
    local ret = math.mod(-21, 7.54)

    ret = ret + 5.92

    assert_true(ret > -0.0001, "Erro no teste do mod com inteiros e floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod10                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste de inteiros com '0'        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (10/11)--
function tc5.test_mod10()
    local ret = math.mod(0, -7)

    assert_equal(0, ret, "Erro no teste do mod de inteiros com '0'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_mod11                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.mod. Teste de floats com '0'          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do mod (11/11)--
function tc5.test_mod11()
    local ret = math.mod(0, 1.4598)

    assert_equal(0, ret, "Erro no teste do mod de floats com '0'")
end


-- ****************** TESTE DE POW ****************** --

-- Testes para o pow (11 teste)--
tc5 = TestCase("math.pow")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow1                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros positivos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (1/11)--
function tc5.test_pow1()
    local ret = math.pow(18,3)

    assert_equal(5832, ret, "Erro no teste do pow com inteiros positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow2                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros negativos     --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (2/11)--
function tc5.test_pow2()
    local ret = math.pow(-2, -2)

    assert_equal(0.25, ret, "Erro no teste do pow com inteiros negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow3                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros               --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (3/11)--
function tc5.test_pow3()
    local ret = math.pow(-12,3)

    assert_equal(-1728, ret, "Erro no teste do pow com inteiros")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow4                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com floats positivos       --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (4/11)--
function tc5.test_pow4()
    local ret = math.pow(4.68, 0.83)

    ret = ret - 3.6

    assert_true(ret < 0.001, "Erro no teste do pow com floats positivos")
end


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow5                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros e floats      --
--            positivos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (5/11)--
function tc5.test_pow5()
    local ret = math.pow(7, 2.56)

    ret = ret - 145.6971

    assert_true(ret < 0.001, "Erro no teste do pow com inteiros e floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow6                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros e floats      --
--            negativos                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (6/11)--
function tc5.test_pow6()
    local ret = math.pow(-8.34, -5)

    ret = ret - 0.5569

    assert_true(ret < 0.001, "Erro no teste do pow com inteiros e floats negativos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow7                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiros e floats      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (7/11)--
function tc5.test_pow7()
    local ret = math.pow(2,-1.54)

    ret = ret - 0.3438

    assert_true(ret < 0.001, "Erro no teste do pow com inteiros e floats")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow8                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com inteiro elevado a '0'  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (8/11)--
function tc5.test_pow8()
    local ret = math.pow(2,0)

    assert_equal(1, ret, "Erro no teste do pow com inteiro elevado a '0'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow9                                                                          --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com float elevado a '0'    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (9/11)--
function tc5.test_pow9()
    local ret = math.pow(1.29,0)

    assert_equal(1, ret, "Erro no teste do pow com float elevado a '0'")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow10                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com '0' elevado a inteiro  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (10/11)--
function tc5.test_pow10()
    local ret = math.pow(0, 21)

    assert_equal(0, ret, "Erro no teste do pow com '0' elevado a inteiro")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_pow11                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.pow. Teste com '0' elevado a inteiro  --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do pow (11/11)--
function tc5.test_pow11()
    local ret = math.pow(0, 2.698)

    assert_equal(0, ret, "Erro no teste do pow com '0' elevado a float")
end

-- ****************** TESTE DE SQRT ****************** --

-- Testes para o sqrt (3 teste)--
tc5 = TestCase("math.sqrt")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_sqrt1                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.sqrt. Teste com inteiros positivos    --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do sqrt (1/3)--
function tc5.test_sqrt1()
    local ret = math.sqrt(18)

    ret = ret - 4.2426

    assert_true(ret < 0.001, "Erro no teste do sqrt com inteiros positivos")
end


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_sqrt2                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.sqrt. Teste com floats positivos      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do sqrt (2/3)--
function tc5.test_sqrt2()
    local ret = math.sqrt(4.68)

    ret = ret - 2.1633

    assert_true(ret < 0.001, "Erro no teste do sqrt com floats positivos")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_sqrt3                                                                         --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.sqrt. Teste com '0'                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do sqrt (3/3)--
function tc5.test_sqrt3()
    local ret = math.sqrt(0)

    assert_equal(0, ret, "Erro no teste do sqrt com '0'")
end


-- ****************** TESTE DE RANDOM ****************** --

-- Testes para o random (3 teste)--
tc6 = TestCase("math.random")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_random1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.random. Teste sem parametros para     --
--            validar o intervalo [0,1)                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do random (1/3)--
function tc6.test_random1()
    local i = 0
    repeat
        ret = math.random()

        assert_true((ret >= 0 and ret < 1), "Erro no teste do random intervalo [0,1)")

        i = i + 1
    until i == numRandomTestes
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_random2                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.random. Teste com 1 parametro para    --
--            validar o intervalo [1,n]                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do random (2/3)--
function tc6.test_random2()
    local i = 0
    repeat
        ret = math.random(2)

        assert_true((ret >= 1 and ret <= 2), "Erro no teste do random intervalo [1,2]")

        i = i + 1
    until i == numRandomTestes
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_random3                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.random. Teste com 2 parametros para   --
--            validar o intervalo [n1,n2]                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste do random (3/3)--
function tc6.test_random3()
    local i = 0
    repeat
        ret = math.random(5,7)

        assert_true((ret >= 5 and ret <= 7), "Erro no teste do random no intervalo [5,7]")

        i = i + 1
    until i == numRandomTestes
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteand1                                                                    --
--  Descricao: Caso de sucesso. Realiza o and de dois inteiros                                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_bitwiseand1()

    local c = math.bitwiseand( 14, 2 )

    assert_equal( 2, c, "test_math_byteand1. Caso de sucesso. Resultado do 'and' incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteor1                                                                     --
--  Descricao: Caso de sucesso. Realiza o or de dois inteiros                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_bitwiseor1()

    local c = math.bitwiseor( 3, 21 )

    assert_equal( 23, c, "test_math_byteor1. Caso de sucesso. Resultado do 'or' incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftleft1                                                              --
--  Descricao: Caso de sucesso. Realiza o shiftleft de uma posicao em um inteiro                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftleft1()

    local result = math.shiftl( 2, 1 )

    assert_equal( 4, result, "test_math_byteshiftleft1. Caso de sucesso. shiftleft de 1 posicao em um byte")
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftleft2                                                              --
--  Descricao: Caso de sucesso. Realiza o shiftleft de tres posicoes em um byte                      --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftleft2()

    local result = math.shiftl( 5, 3 )

    assert_equal( 40, result, "test_math_byteshiftleft2. Caso de sucesso. shiftleft de 3 posicoes em um byte. ")
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftleft3                                                              --
--  Descricao: Caso de sucesso. Passa uma quantidade negativa                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftleft3()

    local result = math.shiftl( 8, -1 )

    assert_equal( 0, result, "test_math_byteshiftleft3. Caso de sucesso. shiftleft de -1 em um byte. ")
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftright1                                                             --
--  Descricao: Caso de sucesso. Realiza o shiftright de uma posicao em um byte                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftright1()

    local result = math.shiftr( 8, 1 )

    assert_equal( 4, result, "test_math_byteshiftright1. Caso de sucesso. shiftright de 1 posicao em um byte.")
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftright2                                                             --
--  Descricao: Caso de sucesso. Realiza o shiftright de tres posicoes em um byte                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftright2()

    local result = math.shiftr( 64, 3 )

    assert_equal( 8, result, "test_math_byteshiftright2. Caso de sucesso. shiftright de 3 posicoes em um byte.")
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_math_byteshiftright3                                                             --
--  Descricao: Caso de sucesso. Passa uma quantidade negativa                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_math_byteshiftright3()

    local result = math.shiftr( 8, -1 )

    assert_equal( 0, result, "test_math_byteshiftright3. Caso de sucesso. shiftright de -1 em um byte.")
end

-- ****************** TESTE DE BINARYNOT ****************** --

-- Testes para o binarynot (1 teste)--
tc_binarynot = TestCase("math.binarynot")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_binarynot1                                                                    --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o math.binarynot.                            --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------
-- Teste do binarynot (1/1)--
function tc_binarynot.test_bitwisenot1()
    local result = math.bitwisenot( -1 )

    assert_equal( 0, result, "test_binarynot1. Caso de sucesso. not de -1.")
end

lunit.run()
