-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              comm/main.lua                                                       --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                                                                                        --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      27/04/2006        Criacao                             --
--  Jamerson Lima        jrfl          NA      14/06/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP006) --
--  Jamerson Lima        jrfl         14838    26/07/2006        Correcao do teste para rodar no Hype--
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

phone_number = ""
pabx = ""


environment = ""

-------------------------------------------------------------------------------------------------------
--                                   Funcoes                                                         --
-------------------------------------------------------------------------------------------------------

function choose_env()

    if( environment:len() == 0 ) then
        local m = ui.menu( "Plataforma", { "Ingenico", "Hypercom", "PC" } )

        if( m == nil ) then
            error( "Menu nao pode ser criado" )
        end

        m:show()

        if( m:accepted() == 1 ) then --ingenico
            environment = "ingenico"
        elseif( m:accepted() == 2 ) then  -- hypercom
            environment = "hypercom"
        else --pc
            environment = "pc"
        end

        ui.destroy( m )
    end
end

-------------------------------------------------------------------------------------------------------
--                                   COMM TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "comm" )

function tc:setup()

    choose_env()

    if( phone_number:len() == 0 ) then

        if( environment == "pc" ) then
            phone_number = "127000000001"
            pabx = "80"
        else
            display.clear()

            local tf = ui.textfield("CONFIGURACAO", "Digite o fone:", 30, 1 )
            tf:show()
            phone_number = tf:text()

            ui.destroy( tf )
        end
    end
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_load_config_save1                                                                --
--  Descricao: Caso de sucesso. Carrega a configuracao padrao, altera uma propriedade e salva em     --
--             um arquivo                                                                            --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_load_config_save1()

    local result          = true
    local error_message   = ""
    local error_code      = 0
    local config_filename = "comm1.cfg"
    local config_key      = "baud_rate"
    local default_config_value    = ""
    local config_value    = "1200"

    if( environment == "ingenico" ) then
        default_config_value    = "2400"
    end

    comm.disconnect()

     --carrega a configuracao padrao
    result = comm.loadconfig()

    assert_true( result, "test_load_config_save1. Caso de sucesso. Carregando configuracao padrao. Retorno incorreto", true )

    assert_equal( default_config_value, comm.config( config_key ), "test_load_config_save1. Caso de sucesso. Carregando configuracao padrao. Propriedade incorreta", true )

    --altera configuracao de uma chave
    result = comm.config( config_key, config_value )

    assert_equal( config_value, result, "test_load_config_save1. Caso de sucesso. Alterando propriedade. Retorno incorreto", true )

    assert_equal( config_value, comm.config( config_key ), "test_load_config_save1. Caso de sucesso. Alterando propriedade. Propriedade nao foi alterada", true )

    --chama funcao testada
    result = comm.saveconfig( config_filename )

    assert_true( result, "test_load_config_save1. Caso de sucesso. Salvando configuracao. Retorno incorreto", true )

    assert_true( os.exists( config_filename ), "test_load_config_save1. Caso de sucesso. Salvando configuracao. Arquivo nao foi criado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_load_config_save2                                                                --
--  Descricao: Caso de sucesso. Carrega um arquivo de configuracao, altera uma propriedade e salva   --
--             em um arquivo                                                                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_load_config_save2()

    local result          = true
    local error_message   = ""
    local error_code      = 0
    local load_config_filename = "valido"
    local config_filename = "comm2.cfg"
    local config_key      = "baud_rate"
    local default_config_value    = "57600"
    local config_value    = "1200"
    
    comm.disconnect()

    assert_true( os.exists( load_config_filename ), string.format( "Dependencia: %s nao encontrado", load_config_filename, true ) )

     --carrega a configuracao padrao
    result = comm.loadconfig( load_config_filename )

    assert_true( result, "test_load_config_save2. Caso de sucesso. Carregando arquivo de configuracao. Retorno incorreto", true )

    assert_equal( default_config_value, comm.config( config_key ), "test_load_config_save2. Caso de sucesso. Carregando arquivo de configuracao. Propriedade incorreta", true )

    --altera configuracao de uma chave
    result = comm.config( config_key, config_value )

    assert_equal( config_value, result, "test_load_config_save2. Caso de sucesso. Alterando propriedade. Retorno incorreto", true )

    assert_equal( config_value, comm.config( config_key ), "test_load_config_save2. Caso de sucesso. Alterando propriedade. Propriedade nao foi alterada", true )

    --chama funcao testada
    result = comm.saveconfig( config_filename )

    assert_true( result, "test_load_config_save2. Caso de sucesso. Salvando configuracao. Retorno incorreto", true )

    assert_true( os.exists( config_filename ), "test_load_config_save2. Caso de sucesso. Salvando configuracao. Arquivo nao foi criado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_load_config_save3                                                                --
--  Descricao: Caso de sucesso. Carrega um arquivo de configuracao vazio, e verifica se deu certo    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_load_config_save3()

    local result          = true
    local load_config_filename = "vazio"
    local config_key = "baud_rate"
    local config_value = ""

    if( environment == "ingenico" ) then
        config_value    = "2400"
    end
    
    comm.disconnect()

    assert_true( os.exists( load_config_filename ), string.format( "Dependencia: %s nao encontrado", load_config_filename, true ) )

     --carrega a configuracao padrao
    result = comm.loadconfig( load_config_filename )

    assert_true( result, "test_load_config_save3. Caso de sucesso. Carregando arquivo vazio. Retorno incorreto", true )

    assert_equal( config_value, comm.config( config_key ), "test_load_config_save3. Caso de sucesso. Carregando arquivo vazio. Propriedade default nao foi carregada" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_load_config_save4                                                                --
--  Descricao: Caso de sucesso do save. salva configuracao default                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_load_config_save4()

    local result          = true
    local config_filename = "comm4.cfg"
    
    comm.disconnect()

     --carrega a configuracao padrao
    result = comm.loadconfig()

    assert_true( result, "Dependencia: Conf. nao carregada", true )

    --chama funcao testada
    result = comm.saveconfig( config_filename )

    assert_true( result, "test_load_config_save4. Caso de sucesso. Salvando conf. default. Retorno incorreto", true )

    assert_true( os.exists( config_filename ), "test_load_config_save4. Caso de sucesso. Salvando conf. default. Arquivo nao foi criado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_connect_check_disconnect1                                                        --
--  Descricao: Caso de sucesso. Conecta, verifica o estado da conexao, desconecta e verifica         --
--  se o estado esta desconectado                                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_connect_check_disconnect1()

    local pressed_key = KEY_ZERO
    
    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22bis" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    display.clear()
    display.print( "Aperte alguma tecla para discar..." )
    keyboard.getkeystroke( -1 )

    local result = comm.connect( phone_number )

    assert_true( result, "test_connect_check_disconnect1. Caso de sucesso. Conectando com um numero valido. Retorno incorreto", true )

    while pressed_key ~= KEY_CANCEL do
        display.clear()
        display.print( "Enter=Recarr.", 0, 0 )
        display.print( "Cancel=Sair", 1, 0 )
        display.print( "Status:", 2, 0 )
        display.print( tostring( comm.check() ), 3, 0 )
        pressed_key = keyboard.getkeystroke( -1 )
    end

    assert_equal( "connected", comm.check(), "test_connect_check_disconnect1. Caso de sucesso. Conectando com um numero valido. Nao conectado", true )

    result = comm.disconnect()

    assert_true( result, "test_connect_check_disconnect1. Caso de sucesso. desconectando. Retorno incorreto", true )

    assert_equal( "disconnected", comm.check(), "test_connect_check_disconnect1. Caso de sucesso. desconectando. Nao desconectado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_connect_check_disconnect2                                                        --
--  Descricao: tenta conectar passando um numero invalido                                            --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_connect_check_disconnect2()

    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22bis" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    local result, error_message, error_code = comm.connect( "0000" )

    assert_nil( result, "test_connect_check_disconnect2. Conectando com um numero invalido. Retorno incorreto", true )

    assert_equal( TST_ERR_NOANSWER, error_code, "test_connect_check_disconnect2. Conectando com um numero invalido. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_NOANSWER ], error_message, "test_connect_check_disconnect2. Conectando com um numero invalido. Mensagem de erro incorreta" )

    while (comm.check() == "waiting") do
          management.sleep(500)
    end

    assert_equal( "disconnected", comm.check(), "test_connect_check_disconnect2. Conectando com um numero invalido. Nao conectado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_connect_check_disconnect3                                                        --
--  Descricao: Passando string vazia                                                                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_connect_check_disconnect3()

    local error_message = ""
    local error_code = 0

    comm.disconnect()

    local result = comm.loadconfig()

    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22bis" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    local result, error_message, error_code = comm.connect( "" )

    assert_nil( result, "test_connect_check_disconnect3. Passando string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_connect_check_disconnect3. Passando string vazia. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_connect_check_disconnect3. Passando string vazia. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_connect_check_disconnect4                                                        --
--  Descricao: Chama disconnect sem conexao                                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_connect_check_disconnect4()

    local error_message = ""
    local error_code = 0
    
    comm.disconnect()

    local result = comm.loadconfig()
    
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    local result, error_message, error_code = comm.disconnect()

    assert_nil( result, "test_connect_check_disconnect4. Chama disconnect sem conexao. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDSTATE, error_code, "test_connect_check_disconnect4. Chama disconnect sem conexao. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDSTATE ], error_message, "test_connect_check_disconnect4. Chama disconnect sem conexao. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_buffersize1                                                                      --
--  Descricao: Pega tamanho do buffer padrao                                                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_buffersize1()

    local send_buffer    = 0
    local receive_buffer = 0
    
    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    send_buffer, receive_buffer = comm.buffersize()

    assert_equal( 1024, send_buffer, "test_buffersize1. Pega tamanho do buffer padrao. Tamanho do buffer de envio incorreto", true )

    assert_equal( 1024, receive_buffer, "test_buffersize1. Pega tamanho do buffer padrao. Tamanho do buffer de recepcao incorreto", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_buffersize2                                                                      --
--  Descricao: Pega tamanho do buffer apos alteracao                                                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_buffersize2()

    local result             = true
    local send_buffer        = 0
    local receive_buffer     = 0
    local receive_buffer_key = "rbuffsz"
    local send_buffer_key    = "sbuffsz"
    local new_value          = 1200
    
    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    comm.config( receive_buffer_key, tostring(new_value) )
    comm.config( send_buffer_key   , tostring(new_value) )

    send_buffer, receive_buffer = comm.buffersize()

    assert_equal( new_value, send_buffer, "test_buffersize2. Pega tamanho do buffer apos alteracao. Tamanho do buffer de envio incorreto", true )
    assert_equal( new_value, receive_buffer, "test_buffersize2. Pega tamanho do buffer apos alteracao. Tamanho do buffer de recepcao incorreto", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_send1                                                                            --
--  Descricao: Caso de sucesso. Estabelece uma conexao e envia dados por ela.                        --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_send1()

    local pressed_key  = KEY_ZERO
    local send_message = "mensagem de teste"
    
    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )
    
    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22bis" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    display.clear()
    display.print( "Aperte alguma tecla para discar..." )
    keyboard.getkeystroke( -1 )

    local result = comm.connect( phone_number )

    assert_true( result, "Dependencia: connect retorno 'false'", true )

    while pressed_key ~= KEY_CANCEL do
        display.clear()
        display.print( "Enter=Recarr.", 0, 0 )
        display.print( "Cancel=Sair", 1, 0 )
        display.print( "Status:", 2, 0 )
        display.print( tostring( comm.check() ), 3, 0 )
        pressed_key = keyboard.getkeystroke( -1 )
    end

    assert_equal( "connected", comm.check(), "Dependencia: Nao foi conectado", true )

    result = comm.send( send_message )

    if environment == "pc" then
        assert_equal( send_message:len(), result - 2, "test_send1. Caso de sucesso. Enviando dados. Retorno incorreto", true )
    else
        assert_equal( send_message:len(), result, "test_send1. Caso de sucesso. Enviando dados. Retorno incorreto", true )
    end

    result = comm.disconnect()

    assert_true( result, "test_send1. Caso de sucesso. desconectando. Retorno incorreto", true )

    display.clear()
    display.print( string.format( "'%s' foi recebida?", send_message ), 0, 0 )

    local pressed_key = lerTecla()
    assert_equal( KEY_ONE, pressed_key, "test_send1. Caso de sucesso. Mensagem nao foi enviada corretamente", true )

    assert_equal( "disconnected", comm.check(), "test_send1. Caso de sucesso. desconectando. Nao desconectado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_receive1                                                                         --
--  Descricao: Caso de sucesso. Caso de sucesso. Estabelece uma conexao e recebe dados por ela.      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_receive1()

    local pressed_key  = KEY_ZERO
    local received_message = ""
    local message_length = 0
    local buffer_size = 10
    local timeout = 20000

    display.clear()
    display.print( "Teste de recebimento de msg. o POS deve receber 10 bytes para o teste ser bem sucedido. Aperte alguma tecla", 0, 0 )
    keyboard.getkeystroke( -1 )
    
    comm.disconnect()

    local result = comm.loadconfig()
    assert_true( result, "Dependencia: Conf. padrao nao foi carregado", true )

    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22bis" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    display.clear()
    display.print( "Aperte alguma tecla para discar..." )
    keyboard.getkeystroke( -1 )

    local result = comm.connect( phone_number )

    assert_true( result, "Dependencia: Nao conectou",  true )

    while pressed_key ~= KEY_CANCEL do
        display.clear()
        display.print( "Enter=Recarr.", 0, 0 )
        display.print( "Cancel=Sair", 1, 0 )
        display.print( "Status:", 2, 0 )
        display.print( tostring( comm.check() ), 3, 0 )
        pressed_key = keyboard.getkeystroke( -1 )
    end

    assert_equal( "connected", comm.check(), "Dependencia: Nao foi conectado", true )

    display.clear()
    display.print( "Envie dados ao POS" )

    local received_message, message_length = comm.receive( buffer_size, timeout )

    result = comm.disconnect()

    display.clear()
    display.print( "Msg recebida", 0, 0 )
    display.print( tostring( received_message ), 1, 0 )

    pressed_key = lerTecla()

    assert_equal( KEY_ONE, pressed_key, "test_receive1. Caso de sucesso. Erro recebendo mensagem", true )

    assert_true( result, "test_receive1. Caso de sucesso. desconectando. Retorno incorreto", true )

    assert_equal( "disconnected", comm.check(), "test_receive1. Caso de sucesso. desconectando. Nao desconectado" )
end

lunit.run( )