-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              iso/main.lua                                                        --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                    Testes de ISO                                                       --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      19/4/2006         Criacao                             --
--  Jamerson Lima        jrfl         9844     25/4/2006         Rework (HFLEX-TSTR-LUA-CODE-INSP003)--
--  Jamerson Lima        jrfl          NA      16/6/2006         Testes para typeid                  --
--  Jamerson Lima        jrfl          NA      19/6/2006         Correcao de erros                   --
--  Jamerson Lima        jrfl         14812    26/7/2006         Alterando o valor default de version--
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

-- Teste setando um bit de data(bit 07) com 02/18 16:29:30(MMDDhhmmss)
testeExterno1   =   "0000000000" ..
                    "1100" ..
                    "0200000000000000" ..
                    "0218162930"

-- Teste setando um bit bin�rio(bit 03) n..6 com '123'
testeExterno2   =   "0000000000" ..
                    "1100" ..
                    "2000000000000000" ..
                    "000123"

-- Teste setando um bit bin�rio(bit 02) 'LLVAR' n..19 com '123'
testeExterno3   = "0000000000" ..
                  "1100" ..
                  "4000000000000000" ..
                  "031230"

-- Teste setando um bit bin�rio(bit 31) 'LLVAR' ans..99 com 'a1b2@'
testeExterno4   = "0000000000" ..
                  "1100" ..
                  "0000000200000000" ..
                  "056131623240"

-- Teste setando um bit bin�rio(bit 35) 'LLVAR' z..37 com '0123456789:;<=>?'
testeExterno5   = "0000000000" ..
                  "1100" ..
                  "0000000020000000" ..
                  "160123456789ABCDEF"

-- Teste setando os bits 5, 3 e 7 para verificar se a API coloca os bits na ordem correta
testeExterno6   = "0000000000" ..
                  "1100" ..
                  "2A00000000000000" ..
                  "000456" .. -- bit 3
                  "000000000321" .. -- bit 5
                  "0406084020"  -- bit 7

-- Teste setando os bits 3, 7, 22, 31, 35, 37 e 52
testeExterno7   =   "0000000000" ..
                    "1100" ..
                    "2200040228001000" ..
                    "000123" .. -- bit 3
                    "0406155040" .. -- bit 7
                    "6131623263332020" .. -- bit 22
                    "20202020" ..
                    "0961312162324063" .. -- bit 31
                    "3323" ..
                    "160123456789ABCDEF" ..      -- bit 35
                    "6131623220202020" .. -- bit 37
                    "20202020" ..
                    "6162636465666768" -- bit 52

-------------------------------------------------------------------------------------------------------
--                                   Funcoes                                                         --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   ISO.CREATE/ISO.DESTROY TEST CASES                               --
-------------------------------------------------------------------------------------------------------

tc_iso_create_destroy = TestCase("iso.create/iso.destroy")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_create1                                                                      --
--  Descricao: Cria um handle, verifica se as propriedades do handle criado estao corretas, destroi  --
--             o handle e verifica tudo deu certo                                                    --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_create_destroy.test_iso_create_destroy1( )

    --chama a funcao testada
    local handle, error_message, error_code = iso.create( )

    --verifica se o retorno da funcao esta correta
    assert_not_nil( handle, string.format( "test_iso_create_destroy1. Caso de sucesso. Handle retornado nulo na criacao de handle [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --verifica as propriedades do handle retornado
    if( not assert_equal( ISO_CLASS_UNDEF, handle:class(), "test_iso_create_destroy1. Caso de sucesso. Handle com propriedade class incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( ISO_FNC_UNDEF, handle:fnc(), "test_iso_create_destroy1. Caso de sucesso. Handle com propriedade fnc incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( ISO_TRANSORIGN_UNDEF, handle:transorign(), "test_iso_create_destroy1. Caso de sucesso. Handle com propriedade 'transaction originator' incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( ISO_VER_1987, handle:version(), "test_iso_create_destroy1. Caso de sucesso. Handle com propriedade 'version' incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    --destroe o handle criado
    local result = iso.destroy( handle )

    --verifica se o retorno foi o esperado
    assert_true( result, "test_iso_create_destroy1. Caso de sucesso. Retorno incorreto na destruicao de handle", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_create_destroy2                                                              --
--  Descricao: Passa uma tabela(handle invalido) para a funcao destroy                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_create_destroy.test_iso_create_destroy2( )

     local handle = {}

    --tenta fechar o handle invalido
    local result  = pcall( function() iso.destroy( handle ) end )

    assert_false( result, "test_iso_create_destroy2. Retorno incorreto ao fechar handle invalido", true )
end

-------------------------------------------------------------------------------------------------------
--                                   ISO:TYPEID TEST CASES                                           --
-------------------------------------------------------------------------------------------------------

tc_iso_typeid = TestCase("iso:typeid")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_typeid1                                                                      --
--  Descricao: Cria um handle, altera a propriedade typeid do handle, depois verifica se a           --
--             propriedade foi corretamente alterada.                                                --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_typeid.test_iso_typeid1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade version
    local result = handle:typeid( "1234" )

    --verifica se a chamada da funcao foi bem sucedida
    if( not assert_equal( "1234", result, "test_iso_typeid1. Retorno incorreto no caso de sucesso passando parametro" ) ) then
        iso.destroy( handle )
        return
    end

    --pega o valor atual de version
    result = handle:typeid()

    iso.destroy( handle )

    --verifica se o valor atual de version eh igual ao valor passado logo acima
    assert_equal( "1234", result, "test_iso_version1. Retorno incorreto em caso de sucesso sem passar parametro", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_typeid2                                                                      --
--  Descricao: Cria um handle, altera a propriedade typeid do handle com um valor vazio              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_typeid.test_iso_typeid2( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade version
    local result, error_message, error_code = handle:typeid( "" )

    if( not assert_nil( result, "test_iso_typeid2. Passando um tipo vazio. Retorno incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDARG, error_code, "test_iso_typeid2. Passando um tipo vazio. Codigo de erro incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_iso_typeid2. Passando um tipo vazio. Mensagem de erro incorreta" ) ) then
        iso.destroy( handle )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_typeid3                                                                      --
--  Descricao: Cria um handle, altera a propriedade typeid do handle com um valor maior que 4        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_typeid.test_iso_typeid3( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade version
    local result, error_message, error_code = handle:typeid( "12345" )

    if( not assert_nil( result, "test_iso_typeid3. Passando um tipo > 4. Retorno incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDARG, error_code, "test_iso_typeid3. Passando um tipo > 4. Codigo de erro incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_iso_typeid3. Passando um tipo > 4. Mensagem de erro incorreta" ) ) then
        iso.destroy( handle )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_typeid4                                                                      --
--  Descricao: Cria um handle, altera a propriedade typeid do handle com um valor invalido           --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_typeid.test_iso_typeid4( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade version
    local result, error_message, error_code = handle:typeid( "AAAA" )

    if( not assert_nil( result, "test_iso_typeid4. Passando um tipo invalido. Retorno incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDARG, error_code, "test_iso_typeid4. Passando um tipo invalido. Codigo de erro incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_iso_typeid4. Passando um tipo invalido. Mensagem de erro incorreta" ) ) then
        iso.destroy( handle )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--                                   ISO:VERSION TEST CASES                                          --
-------------------------------------------------------------------------------------------------------

tc_iso_version = TestCase("iso:version")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_version1                                                                     --
--  Descricao: Cria um handle, altera a propriedade version do handle, depois verifica se a          --
--             propriedade foi corretamente alterada.                                                --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_version.test_iso_version1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade version
    local result = handle:version( ISO_VER_1993 )

    --verifica se a chamada da funcao foi bem sucedida
    if( not assert_equal( ISO_VER_1993, result, "test_iso_version1. Retorno incorreto no caso de sucesso passando parametro" ) ) then
        iso.destroy( handle )
        return
    end

    --pega o valor atual de version
    result = handle:version( )

    iso.destroy( handle )

    --verifica se o valor atual de version eh igual ao valor passado logo acima
    assert_equal( ISO_VER_1993, result, "test_iso_version1. Retorno incorreto em caso de sucesso sem passar parametro", true )
end

-------------------------------------------------------------------------------------------------------
--                                   ISO:CLASS TEST CASES                                            --
-------------------------------------------------------------------------------------------------------

tc_iso_class = TestCase("iso:class")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_class1                                                                       --
--  Descricao: Cria um handle, altera a propriedade class do handle, depois verifica se a propriedade--
--             foi corretamente alterada.                                                            --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_class.test_iso_class1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade class
    local result = handle:class( ISO_CLASS_AUTHORIZATION )

    --verifica se a chamada da funcao foi bem sucedida
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, result, "tc_iso_class1. Retorno incorreto no caso de sucesso passando parametro" ) ) then
        iso.destroy( handle )
        return
    end

    --pega o valor atual de class
    result = handle:class( )

    iso.destroy( handle )

    --verifica se o valor atual de class eh igual ao valor passado logo acima
    assert_equal( ISO_CLASS_AUTHORIZATION, result, "tc_iso_class1. Retorno incorreto em caso de sucesso sem passar parametro", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_class2                                                                       --
--  Descricao: Cria um handle, altera a propriedade class do handle para um valor invalido e verifica--
--             se a propriedade foi alterada                                                         --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
-- QUEBRA
-- function tc_iso_class.test_iso_class2( )
--
--     --cria o handle para teste
--     local handle, error_message, error_code = iso.create( )
--
--     --verifica se o handle foi criado
--     assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
--
--     --passa um valor invalido
--     local result, error_message, error_code = handle:class( 627184691875987598476894379834769834 )
--
--     --verifica os retornos da funcao
--     if( not assert_nil( result, "tc_iso_class2. Retorno incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( TST_ERR_INVALIDARG, error_code, "tc_iso_class2. Codigo de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "tc_iso_class2. Mensagem de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     --pega o valor atual de class
--     result = handle:class( )
--
--     iso.destroy( handle )
--
--     --verifica se o valor de class nao foi modificado
--     assert_equal( ISO_CLASS_UNDEF, result, "tc_iso_class2. Retorno incorreto em caso de sucesso sem passar parametro" )
-- end

-------------------------------------------------------------------------------------------------------
--                                   ISO:FNC TEST CASES                                              --
-------------------------------------------------------------------------------------------------------

tc_iso_fnc = TestCase("iso:fnc")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_fnc1                                                                         --
--  Descricao: Cria um handle, altera a propriedade fnc do handle, depois verifica se a propriedade  --
--             foi corretamente alterada.                                                            --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_fnc.test_iso_fnc1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade fnc
    local result = handle:fnc( ISO_FNC_REQUEST )

    --verifica se a chamada da funcao foi bem sucedida
    if( not assert_equal( ISO_FNC_REQUEST, result, "tc_iso_fnc1. Retorno incorreto no caso de sucesso passando parametro [%s - '%s']" ) ) then
        iso.destroy( handle )
        return
    end

    --pega o valor atual de fnc
    result, error_message, error_code = handle:fnc( )

    iso.destroy( handle )

    --verifica se o valor atual de fnc eh igual ao valor passado logo acima
    assert_equal( ISO_FNC_REQUEST, result, "tc_iso_fnc1. Retorno incorreto em caso de sucesso sem passar parametro [%s - '%s']", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_fnc2                                                                         --
--  Descricao: Cria um handle, altera a propriedade fnc do handle para um valor invalido e verifica  --
--             se a propriedade foi alterada                                                         --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
-- QUEBRA
-- function tc_iso_fnc.test_iso_fnc2( )
--
--     --cria o handle para teste
--     local handle, error_message, error_code = iso.create( )
--
--     --verifica se o handle foi criado
--     assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
--
--     --passa um valor invalido
--     local result, error_message, error_code = handle:fnc( 627184691875987598476894379834769834 )
--
--     --verifica os retornos da funcao
--     if( not assert_nil( result, "tc_iso_fnc2. Retorno incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( TST_ERR_INVALIDARG, error_code, "tc_iso_fnc2. Codigo de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "tc_iso_fnc2. Mensagem de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     --pega o valor atual de fnc
--     result = handle:fnc( )
--
--     iso.destroy( handle )
--
--     --verifica se o valor de fnc nao foi modificado
--     assert_equal( "UNDEFINED", result, "tc_iso_fnc2. Retorno incorreto em caso de sucesso sem passar parametro [%s - '%s']" )
-- end

-------------------------------------------------------------------------------------------------------
--                                   ISO:transorign TEST CASES                                        --
-------------------------------------------------------------------------------------------------------

tc_iso_transorign = TestCase("iso:transorign")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_transorign1                                                                  --
--  Descricao: Cria um handle, altera a propriedade transorign do handle, depois verifica se a       --
--             propriedade foi corretamente alterada.                                                --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_transorign.test_iso_transorign1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --altera valor da propriedade transorign
    local result = handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --verifica se a chamada da funcao foi bem sucedida
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, result, "tc_iso_transorign1. Retorno incorreto no caso de sucesso passando parametro" ) ) then
        iso.destroy( handle )
        return
    end

    --pega o valor atual de transorign
    result = handle:transorign( )

    iso.destroy( handle )

    --verifica se o valor atual de transorign eh igual ao valor passado logo acima
    assert_equal( ISO_TRANSORIGN_ACQUIRER, result, "tc_iso_transorign1. Retorno incorreto em caso de sucesso sem passar parametro", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_transorign2                                                                  --
--  Descricao: Cria um handle, altera a propriedade transorign do handle para um valor invalido e    --
--             verifica se a propriedade foi alterada                                                --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
-- QUEBRA
-- function tc_iso_transorign.test_iso_transorign2( )
--
--     --cria o handle para teste
--     local handle, error_message, error_code = iso.create( )
--
--     --verifica se o handle foi criado
--     assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )
--
--     --passa um valor invalido
--     local result, error_message, error_code = handle:transorign( 627184691875987598476894379834769834 )
--
--     --verifica os retornos da funcao
--     if( not assert_nil( result, "tc_iso_transorign2. Retorno incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( TST_ERR_INVALIDARG, error_code, "tc_iso_transorign2. Codigo de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     if( not assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "tc_iso_transorign2. Mensagem de erro incorreto passando parametro invalido" ) ) then
--         iso.destroy( handle )
--         return
--     end
--
--     --pega o valor atual de transorign
--     result = handle:transorign( )
--
--     iso.destroy( handle )
--
--     --verifica se o valor de transorign nao foi modificado
--     assert_equal( "UNDEFINED", result, "tc_iso_transorign2. Retorno incorreto em caso de sucesso sem passar parametro" )
-- end

-------------------------------------------------------------------------------------------------------
--                                   ISO:BINARYFIELD TEST CASES                                      --
-------------------------------------------------------------------------------------------------------

tc_iso_binaryfield = TestCase("iso:binaryfield")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_binaryfield1                                                                 --
--  Descricao: Cria um handle, armazena um dado binario no bit 3, recupera este dado e logo depois   --
--             apaga o dado e verifica se foi realmente apagado                                      --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_binaryfield.test_iso_binaryfield1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --chama a funcao testada passando um valor qualquer para o bit 3
    local result = handle:binaryfield( ISO_BIT_003, "123456" )

    if( not assert_equal( "123456", result, "test_iso_binaryfield1. Retorno incorreto no caso de sucesso passando valor" ) ) then
        iso.destroy( handle )
        return
    end

    --recupera valor
    result = handle:binaryfield( ISO_BIT_003 )

    --verifica se o valor recuperado eh o esperado
    if( not assert_equal( "123456", result, "test_iso_binaryfield1. Valor recuperado esta incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    --apaga o valor
    result = handle:binaryfield( ISO_BIT_003, nil )

    if( not assert_nil( result, "test_iso_binaryfield1. Retorno incorreto apagando valor" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_nil( handle:binaryfield( ISO_BIT_003 ), "test_iso_binaryfield1. Tenta recuperar o valor de um bit apagado. Retorno incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_binaryfield2                                                                 --
--  Descricao: Cria um handle e tenta armazenar um dado binario em um bit de data                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_binaryfield.test_iso_binaryfield2( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --chama a funcao testada passando um valor qualquer para o bit 13(data)
    local result, error_message, error_code = handle:binaryfield( ISO_BIT_013, "123456" )

    iso.destroy( handle )

    assert_nil( result, "test_iso_binaryfield2. Retorno incorreto passando dado binario para bit data", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_iso_binaryfield2. Codigo de erro incorreto passando dado binario para bit data", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_iso_binaryfield2. Mensagem de erro incorreta passando dado binario para bit data" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_binaryfield3                                                                 --
--  Descricao: Cria um handle e tenta recuperar o dado do bit 3 sem seta-lo antes                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_binaryfield.test_iso_binaryfield3( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --recupera valor
    local result = handle:binaryfield( ISO_BIT_003 )

    --verifica se o valor recuperado eh o esperado
    assert_nil( result, "test_iso_binaryfield3. Valor recuperado esta incorreto" )

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                   ISO:DATEFIELD TEST CASES                                        --
-------------------------------------------------------------------------------------------------------

tc_iso_datefield = TestCase("iso:datefield")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_datefield1                                                                   --
--  Descricao: Cria um handle, armazena uma data no bit 12, recupera esta data e logo depois         --
--             apaga a data e verifica se foi realmente apagada                                      --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_datefield.test_iso_datefield1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    handle:version(ISO_VER_1993)

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria uma data valida para ser armazenada
    local data = {}
        data.year = 6
        data.month = 04
        data.day = 18
        data.hour = 20
        data.min = 06
        data.sec = 10

    --chama a funcao testada passando um valor qualquer para o bit 14
    local result = handle:datefield( ISO_BIT_012, data )
    
    --testando retorno
    if( not assert_equal( 6, result.year, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 4, result.month, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 18, result.day, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 20, result.hour, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 06, result.min, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 10, result.sec, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    --recupera valor
    local result2 = handle:datefield( ISO_BIT_012 )

    --verifica se o valor recuperado eh o esperado
    if( not assert_equal( 6, result2.year, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 04, result2.month, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 18, result2.day, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 20, result2.hour, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 06, result2.min, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( 10, result2.sec, "test_iso_datefield1. Data recuperada esta incorreta" ) ) then
        iso.destroy( handle )
        return
    end

    --apaga a data
    local result3 = handle:datefield( ISO_BIT_012, nil )

    --verifica se o valor recuperado eh o esperado
    if( not assert_nil( result3, "test_iso_datefield1. Apagando data. Retorno incorreto" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_nil( handle:datefield( ISO_BIT_012 ), "test_iso_datefield1. Data nao foi apagada" ) ) then
        iso.destroy( handle )
        return
    end

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_datefield2                                                                   --
--  Descricao: Cria um handle e tenta armazenar uma hora invalida no bit 14                          --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_datefield.test_iso_datefield2( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria uma data invalida para ser armazenada
    local data = {}
        data.year = 2006
        data.month = 04
        data.day = 18
        data.hour = -100
        data.min = -100
        data.sec = -100

    --chama a funcao testada passando um valor qualquer para o bit 3
    local result, error_message, error_code = handle:datefield( ISO_BIT_014, data )

    if( not assert_nil( result, "test_iso_datefield2. Retorno incorreto passando hora invalida" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDTIME, error_code, "test_iso_datefield2. Codigo de erro incorreto passando hora invalida" ) ) then
        iso.destroy( handle )
        return
    end

    assert_equal( lua_error_message[ TST_ERR_INVALIDTIME ], error_message, "test_iso_datefield2. Mensagem de erro incorreta passando hora invalida" )

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_datefield3                                                                   --
--  Descricao: Cria um handle e tenta armazenar uma data invalida no bit 14                          --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_datefield.test_iso_datefield3( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria uma data invalida para ser armazenada
    local data = {}
        data.year = -100
        data.month = -100
        data.day = -100
        data.hour = 20
        data.min = 06
        data.sec = 10

    --chama a funcao testada passando um valor qualquer para o bit 3
    local result, error_message, error_code = handle:datefield( ISO_BIT_014, data )

    if( not assert_nil( result, "test_iso_datefield3. Retorno incorreto passando data invalida" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDDATE, error_code, "test_iso_datefield3. Codigo de erro incorreto passando data invalida" ) ) then
        iso.destroy( handle )
        return
    end

    assert_equal( lua_error_message[ TST_ERR_INVALIDDATE ], error_message, "test_iso_datefield3. Mensagem de erro incorreta passando data invalida" )

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_datefield4                                                                   --
--  Descricao: Cria um handle e tenta armazenar uma data em um bit binario                           --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_datefield.test_iso_datefield4( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria uma data valida para ser armazenada
    local data = { }
        data.year  = 2006
        data.month = 04
        data.day   = 18
        data.hour  = 20
        data.min   = 06
        data.sec   = 10

    --chama a funcao testada passando uma data para o bit 3(binario)
    local result, error_message, error_code = handle:datefield( ISO_BIT_003, data )

    if( not assert_nil( result, "test_iso_datefield4. Retorno incorreto passando data para bit binario" ) ) then
        iso.destroy( result )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDARG, error_code, "test_iso_datefield4. Codigo de erro incorreto passando data para bit binario" ) ) then
        iso.destroy( handle )
        return
    end

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_iso_datefield4. Mensagem de erro incorreta passando data para bit binario" )

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_datefield5                                                                   --
--  Descricao: Cria um handle e tenta recuperar o dado do bit 14 sem seta-lo antes                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_datefield.test_iso_datefield5( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --recupera valor
    local result, error_message, error_code = handle:datefield( ISO_BIT_014 )

    assert_nil( result, "test_iso_datefield5. Recuperando valor nao setado. Retorno incorreto" )

    iso.destroy( handle )
end

-------------------------------------------------------------------------------------------------------
--                                   ISO:ASSEMBLE/ISO:DISASSEMBLE TEST CASES                         --
-------------------------------------------------------------------------------------------------------

tc_iso_assemble = TestCase("iso:assemble/iso:disassemble")

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_assemble_disassemble1                                                        --
--  Descricao: Cria um handle, monta um buffer, desmonta o buffer e forma um novo handle             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble1( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin = "1396"
        header.destiny = "8816"
        header.tpdu_id = "10"

    --configura handle
    handle:class( ISO_CLASS_NETWORKMANAGEMENT )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    handle:binaryfield( ISO_BIT_003, "000000" )
    handle:binaryfield( ISO_BIT_011, "000000" )
    handle:binaryfield( ISO_BIT_041, "000000" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble1. Montando mensagem. buffer retornado nulo", true )

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria handle iso para recuperar a mensagem
    local handle2, error_message, error_code = iso.create( )

    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2, error_message, error_code = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    if( not assert_not_nil( header2, "test_iso_assemble_disassemble1. Desmontando a mensagem. Header.retornado nulo" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( "1396", header2.origin, "test_iso_assemble_disassemble1. Desmontando a mensagem. header.origin incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( "8816", header2.destiny, "test_iso_assemble_disassemble1. Desmontando a mensagem. header.destiny incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( "10", header2.tpdu_id, "test_iso_assemble_disassemble1. Desmontando a mensagem. Header.tpdu_id incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( ISO_CLASS_NETWORKMANAGEMENT, handle2:class( ), "test_iso_assemble_disassemble1. Desmontando a mensagem. Handle.class incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc( ), "test_iso_assemble_disassemble1. Desmontando a mensagem. Handle.fnc incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign( ), "test_iso_assemble_disassemble1. Desmontando a mensagem. Handle.transorign incorreto" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_assemble_disassemble2                                                        --
--  Descricao: Tenta montar o buffer com a propriedade class indefinida                              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble2( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin = "1396"
        header.destiny = "8816"
        header.tpdu_id = "10"

    --configura handle sem configurar a propriedade class
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    handle:binaryfield( ISO_BIT_003, "000000" )
    handle:binaryfield( ISO_BIT_011, "000000" )
    handle:binaryfield( ISO_BIT_041, "000000" )

    --constroi um buffer
    local buffer, error_message, error_code = handle:assemble( header )

    assert_nil( buffer, "test_iso_assemble_disassemble2. Retorno incorreto montando mensagem com class indefinido", true )

    assert_equal( TST_ERR_INCOMPLETEISOMSG, error_code, "test_iso_assemble_disassemble2. Codigo de erro incorreto montando mensagem com class indefinido", true )

    assert_equal( lua_error_message[ TST_ERR_INCOMPLETEISOMSG ] , error_message, "test_iso_assemble_disassemble2. Mensagem de erro incorreta montando mensagem com class indefinido" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_assemble_disassemble3                                                        --
--  Descricao: Tenta montar o buffer com a propriedade fnc indefinida                                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble3( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin = "1396"
        header.destiny = "8816"
        header.tpdu_id = "10"

    --configura handle sem configurar a propriedade fnc
    handle:class( ISO_CLASS_NETWORKMANAGEMENT )
    handle:transorign( HF_ISO_TRANSORIGN_ACQUIRER )

    handle:binaryfield( ISO_BIT_003, "000000" )
    handle:binaryfield( ISO_BIT_011, "000000" )
    handle:binaryfield( ISO_BIT_041, "000000" )

    --constroi um bufer
    local buffer, error_message, error_code = handle:assemble( header )

    assert_nil( buffer, "test_iso_assemble_disassemble3. Retorno incorreto montando mensagem com fnc indefinido", true )

    assert_equal( TST_ERR_INCOMPLETEISOMSG, error_code, "test_iso_assemble_disassemble3. Codigo de erro incorreto montando mensagem com fnc indefinido", true )

    assert_equal( lua_error_message[ TST_ERR_INCOMPLETEISOMSG ], error_message, "test_iso_assemble_disassemble3. Mensagem de erro incorreta montando mensagem com fnc indefinido" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_assemble_disassemble4                                                        --
--  Descricao: Tenta montar o buffer com a propriedade transorign indefinida                          --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble4( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin = "1396"
        header.destiny = "8816"
        header.tpdu_id = "10"

    --configura handle sem configurar a propriedade transorign
    handle:class( ISO_CLASS_NETWORKMANAGEMENT )
    handle:fnc( ISO_FNC_REQUEST )

    handle:binaryfield( ISO_BIT_003, "000000" )
    handle:binaryfield( ISO_BIT_011, "000000" )
    handle:binaryfield( ISO_BIT_041, "000000" )

    --constroi um bufer
    local buffer, error_message, error_code = handle:assemble( header )

    assert_nil( buffer, "test_iso_assemble_disassemble4. Retorno incorreto montando mensagem com transorign indefinido", true )

    assert_equal( TST_ERR_INCOMPLETEISOMSG, error_code, "test_iso_assemble_disassemble4. Codigo de erro incorreto montando mensagem com transorign indefinido", true )

    assert_equal( lua_error_message[ TST_ERR_INCOMPLETEISOMSG ], error_message, "test_iso_assemble_disassemble4. Mensagem de erro incorreta montando mensagem com transorign indefinido" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_iso_assemble_disassemble5                                                        --
--  Descricao: Tenta desmontar um input stream de string                                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--                                                                                                   --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--                                                                                                   --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble5( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

     --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local string_stream = streams.openstringis( "0123456789012345678901234567890123456789012345678901234567890123456789" )

    local result, error_message, error_code = handle:disassemble( string_stream, -1 )

    if( not assert_nil( result, "test_iso_assemble_disassemble5. Retorno incorreto desmontando um stream de string" ) ) then
        iso.destroy( handle )
        return
    end

    if( not assert_equal( TST_ERR_INVALIDISOFORMAT, error_code, "test_iso_assemble_disassemble5. Codigo de erro incorreto desmontando um stream de string" ) ) then
        iso.destroy( handle )
        return
    end

    assert_equal( lua_error_message[ TST_ERR_INVALIDISOFORMAT ], error_message, "test_iso_assemble_disassemble5. Mensagem de erro incorreta desmontando um stream de string" )


    iso.destroy( handle )
end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble6                                                          --
--  Descricao: Cria um handle, monta um buffer setando o bit 7(data) e compara com um dado externo     --
--                                                                                                     --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble6( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --cria uma data valida para ser armazenada
    local data = {}
        data.year  = 2006
        data.month = 02
        data.day   = 18
        data.hour  = 16
        data.min   = 29
        data.sec   = 30

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 7 com uma data valida
    handle:datefield( ISO_BIT_007, data )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble6. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno1, str_buffer, "test_iso_assemble_disassemble6. Montando mensagem e comparando com dado externo. buffer retornado incorreto" )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble6. Desmontando mensagem. Header recuperado nulo", true )

    --recupera a data do buffer
    local data2 = handle2:datefield( ISO_BIT_007 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble6. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble6. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble6. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble6. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble6. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble6. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se a data recuperada esta correta
    assert_equal( data.month, data2.month, "test_iso_assemble_disassemble6. Desmontando mensagem. Data recuperada com mes incorreto", true )
    assert_equal( data.day, data2.day, "test_iso_assemble_disassemble6. Desmontando mensagem. Data recuperada com dia incorreto", true )
    assert_equal( data.hour, data2.hour, "test_iso_assemble_disassemble6. Desmontando mensagem. Data recuperada com hora incorreto", true )
    assert_equal( data.min, data2.min, "test_iso_assemble_disassemble6. Desmontando mensagem. Data recuperada com minuto incorreto", true )
    assert_equal( data.sec, data2.sec, "test_iso_assemble_disassemble6. Desmontando mensagem. Data recuperada com segundo incorreto" )
end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble7                                                          --
--  Descricao: Cria um handle, monta um buffer setando o bit 3(binario) com '123' e compara com        --
--              um dado externo                                                                        --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble7( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    handle:binaryfield( ISO_BIT_003, "123" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble7. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno2, str_buffer, "test_iso_assemble_disassemble7. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble7. Desmontando mensagem. Header recuperado nulo", true )

    --recupera a data do buffer
    local bit3_value = handle2:binaryfield( ISO_BIT_003 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble7. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble7. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble7. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble7. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble7. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble7. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se o dado binario recuperado esta correto
    assert_equal( "123", bit3_value, "test_iso_assemble_disassemble7. Desmontando mensagem. valor binario recuperado com valor incorreto" )

end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble8                                                          --
--  Descricao: Cria um handle, monta um buffer setando o bit 2(binario) com '123' e compara com        --
--              um dado externo                                                                        --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble8( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 2
    handle:binaryfield( ISO_BIT_002, "123" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble8. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno3, str_buffer, "test_iso_assemble_disassemble8. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble8. Desmontando mensagem. Header recuperado nulo", true )

    --recupera a data do buffer
    local bit2_value = handle2:binaryfield( ISO_BIT_002 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble8. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble8. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble8. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble8. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble8. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble8. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se o dado binario recuperado esta correto
    assert_equal( "123", bit2_value, "test_iso_assemble_disassemble8. Desmontando mensagem. valor binario recuperado com valor incorreto" )

end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble9                                                          --
--  Descricao: Cria um handle, monta um buffer setando o bit 31(binario) com 'a1b2@' e compara com     --
--              um dado externo                                                                        --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble9( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 31
    handle:binaryfield( ISO_BIT_031, "a1b2@" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble9. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno4, str_buffer, "test_iso_assemble_disassemble9. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble9. Desmontando mensagem. Header recuperado nulo", true )

    --recupera a data do buffer
    local bit31_value = handle2:binaryfield( ISO_BIT_031 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble9. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble9. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble9. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble9. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble9. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble9. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se o dado binario recuperado esta correto
    assert_equal( "a1b2@", bit31_value, "test_iso_assemble_disassemble9. Desmontando mensagem. valor binario recuperado com valor incorreto" )

end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble10                                                         --
--  Descricao: Cria um handle, monta um buffer setando o bit 37(binario) com '0123456789:;<=>?'        --
--              e compara com um dado externo                                                          --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble10( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 35
    handle:binaryfield( ISO_BIT_035, "0123456789:;<=>?" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble10. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno5, str_buffer, "test_iso_assemble_disassemble10. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble10. Desmontando mensagem. Header recuperado nulo", true )

    --recupera o dado do buffer
    local bit35_value = handle2:binaryfield( ISO_BIT_035 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble10. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble10. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble10. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble10. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble10. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble10. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se o dado binario recuperado esta correto
    assert_equal( "0123456789:;<=>?", bit35_value, "test_iso_assemble_disassemble10. Desmontando mensagem. valor binario recuperado com valor incorreto" )

end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble11                                                         --
--  Descricao: Teste setando os bits 5, 3 e 7 para verificar se a API coloca os bits na ordem certa    --
--                                                                                                     --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble11( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --cria uma data valida para ser armazenada
    local data = {}
        data.year  = 2006
        data.month = 04
        data.day   = 06
        data.hour  = 08
        data.min   = 40
        data.sec   = 20

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )


    --seta o bit 7 com uma data valida
    handle:datefield( ISO_BIT_007, data )

    --seta o bit 3 & 5 com um dado binario valida
    handle:binaryfield( ISO_BIT_003, "456" )
    handle:binaryfield( ISO_BIT_005, "321" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble11. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno6, str_buffer, "test_iso_assemble_disassemble11. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble11. Desmontando mensagem. Header recuperado nulo", true )

    --recupera os dados do buffer
    local data2 = handle2:datefield( ISO_BIT_007 )
    local bit3_value = handle2:binaryfield( ISO_BIT_003 )
    local bit5_value = handle2:binaryfield( ISO_BIT_005 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble11. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble11. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble11. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble11. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble11. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble11. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se os dados binarios recuperado esta correto
    assert_equal( "456", bit3_value, "test_iso_assemble_disassemble11. Desmontando mensagem. valor binario recuperado do bit 3 com valor incorreto", true )
    assert_equal( "321", bit5_value, "test_iso_assemble_disassemble11. Desmontando mensagem. valor binario recuperado do bit 5 com valor incorreto" )

    --verificando se a data recuperada esta correta
    assert_equal( data.month, data2.month, "test_iso_assemble_disassemble11. Desmontando mensagem. Data recuperada com mes incorreto", true )
    assert_equal( data.day, data2.day, "test_iso_assemble_disassemble11. Desmontando mensagem. Data recuperada com dia incorreto", true )
    assert_equal( data.hour, data2.hour, "test_iso_assemble_disassemble11. Desmontando mensagem. Data recuperada com hora incorreto", true )
    assert_equal( data.min, data2.min, "test_iso_assemble_disassemble11. Desmontando mensagem. Data recuperada com minuto incorreto", true )
    assert_equal( data.sec, data2.sec, "test_iso_assemble_disassemble11. Desmontando mensagem. Data recuperada com segundo incorreto" )
end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble12                                                         --
--  Descricao: Teste com uma msg externa com bits bin�rios e data setados                              --
--             Teste setando os bits 3, 7, 22, 31, 35, 37 e 52                                         --
--             Bit 03 n..6                                                                             --
--             Bit 07 com o valor 04/06 15:50:40 (MMDDhhmmss)                                          --
--             Bit 22 an..12                                                                           --
--             Bit 31 LLVAR ans..99                                                                    --
--             Bit 35 LLVAR z..37                                                                      --
--             Bit 37 anp..12                                                                          --
--             Bit 52 b..8                                                                             --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble12( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "0000"
        header.destiny = "0000"
        header.tpdu_id = "00"

    --cria uma data valida para ser armazenada
    local data = {}
        data.year  = 2006
        data.month = 04
        data.day   = 06
        data.hour  = 15
        data.min   = 50
        data.sec   = 40

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 7 com uma data valida
    handle:datefield( ISO_BIT_007, data )

    --seta os bits com um dado binario valida
    handle:binaryfield( ISO_BIT_003, "123" )
    handle:binaryfield( ISO_BIT_022, "a1b2c3" )
    handle:binaryfield( ISO_BIT_031, "a1!b2@c3#" )
    handle:binaryfield( ISO_BIT_035, "0123456789:;<=>?" )
    handle:binaryfield( ISO_BIT_037, "a1b2" )
    handle:binaryfield( ISO_BIT_052, "abcdefgh" )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble12. Montando mensagem e comparando com dado externo. buffer retornado nulo", true )

    local str_buffer = Convert( buffer )
    assert_equal( testeExterno7, str_buffer, "test_iso_assemble_disassemble12. Montando mensagem e comparando com dado externo. buffer retornado incorreto", true )

    ---- desmontando ----

   local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble12. Desmontando mensagem. Header recuperado nulo", true )

    --recupera os dados do buffer
    local data2 = handle2:datefield( ISO_BIT_007 )
    local bit3_value =  handle2:binaryfield( ISO_BIT_003 )
    local bit22_value = handle2:binaryfield( ISO_BIT_022 )
    local bit31_value = handle2:binaryfield( ISO_BIT_031 )
    local bit35_value = handle2:binaryfield( ISO_BIT_035 )
    local bit37_value = handle2:binaryfield( ISO_BIT_037 )
    local bit52_value = handle2:binaryfield( ISO_BIT_052 )

    --verificando propriedades do handle
    if( not assert_equal( ISO_CLASS_AUTHORIZATION, handle2:class(), "test_iso_assemble_disassemble12. Desmontando mensagem. prop. class incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_FNC_REQUEST, handle2:fnc(), "test_iso_assemble_disassemble12. Desmontando mensagem. prop. fnc incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end
    if( not assert_equal( ISO_TRANSORIGN_ACQUIRER, handle2:transorign(), "test_iso_assemble_disassemble12. Desmontando mensagem. prop. transorign incorreta" ) ) then
        iso.destroy( handle2 )
        return
    end

    iso.destroy( handle2 )

    --verificando header
    assert_equal( tonumber( header.origin ), tonumber( header2.origin ), "test_iso_assemble_disassemble12. Desmontando mensagem. Header com origem incorreta", true )
    assert_equal( tonumber( header.destiny ), tonumber( header2.destiny ), "test_iso_assemble_disassemble12. Desmontando mensagem. Header com destino incorreto", true )
    assert_equal( tonumber( header.tpdu_id ), tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble12. Desmontando mensagem. Header com tpdu_id incorreto", true )

    --verificando se os dados binarios recuperado esta correto
    assert_equal( "123", bit3_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 3 com valor incorreto", true )
    assert_equal( "a1b2c3", bit22_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 22 com valor incorreto", true )
    assert_equal( "a1!b2@c3#", bit31_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 31 com valor incorreto", true )
    assert_equal( "0123456789:;<=>?", bit35_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 35 com valor incorreto", true )
    assert_equal( "a1b2", bit37_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 37 com valor incorreto", true )
    assert_equal( "abcdefgh", bit52_value, "test_iso_assemble_disassemble12. Desmontando mensagem. valor binario recuperado do bit 52 com valor incorreto", true )

    --verificando se a data recuperada esta correta
    assert_equal( data.month, data2.month, "test_iso_assemble_disassemble12. Desmontando mensagem. Data recuperada com mes incorreto", true )
    assert_equal( data.day, data2.day, "test_iso_assemble_disassemble12. Desmontando mensagem. Data recuperada com dia incorreto", true )
    assert_equal( data.hour, data2.hour, "test_iso_assemble_disassemble12. Desmontando mensagem. Data recuperada com hora incorreto", true )
    assert_equal( data.min, data2.min, "test_iso_assemble_disassemble12. Desmontando mensagem. Data recuperada com minuto incorreto", true )
    assert_equal( data.sec, data2.sec, "test_iso_assemble_disassemble12. Desmontando mensagem. Data recuperada com segundo incorreto" )
end

---------------------------------------------------------------------------------------------------------
--                                                                                                     --
--  Nome:      test_iso_assemble_disassemble13                                                         --
--  Descricao: Passando um header com tamanho de campos menores que o permitido                        --
--                                                                                                     --
--  Argumentos:                                                                                        --
--                                                                                                     --
--  Retornos:                                                                                          --
--    Casos de erro:                                                                                   --
--                                                                                                     --
--    Casos de sucesso:                                                                                --
---------------------------------------------------------------------------------------------------------
function tc_iso_assemble.test_iso_assemble_disassemble13( )

    --cria o handle para teste
    local handle, error_message, error_code = iso.create( )

    --verifica se o handle foi criado
    assert_not_nil( handle, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    --cria um header para o handle
    local header = {}
        header.origin  = "1"
        header.destiny = "2"
        header.tpdu_id = "3"

    --cria uma data valida para ser armazenada
    local data = {}
        data.year  = 2006
        data.month = 02
        data.day   = 18
        data.hour  = 16
        data.min   = 29
        data.sec   = 30

    --configura handle
    handle:version(ISO_VER_1993)
    handle:class( ISO_CLASS_AUTHORIZATION )
    handle:fnc( ISO_FNC_REQUEST )
    handle:transorign( ISO_TRANSORIGN_ACQUIRER )

    --seta o bit 7 com uma data valida
    handle:datefield( ISO_BIT_007, data )

    --constroi um buffer
    local buffer = handle:assemble( header )

    iso.destroy( handle )

    assert_not_nil( buffer, "test_iso_assemble_disassemble13. Passando um header com campos pequenos. buffer retornado nulo", true )

    ---- desmontando ----

    local buffer_stream, error_message, error_code = streams.openstringis( buffer )

    assert_not_nil( buffer_stream, string.format( "Dependencia: Input stream nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ), true )

    local handle2, error_message, error_code = iso.create()

    --verifica se o handle foi criado
    if( not assert_not_nil( handle2, string.format( "Dependencia: Handle nao pode ser criado [%s - '%s']", tostring( error_code ), tostring( error_message ) ) ) ) then
        buffer_stream:close()
        return
    end

    local header2 = handle2:disassemble( buffer_stream, -1 )

    buffer_stream:close()

    iso.destroy( handle2 )

    --verifica se o header foi criado
    assert_not_nil( header2, "test_iso_assemble_disassemble13. Passando um header com campos pequenos. Header recuperado nulo", true )

    --verificando header
    assert_equal( 1, tonumber( header2.origin ), "test_iso_assemble_disassemble13. Passando um header com campos pequenos. Header com origem incorreta", true )
    assert_equal( 2, tonumber( header2.destiny ), "test_iso_assemble_disassemble13. Passando um header com campos pequenos. Header com destino incorreto", true )
    assert_equal( 3, tonumber( header2.tpdu_id ), "test_iso_assemble_disassemble13. Passando um header com campos pequenos. Header com tpdu_id incorreto", true )
end

lunit.run( )
