---------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 auxiliar.lua                                                 --
-- Tres Letras Representativas:     TST                                                          --
-- Descricao:                       Fun��es auxiliares utilizadas nos testes unit�rios do LUA    --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         26/01/2006  Criacao                                          --
-- Leandro              lmf         08/02/2006  Adicao de funcionalidades                        --
-- Jamerson Lima       jrfl         04/04/2006  Adicao de funcionalidades                        --
-- Jamerson Lima       jrfl         26/04/2006  Atualizacao dos codigos e mensagens de erro      --
-- Jamerson Lima       jrfl         16/06/2006  Atualizacao dos codigos e mensagens de erro      --
-- Leandro              lmf         26/01/2006  Corre��o de Falha (CR 16740)                     --
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--                                     Variaveis Globais                                         --
---------------------------------------------------------------------------------------------------

TIMEOUT_UI = 30000
--numero do terminal
TST_SYS_IDPRTYSERIALNU = 1

-- Error return codes--

TST_SUCCESS               = 0
TST_ERR_INVALIDARG       = -1
TST_ERR_NOMEMORY         = -2
TST_ERR_DEVICEFAULT      = -3
TST_ERR_RESOURCEALLOC    = -4
TST_ERR_TIMEOUT          = -5
TST_ERR_SMALLBUFF        = -6
TST_ERR_INVALIDSTATE     = -7
TST_ERR_INVALIDPAD       = -8

-- Base 64 codes
-- Range -1001 to -2000
TST_ERR_INVALIDCHAR      = -1001

-- Datetime codes
-- Range -2001 to -3000
TST_ERR_INVALIDDATE      = -2001
TST_ERR_INVALIDTIME      = -2002

-- File System codes
-- Range -3001 to -4000
TST_ERR_NOTFOUND          = -3001
TST_ERR_MAXLEN            = -3002
TST_ERR_ISOPEN            = -3003
TST_ERR_OPENFILES         = -3004
TST_ERR_HDRCORRUPT        = -3005
TST_ERR_NOSPACE           = -3006
TST_ERR_PATHERR           = -3007
TST_ERR_INVALMODE         = -3008
TST_ERR_INVALDESC         = -3009
TST_ERR_READONLY          = -3010
TST_ERR_OUTOFBOUNDS       = -3011
TST_ERR_NOACCESSRIGHT     = -3012
TST_ERR_PATHTOODEEP       = -3013
TST_ERR_ALREADYEXISTS     = -3014
TST_ERR_NOTEMPTY          = -3015
TST_ERR_ISAFILE           = -3016
TST_ERR_EOF               = -3017
TST_ERR_NOTGLOBAL         = -3018
TST_ERR_BUFFOVERFLOW      = -3019
TST_ERR_ISADIR            = -3020
TST_ERR_FILEACCFORBID     = -3021
TST_ERR_EMPTYDIR          = -3022

-- Management codes
-- Range -4001 to -5000

-- Display codes
-- Range -5001 to -6000
TST_ERR_UNKNOWNFORMAT    = -5001

-- Keyboard codes
-- Range -6001 to -7000
TST_ERR_KEYMISMATCH      = -6001
TST_ERR_KEYNOTSUPPORTED  = -6002

-- Printer codes
-- Range -7001 to -8000
TST_PRINTER_OK           = -7001
TST_PRINTER_OOP          = -7002

-- Magnetic codes
-- Range -8001 to -9000
TST_ERR_MAGREADCANCELE   = -8001
TST_ERR_MAG_SWIPE        = -8002
TST_ERR_MAG_TRACK        = -8003
TST_ERR_MAG_UNAVAILABL   = -8004
TST_ERR_MAG_DATAPARITY   = -8005
TST_ERR_MAG_LRC          = -8006
TST_ERR_MAG_LRCPARITY    = -8007

-- Communication codes
-- Range -9001 to -10000
TST_COM_WAITING          = -9001
TST_ERR_CONNFAILED       = -9002
TST_ERR_NOTCONNECTED     = -9003
TST_ERR_COMMPRTYNOTSUP   = -9004
TST_ERR_INVALIDCOMMPRTY  = -9005
TST_ERR_NOCARRIER        = -9006
TST_ERR_NODIALTONE       = -9007
TST_ERR_BUSYLINE         = -9008
TST_ERR_NOANSWER         = -9009
TST_ERR_CONNLOST         = -9010

-- Properties codes
-- Range -10001 to -11000
TST_ERR_KEYNOTFOUND      = -10001

-- UI codes
-- Range -11001 to -12000
TST_STAT_SCREENACCEPT    = -11001
TST_STAT_SCREENREJECT    = -11002
TST_STAT_TEXTTRUNCATE    = -11003
TST_ERR_ELEMENTTPNOTSUPP = -11004
TST_ERR_ALIGNOTSUPP      = -11005
TST_ERR_NOITEMACCEPTED   = -11006


-- Cryptography codes
-- Range -12001 to -13000
TST_ERR_INVALIDRSAKEY    = -12001
TST_ERR_INVALSIGNATURE   = -12002
TST_ERR_NOTAPRIVATEKEY   = -12003
TST_ERR_NOTAPUBLICKEY    = -12004


-- Sys codes
-- Range -13001 to -14000
TST_ERR_INVALSYSPRTY     = -13001

-- Compression codes
-- Range -14001 to -15000
TST_ERR_INVALIDHUFFFORMAT = -14001

-- ISO codes
-- Range -15001 to -16000
TST_ERR_INVALIDBITFORMAT  = -15001
TST_ERR_MAXBITLENGTH      = -15002
TST_ERR_BITNOTSET         = -15003
TST_ERR_INCOMPLETEISOMSG  = -15004
TST_ERR_INVALIDTPDU       = -15005
TST_ERR_INVALIDISOFORMAT  = -15006

-- UTIL codes
-- Range -16001 to -17000
TST_ERR_INVALBCDCHAR      = -16001

-- STREAMS codes
TST_ERR_STREAM_CLOSED     = -17001

-- Barcode codes
TST_ERR_INVALIDBARCODE     = -18001
TST_ERR_INVALIDBARCODEPRTY = -18002

-- Error messages
lua_error_message = {}

lua_error_message[ TST_ERR_INVALIDARG ] = "invalid argument"
lua_error_message[ TST_ERR_NOMEMORY ] = "no memory"
lua_error_message[ TST_ERR_DEVICEFAULT ] = "device fault"
lua_error_message[ TST_ERR_RESOURCEALLOC ] = "resource allocation"
lua_error_message[ TST_ERR_TIMEOUT ] = "timeout"
lua_error_message[ TST_ERR_SMALLBUFF ] = "small buffer"
lua_error_message[ TST_ERR_INVALIDSTATE ] = "invalid state"

-- Base 64 codes
lua_error_message[ TST_ERR_INVALIDCHAR ] =  "invalid char"

-- Datetime codes
lua_error_message[ TST_ERR_INVALIDDATE ] =  "invalid date"
lua_error_message[ TST_ERR_INVALIDTIME ] = "invalid time"

-- File System codes
lua_error_message[ TST_ERR_NOTFOUND ] =  "file/directory not found"
lua_error_message[ TST_ERR_MAXLEN ] = "max length"
lua_error_message[ TST_ERR_ISOPEN  ] = "file already open"
lua_error_message[ TST_ERR_OPENFILES ] = "max files opened"
lua_error_message[ TST_ERR_HDRCORRUPT ] = "header corrupted"
lua_error_message[ TST_ERR_NOSPACE ] = "invalid argument"
lua_error_message[ TST_ERR_PATHERR ] = "path error"
lua_error_message[ TST_ERR_INVALMODE ] = "invalid accessmode"
lua_error_message[ TST_ERR_INVALDESC ] = "invalid description"
lua_error_message[ TST_ERR_READONLY ] = "read only"
lua_error_message[ TST_ERR_OUTOFBOUNDS ] = "out of bounds"
lua_error_message[ TST_ERR_NOACCESSRIGHT ] = "no access right"
lua_error_message[ TST_ERR_PATHTOODEEP ] = "path too deep"
lua_error_message[ TST_ERR_ALREADYEXISTS ] = "directory already exists"
lua_error_message[ TST_ERR_NOTEMPTY  ] = "not empty"
lua_error_message[ TST_ERR_ISAFILE ] = "is a file"
lua_error_message[ TST_ERR_EOF ] = "end of file"
lua_error_message[ TST_ERR_NOTGLOBAL ] = "not global"
lua_error_message[ TST_ERR_BUFFOVERFLOW ] = "buffer over flow"
lua_error_message[ TST_ERR_ISADIR ] = "is a directory"
lua_error_message[ TST_ERR_FILEACCFORBID ] = "access forbiden"

-- Management codes

-- Display codes
lua_error_message[ TST_ERR_UNKNOWNFORMAT ] =  "unknow format"

-- Keyboard codes
lua_error_message[ TST_ERR_KEYMISMATCH ] =  "key mismatch"
lua_error_message[ TST_ERR_KEYNOTSUPPORTED ] = "key not supported"

-- Magnetic codes
lua_error_message[ TST_ERR_MAGREADCANCELE ] =  "read canceled"
lua_error_message[ TST_ERR_MAG_SWIPE ] = "swipe error"
lua_error_message[ TST_ERR_MAG_TRACK ] = "track error"
lua_error_message[ TST_ERR_MAG_UNAVAILABL ] = "unavailable track"
lua_error_message[ TST_ERR_MAG_DATAPARITY ] = "data parity error"
lua_error_message[ TST_ERR_MAG_LRC ] = "LRC to data checksum error"
lua_error_message[ TST_ERR_MAG_LRCPARITY ] = "parity LRC checksum error"

-- Communication codes
lua_error_message[ TST_ERR_CONNFAILED ] =  "connection failed"
lua_error_message[ TST_ERR_NOTCONNECTED ] = "not connected"
lua_error_message[ TST_ERR_COMMPRTYNOTSUP ] = "comm property not suported"
lua_error_message[ TST_ERR_INVALIDCOMMPRTY ] = "invalid comm property"
lua_error_message[ TST_ERR_NOANSWER ] = "no answer"

-- Properties codes
lua_error_message[ TST_ERR_KEYNOTFOUND ] =  "key not found"

-- UI codes
lua_error_message[ TST_ERR_ELEMENTTPNOTSUPP ] =  "element not supported"
lua_error_message[ TST_ERR_ALIGNOTSUPP ] = "align not supported"
lua_error_message[ TST_ERR_NOITEMACCEPTED ] = "no item accepted"

-- Cryptography codes
lua_error_message[ TST_ERR_INVALIDRSAKEY ] =  "invalid rsa key"
lua_error_message[ TST_ERR_INVALSIGNATURE ] = "invalid signature"
lua_error_message[ TST_ERR_NOTAPRIVATEKEY ] = "not a private key"
lua_error_message[ TST_ERR_NOTAPUBLICKEY ] = "not a public key"
lua_error_message[ TST_STAT_TEXTTRUNCATE ] = "text too big"

-- Sys codes
lua_error_message[ TST_ERR_INVALSYSPRTY ] =  "invalid system property"

-- Compression codes
lua_error_message[ TST_ERR_INVALIDHUFFFORMAT ] =  "invalid buffer format"

-- ISO codes
lua_error_message[ TST_ERR_INVALIDBITFORMAT ] =  "invalid bit format"
lua_error_message[ TST_ERR_MAXBITLENGTH ] = "max bit length"
lua_error_message[ TST_ERR_BITNOTSET ] = "bit not set"
lua_error_message[ TST_ERR_INCOMPLETEISOMSG ] = "incomplete iso mensage"
lua_error_message[ TST_ERR_INVALIDTPDU ] = "invalid TPDU"
lua_error_message[ TST_ERR_INVALIDISOFORMAT ] = "invalid iso format"

-- UTIL codes
lua_error_message[ TST_ERR_INVALBCDCHAR ] = "invalid BDC char"

-- Streams codes
lua_error_message[ TST_ERR_STREAM_CLOSED ] = "stream closed"

-- Barcode codes
lua_error_message[ TST_ERR_INVALIDBARCODE ] = "invalid barcode"
lua_error_message[ TST_ERR_INVALIDBARCODEPRTY ] = "invalid barcode property"

---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------

--convert bytestring para string com valores hexadecimais equivalente
function Convert( bytestring, index )
    local result = ""
    local size = 0
    local i = 1

    if( index ~= nil and index > 0 and index < bytestring:len() ) then
        size = index
    else
        size = bytestring:len()
    end

    for i = 1, size, 1 do
        result = ( result .. string.format( "%02X", bytestring:byte( i ) ) )
    end

    return result
end

--carrega conteudo de um arquivo em um buffer string
function LoadFileInBuffer( filename )

    --verifica se o arquivo existe
    if( not os.exists( filename ) ) then
        return nil, ( string.format( "Dependencia: %s nao encontrado", filename ) )
    end

    --abre e le o conteudo do arquivo de teste
    local handle = io.open( filename )

    if( handle == nil ) then
        return nil, ( string.format( "Dependencia: %s nao pode ser aberto", filename ) )
    end

    local buffer = handle:read( "*a" )

    handle:close()

    return buffer
end

--funcao auxiliar que verifica a existencia de um arquivo
function FileExists( filename )

    local exists = os.exists( filename )

    if( exists == nil ) then
        return false
    else
        return true
    end
end

-- ****************** Fun��o auxiliar q l� '1' ou '2' ******************--
function lerTecla()

    local _,linhas = display.geometry()

    display.clear( linhas )
    --display.print( "1-OK / 2-FAIL", linhas - 1, 0 )
    repeat
          key = keyboard.getkeystroke(-1)
    until ( ( key == KEY_ONE ) or ( key == KEY_TWO ) )
    return key
end

-- ****************** Fun��o auxiliar q cria um arquivo de teste ****************** --
function criaArquivo(vazio, global)
    local ret

    if (global) then
        ret = io.open("global.txt", "rwg")
    else
        ret = io.open("teste.txt", "rw")
    end

    local ret2 = false

    if ret ~= nil then
       if (not vazio) then
           ret:write("teste linha 1\n")
           ret:write("teste linha 2\n")
           ret:write("teste linha 3\n")
           ret:write("teste linha 4\n")
       end

       ret2 = io.close(ret)
    end
    
    return ret2
end

-- ****************** Fun��o auxiliar q compara teclas ****************** --
function comparaTecla(key)
    display.clear(3)
    display.print("Tecla: ",3,0)

    if key == nil then
       display.print("TimeOut/Erro", 3, 7)
       return 0
    elseif ( key == KEY_ZERO) then
       display.print("0", 3, 7)
       HF_sleep(100)
       return 1
    elseif ( key == KEY_ONE) then
       display.print("1", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_TWO) then
       display.print("2", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_THREE) then
       display.print("3", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_FOUR) then
       display.print("4", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_FIVE) then
       display.print("5", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_SIX) then
       display.print("6", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_SEVEN) then
       display.print("7", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_EIGHT) then
       display.print("8", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_NINE) then
       display.print("9", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_UP) then
       display.print("up", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_DOWN) then
       display.print("down", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_ENTER) then
       display.print("enter", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_CANCEL) then
       display.print("cancel", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_HASH) then
       display.print("hash", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_CLEAR) then
       display.print("clear", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_F1) then
       display.print("f1", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_STAR) then
       display.print("star", 3, 7)
       HF_sleep(100)
       return 0
    elseif ( key == KEY_MENU) then
       display.print("menu", 3, 7)
       HF_sleep(100)
       return 0
    end

    return 1
end

HF_sleep = management.sleep
display.font("small")
