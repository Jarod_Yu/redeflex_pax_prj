require "ui"
-----------------------
-- Intialize package --
-----------------------
local P = { }
lunit = P

-- Import
local type = type
local print = print
local ipairs = ipairs
local pairs = pairs
local string = string
local table = table
local pcall = pcall
local xpcall = xpcall
--local traceback = debug.traceback
local error = error
local setmetatable = setmetatable
local rawset = rawset
local orig_assert = assert
local getfenv = getfenv
local setfenv = setfenv
local tostring = tostring
local printer = printer
local display = display
local keyboard = keyboard
local io = io --usado para carregar e ler o arquivo config.txt

-- Start package scope
setfenv(1, P)

--------------------------------
-- Private data and functions --
--------------------------------

local run_testcase
local do_assert, check_msg
local stats = { }
local testcases = { }
local stats_inc, tc_mt
local use_config = false --booleano que indica se vai usar o arquivo config.txt ou nao
local config_test_functions = {} --lista que guarda o nome das funcoes do arquivo config.txt( se existir)
local call_config --funcao que verifica se o arquivo config existe. se sim, carrega o nome das funcoes na lista
local actual_function_name = "" --armazena nome da funcao de testes que esta rodando
local block_test = false --variavel que bloqueia ou nao a execucao de um teste

--------------------------
-- Type check functions --
--------------------------

function is_nil(x)
  return type(x) == "nil"
end

function is_boolean(x)
  return type(x) == "boolean"
end

function is_number(x)
  return type(x) == "number"
end

function is_string(x)
  return type(x) == "string"
end

function is_table(x)
  return type(x) == "table"
end

function is_function(x)
  return type(x) == "function"
end

----------------------
-- Config functions --
----------------------
--funcao que verifica se o arquivo config existe. se sim, carrega o nome das funcoes na lista
function call_config( )
  --tenta abrir o arquivo com lista de funcoes
  local config_handle = io.open( "config.txt", "r" )

  if config_handle ~= nil then
    --em caso de sucesso carrega cada linha(que se espera ter um nome de funcao valido) na lista
    --variavel de controle, assim o lunit sabe se esta usando o arquivo config.txt ou nao
    use_config = true

    --cada linha deve ter um nome de funcao da TestCase carregada
    for line in config_handle:lines( ) do
      line = line:sub( 0, ( line:len( ) -1 ) )
      config_test_functions[line] = line
    end

    --no fim de tudo, fecha o arquivo
    io.close( config_handle )
  end
end
----------------------
-- Assert functions --
----------------------

function assert(assertion, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert", msg)
  return( do_assert(not not assertion, string.format("assertion failed (was: %s)", tostring(assertion)), msg, exit_test) )		-- (convert assertion to bool)
end


function assert_fail(msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_fail", msg)
  do_assert(false, "failure", msg, exit_test)
end


function assert_true(actual, msg , exit_test)
  stats_inc("assertions")
  check_msg("assert_true", msg)

--  do_assert(is_boolean(actual), "true expected but was a "..type(actual), msg)
 if is_boolean(actual)
 then
  --do_assert(is_boolean(actual), string.format("true expected but was a %s", type(actual)), msg)
  return( do_assert(actual, string.format("true expected but was a %s", tostring(actual)), msg, exit_test) )
 else
  return( do_assert(actual == true, "true expected but was false", msg, exit_test) )
 end
  return false
end

function assert_false(actual, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_false", msg)

--  do_assert(is_boolean(actual), "false expected but was a "..type(actual), msg)
 if is_boolean(actual)
 then
  --do_assert(is_boolean(actual), string.format("false expected but was a %s", type(actual)), msg)
  return( do_assert( not actual , string.format("false expected but was a %s", tostring(actual)), msg, exit_test) )
 else
  return( do_assert(actual == false, "false expected but was true", msg, exit_test) )
 end
  return false
end

function assert_equal(expected, actual, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_equal", msg)
--  do_assert(expected == actual, "expected '"..tostring(expected).."' but was '"..tostring(actual).."'", msg)

  local auxiliar = string.format("expected '%s' but was '%s'", tostring(expected), tostring(actual))
  return( do_assert(expected == actual, auxiliar, msg , exit_test) )
end


function assert_not_equal(unexpected, actual, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_not_equal", msg)

--  do_assert(unexpected ~= actual, "'"..tostring(expected).."' not expected but was one", msg)
  return( do_assert(unexpected ~= actual, string.format("'%s' not expected but was one", tostring(expected)), msg, exit_test) )
end

function assert_nil(actual, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_nil", msg)

--  do_assert(is_nil(actual), "nil expected but was a "..type(actual), msg)
  return( do_assert(is_nil(actual), string.format("nil expected but was a %s", type(actual)), msg, exit_test) )
end

function assert_not_nil(actual, msg, exit_test)
  stats_inc("assertions")
  check_msg("assert_not_nil", msg)

  return( do_assert(not is_nil(actual), "nil not expected but was one", msg, exit_test) )
end

-----------------------------------------------------------
-- Assert implementation that assumes it was called from --
-- lunit code which was called directly from user code.  --
-----------------------------------------------------------

function do_assert(assertion, base_msg, user_msg, exit_test)
  orig_assert(is_boolean(assertion))
  orig_assert(is_string(base_msg))
  orig_assert(is_string(user_msg) or is_nil(user_msg))

  if( is_nil( exit_test ) ) then
    exit_test = false
  end

  if not assertion then
    if user_msg then
    --  printer.print(base_msg..": "..user_msg.."\n")
      stats.failed = stats.failed + 1
      if( exit_test ) then
        error(string.format("[Assert Error] %s: %s", base_msg, user_msg), 3)
      else
        printer.print(string.format("[Assert Error] %s: %s", base_msg, user_msg))
        printer.linefeed(2);
        return false
      end
    else
--      printer.print(base_msg.."!".."\n")
--      printer.print(base_msg)
      stats.failed = stats.failed + 1
      if( exit_test ) then
        error(string.format("[Assert Error] %s!", base_msg), 3)
      else
        printer.print(string.format("[Assert Error] %s!", base_msg))
        printer.linefeed(2);
        return false
      end
    end
  end
  return true
end

-------------------------------------------
-- Checks the msg argument in assert_xxx --
-------------------------------------------

function check_msg(name, msg)
  orig_assert(is_string(name))
  if not (is_nil(msg) or is_string(msg)) then
    error("lunit."..name.."() expects the optional message as a string but it was a "..type(msg).."!" ,3)
  end
end

-------------------------------------
-- Creates a new TestCase 'Object' --
-------------------------------------

function TestCase(name)
  do_assert(is_string(name), "lunit.TestCase() needs a string as an argument")
  local tc = {
    __lunit_name = name;
    __lunit_setup = nil;
    __lunit_tests = { };
    __lunit_teardown = nil;
    __lunit_test_number = 0
  }
  setmetatable(tc, tc_mt)
  table.insert(testcases, tc)
  return tc
end

tc_mt = {
  __newindex = function(tc, key, value)
    rawset(tc, key, value)
    if is_string(key) and is_function(value) then
      local name = string.lower(key)
      if string.find(name, "^test") or string.find(name, "test$") then
        table.insert(tc.__lunit_tests, key)
      elseif name == "setup" then
        tc.__lunit_setup = value
      elseif name == "teardown" then
        tc.__lunit_teardown = value
      end
    end
  end
}

----------------------------------
-- Runs the complete Test Suite --
----------------------------------

function run( )
  ---------------------------
  -- Initialize statistics --
  ---------------------------

  stats.testcases = 0	-- Total number of Test Cases
  stats.tests = 0	-- Total number of all Tests in all Test Cases
  stats.passed_tests = 0 -- Number of Test passed
  stats.failed_tests = 0 -- Number of Tests failed
  stats.run = 0		-- Number of Tests run
  stats.notrun = 0	-- Number of Tests not run
  stats.failed = 0	-- Number of Asserts failed
  stats.passed = 0	-- Number of Asserts passed
  stats.assertions = 0	-- Number of all assertions made in all Test in all Test Cases
  stats.error = 0

  --------------------------------
  -- Verify config.txt          --
  --------------------------------
  call_config( )

  --------------------------------
  -- Count Test Cases and Tests --
  --------------------------------
  stats.testcases = table.getn(testcases)

  for _, tc in ipairs(testcases) do
    stats_inc("tests" , table.getn(tc.__lunit_tests))
  end

  ------------------
  -- Print Header --
  ------------------
  printer.linefeed(1)
--  printer.print("#### Test Suite with "..stats.tests.." Tests in "..stats.testcases.." Test Cases loaded.")
--  printer.print("#### Test Suite with ")
  printer.print(string.format("#### Test Suite with %i Test Cases loaded.", stats.testcases))

  ------------------------
  -- Run all Test Cases --
  ------------------------
  for _, tc in ipairs(testcases) do
    run_testcase(tc)
  end

  ------------------
  -- Print Footer --
  ------------------

  printer.linefeed(1)
  printer.print("#### Test Suite finished.")

--  local msg_assertions = stats.assertions.." Assertions checked. "
--  local msg_passed     = stats.passed == stats.tests and "All Tests passed" or  stats.passed.." Tests passed"
--  local msg_failed     = stats.failed > 0 and ", "..stats.failed.." failed" or ""
--  local msg_run	       = stats.notrun > 0 and ", "..stats.notrun.." not run" or ""

  stats.passed = stats.assertions - stats.failed
  local msg_assertions = string.format( "%i Assertions checked. ", stats.assertions )
  local msg_passed     = string.format("%i Tests passed", stats.passed, stats.tests)
  local msg_failed     = stats.failed > 0 and string.format(", %i failed", stats.failed) or ""
  local msg_run	       = stats.notrun > 0 and string.format(", %i not run", stats.notrun) or ""
  local msg_error      = stats.error > 0 and string.format(", %i with error", stats.error) or ""

  printer.linefeed( 1 )
--  printer.print(msg_assertions..msg_passed..msg_failed..msg_run.."!")
--  printer.print(msg_passed)
  printer.print( string.format( "Test functions loaded: %i", stats.tests ) )
  printer.print(string.format("%s %s %s %s %s", msg_assertions, msg_passed, msg_failed,msg_error, msg_run))
  printer.linefeed( 10 )

  -----------------
  -- Return code --
  -----------------

  if stats.passed == stats.tests then
    return 0
  else
    return 1
  end
end

-----------------------------
-- Runs a single Test Case --
-----------------------------

function run_testcase(tc)

  orig_assert(is_table(tc))
  orig_assert(is_table(tc.__lunit_tests))
  orig_assert(is_string(tc.__lunit_name))
  orig_assert(is_nil(tc.__lunit_setup) or is_function(tc.__lunit_setup))
  orig_assert(is_nil(tc.__lunit_teardown) or is_function(tc.__lunit_teardown))

  --------------------------------------------
  -- Error handling function                --
  --------------------------------------------
  local function error_handle( err )
    return( tostring( type( traceback() ) ) )
  end

  --------------------------------------------
  -- Protected call to a Test Case function --
  --------------------------------------------

  local function call(errprefix, func)
    orig_assert(is_string(errprefix))
    orig_assert(is_function(func))
    --local ok, errmsg = xpcall(function() func(tc) end, error_handle)
    local ok, errmsg = pcall(function() func(tc) end )
    if not ok then
--      printer.print(errprefix..": "..errmsg)
      printer.print(string.format("%s: %s", errprefix, errmsg))
    end
    return ok
  end

  ------------------------------------
  -- Calls setup() on the Test Case --
  ------------------------------------

  local function setup()
    if tc.__lunit_setup then
      return call("ERROR: setup() failed", tc.__lunit_setup)
    else
      return true
    end
  end

  ------------------------------------------
  -- Calls a single Test on the Test Case --
  ------------------------------------------

  local function run(testname)
    orig_assert(is_string(testname))
    orig_assert(is_function(tc[testname]))

    printer.linefeed(1)
    printer.print( string.format( "RUNNING: %s", testname ) )
    actual_function_name = testname
    local ok = call("FAIL: "..testname, tc[testname])
    if not ok then
      --stats_inc("failed")
      --stats_inc("error")
    else
      stats_inc("passed")
    end
    return ok
  end

  ---------------------------------------
  -- Calls teardown() on the Test Case --
  ---------------------------------------

  local function teardown()
     if tc.__lunit_teardown then
       call("WARNING: teardown() failed", tc.__lunit_teardown)
     end
  end

  ---------------------------------
  -- Run all Tests on a TestCase --
  ---------------------------------

  --printer.linefeed(1)
  --printer.print("#### Running '"..tc.__lunit_name.."' ("..table.getn(tc.__lunit_tests).." Tests)...\n")
  --printer.print(string.format("#### Running %s (%i Test Functions)\n", tc.__lunit_name, table.getn(tc.__lunit_tests)))
  --printer.print(string.format(" (%i Tests\n", table.getn(tc.__lunit_tests)))

  for _, testname in ipairs(tc.__lunit_tests) do
    tc.__lunit_test_number = tc.__lunit_test_number + 1
    --verifica se o lunit esta usando o arquivo config.txt ou nao
    if ( ( use_config == false ) or ( config_test_functions[ testname ] ~= nil ) ) then
      --executa a funcao so se ela estiver no arquivo config.txt ou, se config.txt nao existir, roda todas as funcoes
      if setup() then
        run(testname)
        stats_inc("run")
        teardown()
      else
--      printer.print("WARN: Skipping '"..testname.."'...")
        printer.print(string.format("WARN: Skipping %s", testname))
        stats_inc("notrun")
      end
    end
  end
end

---------------------
-- Import function --
---------------------

function import(name)

  do_assert(is_string(name), "lunit.import() expects a single string as argument")
  
  local user_env = getfenv(2)
  
  --------------------------------------------------
  -- Installs a specific function in the user env --
  --------------------------------------------------
  
  local function install(funcname)
    user_env[funcname] = P[funcname]
  end
  
  
  ----------------------------------------------------------
  -- Install functions matching a pattern in the user env --
  ----------------------------------------------------------
  
  local function install_pattern(pattern)
    for funcname, _ in pairs(P) do
      if string.find(funcname, pattern) then
        install(funcname)
      end
    end
  end
  
  ------------------------------------------------------------
  -- Installs assert() and all assert_xxx() in the user env --
  ------------------------------------------------------------

  local function install_asserts()
    install_pattern("^assert.*")
  end
  
  -------------------------------------------
  -- Installs all is_xxx() in the user env --
  -------------------------------------------

  local function install_tests()
    install_pattern("^is_.+")
  end

  if name == "asserts" or name == "assertions" then
    install_asserts()
  elseif name == "tests" or name == "checks" then
    install_tests()
  elseif name == "all" then
    install_asserts()
    install_tests()
    install("TestCase")
  elseif string.find(name, "^assert.*") and P[name] then
    install(name)
  elseif string.find(name, "^is_.+") and P[name] then
    install(name)
  elseif name == "TestCase" then
    install("TestCase")
  else
    error("luniit.import(): invalid function '"..name.."' to import", 2)
  end
end




--------------------------------------------------
-- Installs a private environment on the caller --
--------------------------------------------------

function setprivfenv()
  local new_env = { }
  local new_env_mt = { __index = getfenv(2) }
  setmetatable(new_env, new_env_mt)
  setfenv(2, new_env)
end




--------------------------------------------------
-- Increments a counter in the statistics table --
--------------------------------------------------

function stats_inc(varname, value)
  orig_assert(is_table(stats))
  orig_assert(is_string(varname))
  orig_assert(is_nil(value) or is_number(value))
  if not stats[varname] then return end
  stats[varname] = stats[varname] + (value or 1)
end




