---------------------------------------------------------------------------------------------------
-- Descricao:                       Testes Unit�rios da extens�o do teclado                      --
---------------------------------------------------------------------------------------------------
--                            Historico                                                          --
-- Nome                 Login       Data        Descricao                                        --
---------------------------------------------------------------------------------------------------
-- Leandro              lmf         25/01/2006  Cria��o                                          --
-- Leandro              lmf         03/02/2006  Cabe�alho das fun��es                            --
-- Jamerson Lima        jrfl        13/06/2006  Rework(HFLEX-LUA-TSTR-CODE-INSP006)              --
-- Leandro              lmf         05/01/2007  Inclusao de novos testes (19096)                 --
-- Guilherme            gkmo        09/02/2007  Corrigindo problema em test_getkeystroke3(20743) --
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
--                                     Imports                                                   --
---------------------------------------------------------------------------------------------------

require "lunit"
require "auxiliar"

lunit.import "all"


---------------------------------------------------------------------------------------------------
--                                     Fun��es                                                   --
---------------------------------------------------------------------------------------------------


-- ****************** TESTE DE GETKEYLABEL ****************** --

-- Testes para o getkeylabel do teclado (2 testes) --
tc = TestCase("keyboard.getkeylabel")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getkeylabel1                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o keyboard.getkeylabel, verifica��o da fun��o--
--            passando '0' como par�metro                                                        --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de getkeylabel com valor 0 (1/3)--
function tc.test_getkeylabel1()
    display.clear()

    ret = keyboard.getkeylabel(0)

    assert_nil(ret, "Erro no label retornado")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getkeylabel2                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o keyboard.getkeylabel, verifica��o da fun��o--
--            passando 'KEY_FOUR' como par�metro                                                 --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de getkeylabel com tecla 4 (2/3)--
function tc.test_getkeylabel2()
    display.clear()

    ret = keyboard.getkeylabel(KEY_FOUR)

    assert_equal("4", ret, "Erro no label retornado")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getkeylabel3                                                                  --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o keyboard.getkeylabel, verifica��o da fun��o--
--            passando 'KEY_UP' como par�metro                                                   --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de getkeylabel com tecla UP (3/3)--
function tc.test_getkeylabel3()
    display.clear()

    ret = keyboard.getkeylabel(KEY_UP)

    assert_equal("up", ret, "Erro no label retornado")
end


-- ****************** TESTE DE GETKEYSTROKE ****************** --

-- Testes para o getkeystroke do teclado (4 testes)--
tc2 = TestCase("keyboard.getkeystroke")

-----------------------------------------------------------------------------------------------------
--                                                                                                 --
--  Nome:     test_getkeystroke1                                                                   --
--                                                                                                 --
--  Descricao:                                                                                     --
--            Funcao respons�vel por validar a fun��o keyboard.getkeystroke, fica lendo e exibindo --
--            as teclas que o usu�rio aperta, at� que a tecla '0' seja presionada. E no fim do     --
--            teste questiona ao usu�rio sobre a corretude do teste                                --
--                                                                                                 --
--  Argumentos:                                                                                    --
--                                                                                                 --
--                                                                                                 --
--  Dependencias:                                                                                  --
--                                                                                                 --
--                                                                                                 --
--  Retornos:                                                                                      --
--    Casos de erro:                                                                               --
--        Nenhum                                                                                   --
--    Casos de sucesso:                                                                            --
--        Nenhum                                                                                   --
-----------------------------------------------------------------------------------------------------

-- Teste de getkeystroke sem timeout (1/3)--
function tc2.test_getkeystroke1()
    repeat
        display.clear()
        display.print("Press any key",0,0)
        display.print("<0> para sair",1,0)
        display.print("Timeout: negativo",2,0)
        display.print("Tecla: ",3,0)

        key = keyboard.getkeystroke(-1)
        ret = comparaTecla(key)
    until ret == 1

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste do getkeystroke sem timeout")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getkeystroke2                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o keyboard.getkeystroke, fica lendo, com     --
--            timeout 2, e exibindo as teclas que o usu�rio aperta, at� que a tecla '0' seja     --
--            presionada. E no fim do teste questiona ao usu�rio sobre a corretude do teste      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de getkeystroke com timeout de 2 secs (2/3)--
function tc2.test_getkeystroke2()
    repeat
        display.clear()
        display.print("Press any key",0,0)
        display.print("<0> para sair",1,0)
        display.print("Timeout: 2 secs",2,0)
        display.print("Tecla: ",3,0)

        key = keyboard.getkeystroke(2)
        ret = comparaTecla(key)
    until ret == 1

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste do getkeystroke com timeout")
end

---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_getkeystroke3                                                                 --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o keyboard.getkeystroke, fica lendo, com     --
--            timeout 0, e exibindo as teclas que o usu�rio aperta, at� que a tecla '0' seja     --
--            presionada. E no fim do teste questiona ao usu�rio sobre a corretude do teste      --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de getkeystroke com timeout 0 (3/3)--
function tc2.test_getkeystroke3()

    local key = nil
    local timer = util.timer(10000) -- 10 segundos

    repeat

        if( key == nil) then
            if (timer:remaining() < 0) then
               assert_fail( "test_getkeystroke3. Tecla nao pode ser pressionada", true )
            end
        else
            timer = util.timer(10000)
        end
                                                                                     
        display.clear()
        display.print( "Press any key",0,0 )
        display.print( "<0> para sair",1,0 )
        display.print( "Timeout: 0 secs",2,0 )
        display.print( "Tecla: ",3,0 )

        key = keyboard.getkeystroke( 0 )
        ret = comparaTecla( key )
    until ( ret == 1 )

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "test_getkeystroke3. Erro no teste do getkeystroke com timeout")
end

-- ****************** TESTE DE IO.READ ****************** --

-- Testes para o io.read (1 testes)--
tc4 = TestCase("io.read")


---------------------------------------------------------------------------------------------------
--                                                                                               --
--  Nome:     test_ioread1                                                                       --
--                                                                                               --
--  Descricao:                                                                                   --
--            Funcao respons�vel por validar a fun��o io.read, teste tendo como entrada padr�o   --
--            o teclado                                                                          --
--                                                                                               --
--  Argumentos:                                                                                  --
--                                                                                               --
--                                                                                               --
--  Dependencias:                                                                                --
--                                                                                               --
--                                                                                               --
--  Retornos:                                                                                    --
--    Casos de erro:                                                                             --
--        Nenhum                                                                                 --
--    Casos de sucesso:                                                                          --
--        Nenhum                                                                                 --
---------------------------------------------------------------------------------------------------

-- Teste de io.read (1/1)--
-- Ler dados do teclado at� q o usu�rio pressione enter --
function tc4.test_ioread1()
    display.clear()

    display.print("Teste de io.read", 0, 0)
    display.print("Digite algo", 1, 0)
    display.print("<Enter> Encerra", 2, 0)

    local ret = io.read()

    display.clear()

    display.print("Dados Lidos:", 0, 0)
    display.print(ret, 1, 0)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste do io.read")
end


-- ****************** TESTE DE KEYBOARD.BEEPON ****************** --

-- Testes para o keyboard.beepon (1 testes)--
tc5 = TestCase("keyboard.beepon")

-- Teste de io.read (1/1)--
-- Ativa o beep e solicita a verificacao do usuario --
function tc5.test_beepon()
    display.clear()

    keyboard.beepon( true )
    assert_true( keyboard.beepon(), "Falha no retorno do beepon", false )

    display.print("Teste de BeepOn", 0, 0)
    display.print("Pressione teclas e", 1, 0)
    display.print("verifique o beep", 2, 0)
    display.print("(CANCEL) para sair", 3, 0)

    local ret = keyboard.getkeystroke( 0 )

    while ret ~= KEY_CANCEL do
          ret = keyboard.getkeystroke( 0 )
    end

    keyboard.beepon( false )
    assert_false( keyboard.beepon(), "Falha no retorno do beepon", false )

    display.clear()

    display.print("Resultado do Teste:", 0, 0)

    tecla = lerTecla()

    assert_equal(KEY_ONE, tecla, "Erro no teste de keyboard.beepon")
end


lunit.run()
