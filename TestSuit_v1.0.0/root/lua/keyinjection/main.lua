require "lunit"

lunit.import "all"

tc1 = TestCase("key_injection")

function tc1.test_storekey1()
    local keyArray = util.bytearray("6AC292FAA1315B4D858AB3A3D7D5933A")
    assert_true(keyinjection.storekey(KEY_TYPE_3DES_DATA, 3, keyArray),
        "Não foi possível armazenar a chave 3DES.")
end

function tc1.test_storekey2()
    local keyArray = util.bytearray("6AC292FAA1315B4D858AB3A3D7D5933A")
    local ksnArray = util.bytearray("FFFF9876543210E00000")
    assert_true(keyinjection.storekey(KEY_TYPE_DUKPT_DATA, 3, keyArray, ksnArray),
        "Não foi possível armazenar a chave DUKPT.")
end

lunit.run()