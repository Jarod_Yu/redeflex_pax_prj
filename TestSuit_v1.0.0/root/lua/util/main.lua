-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 util/main.lua                                                    --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de util                                   --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      1/6/2006         Criacao                              --
--  Jamerson Lima        jrfl         9844     28/06/2006       Rework(HFLEX-LUA-TSTR-CODE-INSP009)  --
--  Leandro Mitsuo       lmf          16742    05/09/2006       Corre��o de Falhas                   --
--  Leandro Mitsuo       lmf           NA      08/09/2006       Corre��o do teste bytearray_set1     --
--  Leonardo N�brega     lfn           NA      24/11/2006       Alterei o valor esperado no teste    --
--                                                              asciitobcd2.                         --
--  Leandro              lmf          19096    08/01/2007       Inclusao de novos testes             --
--  Leandro              lmf           NA      15/01/2007       Rework (HFLEX-LUA-TSTR-CODE-REV012)  --
--  Guilherme            gkmo         20745    12/02/2007       Corrigindo testes de timer.          --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

bcd_filename = "bcdtest"
-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   UTIL TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "util" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciintobcd1                                                                --
--  Descricao: Caso de sucesso. Passa uma string e o tamanho da string                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciintobcd1()

    local value = "1234567890"
    local index = value:len()

    local result = util.asciintobcd( value, index )

    assert_equal( value, Convert( result ), "test_util_asciintobcd1. Passa uma string e o tamanho da string. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciintobcd2                                                                --
--  Descricao: Caso de sucesso. Passa uma string e um tamanho menor que a string                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciintobcd2()

    local value = "1234567890"
    local expected_result = "1234"
    local index = 4

    local result = util.asciintobcd( value, index )

    assert_equal( value:sub( 1, 4 ), Convert( result ), "test_util_asciintobcd2. Passa uma string e um tamanho menor que a string. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciintobcd3                                                                --
--  Descricao: Passa uma string alfanumerica                                                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciintobcd3()

    local value = "asqwresffdhjtyi"
    local index = value:len()
    local error_message = ""
    local error_code = 0

    local result, error_message, error_code = util.asciintobcd( value, index )

    assert_nil( result, "test_util_asciintobcd3. Passa uma string alfanumerica. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALBCDCHAR, error_code, "test_util_asciintobcd3. Passa uma string alfanumerica. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALBCDCHAR ], error_message, "test_util_asciintobcd3. Passa uma string alfanumerica. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciintobcd4                                                                --
--  Descricao: Passa uma string vazia                                                                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciintobcd4()

    local error_message = ""
    local error_code = 0

    local result, error_message, error_code = util.asciintobcd( "", 1 )

    assert_nil( result, "test_util_asciintobcd4. Passa uma string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALBCDCHAR, error_code, "test_util_asciintobcd4. Passa uma string vazia. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALBCDCHAR ], error_message, "test_util_asciintobcd4. Passa uma string vazia. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciitobcd1                                                                 --
--  Descricao: Caso de sucesso. Passa uma string                                                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciitobcd1()

    local result = util.asciitobcd( "1234567890" )

    assert_equal( "1234567890", Convert( result ), "test_util_asciintobcd1. Passa uma string e o tamanho da string. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_asciitobcd2                                                                 --
--  Descricao: Passa uma string vazia                                                                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_asciitobcd2()

    local error_message = ""
    local error_code = 0

    local result, error_message, error_code = util.asciitobcd( "" )


    assert_equal( nil, result, "test_util_asciitobcd2. Passa uma string vazia. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bcdtoascii1                                                                 --
--  Descricao: Caso de sucesso. Passa um dado BCD                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bcdtoascii1()

    local bcd_message, error_message = LoadFileInBuffer( bcd_filename )

    assert_not_nil( bcd_message, error_message, true )

    local result = util.bcdtoascii( bcd_message )

    assert_equal( "319", result, "test_util_bcdtoascii1. Caso de sucesso. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bcdtoascii2                                                                 --
--  Descricao: Passando um valor valido                                                              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bcdtoascii2()

    local result, error_message, error_code = util.bcdtoascii( "a" )


    assert_equal( "61", result, "test_util_bcdtoascii2. Passando um valor valido. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bcdtoascii3                                                                 --
--  Descricao: Passando um valor vazio                                                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bcdtoascii3()

    local result = util.bcdtoascii( "" )

    assert_equal( "", result, "test_util_bcdtoascii3. Passando um valor vazio. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bytearray1                                                                  --
--  Descricao: Caso de sucesso. Cria um bytearray de 10 posicoes                                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bytearray1()

    local result = util.bytearray( 10 )

    assert_not_nil( result, "Caso de sucesso. Retorno incorreto", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bytearray2                                                                  --
--  Descricao: Passa um tamanho igual a  zero.                                                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bytearray2()

    local result, error_message, error_code = util.bytearray( 0 )

    assert_nil( result, "Passa um tamanho igual a  zero. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_util_bytearray2. Passa um tamanho igual a  zero. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_util_bytearray2. Passa um tamanho igual a  zero. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bytearray_set1                                                              --
--  Descricao: Caso de sucesso. Seta um valor no bytearray e verifica se um valor esta correto       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bytearray_set1()

    local b = util.bytearray( 1 )

    assert_not_nil( b, "Dependencia: bytearray nao foi criado", true )

    local result = b:set( 1, 61 )

    assert_true( result, "test_util_bytearray_set1. Caso de sucesso. Setando o valor '61' no byte array. Retorno incorreto", true )
    
    result = b:get( 1 )

    assert_equal( 61, result , "test_util_bytearray_set1. Caso de sucesso. Setando o valor 'a' no byte array. Valor nao foi escrito corretamente" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bytearray_get1                                                              --
--  Descricao: Caso de sucesso. Seta um valor no bytearray e recupera o valor com get                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bytearray_get1()

    local b = util.bytearray( 1 )

    assert_not_nil( b, "Dependencia: bytearray nao foi criado", true )

    local result = b:set( 1, 61 )

    assert_true( result, "Dependencia: Dado nao foi setado", true )

    result = b:get( 1 )

    assert_equal( 61, result, "test_util_bytearray_get1. Caso de sucesso. Recuperando dado de bytearray. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_bytearray_size1                                                             --
--  Descricao: Caso de sucesso. Cria um bytearray e verifica o tamanho                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_bytearray_size1()

    local b = util.bytearray( 10 )

    assert_not_nil( b, "Dependencia: bytearray nao foi criado", true )

    local result = b:size( )

    assert_equal( 10, result, "test_util_bytearray_size1. Caso de sucesso. Verificando o tamanho do bytearray. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_timer1                                                                      --
--  Descricao: Caso de sucesso. Cria um timer e verifica o retorno                                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_timer1()
    local timer = util.timer( 1000 )

    assert_not_nil( timer, "test_util_timer1. Caso de sucesso. Verificando o timer criado. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_remaining1                                                                  --
--  Descricao: Caso de sucesso. Cria um timer e verifica o retorno de timer:remaining                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_remaining1()
    local timer = util.timer( 10000 )

    management.sleep(100)

    assert_true( timer:remaining() < 10000 , "test_util_remaining1. Caso de sucesso. Verificando o retorno de timer:remaining. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_remaining2                                                                  --
--  Descricao: Caso de sucesso. Cria um timer espera expirar e verifica o retorno de timer:remaining --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_remaining2()
    local timer = util.timer( 1000 )

    management.sleep( 1000 )

    assert_true( timer:remaining() <= 0 , "test_util_remaining2. Caso de sucesso. Verificando o retorno de timer:remaining. Retorno incorreto" )
end


-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_util_elapsed1                                                                    --
--  Descricao: Caso de sucesso. Cria um timer e verifica o retorno de timer:elapsed                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_util_elapsed1()
    local timer = util.timer( 1000 )

    management.sleep( 1000 )

    assert_true( timer:elapsed() >= 1000 , "test_util_elapsed1. Caso de sucesso. Verificando o retorno de timer:elapsed. Retorno incorreto" )
end

lunit.run()
