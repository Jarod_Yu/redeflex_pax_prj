-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:              main.lua                                                            --
-- Tres Letras Representativas:  TST                                                                 --
-- Descricao:                                                                                        --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      28/03/2006         Cria��o                            --
--  Jamerson Lima        jrfl          NA      10/04/2006         Rework(HFLEX-TSTR-LUA-CODE-INSP002)--
--  Jamerson Lima        jrfl          NA      06/06/2006         Retirando checagem de msg de erro  --
--  Jamerson Lima        jrfl         9844     24/07/2006         Testes de getnumber & getboolean   --
--  Jamerson Lima        jrfl         9844     26/07/2006         Testes de arq. de prop. global     --
--  Leandro Mitsuo       lmf          16704    11/09/2006         Corre��o do teste getnumber10      --
--  Leandro Mitsuo       lmf          16706    11/09/2006         Corre��o de testes getboolean      --
-------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------
--Para rodar estes testes, os seguintes arquivos devem existir no POS:                                                        --
--   Nome     Conteudo                                                                                                        --
-- ap01.txt   (telefone=32220101)                                                                                             --
-- ap02.txt   (telefone = 32220101)                                                                                           --
-- ap03.txt   (32220101)                                                                                                      --
-- ap04.txt   ()                                                                                                              --
-- ap05.txt   (telefone=)                                                                                                     --
-- ap06.txt   ( =32220101)                                                                                                    --
-- ap07.txt   (telefone=32220101\nTELEFONE=3441-2211)                                                                         --
-- ap08.txt   (tele=fone=32220101)                                                                                            --
-- ap09.txt   (! @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \ | " '=00)                                              --
-- ap10.txt   (carac=! @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \ | " ')                                           --
-- ap11.txt   (telefone=11111111111111111111111111111111111111111111...)                                                      --
-- ap12.txt   (                                                        )                                                      --
-- ap13.txt   (telefone=22222222\ntelefone=33333333)                                                                          --
-- ap14.txt   (telefone=32220101\np.a.b.x.=99\nconn time out=10 ms\nTELEFONE=3441-2211\non demand=1\ndecisao=-1\nnumero=3.25) --
-- ap15       (telefone=32220101\np.a.b.x.=99\nconn time out=10 ms\nTELEFONE=3441-2211\non demand=1\ndecisao=-1\nnumero=3.25) --
-- AP16.TXT   (telefone=32220101\np.a.b.x.=99\nconn time out=10 ms\nTELEFONE=3441-2211\non demand=1\ndecisao=-1\nnumero=3.25) --
-- ap17.txt   (telefone=32220101\np.a.b.x.=99\nconn time out=10 ms\nTELEFONE=3441-2211\non demand=1\ndecisao=-1\nnumero=3.25) --
--------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                   Variaveis Globais                                               --
-------------------------------------------------------------------------------------------------------

--se for true mostra informacoes para debug
debug = false

--table dados usados nos casos de testes
-- os dados sao os seguintes:
----description: descricao do teste atual
----filename: nome do arquivo de propriedades passado para a funcao testada(property.open)
----success: booleano que define se no teste atual a funcao testada deve executar com sucesso ou nao
----expected_error_code: codigo de erro retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
----expected_error_message: mensagem de erro descrevendo o codigo retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)

property_open_tests_definitions = {
    {
        --Teste 01
        [ "description" ] = "Arquivo normal com uma linha",
        [ "filename" ] = "ap01.txt",
        [ "success" ] = true,
    },
    {
        --Teste 02
        [ "description" ] = "Arquivo Normal com espa�os",
        [ "filename" ] = "ap02.txt",
        [ "success" ] = true,
    },
    {
        --Teste 03
        [ "description" ] = "Apenas com um texto",
        [ "filename" ] = "ap03.txt",
        [ "success" ] = true,
    },
    {
        --Teste 04
        [ "description" ] = "vazio",
        [ "filename" ] = "ap04.txt",
        [ "success" ] = true,
    },
    {
        --Teste 05
        [ "description" ] = "Chave sem valor",
        [ "filename" ] = "ap05.txt",
        [ "success" ] = true,
    },
    {
        --Teste 06
        [ "description" ] = "Valor sem chave",
        [ "filename" ] = "ap06.txt",
        [ "success" ] = true,
    },
    {
        --Teste 07
        [ "description" ] = "Alterando o case",
        [ "filename" ] = "ap07.txt",
        [ "success" ] = true,
    },
    {
        --Teste 08
        [ "description" ] = " mais de um '=' em uma linha",
        [ "filename" ] = "ap08.txt",
        [ "success" ] = true,
    },
    {
        --Teste 09
        [ "description" ] = "Chave com caracteres nao alfa numericos",
        [ "filename" ] = "ap09.txt",
        [ "success" ] = true,
    },
    {
        --Teste 10
        [ "description" ] = "Valor com caracteres nao alfa numericos",
        [ "filename" ] = "ap10.txt",
        [ "success" ] = true,
    },
    {
        --Teste 11
        [ "description" ] = "Linha maior que o limite maximo",
        [ "filename" ] = "ap11.txt",
        [ "success" ] = true,
    },
    {
        --Teste 12
        [ "description" ] = "Linha em branco",
        [ "filename" ] = "ap12.txt",
        [ "success" ] = true,
    },
    {
        --Teste 13
        [ "description" ] = "Chave duplicada",
        [ "filename" ] = "ap13.txt",
        [ "success" ] = true,
    },
    {
        --Teste 14
        [ "description" ] = "Arquivo normal",
        [ "filename" ] = "ap14.txt",
        [ "success" ] = true,
    },
    {
        --Teste 15
        [ "description" ] = "Arquivo normal sem extensao",
        [ "filename" ] = "ap15",
        [ "success" ] = true,
    },
    {
        --Teste 16
        [ "description" ] = "Arquivo normal com nome maiusculo",
        [ "filename" ] = "AP16.TXT",
        [ "success" ] = true,
    },
    {
        --Teste 17
        [ "description" ] = "Abrir um arquivo aberto",
        [ "filename" ] = "ap14.txt",
        [ "success" ] = nil,
        [ "expected_error_code" ] = TST_ERR_ISOPEN,
        [ "expected_error_message" ] = "file already open",
    },
    {
        --Teste 18
        [ "description" ] = "Abrir com o numero maximo de arquivos abertos atingido",
        [ "filename" ] = "ap14.txt",
        [ "success" ] = nil,
        [ "expected_error_code" ] = TST_ERR_OPENFILES,
        [ "expected_error_message" ] = "too many files opened",
    },
    {
        --Teste 19
        [ "description" ] = "Filename com caracteres invalidos",
        [ "filename" ] = "@:?.txt",
        [ "success" ] = nil,
        [ "expected_error_code" ] = TST_ERR_PATHERR,
        [ "expected_error_message" ] = "invalid path",
    },
    {
        --Teste 20
        [ "description" ] = "Abrir um arquivo inexistente",
        [ "filename" ] = "ne.txt",
        [ "success" ] = nil,
        [ "expected_error_code" ] = TST_ERR_NOTFOUND,
        [ "expected_error_message" ] = "file/directory not found",
    }
}

--table dados usados nos casos de testes de getstring
-- os dados sao os seguintes:
----description: descricao do teste atual
----filename: nome do arquivo de propriedades usado no teste
----key: chave passada para a funcao testada(property.getstring)
----value: valor retornado esperado
----expected_error_code: codigo de erro retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
----expected_error_message: mensagem de erro descrevendo o codigo retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
property_getstring_tests_definitions = {
    {
        --Teste 01
        [ "description" ] = "Caso de sucesso",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = "32220101",
    },
    {
        --Teste 02
        [ "description" ] = "Chave vazia",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 03
        [ "description" ] = "chave_inexistente",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 04
        [ "description" ] = "Chave e valor com espacos",
        [ "filename" ] = "ap02.txt",
        [ "key" ] = "telefone ",
        [ "expected_value"] = " 32220101",
    },
    {
        --Teste 05
        [ "description" ] = "Chave com valor vazio",
        [ "filename" ] = "ap05.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = "",
    },
    {
        --Teste 06
        [ "description" ] = "Chave com valor contendo '='",
        [ "filename" ] = "ap08.txt",
        [ "key" ] = "tele",
        [ "expected_value"] = "fone=32220101"
    },
    {
        --Teste 07
        [ "description" ] = "Chave com caracteres especiais",
        [ "filename" ] = "ap09.txt",
        [ "key" ] = " ! # @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \\ | \" '",
        [ "expected_value"] = "00"
    },
    {
        --Teste 08
        [ "description" ] = "Valor com caracteres especiais",
        [ "filename" ] = "ap10.txt",
        [ "key" ] = "carac",
        [ "expected_value"] = "! # @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \\ | \" '"
    },
    {
        --Teste 09
        [ "description" ] = "Chave de uma linha maior que o limite",
        [ "filename" ] = "ap11.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ]
    },
    {
        --Teste 10
        [ "description" ] = "Chave duplicada",
        [ "filename" ] = "ap13.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = "22222222"
    }
}

--table dados usados nos casos de testes de getnumber
-- os dados sao os seguintes:
----description: descricao do teste atual
----filename: nome do arquivo de propriedades usado no teste
----key: chave passada para a funcao testada(property.getnumber)
----value: valor retornado esperado
----expected_error_code: codigo de erro retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
----expected_error_message: mensagem de erro descrevendo o codigo retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
property_getnumber_tests_definitions = {
    {
        --Teste 01
        [ "description" ] = "Caso de sucesso com numero inteiro",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = 32220101,
    },
    {
        --Teste 02
        [ "description" ] = "Caso de sucesso com numero ponto flutuante",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "numero",
        [ "expected_value"] = 3.25,
    },
    {
        --Teste 03
        [ "description" ] = "Chave vazia",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 04
        [ "description" ] = "chave_inexistente",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 05
        [ "description" ] = "Chave e valor com espacos",
        [ "filename" ] = "ap02.txt",
        [ "key" ] = "telefone ",
        [ "expected_value"] = 32220101,
    },
    {
        --Teste 06
        [ "description" ] = "Chave com valor vazio",
        [ "filename" ] = "ap05.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = 0,
    },
    {
        --Teste 07
        [ "description" ] = "Chave com caracteres especiais",
        [ "filename" ] = "ap09.txt",
        [ "key" ] = " ! # @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \\ | \" '",
        [ "expected_value"] = 0
    },
    {
        --Teste 08
        [ "description" ] = "Chave de uma linha maior que o limite",
        [ "filename" ] = "ap11.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ]
    },
    {
        --Teste 09
        [ "description" ] = "Chave duplicada",
        [ "filename" ] = "ap13.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = 22222222
    },
    {
        --Teste 10
        [ "description" ] = "Tenta pegar valor nao-numerico",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "conn time out",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_UNKNOWNFORMAT,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_UNKNOWNFORMAT ]
    }
}

--table dados usados nos casos de testes de getboolean
-- os dados sao os seguintes:
----description: descricao do teste atual
----filename: nome do arquivo de propriedades usado no teste
----key: chave passada para a funcao testada(property.getboolean)
----value: valor retornado esperado
----expected_error_code: codigo de erro retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
----expected_error_message: mensagem de erro descrevendo o codigo retornado pela funcao testada(so eh valido se a funcao nao for executada corretamente)
property_getboolean_tests_definitions = {
    {
        --Teste 01
        [ "description" ] = "Caso de sucesso com numero inteiro",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "on demand",
        [ "expected_value"] = true,
    },
    {
        --Teste 02
        [ "description" ] = "Numero inteiro invalido",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "decisao",
        [ "expected_value"] = true,
    },
    {
        --Teste 03
        [ "description" ] = "Chave vazia",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 04
        [ "description" ] = "chave_inexistente",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ],
    },
    {
        --Teste 05
        [ "description" ] = "Chave com valor vazio",
        [ "filename" ] = "ap05.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = true,
    },
    {
        --Teste 06
        [ "description" ] = "Chave de uma linha maior que o limite",
        [ "filename" ] = "ap11.txt",
        [ "key" ] = "telefone",
        [ "expected_value"] = nil,
        [ "expected_error_code" ] = TST_ERR_KEYNOTFOUND,
        [ "expected_error_message" ] = lua_error_message[ TST_ERR_KEYNOTFOUND ]
    },
    {
        --Teste 07
        [ "description" ] = "Tenta pegar valor nao-booleano",
        [ "filename" ] = "ap14.txt",
        [ "key" ] = "conn time out",
        [ "expected_value"] = true,
    }
}

-------------------------------------------------------------------------------------------------------
--                                   Funcoes                                                         --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--  Nome:         test_property_open                                                                 --
--  Descricao:    Funcao de teste do property.open                                                  --
--  Argumentos:                                                                                      --
--                1. test_number: indice inteiro do teste, indica qual teste sera rodado, de acordo  --
--                   com a planilha de testes                                                        --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--                true: Se os testes forem bem sucedidos                                             --
--                false, mensagem de erro: Se houver algum erro nos testes                           --
-------------------------------------------------------------------------------------------------------
function test_property_open( test_number )

    local test_description = property_open_tests_definitions[ test_number ][ "description" ]
    local filename = property_open_tests_definitions[ test_number ][ "filename" ]
    local success = property_open_tests_definitions[ test_number ][ "success" ]
    local expected_error_code = property_open_tests_definitions[ test_number ][ "expected_error_code" ]
    --local expected_error_message = property_open_tests_definitions[ test_number ][ "expected_error_message" ]

    local handle, error_message, error_code = property.open( filename );

    if ( debug ) then
        printer.print( tostring( test_description ) )
        printer.print( tostring( filename ) )
        printer.print( tostring( success ) )
        printer.print( tostring( expected_error_code ) )
        printer.print( tostring( expected_error_message ) )
        printer.print( tostring( handle ) )
        printer.print( tostring( error_message ) )
        printer.print( tostring( error_code ) )
    end

    if( success ) then
        --a funcao chamada deve ser executada com sucesso

        --testa se foi alocado algo no handle retornado
        if( handle == nil ) then
            return false, string.format( "(%i) %s. Handle nulo, arquivo '%s' nao foi aberto", test_number, test_description, filename )
        else

            property.close( handle )
        end

    else
        --a funcao chamada deve falhar a execucao

        --verifica se o handle eh diferente de nil, o codigo e a mensagem de erro retornados
        if( handle ~= nil ) then
            return false, string.format( "(%i) %s. Handle diferente de nulo", test_number, test_description )
        end

        if( error_code ~= expected_error_code ) then
            return false, string.format( "(%i) %s. Codigo de erro esperado: %i, obtido: %i", test_number, test_description, expected_error_code, error_code )
        end

        -- if( error_message ~= expected_error_message ) then
--             return false, string.format( "(%i) %s. Mensagem de erro esperada: %s, obtida: %s", test_number, test_description, expected_error_message, error_message )
--         end
    end

    return true
end

-------------------------------------------------------------------------------------------------------
--  Nome:         test_property_getstring                                                            --
--  Descricao:    Funcao de teste do property:getstring                                              --
--  Argumentos:                                                                                      --
--                1. test_number: indice inteiro do teste, indica qual teste sera rodado, de acordo  --
--                   com a planilha de testes                                                        --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--                true: Se os testes forem bem sucedidos                                             --
--                false, mensagem de erro: Se houver algum erro nos testes                           --
-------------------------------------------------------------------------------------------------------
function test_property_getstring( test_number )

    local test_description = property_getstring_tests_definitions[ test_number ][ "description" ]
    local filename = property_getstring_tests_definitions[ test_number ][ "filename" ]
    local key = property_getstring_tests_definitions[ test_number ][ "key" ]
    local expected_value = property_getstring_tests_definitions[ test_number ][ "expected_value" ]
    local expected_error_code = property_getstring_tests_definitions[ test_number ][ "expected_error_code" ]
    --local expected_error_message = property_getstring_tests_definitions[ test_number ][ "expected_error_message" ]

    --abre o arquivo de propriedades antes do teste
    handle = property.open( filename );

    --testa se foi alocado algo no handle retornado
    if( handle == nil ) then
        return false, string.format( "Dependencia: Handle do arquivo %s nao foi criado", filename )
    end

    --chama a funcao testada
    value, error_message, error_code = handle:getstring( key )

    --fecha handle aberto

    if ( debug ) then
        printer.print( tostring( test_description ) )
        printer.print( tostring( filename ) )
        printer.print( tostring( key ) )
        printer.print( tostring( value ) )
        printer.print( tostring( expected_value ) )
        printer.print( tostring( expected_error_code ) )
        printer.print( tostring( expected_error_message ) )
        printer.print( tostring( handle ) )
        printer.print( tostring( error_message ) )
        printer.print( tostring( error_code ) )
    end
    
    handle:close()

    if( value ~= expected_value ) then
        return false, string.format( "(%i) %s. Valor esperado: %s, obtido: %s", test_number, test_description, tostring( expected_value ), tostring( value ) )
    end

    if( error_code ~= expected_error_code ) then
        return false, string.format( "(%i) %s. Codigo de erro esperado: %i, obtido: %i", test_number, test_description, expected_error_code, error_code )
    end

    -- if( error_message ~= expected_error_message ) then
--         return false, string.format( "(%i) %s. Mensagem de erro esperada: %s, obtida: %s", test_number, test_description, expected_error_message, tostring( error_message ) )
--     end

    return true
end

-------------------------------------------------------------------------------------------------------
--  Nome:         test_property_getnumber                                                            --
--  Descricao:    Funcao de teste do property:getnumber                                              --
--  Argumentos:                                                                                      --
--                1. test_number: indice inteiro do teste, indica qual teste sera rodado, de acordo  --
--                   com a planilha de testes                                                        --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--                true: Se os testes forem bem sucedidos                                             --
--                false, mensagem de erro: Se houver algum erro nos testes                           --
-------------------------------------------------------------------------------------------------------
function test_property_getnumber( test_number )

    local test_description = property_getnumber_tests_definitions[ test_number ][ "description" ]
    local filename = property_getnumber_tests_definitions[ test_number ][ "filename" ]
    local key = property_getnumber_tests_definitions[ test_number ][ "key" ]
    local expected_value = property_getnumber_tests_definitions[ test_number ][ "expected_value" ]
    local expected_error_code = property_getnumber_tests_definitions[ test_number ][ "expected_error_code" ]
    --local expected_error_message = property_getnumber_tests_definitions[ test_number ][ "expected_error_message" ]

    --abre o arquivo de propriedades antes do teste
    handle = property.open( filename );

    --testa se foi alocado algo no handle retornado
    if( handle == nil ) then
        return false, string.format( "Dependencia: Handle do arquivo %s nao foi criado", filename )
    end

    --chama a funcao testada
    value, error_message, error_code = handle:getnumber( key )

    --fecha handle aberto

    if ( debug ) then
        printer.print( tostring( test_description ) )
        printer.print( tostring( filename ) )
        printer.print( tostring( key ) )
        printer.print( tostring( value ) )
        printer.print( tostring( expected_value ) )
        printer.print( tostring( expected_error_code ) )
        printer.print( tostring( expected_error_message ) )
        printer.print( tostring( handle ) )
        printer.print( tostring( error_message ) )
        printer.print( tostring( error_code ) )
    end
    
    handle:close()

    if( value ~= expected_value ) then
        return false, string.format( "(%i) %s. Valor esperado: %s, obtido: %s", test_number, test_description, tostring( expected_value ), tostring( value ) )
    end

    if( error_code ~= expected_error_code ) then
        return false, string.format( "(%i) %s. Codigo de erro esperado: %i, obtido: %i", test_number, test_description, expected_error_code, error_code )
    end

    -- if( error_message ~= expected_error_message ) then
--         return false, string.format( "(%i) %s. Mensagem de erro esperada: %s, obtida: %s", test_number, test_description, expected_error_message, tostring( error_message ) )
--     end

    return true
end

-------------------------------------------------------------------------------------------------------
--  Nome:         test_property_getboolean                                                           --
--  Descricao:    Funcao de teste do property:getboolean                                             --
--  Argumentos:                                                                                      --
--                1. test_number: indice inteiro do teste, indica qual teste sera rodado, de acordo  --
--                   com a planilha de testes                                                        --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--                true: Se os testes forem bem sucedidos                                             --
--                false, mensagem de erro: Se houver algum erro nos testes                           --
-------------------------------------------------------------------------------------------------------
function test_property_getboolean( test_number )

    local test_description = property_getboolean_tests_definitions[ test_number ][ "description" ]
    local filename = property_getboolean_tests_definitions[ test_number ][ "filename" ]
    local key = property_getboolean_tests_definitions[ test_number ][ "key" ]
    local expected_value = property_getboolean_tests_definitions[ test_number ][ "expected_value" ]
    local expected_error_code = property_getboolean_tests_definitions[ test_number ][ "expected_error_code" ]
    --local expected_error_message = property_getboolean_tests_definitions[ test_number ][ "expected_error_message" ]

    --abre o arquivo de propriedades antes do teste
    handle = property.open( filename );

    --testa se foi alocado algo no handle retornado
    if( handle == nil ) then
        return false, string.format( "Dependencia: Handle do arquivo %s nao foi criado", filename )
    end

    --chama a funcao testada
    value, error_message, error_code = handle:getboolean( key )

    --fecha handle aberto

    if ( debug ) then
        printer.print( tostring( test_description ) )
        printer.print( tostring( filename ) )
        printer.print( tostring( key ) )
        printer.print( tostring( value ) )
        printer.print( tostring( expected_value ) )
        printer.print( tostring( expected_error_code ) )
        printer.print( tostring( expected_error_message ) )
        printer.print( tostring( handle ) )
        printer.print( tostring( error_message ) )
        printer.print( tostring( error_code ) )
    end
    
    handle:close()

    if( value ~= expected_value ) then
        return false, string.format( "(%i) %s. Valor esperado: %s, obtido: %s", test_number, test_description, tostring( expected_value ), tostring( value ) )
    end

    if( error_code ~= expected_error_code ) then
        return false, string.format( "(%i) %s. Codigo de erro esperado: %i, obtido: %i", test_number, test_description, expected_error_code, error_code )
    end

    -- if( error_message ~= expected_error_message ) then
--         return false, string.format( "(%i) %s. Mensagem de erro esperada: %s, obtida: %s", test_number, test_description, expected_error_message, tostring( error_message ) )
--     end

    return true
end

-------------------------------------------------------------------------------------------------------
--                                   PROPERTY.OPEN TEST CASES                                        --
-------------------------------------------------------------------------------------------------------

tc_property_open = TestCase("property.open")

-------------------------------------------------------------------------------------------------------
--  Nome:       tc_property_open:setup                                                               --
--  Descricao:  Funcao que verifica a existencia dos arquivos para o teste                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open:setup( )

    --casos de teste em que a existencia do arquivo nao eh necessaria
    if( ( self.__lunit_test_number ~= 19 ) and ( self.__lunit_test_number ~= 20 ) ) then
        --pega o arquivo testado de acordo com o teste atual
        local filename = property_open_tests_definitions[ self.__lunit_test_number ][ "filename" ]

        --caso o arquivo nao seja encontrado eh gerado um erro
        if( not os.exists( filename ) ) then
            --a chamada da funcao error() do lua impede que o lunit rode o caso de teste atual
            error( string.format( "Dependencia: arquivo %s nao encontrado", filename ) )
            return
        end
    end
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open1                                                                  --
--  Descricao:  Arquivo normal com uma linha                                                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open1( )

    --arquivo usado neste teste: ap01.txt
    return_value, error_message = test_property_open( 1 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open2                                                                  --
--  Descricao:  Arquivo Normal com espacos                                                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open2( )

    --arquivo usado neste teste: ap02.txt
    return_value, error_message = test_property_open( 2 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open3                                                                  --
--  Descricao:  Apenas com um texto                                                                  --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open3( )

    --arquivo usado neste teste: ap03.txt
    return_value, error_message = test_property_open( 3 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open4                                                                  --
--  Descricao:  Arquivo vazio                                                                        --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open4( )

    --arquivo usado neste teste: ap04.txt
    return_value, error_message = test_property_open( 4 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open5                                                                  --
--  Descricao:  Chave sem valor                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open5( )

    --arquivo usado neste teste: ap01.txt
    return_value, error_message = test_property_open( 5 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open6                                                                  --
--  Descricao:  Valor sem chave                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open6( )

    --arquivo usado neste teste: ap06.txt
    return_value, error_message = test_property_open( 6 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open7                                                                  --
--  Descricao:  Alterando o case                                                                     --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open7( )

    --arquivo usado neste teste: ap07.txt
    return_value, error_message = test_property_open( 7 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open8                                                                  --
--  Descricao:  Mais de um '=' em uma linha                                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open8( )

    --arquivo usado neste teste: ap08.txt
    return_value, error_message = test_property_open( 8 )

   assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open9                                                                  --
--  Descricao:  Chave com caracteres nao alfa numericos                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open9( )

    --arquivo usado neste teste: ap09.txt
    return_value, error_message = test_property_open( 9 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open10                                                                 --
--  Descricao:  Valor com caracteres nao alfa numericos                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open10( )

    --arquivo usado neste teste: ap10.txt
    return_value, error_message = test_property_open( 10 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open11                                                                 --
--  Descricao:  linha maior que o limite maximo                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open11( )

    --arquivo usado neste teste: ap11.txt
    return_value, error_message = test_property_open( 11 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open12                                                                 --
--  Descricao:  Linha em branco                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open12( )

    --arquivo usado neste teste: ap12.txt
    return_value, error_message = test_property_open( 12 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open13                                                                 --
--  Descricao:  Chave duplicada                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open13( )

    --arquivo usado neste teste: ap13.txt
    return_value, error_message = test_property_open( 13 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open14                                                                 --
--  Descricao:  Arquivo normal                                                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open14( )

    --arquivo usado neste teste: ap14.txt
    return_value, error_message = test_property_open( 14 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open15                                                                 --
--  Descricao:  Arquivo normal sem extensao                                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open15( )

    --arquivo usado neste teste: ap15.txt
    return_value, error_message = test_property_open( 15 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open16                                                                 --
--  Descricao:  Arquivo normal com nome maiusculo                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open16( )

    --arquivo usado neste teste: AP16.TXT
    return_value, error_message = test_property_open( 16 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open18                                                                 --
--  Descricao:  Abrir com o numero maximo de arquivos abertos atingido                               --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open18( )

    --arquivos a serem abertos antes de chamar a funcao de testes
    local filenames = {
        "ap01.txt",
        "ap02.txt",
        "ap03.txt",
        "ap04.txt",
        "ap05.txt",
        "ap06.txt",
        "ap07.txt",
        "ap08.txt",
        "ap09.txt",
        "ap10.txt",
        "ap11.txt",
        "ap12.txt",
        "ap13.txt",
        "ap14.txt"
    }
    
    --array que guardara os handles abertos
    local file_handles = {}

    local error_message = ""
    local error_code = 0
    local i = 1

    --abre todos os arquivos do array ate retornar o erro de todos os arquivos abertos
    repeat
        file_handles[ i ], error_message, error_code = io.open( filenames[ i ], "rw" )
        i = i + 1
    until ( ( error_code == TST_ERR_OPENFILES ) or ( i >= 14 ) )

    --verifica se a condicao de saida foi o erro de numero maximo de arquivos abertos, se nao for fecha os abertos e retorna um erro de dependencia
    if( error_code ~= TST_ERR_OPENFILES ) then
        for j=1,14 do
            if( file_handles[ j ] ~= nil ) then
                file_handles[ j ]:close()
            end
        end
        assert_fail( "Dependencia: numero de arquivos abertos insuficientes" )
        return
    end

    --arquivo usado neste teste: ap14.txt
    return_value, error_message = test_property_open( 18 )

    --fecha todos os handles abertos
    for j=1,14 do
            if( file_handles[ j ] ~= nil ) then
                file_handles[ j ]:close()
            end
        end

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open19                                                                 --
--  Descricao:  Filename com caracteres invalidos                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open19( )

    --arquivo usado neste teste: nenhum (string passada: @:?.txt)
    return_value, error_message = test_property_open( 19 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open20                                                                 --
--  Descricao:  Abrir um arquivo inexistente                                                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open20( )

    --arquivo usado neste teste: nenhum (string passada: ne.txt)
    return_value, error_message = test_property_open( 20 )

    assert_true( return_value, error_message )
end
-------------------------------------------------
----TESTES DE ARQUIVOS DE PROPRIEDADES GLOBAL----
-------------------------------------------------

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_open21                                                                 --
--  Descricao:  Abrir um arquivo global                                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_open.test_property_open21( )

    --cria um arquivo de propriedades global
    local handle = io.open( "gprop.txt", "rwg" )

    assert_not_nil( handle, "Dependencia: Nao foi possivel criar handle de arquivo global", true )

    handle:write( "nome=aaa" )
    handle:close()

    local prop_file = property.open( "gprop.txt", "shared" )

    if( not assert_not_nil( prop_file, "test_property_open21. Abrindo arquivo de propriedades global. Handle nao foi criado" ) ) then
        os.remove( "gprop.txt", true )
        return
    end

    local prop_value = prop_file:getstring( "nome" )

    assert_equal( "aaa", prop_value, "test_property_open21. Abrindo arquivo de propriedades global. Propriedade nao pode ser recuperada" )

    prop_file:close()

    os.remove( "gprop.txt", true )
end

-------------------------------------------------------------------------------------------------------
--                                   PROPERTY.CLOSE TEST CASES                                       --
-------------------------------------------------------------------------------------------------------

tc_property_close = TestCase("property.close")

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_close1                                                                 --
--  Descricao:  Caso de Sucesso. Fecha handle valido                                                 --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_close.test_property_close1( )

    assert_true( os.exists( "ap14.txt" ), "Dependencia: ap14.txt nao encontrado", true )

    --abre um handle do arquivo ap14.txt(Arquivo de configuracao normal) para o teste
    local handle = property.open( "ap14.txt" )
    local return_value = true

    if( handle == nil ) then
        assert_fail( "Dependencia: ap14.txt nao pode ser aberto" )
        return
    end

    --chama funcao testada
    return_value = property.close( handle )

    assert_true( return_value, "(1) Caso de sucesso. Retorno incorreto", true )

    --tenta chamar um metodo de um handle fechado, deve dar erro (o pcall ira tratar o erro gerado)
    return_value = pcall( function() handle:getstring( "telefone" ) end )

    assert_false( return_value, "(1) Caso de sucesso. Handle nao foi fechado" )
end

-------------------------------------------------------------------------------------------------------
--                                   PROPERTY:GETSTRING TEST CASES                                   --
-------------------------------------------------------------------------------------------------------
tc_property_getstring = TestCase("property:getstring")

-------------------------------------------------------------------------------------------------------
--  Nome:       tc_property_getstring:setup                                                          --
--  Descricao:  Funcao que verifica a existencia dos arquivos para o teste                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring:setup( )

    --pega o arquivo testado de acordo com o teste atual
    local filename = property_getstring_tests_definitions[ self.__lunit_test_number ][ "filename" ]

    --caso o arquivo nao seja encontrado eh gerado um erro
    if( not FileExists( filename ) ) then
        --a chamada da funcao error() do lua impede que o lunit rode o caso de teste atual
        error( string.format( "Dependencia: arquivo %s nao encontrado", filename ) )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring1                                                             --
--  Descricao:  Caso de sucesso                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring1( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getstring( 1 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring2                                                             --
--  Descricao:  Chave vazia                                                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring2( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: ""
    return_value, error_message = test_property_getstring( 2 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring3                                                             --
--  Descricao:  Chave inexistente                                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring3( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "chave_inexistente"
    return_value, error_message = test_property_getstring( 3 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring4                                                             --
--  Descricao:  Chave e valor com espacos                                                            --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring4( )

    --arquivo de propriedades usado neste teste: ap02.txt
    --chave usada neste teste: "telefone "
    return_value, error_message = test_property_getstring( 4 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring5                                                             --
--  Descricao:  Chave com valor vazio                                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring5( )

    --arquivo de propriedades usado neste teste: ap05.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getstring( 5 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring6                                                             --
--  Descricao:  Chave com valor contendo '='                                                         --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring6( )

    --arquivo de propriedades usado neste teste: ap08.txt
    --chave usada neste teste: "tele"
    return_value, error_message = test_property_getstring( 6 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring7                                                             --
--  Descricao:  Chave com caracteres especiais                                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring7( )

    --arquivo de propriedades usado neste teste: ap09.txt
    --chave usada neste teste: "! # @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \\ | \" '"
    return_value, error_message = test_property_getstring( 7 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring8                                                             --
--  Descricao:  Valor com caracteres especiais                                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring8( )

    --arquivo de propriedades usado neste teste: ap10.txt
    --chave usada neste teste: "carac"
    return_value, error_message = test_property_getstring( 8 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring9                                                             --
--  Descricao:  Chave de uma linha maior que o limite                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring9( )

    --arquivo de propriedades usado neste teste: ap11.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getstring( 9 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getstring10                                                            --
--  Descricao:  Chave duplicada                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getstring.test_property_getstring10( )

    --arquivo de propriedades usado neste teste: ap13.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getstring( 10 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--                                   PROPERTY:GETNUMBER TEST CASES                                   --
-------------------------------------------------------------------------------------------------------
tc_property_getnumber = TestCase("property:getnumber")

-------------------------------------------------------------------------------------------------------
--  Nome:       tc_property_getnumber:setup                                                          --
--  Descricao:  Funcao que verifica a existencia dos arquivos para o teste                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber:setup( )

    --pega o arquivo testado de acordo com o teste atual
    local filename = property_getnumber_tests_definitions[ self.__lunit_test_number ][ "filename" ]

    --caso o arquivo nao seja encontrado eh gerado um erro
    if( not FileExists( filename ) ) then
        --a chamada da funcao error() do lua impede que o lunit rode o caso de teste atual
        error( string.format( "Dependencia: arquivo %s nao encontrado", filename ) )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber1                                                             --
--  Descricao:  Caso de sucesso com numero inteiro                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber1( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getnumber( 1 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber2                                                             --
--  Descricao:  Caso de sucesso com numero ponto flutuante                                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber2( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "numero"
    return_value, error_message = test_property_getnumber( 2 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber3                                                             --
--  Descricao:  Chave vazia                                                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber3( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: ""
    return_value, error_message = test_property_getnumber( 3 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber4                                                             --
--  Descricao:  Chave inexistente                                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber4( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "chave_inexistente"
    return_value, error_message = test_property_getnumber( 4 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber5                                                             --
--  Descricao:  Caso de sucesso. Chave e valor com espacos                                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber5( )

    --arquivo de propriedades usado neste teste: ap02.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getnumber( 5 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber6                                                             --
--  Descricao:  Caso de sucesso. Chave com valor vazio                                               --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber6( )

    --arquivo de propriedades usado neste teste: ap05.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getnumber( 6 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber7                                                             --
--  Descricao:  Caso de sucesso. Chave com caracteres especiais                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber7( )

    --arquivo de propriedades usado neste teste: ap09.txt
    --chave usada neste teste: "! # @ $ % � & * ( ) - _ + � ` { [ ^ } ] : ; ? / < , > . \\ | \" '"
    return_value, error_message = test_property_getnumber( 7 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber8                                                             --
--  Descricao:  Chave de uma linha maior que o limite                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber8( )

    --arquivo de propriedades usado neste teste: ap11.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getnumber( 8 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber9                                                             --
--  Descricao:  Chave duplicada                                                                      --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber9( )

    --arquivo de propriedades usado neste teste: ap13.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getnumber( 9 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getnumber10                                                            --
--  Descricao:  Tenta pegar chave nao-numerica                                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getnumber.test_property_getnumber10( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "conn time out"
    return_value, error_message = test_property_getnumber( 10 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--                                   PROPERTY:GETBOOLEAN TEST CASES                                  --
-------------------------------------------------------------------------------------------------------
tc_property_getboolean = TestCase("property:getboolean")

-------------------------------------------------------------------------------------------------------
--  Nome:       tc_property_getboolean:setup                                                         --
--  Descricao:  Funcao que verifica a existencia dos arquivos para o teste                           --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean:setup( )

    --pega o arquivo testado de acordo com o teste atual
    local filename = property_getboolean_tests_definitions[ self.__lunit_test_number ][ "filename" ]

    --caso o arquivo nao seja encontrado eh gerado um erro
    if( not FileExists( filename ) ) then
        --a chamada da funcao error() do lua impede que o lunit rode o caso de teste atual
        error( string.format( "Dependencia: arquivo %s nao encontrado", filename ) )
        return
    end
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean1                                                            --
--  Descricao:  Caso de sucesso com numero inteiro                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean1( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "on demand"
    return_value, error_message = test_property_getboolean( 1 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean2                                                            --
--  Descricao:  Numero inteiro invalido                                                              --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean2( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "decisao"
    return_value, error_message = test_property_getboolean( 2 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean3                                                            --
--  Descricao:  Caso de sucesso com numero inteiro invalido                                          --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean3( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: ""
    return_value, error_message = test_property_getboolean( 3 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean4                                                            --
--  Descricao:  Chave inexistente                                                                    --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean4( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "chave_inexistente"
    return_value, error_message = test_property_getboolean( 4 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean5                                                            --
--  Descricao:  Chave com valor vazio                                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean5( )

    --arquivo de propriedades usado neste teste: ap05.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getboolean( 5 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean6                                                            --
--  Descricao:  Chave de uma linha maior que o limite                                                --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean6( )

    --arquivo de propriedades usado neste teste: ap11.txt
    --chave usada neste teste: "telefone"
    return_value, error_message = test_property_getboolean( 6 )

    assert_true( return_value, error_message )
end

-------------------------------------------------------------------------------------------------------
--  Nome:       test_property_getboolean7                                                            --
--  Descricao:  Tenta pegar chave nao-booleano                                                       --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
-------------------------------------------------------------------------------------------------------
function tc_property_getboolean.test_property_getboolean7( )

    --arquivo de propriedades usado neste teste: ap14.txt
    --chave usada neste teste: "conn time out"
    return_value, error_message = test_property_getboolean( 7 )

    assert_true( return_value, error_message )
end

lunit.run()