-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 tdes/main.lua                                                    --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de tdes                                   --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      23/5/2006        Criacao                              --
--  Jamerson Lima        jrfl          9844    15/6/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP007)  --
--  Leonardo N�brega     lfn           17370   13/10/2006       Alterei encrypt_decrypt1 para n�o    --
--                                                              considerar os bytes de padding na    --
--                                                              compara��o da mensagem criptografada --
--                                                              pela api com o conte�do do arquivo   --
--                                                              string.des.                          --
-- Guilherme             gkmo          19097   27/12/2006       Adicionando testes para CBC, Master- --
--                                                              key e criptografia em arquivos       --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

--nome dos arquivos do teste ECB
string_filename = "/string.txt"
ecb_string_filename = "/string.ecb"
key_filename = "/tdes.key"

--nome dos arquivos do teste CBC
cbc_string_filename = "/string.cbc"
iv_filename = "/tdes.ive"

mk_string_filename = "/string.mke"  -- arquivo string.txt descriptografado com tdes.key
mk_indexes_filename = "mk.idx"
-- vari�veis configuradas no setup
wk_filename = nil
mk_index = 0

-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   TDES TEST CASES                                                 --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "tdes" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_tdes_genkey1                                                                     --
--  Descricao: Caso de sucesso. Gerando chave sem passar parametro                                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_tdes_genkey1()

    local result = tdes.genkey()

    assert_not_nil( result, "test_tdes_genkey1. Caso de sucesso. Gerando chave sem passar parametro", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_tdes_genkey2                                                                     --
--  Descricao: Caso de sucesso. Gerando chave passando parametro                                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_tdes_genkey2()

    local result = tdes.genkey( "string de teste " )

    assert_not_nil( result, "test_tdes_genkey2. Caso de sucesso. Gerando chave passando parametro", true )
end


-------------------------------------------------------------------------------------------------------
--                                   ECB TEST CASES                                                  --
-------------------------------------------------------------------------------------------------------

tc_ecb = TestCase( "tdes.ecb" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_ecb_encrypt_decrypt1                                                             --
--  Descricao: Caso de sucesso. Criptografa e descriptografa mensagem string                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_ecb.test_ecb_encrypt_decrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    tdes_key, error_message = LoadFileInBuffer( key_filename )
    assert_not_nil( tdes_key, error_message, true )
    tdes_key = tdes.genkey( tdes_key )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    expected_encrypted_message, error_message = LoadFileInBuffer( ecb_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    local encrypted_message = tdes_key:encrypt( message )
    assert_not_nil( encrypted_message, "test_ecb_encrypt_decrypt1. Caso de sucesso. Criptografa string. Retorno incorreto", true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_ecb_encrypt_decrypt1. Caso de sucesso. Criptografa string. Retorno incorreto",
                  true )

    local result_message = tdes_key:decrypt( encrypted_message )

    assert_equal( message, result_message, "test_ecb_encrypt_decrypt1. Caso de sucesso. Descriptografa string. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_ecb_encrypt_decrypt2                                                             --
--  Descricao: Passando string vazia para encrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_ecb.test_ecb_encrypt_decrypt2()

    local result = true
    local error_message = ""
    local error_code = 0

    local tdes_key = tdes.genkey()
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    local result, error_message, error_code = tdes_key:encrypt( "" )

    assert_nil( result, "test_ecb_encrypt_decrypt2. Passando vazio para encrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_ecb_encrypt_decrypt2. Passando vazio para encrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_ecb_encrypt_decrypt2. Passando vazio para encrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_ecb_encrypt_decrypt3                                                             --
--  Descricao: Passando string vazia para decrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_ecb.test_ecb_encrypt_decrypt3()

    local result = true
    local error_message = ""
    local error_code = 0

    local tdes_key = tdes.genkey()

    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    local result, error_message, error_code = tdes_key:decrypt( "" )

    assert_nil( result, "test_ecb_encrypt_decrypt3. Passando vazio para decrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_ecb_encrypt_decrypt3. Passando vazio para decrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_ecb_encrypt_decrypt3. Passando vazio para decrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_ecb_fencrypt_fdecrypt1                                                           --
--  Descricao: Caso de sucesso. Criptografa e descriptografa de arquivo em modo ECB                  --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_ecb.test_ecb_fencrypt_fdecrypt1()

    -- Carrega a chave
    local tdes_key, error_message = LoadFileInBuffer( key_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Criptografa o arquivo em modo ECB
    local result = tdes_key:fencrypt( string_filename, "temp.des" )
    assert_true( result, "test_ecb_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo ECB. Retorno incorreto", true )

    result = tdes_key:fdecrypt( "temp.des", "temp.txt" )
    assert_true( result, "test_ecb_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo ECB. Retorno incorreto", true )

    local message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    local dec_message, error_message = LoadFileInBuffer( "temp.txt" )
    assert_not_nil( dec_message, error_message, true )

    assert_equal( message, dec_message, "test_ecb_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo ECB. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                   CBC TEST CASES                                                  --
-------------------------------------------------------------------------------------------------------

tc_cbc = TestCase( "tdes.cbc" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_cbc_encrypt_decrypt1                                                             --
--  Descricao: Caso de sucesso. Criptografa e descriptografa mensagem string CBC                     --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_cbc.test_cbc_encrypt_decrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    -- Carrega a chave
    tdes_key, error_message = LoadFileInBuffer( key_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega o vetor de inicializa��o
    local tdes_iv, error_message = LoadFileInBuffer( iv_filename )
    assert_not_nil( tdes_iv, error_message, true )

    -- Carrega a string a ser criptografada
    message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    -- Carrega a string criptografada que ser� usada na compara��o
    expected_encrypted_message, error_message = LoadFileInBuffer( cbc_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    -- Criptografa a string em modo CBC
    local encrypted_message = tdes_key:encrypt( message, tdes_iv )
    assert_not_nil( encrypted_message, "test_cbc_encrypt_decrypt1. Caso de sucesso. Criptografa string. Retorno incorreto", true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_cbc_encrypt_decrypt1. Caso de sucesso. Criptografa string. Retorno incorreto",
                  true )

    local result_message = tdes_key:decrypt( encrypted_message, tdes_iv )

    assert_equal( message, result_message, "test_cbc_encrypt_decrypt1. Caso de sucesso. Descriptografa string. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_cbc_encrypt_decrypt2                                                             --
--  Descricao: Passando string vazia para encrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_cbc.test_cbc_encrypt_decrypt2()

    local result = true
    local error_message = ""
    local error_code = 0

    local tdes_key = tdes.genkey()
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )
    
    -- Carrega o vetor de inicializa��o
    local tdes_iv, error_message = LoadFileInBuffer( iv_filename )
    assert_not_nil( tdes_iv, error_message, true )

    local result, error_message, error_code = tdes_key:encrypt( "", tdes_iv )

    assert_nil( result, "test_cbc_encrypt_decrypt2. Passando vazio para encrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_cbc_encrypt_decrypt2. Passando vazio para encrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_cbc_encrypt_decrypt2. Passando vazio para encrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_cbc_encrypt_decrypt3                                                             --
--  Descricao: Passando string vazia para decrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc_cbc.test_cbc_encrypt_decrypt3()

    local result = true
    local error_message = ""
    local error_code = 0

    local tdes_key = tdes.genkey()

    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    local result, error_message, error_code = tdes_key:decrypt( "", tdes_iv )

    assert_nil( result, "test_cbc_encrypt_decrypt3. Passando vazio para decrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_cbc_encrypt_decrypt3. Passando vazio para decrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_cbc_encrypt_decrypt3. Passando vazio para decrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_cbc_fencrypt_fdecrypt1                                                           --
--  Descricao: Caso de sucesso. Criptografa e descriptografa de arquivo em modo CBC                  --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_cbc.test_cbc_fencrypt_fdecrypt1()

    -- Carrega a chave
    local tdes_key, error_message = LoadFileInBuffer( key_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega o vetor de inicializa��o
    local tdes_iv, error_message = LoadFileInBuffer( iv_filename )
    assert_not_nil( tdes_iv, error_message, true )

    -- Criptografa o arquivo em modo CBC
    local result = tdes_key:fencrypt( string_filename, "temp.des", tdes_iv )
    assert_true( result, "test_cbc_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo CBC. Retorno incorreto", true )

    result = tdes_key:fdecrypt( "temp.des", "temp.txt", tdes_iv )
    assert_true( result, "test_cbc_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo CBC. Retorno incorreto", true )

    local message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    local dec_message, error_message = LoadFileInBuffer( "temp.txt" )
    assert_not_nil( dec_message, error_message, true )

    assert_equal( message, dec_message, "test_cbc_fencrypt_fdecrypt1. Caso de sucesso. Criptografa e descriptografa de arquivo em modo CBC. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                   MK ECB TEST CASES                                               --
-------------------------------------------------------------------------------------------------------

tc_mk = TestCase( "tdes.mk" )

function tc_mk:setup()

    -- Carrega o indice da chave
    local mk_indexes, error_message  = property.open(mk_indexes_filename)
    assert_not_nil( mk_indexes, "Dependencia: nao foi possivel abrir o arquivo " .. mk_indexes_filename, true )
    local platform = os.getenv(SYS_PLATFORMNAME)
    mk_index = mk_indexes:getnumber(platform)
    property.close(mk_indexes)

    if (platform == "HYPERCOM") then
        wk_filename = "wkhyp.key"
    else
        wk_filename = "wking.key"
    end
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_mk_ecb_encrypt1                                                                  --
--  Descricao: Caso de sucesso. Criptografa mensagem string em modo ECB utilizando a MK              --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_mk.test_mk_ecb_encrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    -- Carrega a chave
    tdes_key, error_message = LoadFileInBuffer( wk_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key, mk_index )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega a string a ser criptografada
    message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    -- Carrega a string criptografada que ser� usada na compara��o
    expected_encrypted_message, error_message = LoadFileInBuffer( ecb_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    -- Criptografa a string em modo ECB
    local encrypted_message = tdes_key:encrypt( message )
    assert_not_nil( encrypted_message, "test_mk_ecb_encrypt1. Caso de sucesso. Criptografa mensagem string em modo ECB utilizando a MK. Retorno incorreto", true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_mk_ecb_encrypt1. Caso de sucesso. Criptografa mensagem string em modo ECB utilizando a MK. Retorno incorreto",
                  true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_mk_ecb_fencrypt1                                                                 --
--  Descricao: Caso de sucesso. Criptografa arquivo em modo ECB utilizando a MK                      --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_mk.test_mk_ecb_fencrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    -- Carrega a chave
    tdes_key, error_message = LoadFileInBuffer( wk_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key, mk_index )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega a string criptografada que ser� usada na compara��o
    expected_encrypted_message, error_message = LoadFileInBuffer( ecb_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    -- Criptografa a string em modo ECB
    local result = tdes_key:fencrypt( string_filename, "tempmk.ecb" )
    assert_not_nil( result, "test_mk_ecb_fencrypt1. Caso de sucesso. Criptografa arquivo em modo ECB utilizando a MK. Retorno incorreto", true )

    -- Carrega o arquivo criptografado
    encrypted_message, error_message = LoadFileInBuffer( "tempmk.ecb" )
    assert_not_nil( encrypted_message, error_message, true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_mk_ecb_fencrypt1. Caso de sucesso. Criptografa arquivo em modo ECB utilizando a MK. Retorno incorreto",
                  true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_mk_cbc_encrypt1                                                                  --
--  Descricao: Caso de sucesso. Criptografa mensagem string em modo CBC utilizando a MK              --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_mk.test_mk_cbc_encrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    -- Carrega a chave
    tdes_key, error_message = LoadFileInBuffer( wk_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key, mk_index )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega o vetor de inicializa��o
    local tdes_iv, error_message = LoadFileInBuffer( iv_filename )
    assert_not_nil( tdes_iv, error_message, true )

    -- Carrega a string a ser criptografada
    message, error_message = LoadFileInBuffer( string_filename )
    assert_not_nil( message, error_message, true )

    -- Carrega a string criptografada que ser� usada na compara��o
    expected_encrypted_message, error_message = LoadFileInBuffer( cbc_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    -- Criptografa a string em modo CBC
    local encrypted_message = tdes_key:encrypt( message, tdes_iv )
    assert_not_nil( encrypted_message, "test_mk_cbc_encrypt1. Caso de sucesso. Criptografa mensagem string em modo CBC utilizando a MK. Retorno incorreto", true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_mk_cbc_encrypt1. Caso de sucesso. Criptografa mensagem string em modo CBC utilizando a MK. Retorno incorreto",
                  true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_mk_cbc_fencrypt1                                                                 --
--  Descricao: Caso de sucesso. Criptografa arquivo em modo CBC utilizando a MK                      --
--                                                                                                   --
-------------------------------------------------------------------------------------------------------
function tc_mk.test_mk_cbc_fencrypt1()

    local message = nil
    local expected_encrypted_message = nil
    local error_message = nil
    local tdes_key = nil

    -- Carrega a chave
    tdes_key, error_message = LoadFileInBuffer( wk_filename )
    assert_not_nil( tdes_key, error_message, true )
    -- Cria a chave 3DES
    tdes_key = tdes.genkey( tdes_key, mk_index )
    assert_not_nil( tdes_key, "Dependencia: Chave nao pode ser gerada", true )

    -- Carrega o vetor de inicializa��o
    local tdes_iv, error_message = LoadFileInBuffer( iv_filename )
    assert_not_nil( tdes_iv, error_message, true )

    -- Carrega a string criptografada que ser� usada na compara��o
    expected_encrypted_message, error_message = LoadFileInBuffer( cbc_string_filename )
    assert_not_nil( expected_encrypted_message, error_message, true )

    -- Criptografa a string em modo CBC
    local result = tdes_key:fencrypt( string_filename, "tempmk.cbc", tdes_iv )
    assert_not_nil( result, "test_mk_cbc_fencrypt1. Caso de sucesso. Criptografa arquivo em modo CBC utilizando a MK. Retorno incorreto", true )

    -- Carrega o arquivo criptografado
    encrypted_message, error_message = LoadFileInBuffer( "tempmk.cbc" )
    assert_not_nil( encrypted_message, error_message, true )

    -- remover o padding da mensagem criptografada
    local expected_encrypted_message_no_padding = expected_encrypted_message:sub( 1, -9 )
    -- remover o padding do resultado esperado
    local encrypted_message_no_padding = encrypted_message:sub( 1, -9 )
    -- comparar a mensagem criptografada e o resultado
    assert_equal( expected_encrypted_message_no_padding,
                  encrypted_message_no_padding,
                  "test_mk_ecb_encrypt_decrypt1. Caso de sucesso. Criptografa arquivo em modo CBC utilizando a MK. Retorno incorreto",
                  true )

end

lunit.run()
