require "lunit"

lunit.import "all"

tc1 = TestCase("wifi")

function tc1.test_config()
    assert_true(wifi.config(10, 10000),
        "Não foi possível definir os parâmetros para conexão WI-FI.")
end

function tc1.test_up()
    local result = wifi.config(10, 10000)

    if(result) then
        assert_true(wifi.up(), "Não foi possível ativar a interface WI-FI.")
    end
end

function tc1.test_down()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            assert_true(wifi.down(), "Não foi possível desativar a interface WI-FI.")
        end
    end
end

function tc1.test_scan()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            local resultScan = wifi.scan(10, 10000)
            assert_not_nil(resultScan, "Não foi possível procurar redes WI-FI.")

            if resultScan[1].count == 0 then
                printer.print("\nNenhuma rede WI-FI foi encontrada.\n")
                resultScan[1].ssid = ""
                resultScan[1].tamSsid = 0
                resultScan[1].bssid = ""
                resultScan[1].frequency = 0
                resultScan[1].quality = 0
                resultScan[1].rssi = 0
                resultScan[1].maxTransfRate = 0
                resultScan[1].type = 0
                resultScan[1].authAlgo = 0
                resultScan[1].cryptAlgo = 0
            else
                for i = 1, resultScan[1].count do
                    printer.print("count: " .. resultScan[i].count)
                    printer.print("ssid: " .. resultScan[i].ssid)
                    printer.print("tam ssid: " .. resultScan[i].tamSsid)
                    printer.print("bssid: " .. resultScan[i].bssid)
                    printer.print("frequency: " .. resultScan[i].frequency)
                    printer.print("quality: " .. resultScan[i].quality)
                    printer.print("rssi: " .. resultScan[i].rssi)
                    printer.print("max transf rate: " .. resultScan[i].maxTransfRate)
                    printer.print("type: " .. resultScan[i].type)
                    printer.print("auth algo: " .. resultScan[i].authAlgo)
                    printer.print("crypt algo: " .. resultScan[i].cryptAlgo)
                    printer.print("\n")
                end
            end
        end
    end
end

function tc1.test_connect1()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            result = wifi.scan(10, 10000)

            if(result and result[1].count ~= 0) then
                assert_true(wifi.startconnect(
                    "wifi_pass",
                    true,
                    result[1]
                ), "Não foi possível iniciar o processo de conexão na rede WI-FI com DHCP true.")
            end
        end
    end
end

function tc1.test_connect2()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            result = wifi.scan(10, 10000)

            if(result and result[1].count ~= 0) then
                assert_true(wifi.startconnect(
                    "wifi_pass",
                    false,
                    {
                        ipAddress="172.27.100.254",
                        mask="255.255.255.255",
                        gateway="172.27.64.1",
                        dns1="0.0.0.0",
                        dns2="172.27.65.22"
                    },
                    result[1]
                ), "Não foi possível iniciar o processo de conexão na rede WI-FI com DHCP false.")
            end
        end
    end
end

function tc1.test_status()
    local result = wifi.config(10, 10000)

    if(result) then
        result = wifi.up()
        if(result) then
            resultStatus = wifi.status()
            assert_not_nil(resultStatus, "Não foi possível obter o status da rede WI-FI antes de conectar.")
            
            if(resultStatus ~= nil) then
                printer.print("\n--> Antes do connect\n")
                print_status(resultStatus)
            end

            result = wifi.scan(10, 10000)

            if(result ~= nil and result[1].count) ~= 0 then
                resultConnect = wifi.startconnect(
                    "wifi_pass",
                    true,
                    result[1]
                )

                if(resultConnect ~= nil) then
                    resultStatus = wifi.status()
                    assert_not_nil(resultStatus, "Não foi possível obter o status da rede WI-FI com DHCP true.")

                    if(resultStatus ~= nil) then
                        printer.print("\n--> DHCP true\n")
                        print_status(resultStatus)
                    end

                    wifi.requestdisconnect()
                end

                resultConnect = wifi.startconnect(
                    "wifi_pass",
                    false,
                    {
                        ipAddress="172.27.100.254",
                        mask="255.255.255.255",
                        gateway="172.27.64.1",
                        dns1="0.0.0.0",
                        dns2="172.27.65.22"
                    },
                    result[1]
                )

                if(resultConnect ~= nil) then
                    resultStatus = wifi.status()
                    assert_not_nil(resultStatus, "Não foi possível obter o status da rede WI-FI com DHCP true.")

                    if(resultStatus ~= nil) then
                        printer.print("\n--> DHCP false\n")
                        print_status(resultStatus)
                    end
                end
            end
        end
    end
end

function tc1.test_disconnect1()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            result = wifi.scan(10, 10000)

            if(result ~= nil and result[1].count) ~= 0 then
                resultConnect = wifi.startconnect(
                    "wifi_pass",
                    true,
                    result[1]
                )

                if(resultConnect) then
                    assert_true(wifi.requestdisconnect(), "Não foi possível processar a solicitação de desconexão da rede WI-FI com DHCP true.")
                end
            end
        end
    end
end

function tc1.test_disconnect2()
    local result = wifi.config(10, 10000)
    
    if(result) then
        result = wifi.up()

        if(result) then
            result = wifi.scan(10, 10000)

            if(result ~= nil and result[1].count) ~= 0 then
                resultConnect = wifi.startconnect(
                    "wifi_pass",
                    false,
                    {
                        ipAddress="172.27.100.254",
                        mask="255.255.255.255",
                        gateway="172.27.64.1",
                        dns1="0.0.0.0",
                        dns2="172.27.65.22"
                    },
                    result[1]
                )

                if(resultConnect) then
                    assert_true(wifi.requestdisconnect(), "Não foi possível processar a solicitação de desconexão da rede WI-FI com DHCP false.")
                end
            end
        end
    end
end

function print_status(resultStatus)
    printer.print("wifi status idx: " .. WIFI_STATUS_IDX)
    printer.print("wifi network idx: " .. WIFI_NETWORK_IDX)
    printer.print("wifi ip idx: " .. WIFI_IP_IDX)
    printer.print("status: " .. resultStatus[WIFI_STATUS_IDX].status)
    printer.print("native status: " .. resultStatus[WIFI_STATUS_IDX].nativeStatus)
    printer.print("error type: " .. resultStatus[WIFI_STATUS_IDX].errorType)
    printer.print("native error: " .. resultStatus[WIFI_STATUS_IDX].nativeError)
    printer.print("mac address: " .. resultStatus[WIFI_STATUS_IDX].macAddress)
    printer.print("ssid: " .. resultStatus[WIFI_NETWORK_IDX].ssid)
    printer.print("tam ssid: " .. resultStatus[WIFI_NETWORK_IDX].tamSsid)
    printer.print("bssid: " .. resultStatus[WIFI_NETWORK_IDX].bssid)
    printer.print("frequency: " .. resultStatus[WIFI_NETWORK_IDX].frequency)
    printer.print("quality: " .. resultStatus[WIFI_NETWORK_IDX].quality)
    printer.print("rssi: " .. resultStatus[WIFI_NETWORK_IDX].rssi)
    printer.print("max transf rate: " .. resultStatus[WIFI_NETWORK_IDX].maxTransfRate)
    printer.print("type: " .. resultStatus[WIFI_NETWORK_IDX].type)
    printer.print("auth algo: " .. resultStatus[WIFI_NETWORK_IDX].authAlgo)
    printer.print("crypt algo: " .. resultStatus[WIFI_NETWORK_IDX].cryptAlgo)
    printer.print("ip address: " .. resultStatus[WIFI_IP_IDX].ipAddress)
    printer.print("mask: " .. resultStatus[WIFI_IP_IDX].mask)
    printer.print("gateway: " .. resultStatus[WIFI_IP_IDX].gateway)
    printer.print("dns 1: " .. resultStatus[WIFI_IP_IDX].dns1)
    printer.print("dns 2: " .. resultStatus[WIFI_IP_IDX].dns2)
    printer.print("is dhcp: ")
    printer.print(resultStatus[WIFI_IP_IDX].isDhcp)
    printer.print("lease duration: " .. resultStatus[WIFI_IP_IDX].leaseDuration)
    printer.print("dhcp server address: " .. resultStatus[WIFI_IP_IDX].dhcpServerAddress)
end

lunit.run()