require "lunit"

require "auxiliar"

lunit.import "all"

tc = TestCase("lunit_test")

var_true = true
var_false  = false
var_um = 1
var_zero = 0
var_nil = nil

-- lunit.assert( assertion, [msg] )
function tc.test_assert( )
    lunit.assert( var_true, "passando true para assert" )
    lunit.assert( var_false, "passando false para assert" )
    lunit.assert( nil, "passando nil para assert" )
end

-- lunit.assert_fail( [msg] )
function tc.test_assert_fail( )
    lunit.assert_fail( "chamando assert_fail" )
end

-- lunit.assert_true( actual, [msg] )
function tc.test_assert_true( )
    lunit.assert_true( var_true,
                       "passando true para assert_true" )
    lunit.assert_true( var_false,
                       "passando false para assert_true" )
end

-- lunit.assert_false( actual, [msg] )
function tc.test_assert_false( )
    lunit.assert_false( var_true,
                        "passando true para assert_false" )
    lunit.assert_false( var_false,
                        "passando false para assert_false" )
end

-- lunit.assert_equal( expected, actual, [msg] )
function tc.test_assert_equal( )
    lunit.assert_equal( var_um,
                        var_um,
                        "passando 1 e 1 para assert_equal" )
    lunit.assert_equal( var_um,
                        var_zero,
                        "passando 1 e 0 para assert_equal" )
end

-- lunit.assert_not_equal( unexpected, actual, [msg] )
function tc.test_assert_not_equal( )
    lunit.assert_not_equal( var_um,
                            var_um,
                            "passando 1 e 1 para assert_not_equal" )
    lunit.assert_not_equal( var_um,
                            var_zero,
                            "passando 1 e 0 para assert_not_equal" )
end

-- lunit.assert_nil( actual, [msg] )
function tc.test_assert_nil( )
    lunit.assert_nil( var_um,
                      "passando 1 para assert_nil" )
    lunit.assert_nil( var_nil,
                      "passando nil para assert_nil" )
end

-- lunit.assert_not_nil( actual, [msg] )
function tc.test_assert_not_nil( )
    lunit.assert_not_nil( var_um,
                          "passando 1 para assert_not_nil" )
    lunit.assert_not_nil( var_nil,
                          "passando nil para assert_not_nil" )
end

-- lunit.assert_match( pattern, actual, [msg] )
-- lunit.assert_not_match( pattern, actual, [msg] )
-- lunit.assert_boolean( actual, [msg] )
-- lunit.assert_not_boolean( actual, [msg] )
-- lunit.assert_number( actual, [msg] )
-- lunit.assert_not_number( actual, [msg] )
-- lunit.assert_string( actual, [msg] )
-- lunit.assert_not_string( actual, [msg] )
-- lunit.assert_table( actual, [msg] )
-- lunit.assert_not_table( actual, [msg] )
-- lunit.assert_function( actual, [msg] )
-- lunit.assert_not_function( actual, [msg] )
-- lunit.assert_thread( actual, [msg] )
-- lunit.assert_not_thread( actual, [msg] )
-- lunit.assert_userdata( actual, [msg] )
-- lunit.assert_not_userdata( actual, [msg] )
-- lunit.assert_error( [msg], func )
-- lunit.assert_pass( [msg], func )

lunit.run( )

printer.print("TOTAL: 16")
printer.print("Qnts erros devem dar:")
printer.print("assert: 2")
printer.print("assert_fail: 1")
printer.print("assert_true: 1")
printer.print("assert_false: 1")
printer.print("assert_equal: 1")
printer.print("assert_not_equal: 1")
printer.print("assert_nil: 1")
printer.print("assert_not_nil: 1")