-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 crc32/main.lua                                                   --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de crc32                                  --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA      23/5/2006        Criacao                              --
--  Jamerson Lima        jrfl          9844    16/6/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP008)  --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

--nome dos arquivos que seram utilizados nos testes
indice_filename = "/indices.txt"
string_filename = "/string.txt"

--nome dos arquivos que contem o hash dos arquivos acima
indice_crcfilename = "/indices.bin"
string_crcfilename = "/string.bin"

-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   CRC32 TEST CASES                                                --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "crc32" )

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_crc32_calculate1                                                                 --
--  Descricao: Caso de sucesso. Calcula o checksum de uma string                                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_crc32_calculate1()

    --abre e le o conteudo do arquivo de teste
    local message, error_message = LoadFileInBuffer( string_filename )

    assert_not_nil( message, error_message, true )

    --abre e le o conteudo do arquivo com hash
    local expected_crc_hash, error_message = LoadFileInBuffer( string_crcfilename )

    assert_not_nil( expected_crc_hash, error_message, true )

    --chama a funcao testada
    local result = crc32.calculate( message )

    assert_not_nil( result, "test_crc32_calculate1. Calcula checksum de uma string. Retorno incorreto", true )

    assert_equal( expected_crc_hash, result, "test_crc32_calculate1. Calcula checksum de uma string. Retorno incorreto" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_crc32_calculate2                                                                 --
--  Descricao: Caso de sucesso. Calcula o checksum de um arquivo de indice                           --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_crc32_calculate2()

    --abre e le o conteudo do arquivo de teste
    local message, error_message = LoadFileInBuffer( indice_filename )
    
    assert_not_nil( message, error_message, true )

    --abre e le o conteudo do arquivo com hash
    local expected_crc_hash = LoadFileInBuffer( indice_crcfilename )

    assert_not_nil( expected_crc_hash, error_message, true )

    --chama a funcao testada
    local result = crc32.calculate( message )

    assert_not_nil( result, "test_crc32_calculate2. Calcula checksum de um arquivo de indice. Retorno incorreto", true )

    assert_equal( expected_crc_hash, result, "test_crc32_calculate2. Calcula checksum de um arquivo de indice. Retorno incorreto" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_crc32_calculate3                                                                 --
--  Descricao: Passa uma string vazia para calculate                                                 --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------

function tc.test_crc32_calculate3()

    local result = nil
    local error_message = ""
    local error_code = 0

    --chama a funcao testada
    local result, error_message, error_code = crc32.calculate( "" )

    assert_nil( result, "test_crc32_calculate3. Passando string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_crc32_calculate3. Passando string vazia. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_crc32_calculate3. Passando string vazia. Mensagem de erro incorreta" )

end

lunit.run()