require "lunit"

lunit.import "all"

tc1 = TestCase("ethernet")

function tc1.test_up()
    assert_true(ethernet.up(), "Não foi possível ativar a interface ethernet com DHCP true.")
end

function tc1.test_down()
    result = ethernet.up()

    if(result) then
        assert_true(ethernet.down(), "Não foi possível desativar a interface ethernet com DHCP true.")
    end
end

function tc1.test_status()
    ethernet.up()
    resultStatus = ethernet.status()
    assert_not_nil(resultStatus, "Não foi possível obter o status da interface ethernet com DHCP true.")

    if(resultStatus ~= nil) then
        printer.print("\n--> DHCP false\n")
        print_status(resultStatus)
    end
    ethernet.down()
end

function print_status(resultStatus)
    printer.print("ethernet status idx: " .. ETHERNET_STATUS_IDX)
    printer.print("ethernet network idx: " .. ETHERNET_NETWORK_IDX)
    printer.print("status: " .. resultStatus[ETHERNET_STATUS_IDX].status)
    printer.print("native status: " .. resultStatus[ETHERNET_STATUS_IDX].nativeStatus)
    printer.print("error type: " .. resultStatus[ETHERNET_STATUS_IDX].errorType)
    printer.print("native error: " .. resultStatus[ETHERNET_STATUS_IDX].nativeError)
    printer.print("mac address: " .. resultStatus[ETHERNET_STATUS_IDX].macAddress)
    printer.print("ip address: " .. resultStatus[ETHERNET_NETWORK_IDX].ipAddress)
    printer.print("mask: " .. resultStatus[ETHERNET_NETWORK_IDX].mask)
    printer.print("gateway: " .. resultStatus[ETHERNET_NETWORK_IDX].gateway)
    printer.print("dns 1: " .. resultStatus[ETHERNET_NETWORK_IDX].dns1)
    printer.print("dns 2: " .. resultStatus[ETHERNET_NETWORK_IDX].dns2)
    printer.print("is dhcp: ")
    printer.print(resultStatus[ETHERNET_NETWORK_IDX].isDhcp)
    printer.print("lease duration: " .. resultStatus[ETHERNET_NETWORK_IDX].leaseDuration)
    printer.print("dhcp server address: " .. resultStatus[ETHERNET_NETWORK_IDX].dhcpServerAddress)
end

lunit.run()