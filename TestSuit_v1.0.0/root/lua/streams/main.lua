-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 streams/main.lua                                                 --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de streams                                --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Jamerson Lima        jrfl          NA       15/06/2006      Criacao                              --
--  Jamerson Lima        jrfl         9844      28/06/2006      Rework(HFLEX-LUA-TSTR-CODE-INSP009)  --
--  Jamerson Lima        jrfl          NA       03/06/2006      Colocando sleep apos conectar        --
--  Leandro Mitsuo       lmf          16669     11/09/2006      Corre��o de testes                   --
--  Guilherme            gkmo         19098     26/12/2006      Adicionando testes de RS232          --
--  Guilherme            gkmo         20744     12/02/2007      Corrigindo testes de RS232           --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

phone_number = ""

environment = ""

--conteudo do arquivo is_file.txt
is_file_content = "0123456789"

-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

function choose_env()

    if( environment:len() == 0 ) then
        local m = ui.menu( "Plataforma", { "Ingenico", "Hypercom", "PC" } )

        if( m == nil ) then
            error( "Menu nao pode ser criado" )
        end

        m:show()

        if( m:accepted() == 1 ) then --ingenico
            environment = "ingenico"
        elseif( m:accepted() == 2 ) then  -- hypercom
            environment = "hypercom"
        else --pc
            environment = "pc"
        end

        ui.destroy( m )
    end
end

function ConnectModem()

    display.clear()
    display.print( "Press. tecla para discar" )
    keyboard.getkeystroke( -1 )

    display.clear()
    display.print( "discando para " .. phone_number )

    comm.disconnect()

    assert_true( comm.loadconfig(), "Dependencia: Nao foi possivel carregar configuracao padrao", true )
    
    if( environment == "hypercom" ) then
        comm.config( "modulation_protocol", "v22" )
        comm.config( "bis data_link_protocol", "transparent" )
        comm.config( "dial_mode", "tone" )
        comm.config( "rbuffsz", "1024" )
        comm.config( "sbuffsz", "1024" )
    end

    if ( comm.connect( phone_number ) == false ) then
        return false, "Dependencia: Nao foi possivel conectar"
    end

    while comm.check() == "waiting" do
        management.sleep( 1000 )
    end

    if( comm.check() ~= "connected" ) then
        return false, "Dependencia: Nao foi possivel conectar"
    end

    management.sleep( 2000 )

    display.clear()
    display.print( "conetacdo" )

    return true
end

-------------------------------------------------------------------------------------------------------
--                                   STREAMS TEST CASES                                              --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "streams" )

--pega um numero de telefone valido do usuario
function tc:setup()

    choose_env()

    if( environment == "pc" ) then
        phone_number = "127000000001"
    else
        if( phone_number:len() == 0 ) then
            local tf = ui.textfield( "CONFIGURACAO", "Digite Tel.", 30, 1 )

            if( tf:show() == false ) then
                error( "Dependencia: Teste cancelado" )
            end

            phone_number = tf:text()
        end
    end
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openfileis1                                                              --
--  Descricao: Caso de sucesso. Abre um arquivo existente e fecha stream                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openfileis1()

    local testname = "test_streams_openfileis1"
    local filename = "/isfile.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local is = streams.openfileis( filename )

    assert_not_nil( is, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nulo", true )

    local result = pcall( function() is:read( is_file_content:len(), -1 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nao foi criado", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nao foi criado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openfileis2                                                              --
--  Descricao: Caso de sucesso. Passando arquivo vazio                                               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openfileis2()

    local testname = "test_streams_openfileis2"
    local filename = "/isempty.txt"

    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local is = streams.openfileis( filename )

    assert_not_nil( is, testname .. ": Caso de sucesso. Abre um stream de arquivo vazio. Stream nulo", true )

    local result = pcall( function() is:read( 0, -1 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo vazio. Stream nao foi criado", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo vazio. Stream nao foi criado", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_opencommis1                                                              --
--  Descricao: Chama funcao com  modem desconectado                                                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_opencommis1()

    local testname = "test_streams_opencommis1"

    if( comm.check() ~= "disconnected" ) then
        comm.disconnect()
    end

    local result, error_message, error_code = streams.opencommis()

    assert_nil( result, testname .. ": Chama funcao com  modem desconectado. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDSTATE, error_code, testname .. ": Chama funcao com  modem desconectado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDSTATE ], error_message, testname .. ": Chama funcao com  modem desconectado. Codigo de erro incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_opencommis2                                                              --
--  Descricao: Caso de sucesso. Conecta modem, abre e fecha stream.                                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_opencommis2()

    local testname = "test_streams_opencommis2"

    local result, error_message = ConnectModem()

    assert_true( result, error_message, true )

    local is  = streams.opencommis()

    assert_not_nil( is, testname .. ": Abre o stream de modem. Retorno incorreto", true )

    local result = pcall( function() is:read( 0, 1 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado", true )

    result = is:close()
    comm.disconnect()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openstringis1                                                            --
--  Descricao: Caso de sucesso. abre stream de string e fecha                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openstringis1()

    local testname = "test_streams_openstringis1"
    local text = "1"

    local is = streams.openstringis( text )

    assert_not_nil( is, testname .. ": Caso de sucesso. Abre stream de string. Retorno incorreto", true )

    local result = pcall( function() is:read( 0, 1 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre stream de string. Stream nao foi criado", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre stream de string. Stream nao foi criado", true )

    result = is:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openbufferedis1                                                          --
--  Descricao: Caso de sucesso. Abre stream de string e fecha                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openbufferedis1()

    local testname = "test_streams_openbufferedis1"
    local text = "1"

    local is = streams.openstringis( text )

    assert_not_nil( is, "Dependencia: Stream de string nao foi criado", true )

    local bis = streams.openbufferedis( is )

    if( not assert_not_nil( bis, testname .. ": Caso de sucesso. Bufferizando stream de entrada de string. Retorno incorreto" ) ) then
        is:close()
        return
    end

    local result = pcall( function() bis:read( 0, 1 ) end )

    if( not assert_true( result, testname .. ": Caso de sucesso. Abre stream de string. Stream nao foi criado" ) ) then
        is:close()
        return
    end

    result = pcall( function() bis:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre stream de string. Stream nao foi criado" )

    is:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openbufferedis2                                                          --
--  Descricao: Caso de sucesso. passa um stream de comm com o modem desconectado                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openbufferedis2()

    local testname = "test_streams_openbufferedis2"

    local result, error_message = ConnectModem()

    assert_true( result, error_message, true )

    local is = streams.opencommis()

    comm.disconnect()

    if( not assert_equal( "disconnected", comm.check(), "Dependencia: Nao foi possivel desconectar modem" ) ) then
        if( is ~= nil ) then
            is:close()
        end
        return
    end

    assert_not_nil( is, "Dependencia: Nao foi possivel criar input stream de comm", true )

--    local bis = streams.openbufferedis( is )
    local result, error_message, error_code = streams.openbufferedis( is )

    assert_nil( result, testname .. ": Chama funcao com  modem desconectado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED , error_code, testname .. ": Chama funcao com  modem desconectado. Codigo de erro incorreto" )
    
    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED  ], error_message, testname .. ": Chama funcao com  modem desconectado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openbufferedis3                                                          --
--  Descricao: Passando um input stream fechado                                                      --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openbufferedis3()

    local testname = "test_streams_openbufferedis3"
    local text = "1"

    local is = streams.openstringis( text )

    assert_not_nil( is, "Dependencia: Stream de string nao foi criado", true )

    is:close()

    local result, error_message, error_code = streams.openbufferedis( is )

    assert_nil( result, testname .. ": Passando um input stream fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Passando um input stream fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Passando um input stream fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openrs232is1                                                             --
--  Descricao: Caso de sucesso. Abre uma conex�o RS232 pela COM1 e fecha o stream                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openrs232is1()

    local testname = "test_streams_openrs232is1"
    local port = "COM1"

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Abre um stream de RS232. Porta nulo", true )

    local is = streams.openrs232is(handle)

    assert_not_nil( is, testname .. ": Caso de sucesso. Abre um stream de RS232. Stream nulo", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de RS232. Erro ao fechar o stream", true )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de RS232. Erro ao fechar porta RS232", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openrs232is2                                                             --
--  Descricao: Tenta abrir um stream com handle inv�lido                                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openrs232is2()

    local testname = "test_streams_openrs232is2"

    local result = pcall( function() streams.openrs232is(nil) end )

    assert_false( result, testname .. ": Tenta abrir um stream com handle inv�lido. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openfileos1                                                              --
--  Descricao: Caso de sucesso. Abre um arquivo e fecha stream                                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openfileos1()

    local filename = "/os_file1.txt"
    local testname = "test_streams_openfileos1"

    local outs = streams.openfileos( filename )

    assert_not_nil( outs, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nulo" )

    local result = pcall( function() outs:write( "0123456789", 1000 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nao foi criado", true )

    result = pcall( function() outs:close() end )

    os.remove( testname )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de arquivo. Stream nao foi criado" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openfileos2                                                              --
--  Descricao: Passando um string vazia                                                              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openfileos2()

    local testname = "test_streams_openfileos2"

    local result, error_message, error_code = streams.openfileos( "" )

    assert_nil( result, testname .. ": Passando uma string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, testname .. ": Passando uma string vazia. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, testname .. ": Passando uma string vazia. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_opencommos1                                                              --
--  Descricao: Chama funcao com  modem desconectado                                                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_opencommos1()

    local testname = "test_streams_opencommos1"

    if( comm.check() == "disconnected" ) then
        comm.disconnect()
    end

    local result, error_message, error_code = streams.opencommos()

    assert_nil( result, testname .. ": Chama funcao com  modem desconectado. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDSTATE, error_code, testname .. ": Chama funcao com  modem desconectado. Codigo de erro incorreto" )

    assert_equal( lua_error_message[ TST_ERR_INVALIDSTATE ], error_message, testname .. ": Chama funcao com  modem desconectado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_opencommos2                                                              --
--  Descricao: Caso de sucesso. Conecta modem, abre e fecha stream.                                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_opencommos2()

    local testname = "test_streams_opencommos2"

    local result, error_message = ConnectModem()

    assert_true( result, error_message, true )

    local os  = streams.opencommos()

    assert_not_nil( os, testname .. ": Abre o stream de modem. Retorno incorreto", true )

    local result = pcall( function() os:write( "0123456789", 1 ) end )

    if( not assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado" ) ) then
        comm.disconnect()
        return
    end

    result = pcall( function() os:close() end )

    comm.disconnect()

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openstringos1                                                            --
--  Descricao: Caso de sucesso. abre stream de string e fecha                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openstringos1()

    local testname = "test_streams_openstringos1"

    local os = streams.openstringos()

    assert_not_nil( os, testname .. ": Caso de sucesso. Abre stream de string. Retorno incorreto", true )

    local result = pcall( function() os:write( "0123456789", 1 ) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado", true )

    result = pcall( function() os:content() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado", true )

    result = pcall( function() os:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre o stream de modem. Stream nao foi criado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openbufferedos2                                                          --
--  Descricao: Passando um output stream de um modem desconectado                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openbufferedos2()

    local testname = "test_streams_openbufferedos2"

    local result, error_message = ConnectModem()

    assert_true( result, error_message, true )

    local os = streams.opencommos()

    comm.disconnect()

    if( not assert_equal( "disconnected", comm.check(), "Dependencia: Nao foi possivel desconectar modem" ) ) then
        if( os ~= nil ) then
            os:close()
        end
    end

    assert_not_nil( os, "Dependencia: Nao foi possivel criar input stream de comm", true )

    local result, error_message, error_code = streams.openbufferedos( os )

    assert_nil( result, testname .. ": Chama funcao com  modem desconectado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Chama funcao com  modem desconectado. Codigo de erro incorreto", true )
    
    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Chama funcao com  modem desconectado. Mensagem de erro incorreta" )
end

 -------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openbufferedos3                                                          --
--  Descricao: Passando um output stream fechado                                                     --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openbufferedos3()

    local testname = "test_streams_openbufferedos3"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Stream de string nao foi criado", true )

    os:close()

    local result, error_message, error_code = streams.openbufferedos( os )

    assert_nil( result, testname .. ": Passando um output stream fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Passando um output stream fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Passando um output stream fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openrs232os1                                                             --
--  Descricao: Caso de sucesso. Abre uma conex�o RS232 pela COM1 e fecha o stream                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openrs232os1()

    local testname = "test_streams_openrs232os1"
    local port = "COM1"

    local handle = rs232.open(port)

    assert_not_nil( handle, testname .. ": Caso de sucesso. Abre um stream de RS232. Porta nulo", true )

    local is = streams.openrs232os(handle)

    assert_not_nil( is, testname .. ": Caso de sucesso. Abre um stream de RS232. Stream nulo", true )

    result = pcall( function() is:close() end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de RS232. Erro ao fechar o stream", true )

    result = pcall( function() rs232.close(handle) end )

    assert_true( result, testname .. ": Caso de sucesso. Abre um stream de RS232. Erro ao fechar porta RS232", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_openrs232os2                                                             --
--  Descricao: Tenta abrir um stream com handle inv�lido                                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_openrs232os2()

    local testname = "test_streams_openrs232os2"

    local result = pcall( function() streams.openrs232os(nil) end )

    assert_false( result, testname .. ": Tenta abrir um stream com handle inv�lido. Retorno incorreto", true )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_close1                                                                   --
--  Descricao: Fechando um input stream                                                              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_close1()

    local testname = "test_streams_close1"
    local text = "1"

    local is = streams.openstringis( text )

    assert_not_nil( is, "Dependencia: Input stream nao foi criado", true )

    local ret = is:close()

    assert_true( ret, testname .. ": Fechando um input stream. Retorno incorreto", true )

    local result, error_message, error_code = is:read( 0, 1 )

    assert_nil( result, testname .. ": Verificando se input stream foi fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Verificando se input stream foi fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Verificando se input stream foi fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_close2                                                                   --
--  Descricao: Fechando um output stream                                                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_close2()

    local testname = "test_streams_close2"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local result = os:close()

    assert_true( result, testname .. ": Fechando um Output stream. Retorno incorreto", true )

    result = pcall( function() os:read( 0, 1 ) end )

    assert_false( result, testname .. ": Fechando um output stream. Stream nao foi fechado" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_close3                                                                   --
--  Descricao: Fechando um stream fechado                                                            --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_close3()

    local testname = "test_streams_close3"
    local text = "1"

    local is = streams.openstringis( text )

    assert_not_nil( is, "Dependencia: Input stream nao foi criado", true )

    is:close()

    local result, error_message, error_code = is:close()

    assert_nil( result, testname .. ": Fechando um input stream fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Fechando um input stream fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Fechando um input stream fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_read1                                                                    --
--  Descricao: Caso de sucesso. Passa um input stream valido e le seu conteudo                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_read1()

    local testname = "test_streams_read1"
    local filename = "isfile.txt"
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

     local is = streams.openfileis( filename )

     assert_not_nil( is, "Dependencia: Input stream nao foi criado", true )

     local result = is:read( is_file_content:len(), -1 )

     if( not assert_equal( is_file_content, result, testname .. ": Lendo de um stream valido. Retorno incorreto" ) ) then
        is:close()
        return
    end

    result, error_message, error_code = is:read( is_file_content:len(), -1 )

    is:close()

    assert_nil( result, testname .. ": Lendo final de stream. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Lendo final de stream. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Lendo final de stream. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_read2                                                                    --
--  Descricao: Passa um input stream fechado                                                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_read2()

    local testname = "test_streams_read2"
    local filename = "isfile.txt"
    
    assert_true( os.exists( filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

     local is = streams.openfileis( filename )

     assert_not_nil( is, "Dependencia: Input stream nao foi criado", true )

     local result = is:read( is_file_content:len(), -1 )

     if( not assert_equal( is_file_content, result, testname .. ": Passa um input stream fechado. Retorno incorreto" ) ) then
        is:close()
        return
    end
    
    is:close()

    result, error_message, error_code = is:read( is_file_content:len(), -1 )

    assert_nil( result, testname .. ": Passa um input stream fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Passa um input stream fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Passa um input stream fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_write1                                                                   --
--  Descricao: Caso de sucesso. Passa um output stream valido e escreve nele                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_write1()

    local testname = "test_streams_write1"
    local text = "0123456789"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local result = os:write( text, 10000 )

    if( not assert_equal( text:len(), result, testname .. ": Escrevendo em um stream valido. Retorno incorreto" ) ) then
        os:close()
        return
    end

    assert_equal( text, os:content(), testname .. ": Escrevendo em um stream valido. Dado nao foi escrito no stream" )
    os:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_write2                                                                   --
--  Descricao: Caso de sucesso especificando offset. Passa um output stream valido e escreve nele    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_write2()

    local testname = "test_streams_write2"
    local text = "0123456789"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local result = os:write( text, 10000, 5 )

    if( not assert_equal( text:len() - 4, result, testname .. ": Escrevendo em um stream valido especificando offset. Retorno incorreto" ) ) then
        os:close()
        return
    end

    assert_equal( text:sub(5), os:content(), testname .. ": Escrevendo em um stream valido especificando offset. Dado nao foi escrito no stream" )

    os:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_write3                                                                   --
--  Descricao: Caso de sucesso especificando length. Passa um output stream valido e escreve nele    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_write3()

    local testname = "test_streams_write3"
    local text = "0123456789"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local result = os:write( text, 10000, 5, 2 )

    if( not assert_equal( 2, result, testname .. ": Escrevendo em um stream valido especificando length. Retorno incorreto" ) ) then
        os:close()
        return
    end

    assert_equal( text:sub( 5, 6 ), os:content(), testname .. ": Escrevendo em um stream valido especificando length. Dado nao foi escrito no stream" )

    os:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_write4                                                                   --
--  Descricao: Passa um output stream fechado                                                        --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_write4()

    local testname = "test_streams_write4"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    os:close()

    local result, error_message, error_code = os:write( "0123456789", 1 )

    assert_nil( result, testname .. ": Passa um output stream fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Passa um output stream fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Passa um output stream fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_write5                                                                   --
--  Descricao: Passa um output stream de comm fechado                                                --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_write5()

    local testname = "test_streams_write5"

    local result, error_message = ConnectModem()

    assert_true( result, error_message, true )

    local os = streams.opencommos()

    if( not assert_not_nil( os, "Dependencia: Output stream nao foi criado", true ) ) then
        comm.disconnect()
        return
    end

    comm.disconnect()

    assert_equal( "disconnected", comm.check(), "Dependencia: Modem nao foi desconectado", true )

    local result, error_message, error_code = os:write( "0123456789", 10000 )

    os:close()

    assert_nil( result, testname .. ": Passa um output stream de comm fechado. Retorno incorreto", true )

    assert_equal( TST_ERR_STREAM_CLOSED, error_code, testname .. ": Passa um output stream de comm fechado. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_STREAM_CLOSED ], error_message, testname .. ": Passa um output stream de comm fechado. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_flush1                                                                   --
--  Descricao: Caso de sucesso. Cria um stream bufferizado, escreve nele e da um flush               --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_flush1()

    local testname = "test_streams_flush1"
    local text = "0123456789"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local bos = streams.openbufferedos( os )

    assert_not_nil( bos, "Dependencia: Output stream bufferizado nao foi criado", true )

    local result = bos:write( text, 10000 )

    if( not assert_equal( "", os:content(), "Dependencia: dados escritos antes do flush" ) ) then
        bos:close()
        os:close()
    end

    result = bos:flush( -1 )

    if( not assert_equal( text:len(), result, testname .. ": Caso de sucesso. Flush em um stream bufferizado. Retorno incorreto" ) ) then
        bos:close()
        os:close()
        return
    end

    assert_equal( text, os:content(), testname .. ": Caso de sucesso. Flush em um stream bufferizado. Dados nao foram escritos" )

    bos:close()
    os:close()
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_streams_content1                                                                 --
--  Descricao: Caso de sucesso. Recupera o valor de um stream de  string                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_streams_content1()

    local testname = "test_streams_content1"
    local text = "0123456789"

    local os = streams.openstringos()

    assert_not_nil( os, "Dependencia: Output stream nao foi criado", true )

    local result = os:write( text, 10000 )

    if( not assert_equal( text:len(), result, "Dependencia: Dados nao foram escritos no stream" ) ) then
        os:close()
        return
    end

    assert_equal( text, os:content(), testname .. ": Caso de sucesso. Recuperando conteudo. Dados incorretos" )

    os:close()
end

lunit.run()
