-------------------------------------------------------------------------------------------------------
-- Nome do arquivo:                 rsa/main.lua                                                     --
-- Tres Letras Representativas:     TST                                                              --
-- Descricao:                       Testes das funcoes Lua de crc32                                  --
-------------------------------------------------------------------------------------------------------
--                            Historico                                                              --
-------------------------------------------------------------------------------------------------------
--  Nome                 Login         CR        Data           Descricao                            --
-------------------------------------------------------------------------------------------------------
--  Leandro Mitsuo       lmf           16707   05/09/2006       Corre��o de Falhas                   --
--  Jamerson Lima        jrfl          NA      23/5/2006        Criacao                              --
--  Jamerson Lima        jrfl          9844    16/6/2006        Rework(HFLEX-LUA-TSTR-CODE-INSP008)  --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                     Imports                                                       --
-------------------------------------------------------------------------------------------------------

require "lunit"

require "auxiliar"

lunit.import "all"

-------------------------------------------------------------------------------------------------------
--                                    Variaveis Globais                                              --
-------------------------------------------------------------------------------------------------------

--filename
filename = "/indices.txt"

--nome dos arquivos com chaves (vao ser inicializadas no setup)
rsa_private_key_filename = ""
rsa_public_key_filename  = ""
rsa_bad_key_filename     = "/badkey.key"

--nome dos arquivos que contem o rsa hash
rsa_hash_filename = "/indices.rsa"
rsa_signature     = "indices.sig"

-------------------------------------------------------------------------------------------------------
--                                        Funcoes                                                    --
-------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------
--                                   RSA TEST CASES                                                  --
-------------------------------------------------------------------------------------------------------

tc = TestCase( "rsa" )

function tc:setup()

    local m = ui.menu( "Plataforma", { "Ingenico", "Hypercom", "PC" } )

    if( ( rsa_private_key_filename:len() > 0 ) or ( rsa_public_key_filename:len() > 0 ) ) then
        return
    end

    if( m == nil ) then
        error( "Menu nao pode ser criado" )
    end

    m:show()

    if( m:accepted() == 2 ) then --hypercom
        rsa_private_key_filename = "/hyppvt.key"
        rsa_public_key_filename  = "/hyppbl.key"
    else -- ingenico e pc
        rsa_private_key_filename = "/ingpvt.key"
        rsa_public_key_filename  = "/ingpbl.key"
    end

    ui.destroy( m )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_loadkey_unloadkey1                                                           --
--  Descricao: Caso de sucesso. Carregando uma chave privada                                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_loadkey_unloadkey1()

    --verifica se a chave existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_private_key_filename ), true )

    --chama a funcao testada
    local key = rsa.loadkey( rsa_private_key_filename )

    --verifica os resultados
    assert_not_nil( key, "test_rsa_loadkey_unloadkey1. Caso de sucesso. Carregando uma chave privada", true )

    local result = key:unload( )

    assert_true( result, "test_rsa_loadkey_unloadkey1. Caso de sucesso. Descarregando uma chave privada", true )

    local error = key:unload()

    assert_nil( error, "test_rsa_loadkey_unloadkey1. Caso de sucesso. chave privada nao foi descarregada" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_loadkey_unloadkey2                                                           --
--  Descricao: Caso de sucesso. Carregando uma chave publica                                         --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_loadkey_unloadkey2()

    --verifica se a chave existe
    assert_true( os.exists( rsa_public_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_public_key_filename ), true )

    --chama a funcao testada
    local key = rsa.loadkey( rsa_public_key_filename )

    --verifica os resultados
    assert_not_nil( key, "test_rsa_loadkey_unloadkey2. Caso de sucesso. Carregando uma chave publica", true )

    local result = key:unload()

    assert_true( result, "test_rsa_loadkey_unloadkey2. Caso de sucesso. Descarregando uma chave publica" )

    local error = key:unload()

    assert_nil( error, "test_rsa_loadkey_unloadkey2. Caso de sucesso. chave publica nao foi descarregada" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_loadkey_unloadkey3                                                           --
--  Descricao: Passando uma string vazia                                                             --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_loadkey_unloadkey3()

    local result = nil
    local error_message = ""
    local error_code = 0

    --chama a funcao testada
    result, error_message, error_code =  rsa.loadkey( "" )

    --verifica os resultados
    assert_nil( result, "test_rsa_loadkey_unloadkey3. Passando uma string vazia. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_rsa_loadkey_unloadkey3. Passando uma string vazia. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_rsa_loadkey_unloadkey3. Passando uma string vazia. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_loadkey_unloadkey4                                                           --
--  Descricao: Carregando chave incorreta                                                            --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_loadkey_unloadkey4()

    local result = nil
    local error_message = ""
    local error_code = 0

    --verifica se a chave existe
    assert_true( os.exists( rsa_bad_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_bad_key_filename ), true )

    --chama a funcao testada
    result, error_message, error_code =  rsa.loadkey( rsa_bad_key_filename )

    --verifica os resultados
    assert_nil( result, "test_rsa_loadkey_unloadkey4. Carregando chave incorreta. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDRSAKEY, error_code, "test_rsa_loadkey_unloadkey4. Carregando chave incorreta. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDRSAKEY ], error_message, "test_rsa_loadkey_unloadkey4. Carregando chave incorreta. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_encrypt_decrypt1                                                             --
--  Descricao: Caso de sucesso. Encriptar e decriptar arquivo de indice                              --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_encrypt_decrypt1()

    local message = ""
    local error_message = ""

    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_public_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_public_key_filename ), true )

    --verifica se o arquivo com chave privada existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_private_key_filename ), true )

    message, error_message = LoadFileInBuffer( filename )

    assert_not_nil( message, error_message, true )

    --carrega chave publica para encriptar
    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    --encripta mensagem
    local encrypted_message = public_key:encrypt( message )

    public_key:unload()

    assert_not_nil( encrypted_message, "test_rsa_encrypt_decrypt1. Caso de sucesso. Encriptar arquivo de indice. Retorno incorreto", true )

    --carrega chave privada para decriptar a mensagem
    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    --decripta mensagem
    local result_message = private_key:decrypt( encrypted_message )

    assert_not_nil( result_message, "Dependencia: Mensagem decriptada nula", true )

    private_key:unload()

    assert_equal( message, result_message, "test_rsa_encrypt_decrypt1. Caso de sucesso. Decriptar arquivo de indice. Retorno incorreto", true )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_encrypt_decrypt2                                                             --
--  Descricao: Passando string vazia para encrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_encrypt_decrypt2()

    local result = true
    local error_message = ""
    local error_code = 0

    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_public_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_public_key_filename ), true )

    --carrega chave publica para encriptar
    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    --encripta mensagem
    result, error_message, error_code = public_key:encrypt( "" )

    public_key:unload()

    assert_nil( result, "test_rsa_encrypt_decrypt2. Passando string vazia para encrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code , "test_rsa_encrypt_decrypt2. Passando string vazia para encrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message , "test_rsa_encrypt_decrypt2. Passando string vazia para encrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_encrypt_decrypt3                                                             --
--  Descricao: Passando string vazia para decrypt                                                    --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_encrypt_decrypt3()

    local result = true
    local error_message = ""
    local error_code = 0
    
    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_private_key_filename ), true )

    --carrega chave privada para encriptar
    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    --chama funcao testada
    result, error_message, error_code = private_key:decrypt( "" )

    private_key:unload()

    assert_nil( result, "test_rsa_encrypt_decrypt3. Passando string vazia para decrypt. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code , "test_rsa_encrypt_decrypt3. Passando string vazia para decrypt. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message , "test_rsa_encrypt_decrypt3. Passando string vazia para decrypt. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_encrypt_decrypt4                                                             --
--  Descricao: Tenta encriptar com uma chave privada                                                 --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_encrypt_decrypt4()

    local result = true
    local error_message = ""
    local error_code = 0

    message, error_message = LoadFileInBuffer( filename )

    assert_not_nil( message, error_message, true )

    expected_rsa_hash, error_message = LoadFileInBuffer( rsa_hash_filename )

    assert_not_nil( expected_rsa_hash, error_message, true )
    
    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_private_key_filename ), true )

    --carrega chave privada para encriptar
    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    --encripta mensagem
    result, error_message, error_code = private_key:encrypt( message )

    private_key:unload()

    --verifica resultados
    assert_nil( result, "test_rsa_encrypt_decrypt4. Tenta encriptar com uma chave privada. Retorno incorreto", true )

    assert_equal( TST_ERR_NOTAPUBLICKEY, error_code , "test_rsa_encrypt_decrypt4. Tenta encriptar com uma chave privada. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_NOTAPUBLICKEY ], error_message , "test_rsa_encrypt_decrypt4. Tenta encriptar com uma chave privada. Mensagem de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_sign_verify1                                                                 --
--  Descricao: Caso de sucesso. Assinando e verificando arquivo de indice                            --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_sign_verify1()

    local message = ""
    local error_message = ""

    --carrega arquivo com indice
    message, error_message = LoadFileInBuffer( filename )

    assert_not_nil( message, error_message, true )

    --verifica se o arquivo com chave privada existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_private_key_filename ), true )

    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_public_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_public_key_filename ), true )

    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    local hashstring = sha1.calculate( message )

    if( not assert_not_nil( hashstring, "Dependencia: SHA1 HASH nao pode ser criado" ) ) then
        private_key:unload()
        return
    end

    local result = private_key:sign( hashstring )

    if( not assert_not_nil( result, "test_rsa_sign_verify1. Caso de sucesso. Assinando indice. Retorno incorreto" ,true ) ) then
        private_key:unload()
        return
    end

    private_key:unload()

    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    local result2 = public_key:verify( hashstring, result )

    public_key:unload()

    assert_true( result2, "test_rsa_sign_verify1. Caso de sucesso. Verificando indice. Retorno incorreto" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_sign_verify2                                                                 --
--  Descricao: Passando string vazia para sign                                                       --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_sign_verify2()

    local result = true
    local error_message = ""
    local error_code = 0

    --verifica se o arquivo com chave privada existe
    assert_true( os.exists( rsa_private_key_filename ), string.format( "Dependencia: %s nao encontrado", filename ), true )

    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    result, error_message, error_code = private_key:sign( "" )

    private_key:unload()

    assert_nil( result, "test_rsa_sign_verify2. Passando vazio para sign. Retorno incorreto", true )

    assert_equal( TST_ERR_INVALIDARG, error_code, "test_rsa_sign_verify2. Passando vazio para sign. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_rsa_sign_verify2. Passando vazio para sign. Mensegam de erro incorreta" )

end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_sign_verify3                                                                 --
--  Descricao: Tenta assinar com uma chave publica                                                   --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_sign_verify3()

    local message = ""
    local expected_rsa_hash = ""
    local expected_signature = ""
    local error_message = ""

    --carrega arquivo com indice
    message, error_message = LoadFileInBuffer( filename )
    assert_not_nil( message, error_message, true )

    --verifica se o arquivo com chave publica existe
    assert_true( os.exists( rsa_public_key_filename ), string.format( "Dependencia: %s nao encontrado", rsa_public_key_filename ), true )

    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    local hashstring = sha1.calculate( message )

    if( not assert_not_nil( hashstring, "Dependencia: SHA1 HASH nao pode ser criado" ) ) then
        public_key:unload()
        return
    end

    local result, error_message, error_code = public_key:sign( hashstring )

    public_key:unload()

    assert_nil( result, "test_rsa_sign_verify2. Assinando com chave publica. Retorno incorreto", true )

    assert_equal( TST_ERR_NOTAPRIVATEKEY, error_code, "test_rsa_sign_verify2. Assinando com chave publica. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_NOTAPRIVATEKEY ], error_message, "test_rsa_sign_verify2. Assinando com chave publica. Mensegam de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_sign_verify4                                                                 --
--  Descricao: Passando string vazia para hashstring                                                 --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_sign_verify4()

    local hashstring = sha1.calculate( "0123456789" )

    assert_not_nil( hashstring, "Dependencia: SHA1 HASH nao pode ser criado", true )

    local private_key = rsa.loadkey( rsa_private_key_filename )

    assert_not_nil( private_key, "Dependencia: Chave privada nao pode ser criada", true )

    local signature = private_key:sign( hashstring )

    private_key:unload()

    assert_not_nil( signature, "Dependencia: Assinatura nao pode ser criada" ,true )

    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    local result, error_message, error_code = public_key:verify( "", signature )

    public_key:unload()

    assert_nil( result, "test_rsa_sign_verify4. Passando string vazia para hashstring. Retorno incorreto" )
    assert_equal( TST_ERR_INVALIDARG, error_code, "test_rsa_sign_verify4. Passando string vazia para hashstring. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_rsa_sign_verify4. Passando string vazia para hashstring. Mensegam de erro incorreta" )
end

-------------------------------------------------------------------------------------------------------
--                                                                                                   --
--  Nome:      test_rsa_sign_verify5                                                                 --
--  Descricao: Passando string vazia para signature                                                  --
--                                                                                                   --
--  Argumentos:                                                                                      --
--  Dependencias:                                                                                    --
--  Retornos:                                                                                        --
--    Casos de erro:                                                                                 --
--    Casos de sucesso:                                                                              --
-------------------------------------------------------------------------------------------------------
function tc.test_rsa_sign_verify5()

    local hashstring = sha1.calculate( "0123456789" )

    assert_not_nil( hashstring, "Dependencia: SHA1 HASH nao pode ser criado", true )

    local public_key = rsa.loadkey( rsa_public_key_filename )

    assert_not_nil( public_key, "Dependencia: Chave publica nao pode ser criada", true )

    local result, error_message, error_code = public_key:verify( hashstring, "" )

    public_key:unload()

    assert_nil( result, "test_rsa_sign_verify5. Passando string vazia para signature. Retorno incorreto", true )
    assert_equal( TST_ERR_INVALIDARG, error_code, "test_rsa_sign_verify5. Passando string vazia para signature. Codigo de erro incorreto", true )

    assert_equal( lua_error_message[ TST_ERR_INVALIDARG ], error_message, "test_rsa_sign_verify5. Passando string vazia para signature. Mensegam de erro incorreta" )
end

lunit.run()