/*------------------------------------------------------------
* FileName: HF_API_main.c
* Author: fabrizio.sartori
* Date: 2015-07-13
------------------------------------------------------------*/
//#include "debugger.h"
#include <osal.h>
#include <signal.h>
#include <bits/signum.h>

#include <openssl/rsa.h>
#include <openssl/err.h>
#include <openssl/objects.h>
#include <openssl/err.h>

static void CrashReportInit(void)
{
	signal(SIGILL,    OsSaveCrashReport);
	signal(SIGABRT,   OsSaveCrashReport);
	signal(SIGBUS,    OsSaveCrashReport);
	signal(SIGFPE,    OsSaveCrashReport);
	signal(SIGSEGV,   OsSaveCrashReport);
	signal(SIGSTKFLT, OsSaveCrashReport);
	signal(SIGPIPE,   OsSaveCrashReport);
}

int main(int argc, char **argv)
{
	char szRegValue[128]={0};

	OsRegSetValue("persist.sys.backlighttime",szRegValue);

	OsLogSetTag("RedeFlex");

//	OsLog(LOG_DEBUG,"OsCheckBattery return [%d]",OsCheckBattery());

	CrashReportInit();


	HF_AMS_Main();
	return 0;
}
