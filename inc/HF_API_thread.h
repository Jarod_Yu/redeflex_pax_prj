/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_thread.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Renato
Data     : 
Empresa  : 
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_API_THREAD_H
#define _HF_API_THREAD_H

/***************************************************************************************************************
 * Description:
 * Create a mutex for synchronization between multiple threads.
 * Input Parameter:
 * sem - Pointer should receive an integer that identifies the mutex.
 * Output Parameter:nothing
 * Return:
 * RF_SUCCESS - managed to create the mutex.
 * Another value - error
 ***************************************************************************************************************/
HF_INT32 HF_thread_semaphoreCreate(HF_INT32 *sem);

/***************************************************************************************************************
 * Description:
 * Get exclusive access to the mutex represented by without.
 * Input Parameter:
 * sem - Pointer to the integer that identifies the mutex whose exclusive access is being requested.
 * Output Parameter:
 * Return:
 * RF_SUCCESS - managed to get exclusive access.
 * Another value – error
 ***************************************************************************************************************/
HF_INT32 HF_thread_semaphoreWait(HF_INT32 *sem);

/***************************************************************************************************************
 * Description:
 * Releases the exclusive access to the mutex (previously obtained via RF_thread_semaphoreWait).
 * Input Parameter:
 * sem - Pointer to the integer that identifies the mutex whose exclusive access being released.
 * Output Parameter:
 * Return:
 * RF_SUCCESS - managed to release exclusive access.
 * Another value - error
 ***************************************************************************************************************/
HF_INT32 HF_thread_semaphorePost(HF_INT32 *sem);

/***************************************************************************************************************
 * Description:
 * Create a thread for concurrent execution (not necessarily parallel) of threadMainFunc function.
 * Input Parameter:
 * threadHnd - Pointer should receive a whole (handle) that identifies the thread.
 * threadMainFunc - Pointer to the function to be executed by the thread.
 * Output Parameter:
 * Return:
 * RF_SUCCESS - managed to create a thread.
 * Another value – error
 ***************************************************************************************************************/
HF_INT32 HF_thread_iThreadStart(HF_INT32 *threadHnd, void (*threadMainFunc)(void));

/***************************************************************************************************************
 * Description:
 * Terminate the thread whose handle is passed as a parameter.
 * Input Parameter:
 * threadHnd - integer that identifies the thread (previously created by RF_thread_iThreadStart).
 * Output Parameter:
 * Return:
 * RF_SUCCESS - managed to end a thread.
 * Another value – error
 ***************************************************************************************************************/
HF_INT32 HF_thread_iThreadEnd(HF_INT32 threadHnd);

#endif
