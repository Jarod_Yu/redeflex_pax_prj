/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_sys.h
Descrição: CabeÃ§alho que exporta funÃ§Ãµes de Sistema
Autor    : Leonardo
Data     : 16/05/2007
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_SYS_H_
#define _HF_SYS_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/
#define HF_VERSION     	        "900"
#define HF_SUBVERSION           "0100D"

// comprimento máximo de uma propriedade de sistema
#define HF_SYS_MAXPRTBUFSIZE 1024

// propriedades de sistema
#define HF_SYS_IDPRTYSERIALNU         	1 // numero de serie do terminal
#define HF_SYS_IDPRTYHIPERFLEXVER     	2
#define HF_SYS_IDPRTYPLATFORMNAME     	3
#define HF_SYS_IDPRTYPLATFORMVER      	4
#define HF_SYS_IDPRTYGPRS             	5
#define HF_SYS_IDPRTYHIPERFLEXFULLVER 	6
#define HF_SYS_IDPRTYHIPERFLEXEXTRADATA 7
#define HF_SYS_IDCONNECTIONTYPE         8
//#define HF_SYS_IDPRTY_TCPIP_CON       9 // informa se vai utilizar uma conexao tcpip. util para usar ou nao 2 bytes de tamanho na msg iso8583.
#define HF_SYS_USEUSBKEY                10

#define HF_SYS_BATTERY_NO_CHARGE     0
#define HF_SYS_BATTERY_CHARGING      1
#define HF_SYS_BATTERY_LOW_ALARM     2

// Conexoes suportadas
#define HF_SYS_CONN_DIAL      0
#define HF_SYS_CONN_GPRS      1
#define HF_SYS_CONN_ETHERNET  2
#define HF_SYS_CONN_WIFI      3

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_INT32 HF_sys_getProperty(HF_UINT32 prtyId, HF_CHAR *outBuffer, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Initializes the API redeflex. This function must be called before any use of the API, thus
 * 					  avoiding anomalous behavior. She is responsible for the specific initialization of each platform.
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 *  				  RF_ERR_NOMEMORY -  If there is no more free memory in the system.
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 * 					  RF_ERR_RESOURCEALLOC - If a shared resource could not meet the requisition
 ***************************************************************************************************************/
HF_INT32 HF_sys_startHiperflex(HF_VOID);

/***************************************************************************************************************
 * Description: 	  Desaloca resources reserved by the redeflex CA during its initialization. After a call to this function,
 * 					  called after the redeflex CA may not have the expected behavior. This function should be called
 * 					  when the redeflex CA is not necessary any more.
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 ***************************************************************************************************************/
HF_INT32 HF_sys_shutdownHiperflex(HF_VOID);

/***************************************************************************************************************
 * Description: 	  Get the status of the battery.
 * Input Parameter:	  NULL
 * Output Parameter:  chargStatus - Returns the level of charge of the battery (value from 0 to 100)
 * 					  Status - Returns the state of the battery. It may be:
 * 					  RF_sys_chargeable_battery (battery under load);
 * 					  RF_sys_battery_in_charge (battery is not charged);
 * 					  RF_sys_battery_low_alarm (battery in low level)
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_sys_getBatteryStatus(HF_INT32 * chargStatus, HF_INT32 * Status);

/***************************************************************************************************************
 * Description: 	  Some batteries require that their first shipment is to be made by at least 6 hour to be considered "initialized".
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  RF_TRUE - If the battery is initialized or if the battery has such a concept.
 * 					  RF_FAULT - If the battery is not initialized.
 ***************************************************************************************************************/
HF_BOOL HF_sys_isBatteryInitialized (HF_VOID);

/***************************************************************************************************************
 * Description: 	  Deactivates the POS consumption. This function makes the POS is turned off if it is powered by a battery and the same is not being loaded.
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_sys_alimOff(HF_VOID);

HF_BOOL HF_sys_checkProperty(HF_UINT32 prtyId, HF_CHAR *prtyValue);

HF_INT32 HF_sys_versionDefinition(HF_INT32 versionType, HF_CHAR *outBuffer, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Returns the serial number of the terminal.
 * Input Parameter:	  buffSz - The output buffer size
 * Output Parameter:  outBuffer - The place where it will be placed the result of the query
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_SMALLBUFF - If the buffer passed parameter is insufficient to copy the serial number.
 ***************************************************************************************************************/
HF_INT32 HF_sys_prtySerialNumber(HF_CHAR *outBuffer, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Returns the version of the platform (e.g. "IWP"). Consult the network on which should be returned for a new device.
 * Input Parameter:	  buffSz - The output buffer size
 * Output Parameter:  outBuffer - The place where it will be placed the result of the query
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_SMALLBUFF - If the buffer passed parameter is insufficient to copy the serial number.
 ***************************************************************************************************************/
HF_INT32 HF_sys_prtyPlataformVersion(HF_CHAR *outBuffer, HF_UINT32 buffSz);

HF_INT32 HF_sys_prtyGprsSupport(HF_CHAR *outBuffer, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Returns the name of the platform (e.g., Ingenico)
 * Input Parameter:	  buffSz - The output buffer size
 * Output Parameter:  outBuffer - The place where it will be placed the result of the query
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_SMALLBUFF - If the buffer passed parameter is insufficient to copy the serial number.
 ***************************************************************************************************************/
HF_INT32 HF_sys_prtyPlataformName(HF_CHAR *outBuffer, HF_UINT32 buffSz);

HF_INT32 HF_sys_prtyExtraData(HF_CHAR *outBuffer, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Get a list of types of connectivity supported by the equipment.
 * Input Parameter:	  NULL
 * Output Parameter:  connTypes - Structure that will be filled with the connectivity options
 * 					  size - Pointer to an integer that has to be filled with the quantity of connection options.
 * Return:			  RF_SUCCESS - If the function successfully executed
 ***************************************************************************************************************/
HF_INT32 HF_sys_getConnectionsSuported(HF_INT32 *connTypes, HF_INT32 *size);

HF_INT32 HF_sys_getConnectionType(HF_CHAR *outBuffer, HF_UINT32 buffSz);

HF_INT32 HF_sys_getUseUsbKey(HF_CHAR *outBuffer, HF_UINT32 buffSz);

HF_INT32 HF_sys_reset(HF_VOID);

HF_INT32 HF_sys_setStandbyTimeout (HF_INT32 timeout);

HF_INT32 HF_sys_getStandbyTimeout (HF_VOID);

/***************************************************************************************************************
 * Description: 	  Returns information about the device.
 * Input Parameter:	  option - Specify which option is being requested
 * Output Parameter:  output - Pointer that will receive a pointer to the data of the requested information.
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  Another code, error. In this case, the chamante should not desalocar memory to output.
 * 					  The following options should be implemented:
 * 					  RF_DEVICE_INFO_VERSAO_SO (valor 1) - Version of the operating system of the device.
 * 					  RF_DEVICE_INFO_VERSAO_KERNEL_EMV (valor 50) - The EMV kernel version that is being used.
 * 					  RF_DEVICE_INFO_VERSAO_EMV_CTLESS (valor 100) - Version of the module which deals with size contactless cards.
 * 					  RF_DEVICE_INFO_VERSAO_CTLESS_JCB (valor 101) - Contactless application version of JCB.
 * 					  RF_DEVICE_INFO_VERSAO_CTLESS_MASTERCARD (valor 102) - Contactless application version of MASTERCARD.
 * 					  RF_DEVICE_INFO_VERSAO_CTLESS_VISA (valor 103) - Contactless application version of visa.
 * 					  RF_DEVICE_INFO_VERSAO_CTLESS_AMEX (valor 104) - Contactless application version of AMEX.
 ***************************************************************************************************************/
HF_INT32 HF_sys_getDeviceInfo(HF_UINT32 option, HF_CHAR** output);

#endif
