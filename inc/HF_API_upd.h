/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_upd.h
Descrição: Cabeçalho que exporta as funções de update.
Autor    : Tiago
Data     : 25/03/2009
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_UPD_H_
#define _HF_UPD_H_

typedef struct _HF_UPD_PARAMS_ {
    /* Prefixo da discagem (configurado no terminal).                                            */
    const HF_CHAR* pabx;
    /* Telefone do servidor.                                                                     */
    const HF_CHAR* phone;
    /* Modo de discagem (tone ou pulse).                                                         */
    const HF_CHAR* dial_mode;
    /* N�mero da campanha (usado pelo servidor ingenico para determinar o pacote que ele enviar� */
    /* para o terminal.                                                                          */
    const HF_CHAR* caller_id;
    /* N�mero l�gico (com ele o servidor hypercom determina a aplica��o que enviar�              */
    /* para o terminal).                                                                         */
    const HF_CHAR* num_logico;
} HF_UPD_PARAMS;

HF_INT32 HF_upd_autocargaHiperflex (const HF_CHAR *caminho );
HF_INT32 HF_upd_downloadHiperflex( const HF_UPD_PARAMS* params );

#endif
