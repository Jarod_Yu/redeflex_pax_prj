/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_rs232.h
Descrição: Funções para comunicacao com porta serial
Autor    : Guilherme
Data     : 17/10/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_API_RS232_H_
#define _HF_API_RS232_H_
/*-----------------------------------------------------------------------------------------------*/
/* Nome do arquivo:                 HF_API_rs232.h  											 */
/* Tres Letras Representativas:     RS2                                                          */
/* Descricao:                       Fun��es para comunicacao com porta serial					 */
/*-----------------------------------------------------------------------------------------------*/
/*                            Historico                                                          */
/* Nome                 Login       Data        Descricao                                        */
/*-----------------------------------------------------------------------------------------------*/
/* Guilherme            gkmo        17/10/2006  Removendo API antiga.                            */
/* Guilherme            gkmo        16/10/2006  Removendo constantes espec�ficas de plataforma.  */
/* Guilherme            gkmo        10/10/2006  Nova API para comunica��o serial.                */
/* Itapaje              ifst        20/04/2006  Incluido 'extern C' (09458).                     */
/* Itapaje              ifst        08/03/2006  Criacao                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_RS232_HANDLE HF_VOID

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

    typedef enum {
        HF_RS232_PARITY_NONE,
        HF_RS232_PARITY_ODD,
        HF_RS232_PARITY_EVEN
    } HF_RS232_PARITY;

    typedef enum {
        HF_RS232_BAUD_300,
        HF_RS232_BAUD_600,
        HF_RS232_BAUD_1200,
        HF_RS232_BAUD_2400,
        HF_RS232_BAUD_4800,
        HF_RS232_BAUD_9600,
        HF_RS232_BAUD_19200,
        HF_RS232_BAUD_38400,
        HF_RS232_BAUD_57600,
        HF_RS232_BAUD_115200
    } HF_RS232_BAUD;

    typedef enum {
        HF_RS232_DATASIZE_7,
        HF_RS232_DATASIZE_8
    } HF_RS232_DATASIZE;

    typedef enum {
        HF_RS232_STOP_1,
        HF_RS232_STOP_2
    } HF_RS232_STOP_BITS;

    typedef enum {
        HF_RS232_FLOW_NONE,
        HF_RS232_FLOW_HARD
    }HF_RS232_FLOW_CONTROL;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/
    typedef HF_VOID (*HF_FN_RECEIVE_BYTES)(HF_UINT8 *buffer, HF_UINT32 size);

    typedef struct {
        HF_RS232_BAUD baud;
        HF_RS232_PARITY parity;
        HF_RS232_DATASIZE dataSize;
        HF_RS232_STOP_BITS stopBits;
        HF_RS232_FLOW_CONTROL flowControl;
        HF_FN_RECEIVE_BYTES readCallback;
    } HF_RS232_CONFIG;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/***************************************************************************************************************
 * Description: 	  Opens a serial port.
 * Input Parameter:	  port - Serial port to be opened (RF_RS232_COM1 or RF_RS232_COM2)
 *					  config -The structure containing the port settings. More details on the configuration
 *					  of the serial ports can be seen in Appendix H.
 * Output Parameter:  handle - Pointer to pointer which will be allocated on the "handle" open the door.
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT¨C  if there was a failure in the device or the serial port does not exist.
 * 					  RF_ERR_RESOURCEALLOC - If the shared resource can not meet the requisition
 ***************************************************************************************************************/
HF_INT32 HF_rs232_openH(HF_CHAR *port, const HF_RS232_CONFIG *config, HF_RS232_HANDLE **handle);

/***************************************************************************************************************
 * Description: 	  Wait a message via the serial port referenced by handle.
 * Input Parameter:	  handle - "Handle" the open door by HF_rs232_openH
 *					  bufflen - The maximum quantity to be stored in the buffer
 *					  timeout - Maximum waiting time for the data (in milliseconds)
 * Output Parameter:  buffer - Pointer to the space in memory where the data to be written
 * Return:			  receive data length - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT¨C  if there was a failure in the device or the serial port does not exist.
 * 					  RF_ERR_RESOURCEALLOC - If the shared resource can not meet the requisition
 ***************************************************************************************************************/
HF_INT32 HF_rs232_recvmsg(const HF_RS232_HANDLE *handle, HF_UINT8 *buffer, HF_UINT16 bufflen, HF_UINT32 timeout);

/***************************************************************************************************************
 * Description: 	  Send a packet of data via RS232.
 * Input Parameter:	  handle - "Handle" the open door by HF_rs232_openH
 *					  data - Pointer to the data to be sent
 *					  datalen - The amount of data to be sent
 *					  timeout - Maximum waiting time until the data are sent (in milliseconds)
 * Output Parameter:  NULL
 * Return:			  send data length - If the function successfully executed
 * 					  RF_ERR_TIMEOUT ¨C  If any byte was sent in timeout milliseconds
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_rs232_sendmsg(const HF_RS232_HANDLE *handle, const HF_UINT8 *data, HF_UINT16 datalen, HF_UINT32 timeout);

/***************************************************************************************************************
 * Description: 	  Close the serial port referenced by "handle".
 * Input Parameter:	  handle - Reference to the serial port to be closed.
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 ***************************************************************************************************************/
HF_INT32 HF_rs232_close(HF_RS232_HANDLE *handle);

// TODO avaliar remo��o
//conexao serial via usb (telium)
HF_INT32 HF_rs232_usb_open(HF_VOID);
HF_INT32 HF_rs232_usb_close(HF_VOID);
HF_INT32 HF_rs232_usb_recvmsg(HF_UINT8 *buffer, HF_UINT16 bufflen, HF_UINT32 timeout);
HF_INT32 HF_rs232_usb_sendmsg(const HF_UINT8 *data, HF_UINT16 datalen, HF_UINT32 timeout);
HF_INT32 HF_rs232_usb_status(HF_VOID);

#endif

