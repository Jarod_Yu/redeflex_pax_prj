/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_emv.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Renato
Data     : 27/03/2015
Empresa  : 
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_EMV_H_
#define _HF_EMV_H_

/*===========================================================================*/
/*                           Constants                                       */
/*===========================================================================*/

/* ---------------- */
/* TAGs do Terminal */
/* ---------------- */
#define HTAG_ADDTRMCAP	   0x9F40
#define HTAG_AMOUNT	      0x81
#define HTAG_AMOUNTOTHER	0x9F04
#define HTAG_TAPPVERNUM	   0x9F09
#define HTAG_ARC	         0x8A
#define HTAG_CAPK	         0xDF01
#define HTAG_DDOLDEF	      0xDF02
#define HTAG_TDOLDEF	      0xDF03
#define HTAG_ONLDATA	      0xDF04
#define HTAG_MAXTPBRS	   0xDF05
#define HTAG_TPBRS	      0xDF06
#define HTAG_TACDEF	      0xDF07
#define HTAG_TACDEN	      0xDF08
#define HTAG_TACONL	      0xDF09
#define HTAG_TERMCAPAB	   0x9F33
#define HTAG_TRMCOUNTRY	   0x9F1A
#define HTAG_FLOORLIMIT	   0x9F1B
#define HTAG_TRMTYPE	      0x9F35
#define HTAG_TVBRS	      0xDF0A
#define HTAG_TRNCURRCODE	0x5F2A
#define HTAG_TRNCURREXP	   0x5F36
#define HTAG_TRNDATE	      0x9A
#define HTAG_TRNSEQCOUNT	0x9F41
#define HTAG_TRNTIME	      0x9F21


#define HTAG_MSGPIN	      0xDF20
#define HTAG_MSGERRPIN	   0xDF21
#define HTAG_MSGWRNPIN	   0xDF22
#define HTAG_MSGBLKPIN	   0xDF23
#define HTAG_MSGAFTPIN	   0xDF24
#define HTAG_KEYIDX	      0xDF25
#define HTAG_WK	         0xDF26

#define HTAG_APPCURRCODE	0x9F42
#define HTAG_APPCURREXP	   0x9F44
#define HTAG_APPEFFDATE	   0x5F25
#define HTAG_APPEXPDATE	   0x5F24
#define HTAG_AIP	         0x82
#define HTAG_DFNAME	      0x84
#define HTAG_PAN	         0x5A
#define HTAG_PANSEQNO	   0x5F34
#define HTAG_AUC	         0x9F07
#define HTAG_CAPPVERNUM	   0x9F08
#define HTAG_CHNAME	      0x5F20
#define HTAG_ISSCOUNTRY	   0x5F28
#define HTAG_TRK2	         0x57


#define HTAG_APPCRYPTO	   0x9F26
#define HTAG_ATC	         0x9F36
#define HTAG_CVMRESULTS	   0x9F34
#define HTAG_CRYPTOINFO	   0x9F27
#define HTAG_ISSAPPDATA	   0x9F10
#define HTAG_SCRIPTRES	   0xDF0B
#define HTAG_TVR	         0x95
#define HTAG_TSI	         0x9B
#define HTAG_UNPREDNO	   0x9F37
#define HTAG_ONLPINRES	   0xDF0C

#define HTAG_IACDEFAULT    0x9F0D 
#define HTAG_IACDENIAL     0x9F0E 
#define HTAG_IACONLINE     0x9F0F 
#define HTAG_CVMLIST       0x8E   
#define HTAG_CDOL1         0x8C   
#define HTAG_CDOL2         0x8D   
#define HTAG_SERVCODE      0x5F30 
#define HTAG_TRK1DISCDATA  0x9F1F 
#define HTAG_TRK2DISCDATA  0x9F20 
#define HTAG_SSAPPDATA     0x93   
#define HTAG_IPKCERT       0x90   
#define HTAG_CAPKIDX       0x8F   
#define HTAG_IPKEXP        0x9F32 
#define HTAG_IPKREM        0x92   

/* ------------------------- */
/* Erros Genericos           */
/* ------------------------- */

/* Fun��o executada com sucesso. */
#define HEMVERR_NONE	                0	
/* Par�metro inv�lido passado � fun��o. */
#define HEMVERR_INVPARAM	         -1	
/* Estouro de mem�ria (ou "buffer overflow"). */
#define HEMVERR_BUFOVFL	            -2	
/* Erro interno de execu��o. */
#define HEMVERR_EXECERR	            -3	
/* Erro no formato TLV de entrada da fun��o.  */
#define HEMVERR_BADTLVFORMAT	      -4	
/* Erro de retorno de oper��o cancelada  */
#define HEMVERR_OPERCANC      	   -5
/* Pin ByPASS */
#define HEMVERR_BYPASS         	   -6

/* ------------------------- */
/* Erros de acesso ao cart�o */
/* ------------------------- */
/*	Cart�o ausente. */
#define HEMVERR_NOCARD	            -21
/*	Cart�o mudo (ou n�o h� ATR) */
#define HEMVERR_CARDMUTE	         -22
/*	Erro de comunica��o entre o cart�o e o dispositivo de interface */
#define HEMVERR_ICCCOMMERR	         -23
/*	Erro de comunica��o entre o dispositivo de interface e a aplica��o. */
#define HEMVERR_IFDCOMMERR	         -24

/* ------------------------- */
/* Erros de alto n�vel       */
/* ------------------------- */

/*	Cart�o bloqueado. */
#define HEMVERR_CARDBLOCKED	      -81
/*	Cart�o retornou status inesperado ou dados inv�lidos na resposta a um comando. */
#define HEMVERR_CARDPROBLEM	      -82
/*	Cart�o cont�m dados EMV inv�lidos. */
#define HEMVERR_INVCARDDATA	      -83
/*	Nenhuma aplica��o compat�vel foi achada no cart�o. */
#define HEMVERR_NOCANDIDATE	      -84
/*	Sele��o final de aplica��o falhou. Esta ser� retirada do menu para novo processo de escolha. */
#define HEMVERR_SELECTFAILED	      -85
/*	Objeto TLV requerido n�o foi achado.*/
#define HEMVERR_OBJNOTFOUND	      -86
/*	Objeto duplicado no "pool" TLV. */
#define HEMVERR_DUPLICATEDOBJECT	   -87
/*	Transa��o finalizada - "Service not allowed". */
#define HEMVERR_SERVICENOTALLOWED	-88


/* Constantes para serem usadas como Resultado da transa��o online */

#define HEMVACT_ONL_APPR     0  /* Transa��o aprovada pelo Host */
#define HEMVACT_ONL_DENY     1  /* Transa��o negada pelo Host */
#define HEMVACT_UNAB_ONL     2  /* N�o foi poss�vel conectar o Host */

/* Constantes para serem usadas como Resultado da transa��o */
#define  HEMV_RESULT_NEG      0
#define  HEMV_RESULT_APR      1
#define  HEMV_RESULT_ONL      2
/*===========================================================================*/
/*                           Macros                                          */
/*===========================================================================*/
/*===========================================================================*/
/*                       Type definitions                                    */
/*===========================================================================*/
/*===========================================================================*/
/*                    Extern global variables                                */
/*===========================================================================*/
/*===========================================================================*/
/*                      Exported functions                                   */
/*===========================================================================*/


/*===========================================================================*\
 Fun��o   : HEMV_iInit
 
 Descri��o: Esta fun��o inicializa a Camada HEMV, e deve ser chamada pela 
            aplica��o uma �nica vez, antes de chamar qualquer outra fun��o 
            do kernel
 
 Entradas : n�o h�.
 
 Sa�das   : n�o h�.
 
 Retorno  : n�o h�.
\*===========================================================================*/
int HEMV_iInit (void);

/*===========================================================================*\
 Fun��o   : HEMV_Version
 
 Descri��o: Esta fun��o retorna uma string contendo a vers�o do Kernel EMV do terminal
 
 Entradas : n�o h�.
 
 Sa�das   : pszVersion = String de final NUL contendo a vers�o do Kernel EMV, 
                         cujo formato depende da plataforma.
                         (m�ximo 20 caracteres, excluindo-se o terminador).
 
 Retorno  : n�o h�.
\*===========================================================================*/
void HEMV_Version (char *pszVersion);

/*===========================================================================*\
 Fun��o   : HEMV_iCheckCard
 
 Descri��o: Esta fun��o verifica se um cart�o est� inserido no acoplador de 
            cart�o com chip. Ela pode ser chamada a qualquer momento, n�o 
            interferindo com a transa��o caso esta esteja em andamento.
 
 Entradas : n�o h�.
 
 Sa�das   : n�o h�.
 
 Retorno  : HEMVERR_NONE	Cart�o presente.
            HEMVERR_NOCARD	Cart�o ausente.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iCheckCard (void);

/*===========================================================================*\
 Fun��o   : HEMV_iStartTransaction
 
 Descri��o: Esta fun��o inicializa a Camada HEMV, limpando as vari�veis e 
            recursos do processamento, assim como todos os par�metros definidos 
            pela fun��o HEMV_iDefData , 
            devendo ser chamada antes de iniciar cada uma das transa��es EMV.
 
 Entradas : n�o h�.
 
 Sa�das   : n�o h�.
 
 Retorno  : HEMVERR_NONE	Opera��o bem sucedida.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iStartTransaction (void);

/*===========================================================================*\
 Fun��o   : HEMV_iDefData
 
 Descri��o: Esta fun��o insere um par�metro (objeto) EMV na Camada HEMV. 
            Esta fun��o pode ser chamada diversas vezes, a qualquer momento, 
            de forma a definir par�metros necess�rios ao processamento dos cart�es 
 
 Entradas : uiTag	   = Tag que identifica o par�metro sendo inserido (HTAG_xxxx).
            iLen	   = Tamanho do par�metro.
            pbValue  = Ponteiro para o valor do par�metro.
 
 Sa�das   : n�o h�.
 
 Retorno  : HEMVERR_NONE	Opera��o bem sucedida.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iDefData (unsigned int uiTag, int iLen, const unsigned char *pbValue);

/*===========================================================================*\
 Fun��o   : HEMV_iGetData
 
 Descri��o: Esta fun��o obt�m um dado (objeto) EMV da Camada HEMV. 
            Esta fun��o pode ser chamada diversas vezes, a qualquer momento, 
            de forma a obter dados resultantes do processamento dos cart�es
 
 Entradas : uiTag	   = Tag que identifica o par�metro a ser obtido (HTAG_xxxx).
 
 Sa�das   : piLen	   = Tamanho do par�metro obtido.
            pbValue	= Ponteiro para o valor do par�metro
 
 Retorno  : HEMVERR_NONE	      Opera��o bem sucedida.
            HEMVERR_INVPARAM	   Par�metro inv�lido passado � fun��o.
            HEMVERR_OBJNOTFOUND	Dado n�o existe (n�o foi localizado).
            HEMVERR_xxxxx        Erro
\*===========================================================================*/
int HEMV_iGetData (unsigned int uiTag, int *piLen, unsigned char *pbValue);

/*===========================================================================*\
 Fun��o   : HEMV_iDefApp
 
 Descri��o: Esta fun��o insere uma aplica��o conhecida pelo terminal na lista 
            das aplica��es que podem participar do processo de sele��o no cart�o. 
            HEMV_iDefApp pode ser chamada diversas vezes, 
            sendo este processo iniciado por HEMV_iStartTransaction.
 
 Entradas : iRef	   =  C�digo propriet�rio de refer�ncia  (0 a 255) a ser 
                        devolvido pela fun��o HEMV_iGetCandidateList para 
                        cada aplica��o reconhecida como "candidata" � sele��o.
            iAID_Len	=  Tamanho do identificador em pbAID (em bytes - 5 a 16).
            pbAID	   =  Ponteiro para o identificador da aplica��o, de 5 a 16 bytes.
 
 Sa�das   : n�o h�.
 
 Retorno  : HEMVERR_NONE	Opera��o bem sucedida.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iDefApp (int iRef, int iAID_Len, const unsigned char *pbAID);

/*===========================================================================*\
 Fun��o   : HEMV_iGetCandidateList
 
 Descri��o: Esta fun��o deve ser chamada pela aplica��o ap�s a inser��o da 
            lista de aplica��es usando-se chamadas � fun��o HEMV_iDefApp.
            A Camada HEMV ativa o cart�o e efetua o processo de obten��o da 
            lista de candidatos. Os labels das aplica��es candidatas s�o 
            retornados, juntamente com seus c�digos propriet�rios de refer�ncia, 
            j� em ordem de prioridade para que a aplica��o possa exibir um menu 
            (se necess�rio).
            Antes do processamento de HEMV_iGetCandidateList, deve-se fornecer 
            atrav�s da fun��o HEMV_iDefData a tag HTAG_ADDTRMCAP 
            (caso j� sejam conhecidos da aplica��o):

 
 Entradas : piItems	= N�mero m�ximo de aplica��es candidatas suportado 
                       pelo espa�o alocado para pbLabels.
 
 Sa�das   : piItems	= N�mero de aplica��es candidatas em pbLabels
                       0 = Indica que h� somente uma candidata e 
                           que ela requer confirma��o do operador.
                       1 = Indica que h� somente uma candidata e 
                           que ela n�o requer confirma��o do operador.
                     > 1 = Indica que h� mais de uma candidata, 
                           sendo necess�ria a montagem de um menu.
            
            pbLabels = Seq��ncia ordenada de registros, 
                       Sendo que cada registro possui o seguinte formato:
                       Refer�ncia (1 byte)	C�digo iRef fornecido em HEMV_iDefApp; e
                       Label (16 bytes)	Label da aplica��o (com espa�os � direita).

 
 Retorno  : HEMVERR_NONE	      Opera��o bem sucedida, foi encontrada uma ou mais candidatas.
            HEMVERR_NOCANDIDATE	Nenhuma aplica��o candidata foi encontrada.
            HEMVERR_CARDBLOCKED	Cart�o bloqueado.
            HEMVERR_xxxxx        Erro

\*===========================================================================*/
int HEMV_iGetCandidateList (int *piItems, unsigned char *pbLabels);

/*===========================================================================*\
 Fun��o   : HEMV_iSelectApp
 
 Descri��o: Em HEMV_iSelectApp a Camada HEMV dever� efetuar os seguintes 
            processamentos regidos pela norma:
            "	Initiate Application Processing; e
            "	Read Application Data.
            Caso o kernel identifique um problema que demande a retirada da 
            aplica��o da lista de candidatas para uma nova sele��o, essa situa��o 
            � informada pelo c�digo de retorno HEMVERR_SELECTFAILED de maneira 
            que a aplica��o principal chame novamente HEMV_iGetCandidateList.
 
 Entradas : iAppIdx	= �ndice da aplica��o selecionada (de 0 em diante), 
                       conforme seq��ncia devolvida por HEMV_iGetCandidateList.
 
 Sa�das   : n�o h�.
 
 Retorno  : HEMVERR_NONE	      Opera��o bem sucedida.
            HEMVERR_INVPARAM	   Par�metro inv�lido passado � fun��o.
            HEMVERR_SELECTFAILED	Falha de sele��o, aplica��o foi retirada do menu 
                                 (deve-se chamar HEMV_iGetCandidateList novamente).
            HEMVERR_xxxxx        Erro

\*===========================================================================*/
int HEMV_iSelectApp (int iAppIdx);

/*===========================================================================*\
 Fun��o   : HEMV_iProcessTransaction
 
 Descri��o: Esta fun��o efetua os seguintes processamentos regidos pela norma EMV, 
            at� chegar � primeira decis�o do cart�o:
            "	Offline Data Authentication;
            "	Processing Restrictions;
            "	Cardholder Verification;
            "	Terminal Risk Management;
            "	Terminal Action Analisys; e
            "	Card Action Analisys.
 
 Entradas : phgHmi = Ponteiro para o Handle das fun��es de I/O.
 
 Sa�das   : piResult	    = Resultado da transa��o:
                           0 Negada offline.
                           1 Aprovada offline.
                           2 Deve-se pedir autoriza��o online.
            
             piCVMResult =	Resultado da "verifica��o do portador":
                           0 N�o efetuada (ou falhou).
                           1 PIN offline verificado com sucesso.
                           2 PIN online deve ser enviado para verifica��o no 
                             Emissor (HTAG_ONLPINRES est� dispon�vel).
                           3 Deve-se obter assinatura em papel.
                           4 PIN offline verificado com sucesso e, al�m disso, 
                             deve-se obter assinatura em papel.
                           5 PIN online deve ser enviado para verifica��o 
                              no Emissor (HTAG_ONLPINRES est� dispon�vel) e, 
                              al�m disso, deve-se obter assinatura em papel.
            pvHandle = Ponteiro para o Handle das fun��es de I/O. O handle pode ser
                        Alterado caso acontece captura de senha.
 
 Retorno  : HEMVERR_NONE	Opera��o bem sucedida.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iProcessTransaction (int *piResult, int *piCVMResult, 
                               void *pvHandle);

/*===========================================================================*\
 Fun��o   : HEMV_iCompleteTransaction
 
 Descri��o: Caso a fun��o HEMV_iProcessTransaction indique que a autoriza��o 
            dever� ser feita pelo Emissor (online), esta fun��o dever� ser 
            chamada ao final para completar a transa��o, efetuando os 
            seguintes passos regidos pela norma EMV:
            "	Issuer Authentication;
            "	Script Processing; e
            "	Completion.
 
 Entradas : iOnlineResult = Resultado da transa��o online:
                           0 Negada pelo host.
                           1 Aprovada pelo host.
                           2 N�o foi poss�vel comunicar com o host.
 
 Sa�das   : piResult	   = Resultado final da transa��o:
                           0 Negada pelo cart�o.
                           1 Aprovada pelo cart�o.

 
 Retorno  : HEMVERR_NONE	Opera��o bem sucedida.
            HEMVERR_xxxxx  Erro
\*===========================================================================*/
int HEMV_iCompleteTransaction (int iOnlineResult, int *piResult);

//int HEMV_iPowerOnCard(int *hstBytesSz, unsigned char hstBytes);

//int HEMV_iPowerOffCard(void);

//int HEMV_iTransmit(int sizeofCmdReceived, unsigned char *cmdReceived, int *cmdRespLen, unsigned char cmdResponse);

#endif /* ndef _HF_EMV_H_ */

