/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API.h
Descrição: CabeÃ§alho
Autor    : Leonardo
Data     : 01/04/2008
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex

Autor    : Anderson Neves
Data     : 19/05/2015
Empresa  : C.E.S.A.R
Descrição: Inclusão do arquivo HF_API_util.h nos includes
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_API_H_
#define _HF_API_H_
            
/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/* Globais */
#include "HF_API_types.h"
#include "helperfunctions_common.h"
#include "HF_API_return_codes.h"

/* Camada de abstracao */
#include "HF_API_3des.h"
#include "HF_API_comms.h"
#include "HF_API_datetime.h"
#include "HF_API_emv.h"
#include "HF_API_ethernet.h"
#include "HF_API_fs.h"
#include "HF_API_image.h"
#include "HF_API_display.h"
#include "HF_API_keyboard.h"
#include "HF_API_management.h"
#include "HF_API_modular.h"
#include "HF_API_pin.h"
#include "HF_API_PPComp.h"
#include "HF_API_printer.h"
#include "HF_API_rs232.h"
#include "HF_API_RSA.h"
#include "HF_API_sha1.h"
#include "HF_API_sys.h"
#include "HF_API_thread.h"
#include "HF_API_upd.h"
#include "RF_API_device_info.h"
#include "RF_API_gsm.h"
#include "RF_API_memory.h"
#include "RF_API_wifi.h"
#include "RF_API_sec.h"
#include "RF_API_basebt.h"

/* Funcoes publicas internas */
#include "HF_API_base64.h"
#include "HF_API_CRC32.h"
#include "HF_API_log.h"
#include "HF_API_prng.h"
#include "HF_API_propertyfiles.h"
#include "HF_API_util.h"
#include "RF_API_lua_socket.h"

/* PAX header files*/
#include <osal.h>

/* BC header file */
#include "ppcomp.h"

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*
 * Para atualização de plataforma, deve ser recuperado da versão anterior
 * os campos abaixo na respectiva ordem:
 *
 * - Número lógico
 * - Prefixo de discagem
 */
#define RF_PREVIOUS_CFG_FIELDS_COUNT        2
#define RF_PREVIOUS_CFG_MAX_LENGTH          25

#ifndef RF_PREVIOUS_CFG_VOLUMES_COUNT

#ifdef _TELIUM_
#define RF_PREVIOUS_CFG_VOLUMES_COUNT       2

#else // Verifone
#define RF_PREVIOUS_CFG_VOLUMES_COUNT       1

#endif
#endif

typedef struct RF_PREVIOUS_CFG_T
{
	/* O volume onde está o arquivo de configuração. Exemplo: HOST, I:                           */
	HF_CHAR          configFileVolume[ RF_PREVIOUS_CFG_MAX_LENGTH ];

	/* O nome do arquivo de configuração. Exemplo: RDC01.SH, VRXPOS01                            */
	HF_CHAR          configFileName[ RF_PREVIOUS_CFG_MAX_LENGTH ];

	/* Posições, no arquivo de configuração, dos campos que serão recuperados                    */
	HF_INT32         fieldPosition[ RF_PREVIOUS_CFG_FIELDS_COUNT ];

	/* Tamanhos, no arquivo de configuração, dos campos que serão recuperados                    */
	HF_INT32         fieldSize[ RF_PREVIOUS_CFG_FIELDS_COUNT ];

	/* Os nomes dos volumes com arquivos que devem ser excluídos. Exemplo: REDECARD, SCRIPTS     */
	/* AVISO: As strings devem ser alocadas e apagadas por quem usar este struct                 */
	HF_CHAR          *volumes[ RF_PREVIOUS_CFG_VOLUMES_COUNT ];

} RF_PREVIOUS_CFG_T;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_BOOL HF_AMS_Main(HF_VOID);

#endif
