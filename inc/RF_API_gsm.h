/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : RF_API_gsm.h
Descrição: Cabeçalho que exporta as funções de communicação.
Autor    : Elvio
Data     : 06/06/2014
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _RF_GSM_H_
#define _RF_GSM_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                   Includes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                   Macros/Defines                                              */
/*-----------------------------------------------------------------------------------------------*/

#define DEFAULT_APN "REDECARD.BR"

#define RF_MAX_NETWORK_NAME    20
#define RF_MAX_ICCIDLEN        20

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

// Codigos de erros
typedef enum _RF_GSM_STAGE_A_ERROR { // Etapa A
	RF_GSM_STAGE_A0_ERROR_MODEM_FAILURE = 0,
	RF_GSM_STAGE_A1_ERROR_SIMCARD_DISABLE = 1,
	RF_GSM_STAGE_A2_ERROR_SIMCARD_BLOCKED = 2,
	RF_GSM_STAGE_A3_ERROR_SIMCARD_PIN_FAILURE = 3,
	RF_GSM_STAGE_A4_ERROR_ATTACHMENT_FAILURE = 4,
	RF_GSM_STAGE_A5_ERROR_NO_SIGNAL = 5,
	RF_GSM_STAGE_A6_ERROR_SIMCARD_ABSENT = 6,
	RF_GSM_STAGE_A7_ERROR_SIMCARD_DAMAGED = 7
} RF_GSM_STAGE_A_ERROR;

typedef enum _RF_GSM_STAGE_G1_ERROR { // Etapa G1
	RF_GSM_STAGE_G11_ERROR_INVALID_APN = 1,
	RF_GSM_STAGE_G12_ERROR_GGSN_CONTEXT_FAILURE = 2,
	RF_GSM_STAGE_G13_ERROR_GGSN_IP_FAILURE = 3,
	RF_GSM_STAGE_G14_ERROR_PPP_FAILURE = 4
} RF_GSM_STAGE_G1_ERROR;

typedef enum _RF_GSM_STAGE_C1_ERROR { // Etapa C1
	RF_GSM_STAGE_C11_ERROR_NO_CARRIER = 1,
	RF_GSM_STAGE_C12_ERROR_NO_DIAL_TONE = 2,
	RF_GSM_STAGE_C13_ERROR_NO_ANSWER = 3,
	RF_GSM_STAGE_C14_ERROR_LINE_BUSY = 4
} RF_GSM_STAGE_C1_ERROR;

typedef enum _RF_GSM_DATA_NETWORK {
    RF_GSM_NONE = 0,
	RF_GSM_CSD  = 1, // Circuit Switched Data
    RF_GSM_GPRS = 2, // General Packet Radio Service
    RF_GSM_EDGE = 3  // Enhanced Data Rates for GSM Evolution (E-GPRS)
} RF_GSM_DATA_NETWORK;

typedef enum {
	RF_GSM_ETAPA_A  = 1,
	RF_GSM_ETAPA_G1 = 2,
	RF_GSM_ETAPA_G2 = 3,
	RF_GSM_ETAPA_G3 = 4,
	RF_GSM_ETAPA_G4 = 5,
	RF_GSM_ETAPA_C1 = 6,
	RF_GSM_ETAPA_C2 = 7
} RF_GSM_ETAPA;

typedef enum {
	RF_GSM_ETAPA_ESTADO_INICIO = 1,
	RF_GSM_ETAPA_ESTADO_EXECUTANDO = 2,
	RF_GSM_ETAPA_ESTADO_FIM = 3,
	RF_GSM_ETAPA_ESTADO_ERRO = 4
} RF_GSM_ETAPA_ESTADO;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef struct _RF_GSM_CONFIG_MODEM_STARTUP {
	RF_UINT8 start_modem_try_num;
	RF_UINT32 wait_modem_mseg;
} RF_GSM_CONFIG_MODEM_STARTUP;

typedef struct _RF_GSM_CONFIG_SIGNAL {
	RF_UINT8 ber_maximo;
	RF_UINT8 rssi_minimo;
	RF_UINT32 timeout_signal_wait_mseg;
} RF_GSM_CONFIG_SIGNAL;

typedef struct _RF_CSD_CONFIG {
	RF_GSM_CONFIG_MODEM_STARTUP modem_startup;
	RF_GSM_CONFIG_SIGNAL signal_wait;
} RF_GSM_CSD_CONFIG;

typedef struct _RF_GPRS_CONFIG_PDP_CTX {
	RF_UINT8 pdp_ctx_try_num;
	RF_UINT32 wait_pdp_try_mseg;
	RF_UINT32 timeout_pdp_try_mseg;
} RF_GPRS_CONFIG_PDP_CTX;

typedef struct _RF_GRPS_CONFIG {
	RF_CHAR* apn;
	RF_CHAR* login;
	RF_CHAR* password;
	RF_UINT32 reconnect_wait_mseg;
	RF_GSM_CONFIG_MODEM_STARTUP modem_startup;
	RF_GSM_CONFIG_SIGNAL signal_wait;
	RF_GPRS_CONFIG_PDP_CTX pdp_ctx;
} RF_GSM_GPRS_CONFIG;

typedef struct _RF_GSM_INFO {
	RF_BOOL has_sim_card;
	RF_GSM_DATA_NETWORK gsm_data_network;
	RF_INT32 ber; // Bit Error Rate
	RF_INT32 rssi; // Received signal strength indication
	RF_INT32 radio_level;
	RF_CHAR network_name[RF_MAX_NETWORK_NAME + 1];
	RF_CHAR sim_icc_id[RF_MAX_ICCIDLEN + 1];
} RF_GSM_INFO;

typedef struct _RF_GSM_STATUS_INFO {
	RF_GSM_ETAPA etapa;
	RF_GSM_ETAPA_ESTADO estadoEtapa;

	/* C�digo de erro (da tabela de erros) */
	RF_INT16 codigo_erro;

	/* C�digo de erro CME (radio) */
	RF_INT16 cme_erro;

	/* C�digos de erro e status nativos do dispositivo */
	RF_INT32 codigo_erro_nativo;
	RF_INT32 status_nativo;
} RF_GSM_STATUS_INFO;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

// GSM
RF_INT32 RF_gsm_getInfo(RF_GSM_INFO* gsmInfo);
RF_INT32 RF_gsm_status(RF_GSM_STATUS_INFO* info);

// GSM GPRS
RF_INT32 RF_gprs_config(RF_GSM_GPRS_CONFIG* config);

// GSM CSD
RF_INT32 RF_csd_config(RF_GSM_CSD_CONFIG* config);
RF_INT32 RF_csd_connect(const RF_CHAR* phonenumber, RF_INT32 timeout, RF_INT32 maxIterations);
RF_INT32 RF_csd_disconnect(RF_VOID);
RF_INT32 RF_csd_send(const RF_UINT8* msg, RF_UINT16 length);
RF_INT32 RF_csd_recv(RF_UINT8* data, RF_UINT16 maxlen, RF_UINT32 timeout);

#if defined(DO_NOT_REDEFINE_GSM_CSD_FUNCTIONS) || defined(DISABLE_GCM_CSD_PROXY)

// GSM GPRS
RF_INT32 RF_gprs_up(RF_VOID);
RF_INT32 RF_gprs_down(RF_VOID);

// GSM CSD
RF_INT32 RF_csd_up(RF_VOID);
RF_INT32 RF_csd_down(RF_VOID);

#else

// GSM GPRS
#define RF_gprs_up HFI_gprs_up
#define RF_gprs_down HFI_gprs_down
RF_INT32 HFI_gprs_up(RF_VOID);
RF_INT32 HFI_gprs_down(RF_VOID);


//GSM CSD
#define RF_csd_up HFI_csd_up
#define RF_csd_down HFI_csd_down
RF_INT32 HFI_csd_up(RF_VOID);
RF_INT32 HFI_csd_down(RF_VOID);

#endif

#endif
