/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_comms.h
Descrição: Cabeçalho que exporta as funções do communicação.
Autor    : Jobson
Data     : 27/07/2007
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_COMMS_H_
#define _HF_COMMS_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

// chaves
#define HF_COMM_BRATE            "baud_rate"
#define HF_COMM_PARITY           "parity"
#define HF_COMM_DATASZ           "data_size"
#define HF_COMM_STOPBITS         "stop_bits"
#define HF_COMM_READBUFFSZ       "rbuffsz"
#define HF_COMM_SENDBUFFSZ       "sbuffsz"

#define HF_COMM_DEFPABX          "def_pabx"
#define HF_COMM_DEFNUM           "def_number"
#define HF_COMM_DIALMODE         "dial_mode"
#define HF_COMM_WAITDIALTONE     "wait_dial_tone"  // valor HF_COMM_USED ou HF_COMM_NOT_USED

#define HF_COMM_INITSTR          "init_string"
#define HF_COMM_CFGSTR           "config_string"
#define HF_COMM_PREDIALFASTSTR   "pre_dial_fast_string"
#define HF_COMM_PREDIALSTDSTR    "pre_dial_std_string"
#define HF_COMM_DIALSTR          "dial_string"

#define HF_COMM_FLOWCONTROL      "flow_control"
#define HF_COMM_INTERCHARTIMEOUT "inter_char_timeout"
#define HF_COMM_XONCHAR          "x_on_char"
#define HF_COMM_XOFFCHAR         "x_off_char"
                                
#define HF_COMM_MODPRTCOL        "modulation_protocol"
#define HF_COMM_DATALINKPRTCOL   "data_link_protocol"
#define HF_COMM_CMPSPRTCOL       "compression_protocol"
#define HF_COMM_ERRCORRCTPRTCOL  "err_correction_protocol"

#define HF_COMM_MINRECSPEED  "min_rec_speed"
#define HF_COMM_MAXRECSPEED  "max_rec_speed"
#define HF_COMM_MINSENDSPEED "min_send_speed"
#define HF_COMM_MAXSENDSPEED "max_send_speed"

// valores
#define HF_COMM_LINKP_NONE        "none"
#define HF_COMM_LINKP_HDLC_V80    "hdlc_v80"
#define HF_COMM_LINKP_SDLC        "sdlc"
#define HF_COMM_LINKP_APACS40     "apacs40"
#define HF_COMM_LINKP_SPDH_BASE24 "spdh_base24"

#define HF_COMM_MODEMP_V21     "v21"
#define HF_COMM_MODEMP_V22     "v22"
#define HF_COMM_MODEMP_V22B    "v22bis"
#define HF_COMM_MODEMP_V23     "v23"
#define HF_COMM_MODEMP_V32     "v32"
#define HF_COMM_MODEMP_V32B    "v32b"
#define HF_COMM_MODEMP_V34     "v34"
#define HF_COMM_MODEMP_V90     "v90"
#define HF_COMM_MODEMP_V92D    "v90d"
#define HF_COMM_MODEMP_V92U    "v92u"
#define HF_COMM_MODEMP_K56     "k56"
#define HF_COMM_MODEMP_BELL103 "bell103"
#define HF_COMM_MODEMP_BELL212 "bell212"
#define HF_COMM_MODEMP_V29     "v29"

#define HF_COMM_ERRCORCTN_NONE     "none"
#define HF_COMM_ERRCORCTN_LAPM     "lapm"
#define HF_COMM_ERRCORCTN_MNP      "mnp"
#define HF_COMM_ERRCORCTN_LAPM_MNP "lapm_mnp"

#define HF_COMM_DATACMPS_NONE      "none"
#define HF_COMM_DATACMPS_V42B      "v42b"
#define HF_COMM_DATACMPS_MNP5      "mnp5"
#define HF_COMM_DATACMPS_V42B_MNP5 "v42b_mnp5"

#define HF_COMM_DIALMODE_PULSE "pulse"
#define HF_COMM_DIALMODE_TONE  "tone"

#define HF_COMM_USED           "used"
#define HF_COMM_NOT_USED       "not_used"

#define HF_COMM_DEFAULT_PORT   "16002"

#define HF_COMM_PARITY_NONE    "none"
#define HF_COMM_PARITY_ODD     "odd"
#define HF_COMM_PARITY_EVEN    "even"

#define HF_COMM_FLOWCTRL_NONE     "none"
#define HF_COMM_FLOWCTRL_XON_XOFF "xon_xoff"

#define HF_COMM_DATASZ5B "5"
#define HF_COMM_DATASZ6B "6"
#define HF_COMM_DATASZ7B "7"
#define HF_COMM_DATASZ8B "8"

#define HF_COMM_STOPBITS1B "1"
#define HF_COMM_STOPBITS2B "2"

#define HF_COMM_BRATE_50     "50"
#define HF_COMM_BRATE_75     "75"
#define HF_COMM_BRATE_150    "150"
#define HF_COMM_BRATE_300    "300"
#define HF_COMM_BRATE_600    "600"
#define HF_COMM_BRATE_1200   "1200"   
#define HF_COMM_BRATE_2400   "2400"
#define HF_COMM_BRATE_4800   "4800"
#define HF_COMM_BRATE_9600   "9600"
#define HF_COMM_BRATE_19200  "19200"
#define HF_COMM_BRATE_38400  "38400"
#define HF_COMM_BRATE_57600  "57600"
#define HF_COMM_BRATE_76800  "76800"
#define HF_COMM_BRATE_115200 "115200"

#define HF_COMM_SPEED_300   "300"
#define HF_COMM_SPEED_1200  "1200"
#define HF_COMM_SPEED_2400  "2400"
#define HF_COMM_SPEED_4800  "4800"
#define HF_COMM_SPEED_7200  "7200"
#define HF_COMM_SPEED_9600  "9600"
#define HF_COMM_SPEED_12000 "12000"
#define HF_COMM_SPEED_14400 "14400"
#define HF_COMM_SPEED_16800 "16800"
#define HF_COMM_SPEED_19200 "19200"
#define HF_COMM_SPEED_21600 "21600"
#define HF_COMM_SPEED_24000 "24000"
#define HF_COMM_SPEED_25333 "25333"
#define HF_COMM_SPEED_26400 "26400"
#define HF_COMM_SPEED_26667 "26667"
#define HF_COMM_SPEED_28000 "28000"
#define HF_COMM_SPEED_28800 "28800"
#define HF_COMM_SPEED_29333 "29333"
#define HF_COMM_SPEED_30667 "30667"
#define HF_COMM_SPEED_31200 "31200"
#define HF_COMM_SPEED_32000 "32000"
#define HF_COMM_SPEED_33333 "33333"
#define HF_COMM_SPEED_33600 "33600"
#define HF_COMM_SPEED_34000 "34000"
#define HF_COMM_SPEED_34667 "34667"
#define HF_COMM_SPEED_36000 "36000"
#define HF_COMM_SPEED_37333 "37333"
#define HF_COMM_SPEED_38000 "38000"
#define HF_COMM_SPEED_38400 "38400"
#define HF_COMM_SPEED_38667 "38667"
#define HF_COMM_SPEED_40000 "40000"
#define HF_COMM_SPEED_41333 "41333"
#define HF_COMM_SPEED_42000 "42000"
#define HF_COMM_SPEED_42667 "42667"
#define HF_COMM_SPEED_44000 "44000"
#define HF_COMM_SPEED_45333 "45333"
#define HF_COMM_SPEED_46000 "46000"
#define HF_COMM_SPEED_46667 "46667"
#define HF_COMM_SPEED_48000 "48000"
#define HF_COMM_SPEED_49333 "49333"
#define HF_COMM_SPEED_50000 "50000"
#define HF_COMM_SPEED_50667 "50667"
#define HF_COMM_SPEED_52000 "52000"
#define HF_COMM_SPEED_53333 "53333"
#define HF_COMM_SPEED_54000 "54000"
#define HF_COMM_SPEED_54667 "54667"
#define HF_COMM_SPEED_56000 "56000"

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/* Needs to be fixed!! */
typedef struct {
	HF_INT16 status;
	HF_UINT16 length;
	HF_CHAR *data;
} HF_ONERESULT_T;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_INT32 HF_comm_recvmsg(HF_UINT8 *data, HF_UINT16 maxlen, HF_UINT32 timeout);
HF_INT32 HF_comm_sendmsg(const HF_UINT8 *msg, HF_UINT16 length);
HF_INT32 HF_comm_checkConnect(HF_VOID);
HF_INT32 HF_comm_disconnect(HF_VOID);
HF_INT32 HF_comm_connect(const HF_CHAR *phonenumber, const HF_CHAR *pabx);

HF_INT32 HF_comm_getMaxDialNumbers(HF_UINT8 *phonenumber, HF_UINT8 *pabx);

HF_INT32 HF_comm_getBuffSz(HF_INT32 *sendBuff, HF_INT32 *recBuff);

HF_INT32 HF_comm_configDefault(HF_VOID);
HF_INT32 HF_comm_configLoad(const HF_CHAR *filename);
HF_INT32 HF_comm_setProperty(const HF_CHAR *key, const HF_CHAR *value);
HF_INT32 HF_comm_getProperty(const HF_CHAR *pKey, HF_CHAR *pBuffer, HF_UINT32 pBuffSz);
HF_INT32 HF_comm_configSave(const HF_CHAR *filename);

#endif

