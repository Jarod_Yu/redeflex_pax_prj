/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_types.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Data:
Data     : 27/05/2015
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Daniel Moura
Data     : 27/05/2015
Empresa  : C.E.S.A.R
Descrição: Criação
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef __HF_TYPES_H__
#define __HF_TYPES_H__

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

#ifdef __TELIUM2__
#include <SDK30.h>
#elif _VRXEVO
#include <eoslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#else
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#include <math.h>
#include <ctype.h>

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_API_DEPRECATED_DO_NOT_IMPLEMENT

#ifndef HUGE_VAL
#define HUGE_VAL      1e500
#endif

#ifndef HF_MIN
#define HF_MIN(x,y)  ( ((x) < (y)) ? (x) : (y) )
#endif

#ifndef HF_MAX
#define HF_MAX(x,y)  ( ((x) > (y)) ? (x) : (y) )
#endif

#define HF_MAX_INT8  0x7F
#define HF_MAX_INT16 0x7FFF
#define HF_MAX_INT32 0x7FFFFFFF

#define HF_MIN_INT8  0x80
#define HF_MIN_INT16 0x8000
#define HF_MIN_INT32 0x80000000

#define HF_MAX_UINT8  0xFF
#define HF_MAX_UINT16 0xFFFF
#define HF_MAX_UINT32 0xFFFFFFFF


/*------------- string.h -------------*/

#define  HF_strcpy           strcpy
#define  HF_strncpy          strncpy
#define  HF_strcat           strcat
#define  HF_strcmp           strcmp
#define  HF_strncmp          strncmp
#define  HF_strchr           strchr
#define  HF_strrchr          strrchr
#define  HF_strlen           strlen
#define  HF_strlen           strlen

#define  HF_memcpy           memcpy
#define  HF_memmove          memmove
#define  HF_memcmp           memcmp
#define  HF_memchr           memchr
#define  HF_memset           memset
#define  HF_strstr           strstr
#define  HF_strpbrk          strpbrk



/*------------- stdio.h -------------*/

#define  HF_sprintf          sprintf

/*------------- stdlib.h -------------*/

#define  HF_abs              abs
#define  HF_atof             atof
#define  HF_atoint32         atoi
#define  HF_atoint64         atol
#define  HF_div              div
#define  HF_labs             labs
#define  HF_ldiv             ldiv

#define  HF_strtol           strtol
#define  HF_strtoul          strtoul
#define  HF_atoi             atoi
#define	 HF_toupper	         toupper
#define	 HF_tolower	         tolower

#define  HF_isascii(c)  (((c) & ~0x7F) == 0)

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*
 * Definições para Telium
 */
typedef unsigned char         HF_UCHAR;
typedef unsigned char         HF_UINT8;
typedef unsigned short        HF_UINT16;
#define HF_UINT32             unsigned int
typedef unsigned long long    HF_UINT64;

#define HF_CHAR               char
typedef char                  HF_INT8;
typedef short                 HF_INT16;
#define HF_INT32              int
typedef long long             HF_INT64;

#define HF_VOID               void
typedef double                HF_DOUBLE;

typedef struct {
   HF_CHAR *msg1;
   HF_CHAR *msg2;
   HF_UINT8 digits;
} callbackData_t;

typedef char                  HF_BOOL;
#define HF_TRUE               1
#define HF_FALSE              0
#define HF_NULL               0

typedef HF_UINT32             HF_SIZE_T;
typedef HF_UINT32             HF_PTRDIFF_T;
typedef int                   HF_NATIVE_INT;
typedef unsigned int          HF_NATIVE_UINT;
typedef long                  HF_NATIVE_LONG;
typedef unsigned long         HF_NATIVE_ULONG;

#define RF_UCHAR              HF_UCHAR
#define RF_UINT8              HF_UINT8
#define RF_UINT16             HF_UINT16
#define RF_UINT32             HF_UINT32
#define RF_UINT64             HF_UINT64
#define RF_CHAR               HF_CHAR
#define RF_INT8               HF_INT8
#define RF_INT16              HF_INT16
#define RF_INT32              HF_INT32
#define RF_INT64              HF_INT64
#define RF_VOID               HF_VOID
#define RF_DOUBLE             HF_DOUBLE
#define RF_SIZE_T             HF_SIZE_T
#define RF_PTRDIFF_T          HF_PTRDIFF_T
#define RF_NATIVE_INT         HF_NATIVE_INT
#define RF_NATIVE_UINT        HF_NATIVE_UINT
#define RF_NATIVE_LONG        HF_NATIVE_LONG
#define RF_NATIVE_ULONG       HF_NATIVE_ULONG
#define RF_BOOL               HF_BOOL
#define RF_TRUE               HF_TRUE
#define RF_FALSE              HF_FALSE
#define RF_NULL               HF_NULL

#define RF_DIR_SEPARATOR "/"

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/


/*------------- math.h -------------*/

#define  HF_modf             modf
#define  HF_qsort            qsort

#define  HF_fmod             fmod
#define  HF_pow              pow
#define  HF_sqrt             sqrt
#define  HF_fabs             fabs
#define  HF_ceil             ceil
#define  HF_floor            floor
#define  HF_strtod           strtod



#ifdef HF_CUSTOM_HEADER
#include HF_CUSTOM_HEADER
#endif

#endif
