/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : RF_API_memory.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Data:
Data     : 27/03/2015
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _RF_API_MEMORY_H_
#define _RF_API_MEMORY_H_

/***************************************************************************************************************
 * Description: 	  This function will be called by redeflex to allocate, desalocar and re allocate memory.
 * 					  The expected behavior must be equal to the realloc the C standard.
 * Input Parameter:	  size - Number of bytes to allocate (ptr == null), to relocate (ptr != null and size > 0).
 * 					  If NTP != null and size == 0, the memory pointed by ptr must be deallocated.
 * Output Parameter:  ptr - The case is different from null (zero), the pointer to the area to be deallocated (size = = 0) or reallocated (size! = 0).
 *				      If set to null (zero) indicates that it is to be allocated size bytes.
 * Return:			  NULL (zero) - Pointer to the new memory block allocated (which may be the same as above, in the case of re allocation).
 * 					  Pointer to the new memory block allocated (which may be the same as above, in the case of re allocation).
 ***************************************************************************************************************/
RF_VOID* RF_memory_realloc(RF_VOID* ptr,RF_SIZE_T size);


//Maximo que pode ser alocado pela aplicacao (EM KB)
/***************************************************************************************************************
 * Description: 	  This function returns the maximum amount of memory that can be allocated by the redeflex (using RF_memory_realloc).
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  The amount of kilobytes (bytes / 1024) that can be allocated by the redeflex.
 ***************************************************************************************************************/
RF_SIZE_T RF_memory_max_available_KB(RF_VOID);


#endif

