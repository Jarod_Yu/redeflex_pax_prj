/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_log.h
Descrição: Cabeçalho que exporta as funções de log
Autor    : Guilherme
Data     : 18/08/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_LOG_H_
#define _HF_LOG_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/***** Severities ******/
#define HF_LOG_FATAL    0
#define HF_LOG_ALERT    1
#define HF_LOG_CRITICAL    2
#define HF_LOG_WARNING    3
#define HF_LOG_NOTICE    4
#define HF_LOG_INFO        5
#define HF_LOG_DEBUG    6
/***********************/
#define HF_MAX_SEVERITY 7


/***** Sub-systems *****/
#define HF_LOG_LOADER    0
#define HF_LOG_API        1
#define HF_LOG_AMS        2
#define HF_LOG_LUA        3
#define HF_LOG_GERAL    4
/***********************/
#define HF_SUBSYSTEM_MAX 5


#define HFI_LOGCFG_FILEPATH "/cfg/log.cfg"
#define HFI_LOGDATA_FILEPATH "/log/log.txt"
#define HFI_LOGDATA_DIRPATH "/log"

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef struct _HF_LOG_RECORDS_
{
    HF_UINT32 subsystem;
    HF_UINT16 severity;
    HF_CHAR *description;
} HF_LOG_Records;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/
/* Exported via the API */
HF_VOID HF_LOG_Record(HF_LOG_Records *record);
HF_VOID HF_LOG_Stream_Open(HF_VOID);
HF_VOID HF_LOG_Stream_Close(HF_VOID);
HF_VOID HF_LOG_Elements(HF_LOG_Records *record);

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

#endif

