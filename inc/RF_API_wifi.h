/*-----------------------------------------------------------------------------------------------*/
/* Nome do arquivo:                 RF_API_wifi.h                                                */
/* Tres Letras Representativas:     API                                                          */
/* Descricao:                       Cabe�alho que exporta as fun��es de WIFI.                    */
/*-----------------------------------------------------------------------------------------------*/
/*                            Historico                                                          */
/*-----------------------------------------------------------------------------------------------*/
/*  Nome                 Login         CR        Data         Descricao                          */
/*-----------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*/

#ifndef _RF_API_WIFI_H_
#define _RF_API_WIFI_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                   Includes                                                    */
/*-----------------------------------------------------------------------------------------------*/

typedef enum {
	// BSS de Infraestrutura (access point)
	RF_WIFI_SCAN_TIPO_BSS_INFRA = 1,

	//BSS independente (ad-hoc)
	RF_WIFI_SCAN_TIPO_BSS_INDEPENDENT,

	//Desconhecido
	RF_WIFI_SCAN_TIPO_BSS_UNKNOWN,
} RF_WIFI_SCAN_ITEM_TYPE;

typedef enum {
	//Open System authentication algorithm
	RF_WIFI_AUTH_80211_OPEN = 1,

	//802.11 Shared Key authentication algorithm
	RF_WIFI_AUTH_80211_PSK,

	//Wi-Fi Protected Access (WPA) algorithm
	RF_WIFI_AUTH_WPA,

	//WPA algorithm that uses preshared keys (PSK)
	RF_WIFI_AUTH_WPA_PSK,

	RF_WIFI_AUTH_WPA_NONE,

	// 802.11i Robust Security Network Association (RSNA) algorithm. WPA2 is one such algorithm
	RF_WIFI_AUTH_RSNA,

	// 802.11i RSNA algorithm that uses PSK
	RF_WIFI_AUTH_RSNA_PSK,
} RF_WIFI_AUTH;

typedef enum {
	//Nenhuma criptografia
	RF_WIFI_CIPHER_NONE = 1,

	// WEP com chave de 40 bit
	RF_WIFI_CIPHER_WEP40,

	// Temporal Key Integrity Protocol (TKIP)
	RF_WIFI_CIPHER_TKIP,

	//AES-CCMP
	RF_WIFI_CIPHER_CCMP,

	// WEP com chave de 104 bit
	RF_WIFI_CIPHER_WEP104,

	// Wi-Fi Protected Access (WPA) Use Group Key
	RF_WIFI_CIPHER_WPA_USE_GROUP,

	// Robust Security Network (RSN) Use Group Key
	RF_WIFI_CIPHER_RSN_USE_GROUP,

	//Wep com chave de qualquer tamanho
	RF_WIFI_CIPHER_WEP,
} RF_WIFI_CIPHER;

typedef enum {
	//Interface wifi desativada
	RF_WIFI_STATUS_DOWN = 1,
	//Interface ativa, mas sem conexao
	RF_WIFI_STATUS_DISCONNECTED,
	//Associando ao Access Point
	RF_WIFI_STATUS_ASSOCIATING,
	//Associado ao Access Point
	RF_WIFI_STATUS_ASSOCIATED,
	//Authenticando no AP
	RF_WIFI_STATUS_AUTHENTICATING,
	//Obtendo endereco IP
	RF_WIFI_STATUS_OBTAINING_IPADDR,
	//Conectado na rede wifi
	RF_WIFI_STATUS_CONNECTED,
	//Error na conexao
	RF_WIFI_STATUS_ERROR
} RF_WIFI_STATUS;

typedef enum {
	//Desconhecido
	RF_WIFI_ERROR_UNKNOWN = 1,
	//Rede WI-FI nao encontrada
	RF_WIFI_ERROR_NETWORK_NOT_FOUND,
	//Erro durante associacao ao AP
	RF_WIFI_ERROR_ASSOCIATION,
	//Erro na authenticacao
	RF_WIFI_ERROR_AUTHENTICATION,
	//Erro na authenticacao - senha incorreta
	RF_WIFI_ERROR_AUTHENTICATION_PASSWORD,
	//Erro no processo de dhcp
	RF_WIFI_ERROR_DHCP,
	//Erro - conflito de IP
	RF_WIFI_ERROR_NETWORK_IP_CONFICT,
	//Erro - gateway nao disponivel
	RF_WIFI_ERROR_NETWORK_IP_GATEWAY,
	//Outro erro de rede
	RF_WIFI_ERROR_NETWORK,
} RF_WIFI_ERROR;

typedef struct RF_WIFI_NETWORK_INFO {
	// SSID
	RF_UCHAR ssid[34];
	RF_UINT8 tam_ssid;

	//MAC do Access Point
	RF_UCHAR bssid[6];

	//Frequencia em KHz
	RF_UINT32 frequency;

	//Qualidade do sinal - 0 a 100
	RF_UINT8 quality;

	//Nivel de sinal em dBm(RSSI)
	RF_INT32 rssi;

	//Taxa de transferencia m�xima da rede (em bps)
	RF_UINT32 maxTransfRate;

	//Tipo de conex�o WI-FI
	RF_WIFI_SCAN_ITEM_TYPE type;

	//Algoritmo de autenticacao
	RF_WIFI_AUTH authAlgo;

	//Algoritmo de criptografia
	RF_WIFI_CIPHER cryptAlgo;
} RF_WIFI_NETWORK_INFO;

typedef struct RF_WIFI_SCAN_RESULTS {
// Quantidade de RF_WIFI_NETWORK_INFO encontrados
	RF_UINT16 count;

// Ponteiro para array de RF_WIFI_NETWORK_INFO, cujo tamanho � count
	RF_WIFI_NETWORK_INFO* array;
} RF_WIFI_SCAN_RESULTS;

typedef struct RF_WIFI_STATUS_INFO {
	// Status da conexao
	RF_WIFI_STATUS status;
	//Codigo nativo do status
	RF_INT32 nativeStatus;
	//Indica o tipo do erro
	RF_WIFI_ERROR errorType;
	//Codigo nativo do erro
	RF_INT32 nativeError;
	//Dados da rede conectada/em conexao
	RF_WIFI_NETWORK_INFO network;
	//Dados da configuracao da rede IP da rede conectada
	RF_IP_NETWORK_INFO ipInfo;
	//Endereco MAC no formato IEEE 802) ex. 01-23-45-67-89-AB
	RF_CHAR macAddress[17 + 1];
} RF_WIFI_STATUS_INFO;

typedef struct RF_WIFI_CONFIG_INFO {
	//N�mero m�ximo de tentativas de estabelecer a conex�o W-FI.
	RF_UINT16 connect_max_retry;
	//Tempo que o dispositivo deve aguardar antes de tentar re-estabelecer a conex�o WI-FI.
	RF_UINT32 reconnect_wait_mseg;
} RF_WIFI_CONFIG_INFO;

// ################## functions ##########################
/**
 * Fun��o que ser� chamada pela aplica��o para definir os par�metros para conex�o na rede WI-FI.
 * @param RF_WIFI_CONFIG_INFO *config - Ponteiro para a estrutura RF_WIFI_CONFIG_INFO, na qual s�o informados:
 * 		  connect_max_retry - N�mero m�ximo de tentativas de estabelecer a conex�o WI-FI.
 * 						      Se zero, n�o h� limite. Isto �, fica em loop at� ser desativado.
 * 		  reconnect_wait_mseg - Tempo que o dispositivo deve aguardar antes de
 * 								tentar re-estabelecer a conex�o WI-FI.
 * @return RF_SUCCESS � se a fun��o executou com sucesso.
 *         RF_ERR_INVALIDARG � Ponteiro nulo
 *         RF_DEVICE_FAULT � Erro na execu��o
 */
RF_INT32 RF_WIFI_config(RF_WIFI_CONFIG_INFO *config);

/**
 * Ativa a interface WI-FI do dispositivo.
 * @return RF_SUCCESS      � Interface ativada.
 *         RF_DEVICE_FAULT � Erro ativando interface
 */
RF_INT32 RF_WIFI_up(RF_VOID);

/**
 * Destiva a interface WI-FI do dispositivo.
 * @return RF_SUCCESS � Interface desativada.
 *         RF_DEVICE_FAULT � Erro desativando interface.
 */
RF_INT32 RF_WIFI_down(RF_VOID);


/**
 * Obtem a lista de redes WI-FI dispon�veis.
 *
 * A fun��o deve retornar quando expirar o timeout ou atingir o m�ximo de resultados poss�veis.
 *
 * @param p_results - Ponteiro para a estrutura que armazena a lista de redes WI-FI dispon�veis.
 * @param maxResults - Quantidade m�xima de redes WI-FI dispon�veis a ser retornada.
 * @param timeout_ms  - Timeout em milissegundos.
 * @return RF_SUCCESS � Lista de redes WI-FI obtida com sucesso. A lista pode estar vazia,
 * isto � n�o foram encontradas redes WI-FI.
 *         RF_DEVICE_FAULT � Erro.
 *
 *
 */
RF_INT32 RF_WIFI_scan(RF_WIFI_SCAN_RESULTS* p_results, RF_UINT16 maxResults,
		RF_UINT32 timeout_ms);

/**
 * Inicia o processo de conex�o em uma rede WI-FI.
 *
 * A fun��o dever� retornar imediatamente, isto � n�o dever� aguardar a completa
 * conex�o na rede WI-FI.
 *
 * A conex�o na rede WI-FI dever� ser realizada em background por uma thread/task.
 * Esta thread tamb�m ser� respons�vel por detectar quedas e restabelecer automaticamente a conex�o.
 *
 * A Thread dever� respeitar os par�metros definidos na RF_WIFI_config, zerando o contador de tentativas
 * de reconex�o cada vez que a conex�o for estabelecida.
 *
 * Durante o estabelecimento da conex�o com a rede WI-FI o status obtido atrav�s da
 * fun��o RF_WIFI_status deve ser atualizado constantemente.
 *
 * @param network - dados da conex�o Wi-fi que o dispositivo dever� conectar.
 *                  A conexao s� poder� ser realizada caso os seguintes parametros
 *                  sejam identicos a uma rede acess�vel: ssid, tam_ssid, algoritmoAuth, algoritmoCripto
 * @param p_ascii_password - String terminada em NULL('\0') contendo a senha de conex�o da rede WI-FI.
 *                           Caso a rede em quest�o n�o precise de senha o par�metro ser� ignorado.
 *                           A implementa��o dever� copiar o valor da string para suas estruturas
 *                           internas. Ap�s o retorno da fun��o n�o h� garantia que este ponteiro continuar� integro.
 *
 * @return RF_SUCCESS � Iniciado processo de conex�o na rede WI-FI.
 * 		   RF_ERR_INVALIDSTATE � Fun��o RF_WIFI_config n�o foi chamada anteriormente.
 *         RF_DEVICE_FAULT � Iniciando processo de conex�o na rede WI-FI.
 */
RF_INT32 RF_WIFI_start_connect(RF_WIFI_NETWORK_INFO network,
		RF_IP_NETWORK_INFO ipInfo, const RF_CHAR* p_ascii_password);

/**
 * Obtem o status da conex�o WI-FI do dispositivo.
 *
 * @param p_item Ponteiro para a estrutura que armazena o status.
 *
 * @return RF_SUCCESS � Status obtido com sucesso.
 *         RF_DEVICE_FAULT � Erro obtendo status
 */
RF_INT32 RF_WIFI_status(RF_WIFI_STATUS_INFO *p_item);

/**
 * Indica para o dispositivo que a conex�o WI-FI deve ser desconectada.
 * A fun��o dever� retornar imediatamente, isto � n�o dever� aguardar a
 * completa desconex�o da rede WI-FI (a desconex�o da rede WI-FI dever� ser
 * realizada em background).
 * Durante a desconex�o o status obtido atrav�s da fun��o RF_WIFI_status
 * deve ser atualizado constantemente.
 *
 * @return RF_SUCCESS � Solicita��o processada com sucesso
 *         RF_DEVICE_FAULT � Erro processando solicita��o
 */
RF_INT32 RF_WIFI_request_disconnect(RF_VOID);

#endif
