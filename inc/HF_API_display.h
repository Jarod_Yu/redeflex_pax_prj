/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_display.h
Descrição: CabeÃ§alho que exporta as funÃ§Ãµes de tela do API
Autor    : Guilherme
Data     : 14/09/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_DISPLAY_H_
#define _HF_DISPLAY_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_DISPLAY_LEFTALIGNED      1
#define HF_DISPLAY_CENTERALIGNED    2
#define HF_DISPLAY_RIGHTALIGNED     4

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

typedef enum {
    HF_FONT_SMALL = 0,
    HF_FONT_NORMAL = 1,
    HF_FONT_LARGE = 2,
    HF_FONT_LARGE_BOLD = 3
} HF_FONT_T;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/
typedef HF_VOID * HF_SCREEN_HANDLE_DEPRECATED;



/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Functions                                                  */
/*-----------------------------------------------------------------------------------------------*/
// T_GL_HWIDGET HF_display_getWindow(HF_VOID);
// T_GL_HGRAPHIC_LIB HF_display_getGraphLib(HF_VOID);

HF_INT32 HF_display_print(HF_UINT32 row, HF_UINT32 column, const HF_CHAR *text);
HF_INT32 HF_display_printAligned(HF_UINT32 row, HF_INT32 alignment, const HF_CHAR *text);
HF_INT32 HF_display_printimage(const HF_IMAGE *image, HF_UINT32 x, HF_UINT32 y);
HF_INT32 HF_display_clearline(HF_UINT32 line);
HF_INT32 HF_display_clear(HF_VOID);
HF_INT32 HF_display_clear_screen(HF_VOID);
HF_INT32 HF_display_geometry(HF_UINT32 *lines, HF_UINT32 *columns);
HF_INT32 HF_display_pixels(HF_UINT32 *width, HF_UINT32 *height);
HF_INT32 HF_display_setBacklight(HF_UINT8 percent);
HF_INT32 HF_display_drawLine(HF_UINT32 x0, HF_UINT32 y0, HF_UINT32 x1, HF_UINT32 y1);
HF_INT32 HF_display_drawPoint(HF_UINT32 x, HF_UINT32 y);
HF_INT32 HF_display_drawRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height);
HF_INT32 HF_display_getCursorPosition(HF_UINT32 *column, HF_UINT32 *line);
HF_INT32 HF_display_setFont(HF_FONT_T);
HF_INT32 HF_display_getFont(HF_FONT_T *);
HF_INT32 HF_display_getFontDimension(HF_FONT_T, HF_UINT32 *width, HF_UINT32 *height);
HF_VOID  HF_display_setAutoFlush(HF_BOOL value);
HF_BOOL  HF_display_getAutoFlush(HF_VOID);
HF_VOID  HF_display_setInvertColors(HF_BOOL value);
HF_BOOL  HF_display_getInvertColors(HF_VOID);
HF_VOID  HF_display_flush(HF_VOID);
HF_INT32 HF_display_setColor(HF_INT64 text, HF_INT64 background);
HF_INT64 HF_display_getColor(HF_VOID);
HF_INT64 HF_display_getBackgroundColor(HF_VOID);
HF_INT32 HF_display_fillRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height);
HF_BOOL  HF_display_setHeader(HF_BOOL value);
HF_BOOL  HF_display_setFooter(HF_BOOL value);
HF_INT32 HF_display_setCustomHeader(HF_BOOL onOff, const HF_IMAGE *image);
HF_INT32 HF_display_setCustomFooter(HF_BOOL onOff, const HF_IMAGE *image);

// T_GL_SCALE should not be there...
//T_GL_SCALE HFI_getFontSize(HF_FONT_T hf_font);

#endif
