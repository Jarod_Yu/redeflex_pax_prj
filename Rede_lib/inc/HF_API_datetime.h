/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_datetime.h
Descrição: Cabeçalho que exporta as funções de data e hora.
Autor    : Itapaje
Data     : 20/04/2006
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_DATETIME_H_
#define _HF_DATETIME_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef struct {
	HF_UINT32 year;
	HF_UINT32 month;
	HF_UINT32 day;
	HF_UINT32 hour;
	HF_UINT32 minute;
	HF_UINT32 second;
	HF_UINT32 weekday;
	HF_UINT32 yearday;
} HF_TM_T;

enum HF_DAY
{
    HF_sunday = 1,
    HF_monday,
    HF_tuesday,
    HF_wednesday,
    HF_thursday,
    HF_friday,
    HF_saturday
};

enum HF_MONTH
{
    HF_january = 1,
    HF_february,
    HF_march,
    HF_april,
    HF_may,
    HF_june,
    HF_july,
    HF_august,
    HF_september,
    HF_october,
    HF_november,
    HF_december
};



/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*
*************************************************************
Author and Publisher:	Gabriel
Date:					2015/5/22
Name:					HF_get_datetime
Description:			Returns the current date and time of the system as a pointer to a structure RF_TM_T, allocated in static memory. This pointer must not be freed with free () function or similar !
Parameters:
Returns:				HM_TM_T pointer to a structure or RF_NULL on failure. The error code, if any, may be recovered in RF_errno variable. The values may be:
						RF_ERR_DEVICEFAULT - if there was a fault in the device
Modified: 	PAX Application Development Department Kevin Liu   2015/5/22 written by Kevin
*************************************************************
*/
HF_TM_T* HF_get_datetime(HF_VOID);
/*
*************************************************************
Author and Publisher:	Gabriel
Date:					2015/5/22
Name:					HF_get_time
Description:			Returns the current time in terms of seconds elapsed since 01/01/1970.
Parameters:
Returns:				The number of seconds in case of success. In case of failure:
 	 	 	 	 	 	RF_ERR_DEVICEFAULT - if there was a fault in the device
Modified: 	PAX Application Development Department Kevin Liu   2015/5/22 written by Kevin
*************************************************************
*/
HF_INT32 HF_get_time(HF_VOID);
HF_TM_T* HF_localtime (HF_INT32 time);
HF_INT32 HF_mktime (const HF_TM_T *pTime);

/*
*************************************************************
Author and Publisher:	Gabriel
Date:					2015/5/22
Name:					HF_set_datetime
Description:			Change the date and time of the system.
Parameters:				SYSTEMTIME - The new time system
Returns:				RF_SUCCESS - if the function executed successfully
						RF_ERR_INVALIDDATE - if the date is invalid (ex .: prior to 1970)
						RF_ERR_INVALIDTIME - if the time is invalid
						RF_ERR_RESOURCEALLOC - if the shared resource can not meet the request
Modified: 	PAX Application Development Department Kevin Liu   2015/5/22 written by Kevin
*************************************************************
*/
HF_INT32 HF_set_datetime(const HF_TM_T *systemtime);

HF_UINT32 HF_calculate_weekday(HF_UINT32 day, HF_UINT32 month, HF_UINT32 year);
HF_UINT32 HF_calculate_yearday(HF_UINT32 day, HF_UINT32 month, HF_UINT32 year);

HF_INT32 HF_validatetime (const HF_TM_T *pTime);

#endif

