/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_image.h
Descrição: Cabeçalho que exporta as funções de imagem
Autor    : Adriano
Data     : 26/09/2013
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_IMAGE_H_
#define _HF_IMAGE_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_IMAGE_MAX_WIDTH 512
#define HF_IMAGE_MAX_HEIGHT 1982

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

typedef HF_VOID HF_IMAGE;

typedef struct HF_EDITABLE_IMAGE_{
    HF_UINT32         width;
    HF_UINT32         height;
    HF_INT8           *data;
} HF_EDITABLE_IMAGE;

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

HF_INT32 HF_image_loadimageH(const HF_CHAR *filename, HF_IMAGE **image);
HF_INT32 HF_image_disposeimage(HF_IMAGE *image);
HF_INT32 HF_image_getimagedimension(const HF_IMAGE *hfImage, HF_UINT32 *width, HF_UINT32 *height);

HF_INT32 HF_image_fromEditable(HF_EDITABLE_IMAGE *inputimage, HF_IMAGE **outputimage);
HF_INT32 HF_image_toEditable(HF_IMAGE *inputimage, HF_EDITABLE_IMAGE **outputimage);

/*
HF_INT32 HF_image_loadgimageH(const HF_CHAR *filename, HF_IMAGE **image);
HF_INT32 HF_image_disposegimage(HF_IMAGE *hfImage);
*/

#endif

