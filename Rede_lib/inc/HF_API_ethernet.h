/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_ethernet.h
Descrição: Arquivo fonte da plataforma Redeflex
Autor    : Renato
Data     : 27/03/2015
Empresa  : Rede S.A.
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_API_ETHERNET_H_
#define _HF_API_ETHERNET_H_

/*
 * Uso e configuracao de redes TCP/IP
 */

/*
 * Chaves
 */
#define HF_NET_INTERFACE        "interface"     // Ethernet ou wifi
#define HF_NET_IP               "ip"            // IP local
#define HF_NET_DHCP             "dhcp"          // Usa ou nao DHCP
#define HF_NET_MASK             "net_mask"      // Mascara de rede
#define HF_NET_GATEWAY          "gateway"       // ex:192.168.0.254
#define HF_NET_DNS1             "dns1"          // DNS primario
#define HF_NET_DNS2             "dns2"          // DNS secundario
#define HF_NET_WIFI_SSID        "wifi_ssid"     // nome da rede WIFI
#define HF_NET_WIFI_AUTH        "wifi_auth"     // tipo de autorizacao
#define HF_NET_WIFI_CRIPT       "wifi_cript"    // tipo de criptografia
#define HF_NET_WIFI_PASS        "wifi_pass"     // senha para acesso WIFI

/*
 * Valores
 */
#define HF_NET_TYPE_ETH             "ethernet"
#define HF_NET_TYPE_WIFI            "wifi"
#define HF_NET_DHCP_USED            "used"
#define HF_NET_DHCP_NOT_USED        "not_used"
#define HF_NET_WIFI_AUTH_OPEN       "open"
#define HF_NET_WIFI_AUTH_SHARED     "shared"
#define HF_NET_WIFI_AUTH_WPAPSK     "wpa-psk"
#define HF_NET_WIFI_AUTH_WPA2PSK    "wpa2-psk"
#define HF_NET_WIFI_CRIPT_DISABLED  "disabled"
#define HF_NET_WIFI_CRIPT_WEP       "wep"
#define HF_NET_WIFI_CRIPT_AES       "aes"
#define HF_NET_WIFI_CRIPT_TKIP      "tkip"

/*
 * Inicia uma conexao TCP/IP
 */
HF_INT32 HF_network_connect(const HF_CHAR *pIp, const HF_CHAR *pPort, HF_BOOL  useSsl);

/*
 * Desconecta TCP/IP
 */
HF_INT32 HF_network_disconnect(HF_VOID);

/*
 * Retorna o estado de uma conexao TCP/IP
 */
HF_INT32 HF_network_status(HF_VOID);

/*
 * Envia dados
 */
HF_INT32 HF_network_sendmsg(const HF_UINT8 *msg, HF_UINT16 length);

/*
 * Aguarda dados
 */
HF_INT32 HF_network_recvmsg(HF_UINT8 *data, HF_UINT16 maxlen, HF_UINT32 timeout);

/*
 * Inicializa (conecta) rede
 */
HF_INT32 HF_network_init(void);

/*
 * Desconecta rede
 */
HF_INT32 HFI_network_shutdown (HF_VOID);

/*
 * Le configuracoes de um arquivo
 */
HF_INT32 HF_network_configLoad(const HF_CHAR *filename);

/*
 * Modifica o valor de uma propriedade
 */
HF_INT32 HF_network_setProperty(const HF_CHAR *key, const HF_CHAR *value);

/*
 * Retorna o valor de uma propriedade
 */
HF_INT32 HF_network_getProperty(const HF_CHAR *key, HF_CHAR *buffer, HF_INT32 buffSz);

HF_INT32 HF_network_isAvailable(HF_VOID);

// --------
// nova api
// --------

typedef struct {
	RF_UINT32 ipAddress; // Endereco IP na rede, formatado em big-endian
	RF_UINT32 mask;
	RF_UINT32 gateway;
	RF_UINT32 dns1;
	RF_UINT32 dns2;

	RF_BOOL isDhcp;
	RF_INT32 leaseDuration; // em segundos
	RF_UINT32 dhcpServerAddress;
} RF_IP_NETWORK_INFO;

typedef struct {
	RF_UINT16 connect_max_retry;
	RF_UINT32 reconnect_wait_mseg;
	RF_IP_NETWORK_INFO ipInfo;
} RF_ETHERNET_CONFIG_INFO;

typedef enum {
	// Interface nao foi ativada
	RF_ETHERNET_STATUS_DOWN = 1,

	// Cabo de rede desconectado
	RF_ETHERNET_STATUS_DISCONNECTED,

	// Iniciou o processo de conexao
	RF_ETHERNET_STATUS_CONNECTING,

	// Obtendo IP da rede
	RF_ETHERNET_STATUS_OBTAINING_IPADDR,

	// Conectado na rede ip
	RF_ETHERNET_STATUS_CONNECTED,

	// Erro na conexao
	RF_ETHERNET_STATUS_ERROR
} RF_ETHERNET_STATUS;

typedef enum {
	// Desconhecido
	RF_ETHERNET_ERROR_UNKNOWN = 1,

	// Cabo desconectado
	RF_ETHERNET_ERROR_DISCONNECTED,

	// Erro no processo de dhcp
	RF_ETHERNET_ERROR_DHCP,

	// Erro - conflito de IP
	RF_ETHERNET_ERROR_NETWORK_IP_CONFICT,

	// Erro - gateway nao disponivel
	RF_ETHERNET_ERROR_NETWORK_IP_GATEWAY,

	// Outro erro de rede
	RF_ETHERNET_ERROR_NETWORK
} RF_ETHERNET_ERROR;

typedef struct {
	// Status da conexao
	RF_ETHERNET_STATUS status;

	// Codigo nativo do status
	RF_INT32 nativeStatus;

	// Indica o tipo do erro
	RF_ETHERNET_ERROR errorType;

	// Codigo nativo do erro
	RF_INT32 nativeError;

	// Dados da configuracao da rede IP da rede conectada
	RF_IP_NETWORK_INFO ipInfo;

	// Endereco MAC no formato IEEE 802 ex. 01-23-45-67-89-AB (string terminando em NULL)
	RF_CHAR macAddress[17 + 1];
} RF_ETHERNET_STATUS_INFO;


RF_INT32 RF_ETHERNET_config(RF_ETHERNET_CONFIG_INFO*);
// RF_SUCCESS  se a função executou com sucesso.
// RF_ERR_INVALIDARG  ponteiro nulo.
// RF_DEVICE_FAULT  erro na execução.

RF_INT32 RF_ETHERNET_up(RF_VOID);
// RF_SUCCESS  Interface ativada: processo de conexão na rede iniciado.
// RF_ERR_INVALIDSTATE  Função RF_ETHERNET_config não foi chamada anteriormente.
// RF_DEVICE_FAULT  Erro ativando interface.

RF_INT32 RF_ETHERNET_down(RF_VOID);
// RF_SUCCESS  Iniciado o processo de desativação da interface ETHERNET.
// RF_DEVICE_FAULT  Erro iniciando o processo de desativação da interface ETHERNET.

RF_INT32 RF_ETHERNET_status(RF_ETHERNET_STATUS_INFO*);
// RF_SUCCESS  Status obtido com sucesso.
// RF_DEVICE_FAULT  Erro obtendo status.

#endif
