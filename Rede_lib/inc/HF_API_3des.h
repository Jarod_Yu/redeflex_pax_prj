/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_3des.h
Descrição: Cabeçalho que exporta as funções de 3DES.
Autor    : Guilherme
Data     : 17/04/2007
Empresa  : C.E.S.A.R
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_3DES_H_
#define _HF_3DES_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/***************************************************************************************************************
 * Description: Executes encryption 3DES (ECB) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter:		key - Pointer to the 3DES key in memory
 *								  		data - Pointer to the data to be encrypted
 *								  		dataSz - The length of data to be encrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the encrypted data are to be stored.
 *												  The size of this area of memory should be equal to or greater than the length of the data to be encrypted
 * Return:						RF_SUCCESS - If the function successfully executed
 * 									// TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_3des_encryptBlock(const HF_INT8 *key, const HF_INT8 *data,
        HF_UINT32 dataSz, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Executes decryption 3DES (ECB) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter:		key - Pointer to the 3DES key in memory
 *								  		data - Pointer to the data to be decrypted
 *								  		dataSz - The length of data to be decrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the decrypted data are to be stored.
 *												  The size of this area of memory should be equal to or greater than the length of the data to be decrypted
 * Return:						RF_SUCCESS - If the function successfully executed
 * 									// TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_3des_decryptBlock(const HF_INT8 *key, const HF_INT8 *data,
        HF_UINT32 dataSz, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Executes encryption 3DES (ECB, using a masterkey) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter:		keyID - The index of the masterkey to be used
 * 									workingKey - Pointer to the 3DES key encrypted with the masterkey inkex keyID
 *								  		data - Pointer to the data to be encrypted
 *								  		dataSz - The length of data to be encrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the encrypted data are to be stored.
 *												  The size of this area of memory should be equal to or greater than the length of the data to be encrypted
 * Return:						RF_SUCCESS - If the function successfully executed
 * 									// TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_mk3des_encryptBlock(HF_UINT16 keyID, const HF_INT8 *workingKey,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Executes encryption 3DES (CBC) in a data block of size datasz.
 *                       The function must receive the pointer to the block output (result) previously allocated.
 *                       This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter: key - Pointer to the 3DES key in memory
 *                                iv - Initialization vector of the CBC mode
 *	                              data - Pointer to the data to be encrypted
 *	                              dataSz - The length of data to be encrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the encrypted data are to be stored.
 *                                               The size of this area of memory should be equal to or greater than the length of the data to be encrypted
 * Return:                  RF_SUCCESS - If the function successfully executed
 *                                // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_3des_CBC_encryptBlock(const HF_INT8 *key, const HF_INT8 *iv,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Executes decryption 3DES (CBC) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter: key - Pointer to the 3DES key in memory
 *                                iv - Initialization vector of the CBC mode
 *	                               data - Pointer to the data to be decrypted
 *                                dataSz - The length of data to be decrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the decrypted data are to be stored.
 *	                                              The size of this area of memory should be equal to or greater than the length of the data to be decrypted
 * Return:                  RF_SUCCESS - If the function successfully executed
 * 							   // TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_3des_CBC_decryptBlock(const HF_INT8 *key, const HF_INT8 *iv,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result);

/***************************************************************************************************************
 * Description: Executes encryption 3DES (CBC, using a masterkey) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter:		keyID - The index of the masterkey to be used
 * 									workingKey - Pointer to the 3DES key encrypted with the masterkey inkex keyed
 * 									iv - Initialization vector of the CBC mode
 *								  		data - Pointer to the data to be encrypted
 *								  		dataSz - The length of data to be encrypted (Multiple of 8)
 *	Output Parameter: result - Pointer to the memory area where the encrypted data are to be stored.
 *												  The size of this area of memory should be equal to or greater than the length of the data to be encrypted
 * Return:						RF_SUCCESS - If the function successfully executed
 * 									// TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
HF_INT32 HF_mk3des_CBC_encryptBlock(HF_UINT16 keyID, const HF_INT8 *workingKey,
        const HF_INT8 *iv, const HF_INT8 *data, HF_UINT32 dataSz,
        HF_INT8 *result);


/***************************************************************************************************************
 * Description: Executes encryption 3DES (ECB, using a masterkey) in a data block of size datasz.
 * 					  The function must receive the pointer to the block output (result) previously allocated.
 * 					  This function does not add padding to the result, so the size of the data block must be a multiple of 8.
 * Input Parameter:		keyID - The index of the TIK to be used
 * 						inputData - Pointer to the data to be encrypted
 *						inputDataSize - the size of the encrypted data
 *						outputData - Pointer to the memory area where the encrypted data are to be stored.
 *	Output Parameter: outputData - Pointer to the memory area where the encrypted data are to be stored.
 *					  outputKsn - Pointer to the memory are where the KSN are to be stored.
 * Return:						RF_SUCCESS - If the function successfully executed
 * 									// TODO: More error code need to be defined by REDE
 ***************************************************************************************************************/
RF_INT32 RF_mk3des_ECB_encryptBlockDUKPT(RF_UINT16 keyID, const
		RF_UINT8 *inputData, RF_UINT32 inputDataSize, RF_UINT8 *outputData,
		RF_UINT8 *outputKsn);


#ifdef HF_3DES_USE_FIXED_MK
HF_INT32 HF_fixed_mk3des_encryptBlock(HF_UINT16 keyID, const HF_INT8 *workingKey, const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result);
HF_VOID HFI_debug_3des_key (HF_CHAR *name, HF_INT8 *key);
#endif

#endif
