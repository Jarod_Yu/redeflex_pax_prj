/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_fs.h
Descri莽茫o: Cabecalho que exporta as funcoes do systema dos arquivos.
Autor    : Guilherme
Data     : 10/04/2007
Empresa  : C.E.S.A.R
*********************** MODIFICA脟脮ES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descri莽茫o: Inspe莽茫o de c贸digo
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_FS_H_
#define _HF_FS_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

#define HF_FILE_SEEK_SET        0
#define HF_FILE_SEEK_CUR        1
#define HF_FILE_SEEK_END        2
#define HF_FS_MAX_DIR_DEPTH     (6 + 1) // no m锟絰imo 5 diret锟絩ios e o '1' 锟� para o arquivo
#define HF_FS_MAX_FILE_NAME     15
#define HF_FS_MAX_PATH_LENGTH   ((HF_FS_MAX_FILE_NAME + 1) * HF_FS_MAX_DIR_DEPTH) * 2
#define HF_FS_MAXREADBUFFSZ     2147483647
#define HF_FS_MAXWRITEBUFFSZ    2147483647

// deve ir para outro lugar
typedef HF_VOID * HF_HANDLE_T;
typedef HF_VOID * HF_FILE_T;

#define HF_INVALID_HANDLE -1
/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

#if defined(DO_NOT_REDEFINE_FILESYSTEM_FUNCTIONS) || defined(DISABLE_FILESYSTEM_PROXY)

HF_INT32   HF_fs_fopenH(const HF_CHAR *filename, const HF_CHAR *accessmode, HF_FILE_T **file);
HF_INT32 HF_fs_fclose(HF_FILE_T *fp);
HF_INT32 HF_fs_fread(const HF_FILE_T *fp, HF_INT8 *buffer, HF_UINT32 length);
HF_INT32 HF_fs_fseek(const HF_FILE_T *fp, HF_INT32 offset, HF_UINT16 position);
HF_BOOL  HF_fs_exists(const HF_CHAR *path);
HF_INT32 HF_fs_getfsize(const HF_CHAR *filename, HF_UINT32 *fileSz);

#else

#define HF_fs_fopenH HFI_fs_fopenH
#define HF_fs_fclose HFI_fs_fclose
#define HF_fs_fread HFI_fs_fread
#define HF_fs_fseek HFI_fs_fseek
#define HF_fs_exists HFI_fs_exists
#define HF_fs_getfsize HFI_fs_getfsize


// Fab@2015/09/12 ADD START
HF_INT32 HFI_fs_start(HF_VOID);
HF_INT32 HFI_fs_shutdown(HF_VOID);
// Fab@2015/09/12 ADD END

/***************************************************************************************************************
 * Description: 	  Opens a file. After the use of the file, it is imperative that the handle returned
 * 					  by this function is closed through a function call RF _ FS _ Fclose.
 * Input Parameter:	  filename - Name (absolute or relative) of the file to open
 *					  accessmode - String indicating the type of access
 * Output Parameter:  file - A pointer of type RF_file_t * if success
 * Return:			  One of the following:
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_ISOPEN - If the file is already open
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_HDRCORRUPT - If the file header is corrupted
 * 					  RF_ERR_NOSPACE - If there is no more room to open a file for writing
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_INVALMODE - Accessmode invalid
 * 					  RF_ERR_NOMEMORY - If there is no more free memory in the system.
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32   HFI_fs_fopenH(const HF_CHAR *filename, const HF_CHAR *accessmode, HF_FILE_T **file);

/***************************************************************************************************************
 * Description: 	  Close a file opened by RF_FS_fopen().
 * Input Parameter:	  fp - The descriptor file (obtained by RF_FS_fopen)
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HFI_fs_fclose(HF_FILE_T *fp);

/***************************************************************************************************************
 * Description: 	  Makes the reading of data from a file open. The maximum amount of bytes that can be read
 * 					  at one time is specified by the macro RF_FS_maxreadbuffsz.
 * Input Parameter:	  fp - The descriptor of the file
 *					  length - Number of bytes to be read
 * Output Parameter:  buffer - Pointer to the space where to store the data
 * Return:			  The number of bytes read is successful, in the event of a failure, one of the following:
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 * 					  RF_ERR_EOF - If there are no more data in the file
 ***************************************************************************************************************/
HF_INT32 HFI_fs_fread(const HF_FILE_T *fp, HF_INT8 *buffer, HF_UINT32 length);
// Jarod@2015/09/14 UPD START
//HF_INT32 HF_fs_readline(const HF_FILE_T *fd, HF_CHAR *buffer, HF_UINT32 maxlen);
HF_INT32 HFI_fs_readline(const HF_FILE_T *fd, HF_CHAR *buffer, HF_UINT32 maxlen);
// Jarod@2015/09/14 UPD END

/***************************************************************************************************************
 * Description: 	  Position the cursor reading / writing of the file in a determined position.
 * Input Parameter:	  fp - The descriptor of the file
 *					  offset - Number of bytes to position the cursor reading / writing (a negative value makes the cursor to move backwards)
 *					  position - Relative position from where it will be made positioning
 * Output Parameter:  NULL
 * Return:			  The position of the cursor after the amendment relating to the beginning of the file. In case of failure:
 * 					  RF_ERR_OUTOFBOUNDS - The positioning of the cursor is before the start of the file or after the end
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HFI_fs_fseek(const HF_FILE_T *fp, HF_INT32 offset, HF_UINT16 position);

/***************************************************************************************************************
 * Description: 	  Check if the last path represents a file or directory.
 * Input Parameter:	  path - The path to the file or directory.
 * Output Parameter:  NULL
 * Return:			  RF_TRUE - If it is a valid file or directory
 * 					  RF_FALSE - Otherwise (if there is no such file or directory, or if there is a mistake in the way)
 ***************************************************************************************************************/
HF_BOOL HFI_fs_exists(const HF_CHAR *path);

/***************************************************************************************************************
 * Description: 	  Returns the size of the file past a filename.
 * Input Parameter:	  filename - The path of the file
 * Output Parameter:  fileSz - The parameter of return which informs the file size
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_ISADIR - If the path points to a directory.
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_HDRCORRUPT - If the file header is corrupted
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_NOMEMORY - If there is no more free memory in the system.
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HFI_fs_getfsize(const HF_CHAR *filename, HF_UINT32 *fileSz);

#endif

HF_BOOL RF_fs_existsexternal(const HF_CHAR *path, const HF_CHAR *volume);
HF_INT32 RF_fs_chdirexternal(const HF_CHAR *dirname, const HF_CHAR *volume);
HF_INT32 RF_fs_opendirexternal(const HF_CHAR *dirname, HF_HANDLE_T *hDir, const HF_CHAR *volume);
HF_INT32 RF_fs_readdirexternal(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz, const HF_CHAR *volume);
HF_BOOL RF_fs_isdirexternal(const HF_CHAR *path, const HF_CHAR *volume);
HF_INT32 RF_fs_unlinkexternal(const HF_CHAR *filename, const HF_CHAR *volume);
HF_INT32 RF_fs_fopenexternalH(const HF_CHAR *filename, const HF_CHAR *accessMode, const HF_CHAR *volume, HF_FILE_T **output);


/***************************************************************************************************************
 * Description: 	  Writes data in a file open. The maximum amount of bytes that can be written on a given
 * 					  platform is defined by RF_FS_maxwritebuffsz.
 * Input Parameter:	  fp - The descriptor of the file
 * 					  buffer - The data to be written to
 *					  length - Number of bytes to be write
 * Output Parameter:  NULL
 * Return:			  The number of bytes are written if success. In case of failure:
 *  				  RF_ERR_READONLY - If the file open for reading only
 * 					  RF_ERR_NOSPACE - If there is no more room to open a file for writing
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 * 					  RF_ERR_BUFFOVERFLOW - If the amount of data to be written is larger than the allowed for platform.
 ***************************************************************************************************************/
HF_INT32 HF_fs_fwrite(const HF_FILE_T *fp, const HF_INT8 *buffer, HF_UINT32 length);

/***************************************************************************************************************
 * Description: 	  Delete a file or directory. If filename to specify a directory, then the directory needs to be empty to be removed.
 * Input Parameter:	  filename - The name of the file or directory.
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_NOACCESSRIGHT - If you do not have permission to remove
 * 					  RF_ERR_HDRCORRUPT - If the file header is corrupted
 * 					  RF_ERR_NOTEMPTY - se o diret贸rio a ser exclu铆do n茫o est谩 vazio
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_unlink(const HF_CHAR *filename);

/***************************************************************************************************************
 * Description: 	  Changes the current directory (CWD).
 * Input Parameter:	  dirname - The name of the directory.
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_PATHTOODEEP - If the depth of the path is greater than the allowed
 * 					  RF_ERR_ISAFILE - If the path points to a file
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_chdir(const HF_CHAR *dirname);

/***************************************************************************************************************
 * Description: 	  Returns the current directory.
 * Input Parameter:	  NULL
 * Output Parameter:  dirname - The name of the directory is stored in the current absolute pointer
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_getcwd(HF_CHAR *dirname);

/***************************************************************************************************************
 * Description: 	  Create a new directory.
 * Input Parameter:	  dirname - The name of the directory to be created
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_PATHTOODEEP - If the depth of the path is greater than the allowed
 * 					  RF_ERR_ALREADYEXISTS - If the directory already exists,
 * 					  RF_ERR_NOSPACE - If there is no more room to create the directory
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_mkdir(const HF_CHAR *dirname);

/***************************************************************************************************************
 * Description: 	  Renames a file or directory.
 * Input Parameter:	  oldname - The current name of the file or directory.
 * 					  newname - The new file name or directory.
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_ALREADYEXISTS - If the directory already exists,
 * 					  RF_ERR_NOACCESSRIGHT - If you do not have permission to remove
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_HDRCORRUPT - If the file header is corrupted
 * 					  RF_ERR_NOSPACE - If there is no more room to create the directory
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_rename(const HF_CHAR *oldname, const HF_CHAR *newname);

/***************************************************************************************************************
 * Description: 	  Returns the amount of available space in the file system in bytes.
 * Input Parameter:	  NULL
 * Output Parameter:  NULL
 * Return:			  free disk space if successfully excuted
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_freediskspace(HF_VOID);


/***************************************************************************************************************
 * Description: 	  Open a directory to list your files and directories. When it is no longer necessary to read the contents of a directory,
 * 					  it is imperative that the handle returned by this function is closed through a function call RF_FS_closedir.
 * Input Parameter:	  dirname - The name of the directory.
 * Output Parameter:  hDir - The parameter of return which holds the handle of the open directory
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_EMPTYDIR - If the directory is empty
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_OPENFILES - If there are many open directories.
 * 					  RF_ERR_NOMEMORY - If there is no more free memory in the system.
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_opendir(const HF_CHAR *dirname, HF_HANDLE_T *hDir);

/***************************************************************************************************************
 * Description: 	  Reads the next file or directory to a directory open. Will always be returned the file name
 * 					  on the open directory, i.e. without the way.
 * Input Parameter:	  hDir - Handle to the open directory
 * 					  buffSz - The size of the buffer
 * Output Parameter:  filefound - The output buffer that will hold the next file or directory.
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_EMPTYDIR - If the directory is empty
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_readdir(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz);

/***************************************************************************************************************
 * Description: 	  Closes an open directory by RF_FS_opendir().
 * Input Parameter:	  dirsearch - Handle to the open directory
 * Output Parameter:  NULL
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_closedir(HF_HANDLE_T dirsearch);


/***************************************************************************************************************
 * Description: 	  Check if the path past is a directory.
 * Input Parameter:	  path - The path to the file or directory.
 * Output Parameter:  NULL
 * Return:			  RF_TRUE - If a directory.
 * 					  RF_FALSE - Otherwise (if a file or if there is an error)
 ***************************************************************************************************************/
HF_BOOL HF_fs_isdir(const HF_CHAR *path);
HF_BOOL HF_fs_isglobal(const HF_CHAR *filename);


/***************************************************************************************************************
 * Description: 	  Returns when it was the last modification of the file or directory in the seconds passed from 1 January 1970.
 * Input Parameter:	  filename - The path of the file or directory.
 * Output Parameter:  mTime - The parameter of return which informs the time of last modification
 * Return:			  RF_SUCCESS - If the function successfully executed
 * 					  RF_ERR_NOTFOUND - If the path of the file larger than the allowed
 * 					  RF_ERR_MAXLEN - If the maximum number of open files achieved
 * 					  RF_ERR_HDRCORRUPT - If the file header is corrupted
 * 					  RF_ERR_PATHERR - If the file name contains an invalid character sequence.
 * 					  RF_ERR_NOMEMORY - If there is no more free memory in the system.
 * 					  RF_ERR_DEVICEFAULT - If there is any fault in the device.
 ***************************************************************************************************************/
HF_INT32 HF_fs_getmtime(const HF_CHAR *filename, HF_INT32 *mTime);

#endif

