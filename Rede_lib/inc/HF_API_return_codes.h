/*
Copyright 2015 Rede S.A.
*************************************************************
Nome     : HF_API_return_codes.h
Descrição: Mapeamento das constantes de erro e sucesso da API
		   HiperFlex.
Autor    : Renato
Data     : 27/01/2006
Empresa  : 
*********************** MODIFICAÇÕES ************************
Autor    : Pedro Zanetti
Data     : 24/04/2015
Empresa  : C.E.S.A.R
Descrição: Inspeção de código
ID       : 01 Projeto Redeflex

Autor    : Anderson Neves
Data     : 20/05/2015
Empresa  : C.E.S.A.R
Descrição: Adição do define RF_ERR_NOTFOUND
ID       : 01 Projeto Redeflex

Autor    : Anderson Neves
Data     : 27/05/2015
Empresa  : C.E.S.A.R
Descrição: Adição do define HF_ERR_BASEBT_ASSOCIATION_FAILURE,
           adição do define HF_ERR_BASEBT_DISASSOCIATION_FAILURE e
           adição do define HF_ERR_BASEBT_DISASSOCIATION_NOT_FOUND
ID       : 01 Projeto Redeflex
*************************************************************
*/

#ifndef _HF_RETURN_CODES_H_
#define _HF_RETURN_CODES_H_

/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/


/* General codes */
/* Range 0 to -1000 */
#define HF_SUCCESS               0
#define HF_ERR_INVALIDARG       -1
#define HF_ERR_NOMEMORY         -2
#define HF_ERR_DEVICEFAULT      -3
#define HF_ERR_RESOURCEALLOC    -4
#define HF_ERR_TIMEOUT          -5
#define HF_ERR_SMALLBUFF        -6
#define HF_ERR_INVALIDSTATE     -7
#define HF_ERR_INVALIDPAD       -8
#define HF_ERR_NO_PIN           -9
#define HF_ERR_AUTOCARGA        -10
#define HF_ERR_NATIVENOTCREATED -11
#define HF_ERR_INVALIDPARAM 	-12

// Marcelo - Retirar
#define HF_ERR_INVALIDPRTY      -12
#define HF_ERR_PRTYNOTSUP       -13

#define HF_ERR_DEPRECATED       -14

/* Base 64 codes */
/* Range -1001 to -2000 */
#define	HF_ERR_INVALIDCHAR	    -1001

/* Datetime codes */
/* Range -2001 to -3000 */
#define	HF_ERR_INVALIDDATE	    -2001
#define	HF_ERR_INVALIDTIME	    -2002

/* File System codes */
/* Range -3001 to -4000 */
#define HF_ERR_NOTFOUND          -3001
#define HF_ERR_MAXLEN            -3002
#define HF_ERR_ISOPEN            -3003
#define HF_ERR_OPENFILES         -3004
#define HF_ERR_HDRCORRUPT        -3005
#define HF_ERR_NOSPACE           -3006
#define HF_ERR_PATHERR           -3007
#define HF_ERR_INVALMODE         -3008
#define HF_ERR_INVALDESC         -3009
#define HF_ERR_READONLY          -3010
#define HF_ERR_OUTOFBOUNDS       -3011
#define HF_ERR_NOACCESSRIGHT     -3012
#define HF_ERR_PATHTOODEEP       -3013
#define HF_ERR_ALREADYEXISTS     -3014
#define HF_ERR_NOTEMPTY          -3015
#define HF_ERR_ISAFILE           -3016
#define HF_ERR_EOF               -3017
#define HF_ERR_NOTGLOBAL         -3018
#define HF_ERR_BUFFOVERFLOW      -3019
#define HF_ERR_ISADIR            -3020
#define HF_ERR_FILEACCFORBID     -3021
#define HF_ERR_EMPTYDIR          -3022

/* Management codes */
/* Range -4001 to -5000 */

/* Display codes */
/* Range -5001 to -6000 */
#define HF_ERR_UNKNOWNFORMAT    -5001

/* Keyboard codes */
/* Range -6001 to -7000 */
#define	HF_ERR_KEYMISMATCH  	-6001
#define	HF_ERR_KEYNOTSUPPORTED 	-6002

/* Printer codes */
/* Range -7001 to -8000 */
#define HF_PRINTER_OK           -7001
#define HF_PRINTER_OOP          -7002

/* Magnetic codes */
/* Range -8001 to -9000 */
#define	HF_ERR_MAGREADCANCEL    -8001
#define	HF_ERR_MAG_SWIPE        -8002
#define	HF_ERR_MAG_TRACK        -8003
#define	HF_ERR_MAG_UNAVAILABL	-8004
#define	HF_ERR_MAG_DATAPARITY	-8005
#define	HF_ERR_MAG_LRC	        -8006
#define	HF_ERR_MAG_LRCPARITY	-8007
#define	HF_ERR_MAGREADNOTCANCEL -8008

/* Communication codes */
/* Range -9001 to -10000 */
#define HF_COM_DISCONNECTED     -9001
#define HF_COM_WAITING	        -9002
#define HF_COM_CONNECTED        -9003
#define HF_ERR_COMMPRTYNOTSUP	-9004
#define HF_ERR_INVALIDCOMMPRTY	-9005
#define HF_ERR_NOCARRIER        -9006
#define HF_ERR_NODIALTONE       -9007
#define HF_ERR_BUSYLINE         -9008
#define HF_ERR_NOANSWER         -9009
#define HF_ERR_CONNLOST         -9010


/* Properties codes */
/* Range -10001 to -11000 */
#define	HF_ERR_KEYNOTFOUND  	-10001

/* UI codes */
/* Range -11001 to -12000 */
#define	HF_STAT_SCREENACCEPT	  -11001
#define	HF_STAT_SCREENREJECT	  -11002
#define	HF_STAT_TEXTTRUNCATE	  -11003
#define HF_ERR_ELEMENTTPNOTSUPP   -11004
#define HF_ERR_ALIGNOTSUPP        -11005
#define HF_ERR_NOITEMACCEPTED     -11006
#define	HF_STAT_MENUCLICKED  	  -11007

/* Cryptography codes */
/* Range -12001 to -13000 */
#define HF_ERR_INVALIDRSAKEY    -12001
#define HF_ERR_INVALSIGNATURE   -12002
#define HF_ERR_NOTAPRIVATEKEY   -12003
#define HF_ERR_NOTAPUBLICKEY    -12004


/* Sys codes */
/* Range -13001 to -14000 */
#define HF_ERR_INVALSYSPRTY     -13001

/* Compression codes */
/* Range -14001 to -15000 */
#define HF_ERR_INVALIDHUFFFORMAT -14001
#define HF_ERR_INVALIDLZMA       -14002

/* ISO codes */
/* Range -15001 to -16000 */
#define HF_ERR_INVALIDBITFORMAT  -15001
#define HF_ERR_MAXBITLENGTH      -15002
#define HF_ERR_BITNOTSET         -15003
#define HF_ERR_INCOMPLETEISOMSG  -15004
#define HF_ERR_INVALIDTPDU       -15005
#define HF_ERR_INVALIDISOFORMAT  -15006

/* UTIL codes */
/* Range -16001 to -17000 */
#define HF_ERR_INVALBCDCHAR      -16001

/* Streams codes */
/* Range -17001 to -18000 */
#define HF_ERR_STREAM_CLOSED     -17001
#define HF_ERR_TIMEREXPIRED		 -17002

/* Barcode codes */
/* Range -18001 to -19000 */
#define HF_ERR_INVALIDBARCODE     -18001
#define HF_ERR_INVALIDBARCODEPRTY -18002

/* GPRS codes */
/* Range -19001 to -20000 */
#define HF_GPRS_CONNECTED 		  -19001
#define HF_GPRS_WAITING 		  -19002
#define HF_GPRS_DISCONNECTED 	  -19003
#define HF_ERR_GPRS_REGISTRATION  -19004
#define HF_ERR_GPRS_HOSTNOTFOUND  -19005
#define HF_ERR_GPRS_CONNREFUSED   -19006

#define HF_ERR_GPRS_OPER_NAO_PERM		-19007		//3
#define HF_ERR_GPRS_OPER_INVALIDA		-19008		//4
#define HF_ERR_GPRS_SIM_BLOQUEADO		-19009		//5
#define HF_ERR_GPRS_SEM_SIM_CARD		-19010		//10
#define HF_ERR_GPRS_PIN_OBRIGATORIO		-19011		//11
#define HF_ERR_GPRS_PUK_OBRIGATORIO		-19012		//12
#define HF_ERR_GPRS_FALHA_NO_SIM		-19013		//13
#define HF_ERR_GPRS_SIM_OCUPADO			-19014		//14
#define HF_ERR_GPRS_ERRO_NO_SIM			-19015		//15
#define HF_ERR_GPRS_SENHA_INCORRETA		-19016		//16
#define HF_ERR_GPRS_PIN2_OBRIGATORIO	-19017		//17
#define HF_ERR_GPRS_PUK2_OBRIGATORIO	-19018		//18
#define HF_ERR_GPRS_TIMEOUT_REDE		-19019		//31
#define HF_ERR_GPRS_GSM_INDISP			-19020		//47
#define HF_ERR_GPRS_BLOQUEADO			-19021		//107
#define HF_ERR_GPRS_LOC_BLOQUEADA		-19022		//112
#define HF_ERR_GPRS_ROAMING_INDISP		-19023		//113
#define HF_ERR_GPRS_SERVICO_INDISP		-19024		//132
#define HF_ERR_GPRS_AUTENTICANDO_PDP	-19025		//149
#define HF_ERR_GPRS_APN_INCORRETA		-19026		//533
#define HF_ERR_GPRS_ERRO_IP             -19027
#define HF_ERR_GPRS_ERRO_PPP            -19028
#define HF_LDR_ERR_GPRS_ERRO_MODEM      -19029
//TODO
#define HF_ERR_GPRS_DEV_BUSY			-19030
#define HF_ERR_GPRS_DEV_NOT_OPEN        -19031
#define HF_ERR_GPRS_NO_PORT				-19032
#define HF_ERR_NO_PORT					-19033
#define HF_ERR_WL_NEEDPIN				-19034
#define HF_ERR_GPRS_BATTERY_ABSENT 		-19035

/* Ethernet codes */
/* Range -20001 to -21000 */
#define HF_NETWORK_CONNECTED  		 -20001
#define HF_NETWORK_WAITING  		 -20002
#define HF_NETWORK_DISCONNECTED  	 -20003
#define HF_ERR_NETWORK_ADDRINUSE     -20004
#define HF_ERR_NETWORK_ENETDOWN      -20005
#define HF_ERR_NETWORK_REGISTRATION  -20006
#define HF_ERR_NETWORK_HOSTNOTFOUND  -20007
#define HF_ERR_NETWORK_CONNLOST      -20008
#define HF_ERR_NETWORK_DEVICEFAULT   -20009
#define HF_NETWORK_HANDSHAKE         -20010

/* Base bluetooth codes */
/* Range -21001 to -22000 */
#define HF_ERR_BASEBT_ASSOCIATION_FAILURE  		 -21001
#define HF_ERR_BASEBT_DISASSOCIATION_FAILURE     -21002
#define HF_ERR_BASEBT_DISASSOCIATION_NOT_FOUND   -21003

#define HF_TIMEOUT                  	-89 //timeout na entrada de pin
#define HF_NO_PIN                   	-90 //Pin nao digitado
#define HF_CANCELED                 	-91 //Botao "calcel" foi apertado

#ifdef _TELIUM_
#include <LinkLayer.h>

#define RF_STATUS_GPRS_DISCONNECTED					LL_STATUS_GPRS_DISCONNECTED
#define RF_STATUS_GPRS_ERROR_NO_SIM					LL_STATUS_GPRS_ERROR_NO_SIM
#define RF_STATUS_GPRS_ERROR_SIM_LOCK				LL_STATUS_GPRS_ERROR_SIM_LOCK
#define RF_STATUS_GPRS_ERROR_BAD_PIN				LL_STATUS_GPRS_ERROR_BAD_PIN
#define RF_STATUS_GPRS_ERROR_NO_PIN					LL_STATUS_GPRS_ERROR_NO_PIN
#define RF_STATUS_GPRS_ERROR_PPP					LL_STATUS_GPRS_ERROR_PPP
#define RF_STATUS_GPRS_ERROR_NO_SIGNAL   			LL_STATUS_GPRS_ERROR_NO_SIGNAL
#define RF_STATUS_GPRS_ERROR_UNKNOWN				LL_STATUS_GPRS_ERROR_UNKNOWN
#define RF_STATUS_GPRS_CONNECTING					LL_STATUS_GPRS_CONNECTING
#define RF_STATUS_GPRS_AVAILABLE					LL_STATUS_GPRS_AVAILABLE
#define RF_STATUS_GPRS_CONNECTING_PPP				LL_STATUS_GPRS_CONNECTING_PPP
#define RF_STATUS_GPRS_CONNECTED					LL_STATUS_GPRS_CONNECTED

/* LinkLayer */
#define RF_ERROR_OK									LL_ERROR_OK
#define RF_ERRORS									LL_ERRORS
#define RF_ERROR_NETWORK_NOT_READY					LL_ERROR_NETWORK_NOT_READY
#define RF_ERROR_NETWORK_NOT_SUPPORTED				LL_ERROR_NETWORK_NOT_SUPPORTED
#define RF_ERROR_SERVICE_CALL_FAILURE				LL_ERROR_SERVICE_CALL_FAILURE

#endif

#define RF_MAX_INT32 								HF_MAX_INT32
// Common GSM status
#define RF_STATUS_DOWN	 							10
#define RF_STATUS_UP								20
#define RF_STATUS_CONNECTING						30
#define RF_STATUS_CONNECTED							40
#define RF_STATUS_DISCONNECTED                      50


#define RF_CONFIGURED								500
#define RF_NOT_CONFIGURED							550

#define RF_TIMER_INFINITE 							HF_TIMER_INFINITE
/* General codes */
/* Range 0 to -1000 */
#define RF_SUCCESS									HF_SUCCESS
#define RF_ERR_INVALIDARG       					HF_ERR_INVALIDARG
#define RF_ERR_NOMEMORY         					HF_ERR_NOMEMORY
#define RF_ERR_DEVICEFAULT      					HF_ERR_DEVICEFAULT
#define RF_ERR_RESOURCEALLOC    					HF_ERR_RESOURCEALLOC
#define RF_ERR_TIMEOUT          					HF_ERR_TIMEOUT
#define RF_ERR_SMALLBUFF        					HF_ERR_SMALLBUFF
#define RF_ERR_INVALIDSTATE     					HF_ERR_INVALIDSTATE
#define RF_ERR_INVALIDPAD       					HF_ERR_INVALIDPAD
#define RF_ERR_NO_PIN           					HF_ERR_NO_PIN
#define RF_ERR_AUTOCARGA        					HF_ERR_AUTOCARGA
#define RF_ERR_NATIVENOTCREATED 					HF_ERR_NATIVENOTCREATED
#define RF_ERR_INVALIDPARAM 						HF_ERR_INVALIDPARAM
#define RF_ERR_DEPRECATED       					HF_ERR_DEPRECATED

/* File System codes */
/* Range -3001 to -4000 */
#define RF_ERR_NOTFOUND       						HF_ERR_NOTFOUND

/* Communication codes */
/* Range -9001 to -10000 */
#define RF_COM_CONNECTED							HF_COM_CONNECTED
#define RF_ERR_CONNLOST								HF_ERR_CONNLOST

/* Streams codes */
/* Range -17001 to -18000 */
#define RF_ERR_STREAM_CLOSED						HF_ERR_STREAM_CLOSED

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

#endif

