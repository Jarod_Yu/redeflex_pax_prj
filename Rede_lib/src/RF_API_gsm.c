#define DO_NOT_REDEFINE_GSM_CSD_FUNCTIONS

#include "HF_API.h"
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <osal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<ctype.h>

static RF_INT32 OsWlPortOpen(const RF_CHAR *Attr);
static RF_INT32 OsWlPortSend(const RF_VOID *SendBuf, RF_INT32 SendLen);
static RF_INT32 OsWlPortRecv(RF_VOID *RecvBuf, RF_INT32 RecvLen, RF_INT32 TimeoutMs);
static RF_VOID OsWlPortClose();
static RF_VOID OsWlPortReset();
static RF_VOID time_add_ms(struct timeval *tv, RF_INT32 ms);
static RF_INT32 time_compare(struct timeval tv1, struct timeval tv2);
static RF_INT32 parse_attr(const RF_CHAR *attr, RF_INT32 *speed, RF_INT32 *databits, RF_INT32 *parity, RF_INT32 *stopbits);
static RF_INT32 SetOpt(RF_INT32 fd, RF_INT32 nSpeed, RF_INT32 nBits, RF_CHAR nEvent, RF_INT32 nStop);

#define TIMER_TEMPORARY		4       // Temporary timer(Shared by different modules
#define CONFIG_INFO_MAXLENGTH			49

//static RF_GSM_GPRS_CONFIG *s_stGsmGprsConfig = NULL;
static RF_BOOL bInitGprs = 0;
static RF_INT32 fp_uart = -1;
static RF_BOOL bCallConfig = 0;
static RF_BOOL bInitStatusInfo = 0;

static RF_GSM_GPRS_CONFIG *s_stGsmGprsConfig = NULL;
static RF_GSM_STATUS_INFO *s_stGsmStatusInfo = NULL;  // Declare this

// Jarod@2015/08/31 UPD START
void showState(void);
// Jarod@2015/08/31 UPD END

static RF_GSM_INFO* s_stGsmInfo = NULL;

typedef struct _NET_WORK_INFO
{
	char code[6];
	char operator[64];
}NET_WORK_INFO;

const NET_WORK_INFO st_NetWorkInfo[] =
{
	{"72402", "TIM BRASIL"},
	{"72403", "TIM BRASIL"},
	{"72404", "TIM BRASIL"},
	{"72405", "Claro"},
	{"72416", "BrTCel"},
	{"00000", "UNKNOWN"}
};

/*
RF_INT32 RF_csd_config(RF_GSM_CSD_CONFIG* config)
{
	return 0;
}
RF_INT32 RF_csd_connect(const RF_CHAR* phonenumber, RF_INT32 timeout, RF_INT32 maxIterations)
{
	return 0;
}

RF_INT32 RF_csd_disconnect(RF_VOID)
{
	return 0;
}

RF_INT32 RF_csd_send(const RF_UINT8* msg, RF_UINT16 length)
{
	return 0;
}

RF_INT32 RF_csd_recv(RF_UINT8* data, RF_UINT16 maxlen, RF_UINT32 timeout)
{
	return 0;
}
*/

RF_INT32 RF_csd_config(RF_GSM_CSD_CONFIG* config)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

    return RF_SUCCESS;
}

RF_INT32 RF_csd_connect(const RF_CHAR* phonenumber, RF_INT32 timeout, RF_INT32 maxIterations)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

    return RF_SUCCESS;
}

RF_INT32 RF_csd_disconnect(RF_VOID)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

    return RF_SUCCESS;
}

RF_INT32 RF_csd_send(const RF_UINT8* msg, RF_UINT16 length)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

    return RF_SUCCESS;
}

RF_INT32 RF_csd_recv(RF_UINT8* data, RF_UINT16 maxlen, RF_UINT32 timeout)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

    return RF_SUCCESS;
}

RF_INT32 RF_csd_up(RF_VOID)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

	return RF_SUCCESS;
}
RF_INT32 RF_csd_down(RF_VOID)
{
    // ---------------------------------
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------

	return RF_SUCCESS;
}

RF_INT32 RF_gprs_config(RF_GSM_GPRS_CONFIG *config)
{
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"1. ENTROU 1. RF_gprs_config");

	if (NULL==config || NULL==config->apn || NULL==config->login || NULL==config->password)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"2. RF_ERR_INVALIDARG");
		return RF_ERR_INVALIDARG;
	}

	// Jarod@2015/08/30 UPD START
	if (0 == bInitStatusInfo)
	{
		return RF_ERR_INVALIDSTATE;
	}
	// Jarod@2015/08/30 UPD END

	if (strlen(config->apn)>CONFIG_INFO_MAXLENGTH || strlen(config->login)>CONFIG_INFO_MAXLENGTH ||
		strlen(config->login)>CONFIG_INFO_MAXLENGTH)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"3. RF_ERR_INVALIDARG");
		return RF_ERR_INVALIDARG;
	}

	if (NULL==s_stGsmGprsConfig || NULL==s_stGsmGprsConfig->apn ||
		NULL==s_stGsmGprsConfig->login || NULL==s_stGsmGprsConfig->password)
	{
		s_stGsmGprsConfig = malloc(sizeof(RF_GSM_GPRS_CONFIG));
		s_stGsmGprsConfig->apn = malloc(strlen(config->apn) + 1);
		s_stGsmGprsConfig->login = malloc(strlen(config->login) + 1);
		s_stGsmGprsConfig->password = malloc(strlen(config->password) + 1);
	}
	else
	{
		free(s_stGsmGprsConfig->password);
		s_stGsmGprsConfig->password = NULL;
		free(s_stGsmGprsConfig->login);
		s_stGsmGprsConfig->login = NULL;
		free(s_stGsmGprsConfig->apn);
		s_stGsmGprsConfig->apn = NULL;
		free(s_stGsmGprsConfig);
		s_stGsmGprsConfig = NULL;

		s_stGsmGprsConfig = malloc(sizeof(RF_GSM_GPRS_CONFIG));
		s_stGsmGprsConfig->apn = malloc(strlen(config->apn) + 1);
		s_stGsmGprsConfig->login = malloc(strlen(config->login) + 1);
		s_stGsmGprsConfig->password = malloc(strlen(config->password) + 1);
	}
	if (NULL== s_stGsmGprsConfig || NULL==s_stGsmGprsConfig->apn ||
		NULL==s_stGsmGprsConfig->login || NULL==s_stGsmGprsConfig->password)
	{
		return HF_ERR_NOMEMORY;
	}
	//TODO, it's meaningless about the reconnect_wait_mseg
	s_stGsmGprsConfig->reconnect_wait_mseg = config->reconnect_wait_mseg;
	strcpy(s_stGsmGprsConfig->apn, config->apn);
	strcpy(s_stGsmGprsConfig->login, config->login);
	strcpy(s_stGsmGprsConfig->password, config->password);
	memcpy(&s_stGsmGprsConfig->modem_startup, &config->modem_startup, sizeof(RF_GSM_CONFIG_MODEM_STARTUP));
	memcpy(&s_stGsmGprsConfig->pdp_ctx, &config->pdp_ctx, sizeof(RF_GPRS_CONFIG_PDP_CTX));
	memcpy(&s_stGsmGprsConfig->signal_wait, &config->signal_wait, sizeof(RF_GSM_CONFIG_SIGNAL));
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"%s--%d, apn:%s, login:%s, password:%s,\
		  reconnect_wait_mseg:%d, start_modem_try_num:%d, wait_modem_mseg:%d, ber_maximo:%d, rssi_minimo:%d,\
		  timeout_signal_wait_mseg:%d, pdp_ctx_try_num:%d, wait_pdp_try_mseg:%d, timeout_pdp_try_mseg:%d",
		  __FUNCTION__, __LINE__, s_stGsmGprsConfig->apn, s_stGsmGprsConfig->login, s_stGsmGprsConfig->password,
		  s_stGsmGprsConfig->reconnect_wait_mseg, s_stGsmGprsConfig->modem_startup.start_modem_try_num,
		  s_stGsmGprsConfig->modem_startup.wait_modem_mseg, s_stGsmGprsConfig->signal_wait.ber_maximo,
		  s_stGsmGprsConfig->signal_wait.rssi_minimo, s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg,
		  s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num, s_stGsmGprsConfig->pdp_ctx.wait_pdp_try_mseg,
		  s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg);

	//ready to delete pdp setting
//	if (s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg > 30 || s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg < 5)
//	{
//		s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg = 30;
//	}
//
//	if (s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num < 2 || s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num > 4)
//	{
//		s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num = 4;
//	}

//	if (s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg <= 0 ||s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg > 8)
	if (s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg<0 ||
		s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg>60000)
	{
		s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg = 30000;
	}

	if (s_stGsmGprsConfig->signal_wait.rssi_minimo < 10 || s_stGsmGprsConfig->signal_wait.rssi_minimo >= 31)
	{
		s_stGsmGprsConfig->signal_wait.rssi_minimo = 10;
	}

	if (s_stGsmGprsConfig->modem_startup.start_modem_try_num<0 || s_stGsmGprsConfig->modem_startup.start_modem_try_num>5 )
	{
		//3->5
		s_stGsmGprsConfig->modem_startup.start_modem_try_num = 5;
	}
	//second -> ms
	if (s_stGsmGprsConfig->modem_startup.wait_modem_mseg<0 || s_stGsmGprsConfig->modem_startup.wait_modem_mseg >5000)
	{
		//3->500
		s_stGsmGprsConfig->modem_startup.wait_modem_mseg = 500;
	}

	bCallConfig = 1;

	return RF_SUCCESS;
}

RF_INT32 RF_gprs_up(RF_VOID)
{
	RF_INT32 iRet = -1, iRetryTime = -1;

	// Jarod@2015/09/11 UPD START
	//RF_UCHAR ucRecvBuf[255] = "\0", ucSendBuf[255] = "\0";
	// Jarod@2015/09/11 UPD END

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__, __LINE__,"RF_gprs_up");

	// Jarod@2015/08/30 UPD START
	if (0 == bInitStatusInfo)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__, __LINE__,"OsWlInit, bInitStatusInfo == 0");
		return RF_ERR_INVALIDSTATE;
	}
	// Jarod@2015/08/30 UPD END


	// Jarod@2015/08/31 ADD START
	if(NULL == s_stGsmStatusInfo)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__, __LINE__,"NULL == s_stGsmStatusInfo");
		return RF_ERR_INVALIDARG;
	}
	// Jarod@2015/08/31 ADD END


	// Jarod@2015/08/31 UPD START
	//if (s_stGsmStatusInfo->etapa == RF_GSM_ETAPA_A && s_stGsmStatusInfo->estadoEtapa == RF_GSM_ETAPA_ESTADO_INICIO)
	// Jarod@2015/08/31 UPD END
	{
		iRet = OsWlLock();

		PaxLog_GSM(LOG_DEBUG,__FUNCTION__, __LINE__,"OsWlLock, iRet:%d",iRet);
		if (0 != iRet)
		{
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;

			if (ERR_DEV_NOT_EXIST == iRet)
			{
				return HF_ERR_DEVICEFAULT;
			}
			else if (ERR_DEV_BUSY == iRet)
			{
				return HF_ERR_GPRS_DEV_BUSY;
			}
			//else if (ERR_BATTERY_ABSENT == iRet)
			else if(BATTERY_LEVEL_ABSENT == iRet)
			{
				return HF_ERR_GPRS_BATTERY_ABSENT;
			}
			else
			{
				//TODO
				return iRet;
			}
		}

		// TODO, NULL is just for now
		iRet = OsWlInit(NULL);

		PaxLog_GSM(LOG_DEBUG,__FUNCTION__, __LINE__,"OsWlInit, iRet:%d",iRet);
		if (0 != iRet)
		{
			//TODO
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;
			return RF_ERR_INVALIDARG;
		}

		bInitGprs = 1;

		// Jarod@2015/08/31 UPD START
		//// Phase A.2
		s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
		s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
		// Jarod@2015/08/31 UPD END

	}


	// Jarod@2015/08/31 UPD START
	//else if (s_stGsmStatusInfo->etapa == RF_GSM_ETAPA_A && s_stGsmStatusInfo->estadoEtapa == RF_GSM_ETAPA_ESTADO_EXECUTANDO)
	// Jarod@2015/08/31 UPD END
	{
		if (0==bCallConfig)
		{
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"without GSM_config");

			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;
			return RF_ERR_INVALIDSTATE;
		}

		//TODO
		iRetryTime = s_stGsmGprsConfig->modem_startup.start_modem_try_num;
		while(iRetryTime--)
		{
			//TODO
			iRet = OsWlLogin(s_stGsmGprsConfig->apn, s_stGsmGprsConfig->login, s_stGsmGprsConfig->password, PPP_ALG_PAP, 0, 300000, 0);

			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlLogin, iRet:%d",iRet);

			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlLogin, APN:%s,PWD:%s,USER:%s,",s_stGsmGprsConfig->apn,s_stGsmGprsConfig->password,s_stGsmGprsConfig->login);

			// Jarod@2015/09/02 UPD START
			//if (0 == iRet || PPP_LOGINING == iRet)
			//{
			//	break;
			//}
			//else
			//{
				//sleep(s_stGsmGprsConfig->modem_startup.wait_modem_mseg);
			//	OsSleep(s_stGsmGprsConfig->modem_startup.wait_modem_mseg);
			//	continue;
			//}
			if (0 == iRet || PPP_LOGINING == iRet)
			{
				break;
			}
			// Jarod@2015/09/03 UPD START
			//else if (iRet < 0)
			//{
			//	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\nOsWlLogin return:[%d]\n\n",iRet);
			//	return RF_ERR_INVALIDSTATE;
			//}
			// Jarod@2015/09/03 UPD END
			else
			{
				PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\nOsWlLogin Retry times[%d]\n\n",iRetryTime);
				OsSleep(s_stGsmGprsConfig->modem_startup.wait_modem_mseg);
				continue;
			}
			// Jarod@2015/09/02 UPD END
		}

		if (0 == iRet)
		{
			//TODO
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_FIM;
			return RF_SUCCESS;
		}
		else if (PPP_LOGINING == iRet)
		{
			//TODO
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
			return RF_SUCCESS;
		}
		else if (ERR_INVALID_PARAM == iRet || ERR_NET_PASSWD == iRet)
		{
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlLogin,G1-4, iRet:%d",iRet);
			return RF_ERR_INVALIDARG;
		}
		else
		{
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;
			return RF_ERR_DEVICEFAULT;
		}
	}
}

RF_INT32 RF_gprs_down(RF_VOID)
{
	RF_INT32 iRet;
//	ST_TIMER st_timer = {0,0,0};

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	// Jarod@2015/08/30 UPD START
	if (0 == bInitStatusInfo)
	{
		return RF_ERR_INVALIDSTATE;
	}
	// Jarod@2015/08/30 UPD END

	iRet = OsWlCheck();

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlCheck, iRet:%d",iRet);

	if (ERR_NET_LOGOUT==iRet || ERR_NET_IF==iRet)
	{
		//TODO
		// Jarod@2015/08/31 UPD START
		s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
		s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_INICIO;
		// Jarod@2015/08/31 UPD END

		return RF_SUCCESS;
	}

	iRet = OsWlLogout();

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlLogout, iRet:%d",iRet);
	if (ERR_DEV_NOT_OPEN == iRet)
	{
		return RF_ERR_DEVICEFAULT;
	}
	while (1)
	{
		sleep(1);
		iRet = OsWlCheck();
		if (PPP_LOGOUTING != iRet)
		{
			break;
		}
	}

	OsWlUnLock();

	bInitGprs = 0;

    return RF_SUCCESS;
}

RF_INT32 RF_gsm_getInfo(RF_GSM_INFO* gsmInfo)
{
	RF_INT32 iRet = -1, iRssi = -1, iBer = -1, iCnt = -1, iCme = -1, iFmt = -1, iMode = -1, iAllDigit = -1;
	// Jarod@2015/09/02 UPD START
	// what the hell of these ?????????
	//RF_UCHAR ucRecvBuf[255] = "\0", ucNetWorkInfo[64] = "\0";
	RF_UCHAR ucRecvBuf[255] = {0}, ucNetWorkInfo[64] = {0};
	// Jarod@2015/09/02 UPD END
	RF_CHAR *pResponse = NULL;


	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"\n\n\n\n");

	showState();

	if (NULL==gsmInfo)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__," RF_ERR_INVALIDARG ");
		return RF_ERR_INVALIDARG;
	}
	
	if (NULL == s_stGsmInfo)
	{
		s_stGsmInfo = (RF_GSM_INFO*)malloc(sizeof(RF_GSM_INFO));

		// Jarod@2015/09/02 ADD START
		memset(s_stGsmInfo, 0, sizeof(RF_GSM_INFO));
		// Jarod@2015/09/02 ADD END
	}

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_gsm_getInfo start!");

	memset(gsmInfo, 0, sizeof(RF_GSM_INFO));

	iRet = OsWlPortOpen("115200,8,n,1");
	if (0 != iRet)
	{
		// Jarod@2015/09/15 ADD START
		// if PortOpen failure, please check the WL Init again
		iRet = OsWlCheck();
		if (iRet<0)
		{
			iRet = OsWlLock();
			if (iRet == RET_OK)
			{
				iRet = OsWlInit(NULL);
				PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== OsWlInit return : [%d] ======",iRet);
				if (iRet != RET_OK)
				{
					return RF_ERR_DEVICEFAULT;
				}
				//If WlInit Success, then return RF_ERR_DEVICEFAULT to let Rede call getInfo again.
			}
			else
			{
				PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== OsWlPortOpen Failure ======");
				PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== RF_ERR_DEVICEFAULT ======");
				return RF_ERR_DEVICEFAULT;
			}
		}

		// Jarod@2015/09/15 ADD END
		//TODO
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== OsWlPortOpen Failure ======");
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
		return RF_ERR_DEVICEFAULT;
	}


	// Jarod@2015/08/31 ADD START
	// move check simcard to the fisrt position
	// Jarod@2015/08/31 ADD END
	// check has simcard or not
	OsWlPortSend("AT+CRSM=?\r", strlen("AT+CRSM=?\r"));

	// Jarod@2015/09/04 UPD START
	memset(ucRecvBuf, 0, sizeof(ucRecvBuf));
	// Jarod@2015/09/04 UPD END

	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+CRSM, iRet:%d, ucRecvBuf:%s", iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		//TODO
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_INVALIDARG:AT+CRSM, iRet:%d, ucRecvBuf:%s", iRet, ucRecvBuf);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== 1.OsWlPortRecv Failure ======");
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
		return RF_ERR_INVALIDARG;
	}


	// Jarod@2015/09/04 UPD START
	// if any error return, then we consider that is no sim card
	//pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CME ERROR: 10");
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CME ERROR: ");
	// Jarod@2015/09/04 UPD END

	if (pResponse)
	{
		sscanf(pResponse, "%*s%s %d",ucRecvBuf,&iCme);

		// according to the AT specification, the Cme should between 1 to 1283
		if (iCme >= 1 && iCme <= 1999)
		{
			s_stGsmStatusInfo->cme_erro = 10;
			s_stGsmInfo->has_sim_card = HF_FALSE;
		}
		else
		{
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"pResponse:%s",pResponse);
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"iCme:%d",iCme);
			// Jarod@2015/08/31 ADD START
			s_stGsmStatusInfo->cme_erro = 10; //"+CME ERROR: 10"
			// Jarod@2015/08/31 ADD END
			s_stGsmInfo->has_sim_card = HF_FALSE;
		}
	}
	else
	{
		s_stGsmInfo->has_sim_card = HF_TRUE;
	}
	// Jarod@2015/09/01 UPD START
	memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	// Jarod@2015/09/01 UPD END
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"has_sim_card:%d", s_stGsmInfo->has_sim_card);


	//get rssi
	OsWlPortSend("AT+CSQ\r", strlen("AT+CSQ\r"));

	// Jarod@2015/09/04 UPD START
	memset(ucRecvBuf, 0, sizeof(ucRecvBuf));
	// Jarod@2015/09/04 UPD END

	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+CSQ , iRet:%d, ucRecvBuf:%s",iRet, ucRecvBuf);


	// Jarod@2015/09/03 UPD START
	if (iRet == 0)
	{
		OsSleep(1000);
		//try again
		iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);

		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+CSQ , iRet:%d, ucRecvBuf:%s",iRet, ucRecvBuf);
	}

	// if still <=0, then return error
	if (iRet <= 0)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_INVALIDARG:AT+CSQ , iRet:%d, ucRecvBuf:%s",iRet, ucRecvBuf);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== 2.OsWlPortRecv Failure ======");
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
		return RF_ERR_INVALIDARG;
	}
	// Jarod@2015/09/03 UPD END

	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CSQ:");
	if (pResponse)
	{
		sscanf(pResponse, "%*s%d,%d", &iRssi, &iBer);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"=====   iRssi:[%d]\n\n",iRssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"=====   iBer:[%d]\n\n",iBer);

		// Jarod@2015/09/04 UPD START
		if (iRssi>=0 && iRssi<10)
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 0;
		}// Jarod@2015/09/04 UPD END
		else if (iRssi>=10 && iRssi<15)
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 20;
		}
		else if (iRssi>=15 && iRssi<19)
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 40;
		}
		else if (iRssi>=19 && iRssi<23)
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 60;
		}
		else if (iRssi>=23 && iRssi<=27)
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 80;
		}
		// Jarod@2015/09/04 UPD START
		// if iRssi return 31 ??????
		//else if (iRssi>27 && iRssi<31)
		else if (iRssi>27 && iRssi<=31)
		// Jarod@2015/09/04 UPD END
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 100;
		}
		else// Jarod@2015/09/04 UPD START
		{
			s_stGsmInfo->rssi = iRssi;
			s_stGsmInfo->radio_level = 0;
		}// Jarod@2015/09/04 UPD END


		// sometimes,iBer always be 99
		if (iBer >= 0 && iBer <= 7)
		{
			s_stGsmInfo->ber = iBer;
		}
		else
		{
			s_stGsmInfo->ber = 0;
		}
	}
	// Jarod@2015/09/01 UPD START
	memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	// Jarod@2015/09/01 UPD END




	//get network name or network code
	OsWlPortSend("AT+COPS?\r", strlen("AT+COPS?\r"));

	// Jarod@2015/09/04 UPD START
	memset(ucRecvBuf, 0, sizeof(ucRecvBuf));
	// Jarod@2015/09/04 UPD END

	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+COPS, iRet:%d, ucRecvBuf:%s",iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		//TODO
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_INVALIDARG:AT+COPS, iRet:%d, ucRecvBuf:%s",iRet, ucRecvBuf);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== 3.OsWlPortRecv Failure ======");
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
		return RF_ERR_INVALIDARG;
	}

	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+COPS:");

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+COPS, pResponse:%s",pResponse);

	if (pResponse)
	{

		// Jarod@2015/09/04 ADD START
		//first of all, make sure the format is as : +COPS: x,x,"xxxxxxx..."
		sscanf(pResponse,"%*s%d,%d,%s",&iFmt,&iMode,ucNetWorkInfo);

		if ((ucNetWorkInfo[0] != '\"') || (ucNetWorkInfo[strlen((char *)ucNetWorkInfo)-1] != '\"'))
		{
			strcpy((char *)ucNetWorkInfo,(char *)"UNKNOWN");
			strcpy(s_stGsmInfo->network_name, (char *)ucNetWorkInfo);
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"1.ucNetWorkInfo:%s",ucNetWorkInfo);
		}
		else
		{

			// Jarod@2015/09/04 ADD START
			//TODO
			iCnt = (strrchr((char *)ucNetWorkInfo, '\"') - strchr((char *)ucNetWorkInfo, '\"') - 1);
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"2.%d",iCnt);

			strncpy((char *)ucNetWorkInfo, strchr((char *)ucNetWorkInfo, '\"') + 1, iCnt);
			ucNetWorkInfo[iCnt]= 0;
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"3.ucRecvBuf:%s,%d",ucNetWorkInfo,iCnt);

			// check is all digit or not
			for (iCnt=0;iCnt<strlen((char *)ucNetWorkInfo);iCnt++)
			{
				if(isdigit(ucNetWorkInfo[iCnt]))
				{
					iAllDigit = 1;
				}
				else
				{
					iAllDigit = 0;
					break;
				}
			}

			// if not alldigit,just display
			if (iAllDigit)
			{
				if (5 == strlen((char *)ucNetWorkInfo)) //
				{
					// check table
					for (iCnt=0; 0!=strcmp((char *)ucNetWorkInfo, st_NetWorkInfo[iCnt].code) && (iCnt < ((sizeof(st_NetWorkInfo) / sizeof(NET_WORK_INFO)) - 1 )); iCnt++)
					{
					}
					PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"4.network_name:%s", st_NetWorkInfo[iCnt].operator);
					strcpy(s_stGsmInfo->network_name, st_NetWorkInfo[iCnt].operator);
				}
				else if (iFmt == 2)
				{
					//return unknown or null
					PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"7.network_name: UNKNOWN");
					strcpy(s_stGsmInfo->network_name, "UNKNOWN");
				}
				else
				{
					//display directly
					PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"6.network_name:%s",ucNetWorkInfo);
					strcpy(s_stGsmInfo->network_name, (RF_CHAR *)ucNetWorkInfo);
				}
			}
			else
			{
				if (iFmt == 2)
				{
					//return unknown or null
					PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"7.network_name: UNKNOWN");
					strcpy(s_stGsmInfo->network_name, "UNKNOWN");
				}
				else
				{
					//just display directly
					PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"8.network_name:%s",ucNetWorkInfo);
					strcpy(s_stGsmInfo->network_name, (RF_CHAR *)ucNetWorkInfo);
				}
			}
			// Jarod@2015/09/04 ADD END

/*			//TODO
			strncpy((RF_CHAR *)ucNetWorkInfo, strchr(ucNetWorkInfo, '\"') + 1, (strrchr(ucNetWorkInfo, '\"') - strchr(ucNetWorkInfo, '\"') - 1) );
			if (5==strlen((RF_CHAR *)ucNetWorkInfo) && isdigit(ucNetWorkInfo[0]))
			{
				for (iCnt=0; 0!=strcmp((RF_CHAR *)ucNetWorkInfo, st_NetWorkInfo[iCnt].code) && (iCnt < ((sizeof(st_NetWorkInfo) / sizeof(NET_WORK_INFO)) - 1 )); iCnt++)
				{
				}
				strcpy(s_stGsmInfo->network_name, st_NetWorkInfo[iCnt].operator);
			}
			else
			{
				strcpy(s_stGsmInfo->network_name, (RF_CHAR *)ucNetWorkInfo);
			}
*/
		}

		// Jarod@2015/09/04 ADD END
	}

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"network_name:%s",s_stGsmInfo->network_name);
	// Jarod@2015/09/01 UPD START
	memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	// Jarod@2015/09/01 UPD END



	// get CCID
	OsWlPortSend("AT+CCID?\r", strlen("AT+CCID?\r"));

	// Jarod@2015/09/04 UPD START
	memset(ucRecvBuf, 0, sizeof(ucRecvBuf));
	// Jarod@2015/09/04 UPD END

	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"AT+CCID, iRet:%d, ucRecvBuf:%s", iRet, ucRecvBuf);
	if (iRet <= 0 )
	{
		//TODO
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====== 4.OsWlPortRecv Failure ======");
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
		return RF_ERR_INVALIDARG;
	}
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CCID:");
	if (pResponse)
	{
		sscanf(pResponse, "%*s%s", s_stGsmInfo->sim_icc_id);
	}

/*	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"pResponse:%s", pResponse);
	strcpy(pResponse,"CCID:ERROR");
	sscanf(pResponse, "%*s%s", s_stGsmInfo->sim_icc_id);*/

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"pResponse:%s", pResponse);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"sim_icc_id:%s", s_stGsmInfo->sim_icc_id);
	// Jarod@2015/09/01 UPD START
	memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	// Jarod@2015/09/01 UPD END

	OsWlPortClose();

	iRet = OsWlCheck();
	if (RF_SUCCESS != iRet)
	{
		s_stGsmInfo->gsm_data_network = RF_GSM_NONE;
	}
	else
	{
		s_stGsmInfo->gsm_data_network = RF_GSM_GPRS;
	}
	memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"=========== Final Value ============");
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- has_sim_card:%d",gsmInfo->has_sim_card);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- sim_icc_id:%s",gsmInfo->sim_icc_id);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- rssi:%d",gsmInfo->rssi);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- radio_level:%d",gsmInfo->radio_level);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- network_name:%s",gsmInfo->network_name);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- gsm_data_network:%d",gsmInfo->gsm_data_network);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_GSM_INFO -- ber:%d",gsmInfo->ber);
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"====================================");

	// Jarod@2015/09/01 UPD START
	// should copy s_stGsmInfo to gsmInfo after each steps
	//memcpy(gsmInfo, s_stGsmInfo, sizeof(RF_GSM_INFO));
	// Jarod@2015/09/01 UPD END
    return RF_SUCCESS;
}

// Jarod@2015/08/30 UPD START
RF_INT32 RF_gsm_status(RF_GSM_STATUS_INFO* info)
{
	RF_INT32 iRet=0;

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_gsm_status");

	if (NULL==info)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_gsm_status NULL==info");
		return RF_ERR_INVALIDARG;
	}

	showState();

	if(NULL == s_stGsmStatusInfo)
	{
		OsWlUnLock();

		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_gsm_status OsWlUnLock");

		s_stGsmStatusInfo = (RF_GSM_STATUS_INFO*)malloc(sizeof(RF_GSM_STATUS_INFO));
		if(s_stGsmStatusInfo)
		{
			// Phase A.1
			PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_gsm_status Phase A.1");

			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_INICIO;

			bInitStatusInfo = 1;
		}
	}
	else
	//if (s_stGsmStatusInfo->etapa == RF_GSM_ETAPA_G1 && s_stGsmStatusInfo->estadoEtapa == RF_GSM_ETAPA_ESTADO_EXECUTANDO)
	{
		iRet = OsWlCheck();
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"OsWlCheck return [%d]",iRet);
		switch(iRet)
		{
		case PPP_LOGINING:
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
			break;
		case RET_OK:
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_FIM;
			break;
		case PPP_LOGOUTING:
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
			break;
		case ERR_NET_LOGOUT:
			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			// Jarod@2015/08/31 UPD START
			//s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_FIM;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_INICIO;
			// Jarod@2015/08/31 UPD END
			break;
		//ERR_NET_IF
		default:
			// Jarod@2015/09/01 UPD START
			// there is no G1.4 status, if logining failure, just return to the beginning.
			//if (s_stGsmStatusInfo->etapa == RF_GSM_ETAPA_G1)
			//{
			//	s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_G1;
			//	s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_ERRO;
			//}
			//else
			//{
			//	s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			//	s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_INICIO;
			//}

			s_stGsmStatusInfo->etapa = RF_GSM_ETAPA_A;
			s_stGsmStatusInfo->estadoEtapa  = RF_GSM_ETAPA_ESTADO_INICIO;

			// Jarod@2015/09/01 UPD END
			break;
		}
	}

	memcpy(info, s_stGsmStatusInfo, sizeof(RF_GSM_STATUS_INFO));
	return RF_SUCCESS;
}
// Jarod@2015/08/30 UPD END

static RF_INT32 SetOpt(RF_INT32 fd, RF_INT32 nSpeed, RF_INT32 nBits, RF_CHAR nEvent, RF_INT32 nStop)
{
	struct termios newtio, oldtio;

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if (tcgetattr(fd, &oldtio) != 0)
	{
		printf("SetupSerial 1");
		return -1;
	}
	memset(&newtio, 0, sizeof(newtio));
	newtio.c_cflag |= CLOCAL | CREAD;
	newtio.c_cflag &= ~CSIZE;
	newtio.c_cflag &= ~CRTSCTS;
	newtio.c_lflag &= ~ICANON;

	switch (nBits)
	{
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
	default:
		newtio.c_cflag |= CS8;
		break;
	}

	switch (nEvent)
	{
	case 'O':
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':
		newtio.c_iflag |= (INPCK | ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':
	default:
		newtio.c_cflag &= ~PARENB;
		break;
	}

	switch (nSpeed)
	{
	case 1200:
		cfsetispeed(&newtio, B1200);
		cfsetospeed(&newtio, B1200);
		break;
	case 2400:
		cfsetispeed(&newtio, B2400);
		cfsetospeed(&newtio, B2400);
		break;
	case 4800:
		cfsetispeed(&newtio, B4800);
		cfsetospeed(&newtio, B4800);
		break;
	case 9600:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	case 19200:
		cfsetispeed(&newtio, B19200);
		cfsetospeed(&newtio, B19200);
		break;
	case 38400:
		cfsetispeed(&newtio, B38400);
		cfsetospeed(&newtio, B38400);
		break;
	case 57600:
		cfsetispeed(&newtio, B57600);
		cfsetospeed(&newtio, B57600);
		break;
	case 115200:
		cfsetispeed(&newtio, B115200);
		cfsetospeed(&newtio, B115200);
		break;
	default:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	}

	if (nStop == 1)
	{
		newtio.c_cflag &= ~CSTOPB;
	}
	else if (nStop == 2)
	{
		newtio.c_cflag |= CSTOPB;
	}

	newtio.c_cc[VTIME] = 0;
	newtio.c_cc[VMIN] = 0;
	tcflush(fd, TCIFLUSH);
	if ((tcsetattr(fd, TCSANOW, &newtio)) != 0)
	{
		printf("com set error");
		return -1;
	}
	printf("set uart as \"%d %d %c %d\" done!\n", nSpeed, nBits,
	    nEvent, nStop);
	return 0;
}

static RF_INT32 parse_attr(const RF_CHAR *attr, RF_INT32 *speed, RF_INT32 *databits,
    RF_INT32 *parity, RF_INT32 *stopbits)
{
	RF_INT32 iCnt = -1;
	RF_CHAR buf[50] = "\0";
	const long baud_tab[] = { 600, 1200, 2400, 4800, 9600, 14400, 19200,
	    28800, 38400, 57600, 115200, 230400, };
	RF_CHAR key[] = " ,.\r\n";
	RF_CHAR *str = NULL;

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if (strlen(attr) > 49)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"strlen(attr) > 49");
		return -1;
	}
	strcpy(buf, attr);
	/* attr = buf; */
	str = strtok(buf, key);
	if (str == NULL)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"strtok return null");
		return -1;
	}
	*speed = atoi(str);
	for (iCnt = 0; iCnt < (RF_INT32)(sizeof(baud_tab) / sizeof(long)); iCnt++)
	{
		if (baud_tab[iCnt] == *speed)
		{
			break;
		}
	}
	if (iCnt == sizeof(baud_tab) / sizeof(long))
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"iCnt == sizeof(baud_tab) / sizeof(long)");
		return -1;
	}

	str = strtok(NULL, key);
	if (str == NULL)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"strtok return null 2");
		return -1;
	}
	*databits = atoi(str);
	if (*databits < 5 || *databits > 8)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"*databits < 5 || *databits > 8");
		return -1;
	}

	str = strtok(NULL, key);
	if (str == NULL || strlen(str) > 1)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"strtok return null 3");
		return -1;
	}
	if (str[0] == 'e' || str[0] == 'n' || str[0] == 'o')
	{
		str[0] = toupper(str[0]);
	}
	if (str[0] != 'E' && str[0] != 'N' && str[0] != 'O')
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"str[0] != 'E' && str[0] != 'N' && str[0] != 'O'");
		return -1;
	}
	*parity = str[0];

	str = strtok(NULL, key);
	if (str == NULL || strlen(str) > 1)
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"strtok return null 4");
		return -1;
	}
	if (str[0] != '1' && str[0] != '2')
	{
		PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"str[0] != '1' && str[0] != '2'");
		return -1;
	}
	*stopbits = atoi(str);
	return 0;
}

static RF_INT32 time_compare(struct timeval tv1, struct timeval tv2)
{
	//PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if (tv1.tv_sec > tv2.tv_sec)
	{
		return 1;
	}
	else if (tv1.tv_sec < tv2.tv_sec)
	{
		return -1;
	}
	else if (tv1.tv_usec > tv2.tv_usec)
	{
		return 1;
	}
	else if (tv1.tv_usec < tv2.tv_usec)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static RF_VOID time_add_ms(struct timeval *tv, RF_INT32 ms)
{
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	tv->tv_sec += ms / 1000;
	tv->tv_usec += ((ms % 1000) * 1000);
}

RF_INT32 OsWlPortOpen(const RF_CHAR *Attr)
{
	RF_INT32 speed = -1, databits = -1, stopbits = -1, parity = -1;

	PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"");

	if (Attr == NULL)
	{
		PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"Attr == NULL");
		return -1;
	}

	if (parse_attr(Attr, &speed, &databits, &parity, &stopbits) < 0)
	{
		PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"parse_attr<0");
		return -1;
	}


    if(fp_uart > 0)
    {
    	PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"fp_uart > 0, close(fp_uart)");
        close(fp_uart);
    }


    // Jarod@2015/09/03 ADD START
/*
    if (0 == bInitGprs)
    {
    	fp_uart = open("/dev/ttywl", O_RDWR | O_NOCTTY);
	    // Jarod@2015/09/02 UPD START
    	// why is ttyAMA1 but ttywl  ????????
    	//PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"ttyAMA1,fp_uart:%d", fp_uart);
    	PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"ttywl,fp_uart:%d", fp_uart);
	    // Jarod@2015/09/02 UPD END
    }
    else
*/
    // Jarod@2015/09/03 ADD END

    {
    	fp_uart = open("/dev/mux1", O_RDWR | O_NOCTTY);
    	PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"mux1,fp_uart:%d", fp_uart);
    }

	if(fp_uart < 0)
	{
		//TODO
		PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"fp_uart < 0");
		return -1;
	}

	if (SetOpt(fp_uart, speed, databits, parity, stopbits) < 0)
	{
		//TODO
		PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"close(fp_uart)");
		return -1;
	}

	tcflush(fp_uart, TCIOFLUSH);

	PaxLog_GSM(LOG_DEBUG,  __FUNCTION__, __LINE__,"tcflush(fp_uart, TCIOFLUSH)");

	return 0;
}

RF_VOID OsWlPortClose()
{
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if (fp_uart < 0)
	{
		return;
	}
	close(fp_uart);
	fp_uart = -1;
}

RF_VOID OsWlPortReset()
{
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	tcflush(fp_uart, TCIOFLUSH);
}

RF_INT32 OsWlPortSend(const RF_VOID *SendBuf, RF_INT32 SendLen)
{
	RF_INT32 ret = 0;
	const RF_CHAR *buf = SendBuf;

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	/*When you don't set open(..O_NDELAY) or fcntl(fd, F_SETFL, FNDELAY),
	The while() and usleep() isn't needed.*/
	while (SendLen > 0)
	{
		ret = write(fp_uart, buf, SendLen);
		if (ret < 0)
		{
			break;
		}
		buf += ret;
		SendLen -= ret;
		usleep(10 * 1000);
	}
	return (ret >= 0) ? 0 : -1;
}

static RF_INT32 port_err(RF_INT32 err)
{
	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if (err == EFAULT)
	{
		return -1013;
	}
	return -1014;
}

RF_INT32 OsWlPortRecv(RF_VOID *RecvBuf, RF_INT32 RecvLen, RF_INT32 TimeoutMs)
{
	RF_CHAR *buf = RecvBuf;
	RF_INT32 ret = -1, err = -1, total = -1;
	struct timeval tv1, tv2;

	PaxLog_GSM(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	/* Flush the input and output pools. */
	if (buf == NULL && RecvLen == 0)
	{
		//TODO
		OsWlPortReset();
		return 0;
	}

	if (buf == NULL || RecvLen < 0 || TimeoutMs < 0)
	{
		//TODO
		return -1;
	}

	if (!RecvLen)
	{
		//TODO
		return 0;
	}

	if (TimeoutMs < 100 && TimeoutMs > 0)
	{
		TimeoutMs = 100;
	}

	if (gettimeofday(&tv1, NULL) < 0)
	{
		return -3209;
	}
	time_add_ms(&tv1, TimeoutMs);

	total = 0;
	while (RecvLen > 0)
	{
		ret = read(fp_uart, buf, RecvLen);
		if (ret < 0)
		{
			err = errno;
			if (err != EAGAIN && err != EINTR)
			{
				return port_err(err);
			}
		}
		else
		{
			buf += ret;
			RecvLen -= ret;
			total += ret;
			if (RecvLen <= 0)
			{
				break;
			}
			if (gettimeofday(&tv2, NULL) < 0)
			{
				return -3209;
			}
			if (time_compare(tv1, tv2) <= 0)
			{
				break;
			}
		}
	}
	return total;
}


void showState(void)
{
	PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- showState -------");

	if (s_stGsmStatusInfo == NULL)
	{
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- s_stGsmStatusInfo -------");
		return;
	}

	switch(s_stGsmStatusInfo->etapa)
	{
	case RF_GSM_ETAPA_A:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- A -------");
		break;
	case RF_GSM_ETAPA_G1:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- G1 -------");
		break;
	case RF_GSM_ETAPA_G2:
	case RF_GSM_ETAPA_G3:
	case RF_GSM_ETAPA_G4:
	case RF_GSM_ETAPA_C1:
	case RF_GSM_ETAPA_C2:
		break;
	}

	switch(s_stGsmStatusInfo->estadoEtapa)
	{
	case RF_GSM_ETAPA_ESTADO_INICIO:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- 1 -------");
		break;
	case RF_GSM_ETAPA_ESTADO_EXECUTANDO:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- 2 -------");
		break;
	case RF_GSM_ETAPA_ESTADO_FIM:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- 3 -------");
		break;
	case RF_GSM_ETAPA_ESTADO_ERRO:
		PaxLog_GSM(LOG_DEBUG, __FUNCTION__, __LINE__, "------- 4 -------");
		break;
	}
}
