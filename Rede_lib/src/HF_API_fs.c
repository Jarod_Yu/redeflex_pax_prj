#define DO_NOT_REDEFINE_FILESYSTEM_FUNCTIONS
#include "HF_API.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <osal.h>
#include <xui.h>

// hzg 20150428---------------------
#include <errno.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statfs.h>
// ---------------------------------

// Fabrizio@21102015 - Includes
#include <pthread.h>

#define MAX_OPEN_FILES  30
#define STACK_MAX_DEPTH  10
#define ACCESS_MODE_LENGTH	2
//#define ROOTPATH "/data/app/MAINAPP/root"

#ifdef HF_FS_MAX_DIR_DEPTH
	#undef HF_FS_MAX_DIR_DEPTH
	#undef HF_FS_MAX_PATH_LENGTH
#endif
#define HF_FS_MAX_DIR_DEPTH ((6 + 1) + 4)	//Considerar /data/app/MAINAPP/root
#define HF_FS_MAX_PATH_LENGTH   ((HF_FS_MAX_FILE_NAME + 1) * HF_FS_MAX_DIR_DEPTH) * 2

typedef struct {
    char filename[HF_FS_MAX_PATH_LENGTH];
    FILE *f;
} HFI_FILE_T;

static HF_INT32 HF_errno;

typedef struct _tagFileInfo
{
	char *fileName;
	FILE *pFile;
	HF_INT32 rwFlag;   //1 write  0 read

}FILE_INFO;

typedef struct _tagFileNode
{
	FILE_INFO  fileInfo;
	struct _tagFileNode *next;
}FILE_NODE;

FILE_NODE *nodeHeader = NULL;

HF_INT32 curOpenFileNum = 0;

HF_INT32 stackPosition = -1;
//HF_CHAR cwd[HF_FS_MAX_PATH_LENGTH] = "/data/app/MAINAPP/root";
static HF_CHAR cwd[HF_FS_MAX_PATH_LENGTH];

// Jarod@2015/09/16 UPD START
//static HF_CHAR path_stack[STACK_MAX_DEPTH][HF_FS_MAX_FILE_NAME];
static HF_CHAR path_stack[STACK_MAX_DEPTH][HF_FS_MAX_FILE_NAME*2];
// Jarod@2015/09/16 UPD END


// Jarod@2015/09/18 ADD START
/*
static HF_VOID nodeLogView(FILE_NODE *viewNode);
static HF_VOID changeFileListHeader(FILE_NODE *newNode);
static HF_BOOL nodeInsert(FILE_NODE *header, FILE_NODE *insertNode);
static HF_BOOL nodeDelete(FILE_NODE *header, FILE *fd);
static HF_BOOL nodeSearch(FILE_NODE *header, const HF_CHAR *filename);
static FILE_NODE *CreateNewNode(const HF_CHAR *filename, const HF_CHAR *accessmode, FILE *fd);
static HF_BOOL listInsert(const HF_CHAR *filename,  const HF_CHAR *accessmode, HFI_FILE_T *fp);
*/
// Jarod@2015/09/18 ADD END

//static HF_BOOL getAbsolutePath(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen);
static HF_BOOL checkPath(const HF_CHAR *filename, /*[PAXBR]*/HF_CHAR *szAbPath);
static HF_BOOL isDirEmpty(DIR *dirPtr);
static HF_VOID stack_init();
static HF_CHAR* stack_pop();
static HF_BOOL stack_push(HF_CHAR *dir);
static HF_VOID stack_clean();

// Fabrizio - 20150928       : Control open files and file access
static iOpenFilesCounter;

// Fabrizio@21102015 - MUTEX variable, due to https://bitbucket.org/cavamora/pax/issues/5
static pthread_mutex_t	fsMutex;

typedef struct _stOpenFiles
{
	HF_FILE_T	fileHandle;
	HF_CHAR		szFileName[HF_FS_MAX_PATH_LENGTH];
	HF_CHAR		szAccessMode[ACCESS_MODE_LENGTH + 1];
} stOpenFiles;

static stOpenFiles openFilesList[MAX_OPEN_FILES];

static HF_INT32 canFileBeOpened(const HF_CHAR *filename, const HF_CHAR *accessmode);
static HF_VOID insertNewOpenFile(HF_FILE_T *fileHandle, const HF_CHAR *filename, const HF_CHAR *accessmode);
static HF_VOID fileClosed(HF_FILE_T *fileHandle);
static HF_VOID printOpenFilesList(HF_VOID);
// Fabrizio - 20150928 - END : Control open files and file access

// Fab@2015/09/12 ADD START
HF_INT32 HFI_fs_start(HF_VOID)
{
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTER HFI_fs_start()");

	// Create the root directory, if necessary
	sprintf(cwd, "%s", "/data/app/MAINAPP/root");
	HF_fs_mkdir(cwd);

	// Initialize directories stack
	stack_init();

	// Initialize open files counter
	iOpenFilesCounter = 0;
	memset(openFilesList, 0, sizeof(openFilesList));

	// Fabrizio@21102015 - Initializes mutex
	pthread_mutex_init(&fsMutex, NULL);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"EXIT HFI_fs_start()");
	return HF_SUCCESS;
}
// Fab@2015/09/12 ADD END

// Fab@2015/09/24 ADD START
HF_INT32 HFI_fs_shutdown(HF_VOID)
{
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTER HFI_fs_shutdown()");

	// Clean directories stack
	stack_clean();

	// Initialize open files counter
	iOpenFilesCounter = 0;
	memset(openFilesList, 0, sizeof(openFilesList));

	// Fabrizio@21102015 - Finishes mutex
	pthread_mutex_destroy(&fsMutex);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"EXIT HFI_fs_shutdown()");
	return HF_SUCCESS;
}
// Fab@2015/09/12 ADD END

HF_INT32 HF_fs_fopenH(const HF_CHAR *filename, const HF_CHAR *accessmode, HF_FILE_T **file)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);

	HF_INT32 iRet = 0;
	
	// Jarod@2015/07/28 ADD START
	DIR *dirPtr = NULL;
	// Jarod@2015/07/28 ADD END
	
	HFI_FILE_T *h_file_t = NULL;
	HF_CHAR absolutePath[HF_FS_MAX_PATH_LENGTH] = {0};

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTER HF_fs_fopenH[%s - %s]", filename, accessmode);

	if((NULL == file) || (NULL == filename))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fopenH (NULL == file) return [%d]", RF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return RF_ERR_DEVICEFAULT;
	}

	// Jarod@2015/09/15 UPD START
	////if the open file number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	//if(curOpenFileNum >= MAX_OPEN_FILES)
	//{
	//	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fopenH[HF_ERR_OPENFILES] : %d, %d", curOpenFileNum, MAX_OPEN_FILES);
	//    	return HF_ERR_OPENFILES;
	//}
	//if the open file number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	// Jarod@2015/09/15 UPD END

/*
	if(*filename == '/')
	{
		filename++;
	}
*/

	//whether the path is correct
//	if(!checkPath(filename, NULL))
	if(!checkPath(filename, absolutePath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_fopenH[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	//whether accessmode is correct
    if( (NULL == accessmode) || ((0 != strcmp(accessmode, "r")) && (0 != strcmp(accessmode, "rw"))))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_fopenH[%d]", HF_ERR_INVALMODE);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_INVALMODE;
    }

    //if filename is a directory
    dirPtr = opendir(filename);
    if(NULL != dirPtr)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"5. HF_fs_fopenH[%d]", HF_ERR_ISADIR);
    	closedir(dirPtr);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_ISADIR;
    }

/*
    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"5.1 getAbsolutePath(%s, %s, %d)", filename, absolutePath, HF_FS_MAX_PATH_LENGTH);
    iRet = getAbsolutePath(filename, absolutePath, HF_FS_MAX_PATH_LENGTH);
    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"5.2 getAbsolutePath(%s, %s, %d) = [%d]", filename, absolutePath, HF_FS_MAX_PATH_LENGTH, iRet);
    if(0 == iRet)
    {
    	strcpy(absolutePath, filename);
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"6. HF_fs_fopenH[%s, %s]", absolutePath, filename);
    }
*/

    // Jarod@2015/09/15 UPD START
    ////if the file is already open in "rw" mode, return HF_ERR_ISOPEN
    //	if(nodeSearch(nodeHeader, absolutePath) && (0 == strcmp(accessmode, "rw")))
    //	{
	//		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"file is write absolutePath:%s ",absolutePath);
    //		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"6. File is already open in rw mode - absolutePath[%s]", absolutePath);
    //		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"6. HF_fs_fopenH[%d]", HF_ERR_ISOPEN);
    //	  	return HF_ERR_ISOPEN;
    //	}
    //if the file is already open in "rw" mode, return HF_ERR_ISOPEN
    // Jarod@2015/09/15 UPD END

	h_file_t = (HFI_FILE_T *)malloc(sizeof(HFI_FILE_T));
    if(!h_file_t)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"7. HF_fs_fopenH[%d]", HF_ERR_NOMEMORY);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_NOMEMORY;
    }

    // [PAXBR] - Replace filename with absolutePath
    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"h_file_t->filename[%s]", absolutePath);
	strcpy(h_file_t->filename, absolutePath);
	//PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"h_file_t->filename[%s] | absolutePath[%s]", h_file_t->filename, absolutePath);

	// Fabrizio - 20150928 : First check if a new file can be opened
	printOpenFilesList();
	iRet = canFileBeOpened(h_file_t->filename, accessmode);
    PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__,__LINE__,"canFileBeOpened(%s, %s) = [%d]", h_file_t->filename, accessmode, iRet);
	if ( iRet != HF_SUCCESS )
	{
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return iRet;
	}
	// Fabrizio - 20150928 : First check if a new file can be opened

    //trans open mode while accessmode is "rw"
    if(0 == strcmp(accessmode, "rw"))
    {
    	// [PAXBR] - Replace filename with absolutePath
    	if(0 == access(absolutePath, F_OK))
		{
			h_file_t->f = fopen(absolutePath, "r+");
		}
		else
		{
			h_file_t->f = fopen(absolutePath, "w+");
		}
    }
    else
    {
    	h_file_t->f = fopen(absolutePath, "r");
    }

    if (!h_file_t->f)
    {
    	free(h_file_t);
    	//fopen failed return the errno
    	// [PAXBR]
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d - %s]", errno, strerror(errno));
		switch(errno)
		{
		case ENOENT:
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOTFOUND);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return HF_ERR_NOTFOUND;
		case ENOMEM:
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOMEMORY);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return HF_ERR_NOMEMORY;
		case ENOSPC:
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", HF_ERR_NOSPACE);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return HF_ERR_NOSPACE;
		default:
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"8. HF_fs_fopenH[%d]", RF_ERR_DEVICEFAULT);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return RF_ERR_DEVICEFAULT;
		}
    }

    // Jarod@2015/09/15 UPD START
    ////insert node to open file list
    //if(HF_FALSE == listInsert(absolutePath, accessmode, h_file_t))
    //{
    //		free(h_file_t);
    //		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"9. HF_fs_fopenH[%d]", HF_errno);
    //	return HF_errno;
    //}
    //insert node to open file list
    // Jarod@2015/09/15 UPD END

    *file = h_file_t;

	// Fabrizio - 20150928 : Insert the file in the open files list
	insertNewOpenFile(*file, h_file_t->filename, accessmode);
	// Fabrizio - 20150928 : Insert the file in the open files list

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fopenH[0x%04x]", *file);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_FILE_T *HF_fs_fopenglobalH(const HF_CHAR *filename, const HF_CHAR *accessMode)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fopenglobalH(%s, %s)", filename, accessMode);
    return HF_NULL;
}

HF_FILE_T *HF_fs_fopenexternalH(const HF_CHAR *filename, const HF_CHAR *accessMode, const HF_CHAR *fatherApp)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fopenexternalH(%s, %s, %s)", filename, accessMode, fatherApp);
    return HF_NULL;
}

HF_INT32 HF_fs_fclose(HF_FILE_T *fp)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fclose : [%s]",fpi->filename);

    if (!fpi)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fclose[%d]", HF_SUCCESS);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_SUCCESS;
    }

    if (!fpi->f)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fclose[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_ERR_DEVICEFAULT;
    }

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fclose[%s]", fpi->filename);

    // Jarod@2015/09/15 UPD START
    ////delete node from file list
    // 	if(nodeDelete(nodeHeader, fpi->f))
    // 	{
    // 		curOpenFileNum--;
    // 	}
    //delete node from file list
    // Jarod@2015/09/15 UPD END

	fclose(fpi->f);

	// Fabrizio - 20150928 : File closed
	fileClosed(fp);
	// Fabrizio - 20150928 : File closed

	fpi->f = NULL;

	memset(fpi->filename, 0, HF_FS_MAX_PATH_LENGTH);
	free(fpi);
	fpi = NULL;

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fclose[%d]", HF_SUCCESS);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);

	return HF_SUCCESS;
}

// Jarod@2015/09/14 UPD START
//HF_INT32 HF_fs_readline(const HF_FILE_T *fd, HF_CHAR *buffer, HF_UINT32 maxlen)
HF_INT32 HFI_fs_readline(const HF_FILE_T *fd, HF_CHAR *buffer, HF_UINT32 maxlen)
// Jarod@2015/09/14 UPD END
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);

	HFI_FILE_T *fdi = (HFI_FILE_T *)fd;

	//TODO if '\n' or '\r' need store in the buffer?
	HF_CHAR ptr = 0;
	HF_INT32 i = 0;
	HF_INT32 ret = 0;

	if((!fd) || (!buffer))
	{
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return RF_ERR_DEVICEFAULT;
	}

	PaxLog_fs(LOG_DEBUG,"ENTROU HFI_fs_readline : [%s--%d]", fdi->filename, maxlen);

	//read byte one by one until '\n' or '\r'
	for(i=0; i<maxlen; i++)
	{
		ret = fread(&ptr, 1, 1, fdi->f);

		//&ptr = fgets(&ptr, 1, fdi->f);

		if(!ret)
		{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__, __LINE__,"ENTROU HFI_fs_readline i :%d", i);
			PaxLog_fs(LOG_DEBUG,__FUNCTION__, __LINE__, "ENTROU HFI_fs_readline ret :%d", ret);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return ret;
			//return HF_ERR_EOF;
		}
		else if((ptr == '\n') || (ptr == '\r'))
		{
			//\n\r read one more byte
			//fread(&ptr, 1, 1, fdi->f);
			PaxLog_fs(LOG_DEBUG,__FUNCTION__, __LINE__, "ENTROU HFI_fs_readline return i :%d", i);
			break;
		}
		else
		{
			*buffer++ = ptr;
		}
	}

/*	if(i >= maxlen)
	{
		return RF_ERR_SMALLBUFF;
	}
	else
	{
	    return RF_SUCCESS;
	}*/

	PaxLog_fs(LOG_DEBUG,__FUNCTION__, __LINE__, "ENTROU HFI_fs_readline final ret :%d", ret);

	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);

	return ret;
}

HF_INT32 HF_fs_fread(const HF_FILE_T *fp, HF_INT8 *buffer, HF_UINT32 length)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);

    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fread :[%s --%d]",fpi->filename, length);
    if((!fpi )|| (!fpi->f ) || (NULL == buffer) || (length > HF_FS_MAXREADBUFFSZ))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_fread[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_ERR_DEVICEFAULT;
    }

    HF_INT32 ret = fread(buffer, 1, length, fpi->f);
    if (!ret)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fread[%d]", HF_ERR_EOF);
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_fread[errno=%d %s] \n", errno,strerror(errno));
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	ret = HF_ERR_EOF;
    }

    PaxLog_fs_CA(LOG_DEBUG, __FUNCTION__, __LINE__, "SAIU HF_fs_fread[%d]", ret);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);

    return ret;
}

HF_INT32 HF_fs_fwrite(const HF_FILE_T *fp, const HF_INT8 *buffer, HF_UINT32 length)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);

    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fwrite: [%s--%d]",fpi->filename, length);

	HF_INT32 writeCnt = 0;

    if ((!fpi) || (!fpi->f) || (!buffer))
    {
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_ERR_DEVICEFAULT;
    }

    if(length > HF_FS_MAXWRITEBUFFSZ)
    {
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_BUFFOVERFLOW;
    }

    writeCnt =  fwrite(buffer, 1, length, fpi->f);
    if(writeCnt < 0)
    {
    	switch(errno)
    	{
    	case EROFS:
    		// Fabrizio@21102015 - Releases the resource
    		pthread_mutex_unlock(&fsMutex);
    		return HF_ERR_READONLY;
    	case ENOSPC:
    		// Fabrizio@21102015 - Releases the resource
    		pthread_mutex_unlock(&fsMutex);
    		return HF_ERR_NOSPACE;
    	default:
    		// Fabrizio@21102015 - Releases the resource
    		pthread_mutex_unlock(&fsMutex);
    		return HF_ERR_DEVICEFAULT;
    	}
    }

    PaxLog_fs_CA(LOG_DEBUG, __FUNCTION__, __LINE__, "SAIU HF_fs_fwrite[%d]", writeCnt);

	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return writeCnt;
}

HF_INT32 HF_fs_fseek(const HF_FILE_T *fp, HF_INT32 offset, HF_UINT16 position)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);

    HFI_FILE_T *fpi = (HFI_FILE_T *)fp;
    HF_INT32 iRet;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_fseek :[%s]",fpi->filename);

    if ((!fpi) || (!fpi->f))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fseek[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_ERR_DEVICEFAULT;
    }

    //Gabriel - 25092015
    if((position == HF_FILE_SEEK_END) && (offset > 0))
    {
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_OUTOFBOUNDS;
    }
    //Gabriel - 25092015

    if(fseek(fpi->f, offset, position))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_fseek[%d]", HF_ERR_OUTOFBOUNDS);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_OUTOFBOUNDS;
    }

    iRet = ftell(fpi->f);

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_fseek[%d]", iRet);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return iRet;
}

HF_INT32 HF_fs_unlink(const HF_CHAR *filename)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	DIR *dirPtr = NULL;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_unlink :%s", filename);

/*
	if((NULL != filename) && (*filename == '/'))
	{
		filename++;
	}
*/

	if(!checkPath(filename, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_unlink[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	//if the path is a dir
	if(HF_fs_isdir(abPath))
	{
		dirPtr = opendir(abPath);
		if(NULL == dirPtr)
		{
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_unlink[%d]", RF_ERR_DEVICEFAULT);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return RF_ERR_DEVICEFAULT;
		}

		//if the dir is not empty
		if(!isDirEmpty(dirPtr))
		{
			closedir(dirPtr);
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_unlink[%d]", HF_ERR_NOTEMPTY);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return HF_ERR_NOTEMPTY;
		}

		closedir(dirPtr);
	}

	//if the path is a file or empty dir
	//remove() delete file and empty dir
	//unlink() delete only file
	if(remove(abPath))
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOTFOUND);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOTFOUND;
			case EACCES:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOACCESSRIGHT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOACCESSRIGHT;
			case ENOMEM :
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", HF_ERR_NOMEMORY);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_unlink[%d]", RF_ERR_DEVICEFAULT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return RF_ERR_DEVICEFAULT;
		}
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_unlink");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
	return HF_SUCCESS;
}

HF_INT32 HF_fs_chdir(const HF_CHAR *dirname)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_chdir :%s PWD:%s", dirname, (char*)getcwd(NULL, 0));

    memset(abPath, 0, sizeof(abPath));

    // Fabrizio - 20150925: Back to root
    if ( !strcmp(dirname, "/") )
    {
    	stack_clean();
    	stack_init();
    	sprintf(cwd, "/data/app/MAINAPP/root");
        PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_chdir cwd:%s",cwd);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
        return HF_SUCCESS;
    }
    // Fabrizio - 20150925

/*
	if((NULL != dirname) && (*dirname == '/'))
	{
		dirname++;
	}
*/

	if(!checkPath(dirname, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_chdir [errno=%d %s] \n", errno,strerror(errno));
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	//whether the path is a file
    FILE *file = fopen(abPath, "r");
    if (NULL != file)
    {
		fclose(file);
    	if(HF_FALSE == HF_fs_isdir(abPath))
    	{
    		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"richard test HF_fs_isdir HF_FALSE");
    		// Fabrizio@21102015 - Releases the resource
    		pthread_mutex_unlock(&fsMutex);
    		return HF_ERR_ISAFILE;
    	}
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"richard test HF_fs_isdir HF_TRUE");
    }
    else
    {
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_NOTFOUND;
    }

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"chdir [%s]", abPath);
/*
//    if(chdir("/data/app/MAINAPP"))
    if(chdir(abPath))
    {
		switch(errno)
		{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_chdir PWD:%s", getcwd(NULL, NULL));
			case ENOENT:
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_chdir[%d]", HF_ERR_NOTFOUND);
				return HF_ERR_NOTFOUND;
			default:
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_chdir[%d]", RF_ERR_DEVICEFAULT);
				return RF_ERR_DEVICEFAULT;
		}
    }
    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"After chdir [%s]", abPath);
*/
    strcpy(cwd, abPath);

    // Fabrizio - 20150924

    // Fabrizio - 20150924

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_chdir cwd:%s",cwd);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_getcwd(HF_CHAR *dirname)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_getcwd(%s)", dirname);

	if(NULL == dirname)
	{
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return RF_ERR_DEVICEFAULT;
	}

/*    if(!getcwd(dirname, HF_FS_MAX_PATH_LENGTH))
    {
//    	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getcwd[%d]", RF_ERR_DEVICEFAULT);
    	return RF_ERR_DEVICEFAULT;
    }
*/
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"Current cwd     [%s]", cwd);

	strcpy(dirname, cwd);

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getcwd[%s]", dirname);

	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_mkdir(const HF_CHAR *dirname)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_mkdir(%s)", dirname);

	memset(abPath, 0, sizeof(abPath));

/*
	if((NULL != dirname) && (*dirname == '/'))
	{
		dirname++;
	}
*/

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_mkdir");
	if(!checkPath(dirname, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_mkdir[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1.1 HF_fs_mkdir[%s]", dirname);

	// check if the dir is exists
	// [PAXBR] - Replace dirname with abPath
	if ( abPath != NULL )
	{
		if(opendir(abPath))
		{
			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_mkdir[%d]", HF_ERR_ALREADYEXISTS);
			// Fabrizio@21102015 - Releases the resource
			pthread_mutex_unlock(&fsMutex);
			return HF_ERR_ALREADYEXISTS;
		}

		   //TODO what is the mode?
		  //if(mkdir(abPath, S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP|S_IXGRP|S_IROTH|S_IWOTH|S_IXOTH))
		if(mkdir(abPath, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH))
		{
		   PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. mkdir[%d - %s]", errno, strerror(errno));
		   switch(errno)
		   {
			   case ENOSPC:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_mkdir[%d]", HF_ERR_NOSPACE);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOSPACE;
			   default:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_mkdir[%d]", HF_ERR_DEVICEFAULT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_DEVICEFAULT;
		   }
		}
	 }

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_mkdir");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_rename(const HF_CHAR *oldname, const HF_CHAR *newname)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	HF_CHAR  abPathOld[HF_FS_MAX_PATH_LENGTH];
	HF_CHAR  abPathNew[HF_FS_MAX_PATH_LENGTH];

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_rename:%s %s", oldname, newname);

    memset(abPathOld, 0, sizeof(abPathOld));
    memset(abPathNew, 0, sizeof(abPathNew));

/*
	if((NULL != oldname) && (*oldname == '/'))
	{
		oldname++;
	}

	if((NULL != newname) && (*newname == '/'))
	{
		newname++;
	}
*/

	if(!checkPath(oldname, abPathOld) || !checkPath(newname, abPathNew))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_rename[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	//if file or dir is exists
	if(HF_fs_exists(abPathNew))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_rename[%d]", HF_ERR_ALREADYEXISTS);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_ALREADYEXISTS;
	}

    if(rename(abPathOld, abPathNew))
    {
    	switch(errno)
    	{
    		case ENOENT:
    			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOTFOUND);
    			// Fabrizio@21102015 - Releases the resource
    			pthread_mutex_unlock(&fsMutex);
    			return HF_ERR_NOTFOUND;
    		case EACCES:
    			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOACCESSRIGHT);
    			// Fabrizio@21102015 - Releases the resource
    			pthread_mutex_unlock(&fsMutex);
    			return HF_ERR_NOACCESSRIGHT;
    		case ENOSPC:
    			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_NOSPACE);
    			// Fabrizio@21102015 - Releases the resource
    			pthread_mutex_unlock(&fsMutex);
    			return HF_ERR_NOSPACE;
    		default:
    			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_rename[%d]", HF_ERR_DEVICEFAULT);
    			// Fabrizio@21102015 - Releases the resource
    			pthread_mutex_unlock(&fsMutex);
    			return HF_ERR_DEVICEFAULT;
    	}
    }

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_rename");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_freediskspace(HF_VOID)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_unlock(&fsMutex);
	struct statfs diskInfo;

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_freediskspace");

	memset(&diskInfo, 0, sizeof(diskInfo));

	if(statfs("/data", &diskInfo))  // /data/app/MAINAPP
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_rename[%d], str(errno):%s", HF_ERR_DEVICEFAULT, strerror(errno));
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_freediskspace is [%d]",diskInfo.f_bfree*diskInfo.f_bsize);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return (diskInfo.f_bfree*diskInfo.f_bsize);
}

HF_INT32 HF_fs_opendir(const HF_CHAR *dirname, HF_HANDLE_T * hDir)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	DIR *dirPtr = NULL;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_opendir:[%s],[%d]",dirname ,curOpenFileNum);

	if((!dirname) || (!hDir))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_opendir[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}

/*
	if(*dirname == '/')
	{
		dirname++;
	}
*/

	// Jarod@2015/09/15 UPD START
	////if the open dir number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	//if(curOpenFileNum >= MAX_OPEN_FILES)
	//{
	//	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_opendir[HF_ERR_OPENFILES]: [%d >= %d]", curOpenFileNum, MAX_OPEN_FILES);
	//    	return HF_ERR_OPENFILES;
	//}
	//if the open dir number is over MAX_OPEN_FILES, return HF_ERR_OPENFILES
	// Jarod@2015/09/15 UPD END

	if(!checkPath(dirname, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_opendir[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

    dirPtr = opendir(abPath);
	if(NULL == dirPtr)
	{
		switch(errno)
		{
			//no such directory
			case ENOENT:
			case ENOTDIR:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_NOTFOUND);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_NOMEMORY);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_opendir[%d]", HF_ERR_DEVICEFAULT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_DEVICEFAULT;
		}
	}

	//if dir is empty
	if(isDirEmpty(dirPtr))
	{
		//close dir and return
		closedir(dirPtr);
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"5. HF_fs_opendir[%d]", HF_ERR_EMPTYDIR);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_EMPTYDIR;
	}

	//get the dir handle
	*hDir = dirPtr;

	// Jarod@2015/09/15 UPD START
	//curOpenFileNum++;
	// Jarod@2015/09/15 UPD END

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_opendir[%s][%04x]", dirname, hDir);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_readdir(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	off_t offDir = 0;
	struct dirent *entry = NULL;
	int last_errno = -1;
	int i = 0;

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_readdir");

	// Jarod@2015/09/10 UPD START
	OsSleep(50);
	// Jarod@2015/09/10 UPD END

	if((!hDir) || (!filefound))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_readdir[%d]", HF_ERR_DEVICEFAULT);
		offDir = 0;
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}

	offDir = telldir((DIR *)hDir);

	if(isDirEmpty((DIR *)hDir))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_readdir[%d]", HF_ERR_EMPTYDIR);
		offDir = 0;
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_EMPTYDIR;
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. offDir[%d]", offDir);

	seekdir((DIR *)hDir, offDir);

	last_errno = errno;	//to check if the errno is changed

	// Jarod@2015/09/17 ADD START
	for (i=0;i<3;i++)
	{
		entry = readdir((DIR *)hDir);
		if(!entry)
		{
			// Jarod@2015/09/12 UPD START
			//PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_readdir[%s]:[%d]", hDir,HF_ERR_DEVICEFAULT);
			//return HF_ERR_DEVICEFAULT;
			if (last_errno == errno) // if errno no changed, that means the dir is empty
			{
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_readdir EMPTYDIR! errno[%d -- %s]", errno, strerror(errno));
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_EMPTYDIR;
			}
			else
			{
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_readdir FAULT! errno[%d -- %s]", errno, strerror(errno));
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_DEVICEFAULT;
			}
			// Jarod@2015/09/12 UPD END
		}
		else
		{
			// rede don't like the directory as . or .. , so....
			if ( ( strcmp(entry->d_name, ".") == 0 ) || ( strcmp(entry->d_name, "..") == 0))
			{
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"5. entry->d_name[%s]", entry->d_name);
				continue;
			}

			PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"6. HF_fs_readdir SUCCESS! [%s]",entry->d_name);
			break;
		}
	}
	// Jarod@2015/09/17 ADD END

	if(strlen(entry->d_name) > buffSz+1)
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"7. HF_fs_readdir[%d]", HF_ERR_DEVICEFAULT);
		offDir = 0;
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}
	else
	{
		strcpy(filefound, entry->d_name);
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_readdir:[%s]",entry->d_name);
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_INT32 HF_fs_closedir(HF_HANDLE_T dirsearch)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_unlock(&fsMutex);
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_closedir");

    if(!dirsearch)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_closedir[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_DEVICEFAULT;
    }

    if(0 != closedir((DIR *)dirsearch))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_closedir[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_DEVICEFAULT;
    }

    // Jarod@2015/09/15 UPD START
    ////free(dirsearch);
    //curOpenFileNum--;
    // Jarod@2015/09/15 UPD END

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_closedir");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_BOOL HF_fs_isdir(const HF_CHAR *path)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	struct stat dirStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	memset(&dirStat, 0, sizeof(dirStat));

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_isdir:%s", path);

    if(!path)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_isdir[%d]", HF_FALSE);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_FALSE;
    }

/*
	if(*path == '/')
	{
		path++;
	}
*/

	if(!checkPath(path, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_isdir checkPath - error");
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_FALSE;
	}

/*
	if(HF_FALSE == getAbsolutePath(path, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_isdir getAbsolutePath - error");
		return HF_FALSE;
	}
*/

	if(stat(abPath, &dirStat) != 0)
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_isdir[%d], errno:%d", HF_FALSE, errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_FALSE;
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_isdir: %d", S_ISDIR(dirStat.st_mode));
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
	return S_ISDIR(dirStat.st_mode);
}

HF_BOOL HF_fs_isglobal(const HF_CHAR *filename)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------
    // TODO
    // ---------------------------------
    return HF_FALSE;
}

HF_BOOL HF_fs_exists(const HF_CHAR *path)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	struct stat dirStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	memset(&dirStat, 0, sizeof(dirStat));
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_exists:%s", path);

    if(!path)
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_exists[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_DEVICEFAULT;
    }

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_fs_exists[%s]", path);

/*
	if(*path == '/')
	{
		path++;
	}
*/

	if(!checkPath(path, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_exists checkPath - error");
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_FALSE;
	}

/*
	if(HF_FALSE == getAbsolutePath(path, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_exists getAbsolutePath - error");
		return HF_FALSE;
	}
*/

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"abPath[%s]", abPath);
	if(stat(abPath, &dirStat) != 0)   //stat excute success means the path is a dir or file
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_exists[%d], errno:%d", HF_FALSE, errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_FALSE;
	}

	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_exists");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
	return HF_TRUE;
}

HF_INT32 HF_fs_getfsize(const HF_CHAR *filename, HF_UINT32 *fileSz)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	struct stat fileStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	memset(&fileStat, 0, sizeof(fileStat));
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_getfsize:%s", filename);

	if((!filename) || (!fileSz))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getfsize[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}

/*
	if(*filename == '/')
	{
		filename++;
	}
*/

	*fileSz = 0;

	if(!checkPath(filename, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_getfsize[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

    if(HF_fs_isdir(abPath))
    {
    	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getfsize[%d]", HF_ERR_ISADIR);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
    	return HF_ERR_ISADIR;
    }

	if(stat(abPath, &fileStat) != 0)
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_NOTFOUND);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_NOMEMORY);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"4. HF_fs_getfsize[%d]", HF_ERR_DEVICEFAULT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_DEVICEFAULT;
		}
	}

    *fileSz = fileStat.st_size;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getfsize");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS ;
}

HF_INT32 HF_fs_getmtime(const HF_CHAR *filename, HF_INT32 *mTime)
{
	// Fabrizio@21102015 - Locks the resource
	pthread_mutex_trylock(&fsMutex);
	struct stat fileStat;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	memset(abPath, 0, sizeof(abPath));
	memset(&fileStat, 0, sizeof(fileStat));
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_fs_getmtime");

	if((!filename) || (!mTime))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"1. HF_fs_getmtime[%d]", HF_ERR_DEVICEFAULT);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_ERR_DEVICEFAULT;
	}

/*
	if(*filename == '/')
	{
		filename++;
	}
*/

	if(!checkPath(filename, abPath))
	{
		PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"2. HF_fs_getmtime[%d]", HF_errno);
		// Fabrizio@21102015 - Releases the resource
		pthread_mutex_unlock(&fsMutex);
		return HF_errno;
	}

	if(stat(abPath, &fileStat) != 0)
	{
		switch(errno)
		{
			case ENOENT:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_NOTFOUND);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOTFOUND;
			case ENOMEM:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_NOMEMORY);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_NOMEMORY;
			default:
				PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"3. HF_fs_getmtime[%d]", HF_ERR_DEVICEFAULT);
				// Fabrizio@21102015 - Releases the resource
				pthread_mutex_unlock(&fsMutex);
				return HF_ERR_DEVICEFAULT;
		}
	}

    *mTime = fileStat.st_mtim.tv_sec;

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU HF_fs_getmtime");
	// Fabrizio@21102015 - Releases the resource
	pthread_mutex_unlock(&fsMutex);
    return HF_SUCCESS;
}

HF_BOOL RF_fs_existsexternal(const HF_CHAR *path, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_existsexternal(%s, %s)", path, volume);
    return HF_FALSE;
}

HF_INT32 RF_fs_chdirexternal(const HF_CHAR *dirname, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_chdirexternal(%s, %s)", dirname, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_opendirexternal(const HF_CHAR *dirname, HF_HANDLE_T *hDir, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_opendirexternal(%s, %s)", dirname, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_readdirexternal(HF_HANDLE_T hDir, HF_CHAR *filefound, HF_UINT32 buffSz, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_readdirexternal(%s, %d, %s)", filefound, buffSz, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_BOOL RF_fs_isdirexternal(const HF_CHAR *path, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_isdirexternal(%s, %s)", path, volume);
    return HF_FALSE;
}

HF_INT32 RF_fs_unlinkexternal(const HF_CHAR *filename, const HF_CHAR *volume)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_unlinkexternal(%s, %s)", filename, volume);
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 RF_fs_fopenexternalH(const HF_CHAR *filename, const HF_CHAR *accessMode, const HF_CHAR *volume, HF_FILE_T **output)
{
	PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_fs_fopenexternalH(%s, %s, %s)", filename, accessMode, volume);
    return HF_NULL;
}

//TODO Below is the function define by richard
/************add by richard 2015-05-15 file limit***********/
// Jarod@2015/09/18 ADD START
/*
static HF_VOID nodeLogView(FILE_NODE *viewNode)
{
	FILE_NODE *tempNode = NULL;
	HF_INT32 listLen = 0;

	if((NULL == viewNode))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"viewNode NULL");
		return ;
	}

	tempNode = viewNode;
	while(NULL != tempNode)
	{
		listLen++;
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"viewNode fileName:%s pFile:%p rwFlag:%d CurfNum:%d listLen:%d",
			tempNode->fileInfo.fileName, tempNode->fileInfo.pFile, tempNode->fileInfo.rwFlag, curOpenFileNum, listLen);

		tempNode = tempNode->next;
	}
}

//change global variable - nodeHeader
static HF_VOID changeFileListHeader(FILE_NODE *newNode)
{
	nodeHeader = newNode;
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"change ->nodeHeader:%p ",nodeHeader);
}

static HF_BOOL nodeInsert(FILE_NODE *header, FILE_NODE *insertNode)
{
	FILE_NODE *tempNode = NULL;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeInsert");

	if(NULL == insertNode)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeInsert[%d]", HF_FALSE);
		return HF_FALSE;
	}

	//insert the first file node
	if(NULL == header)
	{
		changeFileListHeader(insertNode);
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeInsert[%d]", HF_TRUE);
		return HF_TRUE;
	}

	tempNode = header;
	while(NULL != tempNode->next)
	{
		tempNode = tempNode->next;
	}

	tempNode->next = insertNode;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeInsert");
	return HF_TRUE;
}


static HF_BOOL nodeDelete(FILE_NODE *header, FILE *fd)
{
	FILE_NODE *tempNode = NULL;
	FILE_NODE *tempNode2 = NULL;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeDelete");

	if((NULL == header) || (NULL == fd))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeDelete[%d]", HF_FALSE);
		return HF_FALSE;
	}

	tempNode = header;

	//if the delete node is at the first of the list
	if(tempNode->fileInfo.pFile == fd)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ANTES DELETE %p %p",tempNode, tempNode->next);
		tempNode2 = tempNode->next;
		free(tempNode->fileInfo.fileName);
		free(tempNode);

		//change nodeHeader
		changeFileListHeader(tempNode2);

		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"DELETE PRIMEIRO N�� %p",tempNode);
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeDelete[%d]", HF_TRUE);

		return HF_TRUE;
	}

	//delete a node in the mid or end of the list
	while(NULL != tempNode->next)
	{
		if(tempNode->next->fileInfo.pFile == fd)
		{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ANTES DELETE %p %p %p",tempNode, tempNode->next, tempNode->next->next);
			tempNode2 = tempNode->next->next;

			//free node memory
			free(tempNode->next->fileInfo.fileName);
			free(tempNode->next);

			tempNode->next = tempNode2;
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"AP��S DELETE %p %p",tempNode, tempNode->next);

			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. SAIU nodeDelete[%d]", HF_TRUE);

			return HF_TRUE;
		}

		tempNode = tempNode->next;
	}

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeDelete(%d)", HF_FALSE);

	return HF_FALSE;
}

static HF_BOOL nodeSearch(FILE_NODE *header, const HF_CHAR *filename)
{
	FILE_NODE *tempNode = NULL;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU nodeSearch");

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. nodeSearch[%s]", filename);

	if((NULL == filename) || (NULL == header))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeSearch[%d]", HF_FALSE);
		return HF_FALSE;
	}

	tempNode = header;
	while(NULL != tempNode)
	{
		if(0 == strcmp(tempNode->fileInfo.fileName, filename) && (1 == tempNode->fileInfo.rwFlag))
		{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. nodeSearch[%d]", HF_TRUE);
			return HF_TRUE;
		}

		tempNode = tempNode->next;
	}

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU nodeSearch[%d]", HF_FALSE);
	return HF_FALSE;
}


static FILE_NODE *CreateNewNode(const HF_CHAR *filename, const HF_CHAR *accessmode, FILE *fd)
{
	FILE_NODE *fileNode = NULL;

	//get file infomation

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU CreateNewNode");

	if((NULL == filename) || (NULL == accessmode) || (NULL == fd))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. CreateNewNode[%d]", HF_NULL);
		return HF_NULL;
	}

	//malloc filenode
	fileNode = (FILE_NODE *)malloc(sizeof(FILE_NODE));
	if(NULL == fileNode)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc FALHOU");
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. CreateNewNode[%d]", HF_NULL);
		return NULL;
	}
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc fileNode:%p LEN:%d", fileNode, sizeof(FILE_NODE));

	//malloc filename
	fileNode->fileInfo.fileName = (HF_CHAR *)malloc((strlen(filename)+1));
	if(NULL == fileNode)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. CreateNewNode[%d]", HF_NULL);
		return NULL;
	}
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"malloc fileName:%p LEN:%d",fileNode->fileInfo.fileName, strlen(filename)+1);

	strcpy(fileNode->fileInfo.fileName, filename);
	fileNode->fileInfo.pFile = fd;
	fileNode->next = NULL;
	if(0 == strcmp(accessmode, "r"))
	{
		fileNode->fileInfo.rwFlag = 0;
	}
	else
	{
		fileNode->fileInfo.rwFlag = 1;
	}
	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"create node fileName:%s pFile:%p rwFlag:%d", fileNode->fileInfo.fileName, fileNode->fileInfo.pFile, fileNode->fileInfo.rwFlag);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU CreateNewNode");

	return fileNode;
}

static HF_BOOL listInsert(const HF_CHAR *filename,  const HF_CHAR *accessmode, HFI_FILE_T *fp)
{
	FILE_NODE *fileNode = NULL;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU listInsert");

	if((NULL == filename) || (NULL == accessmode) || (NULL == fp))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. listInsert[%d]", HF_NULL);
		return HF_NULL;
	}

    //create filenode for the open file
	fileNode = CreateNewNode(filename, accessmode, fp->f);
	if(NULL == fileNode)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. listInsert[%d]", HF_ERR_NOMEMORY);
		HF_errno = HF_ERR_NOMEMORY;
		return HF_FALSE;
	}

	if(HF_TRUE == nodeInsert(nodeHeader, fileNode))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"INSER??O COM SUCESSO  pFile:%p ",fileNode->fileInfo.pFile);
		// Jarod@2015/09/15 UPD START
		//curOpenFileNum++;
		// Jarod@2015/09/15 UPD END
	}

	//show current file node
	nodeLogView(nodeHeader);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU listInsert");
	return HF_TRUE;
}
*/
// Jarod@2015/09/18 ADD END

/***********************************************************/
static HF_VOID stack_init()
{
	stackPosition = 3;
	memset(path_stack, 0, sizeof(path_stack));
	strcpy(path_stack[0], "data");
	strcpy(path_stack[1], "app");
	strcpy(path_stack[2], "MAINAPP");
	strcpy(path_stack[3], "root");
}

static HF_VOID stack_clean()
{
	stackPosition = -1;
	memset(path_stack, 0, sizeof(path_stack));
}

static HF_BOOL stack_push(HF_CHAR *dir)
{
	stackPosition++;
	if((stackPosition >= 0) && (stackPosition < STACK_MAX_DEPTH))
	{
		strcpy(path_stack[stackPosition], dir);
		return HF_TRUE;
	}
	else
	{
		return HF_FALSE;
	}
}

static HF_CHAR* stack_pop()
{
	if((stackPosition < 0) || (stackPosition >= STACK_MAX_DEPTH))
	{
		return NULL;
	}
	else
	{
		return path_stack[stackPosition--];
	}
}


/*
static HF_BOOL getAbsolutePath(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen)
{
	int i = 0;
	HF_CHAR * tok = NULL;
	HF_CHAR * abPath = NULL;

	HF_CHAR temp[HF_FS_MAX_PATH_LENGTH];


	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU getAbsolutePath");

	stack_init();
	memset(temp, 0 , maxLen);

	if((NULL == filename) || (NULL == output))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. getAbsolutePath[%d]", HF_FALSE);
		return HF_FALSE;
	}

	if(*filename == '/')
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"richard test filename [%s]", filename);
		filename++;
	}

	// Jarod@2015/09/09 UPD START
	//if(strncmp(filename, "data/app/MAINAPP", 16) == 0)
	if(strncmp(filename, "data/app/MAINAPP/root", 21) == 0)
	// Jarod@2015/09/09 UPD END
	{
		*output++ = '/';
		strcpy(output, filename);
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"richard test output [%s]", output);
		return HF_TRUE;
	}


	// Jarod@2015/09/16 ADD START
	if (strlen(filename)>HF_FS_MAX_PATH_LENGTH)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"path is > HF_FS_MAX_PATH_LENGTH");
		return HF_FALSE;
	}
	// Jarod@2015/09/16 ADD END

	strcpy(temp, filename);
	tok = strtok(temp, "/");
	while (tok != HF_NULL)
	{
		if (strcmp(tok, "..") == 0)
		{
			if (NULL == stack_pop())
			{
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. getAbsolutePath stack_pop NULL");
				stack_clean();
				return HF_FALSE;
			}
		}
		else if (HF_strcmp(tok, ".") != 0)
		{
			if(strlen(tok) > HF_FS_MAX_FILE_NAME)
			{
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. getAbsolutePath strlen(tok) > HF_FS_MAX_FILE_NAME");
				stack_clean();
				return HF_FALSE;
			}

			stack_push(tok);
		}
		tok = strtok(NULL, "/");
	}

	memset(temp, 0 , maxLen);
	abPath = temp;
	for(i=0; i<=stackPosition; i++)
	{
		*abPath++ = '/';
		strcpy(abPath, path_stack[i]);
		abPath += strlen(path_stack[i]);
	}

	strcpy(output, temp);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU getAbsolutePath, output:%s", output);
	stack_clean();
	return HF_TRUE;
}
*/

static HF_BOOL getAbsolutePath(const HF_CHAR *filename, HF_CHAR *output, HF_INT32 maxLen)
{
	int i = 0;
	HF_CHAR * tok = NULL;
	HF_CHAR * abPath = NULL;

	HF_CHAR temp[HF_FS_MAX_PATH_LENGTH];

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU getAbsolutePath");

	memset(temp, 0 , maxLen);

	if((NULL == filename) || (NULL == output))
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. getAbsolutePath[%d]", HF_FALSE);
		return HF_FALSE;
	}

	// Jarod@2015/09/09 UPD START
	//if(strncmp(filename, "data/app/MAINAPP", 16) == 0)
	if(strncmp(filename, "/data/app/MAINAPP/root", 22) == 0)
	// Jarod@2015/09/09 UPD END
	{
		*output++ = '/';
		strcpy(output, filename);
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"Output is root file [%s]", output);
		return HF_TRUE;
	}

	// Jarod@2015/09/16 ADD START
	if (strlen(filename)>HF_FS_MAX_PATH_LENGTH)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"path is > HF_FS_MAX_PATH_LENGTH");
		return HF_FALSE;
	}
	// Jarod@2015/09/16 ADD END

	if(*filename == '/')
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"filename checked [%s]", filename);
		filename++;
	}
	else
	{
		// Fabrizio - 20150924: Concatenates the file with the cwd
		sprintf(output, "%s/%s", cwd, filename);
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"Output is cwd + file/dir [%s]", output);
		return HF_TRUE;
	}

	stack_init();
	strcpy(temp, filename);
	tok = strtok(temp, "/");
	while (tok != HF_NULL)
	{
		if (strcmp(tok, "..") == 0)
		{
			if (NULL == stack_pop())
			{
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. getAbsolutePath stack_pop NULL");
				stack_clean();
				return HF_FALSE;
			}
		}
		else if (HF_strcmp(tok, ".") != 0)
		{
			if(strlen(tok) > HF_FS_MAX_FILE_NAME)
			{
				PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. getAbsolutePath strlen(tok) > HF_FS_MAX_FILE_NAME");
				stack_clean();
				return HF_FALSE;
			}

			stack_push(tok);
		}
		tok = strtok(NULL, "/");
	}

	memset(temp, 0 , maxLen);
	abPath = temp;
	for(i=0; i<=stackPosition; i++)
	{
		*abPath++ = '/';
		strcpy(abPath, path_stack[i]);
		abPath += strlen(path_stack[i]);
	}

	strcpy(output, temp);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU getAbsolutePath, output:%s", output);
	stack_clean();
	return HF_TRUE;
}

static HF_BOOL checkPath(const HF_CHAR *filename, HF_CHAR *szAbPath)
{
	HF_INT32 dirDepth = 0;
	HF_INT32 pathLen = 0;
	HF_INT32 dirOrFileLen = 0;
	HF_INT32 extendLen = 0;
	HF_CHAR  abPath[HF_FS_MAX_PATH_LENGTH];

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU checkPath");

	memset(abPath, 0, sizeof(abPath));

	// [PAXBR] - Initialize variable
	if ( szAbPath != NULL )
	{
		memset(szAbPath, 0, sizeof(szAbPath));
	}

	if(!filename)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. checkPath[%d]", RF_FALSE);
		HF_errno = RF_ERR_DEVICEFAULT;
		return RF_FALSE;
	}

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. checkPath[%s]", filename);

	//exclude the './' and "../"
	if(HF_FALSE == getAbsolutePath(filename, abPath, HF_FS_MAX_PATH_LENGTH))
	{
		HF_errno = HF_ERR_MAXLEN;
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"3. checkPath[%d]", RF_FALSE);
		return RF_FALSE;
	}

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"4. checkPath[%s, %s]", filename, abPath);

	//check the path word one by one
    while(*(abPath+pathLen) != '\0')
    {
    	if(*(abPath+pathLen) == '/')
    	{
    		dirOrFileLen = 0;
    		pathLen++;
    		dirDepth++;
    		continue;
    	}

    	//check first word of dir or file name
    	if(0 == dirOrFileLen)
    	{
    		if((*(abPath+pathLen)>='a' && *(abPath+pathLen) <='z') ||
    		   (*(abPath+pathLen)>='A' && *(abPath+pathLen) <='Z'))
    		{
    			dirOrFileLen++;
    			pathLen++;
    		}
    		else
    		{
    			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"5. checkPath[%s, %s]", filename, abPath);
    			HF_errno = HF_ERR_PATHERR;
    			return RF_FALSE;
    		}
    	}
    	else
    	{
    		if('.' == *(abPath+pathLen) && !extendLen)
    		{
    			extendLen++;
    			pathLen++;
    			dirOrFileLen++;
    		}
    		else
    		{
    			if((*(abPath+pathLen)>='a' && *(abPath+pathLen) <='z') ||
    			   (*(abPath+pathLen)>='A' && *(abPath+pathLen) <='Z') ||
    			   (*(abPath+pathLen)>='0' && *(abPath+pathLen) <='9') || *(abPath+pathLen) == '_')
    			{
    				pathLen++;
    				dirOrFileLen++;
    				if(extendLen) extendLen++;
    			}
    			else
    			{
        			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"6. checkPath[%s, %s]", filename, abPath);

    				HF_errno = HF_ERR_PATHERR;
    				return RF_FALSE;
    			}
    		}
    	}

    	if((dirOrFileLen >= HF_FS_MAX_FILE_NAME) || (pathLen >= HF_FS_MAX_PATH_LENGTH))
    	{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"7. checkPath[%s, %s]", filename, abPath);

        	HF_errno = HF_ERR_MAXLEN;
        	return RF_FALSE;
    	}

    	if(dirDepth > HF_FS_MAX_DIR_DEPTH)
    	{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"8. checkPath[%s, %s]", filename, abPath);

    		HF_errno = HF_ERR_PATHTOODEEP;
    		return RF_FALSE;
    	}

    	if(extendLen > 4)
    	{
			PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"9. checkPath[%s, %s]", filename, abPath);

    		HF_errno = HF_ERR_PATHERR;
    		return RF_FALSE;
    	}
    }

    // [PAXBR] - Replace the filename with the absolute path
    if ( szAbPath != NULL )
    {
    	strcpy(szAbPath, abPath);
    }

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU checkPath");

    return RF_TRUE;
}

static HF_BOOL isDirEmpty(DIR *dirPtr)
{
	HF_INT32 elemNum = 0;

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU isDirEmpty");

	if(!dirPtr)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"1. isDirEmpty[%d]", HF_FALSE);
		return HF_FALSE;
	}

	//let the pointer point to the begin position
	rewinddir(dirPtr);


	//check whether the dir is empty
	while(readdir(dirPtr))
	{
		elemNum++;
	}

	if(elemNum == 2)
	{
		PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"2. isDirEmpty[%d]", HF_TRUE);
		return HF_TRUE;
	}

	//let the pointer point to the begin position
	rewinddir(dirPtr);

	PaxLog_fs(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU isDirEmpty[%d]", HF_FALSE);
	return HF_FALSE;
}

// 20150928 - Fabrizio : Files open control
static HF_INT32 canFileBeOpened(const HF_CHAR *filename, const HF_CHAR *accessmode)
{
	int iListIndex;
	HF_CHAR szAccessModeCurrentFile[ACCESS_MODE_LENGTH + 1];
	HF_INT32 iRet = HF_SUCCESS;

	// Check if the file counter has reached the maximum value
	if ( iOpenFilesCounter >= MAX_OPEN_FILES )
	{
		PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__,__LINE__,"MAX_OPEN_FILES reached");
		return HF_ERR_OPENFILES;
	}

    PaxLog_fs_CA(LOG_DEBUG,__FUNCTION__,__LINE__,"canFileBeOpened(%s, %s)", filename, accessmode);

	// Sweeps the file list, to see if the file is already opened, and its access mode
	for ( iListIndex = 0; iListIndex < MAX_OPEN_FILES; iListIndex++ )
	{
		// The list may contain NULL values in the middle
		if ( openFilesList[iListIndex].fileHandle <= 0 )
			continue;

		if ( !strcmp(openFilesList[iListIndex].szFileName, filename) )
		{
			strcpy(szAccessModeCurrentFile, openFilesList[iListIndex].szAccessMode);
			if ( !strcmp(szAccessModeCurrentFile, "r") )
			{
				PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__,__LINE__,"This occurrence [%s - %s] is in reading mode access", filename, accessmode);
				continue;
			}

			if ( !strcmp(szAccessModeCurrentFile, "w")  || !strcmp(szAccessModeCurrentFile, "w+") ||
				 !strcmp(szAccessModeCurrentFile, "r+") || !strcmp(szAccessModeCurrentFile, "rw") )
			{
				if ( !strcmp(accessmode, "r") )
				{
					PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__,__LINE__,"File [%s - %s] can be opened", filename, accessmode);
					return HF_SUCCESS;
				}
				else
				{
					PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__,__LINE__,"File [%s - %s] cannot be opened", filename, accessmode);
					return HF_ERR_ISOPEN;
				}
			}
		}
	}

	return iRet;
}

static HF_VOID insertNewOpenFile(HF_FILE_T *fileHandle, const HF_CHAR *filename, const HF_CHAR *accessmode)
{
	int iListIndex;

	// Sweeps the file list
	for ( iListIndex = 0; iListIndex < MAX_OPEN_FILES; iListIndex++ )
	{
		// The current index may contain NULL values, so it's a place to insert the new open file data
		if ( openFilesList[iListIndex].fileHandle <= 0 )
		{
			openFilesList[iListIndex].fileHandle = fileHandle;
			strcpy(openFilesList[iListIndex].szFileName, filename);
			strcpy(openFilesList[iListIndex].szAccessMode, accessmode);
			iOpenFilesCounter++;
			break;
		}
	}
}

static HF_VOID fileClosed(HF_FILE_T *fileHandle)
{
	int iListIndex;

	// Sweeps the file list
	for ( iListIndex = 0; iListIndex < MAX_OPEN_FILES; iListIndex++ )
	{
		// The current index may contain NULL values
		if ( openFilesList[iListIndex].fileHandle <= 0 )
		{
			continue;
		}

		// Remove the file from the list.
		if ( openFilesList[iListIndex].fileHandle == fileHandle )
		{
			memset(openFilesList[iListIndex].szFileName  , 0, sizeof(openFilesList[iListIndex].szFileName));
			memset(openFilesList[iListIndex].szAccessMode, 0, sizeof(openFilesList[iListIndex].szAccessMode));
			openFilesList[iListIndex].fileHandle = NULL;
			iOpenFilesCounter--;
			break;
		}
	}
}

static HF_VOID printOpenFilesList(HF_VOID)
{
#ifdef fs_TFS_Log
	int iListIndex;

	// Sweeps the file list
	for ( iListIndex = 0; iListIndex < MAX_OPEN_FILES; iListIndex++ )
	{
		// The current index may contain NULL values
		if ( openFilesList[iListIndex].fileHandle <= 0 )
		{
			continue;
		}

		// Print each valid entry of the list.
		PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__, __LINE__, "openFilesList[%d].szFileName   = [0x%04x]", iListIndex, (HF_INT32)(openFilesList[iListIndex].fileHandle));
		PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__, __LINE__, "openFilesList[%d].szFileName   = [%s]", iListIndex, openFilesList[iListIndex].szFileName);
		PaxLog_fs_TFS(LOG_DEBUG,__FUNCTION__, __LINE__, "openFilesList[%d].szAccessMode = [%s]", iListIndex, openFilesList[iListIndex].szAccessMode);
	}
#endif
}

