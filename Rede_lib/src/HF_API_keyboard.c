#include "HF_API.h"
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include "osal.h"
#include <xui.h>

#define KEY_NOT_SUPPORT "not support"
#define	BUZZER_FREQ_DEV "/sys/devices/platform/keypad/buzzer_keypad_freq"
#define	BUZZER_DURATION_DEV "/sys/devices/platform/keypad/buzzer_keypad_duration"
#define	BUZZER_TRIGGER_DEV "/sys/devices/platform/keypad/buzzer_keypad_enable"

static RF_KEYBOARD_CODES_T s_glKeysWindows[] = {
		{HF_KEY_ZERO, "XUI_KEY0"},
		{HF_KEY_ONE, "XUI_KEY1"},
		{HF_KEY_TWO, "XUI_KEY2"},
		{HF_KEY_THREE, "XUI_KEY3"},
		{HF_KEY_FOUR, "XUI_KEY4"},
		{HF_KEY_FIVE, "XUI_KEY5"},
		{HF_KEY_SIX, "XUI_KEY6"},
		{HF_KEY_SEVEN, "XUI_KEY7"},
		{HF_KEY_EIGHT, "XUI_KEY8"},
		{HF_KEY_NINE, "XUI_KEY9"},
		{HF_KEY_ENTER, "XUI_KEYENTER"},
		{HF_KEY_CANCEL, "XUI_KEYCANCEL"},
		// Jarod@2015/09/14 UPD START
		//{HF_KEY_HASH, KEY_NOT_SUPPORT},
		{HF_KEY_HASH, "XUI_KEYFUNC"},
		// Jarod@2015/09/14 UPD END
		{HF_KEY_CLEAR, "XUI_KEYCLEAR"},
		{HF_KEY_STAR, KEY_NOT_SUPPORT},
		{HF_KEY_DOWN, KEY_NOT_SUPPORT},
		{HF_KEY_UP, KEY_NOT_SUPPORT},
		{HF_KEY_MENU, KEY_NOT_SUPPORT},
		{HF_KEY_LEFT, KEY_NOT_SUPPORT},
		{HF_KEY_RIGHT, KEY_NOT_SUPPORT},
		{HF_KEY_F1, "XUI_KEYF1"},
		{HF_KEY_F2, "XUI_KEYF2"},
		{0}
};

RF_KEYBOARD_CODES_T *RF_keyboard_getkeyset(void)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	return((RF_KEYBOARD_CODES_T *)&s_glKeysWindows);
}

const HF_CHAR *HF_keyboard_getkeylabel(HF_UINT32 key)
{
	RF_KEYBOARD_CODES_T * pstLocalKeys = NULL;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

    if(((key-1) >= HF_MAX_API_KEY_VALUE) || (0 == key)) //HF_KEYS_MAX isn't equal to HF_MAX_API_KEY_VALUE.
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__, "HF_ERR_KEYNOTSUPPORTED \n");//The global variable HF_errno was discontinued
        return RF_NULL;
    }

    pstLocalKeys = RF_keyboard_getkeyset();

    if(strncmp(pstLocalKeys[key-1].label,KEY_NOT_SUPPORT,sizeof(KEY_NOT_SUPPORT)) == 0)
    {
    	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_ERR_KEYNOTSUPPORTED \n");
    	return RF_NULL;
    }

    return pstLocalKeys[key-1].label;
}

static int getRedeKeyCode(int iXuiKey)
{
	int iKeyValue = -1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	switch(iXuiKey)
	{
		case XUI_KEY0:
			iKeyValue = HF_KEY_ZERO;
			break;

		case XUI_KEY1:
			iKeyValue = HF_KEY_ONE;
			break;

		case XUI_KEY2:
			iKeyValue = HF_KEY_TWO;
			break;

		case XUI_KEY3:
			iKeyValue = HF_KEY_THREE;
			break;

		case XUI_KEY4:
			iKeyValue = HF_KEY_FOUR;
			break;

		case XUI_KEY5:
			iKeyValue = HF_KEY_FIVE;
			break;

		case XUI_KEY6:
			iKeyValue = HF_KEY_SIX;
			break;

		case XUI_KEY7:
			iKeyValue = HF_KEY_SEVEN;
			break;

		case XUI_KEY8:
			iKeyValue = HF_KEY_EIGHT;
			break;

		case XUI_KEY9:
			iKeyValue = HF_KEY_NINE;
			break;

		case XUI_KEYCANCEL:
			iKeyValue = HF_KEY_CANCEL;
			break;

		case XUI_KEYCLEAR:
			iKeyValue = HF_KEY_CLEAR;
			break;

		case XUI_KEYENTER:
			iKeyValue = HF_KEY_ENTER;
			break;

		case XUI_KEYF1:
			iKeyValue = HF_KEY_F1;
			break;

		case XUI_KEYF2:
			iKeyValue = HF_KEY_F2;
			break;

		// Jarod@2015/09/14 UPD START
		////case XUI_KEYFUNC:
		//	//break;
		case XUI_KEYFUNC:
			iKeyValue = HF_KEY_HASH;
			break;
		// Jarod@2015/09/14 UPD END

		default://don't need to handle.
			break;
	}

	return iKeyValue;
}

HF_INT32 HF_keyboard_getkeystroke(HF_INT32 timeout)//timeout unit:seconds
{
	// Jarod@2015/09/01 UPD START
	HF_INT32 iRet = 0;
	HF_INT32 iKey = -1;
	HF_INT32 iLeftTime = -1;
	// Jarod@2015/09/01 UPD END
	ST_TIMER stTimer = {0};

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"timeout:[%d]",timeout);

	if(timeout < 0)
	{
		while(1)
		{
			if(XuiHasKey() == 1)
			{
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1.XuiGetKey[%d]",iKey);
				iKey = getRedeKeyCode(XuiGetKey());
				return iKey;
			}
		}

		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_DEVICEFAULT timeout < 0");
		return RF_ERR_DEVICEFAULT;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"timeout>0");

	memset(&stTimer,0,sizeof(ST_TIMER));
	iRet = OsTimerSet(&stTimer, 1000*timeout);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_RESOURCEALLOC:[%d]",iRet);
	if(RET_OK != iRet)//s->ms
	{
		return RF_ERR_RESOURCEALLOC;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"start loop");

	while(1)
	{
		iLeftTime = OsTimerCheck(&stTimer);

		//PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"OsTimerCheck iLeftTime:[%d]",iLeftTime);

		if(iLeftTime > 0)
		{
			//PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"iLeftTime>0");
			if(XuiHasKey() == 1)
			{
				iKey = getRedeKeyCode(XuiGetKey());
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2.XuiGetKey[%d]",iKey);
				return iKey;
			}
		}
		else if(0 == iLeftTime)
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"iLeftTime==0");
			if(XuiHasKey() == 1)
			{
				iKey = getRedeKeyCode(XuiGetKey());
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3.XuiGetKey[%d]",iKey);
				return iKey;
			}
			else
			{
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_TIMEOUT");
				return RF_ERR_TIMEOUT;
			}
		}
		else
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_DEVICEFAULT");
			return RF_ERR_DEVICEFAULT;
		}
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"RF_ERR_DEVICEFAULT END");

	return RF_ERR_DEVICEFAULT;
}

HF_INT32 HF_keyboard_getkeystroke_nobuffer(HF_INT32 timeout)
{
	int iRet = -1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	XuiClearKey();

	iRet = HF_keyboard_getkeystroke(timeout);

	return iRet;
}

HF_INT32 HF_keyboard_buzzer(HF_UINT32 length, HF_UINT32 frequency)
{
	int iToneType = 1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//OsLogSetTag("buzzer  danielle");
	//PaxLog_Keyboard(LOG_INFO, "length:%d frequency:%d \n",length,frequency);

	//refer BeepFreq[] = { 0, 1680, 1850, 2020, 2130, 2380, 2700, 2750 };
	
	if(frequency <= HF_BUZZER_LOW)
	{
		iToneType = 1;
	}
	else if(frequency <=  HF_BUZZER_MIDTONE)
	{
		iToneType = 3;
	}
	else if(frequency <=  HF_BUZZER_DEFAULT)
	{
		iToneType = 4;
	}
	else
	{
		iToneType = 7;
	}
	
	OsBeep(iToneType, length);
	
    return RF_SUCCESS;

	/*
	int iBuzzerFreqFd = -1;
	int iBuzzerDurFd = -1;
	int iRetFreq = -1;
	int iRetDur = -1;
	char cFreq[6]={0};
	char cDuration[6]={0};

	PaxLog_KeyboardSetTag("buzzer  danielle");
	PaxLog_Keyboard(LOG_INFO, "First length:%d frequency:%d \n",length,frequency);
	
	if(HF_BUZZER_LOW  == frequency)
	{
		frequency = 1680;
	}
	else if(HF_BUZZER_MIDTONE  == frequency)
	{
		frequency = 2020; 
	}
	else if(HF_BUZZER_HIGH  == frequency)
	{
		frequency = 2750 ;
	}
	else 
	{
		frequency = 2380;
	}
	
	iBuzzerFreqFd = open(BUZZER_FREQ_DEV,O_RDWR|O_TRUNC);
	iBuzzerDurFd = open(BUZZER_DURATION_DEV,O_RDWR|O_TRUNC);

	if((iBuzzerFreqFd < 0) || (iBuzzerDurFd < 0))
	{
		close(iBuzzerFreqFd);
		close(iBuzzerDurFd);

		return RF_ERR_DEVICEFAULT;
	}

	//condition judge is based on input param of Prolin api OsBeep.
	if(length < 10)
	{
		length = 10;
	}
	else if(length > 10000)
	{
		length = 10000;
	}

	//ears can only hear between 20 and 20000.
	if(frequency < 20)
	{
		frequency = 20;
	}
	else if(frequency > 20000)
	{
		frequency = 20000;
	}

	memset(cFreq,0,sizeof(cFreq));
	memset(cDuration,0,sizeof(cDuration));

	snprintf(cDuration,sizeof(cDuration),"%d",length);
	snprintf(cFreq,sizeof(cFreq),"%d",frequency);

	iRetFreq = write(iBuzzerFreqFd,cFreq,sizeof(cFreq));
	iRetDur = write(iBuzzerDurFd,cDuration,sizeof(cDuration));

	if((iRetFreq < 0) || (iRetDur < 0))
	{
		close(iBuzzerFreqFd);
		close(iBuzzerDurFd);
		
		return RF_ERR_DEVICEFAULT;
	}

	close(iBuzzerFreqFd);
	close(iBuzzerDurFd);

	PaxLog_Keyboard(LOG_INFO, "Last length:%d frequency:%d \n",length,frequency);

	HF_keyboard_setBeepOn(1);//danielle 8.11 TODO

      return RF_SUCCESS;
	*/ 
}

HF_INT32 HF_keyboard_setBeepOn(HF_BOOL value)
{
	int iBuzzerTriggerFd = -1;
	int iRet = -1;
	char cTmp[2]={0};

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	iBuzzerTriggerFd = open(BUZZER_TRIGGER_DEV,O_WRONLY|O_TRUNC);
	if(iBuzzerTriggerFd < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

	if(value == 1)
	{
		snprintf(cTmp,sizeof(cTmp),"%d",1);
	}

	iRet = write(iBuzzerTriggerFd,cTmp,sizeof(cTmp));

	close(iBuzzerTriggerFd);

	if(iRet < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

    return RF_SUCCESS;
}

HF_BOOL  HF_keyboard_getBeepOn(HF_VOID)
{
	int iBuzzerTriggerFd = -1;
	int iRet = -1;
	char cTmp[2]={0};

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	iBuzzerTriggerFd = open(BUZZER_TRIGGER_DEV,O_RDONLY);
	if(iBuzzerTriggerFd < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

	iRet = read(iBuzzerTriggerFd,cTmp,1);

	close(iBuzzerTriggerFd);

	if(iRet < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

	if(strcmp(cTmp,"1") == 0)
	{
		return RF_TRUE;
	}

    return RF_FALSE;
}

/*
 * USB functions was created to support a keyboard connection to the USB port.
 * As the D200 doesn't have this option, we don't implement these functions.
 */
HF_INT32 HF_keyboard_usb_open(HF_CHAR *keymapstr)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 HF_keyboard_usb_close()
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 HF_keyboard_usb_getkeystroke(HF_INT32 timeout)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_ERR_DEVICEFAULT;
}

HF_INT32 HF_keyboard_univ_getkeystroke(HF_INT32 timeout)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_ERR_DEVICEFAULT;
}
