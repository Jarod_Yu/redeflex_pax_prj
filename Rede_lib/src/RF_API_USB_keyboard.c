#ifndef _RF_API_USB_KEYBOARD_C_
#define _RF_API_USB_KEYBOARD_C_
/*
Copyright 2015 EBICS
*************************************************************
Nome     : RF_API_USB_keyboard.c
Descri玢o: Fun珲es de teclado USB
Autor    : 
Login    : 
Data     : 
Empresa  : 
*************************************************************/
/*-----------------------------------------------------------------------------------------------*/
/*                                     Includes                                                  */
/*-----------------------------------------------------------------------------------------------*/
#include "HF_API.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------------------------*/
/*                                    Macros/Defines                                             */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Estruturas/Typedefs                                        */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Enumeracoes                                                */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Unioes                                                     */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Globais                                          */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Variaveis Locais                                           */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------*/
/*                                    Funcoes                                                    */
/*-----------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------
Autor,Empresa:	
Funcao		:	RF_USB_keyboard_getkeystroke()
Descri玢o	:	Espera e retorna o c骴igo da tecla pressionada no teclado pelo usu醨io ou um
				c骴igo de erro (podendo indicar timeout).
				O valor retornado (em caso de sucesso) � o byte capturado no teclado (o
				mapeamento desta byte em c骴igo de tecla ser� realizado pelo RedeFlex).
				A fun玢o deve utilizar de "bufferiza玢o" para captura teclas digitas pelo usu醨io
				antes da chamada da fun玢o
Argumentos	:	Timeout	-	tempo m醲imo de espera pela tecla em segundos.
				Um valor negativo indica espera indefinida.
				O valor zero indica retornar imediatamente, indicando TIMEOUT caso o usu醨io n鉶
				tenha digitado nenhuma tecla anteriormente.
Retorno		:	O byte capturado do teclado em caso de sucesso (um valor n鉶 negativo). Em caso
				de falha, um dos seguintes valores:
					RF_ERR_RESOURCEALLOC	-	se o recurso compartilhado n鉶 p鬱e atender �
												requisi玢o (ex.: n鉶 p鬱e alocar um timer)
					RF_ERR_TIMEOUT			-	se tiver passado o tempo definido pelo par鈓etro
												'timeout' e nenhuma tecla tiver sido pressionada
					RF_ERR_DEVICEFAULT		-	se houve alguma falha no dispositivo
-----------------------------------------------------------------------------------------------*/
RF_INT32 RF_USB_keyboard_getkeystroke( RF_INT32 timeout )
{

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	return RF_SUCCESS;
}

/*-----------------------------------------------------------------------------------------------
Autor,Empresa:	
Funcao		:	RF_USB_keyboard_getkeystroke_nobuffer()
Descri玢o	:	Esta fun玢o tem o mesmo comportamento da fun玢o RF_USB_keyboard_getkeystroke, com
				a 鷑ica diferen鏰 que ela desconsidera teclas que tenham sido pressionadas pelo
				usu醨io antes da chamada da fun玢o, limpando o 揵uffer� de teclas j� pressionadas
Argumentos	:	Timeout	-	tempo m醲imo de espera pela tecla em segundos.
				Um valor negativo indica espera indefinida.
Retorno		:	O c骴igo da tecla pressionada em caso de sucesso (um valor n鉶 negativo). Em caso
				de falha, um dos seguintes valores:
					RF_ERR_RESOURCEALLOC	-	se o recurso compartilhado n鉶 p鬱e atender �
												requisi玢o (ex.: n鉶 p鬱e alocar um timer)
					RF_ERR_TIMEOUT			-	se tiver passado o tempo definido pelo par鈓etro
												'timeout' e nenhuma tecla tiver sido pressionada
					RF_ERR_DEVICEFAULT		-	se houve alguma falha no dispositivo
-----------------------------------------------------------------------------------------------*/
RF_INT32 RF_USB_keyboard_getkeystroke_nobuffer( RF_INT32 timeout )
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	return RF_SUCCESS;
}
#ifdef __cplusplus
}
#endif // __cplusplus
#endif // _RF_API_USB_KEYBOARD_C_
