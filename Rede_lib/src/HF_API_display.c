#include "HF_API.h"

HF_INT32 HF_display_print(HF_UINT32 row, HF_UINT32 column, const HF_CHAR *text)
{
    // ---------------------------------
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_display_printAligned(HF_UINT32 row, HF_INT32 alignment, const HF_CHAR *text)
{
    // ---------------------------------
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_display_printimage(const HF_VOID *image, HF_UINT32 x, HF_UINT32 y)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_clearline(HF_UINT32 line)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_clear(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_clear_screen(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_geometry(HF_UINT32 *lines, HF_UINT32 *columns)
{
	*lines = 0;
	*columns = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_pixels(HF_UINT32 *width, HF_UINT32 *height)
{
    *width = 240;
    *height = 320;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_setBacklight(HF_UINT8 percent)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawLine(HF_UINT32 x0, HF_UINT32 y0, HF_UINT32 x1, HF_UINT32 y1)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawPoint(HF_UINT32 x, HF_UINT32 y)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_getCursorPosition(HF_UINT32 *column, HF_UINT32 *line)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_setFont(HF_FONT_T pFont)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_getFont(HF_FONT_T *pFont)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_getFontDimension(HF_FONT_T pFont, HF_UINT32 *width, HF_UINT32 *height)
{
    *width = 1;
    *height = 1;
    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_VOID  HF_display_setAutoFlush(HF_BOOL value)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
}

HF_BOOL  HF_display_getAutoFlush(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_VOID  HF_display_setInvertColors(HF_BOOL value)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
}

HF_BOOL  HF_display_getInvertColors(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_VOID  HF_display_flush(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
}

HF_INT32 HF_display_setColor(HF_INT64 text, HF_INT64 background)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT64 HF_display_getColor(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT64 HF_display_getBackgroundColor(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_fillRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_BOOL  HF_display_setHeader(HF_BOOL value)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	return HF_SUCCESS;
}

HF_BOOL  HF_display_setFooter(HF_BOOL value)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	return HF_SUCCESS;
}

HF_INT32 HF_display_setCustomHeader(HF_BOOL onOff, const HF_VOID *image)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_display_setCustomFooter(HF_BOOL onOff, const HF_VOID *image)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
