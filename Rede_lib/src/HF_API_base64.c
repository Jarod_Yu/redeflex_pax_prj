#include "HF_API.h"
#include "HF_API_base64.H"

#if 1


const HF_INT8 base64_alphabet[]=
{"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="};


HF_INT32 HF_base64_encodeH(const HF_INT8 *data, HF_INT32 length, HF_INT8 **result, HF_INT32 *resultSz)
{
	HF_INT8 szCode[4]={0};
	HF_INT32 i = 0, j = 0, result_len = 0;
	HF_INT32 uiCodeLen=0;
	HF_INT8 szOutput[2048] = {0};

	if ((data == NULL) || (result == NULL))
	{
		return HF_ERR_INVALIDPARAM;
	}

	*result = (char*)malloc(length*3);

	memset(*result, 0, length*3);

	for(i = 0,j=0; i<length; i+=3, j+=4)
	{
		memset(szCode, 0, 4);
		uiCodeLen = 0;
		szCode[0] = data[i];
		uiCodeLen += 1;
		if(i < (length - 1))
		{
			szCode[1] = data[i+1];
			uiCodeLen += 1;
		}
		if(i < (length - 2))
		{
			szCode[2] = data[i+2];
			uiCodeLen += 1;
		}

		if(uiCodeLen == 3)
		{
			szOutput[j] = base64_alphabet[(szCode[0]&0xfc)>>2];
			szOutput[j+1] = base64_alphabet[((szCode[0]&0x03)<<4) + ((szCode[1]&0xf0)>>4)];
			szOutput[j+2] = base64_alphabet[((szCode[1]&0x0f)<<2) + ((szCode[2]&0xc0)>>6)];
			szOutput[j+3] = base64_alphabet[szCode[2]&0x3f];
		}
		else if(uiCodeLen == 2)
		{
			szOutput[j] = base64_alphabet[(szCode[0]&0xfc)>>2];
			szOutput[j+1] = base64_alphabet[((szCode[0]&0x03)<<4) + ((szCode[1]&0xf0)>>4)];
			szOutput[j+2] = base64_alphabet[(szCode[1]&0x0f)<<2];
			szOutput[j+3] = base64_alphabet[64];
		}
		else
		{
			szOutput[j] = base64_alphabet[(szCode[0]&0xfc)>>2];
			szOutput[j+1] = base64_alphabet[(szCode[0]&0x03)<<4];
			szOutput[j+2] = base64_alphabet[64];
			szOutput[j+3] = base64_alphabet[64];
		}
	}

	result_len = strlen(szOutput);
	*resultSz = result_len;
	PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "encode result length:%d",result_len);
	PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "encode result:%s",szOutput);
	memcpy(*result, szOutput, result_len);

	// Jarod@2015/09/14 UPD START
	//return result_len;
	return HF_SUCCESS;
	// Jarod@2015/09/14 UPD END
}

HF_INT32 HF_base64_decodeH(const HF_INT8 *data, HF_INT32 length, HF_INT8 **result, HF_INT32 *resultSz)
{
	HF_INT8 szCode[5]={0};
	HF_INT32 i=0,j=0,k=0,m=0,result_len=0;
	HF_INT8 szASC[4]={0};
	HF_INT8 szOutput[2048]={0};

	if ((data == NULL) || (result == NULL))
	{
		return HF_ERR_INVALIDPARAM;
	}

	PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "\n\n===== decodeH length:[%d]=====\n\n",length);

	PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "\n\n===== decodeH data=====\n\n%s",data);

	*result = (char*)malloc(length);

	memset(*result, 0, length);

	for(i = 0,j=0; i<length; i+=4, j+=3)
	{
		memset(szCode, 0, 5);
		szCode[0] = data[i];
		szCode[1] = data[i+1];
		szCode[2] = data[i+2];
		szCode[3] = data[i+3];

		for(k=0; k<4; k++)
		{
			for(m=0; m<65; m++)
			{
				if(szCode[k] == base64_alphabet[m])
				{
					szASC[k] = m;
					break;
				}
			}
		}

		if((szCode[2] == '=') && (szCode[3] == '='))
		{
			szOutput[j] = (szASC[0]<<2) + ((szASC[1]<<2)>>6);
			result_len = (j+1);
			PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "decode result j:%d,result_len:%d",j,result_len);
			break;
		}
		else if(szCode[3] == '=')
		{
			szOutput[j] = (szASC[0]<<2) + ((szASC[1]<<2)>>6);
			szOutput[j+1] = (szASC[1]<<4) + ((szASC[2]<<2)>>4);
			result_len = (j+2);
			PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "decode result j:%d,result_len:%d",j,result_len);
			break;
		}
		else
		{
			szOutput[j] = (szASC[0]<<2) + ((szASC[1]<<2)>>6);
			szOutput[j+1] = (szASC[1]<<4) + ((szASC[2]<<2)>>4);
			szOutput[j+2] = (szASC[2]<<6) + szASC[3];
			// Jarod@2015/09/14 UPD START
			result_len = j+3;
			// Jarod@2015/09/14 UPD END
		}
	}

	PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "decode result length:%d",result_len);

	for (i=0;i<result_len;i++)
	{
		PaxLog_Base64(LOG_DEBUG, __FUNCTION__, __LINE__, "decode result [%d]: %02x",i,szOutput[i]);
	}

	*resultSz = result_len;

	memcpy(*result, szOutput, result_len);


	// Jarod@2015/09/14 UPD START
	//return result_len;
	return HF_SUCCESS;
	// Jarod@2015/09/14 UPD END
}
#endif


