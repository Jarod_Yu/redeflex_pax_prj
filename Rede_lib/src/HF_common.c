/*
 * HF_common.c
 *
 *  Created on: Sep 12, 2015
 *      Author: Jarod
 */
#include <stdlib.h>
#include "HF_API.h"


HF_INT32 HF_rand(HF_VOID)
{
	return rand();
}

RF_VOID* HF_malloc(RF_SIZE_T size)
{
	return malloc((size_t)size);
}

RF_VOID  HF_free(RF_VOID* ptr)
{
	free(ptr);
}
