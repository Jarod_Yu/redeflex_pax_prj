#include "HF_API.h"


/**
 * Fun玢o que ser� chamada pela aplica玢o para definir os par鈓etros para conex鉶 na rede WI-FI.
 * @param RF_WIFI_CONFIG_INFO *config - Ponteiro para a estrutura RF_WIFI_CONFIG_INFO, na qual s鉶 informados:
 *        connect_max_retry - N鷐ero m醲imo de tentativas de estabelecer a conex鉶 WI-FI.
 *                            Se zero, n鉶 h� limite. Isto �, fica em loop at� ser desativado.
 *        reconnect_wait_mseg - Tempo que o dispositivo deve aguardar antes de
 *                              tentar re-estabelecer a conex鉶 WI-FI.
 * @return RF_SUCCESS � se a fun玢o executou com sucesso.
 *         RF_ERR_INVALIDARG � Ponteiro nulo
 *         RF_DEVICE_FAULT � Erro na execu玢o
 */
RF_INT32 RF_WIFI_config(RF_WIFI_CONFIG_INFO *config)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

/**
 * Ativa a interface WI-FI do dispositivo.
 * @return RF_SUCCESS      � Interface ativada.
 *         RF_DEVICE_FAULT � Erro ativando interface
 */
RF_INT32 RF_WIFI_up(RF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

/**
 * Destiva a interface WI-FI do dispositivo.
 * @return RF_SUCCESS � Interface desativada.
 *         RF_DEVICE_FAULT � Erro desativando interface.
 */
RF_INT32 RF_WIFI_down(RF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}


/**
 * Obtem a lista de redes WI-FI dispon韛eis.
 *
 * A fun玢o deve retornar quando expirar o timeout ou atingir o m醲imo de resultados poss韛eis.
 *
 * @param p_results - Ponteiro para a estrutura que armazena a lista de redes WI-FI dispon韛eis.
 * @param maxResults - Quantidade m醲ima de redes WI-FI dispon韛eis a ser retornada.
 * @param timeout_ms  - Timeout em milissegundos.
 * @return RF_SUCCESS � Lista de redes WI-FI obtida com sucesso. A lista pode estar vazia,
 * isto � n鉶 foram encontradas redes WI-FI.
 *         RF_DEVICE_FAULT � Erro.
 *
 *
 */
RF_INT32 RF_WIFI_scan(RF_WIFI_SCAN_RESULTS* p_results, RF_UINT16 maxResults, RF_UINT32 timeout_ms)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

/**
 * Inicia o processo de conex鉶 em uma rede WI-FI.
 *
 * A fun玢o dever� retornar imediatamente, isto � n鉶 dever� aguardar a completa
 * conex鉶 na rede WI-FI.
 *
 * A conex鉶 na rede WI-FI dever� ser realizada em background por uma thread/task.
 * Esta thread tamb閙 ser� respons醰el por detectar quedas e restabelecer automaticamente a conex鉶.
 *
 * A Thread dever� respeitar os par鈓etros definidos na RF_WIFI_config, zerando o contador de tentativas
 * de reconex鉶 cada vez que a conex鉶 for estabelecida.
 *
 * Durante o estabelecimento da conex鉶 com a rede WI-FI o status obtido atrav閟 da
 * fun玢o RF_WIFI_status deve ser atualizado constantemente.
 *
 * @param network - dados da conex鉶 Wi-fi que o dispositivo dever� conectar.
 *                  A conexao s� poder� ser realizada caso os seguintes parametros
 *                  sejam identicos a uma rede acess韛el: ssid, tam_ssid, algoritmoAuth, algoritmoCripto
 * @param p_ascii_password - String terminada em NULL('\0') contendo a senha de conex鉶 da rede WI-FI.
 *                           Caso a rede em quest鉶 n鉶 precise de senha o par鈓etro ser� ignorado.
 *                           A implementa玢o dever� copiar o valor da string para suas estruturas
 *                           internas. Ap髎 o retorno da fun玢o n鉶 h� garantia que este ponteiro continuar� integro.
 *
 * @return RF_SUCCESS � Iniciado processo de conex鉶 na rede WI-FI.
 *         RF_ERR_INVALIDSTATE � Fun玢o RF_WIFI_config n鉶 foi chamada anteriormente.
 *         RF_DEVICE_FAULT � Iniciando processo de conex鉶 na rede WI-FI.
 */
RF_INT32 RF_WIFI_start_connect(RF_WIFI_NETWORK_INFO network, RF_IP_NETWORK_INFO ipInfo, const RF_CHAR* p_ascii_password)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

/**
 * Obtem o status da conex鉶 WI-FI do dispositivo.
 *
 * @param p_item Ponteiro para a estrutura que armazena o status.
 *
 * @return RF_SUCCESS � Status obtido com sucesso.
 *         RF_DEVICE_FAULT � Erro obtendo status
 */
RF_INT32 RF_WIFI_status(RF_WIFI_STATUS_INFO *p_item)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

/**
 * Indica para o dispositivo que a conex鉶 WI-FI deve ser desconectada.
 * A fun玢o dever� retornar imediatamente, isto � n鉶 dever� aguardar a
 * completa desconex鉶 da rede WI-FI (a desconex鉶 da rede WI-FI dever� ser
 * realizada em background).
 * Durante a desconex鉶 o status obtido atrav閟 da fun玢o RF_WIFI_status
 * deve ser atualizado constantemente.
 *
 * @return RF_SUCCESS � Solicita玢o processada com sucesso
 *         RF_DEVICE_FAULT � Erro processando solicita玢o
 */
RF_INT32 RF_WIFI_request_disconnect(RF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
