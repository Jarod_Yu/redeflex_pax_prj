#include "HF_API.h"

#ifndef	PED_TPK
#define PED_TPK		0x03
#endif

#ifndef PED_TDK
#define PED_TDK		0X05
#endif

#ifndef PED_TIK
#define PED_TIK		0X10
#endif

static RF_INT32 SEC_write_3des(RF_UINT16 keyID, const RF_UINT8 *workingKey, RF_INT32 type)
{
    HF_UINT8 keyBlock[184] = {0};
    HF_UINT32 byteCounter = 0;

    keyBlock[byteCounter] = 0x03; // Format, fixed as 0x03 according to PROLIN API user guide
    byteCounter++;
    keyBlock[byteCounter] = PED_TPK;	// plaintext , ignore Source key type
    byteCounter++;
    keyBlock[byteCounter] = 0;	// Source key index
    byteCounter++;
    keyBlock[byteCounter] = keyID;	// Destination key index
    byteCounter++;

    // The next 7 bytes of keyBlock are revered
    byteCounter += 7;

    if(RF_SEC_KEY_TYPE_3DES_PIN == type)// Destination key type
    {
    	keyBlock[byteCounter] = PED_TPK;
    }
    else//RF_SEC_KEY_TYPE_3DES_DATA
    {
    	keyBlock[byteCounter] = PED_TDK;
    }

    byteCounter++;
    keyBlock[byteCounter] = 16;	// For now we are using 16 bytes of key
    byteCounter++;
    memcpy(&keyBlock[byteCounter], workingKey, 16);
    
    // Buf fix reason: 24 means DstKeyValue's offset value
    byteCounter += 24;// There are 24 bytes totally reserved for the key data field according to PROLIN API user guide

    keyBlock[byteCounter] = 0x00;		// KCV check mode - NONE
    byteCounter++;
    // No KCV related data following

    return OsPedWriteKey(keyBlock);
}

static RF_INT32 SEC_write_dukpt(RF_UINT16 keyID, const RF_UINT8 *workingKey, const RF_UINT8 *p_ksn, RF_UINT32 ksn_sz)
{
    HF_UINT8 keyBlock[184] = {0};
    HF_UINT32 byteCounter = 0;

    keyBlock[byteCounter] = 0x03; // Format, fixed as 0x03 according to PROLIN API user guide
    byteCounter++;
    keyBlock[byteCounter] = PED_TIK;	// plaintext , ignore Source key type
    byteCounter++;
    keyBlock[byteCounter] = 0;	// Source key index
    byteCounter++;
    keyBlock[byteCounter] = keyID;	// Destination key index
    byteCounter++;

    // The next 7 bytes of keyBlock are revered
    byteCounter += 7;

    keyBlock[byteCounter] = PED_TIK;// Destination key type

    byteCounter++;
    keyBlock[byteCounter] = 16;	// For now we are using 16 bytes of key   TODO
    byteCounter++;
    memcpy(&keyBlock[byteCounter], workingKey, 16);
    
    // Buf fix reason: 24 means DstKeyValue's offset value
    byteCounter += 24;// There are 24 bytes totally reserved for the key data field according to PROLIN API user guide
    
    keyBlock[byteCounter] = 0x00;		// KCV check mode - NONE
    byteCounter++;
    // No KCV related data following

    memcpy(&keyBlock[174], p_ksn, ksn_sz);

    return OsPedWriteTIK(keyBlock);
}

RF_INT32 RF_SEC_store_key(RF_SEC_KEY_TYPE type, RF_UINT16 keyIdx, const RF_UINT8 *p_key, RF_UINT32 key_sz, const RF_UINT8 *p_ksn, RF_UINT32 ksn_sz)
{
	RF_INT32 ret = -1;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	if ((NULL == p_key) || (0 == key_sz))
	{
		 PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"key or keySz err \n");
		 
		 return RF_ERR_INVALIDARG;
	}

	if ( 0 == ((RF_SEC_KEY_TYPE_3DES_PIN == type) || (RF_SEC_KEY_TYPE_3DES_DATA == type) || \
			(RF_SEC_KEY_TYPE_DUKPT_PIN == type) || (RF_SEC_KEY_TYPE_DUKPT_DATA == type)))
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"keyType err \n");
		return RF_ERR_INVALIDARG;
	}

	if (!(keyIdx >= 1 && keyIdx <= 100)) // prolin DUKPT group also [1,100], but monitor [1,10]
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"keyIdx err \n");

		return RF_ERR_INVALIDARG;
	}

	if ((RF_SEC_KEY_TYPE_DUKPT_PIN == type) || (RF_SEC_KEY_TYPE_DUKPT_DATA == type))
	{
		if ((NULL == p_ksn)  || (0 == ksn_sz))
		{
			 PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"keysn or ksnSz err \n");

			 return RF_ERR_INVALIDARG;
		}

		if(RF_SEC_KEY_TYPE_DUKPT_PIN == type) //keyIdx [1,50]
		{
			if(keyIdx > 50)
			{
				keyIdx -= 50;
			}
		}
		else	//RF_SEC_KEY_TYPE_DUKPT_DATA keyIdx [51,100]
		{
			if(keyIdx < 50)
			{
				keyIdx += 50;
			}
		}

		if(ksn_sz > 10)
		{
			ksn_sz = 10;
		}
	}

	if (0 != OsPedOpen())
	{
		 PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"open ped err \n");
		 return RF_ERR_DEVICEFAULT;
	}

	switch(type)
	{
	case RF_SEC_KEY_TYPE_3DES_PIN:
	case RF_SEC_KEY_TYPE_3DES_DATA:
		ret = SEC_write_3des(keyIdx, p_key, type);
		break;

	case RF_SEC_KEY_TYPE_DUKPT_PIN:
	case RF_SEC_KEY_TYPE_DUKPT_DATA:
		ret = SEC_write_dukpt(keyIdx, p_key, p_ksn, ksn_sz);
		break;

	default:
		//null
		break;
	}

	OsPedClose();

	if(0 != ret)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"store_key err \n");
		return RF_ERR_DEVICEFAULT;
	}

	return RF_SUCCESS;
}

