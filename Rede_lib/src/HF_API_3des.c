#include "HF_API.h"

/***************************************************************************************************************
 * Question summary for 3DES that need to be clarified by Fabrizio:
 * 1. There are 2 different kinds of key length for 3DES -- double(16 bytes) & triple(24bytes) length key. Which one shall we use. (Done - 16 byte will always be used)
 * 2. Details of error codes need to be defined (e.g. invalid parameter).
 * 3. For API "HF_mk3des_encryptBlock" and "HF_mk3des_CBC_encryptBlock", it's pre-clarified as "deprecated" in this source file. Shall we implement them? (Done - don't need to impletment)
 * 4. Concerning the 2 APIs mentioned at point 3 above, there aren't corresponding decryption APIs. Need to be confirmed. (Done - don't need to implement)
 ***************************************************************************************************************/

#ifndef	PED_TMK
#define PED_TMK		0x02
#endif

#ifndef PED_TDK
#define PED_TDK		0X05
#endif

#define TDK_DERIVEDWK_INDEX		99

// Calculate XOR result of vector1 and vector2. The pointer result may be equal to vector1
static HF_VOID pubXOR(const HF_INT8 *vector1, const HF_INT8 *vector2,
        HF_UINT32 dataLength, HF_INT8 *result)
{
    HF_UINT32 loopCounter = 0;

    if ((NULL == vector1) || (NULL == vector2) || (NULL == result)
            || (0 == dataLength))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__," PARA IS NULL");
        return;
    }

    memcpy(result, vector1, dataLength);
    for (loopCounter = 0; loopCounter < dataLength; loopCounter++)
    {
        result[loopCounter] ^= vector2[loopCounter];
    }
}

static HF_INT32 deriveTDK(HF_UINT16 keyID, const HF_INT8 *workingKey)
{
    HF_UINT8 keyBlock[256];
    HF_UINT32 byteCounter = 0;

    // Jarod@2015/07/23 UPD START
    //memset(&keyBlock[byteCounter], 0, 4);
    memset(&keyBlock[byteCounter], 0, sizeof(keyBlock));
    // Jarod@2015/07/23 UPD END

    keyBlock[byteCounter] = 0x03; // Format, fixed as 0x03 according to PROLIN API user guide
    byteCounter++;

    keyBlock[byteCounter] = PED_TMK;	// Source key type
    byteCounter++;

    keyBlock[byteCounter] = keyID;	// Source key index
    byteCounter++;
    keyBlock[byteCounter] = TDK_DERIVEDWK_INDEX;	// Destination key index
    byteCounter++;


    // The next 7 bytes of keyBlock are revered
    byteCounter += 7;

    // Jarod@2015/07/23 UPD START
    //memset(&keyBlock[byteCounter], 0, 163);
    // Jarod@2015/07/23 UPD END

    keyBlock[byteCounter] = PED_TDK;	// Destination key type
    byteCounter++;
    keyBlock[byteCounter] = 16;	// For now we are using 16 bytes of key
    byteCounter++;
    memcpy(&keyBlock[byteCounter], workingKey, 16);

    // Jarod@2015/07/23 ADD START
    // Buf fix reason: 24 means DstKeyValue's offset value
    // keyBlock[byteCounter] += 24;// There are 24 bytes totally reserved for the key data field according to PROLIN API user guide
    byteCounter += 24;// There are 24 bytes totally reserved for the key data field according to PROLIN API user guide
    // Jarod@2015/07/23 ADD END

    keyBlock[byteCounter] = 0x00;		// KCV check mode - NONE
    byteCounter++;
    // No KCV related data following

    return OsPedWriteKey(keyBlock);
}

HF_INT32 HF_3des_encryptBlock(const HF_INT8 *key, const HF_INT8 *data,
        HF_UINT32 dataSz, HF_INT8 *result)
{
    HF_UINT32 dataBlockNum = 0, loopCounter = 0;

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_3des_encryptBlock=====\n");

    if ((NULL == key) || (NULL == data) || (0 == dataSz) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_INVALIDARG;
    }

    // The size of the data block must be a multiple of 8
    if (0 != dataSz % 8)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    dataBlockNum = dataSz / 8;
    for (loopCounter = 0; loopCounter < dataBlockNum; loopCounter++)
    {
        OsDES((const HF_UINT8 *) &data[8 * loopCounter],
                (HF_UINT8 *) &result[8 * loopCounter], (const HF_UINT8 *) key,
                16, 1);	// TODO: 16 bytes of key or 24 bytes of key? Need to be clarified
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_3des_decryptBlock(const HF_INT8 *key, const HF_INT8 *data,
        HF_UINT32 dataSz, HF_INT8 *result)
{
    HF_UINT32 dataBlockNum = 0, loopCounter = 0;

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_3des_decryptBlock=====\n");

    if ((NULL == key) || (NULL == data) || (0 == dataSz) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    // The size of the data block must be a multiple of 8
    if (0 != dataSz % 8)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    dataBlockNum = dataSz / 8;
    for (loopCounter = 0; loopCounter < dataBlockNum; loopCounter++)
    {
        OsDES((const HF_UINT8 *) &data[8 * loopCounter],
                (HF_UINT8 *) &result[8 * loopCounter], (const HF_UINT8 *) key,
                16, 0);
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_mk3des_encryptBlock(HF_UINT16 keyID, const HF_INT8 *workingKey,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result)
{
    // DEPRECATED
    HF_INT32 ret;

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_mk3des_encryptBlock=====\n");

    if ((NULL == workingKey) || (NULL == data) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (!(keyID >= 1 && keyID <= 100))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (0 != OsPedOpen())
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    ret = deriveTDK(keyID, workingKey);
    if (0 != ret)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_DEVICEFAULT=====\n\n");

    	// Jarod@2015/07/24 UPD START
    	OsPedClose();
    	// Jarod@2015/07/24 UPD END

        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }
    ret = OsPedDes(TDK_DERIVEDWK_INDEX, NULL, (const HF_UINT8 *) data,
            (HF_INT32) dataSz, (HF_UINT8 *) result, 0x01);
    if (0 != ret)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_DEVICEFAULT=====\n\n");

    	// Jarod@2015/07/24 UPD START
    	OsPedClose();
    	// Jarod@2015/07/24 UPD END

        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    OsPedClose();

    return HF_SUCCESS;
}

HF_INT32 HF_3des_CBC_encryptBlock(const HF_INT8 *key, const HF_INT8 *iv,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result)
{
    HF_UINT32 dataBlockNum = 0, loopCounter = 0;
    HF_INT8 dataBuffer[8];

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_3des_CBC_encryptBlock=====\n");

    if ((NULL == key) || (NULL == data) || (0 == dataSz) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    // The size of the data block must be a multiple of 8
    if (0 != dataSz % 8)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    memset(dataBuffer, 0, sizeof(dataBuffer));
    if (NULL == iv)
    {
        // If the initial XOR vector is NULL, use 8 bytes of 0 as the default one.
        memcpy(dataBuffer, "\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    } else
    {
        memcpy(dataBuffer, iv, 8);
    }

    dataBlockNum = dataSz / 8;
    for (loopCounter = 0; loopCounter < dataBlockNum; loopCounter++)
    {
        pubXOR(dataBuffer, &data[8 * loopCounter], 8, dataBuffer);
        OsDES((const HF_UINT8 *) dataBuffer,
                (HF_UINT8 *) &result[8 * loopCounter], (HF_UINT8 *) key, 16, 1);// TODO: 16 bytes of key or 24 bytes of key? Need to be clarified

        memcpy(dataBuffer, &result[8 * loopCounter], 8);
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_3des_CBC_decryptBlock(const HF_INT8 *key, const HF_INT8 *iv,
        const HF_INT8 *data, HF_UINT32 dataSz, HF_INT8 *result)
{
    HF_UINT32 dataBlockNum = 0, loopCounter = 0;
    HF_INT8 dataBuffer[8];

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_3des_CBC_decryptBlock=====\n");

    if ((NULL == key) || (NULL == data) || (0 == dataSz) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    // The size of the data block must be a multiple of 8
    if (0 != dataSz % 8)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    memset(dataBuffer, 0, sizeof(dataBuffer));
    if (NULL == iv)
    {
        // If the initial XOR vector is NULL, use 8 bytes of 0 as the default one.
        memcpy(dataBuffer, "\x00\x00\x00\x00\x00\x00\x00\x00", 8);
    } else
    {
        memcpy(dataBuffer, iv, 8);
    }

    dataBlockNum = dataSz / 8;
    for (loopCounter = 0; loopCounter < dataBlockNum; loopCounter++)
    {
        OsDES((const HF_UINT8 *) &data[8 * loopCounter],
                (HF_UINT8 *) &result[8 * loopCounter], (const HF_UINT8 *) key,
                16, 0);	// TODO: 16 bytes of key or 24 bytes of key? Need to be clarified

        pubXOR(&result[8 * loopCounter], dataBuffer, 8,
                &result[8 * loopCounter]);
        memcpy(dataBuffer, &data[8 * loopCounter], 8);
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    return HF_SUCCESS;
}

HF_INT32 HF_mk3des_CBC_encryptBlock(HF_UINT16 keyID, const HF_INT8 *workingKey,
        const HF_INT8 *iv, const HF_INT8 *data, HF_UINT32 dataSz,
        HF_INT8 *result)
{
    // DEPRECATED
    HF_INT32 ret;

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU HF_mk3des_CBC_encryptBlock=====\n");

    if ((NULL == workingKey) || (NULL == data) || (NULL == result))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (!(keyID >= 1 && keyID <= 100))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (0 != OsPedOpen())
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }
    ret = deriveTDK(keyID, workingKey);
    if (0 != ret)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_DEVICEFAULT=====\n\n");
    	// Jarod@2015/07/24 UPD START
    	OsPedClose();
    	// Jarod@2015/07/24 UPD END
        return HF_ERR_DEVICEFAULT;
    }
    // Jarod@2015/07/23 UPD START
    //ret = OsPedDes(TDK_DERIVEDWK_INDEX, (HF_UINT8 *) iv,
    //        (const HF_UINT8 *) data, (HF_INT32) dataSz, (HF_UINT8 *) result,
    //        0x00);
    // Des mode 0x00 means ECB decrypt;
    // Des mode 0x03 means CBC encrypt;
    ret = OsPedDes(TDK_DERIVEDWK_INDEX, (HF_UINT8 *) iv,
            (const HF_UINT8 *) data, (HF_INT32) dataSz, (HF_UINT8 *) result,
            0x03);
    // Jarod@2015/07/23 UPD END

    if (0 != ret)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_DEVICEFAULT=====\n\n");
    	// Jarod@2015/07/24 UPD START
    	OsPedClose();
    	// Jarod@2015/07/24 UPD END

        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");

    OsPedClose();

    return HF_SUCCESS;
}

RF_INT32 RF_mk3des_ECB_encryptBlockDUKPT(RF_UINT16 keyID, const
		RF_UINT8 *inputData, RF_UINT32 inputDataSize, RF_UINT8 *outputData,
		RF_UINT8 *outputKsn)
{

    // DEPRECATED
    HF_INT32 ret;

    PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====ENTROU RF_mk3des_ECB_encryptBlockDUKPT=====\n");

    if ((NULL == inputData) || (NULL == outputData) || (NULL == outputKsn))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====1.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (inputDataSize > 8192)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====2.HF_ERR_DEVICEFAULT=====\n\n");
    	return HF_ERR_DEVICEFAULT;
    }

    if (!(keyID >= 1 && keyID <= 100))
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====3.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    if (0 != OsPedOpen())
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====4.HF_ERR_DEVICEFAULT=====\n\n");
        // TODO: More detail error code may needed
        return HF_ERR_DEVICEFAULT;
    }

    ret = OsPedDesDukpt(keyID, 0x01, NULL, inputDataSize, inputData, outputData, outputKsn, 0x01);



    if (0 != ret)
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====5.HF_ERR_DEVICEFAULT=====\n\n");
    	OsPedClose();
        return HF_ERR_DEVICEFAULT;
    }
    else
    {
    	PaxLog_3des(LOG_DEBUG,__FUNCTION__,__LINE__,"\t=====HF_SUCCESS=====\n\n");
    	//OsPedIncreaseKsnDukpt(keyID);

    	OsPedClose();
    	return HF_SUCCESS;
    }
}

