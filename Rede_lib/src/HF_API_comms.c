#include "HF_API.h"

HF_INT32 HF_comm_configDefault(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_configLoad(const HF_CHAR *filename)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_setProperty(const HF_CHAR *key, const HF_CHAR *value)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getProperty(const HF_CHAR *pKey, HF_CHAR *pBuffer, HF_UINT32 pBuffSz) 
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_configSave(const HF_CHAR *filename)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getMaxDialNumbers(HF_UINT8 *maxPhonenumber, HF_UINT8 *maxPabx)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_connect(const HF_CHAR *pPhonenumber, const HF_CHAR *pPabx)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_checkConnect(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_disconnect(HF_VOID)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_recvmsg(HF_UINT8 *data, HF_UINT16 maxlen, HF_UINT32 timeout)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_sendmsg(const HF_UINT8 *msg, HF_UINT16 length)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getBuffSz(HF_INT32 *sendBuff, HF_INT32 *recBuff)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_SUCCESS;
}
