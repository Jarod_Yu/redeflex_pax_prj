/*------------------------------------------------------------
* FileName: HF_PAX_LOG.c
* Author: Jarod
* Date: 2015-07-14
------------------------------------------------------------*/
//#include "debugger.h"
#include "HF_API.h"
#include "stdarg.h"

//#define MAX_CHARS_RS232 (1 * 1024)

void PaxLog_GSM(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef GSM_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Module(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Module_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Sha1(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Sha1_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_fs(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef fs_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_fs_CA(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef fs_CA_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_fs_TFS(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef fs_TFS_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_RSA(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef RSA_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Socket(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Socket_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_3des(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef des_Log
	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_BC(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef BC_Log
	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}


void PaxLog_thread(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef thread_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}


void PaxLog(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef PAX_LOG
	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Capabilities(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Capabilities_Log
	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Base64(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Base64_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Keyboard(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Keyboard_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Sys(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Sys_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_SelfUpdate(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef SelfUpdate_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

void PaxLog_Printer(char Debug_level,const char *szFuncName, const int line, char *szLogInfo, ...)
{
#ifdef Printer_Log

	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
#endif
}

//End of file

