#include "HF_API.h"
#include <string.h>

struct RF_API_BC_callback_reg local_callbacks;
static RF_BOOL bOpenBc = 0;

int APPBC_CALLBACK_iDisplayClear (void);
int APPBC_CALLBACK_iDisplay (char *pszMsg);
int APPBC_CALLBACK_iMenu (char *pszTitle, char **pvszItem,
                          int iItem, int iTimeout);
int APPBC_CALLBACK_iGetPIN (char *pszMsg, int iNum);


static HF_INT32 controlBc(void)
{
	HF_INT32 iRet;

	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU controlBc");

	if(!bOpenBc)
	{
		iRet = PP_Open("01");
		PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "PP_Open: %d", iRet);
	}
	else
	{
		iRet = PP_Close("                                ");
		if(iRet == PPCOMP_OK)
		{
			bOpenBc = 0;
		}

		PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "PP_Close: %d", iRet);
		iRet = PP_Open("01");
		PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "PP_Open: %d", iRet);
	}

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "iRet:%d", iRet);
	if(iRet != PPCOMP_OK)
	{
		return iRet;
	}
	bOpenBc = 1;

	return iRet;
}

int APPBC_CALLBACK_iDisplayClear(void)
{
	HF_INT16 ret;

	ret = local_callbacks.cb_screen;
	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iDisplayClear:[%d]", ret);

	return HF_SUCCESS;
}

int APPBC_CALLBACK_iDisplay(char *pszMsg)
{
	HF_INT16 i, ret;

	if ( NULL == pszMsg )
		return HF_ERR_INVALIDARG;

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iDisplay[len:%d] -> pszMsg:", strlen(pszMsg));
	for(i = 0; pszMsg[i] != '\0'; i++)
	{
		PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "[%d][%c]", pszMsg[i], pszMsg[i]);
	}

	while(*pszMsg == '\n' || *pszMsg == ' ')
	{
		pszMsg++;
	}

	if(strstr(pszMsg, "ATUALIZANDO") || strstr(pszMsg, "INSIRA") || strstr(pszMsg, "PROCESSANDO") || strstr(pszMsg, "SELECIONADO:")
			|| strstr(pszMsg, "SENHA") || strstr(pszMsg, "APLICACAO") || strstr(pszMsg, "RETIRE") || strstr(pszMsg, "retire"))
	{
		ret = local_callbacks.cb_show_msg(pszMsg);
	}

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iDisplay:[%d]", ret);

	return ret;
}

int APPBC_CALLBACK_iMenu(char *pszTitle, char **pvszItem,
                          int iItem, int iTimeout)
{
	HF_INT16 ret;

	if ( NULL == pszTitle || NULL == *pvszItem )
		return HF_ERR_INVALIDARG;

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iMenu(%s,%d,%d)", pszTitle, iItem, iTimeout);

	ret = local_callbacks.cb_show_menu(pszTitle, pvszItem, iItem, iTimeout);
	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iMenu[%d]", ret);

	//[PAXBR][09.09.2015] - Retornar o mesmo valor vindo de cb_show_menu
	return ret;
}

int APPBC_CALLBACK_iGetPIN(char *pszMsg, int iNum)
{
	HF_INT16 ret;

	if ( NULL == pszMsg )
		return HF_ERR_INVALIDARG;

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iGetPIN(%d,%s)", iNum, pszMsg);

	ret = local_callbacks.cb_show_pass_info(pszMsg, iNum);

	//[PAXBR]: Posicionar o layout de asteriscos para quando for PINOFF
	OsPedSetAsteriskLayout (160, 191, 24, RGB(255, 128, 0), PED_ASTERISK_ALIGN_CENTER);
	OsSleep(50);

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "APPBC_CALLBACK_iGetPIN[%d]", ret);

	return ret;
}

RF_INT16 RF_API_BC_register_callbacks(struct RF_API_BC_callback_reg callbacks)
{
	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "ENTROU RF_API_BC_register_callbacks");
    local_callbacks = callbacks;
    PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "EXECUTOU RF_API_BC_register_callbacks");
    return PP_OK;
}

HF_INT32 HF_PP_SetDisplayCallback(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_PP_SetDisplayExCallback(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_PP_SetMenuCallback (HF_VOID)
{
    return HF_SUCCESS;
}

//29.06.2015
HF_INT32 HF_PP_Open(HF_CHAR * psCom)
{
	HF_INT32 ret=0;

	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_Open");

	ret = PP_Open(psCom);
	if(ret == PPCOMP_OK)
	{
		bOpenBc = 1;
	}

	PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "PP_Open: %d", ret);

	return ret;
}

//29.06.2015
HF_INT32 HF_PP_Close(HF_CHAR * psIdleMsg)
{
	HF_INT32 ret=0;

	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_Close(%s)", psIdleMsg);

    ret = PP_Close(psIdleMsg);
    if(ret == PPCOMP_OK)
    {
    	bOpenBc = 0;
    }

    PaxLog_BC(LOG_ERROR, __FUNCTION__, __LINE__, "PP_Close: %d", ret);
    return ret;
}

HF_INT32 HF_PP_Abort(void)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_Abort");
	return PP_Abort();
}

HF_INT32 HF_PP_GetInfo(HF_CHAR * psInput, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GetInfo");
    return PP_GetInfo(psInput, psOutput);
}

HF_INT32 HF_PP_DefineWKPAN(HF_CHAR * psInput, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_DefineWKPAN");
    return PP_DefineWKPAN(psInput, psOutput);
}

//29.06.2015
HF_INT32 HF_PP_Display(HF_CHAR * psMsg)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU PP_Display(%s)", psMsg);
    return PP_Display(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_DisplayEx(HF_CHAR * psMsg)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_PP_DisplayEx(%s)", psMsg);
    return PP_DisplayEx(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_StartGetKey(void)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartGetKey");
    return PP_StartGetKey();
}

//29.06.2015
HF_INT32 HF_PP_GetKey(void)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GetKey");
    return PP_GetKey();
}

HF_INT32 HF_PP_StartGetPIN(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartGetPIN");
    return PP_StartGetPIN(psInput);
}

HF_INT32 HF_PP_GetPIN(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GetPIN");
    return PP_GetPIN(psOutput, psMsgNotify);
}

//29.06.2015
HF_INT32 HF_PP_StartCheckEvent(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartCheckEvent");
    return PP_StartCheckEvent(psInput);
}

//29.06.2015
HF_INT32 HF_PP_CheckEvent(HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_CheckEvent");
    return PP_CheckEvent(psOutput);
}

HF_INT32 HF_PP_EncryptBuffer(HF_CHAR * psInput, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_EncryptBuffer");
    return PP_EncryptBuffer(psInput, psOutput);
}

HF_INT32 HF_PP_GetDUKPT(HF_CHAR * psInput, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GetDUKPT");
    return PP_GetDUKPT(psInput, psOutput);
}

HF_INT32 HF_PP_StartChipDirect(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartChipDirect");
    return PP_StartChipDirect(psInput);
}

HF_INT32 HF_PP_ChipDirect(HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_ChipDirect");
    return PP_ChipDirect(psOutput);
}

//29.06.2015
HF_INT32 HF_PP_StartRemoveCard(HF_CHAR * psMsg)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"psMsg:%s", psMsg);
    return PP_StartRemoveCard(psMsg);
}

//29.06.2015
HF_INT32 HF_PP_RemoveCard(HF_CHAR * psMsgNotify)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_RemoveCard");
    return PP_RemoveCard(psMsgNotify);
}

HF_INT32 HF_PP_StartGenericCmd(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartGenericCmd");
    return PP_StartGenericCmd(psInput);
}

HF_INT32 HF_PP_GenericCmd(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GenericCmd");
    return PP_GenericCmd(psOutput, psMsgNotify);
}

//29.06.2015
HF_INT32 HF_PP_StartGetCard(HF_CHAR * psSrvConPar, HF_CHAR * psInput)
{
	HF_INT32 iRet;

	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_PP_StartGetCard");

	iRet = controlBc();
	if(iRet != PPCOMP_OK)
	{
		return iRet;
	}

	iRet = PP_StartGetCard(psInput);
	PaxLog_BC(LOG_ERROR,__FUNCTION__, __LINE__," psInput:%s | iRet:%d",psInput,iRet);

	return iRet;
}

//29.06.2015
HF_INT32 HF_PP_GetCard(HF_CHAR * psOutput, HF_CHAR * psSrvErrMsg, HF_CHAR * psMsgNotify)
{
	HF_INT32 iRet;

	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	//questionar psSrvErrMsg

	if(psOutput == NULL || psMsgNotify == NULL)
	{
		PaxLog_BC(LOG_ERROR,__FUNCTION__, __LINE__," psOutput:%d | psMsgNotify:%d",psOutput,psMsgNotify);
		return 1;
	}

	PaxLog_BC(LOG_ERROR,__FUNCTION__, __LINE__," psOutput:%s | psMsgNotify:%s",psOutput,psMsgNotify);

	iRet = PP_GetCard(psOutput, psMsgNotify);
	PaxLog_BC(LOG_ERROR,__FUNCTION__, __LINE__," psOutput:%s | psMsgNotify:%s | iRet:%d",psOutput,psMsgNotify,iRet);

    return iRet;
}

HF_INT32 HF_PP_ResumeGetCard(void)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_ResumeGetCard");
    return PP_ResumeGetCard();
}

HF_INT32 HF_PP_ChangeParameter(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_ChangeParameter");
    return PP_ChangeParameter(psInput);
}

HF_INT32 HF_PP_StartGoOnChip(HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psTagsOpt)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_StartGoOnChip");
    return PP_StartGoOnChip(psInput, psTags, psTagsOpt);
}

HF_INT32 HF_PP_GoOnChip(HF_CHAR * psOutput, HF_CHAR * psMsgNotify)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GoOnChip");
    return PP_GoOnChip(psOutput, psMsgNotify);
}

HF_INT32 HF_PP_FinishChip(HF_CHAR * psInput, HF_CHAR * psTags, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_FinishChip");
    return PP_FinishChip(psInput, psTags, psOutput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadInit(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_TableLoadInit");
    return PP_TableLoadInit(psInput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadRec(HF_CHAR * psInput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_TableLoadRec");
    return PP_TableLoadRec(psInput);
}

//29.06.2015
HF_INT32 HF_PP_TableLoadEnd(void)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_TableLoadEnd");
    return PP_TableLoadEnd();
}

//29.06.2015
HF_INT32 HF_PP_GetTimeStamp(HF_CHAR * psInput, HF_CHAR * psOutput)
{
	PaxLog_BC(LOG_DEBUG,__FUNCTION__,__LINE__,"CHAMOU HF_PP_GetTimeStamp");
    return PP_GetTimeStamp(psInput, psOutput);
}

/*HF_INT32 Ulong2Asc(HF_INT32 x, HF_CHAR y, HF_CHAR _x)
{
	return 0;
}

HF_INT32 ulAsc2Ulong(HF_CHAR x,HF_INT32 size_x)
{
	return 0;
}*/

int APP_iTestCancel()
{
	return 0;
}

/*

int _PIN_iGetKeyInfo()
{
	return 0;
}

int _PIN_iInsKey()
{
	return 0;
}

int _PIN_iOpen()
{
	return 0;
}

int _PIN_iEncryptEx()
{
	return 0;
}

int _PIN_iEncrypt()
{
	return 0;
}
*/
