#include "HF_API.h"
#include "Xui.h"
#include "osal.h"

#define BEEP_TYPE 1
#define BEEP_TIME 100
#define BEEP_ON 1

HF_UINT8 ifBeepSwipe = 0;
HF_UINT8 ifBeepIC = 0;
HF_UINT8 ifBeepTouch = 0;
HF_UINT8 ifBeepKey = 0;
HF_UINT8 ifBeepClss = 0;

Int16 POS_iDisplayImage(Int16 iX, Int16 iY, Uint32 ulSize, const Byte *pbPtr)
{
    HF_INT32 iRet = 0;
	XuiImg *imgWallet = NULL;
	XuiWindow *rootCanvas = NULL;

	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"==============ARGUMENTS===============");
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"iX:%d",iX);
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"iY:%d",iY);
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"Size:%d",ulSize);


	//check Xui running status
	if(0 == XuiIsRunning())
	{
		//TODO
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"2. POS_iDisplayImage[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	//get Xui root canvas
	rootCanvas = XuiRootCanvas();
	if(NULL == rootCanvas)
	{
		//TODO
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"4. POS_iDisplayImage[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}

	//check input x and y, if one of them is a negative value, return -1.
	if((iX < 0) || (iX > rootCanvas->width) || (iY < 0) || (iY > rootCanvas->height) || (ulSize < 0) || (pbPtr == NULL))
	{
		//TODO
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"5. POS_iDisplayImage[%d]", HF_ERR_INVALIDARG);
		return HF_ERR_INVALIDARG;
	}
	//Xui load image from memory
	imgWallet = XuiImgLoadFromMem((unsigned char*)pbPtr, ulSize, 0);
	if(NULL == imgWallet)
	{
		//TODO
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"6. POS_iDisplayImage[%d]", HF_ERR_DEVICEFAULT);
		return HF_ERR_DEVICEFAULT;
	}
	//draw image
	iRet = XuiCanvasDrawImg(rootCanvas, iX, iY, imgWallet->width, imgWallet->height, XUI_BG_NORMAL, imgWallet);
	if(iRet < 0)
	{
		//TODO
		//if error, destroy image buffer before return
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"7. POS_iDisplayImage[%d]", HF_ERR_DEVICEFAULT);
		XuiImgFree(imgWallet);
		return HF_ERR_DEVICEFAULT;
	}
	//TODO
	XuiImgFree(imgWallet);
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU POS_iDisplayImage");
	return 0;
}

Int16 POS_iEraseImageNext(void)
{
    // NOT USED
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"");
	return 0;
}

Int32 POS_iGetKeystroke(Int32 timeout)
{
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"");
    return HF_keyboard_getkeystroke(timeout);
}

/*
 * return: 0:success  -1:error
 */

static int waitEventInit(RF_UINT16 events)
{
	HF_INT32 iRet = 0;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//if events contains swipe event
	if(HF_CARDREADER_SWIPE_EVENT == (events & HF_CARDREADER_SWIPE_EVENT))
	{
		iRet = OsMsrOpen();	//open msr
		if(RET_OK != iRet)
		{
			//TODO
			return HF_ERR_DEVICEFAULT;
		}
//		OsMsrReset();
	}
	//if events contains chip event
	if(HF_CARDREADER_CHIP_EVENT == (events & HF_CARDREADER_CHIP_EVENT))
	{
		iRet = OsIccOpen(ICC_USER_SLOT);
		if(RET_OK != iRet)
		{
			//TODO
			return HF_ERR_DEVICEFAULT;
		}
	}
	//if events contains touch event
	if(HF_SCREEN_TOUCH_EVENT == (events & HF_SCREEN_TOUCH_EVENT))
	{
		//empty, D200 not support
	}
	//if events contains keyboard event
	if(HF_KEYBOARD_EVENT == (events & HF_KEYBOARD_EVENT))
	{
		//empty, key get from Xui, it will init in systemInit()
	}
	//if events contains contactless  event
	if(HF_CARDREADER_CLESS_EVENT == (events & HF_CARDREADER_CLESS_EVENT))
	{
		iRet = OsPiccOpen();
		if(RET_OK != iRet)
		{
			//TODO
			return HF_ERR_DEVICEFAULT;
		}
	}
	//TODO
	return 0;
}

static void waitEventClose(RF_UINT16 events)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//if events contains swipe event
	if(HF_CARDREADER_SWIPE_EVENT == (events & HF_CARDREADER_SWIPE_EVENT))
	{
		OsMsrClose();
	}
	//if events contains chip event
	if(HF_CARDREADER_CHIP_EVENT == (events & HF_CARDREADER_CHIP_EVENT))
	{
		OsIccClose(ICC_USER_SLOT);
	}
	//if events contains touch event
	if(HF_SCREEN_TOUCH_EVENT == (events & HF_SCREEN_TOUCH_EVENT))
	{
		//empty, not support
	}
	//if events contains keyboard event
	if(HF_KEYBOARD_EVENT == (events & HF_KEYBOARD_EVENT))
	{
		//empty, Xui will be closed by systemClose()
	}
	//if events contains contactless  event
	if(HF_CARDREADER_CLESS_EVENT == (events & HF_CARDREADER_CLESS_EVENT))
	{
		OsPiccClose();
	}
}

/*
 * return: 1:swipe card
 * 		   2:chip card
 * 		   4:touch screen
 * 		   8:keyboard
 * 		   16:contactless card
 * 		   0:no event
 */
static int waitEventCheck(RF_UINT16 events, RF_VOID** outputs)
{
	HF_INT32 iRet = 0;
	HF_INT32 iKey = 0;
	HF_INT8 pcPiccType = '0';
	HF_UINT8 pucATQx[12] = {0};
	struct POS_iWaitEvents_SWIPE_EVENT_DATA *swipeEventData = NULL;
	ST_MSR_DATA track1 = {{'0'}, 0, 0}, track2 = {{'0'}, 0, 0}, track3 = {{'0'}, 0, 0};
	struct POS_iWaitEvents_KEYBOARD_EVENT_DATA *keyboardEventData = NULL;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//if events contains swipe event
	if(HF_CARDREADER_SWIPE_EVENT == (events & HF_CARDREADER_SWIPE_EVENT))
	{
		//PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"HF_CARDREADER_SWIPE_EVENT");

		iRet = OsMsrSwiped();
		if(1 == iRet) //can't find define TRUE, use 1 to instead
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"OsMsrSwiped return true");

			if(RET_OK == OsMsrRead(&track1,&track2,&track3))
			{
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"OsMsrRead return ok");

				swipeEventData = (struct POS_iWaitEvents_SWIPE_EVENT_DATA*)malloc(sizeof(struct POS_iWaitEvents_SWIPE_EVENT_DATA));
				if(NULL == swipeEventData)
				{
					//TODO
					//malloc error
					return HF_ERR_NOMEMORY;
				}
				memset(swipeEventData, 0, sizeof(struct POS_iWaitEvents_SWIPE_EVENT_DATA));

				swipeEventData->track1 = (Int8*)malloc(track1.DataLen);
				if(NULL == swipeEventData->track1)
				{
					//TODO
					//malloc error
					free(swipeEventData);
					swipeEventData = NULL;
					return HF_ERR_NOMEMORY;
				}
				memset(swipeEventData->track1, 0, sizeof(track1.DataLen));

				swipeEventData->track2 = (Int8*)malloc(track2.DataLen);
				if(NULL == swipeEventData->track2)
				{
					//TODO
					//malloc error
					free(swipeEventData);
					free(swipeEventData->track1);
					swipeEventData = NULL;
					swipeEventData->track1 = NULL;
					return HF_ERR_NOMEMORY;
				}
				memset(swipeEventData->track2, 0, sizeof(track2.DataLen));

				swipeEventData->track3 = (Int8*)malloc(track3.DataLen);
				if(NULL == swipeEventData->track3)
				{
					//TODO
					//malloc error
					free(swipeEventData);
					free(swipeEventData->track1);
					free(swipeEventData->track2);
					swipeEventData = NULL;
					swipeEventData->track1 = NULL;
					swipeEventData->track2 = NULL;
					return HF_ERR_NOMEMORY;
				}
				memset(swipeEventData->track3, 0, sizeof(track3.DataLen));

				memcpy(swipeEventData->track1, track1.TrackData, track1.DataLen);
				memcpy(swipeEventData->track2, track2.TrackData, track2.DataLen);
				memcpy(swipeEventData->track3, track3.TrackData, track3.DataLen);

				*outputs = swipeEventData;

				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"read data");
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track1 len=%d",track1.DataLen);
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track2 len=%d",track2.DataLen);
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track3 len=%d",track3.DataLen);
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track1=%s",swipeEventData->track1);
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track2=%s",swipeEventData->track2);
				PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"track3=%s",swipeEventData->track3);

//				OsMsrClose();
				//beep
				if(BEEP_ON == ifBeepSwipe)
				{
					OsBeep(BEEP_TYPE, BEEP_TIME);
					PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"OsBeep with : %d -- %d",BEEP_TYPE,BEEP_TIME);
				}
				return HF_CARDREADER_SWIPE_EVENT;
			}
		}
	}
	//if events contains chip event
	if(HF_CARDREADER_CHIP_EVENT == (events & HF_CARDREADER_CHIP_EVENT))
	{
		iRet =  OsIccDetect(ICC_USER_SLOT);

		if(RET_OK == iRet)
		{
//			OsIccClose(ICC_USER_SLOT);
			//beep
			if(BEEP_ON == ifBeepIC)
			{
				OsBeep(BEEP_TYPE, BEEP_TIME);
			}
			return HF_CARDREADER_CHIP_EVENT;
		}
	}
	//if events contains touch event
	if(HF_SCREEN_TOUCH_EVENT == (events & HF_SCREEN_TOUCH_EVENT))
	{
//		return HF_SCREEN_TOUCH_EVENT;
	}
	//if events contains keyboard event
	if(HF_KEYBOARD_EVENT == (events & HF_KEYBOARD_EVENT))
	{
		iRet = XuiHasKey();
		if(1 == iRet)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"XuiHasKey return true");

			iKey = XuiGetKey();
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"XuiGetKey return %d",iKey);
			keyboardEventData = (struct POS_iWaitEvents_KEYBOARD_EVENT_DATA *)malloc(sizeof(struct POS_iWaitEvents_KEYBOARD_EVENT_DATA));
			if(NULL == keyboardEventData)
			{
				//TODO
				//malloc error
				return HF_ERR_NOMEMORY;
			}
			memset(keyboardEventData, 0, sizeof(keyboardEventData));

			switch(iKey){
			case XUI_KEY1:
				keyboardEventData->key = HF_KEY_ONE;
				break;
			case XUI_KEY2:
				keyboardEventData->key = HF_KEY_TWO;
				break;
			case XUI_KEY3:
				keyboardEventData->key = HF_KEY_THREE;
				break;
			case XUI_KEY4:
				keyboardEventData->key = HF_KEY_FOUR;
				break;
			case XUI_KEY5:
				keyboardEventData->key = HF_KEY_FIVE;
				break;
			case XUI_KEY6:
				keyboardEventData->key = HF_KEY_SIX;
				break;
			case XUI_KEY7:
				keyboardEventData->key = HF_KEY_SEVEN;
				break;
			case XUI_KEY8:
				keyboardEventData->key = HF_KEY_EIGHT;
				break;
			case XUI_KEY9:
				keyboardEventData->key = HF_KEY_NINE;
				break;
			case XUI_KEY0:
				keyboardEventData->key = HF_KEY_ZERO;
				break;
			case XUI_KEYCANCEL:
				keyboardEventData->key = HF_KEY_CANCEL;
				break;
			case XUI_KEYCLEAR:
				keyboardEventData->key = HF_KEY_CLEAR;
				break;
			case XUI_KEYENTER:
				keyboardEventData->key = HF_KEY_ENTER;
				break;
			case XUI_KEYALPHA:
//				keyboardEventData->key = HF_KEY_ONE;
				break;
			case XUI_KEYSHARP:
//				keyboardEventData->key = HF_KEY_ONE;
				break;
			case XUI_KEYF1:
				// Jarod@2015/08/29 UPD START
				//keyboardEventData->key = HF_KEY_F1;
				// Jarod@2015/08/29 UPD END
				keyboardEventData->key = HF_KEY_F1;
				break;
			case XUI_KEYF2:
				// Jarod@2015/08/29 UPD START
				//keyboardEventData->key = HF_KEY_F2;
				// Jarod@2015/08/29 UPD END
				keyboardEventData->key = HF_KEY_F2;
				break;
			case XUI_KEYF3:
				keyboardEventData->key = HF_KEY_F3;
				break;
			case XUI_KEYF4:
				keyboardEventData->key = HF_KEY_F4;
				break;
			case XUI_KEYFUNC:
// Jarod@2015/09/14 UPD START
//				keyboardEventData->key = HF_KEY_ONE;
				keyboardEventData->key = HF_KEY_HASH;
// Jarod@2015/09/14 UPD END
				break;
			case XUI_KEYUP:
			    // Jarod@2015/08/29 UPD START
				keyboardEventData->key = HF_KEY_F1;
				// Jarod@2015/08/29 UPD END
				break;
			case XUI_KEYDOWN:
				// Jarod@2015/08/29 UPD START
				keyboardEventData->key = HF_KEY_F2;
				// Jarod@2015/08/29 UPD END
				break;
			case XUI_KEYMENU:
				keyboardEventData->key = HF_KEY_MENU;
				break;
			default:
				//empty
				break;
			}

			*outputs = keyboardEventData;
			//beep
			if(BEEP_ON == ifBeepKey)
			{
				OsBeep(BEEP_TYPE, BEEP_TIME);
			}
			return HF_KEYBOARD_EVENT;
		}
	}
	//if events contains contactless  event
	if(HF_CARDREADER_CLESS_EVENT == (events & HF_CARDREADER_CLESS_EVENT))
	{
		if(0 == OsPiccPoll(&pcPiccType, pucATQx))
		{
//			OsPiccClose();

			//beep
			if(BEEP_ON == ifBeepClss)
			{
				OsBeep(BEEP_TYPE, BEEP_TIME);
			}
			return HF_CARDREADER_CLESS_EVENT;
		}
	}
	//TODO
	return 0;
}

Int32 POS_iWaitEvents(Int32 timeout, Uint16 events, void** outputs)
{
    HF_INT32 iRet = 0;
	HF_INT32 iRetEvent = 0;
	ST_TIMER timer = {0, 0, 0};

	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//judge parameter legal or not
	if((events < HF_CARDREADER_SWIPE_EVENT) || (events > (HF_CARDREADER_SWIPE_EVENT | HF_CARDREADER_CHIP_EVENT | HF_SCREEN_TOUCH_EVENT | HF_KEYBOARD_EVENT | HF_CARDREADER_CLESS_EVENT)))
	{
		//TODO
		return HF_ERR_INVALIDARG;
	}
	if(NULL == outputs)
	{
		//TODO
		return HF_ERR_INVALIDARG;
	}


	//find out which events user want to wait and init device
	iRet = waitEventInit(events);
	if(iRet < 0)
	{
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"waitEventInit not ok:%d",iRet);
		waitEventClose(events);
		//TODO
		return iRet;
	}
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"waitEventInit ok");

	//if timeout > 0,means maximum waiting time of the event
	if(timeout > 0)
	{
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"inside timeout>0");
		//set timer to count down
		if(RET_OK != OsTimerSet(&timer, timeout*1000))
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"creatTimer error");
			waitEventClose(events);
			//TODO
			return HF_ERR_INVALIDARG;
		}
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"creatTimer ok");
		while(1)
		{
			//check timer
			if(0 == OsTimerCheck(&timer))
			{
				waitEventClose(events);
				//TODO
				//0 means time out
				return 0;
			}

			//check event
			iRetEvent = waitEventCheck(events, outputs);
			if(0 != iRetEvent)
			{
				PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"1.waitEventCheck return Event:%d",iRetEvent);

				waitEventClose(events);
				//TODO
				return iRetEvent;
			}
		}
	}
	//if timeout = 0,means the function should check if there were any event and return immediately
	else if(0 == timeout)
	{
		iRetEvent = waitEventCheck(events, outputs);
		PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"2.waitEventCheck return Event:%d",iRetEvent);
		if(0 != iRetEvent)
		{
			waitEventClose(events);
			//TODO
			return iRetEvent;
		}
	}
	//if timeout < 0,means the function only return when event captured
	else if(timeout < 0)
	{
		while(1)
		{
			iRetEvent = waitEventCheck(events, outputs);
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"3.waitEventCheck return Event:%d",iRetEvent);
			if(0 != iRetEvent)
			{
				waitEventClose(events);
				//TODO
				return iRetEvent;
			}
		}
	}
	waitEventClose(events);
	//TODO
	//0 means time out
	return 0;
}

RF_INT32 POS_iWaitEvents_options(RF_UINT32 option, RF_UINT16 events, RF_VOID* option_val)
{
	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//judge parameter legal or not
	if(RF_IWAIT_EVTS_OPTION_BEEP != option)
	{
		//TODO
		//option parameter error
		return HF_ERR_INVALIDARG;
	}
	if((events < HF_CARDREADER_SWIPE_EVENT) || (events > (HF_CARDREADER_SWIPE_EVENT | HF_CARDREADER_CHIP_EVENT | HF_SCREEN_TOUCH_EVENT | HF_KEYBOARD_EVENT | HF_CARDREADER_CLESS_EVENT)))
	{
		//TODO
		//events parameter error
		return HF_ERR_INVALIDARG;
	}
	if(!((0 == *(HF_UINT8*)option_val) || (1 == *(HF_UINT8*)option_val)))
	{
		//TODO
		//option_val parameter error
		return HF_ERR_INVALIDARG;
	}

	if(RF_IWAIT_EVTS_OPTION_BEEP == option)
	{
		if((0 == *(HF_UINT8*)option_val) || (1 == *(HF_UINT8*)option_val))
		{
			//if events contains swipe event
			if(HF_CARDREADER_SWIPE_EVENT == (events & HF_CARDREADER_SWIPE_EVENT))
			{
				ifBeepSwipe = *(HF_UINT8*)option_val;
			}
			//if events contains chip event
			if(HF_CARDREADER_CHIP_EVENT == (events & HF_CARDREADER_CHIP_EVENT))
			{
				ifBeepIC = *(HF_UINT8*)option_val;
			}
			//if events contains touch event
			if(HF_SCREEN_TOUCH_EVENT == (events & HF_SCREEN_TOUCH_EVENT))
			{
				ifBeepTouch = *(HF_UINT8*)option_val;
			}
			//if events contains keyboard event
			if(HF_KEYBOARD_EVENT == (events & HF_KEYBOARD_EVENT))
			{
				ifBeepKey = *(HF_UINT8*)option_val;
			}
			//if events contains contactless  event
			if(HF_CARDREADER_CLESS_EVENT == (events & HF_CARDREADER_CLESS_EVENT))
			{
				ifBeepClss = *(HF_UINT8*)option_val;
			}
		}
	}
	//TODO
	//0 means success
	return 0;
}
RF_INT32 POS_iWaitEvents_get_options(RF_UINT32 option, RF_UINT16 events, RF_VOID** option_val)
{
	HF_INT32 iRet = 0;
	HF_UINT8 *beepOrNot = NULL;
	HF_UINT8 *supportEvent = NULL;
	HF_INT8 value[64] = "";
	HF_UINT16 supportList = 0;

	PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"");

	//judge parameter legal or not
	if(!((RF_IWAIT_EVTS_OPTION_BEEP == option) || (RF_IWAIT_EVTS_OPTION_CAPABILITIES == option)))
	{
		//TODO
		//option parameter error
		return HF_ERR_INVALIDARG;
	}
	if(RF_IWAIT_EVTS_OPTION_BEEP == option)
	{
		if(!((HF_CARDREADER_SWIPE_EVENT == events) || (HF_CARDREADER_CHIP_EVENT == events) || (HF_SCREEN_TOUCH_EVENT == events) || (HF_KEYBOARD_EVENT == events) || (HF_CARDREADER_CLESS_EVENT == events)))
		{
			//TODO
			//events parameter error
			return HF_ERR_INVALIDARG;
		}
	}
	if(NULL == option_val)
	{
		//TODO
		//option_val parameter error
		return HF_ERR_INVALIDARG;
	}

	if(RF_IWAIT_EVTS_OPTION_BEEP == option)
	{
		beepOrNot = (HF_UINT8*)malloc(sizeof(HF_UINT8));
		if(NULL == beepOrNot)
		{
			//TODO
			//malloc error
			return HF_ERR_NOMEMORY;
		}
		memset(beepOrNot, 0, sizeof(HF_UINT8));

		//if events contains swipe event
		if(HF_CARDREADER_SWIPE_EVENT == (events & HF_CARDREADER_SWIPE_EVENT))
		{
			*beepOrNot = ifBeepSwipe;
			*option_val = beepOrNot;
		}
		//if events contains chip event
		if(HF_CARDREADER_CHIP_EVENT == (events & HF_CARDREADER_CHIP_EVENT))
		{
			*beepOrNot = ifBeepIC;
			*option_val = beepOrNot;
		}
		//if events contains touch event
		if(HF_SCREEN_TOUCH_EVENT == (events & HF_SCREEN_TOUCH_EVENT))
		{
			*beepOrNot = ifBeepTouch;
			*option_val = beepOrNot;
		}
		//if events contains keyboard event
		if(HF_KEYBOARD_EVENT == (events & HF_KEYBOARD_EVENT))
		{
			*beepOrNot = ifBeepKey;
			*option_val = beepOrNot;
		}
		//if events contains contactless  event
		if(HF_CARDREADER_CLESS_EVENT == (events & HF_CARDREADER_CLESS_EVENT))
		{
			*beepOrNot = ifBeepClss;
			*option_val = beepOrNot;
		}
	}
	else if(RF_IWAIT_EVTS_OPTION_CAPABILITIES == option)
	{
		//swipe card:		[ro.fac.msr]: [E-MAG_MH1601]
		//chip card:		[ro.fac.sci]: [06]
		//touch screen: 	[ro.fac.touchscreen]
		//keyboard:			[ro.fac.keybroad]: [1]
		//contactless:		[ro.fac.pcd]: [03]
		//if OsRegGetValue return 0, means key not exist or value is empty

		supportEvent = (HF_UINT8*)malloc(sizeof(HF_UINT8));
		if(NULL == supportEvent)
		{
			//TODO
			//malloc error
			return HF_ERR_NOMEMORY;
		}
		memset(supportEvent, 0, sizeof(HF_UINT8));

		iRet = OsRegGetValue("ro.fac.msr", value);
		if(iRet > 0)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"swipe card event supported!");
			supportList = supportList | HF_CARDREADER_SWIPE_EVENT;
		}
		iRet = OsRegGetValue("ro.fac.sci", value);
		if(iRet > 0)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"chip card event supported!");
			supportList = supportList | HF_CARDREADER_CHIP_EVENT;
		}
		iRet = OsRegGetValue("ro.fac.touchscreen", value);
		if(iRet > 0)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"touchscreen event supported!");
			supportList = supportList | HF_SCREEN_TOUCH_EVENT;
		}
		iRet = OsRegGetValue("ro.fac.keybroad", value);
		if(iRet > 0)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"hardware keyboard event supported!");
			supportList = supportList | HF_KEYBOARD_EVENT;
		}
		iRet = OsRegGetValue("ro.fac.pcd", value);
		if(iRet > 0)
		{
			PaxLog_Module(LOG_DEBUG,__FUNCTION__,__LINE__,"contactless event supported!");
			supportList = supportList | HF_CARDREADER_CLESS_EVENT;
		}

		*supportEvent = supportList;
		*option_val = supportEvent;
	}
	//TODO
	return 0;
}


