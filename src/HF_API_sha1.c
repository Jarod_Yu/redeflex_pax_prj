#include "HF_API.h"
#include <openssl/sha.h>

static HF_BOOL HFI_SHA1_initialized = HF_FALSE;
static HF_INT8 HFl_SHA1_result[20] = { 0 };
static SHA_CTX HFl_SHA1_context = {0};

HF_INT32 HF_sha1_hash(const HF_VOID *data, HF_UINT32 length, HF_INT8 *result)
{
    if (HF_NULL == data || HF_NULL == result || 0 == length)
    {
        return HF_ERR_INVALIDARG;
    }

    OsSHA(SHA_TYPE_1, data, (HF_INT32) length, (HF_UINT8 *) result);
    return HF_SUCCESS;
}

HF_INT32 HF_sha1_chainInit(HF_VOID)
{
    if (HFI_SHA1_initialized != HF_FALSE)
    {
        return HF_ERR_RESOURCEALLOC;
    }

    HFI_SHA1_initialized = HF_TRUE;
    memset(HFl_SHA1_result, 0, sizeof(HFl_SHA1_result));
    if (1 != SHA1_Init(&HFl_SHA1_context))    // SHA1_Init (open ssl library method) returns 1 if success
    {
        return HF_ERR_DEVICEFAULT;
    }
    return HF_SUCCESS;
}

HF_INT32 HF_sha1_chainUpdate(const HF_VOID *data, HF_UINT32 length)
{
    if (data == HF_NULL || length == 0)
    {
        return HF_ERR_INVALIDARG;
    }
    if (HFI_SHA1_initialized != HF_TRUE)
    {
        return HF_ERR_INVALIDSTATE;
    }

    if (1 != SHA1_Update(&HFl_SHA1_context, data, length))  // SHA1_Update (open ssl library method) returns 1 if success
    {
        return HF_ERR_DEVICEFAULT;
    }

    return HF_SUCCESS;
}

HF_INT32 HF_sha1_chainResult(HF_INT8 *result)
{
    // Jarod@2015/07/27 UPD START
    if (result == HF_NULL)
    {
        return HF_ERR_INVALIDARG;
    }
    // Jarod@2015/07/27 UPD END

    if (HFI_SHA1_initialized != HF_TRUE)
    {
        return HF_ERR_INVALIDSTATE;
    }

    if (1 != SHA1_Final((HF_UINT8 *)result, &HFl_SHA1_context))  // SHA1_Final (open ssl library method) returns 1 if success
    {
        return HF_ERR_DEVICEFAULT;
    }

    HFI_SHA1_initialized = HF_FALSE;
    return HF_SUCCESS;
}
