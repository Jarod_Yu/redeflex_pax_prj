#define DO_NOT_REDEFINE_GSM_CSD_FUNCTIONS

#include "HF_API.h"
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <osal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<ctype.h>

static RF_INT32 OsWlPortOpen(const RF_CHAR *Attr);
static RF_INT32 OsWlPortSend(const RF_VOID *SendBuf, RF_INT32 SendLen);
static RF_INT32 OsWlPortRecv(RF_VOID *RecvBuf, RF_INT32 RecvLen, RF_INT32 TimeoutMs);
static RF_VOID OsWlPortClose();
static RF_VOID OsWlPortReset();
static RF_VOID time_add_ms(struct timeval *tv, RF_INT32 ms);
static RF_INT32 time_compare(struct timeval tv1, struct timeval tv2);
static RF_INT32 parse_attr(const RF_CHAR *attr, RF_INT32 *speed, RF_INT32 *databits, RF_INT32 *parity, RF_INT32 *stopbits);
static RF_INT32 SetOpt(RF_INT32 fd, RF_INT32 nSpeed, RF_INT32 nBits, RF_CHAR nEvent, RF_INT32 nStop);

#define TIMER_TEMPORARY		4       // Temporary timer(Shared by different modules
#define CONFIG_INFO_MAXLENGTH			49

static RF_GSM_GPRS_CONFIG *s_stGsmGprsConfig = NULL;
static RF_BOOL bInitGprs = 0;
static RF_INT32 fp_uart = -1;
static RF_BOOL bCallConfig = 0;

typedef struct _NET_WORK_INFO
{
	char code[6];
	char operator[64];
}NET_WORK_INFO;

const NET_WORK_INFO stNetWorkInfo[] =
{
	{"72402", "TIM BRASIL"},
	{"72403", "TIM BRASIL"},
	{"72404", "TIM BRASIL"},
	{"72405", "Claro"},
	{"72416", "BrTCel"},
	{"00000", "UNKNOWN"}
};

/*
RF_INT32 RF_csd_config(RF_GSM_CSD_CONFIG* config)
{
	return 0;
}
RF_INT32 RF_csd_connect(const RF_CHAR* phonenumber, RF_INT32 timeout, RF_INT32 maxIterations)
{
	return 0;
}

RF_INT32 RF_csd_disconnect(RF_VOID)
{
	return 0;
}

RF_INT32 RF_csd_send(const RF_UINT8* msg, RF_UINT16 length)
{
	return 0;
}

RF_INT32 RF_csd_recv(RF_UINT8* data, RF_UINT16 maxlen, RF_UINT32 timeout)
{
	return 0;
}
*/

RF_INT32 RF_csd_config(RF_GSM_CSD_CONFIG* config)
{
    return RF_SUCCESS;
}

RF_INT32 RF_csd_connect(const RF_CHAR* phonenumber, RF_INT32 timeout, RF_INT32 maxIterations)
{
    return RF_SUCCESS;
}

RF_INT32 RF_csd_disconnect(RF_VOID)
{
    return RF_SUCCESS;
}

RF_INT32 RF_csd_send(const RF_UINT8* msg, RF_UINT16 length)
{
    return RF_SUCCESS;
}

RF_INT32 RF_csd_recv(RF_UINT8* data, RF_UINT16 maxlen, RF_UINT32 timeout)
{
    return RF_SUCCESS;
}

RF_INT32 RF_csd_up(RF_VOID)
{
	return RF_SUCCESS;
}
RF_INT32 RF_csd_down(RF_VOID)
{
	return RF_SUCCESS;
}

RF_INT32 RF_gprs_config(RF_GSM_GPRS_CONFIG *config)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. ENTROU 1. RF_gprs_config");
	if (NULL==config || NULL==config->apn || NULL==config->login || NULL==config->password)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. RF_ERR_INVALIDARG");
		return RF_ERR_INVALIDARG;
	}

	if (strlen(config->apn)>CONFIG_INFO_MAXLENGTH || strlen(config->login)>CONFIG_INFO_MAXLENGTH ||
		strlen(config->login)>CONFIG_INFO_MAXLENGTH)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. RF_ERR_INVALIDARG");
		return RF_ERR_INVALIDARG;
	}

	if (NULL==s_stGsmGprsConfig || NULL==s_stGsmGprsConfig->apn ||
		NULL==s_stGsmGprsConfig->login || NULL==s_stGsmGprsConfig->password)
	{
		s_stGsmGprsConfig = malloc(sizeof(RF_GSM_GPRS_CONFIG));
		s_stGsmGprsConfig->apn = malloc(strlen(config->apn) + 1);
		s_stGsmGprsConfig->login = malloc(strlen(config->login) + 1);
		s_stGsmGprsConfig->password = malloc(strlen(config->password) + 1);
	}
	else
	{
		free(s_stGsmGprsConfig->password);
		s_stGsmGprsConfig->password = NULL;
		free(s_stGsmGprsConfig->login);
		s_stGsmGprsConfig->login = NULL;
		free(s_stGsmGprsConfig->apn);
		s_stGsmGprsConfig->apn = NULL;
		free(s_stGsmGprsConfig);
		s_stGsmGprsConfig = NULL;

		s_stGsmGprsConfig = malloc(sizeof(RF_GSM_GPRS_CONFIG));
		s_stGsmGprsConfig->apn = malloc(strlen(config->apn) + 1);
		s_stGsmGprsConfig->login = malloc(strlen(config->login) + 1);
		s_stGsmGprsConfig->password = malloc(strlen(config->password) + 1);
	}
	if (NULL== s_stGsmGprsConfig || NULL==s_stGsmGprsConfig->apn ||
		NULL==s_stGsmGprsConfig->login || NULL==s_stGsmGprsConfig->password)
	{
		return HF_ERR_NOMEMORY;
	}
	//TODO, it's meaningless about the reconnect_wait_mseg
	s_stGsmGprsConfig->reconnect_wait_mseg = config->reconnect_wait_mseg;
	strcpy(s_stGsmGprsConfig->apn, config->apn);
	strcpy(s_stGsmGprsConfig->login, config->login);
	strcpy(s_stGsmGprsConfig->password, config->password);
	memcpy(&s_stGsmGprsConfig->modem_startup, &config->modem_startup, sizeof(RF_GSM_CONFIG_MODEM_STARTUP));
	memcpy(&s_stGsmGprsConfig->pdp_ctx, &config->pdp_ctx, sizeof(RF_GPRS_CONFIG_PDP_CTX));
	memcpy(&s_stGsmGprsConfig->signal_wait, &config->signal_wait, sizeof(RF_GSM_CONFIG_SIGNAL));
	OsLog(LOG_ERROR, "%s--%d, apn:%s, login:%s, password:%s,\
		  reconnect_wait_mseg:%d, start_modem_try_num:%d, wait_modem_mseg:%d, ber_maximo:%d, rssi_minimo:%d,\
		  timeout_signal_wait_mseg:%d, pdp_ctx_try_num:%d, wait_pdp_try_mseg:%d, timeout_pdp_try_mseg:%d",
		  __FILE__, __LINE__, s_stGsmGprsConfig->apn, s_stGsmGprsConfig->login, s_stGsmGprsConfig->password,
		  s_stGsmGprsConfig->reconnect_wait_mseg, s_stGsmGprsConfig->modem_startup.start_modem_try_num,
		  s_stGsmGprsConfig->modem_startup.wait_modem_mseg, s_stGsmGprsConfig->signal_wait.ber_maximo,
		  s_stGsmGprsConfig->signal_wait.rssi_minimo, s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg,
		  s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num, s_stGsmGprsConfig->pdp_ctx.wait_pdp_try_mseg,
		  s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg);

	//ready to delete pdp setting
	if (s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg > 30 || s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg < 5)
	{
		s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg = 30;
	}

	if (s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num < 2 || s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num > 4)
	{
		s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num = 4;
	}

//	if (s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg <= 0 ||s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg > 8)
	if (s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg<0 ||
		s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg>60000)
	{
		s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg = 30000;
	}

	if (s_stGsmGprsConfig->signal_wait.rssi_minimo < 10 || s_stGsmGprsConfig->signal_wait.rssi_minimo >= 31)
	{
		s_stGsmGprsConfig->signal_wait.rssi_minimo = 10;
	}

	if (s_stGsmGprsConfig->modem_startup.start_modem_try_num<0 || s_stGsmGprsConfig->modem_startup.start_modem_try_num>5 )
	{
		//3->5
		s_stGsmGprsConfig->modem_startup.start_modem_try_num = 5;
	}
	//second -> ms
	if (s_stGsmGprsConfig->modem_startup.wait_modem_mseg<0 || s_stGsmGprsConfig->modem_startup.wait_modem_mseg >5000)
	{
		//3->500
		s_stGsmGprsConfig->modem_startup.wait_modem_mseg = 500;
	}




	bCallConfig = 1;

	return RF_SUCCESS;
}

RF_INT32 RF_gprs_up(RF_VOID)
{
	RF_INT32 iRet = -1, iRetryTime = -1;
	RF_UCHAR ucRecvBuf[255] = "\0", ucSendBuf[255] = "\0";

	if (0==bCallConfig)
	{
		return RF_ERR_INVALIDSTATE;
	}
	iRet = OsWlLock();
	OsLog(LOG_ERROR, "%s-%d, OsWlLock, iRet:%d", __FILE__, __LINE__, iRet);
	if (0 != iRet)
	{
		if (ERR_DEV_NOT_EXIST == iRet)
		{
			return HF_ERR_DEVICEFAULT;
		}
		else if (ERR_DEV_BUSY == iRet)
		{
			return HF_ERR_GPRS_DEV_BUSY;
		}
		else if (ERR_BATTERY_ABSENT == iRet)
		{
			return HF_ERR_GPRS_BATTERY_ABSENT;
		}
		else
		{
			//TODO
			return iRet;
		}
	}

	// TODO, NULL is just for now
	iRet = OsWlInit(NULL);
	OsLog(LOG_ERROR, "%s-%d, OsWlInit, iRet:%d", __FILE__, __LINE__, iRet);
	if (0 != iRet)
	{
		//TODO
		return RF_ERR_INVALIDARG;
	}
	bInitGprs = 1;

	iRet = OsWlPortOpen("115200,8,n,1");
	OsLog(LOG_ERROR, "%s--%d iRet:%d, ", __FILE__, __LINE__, iRet);
	if (0 != iRet)
	{
		//TODO
		return RF_ERR_DEVICEFAULT;
	}

	//FIXME, ready to delete this AT command
	sprintf(ucSendBuf, "AT+GTSET=\"ACTRETRY\",%d,%d\r",
			s_stGsmGprsConfig->pdp_ctx.timeout_pdp_try_mseg, s_stGsmGprsConfig->pdp_ctx.pdp_ctx_try_num);
	OsWlPortSend(ucSendBuf, strlen(ucSendBuf));
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		//TODO
		return RF_ERR_DEVICEFAULT;
	}

	OsWlPortClose();

	//TODO
	iRetryTime = s_stGsmGprsConfig->modem_startup.start_modem_try_num;
	while(iRetryTime--)
	{
		//TODO
		iRet = OsWlLogin(s_stGsmGprsConfig->apn, s_stGsmGprsConfig->login, s_stGsmGprsConfig->password, PPP_ALG_PAP, 0, 300000, 0);
		OsLog(LOG_ERROR, "%s--%d, OsWlLogin, iRet;%d", __FILE__, __LINE__);
		if (0 == iRet || PPP_LOGINING == iRet)
		{
			break;
		}
		else
		{
//			sleep(s_stGsmGprsConfig->modem_startup.wait_modem_mseg);
			OsSleep(s_stGsmGprsConfig->modem_startup.wait_modem_mseg);
			continue;
		}
	}

	if (0 == iRet || PPP_LOGINING == iRet)
	{
		//TODO
		return RF_SUCCESS;
	}
	else if (ERR_INVALID_PARAM == iRet || ERR_NET_PASSWD == iRet)
	{
		return RF_ERR_INVALIDARG;
	}
	else
	{
		return RF_ERR_DEVICEFAULT;
	}

}

RF_INT32 RF_gprs_down(RF_VOID)
{
	RF_INT32 iRet;
//	ST_TIMER st_timer = {0,0,0};

	iRet = OsWlCheck();
	OsLog(LOG_ERROR, "%s-%d, OsWlCheck, iRet:%d", __FILE__, __LINE__, iRet);
	if (ERR_NET_LOGOUT==iRet || ERR_NET_IF==iRet)
	{
		//TODO
		return RF_SUCCESS;
	}

	iRet = OsWlLogout();
	OsLog(LOG_ERROR, "%s-%d, OsWlLogout, iRet:%d", __FILE__, __LINE__, iRet);
	if (ERR_DEV_NOT_OPEN == iRet)
	{
		return RF_ERR_DEVICEFAULT;
	}
	while (1)
	{
		sleep(1);
		iRet = OsWlCheck();
		if (PPP_LOGOUTING != iRet)
		{
			break;
		}
	}

	OsWlUnLock();

	bInitGprs = 0;

    return RF_SUCCESS;
}

RF_INT32 RF_gsm_getInfo(RF_GSM_INFO* gsmInfo)
{
	if (NULL==gsmInfo)
	{
		return RF_ERR_INVALIDARG;
	}
	
	RF_INT32 iRet = -1, iRssi = -1, iCnt = -1;
	RF_UCHAR ucRecvBuf[255] = "\0", ucNetWorkInfo[64] = "\0";
	RF_CHAR *pResponse = NULL;


	memset(gsmInfo, 0, sizeof(RF_GSM_INFO));
	iRet = OsWlPortOpen("115200,8,n,1");
	if (0 != iRet)
	{
		//TODO
		return RF_ERR_DEVICEFAULT;
	}
	//check rssi
	OsWlPortSend("AT+CSQ\r", strlen("AT+CSQ\r"));
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		return RF_ERR_INVALIDARG;
	}
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CSQ:");
	if (pResponse)
	{
		sscanf(pResponse, "%*s%d, %*d", &iRssi);
		if (iRssi>=10 && iRssi<15)
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 20;
		}
		else if (iRssi>=15 && iRssi<19)
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 40;
		}
		else if (iRssi>=19 && iRssi<23)
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 60;
		}
		else if (iRssi>=23 && iRssi<27)
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 80;
		}
		else if (iRssi>27 && iRssi<31)
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 100;
		}
		else
		{
			gsmInfo->rssi = iRssi;
			gsmInfo->radio_level = 0;
		}

	}

	//check network name or network code
	OsWlPortSend("AT+COPS?\r", strlen("AT+COPS?\r"));
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		//TODO
		return RF_ERR_INVALIDARG;
	}
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+COPS:");
	if (pResponse)
	{
		//TODO
		strncpy((RF_CHAR *)ucNetWorkInfo, strchr(pResponse, '\"') + 1, (strrchr(pResponse, '\"') - strchr(pResponse, '\"') - 1) );
		if (5==strlen((RF_CHAR *)ucNetWorkInfo) && isdigit(ucNetWorkInfo[0]))
		{
			for (iCnt=0; 0!=strcmp((RF_CHAR *)ucNetWorkInfo, stNetWorkInfo[iCnt].code) && (iCnt < ((sizeof(stNetWorkInfo) / sizeof(NET_WORK_INFO)) - 1 )); iCnt++)
			{
			}
			strcpy(gsmInfo->network_name, stNetWorkInfo[iCnt].operator);
		}
		else
		{
			strcpy(gsmInfo->network_name, (RF_CHAR *)ucNetWorkInfo);
		}
	}
	OsLog(LOG_ERROR, "%s--%d, network_name:%s", __FILE__, __LINE__, gsmInfo->network_name);

	// check has simcard or not
	OsWlPortSend("AT+CRSM=?\r", strlen("AT+CRSM=?\r"));
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		//TODO
		return RF_ERR_INVALIDARG;
	}
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CME ERROR: 10");
	if (pResponse)
	{
		gsmInfo->has_sim_card = HF_FALSE;
	}
	else
	{
		gsmInfo->has_sim_card = HF_TRUE;
	}
	OsLog(LOG_ERROR, "%s--%d, has_sim_card:%d", __FILE__, __LINE__, gsmInfo->has_sim_card);

	OsWlPortSend("AT+CCID?\r", strlen("AT+CCID?\r"));
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0 )
	{
		//TODO
		return RF_ERR_INVALIDARG;
	}
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CCID:");
	if (pResponse)
	{
		sscanf(pResponse, "%*s%s", gsmInfo->sim_icc_id);
	}
	OsLog(LOG_ERROR, "%s--%d, sim_icc_id:%s", __FILE__, __LINE__, gsmInfo->sim_icc_id);

	OsWlPortClose();

	iRet = OsWlCheck();
	if (RF_SUCCESS != iRet)
	{
		gsmInfo->gsm_data_network = RF_GSM_NONE;
	}
	else
	{
		gsmInfo->gsm_data_network = RF_GSM_GPRS;
	}

    return RF_SUCCESS;
}

RF_INT32 RF_gsm_status(RF_GSM_STATUS_INFO* info)    
{
	if (NULL==info)
	{
		return RF_ERR_INVALIDARG;
	}
	
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_gsm_status");

	RF_INT32 iRet = -1, state = -1, iRssi = -1;
	RF_UCHAR ucRecvBuf[255] = "\0";
	RF_CHAR *pResponse = NULL;
	struct sockaddr_in stSockAddr;
	RF_INT32 socketFD  = -1;
	ST_TIMER st_timer = {0,0,0};

	memset(info, -1, sizeof(RF_GSM_STATUS_INFO));

	//rede call this function before RF_gprs_config, so initialize some paras first.
	if (NULL==s_stGsmGprsConfig)
	{
		s_stGsmGprsConfig = malloc(sizeof(RF_GSM_GPRS_CONFIG));

		s_stGsmGprsConfig->modem_startup.wait_modem_mseg = 500;
		s_stGsmGprsConfig->modem_startup.start_modem_try_num = 5;
		//ber, we don't use this
		s_stGsmGprsConfig->signal_wait.ber_maximo = 3;
		s_stGsmGprsConfig->signal_wait.rssi_minimo = 10;
		s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg = 30000;
	}

	//detect step A
	info->etapa = info->status_nativo = RF_GSM_ETAPA_A;
	iRet = OsWlPortOpen("115200,8,n,1");
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. OsWlPortOpen()");
	if (0 != iRet)
	{
		return RF_ERR_DEVICEFAULT;
	}
	//check the rssi if met
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. ANTES OsTimerSet()");
//	OsTimerSet(&st_timer, (ushort)(s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3.1 st_timer[0x%04x]", &st_timer);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3.2 s_stGsmGprsConfig[0x%04x]", s_stGsmGprsConfig);

	if ( NULL == s_stGsmGprsConfig )
	{
		return RF_ERR_DEVICEFAULT;
	}

	OsTimerSet(&st_timer, s_stGsmGprsConfig->signal_wait.timeout_signal_wait_mseg);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. OsTimerSet()");
	while (0 != OsTimerCheck(&st_timer))
	{
		OsWlPortSend("AT+CSQ\r", strlen("AT+CSQ\r"));
		iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"4. OsWlPortRecv()");
		OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
		if (iRet <= 0)
		{
			return RF_ERR_DEVICEFAULT;
		}
		pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CSQ:");
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"5. strstr()");
		if (pResponse)
		{
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"6. ANTES sscanf()");
			sscanf(pResponse, "%*s%d, %*d", &iRssi);
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"7. APOS sscanf()");
		}
		else
		{
			return RF_ERR_DEVICEFAULT;
		}
		if (iRssi >= s_stGsmGprsConfig->signal_wait.rssi_minimo)
		{
			break;
		}
		OsSleep(500);
	}
	if (iRssi < s_stGsmGprsConfig->signal_wait.rssi_minimo)
	{
		//TODO
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_A5_ERROR_NO_SIGNAL;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"8. RF_SUCCESS");
		return RF_SUCCESS;
	}

	OsWlPortSend("AT+CGREG?\r", strlen("AT+CGREG?\r"));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"9. OsWlPortSend()");
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"10. OsWlPortRecv()");
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	if (iRet <= 0)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"11. RF_ERR_DEVICEFAULT");
		return RF_ERR_DEVICEFAULT;
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"12. ANTES strstr()");
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CME ERROR:");
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"13. APOS strstr()");
	if (pResponse)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"14. ANTES sscanf()");
		sscanf((RF_CHAR *)pResponse, "%*s%*s%d", info->cme_erro);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"15. APOS sscanf()");
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_A4_ERROR_ATTACHMENT_FAILURE;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"16. RF_SUCCESS");
		return RF_SUCCESS;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"17. ANTES strstr");
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CGREG:");
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"18. APOS strstr");
	if (pResponse)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"19. ANTES sscanf");
		sscanf(pResponse, "%*s%*d,%d", &state);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"19. APOS sscanf");
		switch(state)
		{
		case 1:
		case 5:
			info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM;
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"20. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM");
			break;
		case 2:
			info->estadoEtapa = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"21. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_EXECUTANDO");
			break;
		default:
			info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"22. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO");
			break;
		}
	}
	else
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"23. RF_ERR_DEVICEFAULT");
		return RF_ERR_DEVICEFAULT;
	}
	if (RF_GSM_ETAPA_ESTADO_ERRO == info->estadoEtapa)
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_A4_ERROR_ATTACHMENT_FAILURE;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"24. RF_SUCCESS");
		return RF_SUCCESS;
	}

	//detect step G1
	info->etapa = info->status_nativo = RF_GSM_ETAPA_G1;
	OsWlPortSend("AT+CGACT?\r", strlen("AT+CGACT?\r"));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"25. OsWlPortSend");
	iRet = OsWlPortRecv(ucRecvBuf, sizeof(ucRecvBuf), 500);
	OsLog(LOG_ERROR, "%s--%d, iRet:%d, ucRecvBuf:%s", __FILE__, __LINE__, iRet, ucRecvBuf);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"26. ANTES strstr");
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CME ERROR:");
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"27. APOS strstr");
	if (pResponse)
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_G12_ERROR_GGSN_CONTEXT_FAILURE;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"28. ANTES sscanf");
		sscanf(pResponse, "%*s%*s%d", info->cme_erro);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"29. APOS sscanf");
		return RF_SUCCESS;
	}
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"30. ANTES strstr");
	pResponse = strstr((RF_CHAR *)ucRecvBuf, "+CGACT:");
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"31. APOS strstr");
	if (pResponse)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"32. ANTES sscanf");
		sscanf(pResponse, "%*s%d,%*d", &state);
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"33. ANTES sscanf");
		if (0==state)
		{
			info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
			info->codigo_erro = RF_GSM_STAGE_G12_ERROR_GGSN_CONTEXT_FAILURE;
			PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"34. RF_SUCCESS");
			return RF_SUCCESS;
		}
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"35. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM");
	}
	else
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"36. RF_ERR_DEVICEFAULT");
		return RF_ERR_DEVICEFAULT;
	}

	OsWlPortClose();
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"37. OsWlPortClose()");

	//detect step G2
	info->etapa = info->status_nativo = RF_GSM_ETAPA_G2;
	iRet = OsWlCheck();
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"38. OsWlCheck()");
	if (PPP_LOGINING == iRet)
	{
		//FIXME
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_EXECUTANDO;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"39. RF_SUCCESS");
		return RF_SUCCESS;
	}
	else if (0 == iRet)
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"40. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM");
	}
	else
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_G13_ERROR_GGSN_IP_FAILURE;
		info->codigo_erro_nativo = iRet;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"41. RF_SUCCESS");
		return RF_SUCCESS;
	}

	//detect step G3
	info->etapa = info->status_nativo = RF_GSM_ETAPA_G3;
	iRet = OsNetDns("www.baidu.com", (char *)ucRecvBuf, 300000 );
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"42. OsNetDns()");
	OsLog(LOG_ERROR, "%s--%d, iRet :%d", __FILE__, __LINE__, iRet);
	if (0 != iRet)
	{
		info->codigo_erro_nativo = iRet;
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_G14_ERROR_PPP_FAILURE;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"43. RF_SUCCESS");
		return RF_SUCCESS;
	}
	info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM;
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"44. info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM");

	//detect step G4
	info->etapa = info->status_nativo = RF_GSM_ETAPA_G4;
	socketFD = socket(AF_INET, SOCK_STREAM, 0);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"45. socketFD()");
	OsLog(LOG_ERROR, "%s--%d, socketFD:%d", __FILE__, __LINE__, socketFD);
	if (socketFD < 0)
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_G14_ERROR_PPP_FAILURE;
		info->codigo_erro_nativo = iRet;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"46. RF_SUCCESS");
		return RF_SUCCESS;
	}
	memset(&stSockAddr, 0, sizeof(stSockAddr));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"47. memset()");

	stSockAddr.sin_family = AF_INET;
	stSockAddr.sin_port = htons(80);
	stSockAddr.sin_addr.s_addr = inet_addr("119.75.217.109");
	iRet = connect(socketFD, (struct sockaddr *)&stSockAddr, sizeof(stSockAddr));
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"48. connect()");
	OsLog(LOG_ERROR, "%s--%d, iRet :%d", __FILE__, __LINE__, iRet);
	if ( iRet < 0)
	{
		info->estadoEtapa = RF_GSM_ETAPA_ESTADO_ERRO;
		info->codigo_erro = RF_GSM_STAGE_G14_ERROR_PPP_FAILURE;
		info->codigo_erro_nativo = iRet;
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"49. RF_SUCCESS");
		return RF_SUCCESS;
	}
	close(socketFD);
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"50. close()");
	info->estadoEtapa = RF_GSM_ETAPA_ESTADO_FIM;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"51. RF_SUCCESS");
    return RF_SUCCESS;
}

static RF_INT32 SetOpt(RF_INT32 fd, RF_INT32 nSpeed, RF_INT32 nBits, RF_CHAR nEvent, RF_INT32 nStop)
{
	struct termios newtio, oldtio;

	if (tcgetattr(fd, &oldtio) != 0)
	{
		printf("SetupSerial 1");
		return -1;
	}
	memset(&newtio, 0, sizeof(newtio));
	newtio.c_cflag |= CLOCAL | CREAD;
	newtio.c_cflag &= ~CSIZE;
	newtio.c_cflag &= ~CRTSCTS;
	newtio.c_lflag &= ~ICANON;

	switch (nBits)
	{
	case 7:
		newtio.c_cflag |= CS7;
		break;
	case 8:
	default:
		newtio.c_cflag |= CS8;
		break;
	}

	switch (nEvent)
	{
	case 'O':
		newtio.c_cflag |= PARENB;
		newtio.c_cflag |= PARODD;
		newtio.c_iflag |= (INPCK | ISTRIP);
		break;
	case 'E':
		newtio.c_iflag |= (INPCK | ISTRIP);
		newtio.c_cflag |= PARENB;
		newtio.c_cflag &= ~PARODD;
		break;
	case 'N':
	default:
		newtio.c_cflag &= ~PARENB;
		break;
	}

	switch (nSpeed)
	{
	case 1200:
		cfsetispeed(&newtio, B1200);
		cfsetospeed(&newtio, B1200);
		break;
	case 2400:
		cfsetispeed(&newtio, B2400);
		cfsetospeed(&newtio, B2400);
		break;
	case 4800:
		cfsetispeed(&newtio, B4800);
		cfsetospeed(&newtio, B4800);
		break;
	case 9600:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	case 19200:
		cfsetispeed(&newtio, B19200);
		cfsetospeed(&newtio, B19200);
		break;
	case 38400:
		cfsetispeed(&newtio, B38400);
		cfsetospeed(&newtio, B38400);
		break;
	case 57600:
		cfsetispeed(&newtio, B57600);
		cfsetospeed(&newtio, B57600);
		break;
	case 115200:
		cfsetispeed(&newtio, B115200);
		cfsetospeed(&newtio, B115200);
		break;
	default:
		cfsetispeed(&newtio, B9600);
		cfsetospeed(&newtio, B9600);
		break;
	}

	if (nStop == 1)
	{
		newtio.c_cflag &= ~CSTOPB;
	}
	else if (nStop == 2)
	{
		newtio.c_cflag |= CSTOPB;
	}

	newtio.c_cc[VTIME] = 0;
	newtio.c_cc[VMIN] = 0;
	tcflush(fd, TCIFLUSH);
	if ((tcsetattr(fd, TCSANOW, &newtio)) != 0)
	{
		printf("com set error");
		return -1;
	}
	printf("set uart as \"%d %d %c %d\" done!\n", nSpeed, nBits,
	    nEvent, nStop);
	return 0;
}

static RF_INT32 parse_attr(const RF_CHAR *attr, RF_INT32 *speed, RF_INT32 *databits,
    RF_INT32 *parity, RF_INT32 *stopbits)
{
	RF_INT32 iCnt = -1;
	RF_CHAR buf[50] = "\0";
	const long baud_tab[] = { 600, 1200, 2400, 4800, 9600, 14400, 19200,
	    28800, 38400, 57600, 115200, 230400, };
	RF_CHAR key[] = " ,.\r\n";
	RF_CHAR *str = NULL;

	if (strlen(attr) > 49)
	{
		return -1;
	}
	strcpy(buf, attr);
	/* attr = buf; */
	str = strtok(buf, key);
	if (str == NULL)
	{
		return -1;
	}
	*speed = atoi(str);
	for (iCnt = 0; iCnt < (RF_INT32)(sizeof(baud_tab) / sizeof(long)); iCnt++)
	{
		if (baud_tab[iCnt] == *speed)
		{
			break;
		}
	}
	if (iCnt == sizeof(baud_tab) / sizeof(long))
	{
		return -1;
	}

	str = strtok(NULL, key);
	if (str == NULL)
	{
		return -1;
	}
	*databits = atoi(str);
	if (*databits < 5 || *databits > 8)
	{
		return -1;
	}

	str = strtok(NULL, key);
	if (str == NULL || strlen(str) > 1)
	{
		return -1;
	}
	if (str[0] == 'e' || str[0] == 'n' || str[0] == 'o')
	{
		str[0] = toupper(str[0]);
	}
	if (str[0] != 'E' && str[0] != 'N' && str[0] != 'O')
	{
		return -1;
	}
	*parity = str[0];

	str = strtok(NULL, key);
	if (str == NULL || strlen(str) > 1)
	{
		return -1;
	}
	if (str[0] != '1' && str[0] != '2')
	{
		return -1;
	}
	*stopbits = atoi(str);
	return 0;
}

static RF_INT32 time_compare(struct timeval tv1, struct timeval tv2)
{
	if (tv1.tv_sec > tv2.tv_sec)
	{
		return 1;
	}
	else if (tv1.tv_sec < tv2.tv_sec)
	{
		return -1;
	}
	else if (tv1.tv_usec > tv2.tv_usec)
	{
		return 1;
	}
	else if (tv1.tv_usec < tv2.tv_usec)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static RF_VOID time_add_ms(struct timeval *tv, RF_INT32 ms)
{
	tv->tv_sec += ms / 1000;
	tv->tv_usec += ((ms % 1000) * 1000);
}

RF_INT32 OsWlPortOpen(const RF_CHAR *Attr)
{
	RF_INT32 speed = -1, databits = -1, stopbits = -1, parity = -1;

	if (Attr == NULL)
	{
		return -1;
	}

	if (parse_attr(Attr, &speed, &databits, &parity, &stopbits) < 0)
	{
		return -1;
	}


    if(fp_uart > 0)
    {
        close(fp_uart);
    }

    if (0 == bInitGprs)
    {
    	fp_uart = open("/dev/ttywl", O_RDWR | O_NOCTTY);
    	OsLog(LOG_ERROR, "%s--%d, ttyAMA1,fp_uart:%d", __FILE__, __LINE__, fp_uart);
    }
    else
    {
    	fp_uart = open("/dev/mux1", O_RDWR | O_NOCTTY);
    	OsLog(LOG_ERROR, "%s--%d, mux1,fp_uart:%d", __FILE__, __LINE__, fp_uart);
    }

	if(fp_uart < 0)
	{
		//TODO
		return -1;
	}

	if (SetOpt(fp_uart, speed, databits, parity, stopbits) < 0)
	{
		//TODO
	    close(fp_uart);
		return -1;
	}

	tcflush(fp_uart, TCIOFLUSH);

	return 0;
}

RF_VOID OsWlPortClose()
{
	if (fp_uart < 0)
	{
		return;
	}
	close(fp_uart);
	fp_uart = -1;
}

RF_VOID OsWlPortReset()
{
	tcflush(fp_uart, TCIOFLUSH);
}

RF_INT32 OsWlPortSend(const RF_VOID *SendBuf, RF_INT32 SendLen)
{
	RF_INT32 ret = 0;
	const RF_CHAR *buf = SendBuf;

	/*When you don't set open(..O_NDELAY) or fcntl(fd, F_SETFL, FNDELAY),
	The while() and usleep() isn't needed.*/
	while (SendLen > 0)
	{
		ret = write(fp_uart, buf, SendLen);
		if (ret < 0)
		{
			break;
		}
		buf += ret;
		SendLen -= ret;
		usleep(10 * 1000);
	}
	return (ret >= 0) ? 0 : -1;
}

static RF_INT32 port_err(RF_INT32 err)
{
	if (err == EFAULT)
	{
		return -1013;
	}
	return -1014;
}

RF_INT32 OsWlPortRecv(RF_VOID *RecvBuf, RF_INT32 RecvLen, RF_INT32 TimeoutMs)
{
	RF_CHAR *buf = RecvBuf;
	RF_INT32 ret = -1, err = -1, total = -1;
	struct timeval tv1, tv2;

	/* Flush the input and output pools. */
	if (buf == NULL && RecvLen == 0)
	{
		//TODO
		OsWlPortReset();
		return 0;
	}

	if (buf == NULL || RecvLen < 0 || TimeoutMs < 0)
	{
		//TODO
		return -1;
	}

	if (!RecvLen)
	{
		//TODO
		return 0;
	}

	if (TimeoutMs < 100 && TimeoutMs > 0)
	{
		TimeoutMs = 100;
	}

	if (gettimeofday(&tv1, NULL) < 0)
	{
		return -3209;
	}
	time_add_ms(&tv1, TimeoutMs);

	total = 0;
	while (RecvLen > 0)
	{
		ret = read(fp_uart, buf, RecvLen);
		if (ret < 0)
		{
			err = errno;
			if (err != EAGAIN && err != EINTR)
			{
				return port_err(err);
			}
		}
		else
		{
			buf += ret;
			RecvLen -= ret;
			total += ret;
			if (RecvLen <= 0)
			{
				break;
			}
			if (gettimeofday(&tv2, NULL) < 0)
			{
				return -3209;
			}
			if (time_compare(tv1, tv2) <= 0)
			{
				break;
			}
		}
	}
	return total;
}

