/*------------------------------------------------------------
* FileName: HF_PAX_LOG.c
* Author: Jarod
* Date: 2015-07-14
------------------------------------------------------------*/
//#include "debugger.h"
#include "HF_API.h"
#include "stdarg.h"

#define MAX_CHARS_RS232 (8 * 1024)

//log function and line
void PaxLog(char Debug_level, char *szFuncName, int line, char *szLogInfo, ...)
{
	int arg_len=0;


	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	//strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	strcpy(szLogInfo_Header,"                  "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	//arg_len = strlen(szTime);
	//memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s--%05d: %s", szLogInfo_Header, line, szLogInfo_Buff);

	va_end(args);
}


/*
void PaxLog(char Debug_level, char *szFuncName, char *szTime, char *szLogInfo, ...)
{
	int arg_len=0;


	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL || szTime == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	arg_len = strlen(szFuncName);
	strcpy(szLogInfo_Header,"                  (--------): "); // 31 >
	memcpy(&szLogInfo_Header[0],szFuncName,arg_len);

	arg_len = strlen(szTime);
	memcpy(&szLogInfo_Header[19],szTime,arg_len);

	OsLog(Debug_level,"%s%s",szLogInfo_Header,szLogInfo_Buff);

	va_end(args);
}
*/

/*
void PaxLog(char Debug_level, char *szFuncName, char *szTime, char *szLogInfo, ...)
{
	int arg_len=0;

	char szLogInfo_Header[100];
	char szLogInfo_Buff[MAX_CHARS_RS232];

	va_list args;
	va_start(args, szLogInfo);
	memset(szLogInfo_Buff, 0, sizeof(szLogInfo_Buff));
	memset(szLogInfo_Header, 0, sizeof(szLogInfo_Header));

	if (szFuncName == NULL || szTime == NULL)
	{
		return;
	}

	arg_len = vsprintf(szLogInfo_Buff, szLogInfo, args);

	sprintf(szLogInfo_Header,"%=s(%s): ",szFuncName,szTime);

	OsLog(Debug_level,"%s%s",szLogInfo_Header,szLogInfo_Buff);

	va_end(args);
}
*/

/*
void PaxLog_Debug(const char *szRegistro, ...)
{
#ifdef PAX_LOG_DEBUG
	char szRegistroFormatado[MAX_CHARS_RS232];
	va_list args;
	va_start(args, szRegistro);
	memset(szRegistroFormatado, 0, sizeof(szRegistroFormatado));
	vsprintf(szRegistroFormatado, szRegistro, args);
	strcat(szRegistroFormatado, "\r\n\n");
	if ( !OsPortOpen(PORT_COM1, "115200,8,n,1") )
	{
		OsPortReset(PORT_COM1);
		OsPortSend(PORT_COM1, szRegistroFormatado, strlen(szRegistroFormatado));
		OsPortClose(PORT_COM1);
	}
	va_end(args);
#endif
}

void PaxSetLogTag(const char *szLogTagInfo, ...)
{
	int arg_len=0;
	char szLogTagInfo_Func[MAX_CHARS_RS232];
	char fill_spaces[40];

	va_list args;
	va_start(args, szLogTagInfo);
	memset(szLogTagInfo_Func, 0, sizeof(szLogTagInfo_Func));
	arg_len = vsprintf(szLogTagInfo_Func, szLogTagInfo, args);
	memset(fill_spaces,0,sizeof(fill_spaces));

	memset(fill_spaces, ' ', (24-arg_len));

	strcat(szLogTagInfo_Func,fill_spaces);

	OsLogSetTag(szLogTagInfo_Func);

	va_end(args);
}
*/
