#include "HF_API.h"

/**
 * Fun��o que ser� chamada pela aplica��o para definir os par�metros para conex�o na rede WI-FI.
 * @param RF_WIFI_CONFIG_INFO *config - Ponteiro para a estrutura RF_WIFI_CONFIG_INFO, na qual s�o informados:
 *        connect_max_retry - N�mero m�ximo de tentativas de estabelecer a conex�o WI-FI.
 *                            Se zero, n�o h� limite. Isto �, fica em loop at� ser desativado.
 *        reconnect_wait_mseg - Tempo que o dispositivo deve aguardar antes de
 *                              tentar re-estabelecer a conex�o WI-FI.
 * @return RF_SUCCESS � se a fun��o executou com sucesso.
 *         RF_ERR_INVALIDARG � Ponteiro nulo
 *         RF_DEVICE_FAULT � Erro na execu��o
 */
RF_INT32 RF_WIFI_config(RF_WIFI_CONFIG_INFO *config)
{
    return HF_SUCCESS;
}

/**
 * Ativa a interface WI-FI do dispositivo.
 * @return RF_SUCCESS      � Interface ativada.
 *         RF_DEVICE_FAULT � Erro ativando interface
 */
RF_INT32 RF_WIFI_up(RF_VOID)
{
    return HF_SUCCESS;
}

/**
 * Destiva a interface WI-FI do dispositivo.
 * @return RF_SUCCESS � Interface desativada.
 *         RF_DEVICE_FAULT � Erro desativando interface.
 */
RF_INT32 RF_WIFI_down(RF_VOID)
{
    return HF_SUCCESS;
}


/**
 * Obtem a lista de redes WI-FI dispon�veis.
 *
 * A fun��o deve retornar quando expirar o timeout ou atingir o m�ximo de resultados poss�veis.
 *
 * @param p_results - Ponteiro para a estrutura que armazena a lista de redes WI-FI dispon�veis.
 * @param maxResults - Quantidade m�xima de redes WI-FI dispon�veis a ser retornada.
 * @param timeout_ms  - Timeout em milissegundos.
 * @return RF_SUCCESS � Lista de redes WI-FI obtida com sucesso. A lista pode estar vazia,
 * isto � n�o foram encontradas redes WI-FI.
 *         RF_DEVICE_FAULT � Erro.
 *
 *
 */
RF_INT32 RF_WIFI_scan(RF_WIFI_SCAN_RESULTS* p_results, RF_UINT16 maxResults, RF_UINT32 timeout_ms)
{
    return HF_SUCCESS;
}

/**
 * Inicia o processo de conex�o em uma rede WI-FI.
 *
 * A fun��o dever� retornar imediatamente, isto � n�o dever� aguardar a completa
 * conex�o na rede WI-FI.
 *
 * A conex�o na rede WI-FI dever� ser realizada em background por uma thread/task.
 * Esta thread tamb�m ser� respons�vel por detectar quedas e restabelecer automaticamente a conex�o.
 *
 * A Thread dever� respeitar os par�metros definidos na RF_WIFI_config, zerando o contador de tentativas
 * de reconex�o cada vez que a conex�o for estabelecida.
 *
 * Durante o estabelecimento da conex�o com a rede WI-FI o status obtido atrav�s da
 * fun��o RF_WIFI_status deve ser atualizado constantemente.
 *
 * @param network - dados da conex�o Wi-fi que o dispositivo dever� conectar.
 *                  A conexao s� poder� ser realizada caso os seguintes parametros
 *                  sejam identicos a uma rede acess�vel: ssid, tam_ssid, algoritmoAuth, algoritmoCripto
 * @param p_ascii_password - String terminada em NULL('\0') contendo a senha de conex�o da rede WI-FI.
 *                           Caso a rede em quest�o n�o precise de senha o par�metro ser� ignorado.
 *                           A implementa��o dever� copiar o valor da string para suas estruturas
 *                           internas. Ap�s o retorno da fun��o n�o h� garantia que este ponteiro continuar� integro.
 *
 * @return RF_SUCCESS � Iniciado processo de conex�o na rede WI-FI.
 *         RF_ERR_INVALIDSTATE � Fun��o RF_WIFI_config n�o foi chamada anteriormente.
 *         RF_DEVICE_FAULT � Iniciando processo de conex�o na rede WI-FI.
 */
RF_INT32 RF_WIFI_start_connect(RF_WIFI_NETWORK_INFO network, RF_IP_NETWORK_INFO ipInfo, const RF_CHAR* p_ascii_password)
{
    return HF_SUCCESS;
}

/**
 * Obtem o status da conex�o WI-FI do dispositivo.
 *
 * @param p_item Ponteiro para a estrutura que armazena o status.
 *
 * @return RF_SUCCESS � Status obtido com sucesso.
 *         RF_DEVICE_FAULT � Erro obtendo status
 */
RF_INT32 RF_WIFI_status(RF_WIFI_STATUS_INFO *p_item)
{
    return HF_SUCCESS;
}

/**
 * Indica para o dispositivo que a conex�o WI-FI deve ser desconectada.
 * A fun��o dever� retornar imediatamente, isto � n�o dever� aguardar a
 * completa desconex�o da rede WI-FI (a desconex�o da rede WI-FI dever� ser
 * realizada em background).
 * Durante a desconex�o o status obtido atrav�s da fun��o RF_WIFI_status
 * deve ser atualizado constantemente.
 *
 * @return RF_SUCCESS � Solicita��o processada com sucesso
 *         RF_DEVICE_FAULT � Erro processando solicita��o
 */
RF_INT32 RF_WIFI_request_disconnect(RF_VOID)
{
    return HF_SUCCESS;
}
