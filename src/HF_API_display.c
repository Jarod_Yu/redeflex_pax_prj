#include "HF_API.h"

HF_INT32 HF_display_print(HF_UINT32 row, HF_UINT32 column, const HF_CHAR *text)
{
    // ---------------------------------
    OsLogSetTag("HF_display_print");
    OsLog(LOG_DEBUG, "HF_display_print[%s]", text);
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_display_printAligned(HF_UINT32 row, HF_INT32 alignment, const HF_CHAR *text)
{
    // ---------------------------------
    printf("%s\n", text);
    // ---------------------------------
    return HF_SUCCESS;
}

HF_INT32 HF_display_printimage(const HF_VOID *image, HF_UINT32 x, HF_UINT32 y)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_clearline(HF_UINT32 line)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_clear(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_clear_screen(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_geometry(HF_UINT32 *lines, HF_UINT32 *columns)
{
	*lines = 0;
	*columns = 0;
    return HF_SUCCESS;
}

HF_INT32 HF_display_pixels(HF_UINT32 *width, HF_UINT32 *height)
{
    *width = 240;
    *height = 320;
    return HF_SUCCESS;
}

HF_INT32 HF_display_setBacklight(HF_UINT8 percent)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawLine(HF_UINT32 x0, HF_UINT32 y0, HF_UINT32 x1, HF_UINT32 y1)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawPoint(HF_UINT32 x, HF_UINT32 y)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_drawRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_getCursorPosition(HF_UINT32 *column, HF_UINT32 *line)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_setFont(HF_FONT_T pFont)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_getFont(HF_FONT_T *pFont)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_getFontDimension(HF_FONT_T pFont, HF_UINT32 *width, HF_UINT32 *height)
{
    *width = 1;
    *height = 1;
    return HF_SUCCESS;
}

HF_VOID  HF_display_setAutoFlush(HF_BOOL value)
{
}

HF_BOOL  HF_display_getAutoFlush(HF_VOID)
{
    return HF_SUCCESS;
}

HF_VOID  HF_display_setInvertColors(HF_BOOL value)
{
}

HF_BOOL  HF_display_getInvertColors(HF_VOID)
{
    return HF_SUCCESS;
}

HF_VOID  HF_display_flush(HF_VOID)
{
}

HF_INT32 HF_display_setColor(HF_INT64 text, HF_INT64 background)
{
    return HF_SUCCESS;
}

HF_INT64 HF_display_getColor(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT64 HF_display_getBackgroundColor(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_fillRectangle(HF_UINT32 x, HF_UINT32 y, HF_UINT32 width, HF_UINT32 height)
{
    return HF_SUCCESS;
}

HF_BOOL  HF_display_setHeader(HF_BOOL value)
{
	return HF_SUCCESS;
}

HF_BOOL  HF_display_setFooter(HF_BOOL value)
{
	return HF_SUCCESS;
}

HF_INT32 HF_display_setCustomHeader(HF_BOOL onOff, const HF_VOID *image)
{
    return HF_SUCCESS;
}

HF_INT32 HF_display_setCustomFooter(HF_BOOL onOff, const HF_VOID *image)
{
    return HF_SUCCESS;
}
