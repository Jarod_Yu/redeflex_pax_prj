#include "HF_API.h"
#include <sys/time.h>
#include <sys/time.h>
#include "osal.h"

HF_INT32 HF_sleep(HF_UINT32 delay)
{

	OsSleep(delay);

    return HF_SUCCESS;
}

HF_INT32 HF_timer_start(HF_TIMER_T *timer, HF_INT64 count)
{
	HF_INT32 iRet = 0;
	struct timeval starttime = {0, 0};

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_timer_start");

	//check input parameter legal or not
	if(NULL == timer)
	{
		return HF_ERR_INVALIDARG;
	}

	timer->count = count;

	iRet = gettimeofday(&starttime,0);
	if(0 != iRet)
	{
		return HF_ERR_DEVICEFAULT;
	}
	timer->startTime = (((long long)starttime.tv_sec)*1000000 + starttime.tv_usec)/1000;

	return HF_SUCCESS;
}

HF_INT64 HF_timer_elapsed(HF_TIMER_T *timer)
{
	HF_INT32 iRet = 0;
	struct timeval endtime = {0, 0};
    //check input parameter legal or not
	if(NULL == timer)
	{
		return HF_ERR_INVALIDARG;
	}

	iRet = gettimeofday(&endtime,0);
	if(0 != iRet)
	{
		return HF_ERR_DEVICEFAULT;
	}

	return (((long long)endtime.tv_sec)*1000000 + endtime.tv_usec)/1000 - timer->startTime;
}

HF_INT64 HF_timer_remaining(HF_TIMER_T *timer)
{
	HF_INT32 iRet = 0;
	HF_INT64 curTime = 0;
	HF_INT64 targetTime = 0;
	struct timeval endtime = {0, 0};
	//check input parameter legal or not
	if(NULL == timer)
	{
		//TODO
		return HF_ERR_INVALIDARG;
	}

	if (HF_TIMER_INFINITE == timer->count)
	{
		return 1000;
	}

	iRet = gettimeofday(&endtime,0);
	if(0 != iRet)
	{
		return HF_ERR_DEVICEFAULT;
	}

	targetTime = timer->startTime + timer->count;
	curTime = (((long long)endtime.tv_sec)*1000000 + endtime.tv_usec)/1000;

	if(targetTime >= curTime)
	{
		return (targetTime - curTime);
	}
	else
	{
		//TODO
		//is this right?
		return HF_ERR_TIMEREXPIRED;
	}
}
