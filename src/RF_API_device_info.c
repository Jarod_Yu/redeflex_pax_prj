#include "HF_API.h"

RF_INT32 RF_screen_get_current_settings(struct RF_ScreenInfo* p_info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_screen_get_current_settings");

	if (p_info == RF_NULL)
	{
		return RF_ERR_INVALIDARG;
	}

	struct RF_ScreenInfo *pp_info;
	RF_UINT16 p_infosz;
	RF_screen_get_capabilities(&pp_info, &p_infosz);
	memcpy(p_info, pp_info, sizeof(struct RF_ScreenInfo));
	free(pp_info);
	return RF_SUCCESS;
}

RF_INT32 RF_screen_set_current_settings(struct RF_ScreenInfo* p_info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_screen_set_current_settings");
	return RF_SUCCESS;
}

RF_INT32 RF_printer_get_current_settings(struct RF_PrinterInfo* p_info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_printer_get_current_settings");

	if (p_info == RF_NULL)
	{
		return RF_ERR_INVALIDARG;
	}

	struct RF_PrinterInfo *pp_info;
	RF_UINT16 p_infosz;
	RF_printer_get_capabilities(&pp_info, &p_infosz);
	memcpy(p_info, pp_info, sizeof(struct RF_ScreenInfo));
	free(pp_info);
	return RF_SUCCESS;
}

RF_INT32 RF_printer_set_current_settings(struct RF_PrinterInfo* p_info)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_printer_set_current_settings");
	return RF_SUCCESS;
}

RF_INT32 RF_screen_get_capabilities(struct RF_ScreenInfo** pp_info, RF_UINT16* p_infosz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_screen_get_capabilities");

	if (pp_info == RF_NULL || p_infosz == RF_NULL)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"1. RF_screen_get_capabilities RF_NULL");
		return RF_ERR_INVALIDARG;
	}

	*p_infosz = 1;
	*pp_info = (struct RF_ScreenInfo*)calloc(*p_infosz, sizeof(struct RF_ScreenInfo));
	if (*pp_info == RF_NULL)
	{
		PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"2. RF_screen_get_capabilities return RF_ERR_NOMEMORY");
		return (RF_ERR_NOMEMORY);
	}

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"3. RF_screen_get_capabilities");
	// Jarod@2015/07/31 UPD START
	(*pp_info)->mode = GRAPH_MODE_COLOR;
	//(*pp_info)->mode = GRAPH_ORIENTATION_LANDSCAPE;
	// Jarod@2015/07/31 UPD END
	(*pp_info)->pixels_x = 320;
	(*pp_info)->pixels_y = 240;
	(*pp_info)->bmp_format_mask = GRAPH_BITMAP_FMT_JPEG | GRAPH_BITMAP_FMT_GIF | GRAPH_BITMAP_FMT_PNG | GRAPH_BITMAP_FMT_BMP_MONO | GRAPH_BITMAP_FMT_BMP_COLOR;
	(*pp_info)->orientation = GRAPH_ORIENTATION_LANDSCAPE;
	(*pp_info)->structSize = sizeof(struct RF_ScreenInfo);
	
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"SAIU RF_screen_get_capabilities");
	return RF_SUCCESS;
}

RF_INT32 RF_printer_get_capabilities( struct RF_PrinterInfo** pp_info, RF_UINT16* p_infosz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_printer_get_capabilities");

	if (pp_info == RF_NULL || p_infosz == RF_NULL)
	{
		return RF_ERR_INVALIDARG;
	}

	*p_infosz = 1;
	*pp_info = (struct RF_PrinterInfo*)calloc(*p_infosz, sizeof(struct RF_PrinterInfo));
	if (*pp_info == RF_NULL)
	{
		return (RF_ERR_NOMEMORY);
	}

	(*pp_info)->mode = GRAPH_MODE_MONO;
	(*pp_info)->bmp_format_mask = GRAPH_BITMAP_FMT_BMP_MONO;
	(*pp_info)->orientation = GRAPH_ORIENTATION_PORTRAIT;
	(*pp_info)->structSize = sizeof(struct RF_PrinterInfo);

	return RF_SUCCESS;
}

RF_INT32 RF_input_get_capabilities (struct RF_InputInfo** pp_info, RF_UINT16* p_infosz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_input_get_capabilities");

	if (pp_info == RF_NULL || p_infosz == RF_NULL)
	{
		return RF_ERR_INVALIDARG;
	}

	*p_infosz = 1;
	*pp_info = (struct RF_InputInfo*)calloc(*p_infosz, sizeof(struct RF_InputInfo));
	if (*pp_info == RF_NULL)
	{
		return (RF_ERR_NOMEMORY);
	}

	(*pp_info)->method = INPUT_KEYBOARD;

	return RF_SUCCESS;
}

RF_INT32 RF_comm_get_capabilities (struct RF_CommInfo** pp_info, RF_UINT16* p_infosz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_comm_get_capabilities");

	if (pp_info == RF_NULL || p_infosz == RF_NULL)
	{
		return RF_ERR_INVALIDARG;
	}

	*p_infosz = 1;
	*pp_info = (struct RF_CommInfo*)calloc(*p_infosz, sizeof(struct RF_CommInfo));
	if (*pp_info == RF_NULL)
	{
		return (RF_ERR_NOMEMORY);
	}

	(*pp_info)->tech = COMM_WIRELESS_GPRS;

	return RF_SUCCESS;
}
