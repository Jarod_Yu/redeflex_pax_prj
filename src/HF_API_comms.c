#include "HF_API.h"

HF_INT32 HF_comm_configDefault(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_configLoad(const HF_CHAR *filename)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_setProperty(const HF_CHAR *key, const HF_CHAR *value)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getProperty(const HF_CHAR *pKey, HF_CHAR *pBuffer, HF_UINT32 pBuffSz) 
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_configSave(const HF_CHAR *filename)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getMaxDialNumbers(HF_UINT8 *maxPhonenumber, HF_UINT8 *maxPabx)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_connect(const HF_CHAR *pPhonenumber, const HF_CHAR *pPabx)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_checkConnect(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_disconnect(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_recvmsg(HF_UINT8 *data, HF_UINT16 maxlen, HF_UINT32 timeout)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_sendmsg(const HF_UINT8 *msg, HF_UINT16 length)
{
    return HF_SUCCESS;
}

HF_INT32 HF_comm_getBuffSz(HF_INT32 *sendBuff, HF_INT32 *recBuff)
{
    return HF_SUCCESS;
}
