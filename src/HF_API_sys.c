#include "HF_API.h"

#include <osal.h>
#include <sys/statfs.h>
#include <sys/sysinfo.h>
#include <xui.h>

#define RETURN_DATA_LENGTH  100

static HF_BOOL IsConnectExist(HF_CHAR *regName, HF_INT32 verType);

static int GuiInit(int statusbar_height)
{
	char value[128];
	char rotate_str[32];
	char statusbar_str[32];
	int ret;
	char *xui_argv[10];
	int  xui_argc;

	ret = OsRegGetValue("ro.fac.lcd.rotate", value);
	if (ret > 0)
	{
		snprintf(rotate_str, sizeof(rotate_str), "ROTATE=%s", value);
	}
	else
	{
		strcpy(rotate_str, "ROTATE=0");
	}

	if (statusbar_height > 0)
	{
		snprintf(statusbar_str, sizeof(statusbar_str), "STATUSBAR=%d", statusbar_height);
	}
	else {
		strcpy(statusbar_str, "STATUSBAR=0");
	}

	xui_argv[0] = rotate_str;
	xui_argv[1] = statusbar_str;
	xui_argv[2] = NULL;
	xui_argc = 2;

	ret = XuiOpen(xui_argc, xui_argv);
	if (ret == XUI_RET_OK) {
		return RET_OK;
	}
	else {
		return -1;
	}
}

//TODO the function name is different from spec.
HF_INT32 HF_sys_startHiperflex(HF_VOID)
{
	//TODO set a global flag, all the api was only enable while the flag is TRUE
	//TODO init Module(such as GSM)
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_startHiperflex");
	OsWlLock();
	OsWlInit(NULL);

	//init xui
	GuiInit(0);
    return HF_SUCCESS;
}

HF_INT32 HF_sys_shutdownHiperflex(HF_VOID)
{
	//TODO unset a global flag, all the api was disable while the flag is FALSE
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_shutdownHiperflex");

	//close gsm module
	OsWlLogout();
	OsWlUnLock();

	//close xui
	XuiClose();

    return HF_SUCCESS;
}

HF_INT32 HF_sys_getBatteryStatus(HF_INT32 * chargStatus, HF_INT32 * Status)
{
	HF_INT32 iStatusBateria = 0;


	if((NULL == chargStatus) || (NULL == Status))
	{
		return RF_ERR_DEVICEFAULT;
	}

	iStatusBateria = OsCheckBattery();
	switch ( iStatusBateria )
	{
		case BATTERY_LEVEL_0:	// 0 a 20%
			*chargStatus = iStatusBateria;
			*Status = HF_SYS_BATTERY_LOW_ALARM;
			break;

		//electric quantity
		case BATTERY_LEVEL_1:	// 21 a 40%
		case BATTERY_LEVEL_2:	// 41 a 60%
		case BATTERY_LEVEL_3:	// 61 a 80%
		case BATTERY_LEVEL_4:	// 81 a 100%
			*chargStatus = iStatusBateria*20 + 10;
			*Status = HF_SYS_BATTERY_NO_CHARGE;
			break;

		//TODO when chargeing we can't get battery level, return chargeStatus is 0 or 100?
		case BATTERY_LEVEL_CHARGE:
		case BATTERY_LEVEL_COMPLETE:
			*chargStatus = 100;
			*Status = HF_SYS_BATTERY_CHARGING;
			break;

		//no battery return error?
		case BATTERY_LEVEL_ABSENT:
			return RF_ERR_DEVICEFAULT;

		// S300 e S800 retornam o valor abaixo
		case ERR_SYS_NOT_SUPPORT:
			return RF_ERR_DEVICEFAULT;
		default:
		//iStatusBateria was not right
			return RF_ERR_DEVICEFAULT;
	}

    return HF_SUCCESS;
}

HF_INT32 HF_sys_alimOff(HF_VOID)
{
	HF_INT32 retVal = 0;

	retVal = OsPowerOff();
	if(RET_OK != retVal)
	{
		return RF_ERR_DEVICEFAULT;
	}

    return HF_SUCCESS;
}

HF_INT32 HF_sys_prtySerialNumber(HF_CHAR *outBuffer, HF_UINT32 buffSz)
{
	HF_INT32 retVal = 0;
	HF_CHAR strVal[RETURN_DATA_LENGTH];

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_prtySerialNumber");

	memset(strVal, 0, sizeof(strVal));

	if(NULL == outBuffer)
	{
		return RF_ERR_DEVICEFAULT;
	}

	retVal = OsRegGetValue("ro.fac.sn", strVal);
	if(retVal < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

	//whether the buffer length is enough
	if(retVal > buffSz)
	{
		return HF_ERR_SMALLBUFF;
	}
	else
	{
		strncpy(outBuffer, strVal, retVal);
	}

    return HF_SUCCESS;
}

HF_INT32 HF_sys_prtyPlataformVersion(HF_CHAR *outBuffer, HF_UINT32 buffSz)
{
	HF_INT32 retVal = 0;
	HF_CHAR strVal[RETURN_DATA_LENGTH];

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_prtyPlataformVersion");

	memset(strVal, 0, sizeof(strVal));

	if(NULL == outBuffer)
	{
		return RF_ERR_DEVICEFAULT;
	}

	retVal = OsRegGetValue("ro.fac.mach", strVal);
	if(retVal < 0)
	{
		return RF_ERR_DEVICEFAULT;
	}

	//whether the buffer length is enough
	if(retVal > buffSz)
	{
		return HF_ERR_SMALLBUFF;
	}
	else
	{
		strncpy(outBuffer, strVal, retVal);
	}

	return HF_SUCCESS;
}

HF_INT32 HF_sys_prtyPlataformName(HF_CHAR *outBuffer, HF_UINT32 buffSz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_prtyPlataformName");

	if(NULL == outBuffer)
	{
		return RF_ERR_DEVICEFAULT;
	}

	if(buffSz < (strlen("PAX") + 1))
	{
		return HF_ERR_SMALLBUFF;
	}

    strncpy(outBuffer, "PAX", (strlen("PAX")+1));
    return HF_SUCCESS;
}

HF_INT32 HF_sys_getConnectionsSuported(HF_INT32 *connTypes, HF_INT32 *size)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_getConnectionsSuported");

	HF_INT32 connectCnt = 0;

	if((NULL == connTypes) || (NULL == size))
	{
		return RF_ERR_DEVICEFAULT;
	}

	//TODO need to confirm the define of all connect
	//check exist in two method OsRegGetValue and OsGetSysVer
	//1.detect the existence of modem
	if(IsConnectExist("ro.fac.modem", TYPE_MODEM_VER))
	{
		connTypes[connectCnt++] = HF_SYS_CONN_DIAL;
	}

	//2.detect the existence of GPRS
	if(IsConnectExist("ro.fac.gprs", TYPE_GPRS_VER))
	{
		connTypes[connectCnt++] = HF_SYS_CONN_GPRS;
	}

	//3.detect the existence of ETHERNET
	if(IsConnectExist("persist.sys.eth0.enable", TYPE_ETH_VER))
	{
		connTypes[connectCnt++] = HF_SYS_CONN_ETHERNET;
	}

	//4.detect the existence of WIFI
	if(IsConnectExist("ro.fac.wifi", TYPE_WIFI_VER))
	{
		connTypes[connectCnt++] = HF_SYS_CONN_WIFI;
	}

	*size = connectCnt;

    return HF_SUCCESS;
}

HF_INT32 HF_sys_getDeviceInfo(RF_UINT32 option, HF_CHAR** output)
{
	//TODO the return information need to confirm
    HF_CHAR *ret = HF_NULL;
    *output = 0;

    PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU HF_sys_getDeviceInfo");

    switch (option)
    {
        case RF_DEVICE_INFO_VERSAO_SO:
            ret = "SO00";
            break;
        case RF_DEVICE_INFO_VERSAO_KERNEL_EMV:
            ret = "EMV000";
            break;
        case RF_DEVICE_INFO_VERSAO_EMV_CTLESS:
            ret = "CTLESS0000";
            break;
        case RF_DEVICE_INFO_VERSAO_CTLESS_JCB:
            ret = "CTLESS0000";
            break;
        case RF_DEVICE_INFO_VERSAO_CTLESS_MASTERCARD:
            ret = "CTLESSMC0000";
            break;
        case RF_DEVICE_INFO_VERSAO_CTLESS_VISA:
            ret = "CTLESSVISA00";
            break;
        case RF_DEVICE_INFO_VERSAO_CTLESS_AMEX:
            ret = "CTLESSAMEX00";
            break;
        default:
            return HF_ERR_INVALIDARG;
    }
    if (*ret != HF_NULL)
    {
        *output = (HF_CHAR*)calloc(strlen(ret) + 1, sizeof(HF_CHAR));
        if (*output == HF_NULL)
        {
            return HF_ERR_NOMEMORY;
        }
        else
        {
            strcpy(*output, ret);
        }
    }
    return HF_SUCCESS;
}

RF_INT32 RF_sys_getTotalRAM (RF_UINT32 *numberInKB)
{
	//TODO whether return a fix value?
	int iRet = 0;
	struct sysinfo sys;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_sys_getTotalRAM");

	memset(&sys, 0, sizeof(sysinfo));

	iRet = sysinfo(&sys);
	if(0 != iRet)
	{
		return HF_NULL;
	}

	OsLog(LOG_DEBUG, "sysinfo iRet:%d free:%d", iRet, sys.freeram);
	OsLog(LOG_DEBUG, "sysinfo iRet:%d totalram:%d", iRet, sys.totalram);
	*numberInKB = (sys.totalram / 1024);

	return HF_SUCCESS;
}

RF_INT32 RF_sys_getTotalStorage (RF_UINT32 *numberInKB)
{
	//TODO whether return a fix value?
	struct statfs diskInfo;

	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_sys_getTotalStorage");
	memset(&diskInfo, 0, sizeof(diskInfo));

	if(statfs("/data", &diskInfo))
	{
		return HF_ERR_DEVICEFAULT;
	}

	OsLog(LOG_DEBUG, "statfs f_bsize:%d", diskInfo.f_bsize);
	OsLog(LOG_DEBUG, "statfs f_blocks:%d", diskInfo.f_blocks);
	OsLog(LOG_DEBUG, "statfs f_bfree:%d", diskInfo.f_bfree);
	*numberInKB = (diskInfo.f_bsize * diskInfo.f_blocks) / 1024;

	return HF_SUCCESS;
}

RF_INT32 RF_sys_prtyPlatformModel (RF_CHAR *outBuffer, RF_INT32 buffSz)
{
	PaxLog(LOG_DEBUG,__FUNCTION__,__LINE__,"ENTROU RF_sys_prtyPlatformModel");

	//TODO need to confirm Model:      Version:     Name:
	if(NULL == outBuffer)
	{
		return RF_ERR_DEVICEFAULT;
	}

	if(buffSz < (strlen("Model") + 1))
	{
		return HF_ERR_SMALLBUFF;
	}

    strncpy(outBuffer, "Model", (strlen("Model")+1));
    return HF_SUCCESS;
}

/***********************NOT USE FUNCTION BEGIN***************************/
HF_UINT32 HF_sys_getInterfaceHandle(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_sys_prtyExtraData(HF_CHAR *outBuffer, HF_UINT32 buffSz)
{
    strncpy(outBuffer, "N/A", buffSz);
    return HF_SUCCESS;
}

HF_INT32 HF_sys_reset(HF_VOID)
{
    return HF_SUCCESS;
}

HF_INT32 HF_sys_setStandbyTimeout(HF_INT32 timeout)
{
    return HF_SUCCESS;
}

HF_INT32 HF_sys_getStandbyTimeout(HF_VOID)
{
    return 0;
}

HF_BOOL HF_sys_isBatteryInitialized (HF_VOID)
{
    return HF_TRUE;
}
/***********************NOT USE FUNCTION END***************************/

//TODO Below is the function define by richard
static HF_BOOL IsConnectExist(HF_CHAR *regName, HF_INT32 verType)
{
	HF_INT32 retVal = 0;
	HF_CHAR versionNum[RETURN_DATA_LENGTH];
	HF_CHAR regInfo[RETURN_DATA_LENGTH];

	memset(regInfo, 0, sizeof(regInfo));
	memset(versionNum, 0, sizeof(versionNum));

	if(NULL == regName)
	{
		retVal = 0;
	}
	else
	{
		retVal = OsRegGetValue(regName, regInfo);
	}

	OsGetSysVer(verType, versionNum);
	if(0 == strcmp(regName, "persist.sys.eth0.enable"))
	{
		//persist.sys.eth0.enable value is different from other
		if(0x00 != versionNum[0])
		{
			return HF_TRUE;
		}

		if(0 < retVal)
		{
			if(0 == strcmp(regName, "false"))
			{
				return HF_FALSE;
			}
		}
		else if(0 == retVal)
		{
			return HF_TRUE;
		}
	}
	else
	{
		if((0x00 != versionNum[0]) || (0 < retVal))
		{
			return HF_TRUE;
		}
	}

	return HF_FALSE;
}
